#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <mpc.h>

double norm(double _Complex z)
{
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

void hsv2rgb(float h, float s, float v, float *r, float *g, float *b)
{
  float i, f, p, q, t;
  if (s == 0) { *r = *g = *b = v; } else
  {
    h = 6 * (h - floorf(h));
    int ii = (i = floorf(h));
    f = h - i;
    p = v * (1 - s);
    q = v * (1 - (s * f));
    t = v * (1 - (s * (1 - f)));
    switch(ii)
    {
      case 0: *r = v; *g = t; *b = p; break;
      case 1: *r = q; *g = v; *b = p; break;
      case 2: *r = p; *g = v; *b = t; break;
      case 3: *r = p; *g = q; *b = v; break;
      case 4: *r = t; *g = p; *b = v; break;
      default:*r = v; *g = p; *b = q; break;
    }
  }
}

struct point_s
{
  int n;
  double Zz2;
  double _Complex Zz;
  double _Complex dc;
  double _Complex dz;
};
typedef struct point_s point_t;

struct feature_s
{
  point_t pre;
  point_t per;
};
typedef struct feature_s feature_t;

bool find_feature_step(feature_t *f, int mode, point_t *stack, int N, const double _Complex *Zptr, double _Complex c, double s, int maxperiod)
{
  const double er2 = 4096;
  const double spr = 0.25;
  const double spr2 = spr * spr;
  const double sppr = 0.5;
  const double sppr2 = sppr * sppr;
  int k = 0;
  int n = 1;
  int m = 1;
  double _Complex dz = s;
  double _Complex dc = s;
  double _Complex z = c;
  double pr2 = 4;
  double ppr2 = 4;
  while (n < N)
  {
    double _Complex Z = Zptr[m];
    double _Complex Zz = Z + z;
    double Zz2 = norm(Zz);
    double z2 = norm(z);

    // rebase
    if (Zz2 < z2)
    {
      z = Zz;
      m = 0;
      Z = 0;
    }

    // bailout
    if (Zz2 > er2)
    {
      return false;
    }

    if (mode & 1)
    {
      // periodic
      double dc2 = norm(dc);
      if (Zz2 < pr2 * dc2)
      {
        feature_t retval = { { 0, 0, 0, 0, 0 }, { n, Zz2, Zz, dc, dz } };
        *f = retval;
        return true;
      }
      double d2 = Zz2 * spr2 / dc2;
      if (d2 < pr2)
      {
        pr2 = d2;
      }
    }
    if (mode & 2)
    {
      // preperiodic
      if (k)
      {
        if (stack[k - 1].Zz2 > Zz2)
        {
          int p;
          do
          {
            p = k - 1;
            --k;
          } while (k && stack[k - 1].Zz2 > Zz2);
          int period = n - stack[p].n;
          if (period <= maxperiod)
          {
            double dZz2 = norm(Zz - stack[p].Zz);
            double ddc2 = norm(dc - stack[p].dc);
            if (dZz2 < ppr2 * ddc2)
            {
              feature_t retval = { stack[p], { n, Zz2, Zz, dc, dz } };
              *f = retval;
              return true;
            }
            double d2 = dZz2 * sppr2 / ddc2;
            if (d2 < ppr2)
            {
              ppr2 = d2;
            }
          }
        }
        if (k)
        {
          int p = k - 1;
          int period = n - stack[p].n;
          if (period <= maxperiod)
          {
            double dZz2 = norm(Zz - stack[p].Zz);
            double ddc2 = norm(dc - stack[p].dc);
            if (dZz2 < ppr2 * ddc2)
            {
              feature_t retval = { stack[p], { n, Zz2, Zz, dc, dz } };
              *f = retval;
              return true;
            }
            double d2 = dZz2 * sppr2 / ddc2;
            if (d2 < ppr2)
            {
              ppr2 = d2;
            }
          }
        }
      }
      point_t p = { n, Zz2, Zz, dc, dz };
      stack[k++] = p;
    }
    if (mode == 0)
    {
      if (n == f->pre.n)
      {
        point_t p = { n, Zz2, Zz, dc, dz };
        f->pre = p;
      }
      if (n == f->per.n)
      {
        point_t p = { n, Zz2, Zz, dc, dz };
        f->per = p;
        return true;
      }
    }

    // iteration
    dz = 2 * Zz * dz;
    dc = 2 * Zz * dc + s;
    z = (2 * Z + z) * z + c;
    ++m;
    ++n;

  }
  return false;
}

bool find_feature(feature_t *f, double _Complex *c_out, int mode, point_t *stack, int N, const double _Complex *Zptr, double _Complex c_guess, double s, int maxperiod)
{
  const double tol = 1.0 / (1ull << 40);
  const double tol2 = tol * tol;
  const int steps = 32;
  double _Complex c = c_guess;
  // find (pre)periods and do one Newton step
  double _Complex d;
  if (! find_feature_step(f, mode, stack, N, Zptr, c, s, maxperiod))
  {
    return false;
  }
  d = (f->per.Zz - f->pre.Zz) / ((f->per.dc - f->pre.dc) / s);
  c -= d;
  // do Newton steps
  for (int i = 0; i < steps; ++i)
  {
    if (! find_feature_step(f, 0, 0, N, Zptr, c, s, maxperiod))
    {
      return false;
    }
    d = (f->per.Zz - f->pre.Zz) / ((f->per.dc - f->pre.dc) / s);
    c -= d;
    if (norm(d) < norm(c) * tol2)
    {
      break;
    }
  }
  // one more Newton step
  if (! find_feature_step(f, 0, 0, N, Zptr, c, s, maxperiod))
  {
    return false;
  }
  d = (f->per.Zz - f->pre.Zz) / ((f->per.dc - f->pre.dc) / s);
  c -= d;
  *c_out = c;
  return true;
}

double exterior_de(int N, const double _Complex *Zptr, double _Complex c, double s)
{
  const double er2 = 4096;
  int n = 0;
  int m = 0;
  double _Complex dc = 0;
  double _Complex z = 0;
  while (n < N)
  {
    double _Complex Z = Zptr[m];
    double _Complex Zz = Z + z;
    double Zz2 = norm(Zz);
    double z2 = norm(z);

    // rebase
    if (Zz2 < z2)
    {
      z = Zz;
      m = 0;
      Z = 0;
    }

    // bailout
    if (Zz2 > er2)
    {
      return 0.5 * sqrt(Zz2) * log(Zz2) / cabs(dc);
    }

    // iteration
    dc = 2 * Zz * dc + s;
    z = (2 * Z + z) * z + c;
    ++m;
    ++n;
  }
  return 0;
}

struct pair_s
{
  int period;
  int count;
};
typedef struct pair_s pair_t;

int cmp_count_desc(const void *a, const void *b)
{
  const pair_t *p = a;
  const pair_t *q = b;
  int x = p->count;
  int y = q->count;
  return (x < y) - (y < x);
}

int main(int argc, char **argv)
{
  if (argc != 7)
  {
    fprintf(stderr, "usage: %s re im zoom iterations maxppperiod mode\n", argv[0]);
    return 1;
  }
  const int Width = 640 * 3;
  const int Height = 360 * 3;
  const char *Re = argv[1];
  const char *Im = argv[2];
  const char *Zoom = argv[3];
  const int Iterations = atoi(argv[4]);
  const int MaxPPPeriod = atoi(argv[5]);
  const int mode = atoi(argv[6]);

  // initialize
  const double pixel_spacing = 4 / atof(Zoom) / Height;
  const int prec = fmax(53, 16 - log2(pixel_spacing));
  mpc_t C0, C;
  mpc_init2(C0, prec);
  mpc_init2(C, prec);
  mpfr_set_str(mpc_realref(C0), Re, 10, MPFR_RNDN);
  mpfr_set_str(mpc_imagref(C0), Im, 10, MPFR_RNDN);
  mpfr_set_str(mpc_realref(C), /*R*/Re, 10, MPFR_RNDN);
  mpfr_set_str(mpc_imagref(C), /*R*/Im, 10, MPFR_RNDN);
  mpc_sub(C0, C0, C, MPC_RNDNN);
  double _Complex c0 = mpc_get_dc(C0, MPC_RNDNN);

  // calculate reference
  double _Complex *Zptr = malloc(sizeof(*Zptr) * Iterations);
  mpc_t Z;
  mpc_init2(Z, prec);
  mpc_set_dc(Z, 0, MPC_RNDNN);
  for (int i = 0; i < Iterations; ++i)
  {
    Zptr[i] = mpc_get_dc(Z, MPC_RNDNN);
    mpc_sqr(Z, Z, MPC_RNDNN);
    mpc_add(Z, Z, C, MPC_RNDNN);
  }

  // calculate images
  pair_t *pphist = calloc(1, sizeof(*pphist) * MaxPPPeriod);
  unsigned char *ppm = malloc(Width * Height * 3);
  double scale = 30 * pixel_spacing;
  #pragma omp parallel for schedule(dynamic, 1)
  for (int j = 0; j < Height; ++j)
  {
    point_t *stack = malloc(sizeof(*stack) * Iterations);
    for (int i = 0; i < Width; ++i)
    {
      double _Complex c_guess = c0 + (((i + 0.5) / Width - 0.5) * Width + I * (0.5 - (j + 0.5) / Height) * Height) * pixel_spacing;
      double _Complex c_out;
      feature_t f;
      float r = 1, g = 1, b = 1;
      if (find_feature(&f, &c_out, mode, stack, Iterations, Zptr, c_guess, scale, MaxPPPeriod))
      {
        float distance = cabs(c_out - c_guess) / (2 * scale);
        if (distance < 1)
        {
          float angle = carg(c_out - c_guess) / (2 * 3.141592653);
          int preperiod = f.pre.n;
          int period = f.per.n - f.pre.n;
          float h, s, v;
          if (preperiod)
          {
            h = angle * period;
            s = 0.75;
            v = 1 - distance  * distance;
            int *count = &pphist[period - 1].count;
            #pragma omp atomic
            (*count)++;
          }
          else
          {
            //double size = 1 / cabs((f.per.dz / scale) * (f.per.dc / scale)); // for periodic only?
            h = (sqrtf(5) - 1)/2 * period;
            s = 0.75;
            v = 1 - distance * distance;
          }
          hsv2rgb(h, s, v, &r, &g, &b);
        }
      }
      float de = fmin(fmax(0.5 + 0.5 * exterior_de(Iterations, Zptr, c_guess, pixel_spacing), 0), 1);
      r *= de;
      g *= de;
      b *= de;
      int k = (j * Width + i) * 3;
      ppm[k++] = 255 * r;
      ppm[k++] = 255 * g;
      ppm[k++] = 255 * b;
    }
    free(stack);
  }
  for (int p = 0; p < MaxPPPeriod; ++p)
  {
    pphist[p].period = p + 1;
  }
  qsort(pphist, MaxPPPeriod, sizeof(*pphist), cmp_count_desc);
  fprintf(stderr, "# period count\n");
  for (int i = 0; i < 10 && i < MaxPPPeriod; ++i)
  {
    if (pphist[i].count)
    {
      fprintf(stderr, "%d %d\n", pphist[i].period, pphist[i].count);
    }
  }
  // output image
  fprintf(stdout, "P6\n%d %d\n255\n", Width, Height);
  fwrite(ppm, Width * Height * 3, 1, stdout);
  fflush(stdout);
  return 0;
}
