#!/bin/bash
# expects 256 grayscale png files numbered 000.png ... 255.png each 2048x2048
# overwrites temporary PGM files numbered similarly
# output overwrites ../slit-scan.pgm
# the magic number 1887 was tuned in ghci
n=256
y=0
for h in $(ghc -e "putStr . unwords . map (show . ceiling . (1887*)) . (\l -> map (/ sum l) l) $ [ x^4 | x <- [1 .. 256]]")
do
  n=$((n-1))
  convert $(printf %03d $n).png -crop 2048x${h}+0+${y} $(printf %03d $n).pgm
  y=$((y+h))
done
pnmcat -tb $(ls *.pgm | tac) > ../slit-scan.pgm
