#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <cairo/cairo.h>

#define D 2
#define W 256
#define H 192
#define V (W * H)
#define E 8

#define snaps 0.999
#define jitter 1.0
#define shrink 0.99
#define drag 0.9
#define dt 0.1

struct
{
  double position[D];
  double velocity[D];
  int edge[E];
  int dryness;
} vertex[V];

struct
{
  double length;
  double force[D];
  double F;
  int vertex[2];
} edge[E * V / 2];
double force[E * V / 2];
int edges;

void init2d(void)
{
  for (int v = 0; v < V; ++v)
  {
    vertex[v].velocity[0] = 0;
    vertex[v].velocity[1] = 0;
    for (int e = 0; e < E; ++e)
    {
      vertex[v].edge[e] = -1;
    }
  }
  int e = 0;
  for (int j = 0; j < H; ++j)
  for (int i = 0; i < W; ++i)
  {
    int v = j * W + i;
    vertex[v].position[0] = j + jitter * rand()/(double)RAND_MAX;
    vertex[v].position[1] = i + jitter * rand()/(double)RAND_MAX;
#define EDGE(f,dj,di) \
    if (dj <= j && j-dj < H && di <= i && i-di < W) \
    { \
      int u = (j-dj) * W + (i-di); \
      double x = vertex[u].position[0] - vertex[v].position[0]; \
      double y = vertex[u].position[1] - vertex[v].position[1]; \
      edge[e].length = hypot(x, y); \
      edge[e].vertex[0] = u; \
      edge[e].vertex[1] = v; \
      vertex[u].edge[f] = e; \
      vertex[v].edge[f+4] = e; \
      ++e; \
    }
    EDGE(0,1,1)
    EDGE(1,1,0)
    EDGE(2,1,-1)
    EDGE(3,0,1)
#undef EDGE
  }
  edges = e;
  for (int v = 0; v < V; ++v)
  {
    int d = 0;
    for (int e = 0; e < E; ++e)
    {
      d += vertex[v].edge[e] < 0;
    }
    vertex[v].dryness = d;
  }
}

void forces(double dryness)
{
  #pragma omp parallel for
  for (int e = 0; e < edges; ++e)
  {
    int u = edge[e].vertex[0];
    int v = edge[e].vertex[1];
    if (u >= 0 && v >= 0)
    {
      double s = 0;
      for (int d = 0; d < D; ++d)
      {
        double x = edge[e].force[d] = vertex[u].position[d] - vertex[v].position[d];
        s += x * x;
      }
      s = sqrt(s);
      double F = pow(shrink, vertex[u].dryness + vertex[v].dryness + dryness) * edge[e].length - s;
      force[e] = edge[e].F = fabs(F);
      F /= s;
      for (int d = 0; d < D; ++d)
      {
        edge[e].force[d] *= F;
      }
    }
    else
    {
      force[e] = -1;
      edge[e].F = -1;
    }
  }
}

int cmp_double_desc(const void *a, const void *b)
{
  const double *x = a;
  const double *y = b;
  return (*x < *y) - (*y < *x);
}

int snapped = 0;
void snap(void)
{
#if 0
  qsort(force, edges, sizeof(*force), cmp_double_desc);
  double threshold = force[snaps];
#else
  double threshold = 0;
  #pragma omp parallel for reduction(max:threshold)
  for (int e = 0; e < edges; ++e)
  {
    threshold = threshold > force[e] ? threshold : force[e];
  }
  threshold *= snaps;
#endif
  #pragma omp parallel for
  for (int e = 0; e < edges; ++e)
  {
    int u = edge[e].vertex[0];
    int v = edge[e].vertex[1];
    if (u >= 0 && v >= 0)
    {
      if (edge[e].F >= threshold)
      {
        for (int f = 0; f < E; ++f)
        {
          if (vertex[u].edge[f] == e)
          {
            vertex[u].edge[f] = -1;
            vertex[u].dryness++;
            #pragma omp atomic
            snapped++;
          }
          if (vertex[v].edge[f] == e)
          {
            vertex[v].edge[f] = -1;
            vertex[v].dryness++;
            #pragma omp atomic
            snapped++;
          }
        }
        edge[e].vertex[0] = -1;
        edge[e].vertex[1] = -1;
      }
    }
  }
}

void accum(void)
{
  #pragma omp parallel for
  for (int v = 0; v < V; ++v)
  {
    for (int d = 0; d < D; ++d)
    {
      vertex[v].velocity[d] *= drag;
    }
    for (int f = 0; f < E; ++f)
    {
      int e = vertex[v].edge[f];
      if (e >= 0)
      {
        if (edge[e].vertex[0] == v)
        {
          for (int d = 0; d < D; ++d)
          {
            vertex[v].velocity[d] += dt * edge[e].force[d];
          }
        }
        else
        {
          for (int d = 0; d < D; ++d)
          {
            vertex[v].velocity[d] -= dt * edge[e].force[d];
          }
        }
      }
    }
    for (int d = 0; d < D; ++d)
    {
      vertex[v].position[d] += dt * vertex[v].velocity[d];
    }
  }
}

unsigned char pgm[1080][1920];

int main(int argc, char **argv)
{
  if (argc > 1) srand(atoi(argv[1]));
  int bg = 1;
  if (argc > 2) bg = !!atoi(argv[2]);
  int fg = ! bg;
  init2d();
  cairo_surface_t *s = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, 1920, 1080);
  cairo_t *c = cairo_create(s);
  cairo_scale(c, 1920.0/W, 1080.0/H);
  cairo_set_line_width(c, 1);
  cairo_set_line_cap(c, CAIRO_LINE_CAP_ROUND);
  for (int frame = 0; frame < edges; ++frame)
  {
    if (frame % 10 == 0)
    {
      cairo_set_source_rgba(c,bg,bg,bg,1);
      cairo_paint(c);
      cairo_set_source_rgba(c,fg,fg,fg,1);
      for (int e = 0; e < edges; ++e)
      {
        int u = edge[e].vertex[0];
        int v = edge[e].vertex[1];
        if (u >= 0 && v >= 0)
        {
          cairo_move_to(c, vertex[u].position[1], vertex[u].position[0]);
          cairo_line_to(c, vertex[v].position[1], vertex[v].position[0]);
        }
      }
      cairo_stroke(c);
#if 0
      char filename[100];
      snprintf(filename, 100, "%08d.png", frame);
      cairo_surface_write_to_png(s, filename);
      printf("%s\n", filename);
#else
      cairo_surface_flush(s);
      unsigned char *p = cairo_image_surface_get_data(s);
      int stride = cairo_image_surface_get_stride(s);
      #pragma omp parallel for
      for (int j = 0; j < 1080; ++j)
      for (int i = 0; i < 1920; ++i)
        pgm[j][i] = p[j * stride + i * 4 + 2];
      printf("P5\n%d %d\n255\n", 1920, 1080);
      fwrite(pgm, 1920 * 1080, 1, stdout);
      fflush(stdout);
#endif
      if (snapped >= 2 * edges) break;
    }

    forces(frame / 1000.0);
    snap();
    accum();
  }
  cairo_destroy(c);
  cairo_surface_destroy(s);
  return 0;
}
