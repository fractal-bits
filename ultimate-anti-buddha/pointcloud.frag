#version 330 core
#extension GL_ARB_explicit_uniform_location : require

in vec4 f_colour;

out vec4 colour;

void main(void)
{
  colour = f_colour;
}
