#version 330 core
#extension GL_ARB_explicit_uniform_location : require

layout(location = 1) in vec4 v_position;
layout(location = 2) in float v_colour;

out vec4 f_colour;

layout(location = 13) uniform bool stereo;
layout(location = 1) uniform mat4 projection4d;
layout(location = 5) uniform mat4 projection3d[2];

void main(void)
{
  f_colour = vec4(1.0 - gl_InstanceID, stereo ? gl_InstanceID : 1.0, 1.0 - gl_InstanceID, 1) * vec4(vec3(v_colour), 1);
  gl_Position = projection3d[gl_InstanceID] * vec4((projection4d * v_position).xyz, 1);
}
