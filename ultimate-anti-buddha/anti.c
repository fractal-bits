// Ultimate Anti-Buddhabrot (c) 2013, 2024 Claude Heiland-Allen
// https://mathr.co.uk/blog/2013-12-30_ultimate_anti-buddhabrot.html
// gcc -std=c99 -Wall -pedantic -Wextra -O3 -fopenmp -o anti anti.c -lm
// ./anti

#include <assert.h>
#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

typedef unsigned char B;
typedef unsigned int N;
typedef unsigned long int NN;
typedef int Z;
typedef long int ZZ;
typedef double R;
typedef double complex C;
typedef float F;

B logo[16][16];

static const NN threads = 16;

static const R cradius = 2;
static Z zwidth;
static Z zheight;
#define zframes 64
#define fps 30

#define bpm 155.0
#define beats 8
#define duration 2048
#define total_frames (duration/beats / (bpm / 60 / fps / beats))

static const R gain = 1 << 30; // 14 bits of colour accuracy at 1<<16 iterations
                               // 8 bits at 1<<22 iterations...
                               // 1 bit at 1<<29 iterations...
static const R threshold = 5.65685424949238; // 4 * sqrt(2);
static R nearness;

static const NN memory_limit = 24ull << 30; // GiB
static NN memory_allocated = 0;

static const bool subdivide_far_interior = false;

// draw boundary only
static const R absolute_de_threshold = 1.0 / 0.0; // (1 << 16);

static const F dz_saturation = 2. / 3.;
static const F phase_saturation = 2. / 3.;
static const F parent_saturation = 2. / 3.;
static const F parent_strength = 1. / 2.; // colour according to dz (0) vs phase within period (1)
static const F phase_strength = 2. / 3.; // colour according to dz (0) vs phase within period (1)

//#define GRID_SIZE 10
//#define GRID_DIM (1<<GRID_SIZE)
#define HISTOGRAM_SIZE 10
#define HISTOGRAM_DIM (1 << HISTOGRAM_SIZE)

// runtime state for checkpoints
static NN *count; // [zframes][zheight][zwidth][4];
//static NN *hit; // [zheight][zwidth];
//static NN prng[GRID_DIM][GRID_DIM];
static NN potential_errors;
static NN total_orbits;
static NN total_period;
//static NN culled_orbits;
//static NN culled_period;
static NN culled;
static NN samples;
static NN plotted;
static NN level;

// other runtime state
static C z0 = 0;
static R zradius = 2.5;
static NN maxiters;
static R zpixel_size;
static F *pfm; // [zheight][zwidth][3];
static B *ppm; // [zheight][zwidth][3];
static NN histogram[HISTOGRAM_DIM];
static R histogram1[16];

typedef struct
{
  F x[4];
} V;

typedef struct
{
  F x[4][4];
} M;

typedef struct
{
  F x[2][2];
} M2;

typedef struct
{
  float _Complex x[2][2];
} CM;

typedef struct
{
  F pos[4];
#if 0
  N rgba[4];
#endif
//  N vi;
#ifdef SPLAT
  CM basis;
  CM basis1;
  F size[2];
#endif
} P;

M matrixComplex(CM x)
{
  M o = {{
    { creal(x.x[0][0]), -cimag(x.x[0][0]), creal(x.x[0][1]), -cimag(x.x[0][1]) },
    { cimag(x.x[0][0]),  creal(x.x[0][0]), cimag(x.x[0][1]),  creal(x.x[0][1]) },
    { creal(x.x[1][0]), -cimag(x.x[1][0]), creal(x.x[1][1]), -cimag(x.x[1][1]) },
    { cimag(x.x[1][0]),  creal(x.x[1][0]), cimag(x.x[1][1]),  creal(x.x[1][1]) }
  }};
  return o;
}

#define max_points (1ull << 26)
P points[max_points];
F colour[max_points];
NN num_points = 0;

NN xorshift64(NN x)
{
  x ^= x << 13;
  x ^= x >> 7;
  x ^= x << 17;
  return x;
}
/*
void randomize(NN seed)
{
  for (N y = 0; y < GRID_DIM; ++y)
  {
    for (N x = 0; x < GRID_DIM; ++x)
    {
      prng[y][x] = 0;
      for (N chunk = 0; chunk < 8; ++chunk)
      {
        NN u = (seed = xorshift64(seed));
        prng[y][x] <<= 8;
        prng[y][x] |= (NN) ((1 << 8) * (u - 1.0) / 0xFFFFffffFFFFfffeul) & ((1 << 8) - 1);
      }
      prng[y][x] += ! prng[y][x];
    }
  }
}
*/

void allocate(void)
{
  NN bytes = sizeof(*count) * zframes * zwidth * zheight * 4;
  count = malloc(bytes);
  memory_allocated += bytes;
/*
  bytes = sizeof(*hit) * zwidth * zheight;
  hit = malloc(bytes);
  memory_allocated += bytes;
*/
  bytes = 3ul * zwidth * zheight;
  ppm = malloc(bytes);
  memory_allocated += bytes;
  bytes = sizeof(*pfm) * 3 * zwidth * zheight;
  pfm = malloc(bytes);
  memory_allocated += bytes;
}

void deallocate(void)
{
  free(pfm);
  free(ppm);
//  free(hit);
  free(count);
}

void initialize(/*NN seed*/ void)
{
  //randomize(seed);
  memset(count, 0, sizeof(*count) * zframes * zwidth * zheight * 4);
//  memset(hit, 0, sizeof(*hit) * zwidth * zheight);
  potential_errors = 0;
  total_orbits = 0;
  total_period = 0;
  samples = 0;
  plotted = 0;
}

static inline R cabs2(C z) { return creal(z) * creal(z) + cimag(z) * cimag(z); }

static inline int wucleus(C *z0, C c, NN period) {
  R eps = 1e-12;
  R eps2 = eps * eps;
  C z = *z0;
  for (N j = 0; j < 256; ++j) {
    C dz = 1.0;
    for (NN k = 0; k < period; ++k) {
      dz = 2.0 * dz * z;
      z = z * z + c;
    }
    z = *z0 - (z - *z0) / (dz - 1.0);
    R e = cabs2(z - *z0);
    *z0 = z;
    if (e < eps2) {
      return 1;
    }
    if (isnan(e) || isinf(e))
    {
      return 0;
    }
  }
  return 0;
}

static void hsv2rgb(F h, F s, F v, F *rp, F *gp, F *bp) {
  F i, f, p, q, t, r, g, b;
  Z ii;
  if (s == 0.0) {
    r = v;
    g = v;
    b = v;
  } else {
    h = h - floor(h);
    h = h * 6.0;
    i = floor(h);
    ii = i;
    f = h - i;
    p = v * (1 - s);
    q = v * (1 - (s * f));
    t = v * (1 - (s * (1 - f)));
    switch(ii) {
      case 0:  r = v; g = t; b = p; break;
      case 1:  r = q; g = v; b = p; break;
      case 2:  r = p; g = v; b = t; break;
      case 3:  r = p; g = q; b = v; break;
      case 4:  r = t; g = p; b = v; break;
      default: r = v; g = p; b = q; break;
    }
  }
  *rp = r;
  *gp = g;
  *bp = b;
}

static inline F mix(F a, F b, F t)
{
  return a + (b - a) *  t;
}

static inline V vmuls(V m, F u)
{
  for (int i = 0; i < 4; ++i)
  {
    m.x[i] *= u;
  }
  return m;
}

static inline M mmuls(M m, F u)
{
  for (int i = 0; i < 4; ++i)
  for (int j = 0; j < 4; ++j)
  {
    m.x[i][j] *= u;
  }
  return m;
}

static inline M2 mmuls2(M2 m, F u)
{
  for (int i = 0; i < 2; ++i)
  for (int j = 0; j < 2; ++j)
  {
    m.x[i][j] *= u;
  }
  return m;
}

static inline M msubm(M m, M u)
{
  for (int i = 0; i < 4; ++i)
  for (int j = 0; j < 4; ++j)
  {
    m.x[i][j] -= u.x[i][j];
  }
  return m;
}

static inline V vaddv(V m, V u)
{
  for (int i = 0; i < 4; ++i)
  {
    m.x[i] += u.x[i];
  }
  return m;
}

static inline V mmulv(M m, V u)
{
  V v = {{0,0,0,0}};
  for (int i = 0; i < 4; ++i)
  for (int j = 0; j < 4; ++j)
  {
    v.x[i] += m.x[i][j] * u.x[j];
  }
  return v;
}

static inline M mmulm(M m, M n)
{
  M o = {{{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}}};
  for (int i = 0; i < 4; ++i)
  for (int j = 0; j < 4; ++j)
  for (int k = 0; k < 4; ++k)
  {
    o.x[i][j] += m.x[i][k] * n.x[k][j];
  }
  return o;
}

static inline F mnorm(M m)
{
  F o = 0;
  for (int i = 0; i < 4; ++i)
  for (int j = 0; j < 4; ++j)
    o += m.x[i][j] * m.x[i][j];//fmaxf(o, fabsf(m.x[i][j]));
  return o;
}

static inline M transpose(M m)
{
  M o;
  for (int i = 0; i < 4; ++i)
  for (int j = 0; j < 4; ++j)
    o.x[i][j] = m.x[j][i];
  return o;
}

static inline CM ctranspose(CM m)
{
  CM o;
  for (int i = 0; i < 2; ++i)
  for (int j = 0; j < 2; ++j)
    o.x[i][j] = m.x[j][i];
  return o;
}

static inline F vabs(V v)
{
  F o = 0;
  for (int i = 0; i < 4; ++i)
  {
    o = fmaxf(o, fabsf(v.x[i]));
  }
  return o;
}

static inline F vnorm(V v)
{
  F o = 0;
  for (int i = 0; i < 4; ++i)
  {
    o += v.x[i] * v.x[i];
  }
  return o;
}

static inline CM cinverse(CM m)
{
  float _Complex det = m.x[0][0] * m.x[1][1] - m.x[0][1] * m.x[1][0];
  return (CM){{
    { m.x[1][1] / det, -m.x[0][1] / det },
    { -m.x[1][0] / det,  m.x[0][0] / det }
  }};
}

static inline M2 mmulm2(M2 m, M2 n)
{
  M2 o = {{{0,0},{0,0}}};
  for (int i = 0; i < 2; ++i)
  for (int j = 0; j < 2; ++j)
  for (int k = 0; k < 2; ++k)
  {
    o.x[i][j] += m.x[i][k] * n.x[k][j];
  }
  return o;
}

static inline V eig1(M A, F *l1)
{
  V b = {{ 1, 1, 1, 1 }};
  for (int n = 0; n < 100; ++n)
  {
    b = vmuls(b, 1 / sqrt(vnorm(b)));
    b = mmulv(A, b);
  }
  V b1 = mmulv(A, b);
  F sign = b1.x[0] / b.x[0] < 0 ? -1 : 1;
  *l1 = sign * sqrt(vnorm(b));
  return vmuls(b, 1 / *l1);
}

M outer(V u, V v)
{
  M o;
  for (int i = 0; i < 4; ++i)
  for (int j = 0; j < 4; ++j)
    o.x[i][j] = u.x[i] * v.x[j];
  return o;
}

static inline V eig2(M A, V v1, F l1, F *l2)
{
  M B = msubm(A, mmuls(outer(v1, v1), l1));
  return eig1(B, l2);
}

static R animation_time[zframes];
static M transformation_matrix[zframes];
static M transformation_matrix1[zframes];

bool animation_filter(V u, R t)
{
  R p = 4 * t - floor(4 * t);
  Z b = floor(t);
  switch (b & 0)
  {
  case 0: return true;//-2 + 4 * p - 0.01 < u.x[0] && u.x[0] < -2 + 4 * p + 0.01;
  }
  return false;
}

static void plot_iterates(C z1, C c, NN period, NN previous_period, C dz0, R de, N level)
{
/*
  // compute colour based on position relative to component
  F h = carg(dz0) / (2 * M_PI), s = 0*cabs2(dz0), v = 1, r0, g0, b0;
  hsv2rgb(h, s * dz_saturation, v, &r0, &g0, &b0);

  // compute colour based on position relative to parent
previous_period = 1;
  C zp = c; // z1;
  C dzp = 1;
  if (wucleus(&zp, c, previous_period))
  {
    for (NN i = 0; i < previous_period; ++i)
    {
      dzp *= zp;
      zp = zp * zp + c;
    }
  }
  F r2, g2, b2;
  hsv2rgb(carg(dzp) / (2 * M_PI), s * parent_saturation, 1, &r2, &g2, &b2);
  r0 = mix(r0, r2, parent_strength);
  g0 = mix(g0, g2, parent_strength);
  b0 = mix(b0, b2, parent_strength);
*/
  C z = z1, dz = 1, dc = 0;
/*
    #pragma omp atomic
    total_orbits += 1;
    #pragma omp atomic
    total_period += period;
*/
  for (NN i = 0; i < period; ++i) {
    V uu = {{ creal(c), cimag(c), creal(z), cimag(z) }};
#ifdef SPLAT
  R de_c, de_z;
  {
    C w = z, dz = 1, dc = 0, dzdz = 0, dcdz = 0;
    for (NN j = 0; j < period; ++j)
    {
      dzdz = 2 * (w * dzdz + dz * dz);
      dcdz = 2 * (w * dcdz + dc * dz);
      dc = 2 * w * dc + 1;
      dz = 2 * w * dz;
      w = w * w + c;
    }
    de_c = (1 - cabs2(dz)) / cabs(dcdz + dzdz * dc / (1 - dz));
    de_z = (1 - cabs2(dz)) / cabs(dzdz + dcdz * (1 - dz) / dc);
  }

/*
  dc/dc  dz/dc
  dc/dz0 dz/dz0
*/

  CM basis = (CM){{{ 1, dc }, { i == 0 ? 0/*?*/ : (1 - dz) / dc, dz }}};
  CM basis1 = cinverse(basis);
#endif

NN me;
#pragma omp atomic capture
me = num_points++;
if (me < max_points)
{
//  if (0 < i)
/*
      F r1, g1, b1;
      hsv2rgb(i / (F) period, s * phase_saturation, v, &r1, &g1, &b1);
      F r = mix(r0, r1, phase_strength), g  = mix(g0, g1, phase_strength), b = mix(b0, b1, phase_strength);
*/
      N lx = fmin(fmax((creal(dz0) + 1) * 8, 0), 15);
      N ly = fmin(fmax((cimag(dz0) + 1) * 8, 0), 15);
      R amp = gain / (R) period * (logo[lx][ly] / 255.0) * (logo[lx][ly] / 255.0);
      NN vi = ceil(amp);
/*
      NN vi = ceil(v * amp);
      NN ri = ceil(r * amp), gi = ceil(g * amp), bi = ceil(b * amp);
*/

  points[me] = (P){
    { uu.x[0], uu.x[1], uu.x[2], uu.x[3] },
//    { ri, gi, bi, vi }
//    vi
#ifdef SPLAT
    ,
    basis, basis1,
    { de_c / 4, de_z / 4 }
#endif
  };
  colour[me] = amp / gain; 
}
else
{
  abort();
}

#ifdef SPLAT
    dc = 2 * z * dc + 1;
    dz = 2 * z * dz;
#endif
    z = z * z + c;

#if 0
  for (NN frame = 0; frame < zframes; ++frame)
  {
    V vv = mmulv(transformation_matrix[frame], uu);
    C w = vv.x[0] + I * vv.x[1];
    C pz0 = (zwidth / 2.0 - 0.5) + I * (zheight / 2.0 - 0.5);
    C pz = (w - z0) / zpixel_size + pz0;
    if (0 <= creal(pz) && creal(pz) < zwidth && 0 <= cimag(pz) && cimag(pz) < zheight) {
      // compute colour based on iteration relative to period
      NN x = creal(pz);
      NN y = cimag(pz);
      NN k = ((frame * zheight + y) * zwidth) + x;
/*
      #pragma omp atomic
      hit[k] += 1;
*/
      k <<= 2;
      #pragma omp atomic
      count[k+0] += ri;
      #pragma omp atomic
      count[k+1] += gi;
      #pragma omp atomic
      count[k+2] += bi;
/*
      // unlikely race condition: very many increments in between
      NN old_count, new_count;
      #pragma omp atomic read
      old_count = count[k+3];
*/
      #pragma omp atomic
      count[k+3] += vi;
/*
      #pragma omp atomic read
      new_count = count[k+3];
      if (new_count < old_count)
      {
        fprintf(stderr, "OVERFLOW\n");
        abort();
      }
*/
    }
  }
#endif
  }
}

static void plot_points(void)
{
  NN ff = (zframes + threads - 1) / threads;
  F scale = 1 / zpixel_size;
  F x0 = -creal(z0) * scale + (zwidth / 2.0 - 0.5);
  F y0 = -cimag(z0) * scale + (zheight / 2.0 - 0.5);
  NN progress = 0;
  NN total = num_points * zframes;
  #pragma omp parallel for schedule(static,1)
  for (NN thread = 0; thread < threads; ++thread)
  {

  for (NN point = 0; point < num_points; ++point)
  {
    P self = points[point];
    V uu = {{ self.pos[0], self.pos[1], self.pos[2], self.pos[3] }};
/*
    NN ri = self.rgba[0];
    NN gi = self.rgba[1];
    NN bi = self.rgba[2];
    NN vi = self.rgba[3];
*/
    NN vi = ceil(gain * colour[point]);
/*
    M basis = self.basis;
    V ux = {{1,0,0,0}};
    V uy = {{0,1,0,0}};
*/
#ifdef SPLAT
    M size_m = {{{self.size[0], 0, 0, 0}, {0, self.size[0], 0, 0}, {0, 0, self.size[1], 0}, {0, 0, 0, self.size[1]} }};
    M basis = matrixComplex(self.basis);
    M basis1 = matrixComplex(self.basis1);
#endif

  for (NN f = 0; f < ff; ++f)
  {
    NN frame = ff * thread + f;
    if (frame >= zframes) break;

#if 0
    if (! animation_filter(mmulv(transformation_matrix1[frame], uu), animation_time[frame])) continue;
#endif

    V vv = mmulv(transformation_matrix[frame], uu);

/*

for each pixel vv + (u, v), solve

(scale view basis) x = (u, v, 0, 0)

then test |x.c| < size_c and |x.z| < size_z

*/
#ifdef SPLAT
    M dp = mmulm(transformation_matrix[frame], mmulm(basis, size_m));
//    F rx = fmaxf(fmaxf(fmaxf(fabsf(dp.x[0][0]), fabsf(dp.x[1][0])), fabsf(dp.x[2][0])), fabsf(dp.x[3][0])) * scale;
//    F ry = fmaxf(fmaxf(fmaxf(fabsf(dp.x[0][1]), fabsf(dp.x[1][1])), fabsf(dp.x[2][1])), fabsf(dp.x[3][1])) * scale;
    F rx = sqrtf(dp.x[0][0] * dp.x[0][0] + dp.x[1][0] * dp.x[1][0] + dp.x[2][0] * dp.x[2][0] + dp.x[3][0] * dp.x[3][0]) * scale;
    F ry = sqrtf(dp.x[0][1] * dp.x[0][1] + dp.x[1][1] * dp.x[1][1] + dp.x[2][1] * dp.x[2][1] + dp.x[3][1] * dp.x[3][1]) * scale;
    F r = fmaxf(rx, ry);
    V xx = vmuls(mmulv(mmulm(transpose(basis1), transformation_matrix1[frame]), (V){{1,0,0,0}}), 1 / scale);
    V yy = vmuls(mmulv(mmulm(transpose(basis1), transformation_matrix1[frame]), (V){{0,1,0,0}}), 1 / scale);
#endif

NN me;
#pragma omp atomic capture
me = progress++;
if (100 * me / total != 100 * (me - 1) / total)
{
  #pragma omp critical
  fprintf(stderr, "%3d%%\r", (int) (100 * me / total));
}

//   rx = fminf(rx, 64);
//   ry = fminf(ry, 64);

    F xf0 = vv.x[0] * scale + x0;
    F yf0 = vv.x[1] * scale + y0;
#ifdef SPLAT
    for (F dxf = -ceilf(r); dxf <= ceilf(r); dxf += 1)
    for (F dyf = -ceilf(r); dyf <= ceilf(r); dyf += 1)
    {
      V zz = vaddv(vmuls(xx, dxf), vmuls(yy, dyf));
    if (zz.x[0] * zz.x[0] + zz.x[1] * zz.x[1] < self.size[0] * self.size[0] &&
        zz.x[2] * zz.x[2] + zz.x[3] * zz.x[3] < self.size[1] * self.size[1])
    {
      F xf = xf0 + dxf;
      F yf = yf0 + dyf;
#else
      F xf = xf0;
      F yf = yf0;
#endif
    if (0 <= xf && xf < zwidth && 0 <= yf && yf < zheight) {
      // compute colour based on iteration relative to period
      NN x = xf;
      NN y = yf;
      NN k = ((frame * zheight + y) * zwidth) + x;

#if 1
      count[k] += vi;
#else
/*
      #pragma omp atomic
      hit[k] += 1;
*/
      k <<= 2;
//      #pragma omp atomic
      count[k+0] += ri;
//      #pragma omp atomic
      count[k+1] += gi;
//      #pragma omp atomic
      count[k+2] += bi;
/*
      // unlikely race condition: very many increments in between
      NN old_count, new_count;
      #pragma omp atomic read
      old_count = count[k+3];
*/
//      #pragma omp atomic
      count[k+3] += vi;
#endif
/*
      #pragma omp atomic read
      new_count = count[k+3];
      if (new_count < old_count)
      {
        fprintf(stderr, "OVERFLOW\n");
        abort();
      }
*/
    }
#ifdef SPLAT
    }
    }
#endif
  }
  }
  }
}

static bool check_iterates(NN period, C z, C c, R cr)
{
  R r = nearness * zradius;
  cr *= nearness;
  R zr = cr;
  for (NN i = 0; i < period; ++i)
  {
    if (cabs(z - z0) - zr < r)
    {
//#pragma omp critical
//fprintf(stderr, "%d %g %g %g\n", i, cabs(z - z0), zr, r);
      return true;
    }
    zr = zr * (2 * cabs(z) + zr) + cr;
    z = z * z + c;
  }
  return false;
}

static void compute_image(NN frame, Z subsampling) {
  NN total = 0;
  for (Z y = 0; y < zheight; ++y)
    for (Z x = 0; x < zwidth; ++x)
      total += count[((((frame * zheight + y) * zwidth) + x))];// << 2) + 3];
  R s = (zwidth >> subsampling) * (zheight >> subsampling) / (R) total;
  #pragma omp parallel for
  for (Z y = 0; y < zheight >> subsampling; ++y) {
    for (Z x = 0; x < zwidth >> subsampling; ++x) {
      R subcount[4] = {0,0,0,0};
      for (Z dy = 0 ; dy < 1 << subsampling; ++dy) {
        for (Z dx = 0 ; dx < 1 << subsampling; ++dx) {
          Z y0 = (y << subsampling) + dy;
          Z x0 = (x << subsampling) + dx;
          Z k = (((frame * zheight + y0) * zwidth) + x0);// << 2;
          for (Z c = 0; c < 4; ++c) {
            subcount[c] += count[k];// + c];
          }
        }
      }
      if (subcount[3] > 0) {
        R v = sqrt(sqrt(log(1 + log(1 + subcount[3] * s))));
        for (Z c = 0; c < 3; ++c) {
          R u = subcount[c] / subcount[3];
          R g = v * u * 200;
          Z o = fminf(fmaxf(g, 0), 255);
          Z k = (y * (zwidth >> subsampling) + x) * 3 + c;
          pfm[k] = g / 255;
          ppm[k] = o;
        }
      } else {
        Z k = (y * (zwidth >> subsampling) + x) * 3;
        pfm[k + 0] = 0;
        pfm[k + 1] = 0;
        pfm[k + 2] = 0;
        ppm[k + 0] = 0;
        ppm[k + 1] = 0;
        ppm[k + 2] = 0;
      }
    }
  }
}

static void output_ppm(FILE *out, Z subsampling)
{
  ZZ bytes = 3ul * (zwidth >> subsampling) * (zheight >> subsampling);
  fprintf(out, "P6\n%u %u\n255\n", zwidth >> subsampling, zheight >> subsampling);
  fwrite(ppm, bytes, 1, out);
}
/*
static void output_pfm(FILE *out, Z subsampling)
{
  ZZ bytes = sizeof(*pfm) * 3 * (zwidth >> subsampling) * (zheight >> subsampling);
  fprintf(out, "PF\n%u %u\n-1.0\n", zwidth >> subsampling, zheight >> subsampling); // negative means little endian
  fwrite(pfm, bytes, 1, out);
}

static void compute_histogram(Z subsampling) {
  memset(histogram, 0, sizeof(histogram));
  #pragma omp parallel for
  for (Z y = 0; y < zheight >> subsampling; ++y)
  {
    for (Z x = 0; x < zwidth >> subsampling; ++x)
    {
      NN subhit = 0;
      for (Z dy = 0 ; dy < 1 << subsampling; ++dy)
      {
        for (Z dx = 0 ; dx < 1 << subsampling; ++dx)
        {
          Z y0 = (y << subsampling) + dy;
          Z x0 = (x << subsampling) + dx;
          Z k = (y0 * zwidth) + x0;
          subhit += hit[k];
        }
      }
      if (subhit >= HISTOGRAM_DIM)
      {
        subhit = HISTOGRAM_DIM - 1;
      }
      #pragma omp atomic update
      histogram[subhit] += 1;
    }
  }
  histogram1[subsampling] = histogram[1] / ((R) (zheight >> subsampling) * (zwidth >> subsampling));
}

static void output_histogram(FILE *out)
{
  for (N hit = 0; hit < HISTOGRAM_DIM; ++hit)
  {
    if (histogram[hit] > 0)
    {
      fprintf(out, "%u\t%lu\n", hit, histogram[hit]);
    }
  }
}

static void output_histogram1(FILE *out)
{
  for (N subsampling = 0; subsampling < 16; ++subsampling)
  {
    if (histogram1[subsampling] > 0)
    {
      fprintf(out, "%u\t%g\n", subsampling, histogram1[subsampling]);
    }
  }
}
*/
static inline R interior_distance(C *w, C c, NN period, R pixel_size, C *dzp) {
  if (wucleus(w, c, period)) {
    C z = *w;
    C dz = 1.0;
    C dzdz = 0.0;
    C dc = 0.0;
    C dcdz = 0.0;
    for (NN j = 0; j < period; ++j) {
      dcdz = 2.0 * (z * dcdz + dz * dc);
      dc = 2.0 * z * dc + 1.0;
      dzdz = 2.0 * (dz * dz + z * dzdz);
      dz = 2.0 * z * dz;
      z = z * z + c;
    }
    *dzp = dz;
    return (1.0 - cabs2(dz)) / (cabs(dcdz + dzdz * dc / (1.0 - dz)) * pixel_size);
  }
  return -1.0;
}

/*

static void render_recursive_unknown(C c, R grid_spacing, N depth, NN *seed);

static void render_recursive_interior(NN period, C z, C c, R grid_spacing, N depth, NN *seed) {
  if (depth == 0)
  {
    // jitter
    NN u = (*seed = xorshift64(*seed));
    NN v = (*seed = xorshift64(*seed));
    R x = ((u - 1.0) / 0xFFFFffffFFFFfffeul - 0.5) * 2 * grid_spacing;
    R y = ((v - 1.0) / 0xFFFFffffFFFFfffeul - 0.5) * 2 * grid_spacing;
    c += x + I * y;
  }
  C z1 = z, dz = 0;
  R de = interior_distance(&z1, c, period, grid_spacing, &dz);
  if (depth == 0)
  {
    if (de > 0)
    {
      plot_iterates(z1, c, period, dz);
    }
    else
    {
      // should never happen
      NN affected = 1ul << (depth << 1);
//fprintf(stderr, "\n%d %d %g %g+i%g %g+i%g\n", period, affected, de, creal(c), cimag(c), creal(z1), cimag(z1));
      #pragma omp atomic
      potential_errors += affected;
    }
    return;
  }
  if (de > threshold)
  {
    if (check_iterates(period, z1, c, grid_spacing))
    {
      render_recursive_interior(period, z1, c + (-1-I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
      render_recursive_interior(period, z1, c + (-1+I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
      render_recursive_interior(period, z1, c + (+1-I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
      render_recursive_interior(period, z1, c + (+1+I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
    }
    else
    {
      NN culled = 1ul << (depth << 1);
      #pragma omp atomic
      culled_orbits += culled;
      culled *= period;
      #pragma omp atomic
      culled_period += culled;
    }
  }
  else
  {
    // should never happen
    NN affected = 1ul << (depth << 1);
//fprintf(stderr, "\n%d %d %g %g+i%g %g+i%g\n", period, affected, de, creal(c), cimag(c), creal(z1), cimag(z1));
    #pragma omp atomic
    potential_errors += affected;
    render_recursive_unknown(c + (-1-I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
    render_recursive_unknown(c + (-1+I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
    render_recursive_unknown(c + (+1-I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
    render_recursive_unknown(c + (+1+I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
  }
}

static void render_recursive_unknown(C c, R grid_spacing, N depth, NN *seed) {
  if (depth == 0)
  {
    // jitter
    NN u = (*seed = xorshift64(*seed));
    NN v = (*seed = xorshift64(*seed));
    R x = ((u - 1.0) / 0xFFFFffffFFFFfffeul - 0.5) * 2 * grid_spacing;
    R y = ((v - 1.0) / 0xFFFFffffFFFFfffeul - 0.5) * 2 * grid_spacing;
    c += x + I * y;
  }
  R r = nearness * zradius;
  R cr = nearness * grid_spacing;
  R zr = cr;
  C z = 0;
  C dz = 0;
  bool nearby = cabs(z - z0) - zr < r;
  R mz2 = 1.0/0.0;
  for (NN i = 1; i < maxiters; ++i) {
    zr = zr * (2 * cabs(z) + zr) + cr;
    dz = 2 * z * dz + 1;
    z = z * z + c;
    nearby |= cabs(z - z0) - zr < r;
    R z2 = cabs2(z);
    if (! (z2 < 65536)) {
      if (depth == 0)
      {
        return;
      }
      R de = sqrt(z2) * log(z2) / (cabs(dz) * grid_spacing);
      if (de < threshold)
      {
        // near exterior, subdivisions may be interior
        if (nearby)
        {
          render_recursive_unknown(c + (-1-I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
          render_recursive_unknown(c + (-1+I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
          render_recursive_unknown(c + (+1-I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
          render_recursive_unknown(c + (+1+I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
        }
        else
        {
          NN culled = 1ul << (depth << 1);
          #pragma omp atomic
          culled_orbits += culled;
          culled *= i;
          #pragma omp atomic
          culled_period += culled;
        }
      }
      else
      {
        // far exterior, subdivisions guaranteed exterior
      }
      return;
    }
    if (z2 < mz2) {
      mz2 = z2;
      C z1 = z, dzp;
      R de = interior_distance(&z1, c, i, grid_spacing, &dzp);
      if (de > 0) {
        if (depth == 0)
        {
          plot_iterates(z1, c, i, dzp);
          return;
        }
        if (de > threshold)
        {
          // far interior, subdivisions guaranteed interior
          if (check_iterates(i, z1, c, grid_spacing))
          {
            render_recursive_interior(i, z1, c + (-1-I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
            render_recursive_interior(i, z1, c + (-1+I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
            render_recursive_interior(i, z1, c + (+1-I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
            render_recursive_interior(i, z1, c + (+1+I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
          }
          else
          {
            NN culled = 1ul << (depth << 1);
            #pragma omp atomic
            culled_orbits += culled;
            culled *= i;
            #pragma omp atomic
            culled_period += culled;
          }
        }
        else
        {
          // near interior, subdvisions may be exterior
          if (check_iterates(i, z1, c, grid_spacing))
          {
            render_recursive_unknown(c + (-1-I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
            render_recursive_unknown(c + (-1+I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
            render_recursive_unknown(c + (+1-I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
            render_recursive_unknown(c + (+1+I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
          }
          else
          {
            NN culled = 1ul << (depth << 1);
            #pragma omp atomic
            culled_orbits += culled;
            culled *= i;
            #pragma omp atomic
            culled_period += culled;
          }
        }
        return;
      }
    }
  }
  if (depth == 0)
  {
    // should hopefully be rare
    NN affected = 1;
    #pragma omp atomic
    potential_errors += affected;
    return;
  }
  // maximum iteration count reached without any decision
  render_recursive_unknown(c + (-1-I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
  render_recursive_unknown(c + (-1+I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
  render_recursive_unknown(c + (+1-I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
  render_recursive_unknown(c + (+1+I) * 0.5 * grid_spacing, 0.5 * grid_spacing, depth - 1, seed);
}

struct scanline
{
  N y;
  N key;
};

static int cmp_N(N a, N b)
{
  return (a > b) - (b > a);
}

static int cmp_scanline_key(const void *a, const void *b)
{
  const struct scanline *x = a;
  const struct scanline *y = b;
  return cmp_N(x->key, y->key);
}

static struct scanline order[GRID_DIM];

static void render(Z quality) {
  maxiters = 1ul << (16 + quality);
  if (maxiters <= 0)
  {
    exit(1);
  }
  R grid_spacing = 5.0 / GRID_DIM;
  Z progress = 0;
  #pragma omp parallel for schedule(dynamic, 1)
  for (N y = 0; y < GRID_DIM; ++y) {
    for (N x = 0; x < GRID_DIM; ++x) {
      C c = grid_spacing * ((x + 0.5 - GRID_DIM/2.0) + I * (order[y].y + 0.5 - GRID_DIM/2.0));
      //if (cimag(c) >= 0)
      {
        render_recursive_unknown(c, grid_spacing, quality, &prng[y][x]);
      }
    }
    #pragma omp critical
    fprintf(stderr, "%4d/%4d\r", ++progress, GRID_DIM);
  }
  fprintf(stderr, "         \r");
}

void save_checkpoint(void)
{
  FILE *out = fopen("anti.checkpoint", "wb");
  fwrite(count, sizeof(*count) * zwidth * zheight * 4, 1, out);
  fwrite(hit, sizeof(*hit) * zwidth * zheight, 1, out);
  fwrite(prng, sizeof(prng), 1, out);
  fwrite(&potential_errors, sizeof(potential_errors), 1, out);
  fwrite(&total_orbits, sizeof(total_orbits), 1, out);
  fwrite(&total_period, sizeof(total_period), 1, out);
  fwrite(&quality, sizeof(quality), 1, out);
  fclose(out);
}

int load_checkpoint(void)
{
  FILE *in = fopen("anti.checkpoint", "rb");
  if (! in) return 1;
  if (1 != fread(count, sizeof(*count) * zwidth * zheight * 4, 1, in)) return 1;
  if (1 != fread(hit, sizeof(*hit) * zwidth * zheight, 1, in)) return 1;
  if (1 != fread(prng, sizeof(prng), 1, in)) return 1;
  if (1 != fread(&potential_errors, sizeof(potential_errors), 1, in)) return 1;
  if (1 != fread(&total_orbits, sizeof(total_orbits), 1, in)) return 1;
  if (1 != fread(&total_period, sizeof(total_period), 1, in)) return 1;
  if (1 != fread(&quality, sizeof(quality), 1, in)) return 1;
  fclose(in);
  return 0;
}

*/

/*
void save_checkpoint(void)
{
  FILE *out = fopen("anti.checkpoint", "wb");
  fwrite(count, sizeof(*count) * zwidth * zheight * 4, 1, out);
  fwrite(hit, sizeof(*hit) * zwidth * zheight, 1, out);
  fwrite(&level, sizeof(level), 1, out);
  fwrite(&potential_errors, sizeof(potential_errors), 1, out);
  fwrite(&samples, sizeof(samples), 1, out);
  fwrite(&plotted, sizeof(plotted), 1, out);
  fclose(out);
}

bool load_checkpoint(void)
{
  FILE *in = fopen("anti.checkpoint", "rb");
  if (in)
  {
    if (1 == fread(count, sizeof(*count) * zwidth * zheight * 4, 1, in))
    {
      if (1 == fread(hit, sizeof(*hit) * zwidth * zheight, 1, in))
      {
        if (1 == fread(&level, sizeof(level), 1, in))
        {
          if (1 == fread(&potential_errors, sizeof(potential_errors), 1, in))
          {
            if (1 == fread(&samples, sizeof(samples), 1, in))
            {
              if (1 == fread(&plotted, sizeof(plotted), 1, in))
              {
                fclose(in);
                return true;
              }
            }
          }
        }
      }
    }
    fclose(in);
  }
  return false;
}
*/
// FIXME partition boxes by type to save memory

// 16 bytes, could be reduced?
struct box_unknown
{
  NN j, i; // level implicit in source_level
};
NN source_level;

// 48 bytes
struct box_interior
{
  NN j, i, level;
  NN period, previous_period;
  C z;
};

// unknown stored at the tail end of interior, use negative index
// only alloc/free interior
static NN source_unknown_count, source_interior_count, source_allocated;
struct box_unknown *source_unknown; // [source_unknown_count]
struct box_interior *source_interior; // [source_interior_count]
static NN dest_unknown_count, dest_interior_count, dest_allocated;
struct box_unknown *dest_unknown; // [source_unknown_count << 2]
struct box_interior *dest_interior; // [source_interior_count + (source_unknown_count << 2)]

bool box_load(void)
{
  dest_interior = 0;
  dest_unknown = 0;
  dest_interior_count = 0;
  dest_unknown_count = 0;
  FILE *in = fopen("anti.boxes", "rb");
  if (in)
  {
    if (1 == fread(&maxiters, sizeof(maxiters), 1, in))
    {
      if (1 == fread(&source_level, sizeof(source_level), 1, in))
      {
        if (1 == fread(&source_interior_count, sizeof(source_interior_count), 1, in))
        {
          if (1 == fread(&source_unknown_count, sizeof(source_unknown_count), 1, in))
          {
            NN bytes = sizeof(*source_interior) * source_interior_count + sizeof(*source_unknown) * source_unknown_count;
            if ((source_interior = malloc(bytes)))
            {
              memory_allocated += bytes;
              source_allocated = bytes;
              if (1 == fread(source_interior, bytes, 1, in))
              {
                source_unknown = (struct box_unknown *) &source_interior[source_interior_count];
                fclose(in);
                return true;
              }
              memory_allocated -= source_allocated;
              free(source_interior);
              source_allocated = 0;
              source_interior = 0;
            }
            source_unknown_count = 0;
          }
          source_interior_count = 0;
        }
        source_level = 0;
      }
      maxiters = 0;
    }
    fclose(in);
  }
  return false;
}

bool box_save(void)
{
  FILE *out = fopen("anti.boxes", "wb");
  if (out)
  {
    if (1 == fwrite(&maxiters, sizeof(maxiters), 1, out))
    {
      if (1 == fwrite(&source_level, sizeof(source_level), 1, out))
      {
        if (1 == fwrite(&source_interior_count, sizeof(source_interior_count), 1, out))
        {
          if (1 == fwrite(&source_unknown_count, sizeof(source_unknown_count), 1, out))
          {
            if (1 == fwrite(source_interior, sizeof(*source_interior) * source_interior_count, 1, out))
            {
              if (1 == fwrite(source_unknown, sizeof(*source_unknown) * source_unknown_count, 1, out))
              {
                fclose(out);
                return true;
              }
            }
          }
        }
      }
    }
    fclose(out);
  }
  return false;
}

void box_init(void)
{
  maxiters = 1 << 8;
  source_interior_count = 0;
  source_unknown_count = 1;
  dest_interior_count = 0;
  dest_unknown_count = 0;
  NN bytes = sizeof(*source_interior) * source_interior_count + sizeof(*source_unknown) * source_unknown_count;
  source_interior = malloc(bytes);
  source_unknown = (struct box_unknown *) &source_interior[source_interior_count];
  memory_allocated += bytes;
  source_allocated = bytes;
  bytes = sizeof(*source_interior) * (source_interior_count + (source_unknown_count << 2));
  dest_interior = malloc(bytes);
  dest_unknown = (struct box_unknown *) (((char *) dest_interior) + bytes);
  memory_allocated += bytes;
  dest_allocated = bytes;
  source_level = 0;
  source_unknown[0] = (struct box_unknown){ 0, 0 };
}

void box_output_interior(struct box_interior b)
{
  NN me;
  #pragma omp atomic capture
  me = dest_interior_count++;
  dest_interior[me] = b;
}

void box_output_unknown(struct box_unknown b)
{
  NN me;
  #pragma omp atomic capture
  me = dest_unknown_count++;
  dest_unknown[-1-me] = b;
}

bool box_finish(void)
{
  memory_allocated -= source_allocated;
  source_unknown_count = dest_unknown_count;
  source_interior_count = dest_interior_count;
  free(source_interior);
  source_allocated = dest_allocated;
  source_interior = dest_interior;
  source_unknown = &dest_unknown[-dest_unknown_count];
  dest_unknown_count = 0;
  dest_interior_count = 0;
  NN bytes = sizeof(*dest_interior) * (source_interior_count + (source_unknown_count << 2));
  if (memory_allocated + bytes <= memory_limit)
  {
    dest_interior = malloc(bytes);
    dest_unknown = (struct box_unknown *) (((char *) dest_interior) + bytes);
    memory_allocated += bytes;
    dest_allocated = bytes;
  }
  else
  {
    dest_interior = 0;
    dest_unknown = 0;
    dest_allocated = 0;
  }
  return dest_interior;
}

void box_interior_split(struct box_interior b, struct box_interior bs[4])
{
  bs[0] = (struct box_interior){ (b.j << 1) + 0, (b.i << 1) + 0, b.level + 1, b.period, b.previous_period, b.z };
  bs[1] = (struct box_interior){ (b.j << 1) + 0, (b.i << 1) + 1, b.level + 1, b.period, b.previous_period, b.z };
  bs[2] = (struct box_interior){ (b.j << 1) + 1, (b.i << 1) + 0, b.level + 1, b.period, b.previous_period, b.z };
  bs[3] = (struct box_interior){ (b.j << 1) + 1, (b.i << 1) + 1, b.level + 1, b.period, b.previous_period, b.z };
}

void box_unknown_split(struct box_unknown b, struct box_unknown bs[4])
{
  bs[0] = (struct box_unknown){ (b.j << 1) + 0, (b.i << 1) + 0 };
  bs[1] = (struct box_unknown){ (b.j << 1) + 0, (b.i << 1) + 1 };
  bs[2] = (struct box_unknown){ (b.j << 1) + 1, (b.i << 1) + 0 };
  bs[3] = (struct box_unknown){ (b.j << 1) + 1, (b.i << 1) + 1 };
}

C box_interior_coords(struct box_interior b)
{
  R x = (b.i + 0.5 - (1ul << (b.level - 1))) * cradius / (1ul << (b.level - 1));
  R y = (b.j + 0.5 - (1ul << (b.level - 1))) * cradius / (1ul << (b.level - 1));
  return x + I * y;
}

C box_unknown_coords(struct box_unknown b, NN level)
{
  R x = (b.i + 0.5 - (1ul << (level - 1))) * cradius / (1ul << (level - 1));
  R y = (b.j + 0.5 - (1ul << (level - 1))) * cradius / (1ul << (level - 1));
  return x + I * y;
}

void box_unknown_filter(struct box_unknown b, NN level)
{
  R grid_spacing = cradius / (1ul << level);
  C c = box_unknown_coords(b, level);
  C cr = grid_spacing * nearness;
  C z = 0;
  R zr = cr;
  C dz = 0;
  R r = zradius * nearness;
  bool nearby = cabs(z - z0) - zr < r;
  R mz2 = 1.0/0.0;
  NN previous_period = 0;
  for (NN i = 1; i < maxiters; ++i)
  {
    zr = zr * (2 * cabs(z) + zr) + cr;
    dz = 2 * z * dz + 1;
    z = z * z + c;
    nearby |= cabs(z - z0) - zr < r;
    R z2 = cabs2(z);
    if (! (z2 < 65536))
    {
      R de = sqrt(z2) * log(z2) / (cabs(dz) * grid_spacing);
      if (de < threshold)
      {
        // near exterior, subdivisions may be interior
        if (nearby)
        {
          box_output_unknown(b);
        }
        else
        {
          // culled due to not near view
          #pragma omp atomic
          culled++;
        }
      }
      else
      {
        // culled due to far exterior
        #pragma omp atomic
        culled++;
      }
      return;
    }
    if (z2 < mz2)
    {
      C z1 = z, dzp;
      R de = interior_distance(&z1, c, i, grid_spacing, &dzp);
      if (de > 0)
      {
        if (de > threshold)
        {
          if (true)//de * grid_spacing < threshold * absolute_de_threshold)
          {
            // far interior, subdivisions guaranteed interior
            if (check_iterates(i, z1, c, grid_spacing))
            {
              box_output_interior((struct box_interior){ b.j, b.i, level, i, previous_period, z1});
            }
            else
            {
              // culled due to not near view
              #pragma omp atomic
              culled++;
            }
          }
          else
          {
            // culled due to not near boundary
            #pragma omp atomic
            culled++;
          }
        }
        else
        {
          if (true)//de * grid_spacing < threshold * absolute_de_threshold)
          {
            // near interior, subdvisions may be exterior
            if (check_iterates(i, z1, c, grid_spacing))
            {
              box_output_unknown(b);
            }
            else
            {
              // culled due to not near view
              #pragma omp atomic
              culled++;
            }
          }
          else
          {
            // culled due to not near boundary
            #pragma omp atomic
            culled++;
          }
        }
        return;
      }
      mz2 = z2;
      previous_period = i;
    }
  }
  // maximum iteration count reached without any decision
  box_output_unknown(b);
}

/*
void box_interior_filter(struct box_interior b)
{
  // far interior
  C dz;
  R de = interior_distance(&b.z, c, b.period, grid_spacing, &dz);
  if (de > threshold)
  {
    if (true)//de * grid_spacing < threshold * absolute_de_threshold)
    {
      if (check_iterates(b.period, b.z, c, cr))
      {
        box_output_interior(b);
      }
      else
      {
        // culled due to not near view
        #pragma omp atomic
        culled++;
      }
    }
    else
    {
      // culled due to not near boundary
      #pragma omp atomic
      culled++;
    }
  }
  else
  {
    // should never happen
    abort();
    #pragma omp atomic
    potential_errors++;
    b.period = 0;
    box_output_unknown((struct box_unknown){ b.j, b.i });
  }
}
*/

bool box_process(void)
{
  maxiters <<= 1;
  memcpy(dest_interior, source_interior, sizeof(*source_interior) * source_interior_count);
  dest_interior_count = source_interior_count;
  NN dest_level = source_level + 1;
  NN progress = 0;
  #pragma omp parallel for
  for (NN s = 0; s < source_unknown_count; ++s)
  {
    struct box_unknown b = source_unknown[s];
    struct box_unknown bs[4];
    box_unknown_split(b, bs);
    for (N i = 0; i < 4; ++i)
    {
      box_unknown_filter(bs[i], dest_level);
    }
    NN me;
    #pragma omp atomic capture
    me = ++progress;
    if (me * 100 / source_unknown_count != (me - 1) * 100 / source_unknown_count)
    {
      #pragma omp critical
      fprintf(stderr, "level %lu progress %3d%%\r", dest_level, (int) (me * 100 / source_unknown_count));
    }
  }
  source_level = dest_level;
  return box_finish();
}

N hash(N a)
{
  a = (a+0x7ed55d16) + (a<<12);
  a = (a^0xc761c23c) ^ (a>>19);
  a = (a+0x165667b1) + (a<<5);
  a = (a+0xd3a2646c) ^ (a<<9);
  a = (a+0xfd7046c5) + (a<<3);
  a = (a^0xb55a4f09) ^ (a>>16);
  return a;
}

N hashes(NN j, NN i, N l, N ss, N s)
{
  N h = 0;
  h = hash(h ^ hash(j >> 32));
  h = hash(h ^ hash(j));
  h = hash(h ^ hash(i >> 32));
  h = hash(h ^ hash(i));
  h = hash(h ^ hash(l));
  h = hash(h ^ hash(ss));
  h = hash(h ^ hash(s));
  return h;
}

C jitter(R grid_spacing, N seed)
{
  NN x = ((NN) hash(seed + hash(1)) << 32) + hash(seed + hash(2));
  NN y = ((NN) hash(seed + hash(3)) << 32) + hash(seed + hash(4));
  R u = (x / (R) 0xFFFFffffFFFFfffful - 0.5) * 2 * grid_spacing;
  R v = (y / (R) 0xFFFFffffFFFFfffful - 0.5) * 2 * grid_spacing;
  return u + I * v;
}

void box_sample(NN *total_samples, NN *total_plotted)
{
  maxiters <<= 1;
  R grid_spacing = cradius / (1ul << level);
  NN s_shared = 0;
  #pragma omp parallel for schedule(static, 1)
  for (NN t = 0; t < threads; ++t)
  {
    while (true)
    {
      NN s;
      #pragma omp atomic capture
      s = s_shared++;
      if (s >= source_interior_count)
      {
        break;
      }
      if (s * 100 / source_interior_count != (s - 1) * 100 / source_interior_count)
      {
        #pragma omp critical
        fprintf(stderr, "level %lu interior %3d%%\r", level, (int) (s * 100 / source_interior_count));
      }
  
    struct box_interior b0 = source_interior[s];
    Z dlevel = level - b0.level;
    if (dlevel < 0)
    {
      fprintf(stderr, "%lu %lu\n", level, b0.level);
    }
    assert(dlevel >= 0);
#ifdef SPLAT
    dlevel = 2;
#endif
    NN bsize = 1ul << dlevel;
    NN samples = bsize * bsize;
    NN plotted = 0;
    R grid_spacing1 = cradius / (1ull << b0.level);
    for (NN dj = 0; dj < bsize; ++dj)
    {
      for (NN di = 0; di < bsize; ++di)
      {
        struct box_interior b = { (b0.j << dlevel) + dj, (b0.i << dlevel) + di, b0.level + dlevel, b0.period, b0.previous_period, b0.z };
        C c0 = box_interior_coords(b);
        C c = c0 + jitter(grid_spacing1, hashes(b.j, b.i, b.level, 1, 0));
        // far interior
        C z = b.z, dz;
        R de = interior_distance(&z, c, b.period, grid_spacing1, &dz);
        if (de > 0)
        {
          if (de * grid_spacing1 < absolute_de_threshold)
          {
            ++plotted;
            plot_iterates(z, c, b.period, b.previous_period, dz, de * grid_spacing1, b.level);
          }
        }
        else
        {
          #pragma omp atomic
          potential_errors++;
        }
      }
    }
    #pragma omp atomic
    *total_samples += samples;
    #pragma omp atomic
    *total_plotted += plotted;
    }
  }
  s_shared = 0;
  #pragma omp parallel for schedule(static, 1)
  for (NN t = 0; t < threads; ++t)
  {
    while (true)
    {
      NN s;
      #pragma omp atomic capture
      s = s_shared++;
      if (s >= source_unknown_count)
      {
        break;
      }
      if (s * 100 / source_unknown_count != (s - 1) * 100 / source_unknown_count)
      {
        #pragma omp critical
        fprintf(stderr, "level %lu unknown %3d%%\r", level, (int) (s * 100 / source_unknown_count));
      }
  
    struct box_unknown b0 = source_unknown[s];
    Z dlevel = level - source_level;
    assert(dlevel >= 0);
#ifdef SPLAT
    dlevel = 2;
#endif
    NN bsize = 1ul << dlevel;
    NN samples = bsize * bsize;
    NN plotted = 0;
    R grid_spacing1 = cradius / (1ull << source_level);
    for (NN dj = 0; dj < bsize; ++dj)
    {
      for (NN di = 0; di < bsize; ++di)
      {
        struct box_unknown b = { (b0.j << dlevel) + dj, (b0.i << dlevel) + di };
        C c0 = box_unknown_coords(b, level);
        C c = c0 + jitter(grid_spacing1, hashes(b.j, b.i, source_level, 1, 0));
        R mz2 = 1.0/0.0;
        C z = 0;
        NN previous_period = 0;
        NN i;
        for (i = 1; i < maxiters; ++i)
        {
          z = z * z + c;
          R z2 = cabs2(z);
          if (! (z2 < 65536))
          {
            // escaped
            break;
          }
          if (z2 < mz2)
          {
            C z1 = z, dz;
            R de = interior_distance(&z1, c, i, grid_spacing1, &dz);
            if (de > 0)
            {
              // interior
              if (de * grid_spacing1 < absolute_de_threshold)
              {
                ++plotted;
                plot_iterates(z1, c, i, previous_period, dz, de * grid_spacing1, level);
              }
              break;
            }
            mz2 = z2;
            previous_period = i;
          }
        }
        if (i >= maxiters)
        {
          #pragma omp atomic
          potential_errors++;
        }
      }
    }
    #pragma omp atomic
    *total_samples += samples;
    #pragma omp atomic
    *total_plotted += plotted;
    }
  }
}

FILE *open_output(NN seed)
{
  char command[1024];
  snprintf(command, sizeof(command), "ffmpeg -r %d -framerate %d -i - -i audio.caf -shortest -b:a 384k -pix_fmt yuv420p -profile:v high -level:v 4.1 -crf:v 18 -movflags +faststart anti-%ld.mp4", fps, fps, seed);
  return popen(command, "w");
}

int frames_written = 0;
void save_output(FILE* out)
{
  Z size = 0;
  for (size = 0; ! (1 << size > zwidth && 1 << size > zheight); ++size)
    ;
  for (Z subsampling = 0; subsampling <= 0*size; ++subsampling)
  {
    if (zwidth >> subsampling > 0 && zheight >> subsampling > 0)
    {
for (int frame = 0; frame < zframes && frames_written < total_frames; ++frame, ++frames_written)
{
      compute_image(frame, subsampling);
//      char filename[100];
//      snprintf(filename, sizeof(filename), "pnmtopng -interlace -force -compression=9 > anti-%08d-%lu-%d.png", frame, level, size - subsampling);
//      FILE *out = popen(filename, "w");
      output_ppm(out, subsampling);
}
/*
      snprintf(filename, sizeof(filename), "anti-%08d-%lu-%d.pfm", frame, level, size - subsampling);
      out = fopen(filename, "wb");
      output_pfm(out, subsampling);
      fclose(out);
      compute_histogram(subsampling);
      snprintf(filename, sizeof(filename), "anti-%08d-%lu-%d.dat", frame, level, size - subsampling);
      out = fopen(filename, "wb");
      output_histogram(out);
      fclose(out);
*/
    }
  }
/*
  char filename[100];
  snprintf(filename, sizeof(filename), "anti--%08d-%lu.dat", frame, level);
  FILE *out = fopen(filename, "wb");
  output_histogram1(out);
  fclose(out);
*/
}

volatile int running = 1;
void signal_handler(int sig)
{
  (void) sig;
  running = 0;
}

void box_render(void)
{
  printf("preprocessing\n");
  fflush(stdout);
  if (! box_load())
  {
    box_init();
    while (running && source_level < 14 && box_process())
    {
      printf("level %lu interior %lu unknown %lu errors %lu\n", source_level, source_interior_count, source_unknown_count, potential_errors);
      fflush(stdout);
    }
    box_save();
  }
  else
  {
    printf("preprocessed boxes loaded\n");
    fflush(stdout);
  }
  if (! running)
  {
    return;
  }
  printf("level %lu interior %lu unknown %lu errors %lu\n", source_level, source_interior_count, source_unknown_count, potential_errors);
  fflush(stdout);
  level = source_level;
#if 0
  if (! load_checkpoint())
  {
    initialize();
    printf("sampling initialized\n");
    fflush(stdout);
  }
  else
  {
    printf("sampling checkpoint loaded\n");
    fflush(stdout);
  }
  while (running)
  {
#else

  NN seed = time(0);
  printf("seed = %ld\n", seed);
  srand(seed);
  M identity = {{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}}};
  M mm = identity, mm1 = identity;

  FILE *out = open_output(seed);
  NN maxiters0 = maxiters;
  box_sample(&samples, &plotted);
  printf("level %lu samples %lu plotted %lu points %ld errors %lu\n", level, samples, plotted, num_points, potential_errors);
  fflush(stdout);
  maxiters = maxiters0;

  FILE *points_file = fopen("position.f4", "wb");
  fwrite(points, sizeof(*points) * num_points, 1, points_file);
  fclose(points_file);
  FILE *colour_file = fopen("colour.f1", "wb");
  fwrite(colour, sizeof(*colour) * num_points, 1, colour_file);
  fclose(colour_file);
  exit(0);

  M m0 = {{
    {0,0,0,1},
    {0,0,1,0},
    {0,1,0,0},
    {1,0,0,0}
  }};
 M m01 = identity;
/*
  for (int u = 0; u < 4; ++u)
  {
    for (int v = u + 1; v < 4; ++v)
    {
      M uv = identity;
      R t = 2 * M_PI * (rand() / (R) RAND_MAX);
      R c = cos(t);
      R s = sin(t);
      uv.x[u][u] = c;
      uv.x[u][v] = -s;
      uv.x[v][u] = s;
      uv.x[v][v] = c;
      m0 = mmulm(m0, uv);
    }
  }
*/
  M m = m0, minc = identity, mconj, m1 = m01, mconj1;

  int f = 0;
  R beat = 0;
  R increment = bpm / 60 / fps / beats;
  for (int batch = 0; running && frames_written < total_frames; ++batch)
{
  for (int frame = 0; frame < zframes; ++frame)
  {
    if (floor(beat) != floor(beat - increment))
    {
      m = m0;
      mconj = identity;
      for (int u = 0; u < 4; ++u)
      {
        for (int v = u + 1; v < 4; ++v)
        {
          M uv = identity;
          R t = 2 * M_PI * (rand() / (R) RAND_MAX);
          R c = cos(t);
          R s = sin(t);
          uv.x[u][u] = c;
          uv.x[u][v] = -s;
          uv.x[v][u] = s;
          uv.x[v][v] = c;
          mconj = mmulm(mconj, uv);
        }
      }
      mconj1 = identity;
      for (int u = 0; u < 4; ++u)
      {
        for (int v = u + 1; v < 4; ++v)
        {
          M uv = identity;
          R t = 2 * M_PI * (rand() / (R) RAND_MAX);
          R c = cos(t);
          R s = sin(t);
          uv.x[u][u] = c;
          uv.x[u][v] = -s;
          uv.x[v][u] = s;
          uv.x[v][v] = c;
          mconj1 = mmulm(mconj1, uv);
        }
      }
    }

    R phase = beat - floor(beat);
    R t = 2 * M_PI * phase;
    R c = cos(t);
    R s = sin(t);
    minc = (M){
      { {c, -s, 0, 0}
      , {s,  c, 0, 0}
      , {0,  0, c, -s}
      , {0,  0, s,  c}
      }};

    animation_time[frame] = beat;
    beat += increment;
    m = mmulm(transpose(mconj), mmulm(minc, mmulm(mconj, m0)));
    m1 = mmulm(minc, mconj1);
    transformation_matrix[frame] = m;// mmulm(m, m1);///*mmulm((M){{{cos(t),-sin(t),0,0},{sin(t),cos(t),0,0},{0,0,1,0},{0,0,0,1}}}, */mmulm(mmulm(m, (M){{{cos(t),0,-sin(t),0},{0,cos(t),0,-sin(t)},{sin(t),0,cos(t),0},{0,sin(t),0,cos(t)}}}), m1);//);
    transformation_matrix1[frame] = m1;// mmulm(m, m1);///*mmulm((M){{{cos(t),-sin(t),0,0},{sin(t),cos(t),0,0},{0,0,1,0},{0,0,0,1}}}, */mmulm(mmulm(m, (M){{{cos(t),0,-sin(t),0},{0,cos(t),0,-sin(t)},{sin(t),0,cos(t),0},{0,sin(t),0,cos(t)}}}), m1);//);
    ++f;
  }
  initialize();
  plot_points();
  save_output(out);
  }
  pclose(out);
#endif
/*
    level++;
    save_checkpoint();
  }
*/
}

extern int main(int argc, char **argv)
{
  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);
  // load logo
  FILE *f = fopen("logo.pgm", "rb");
  fseek(f, -16 * 16, SEEK_END);
  fread(logo, 16 * 16, 1, f);
  fclose(f);
  // parse arguments
  z0 = 0;
  zradius = 2;
  zwidth = 1080;// / 4;
  zheight = 1080;// / 4;
  if (argc > 3)
  {
    z0 = atof(argv[1]) + I * atof(argv[2]);
    zradius = atof(argv[3]);
  }
  if (argc > 5)
  {
    zwidth = atoi(argv[4]);
    zheight = atoi(argv[5]);
  }
  zpixel_size = zradius * 2 / zheight;
  R fudge_factor = sqrt(2);
  nearness = fudge_factor * hypot(zwidth, zheight) / fmin(zwidth, zheight);
#if 0
  // deterministic randomization
  NN seed = 0x1cedcafec01dc01aul;
  for (N y = 0; y < GRID_DIM; ++y)
  {
    order[y].y = y;
    order[y].key = (seed = xorshift64(seed));
  }
  qsort(order, GRID_DIM, sizeof(*order), cmp_scanline_key);
  allocate();
  if (load_checkpoint())
  {
    initialize(seed);
  }
  ++quality;
  for (; running; ++quality)
  {
    fprintf(stderr, "QUALITY: %d\n", quality);
    render(quality);
    fprintf(stderr, "total %lu/%lu culled %lu/%lu error %lu\n", total_orbits, total_period, culled_orbits, culled_period, potential_errors);
    save_output(quality);
    if (quality > 4)
    {
      save_checkpoint();
    }
  }
#else
  allocate();
  box_render();
#endif
  deallocate();
  return 0;
}
