// Ultimate Anti-Buddhagram (c) 2019 Claude Heiland-Allen
// ./UltimateAntiBuddhagram | ffmpeg -i - video.mp4

#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#ifndef MAP_HUGE_2MB
#define MAP_HUGE_2MB (21 << MAP_HUGE_SHIFT)
#endif
#ifndef MAP_HUGE_1GB
#define MAP_HUGE_1GB (30 << MAP_HUGE_SHIFT)
#endif

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "geometry.h"

static inline int sgn(double x) { return (x > 0) - (0 > x); }

static void *malloc_huge(size_t *size)
{
  // round up to 1GB
  size_t v = *size;
  v += (1 << 30) - 1;
  v >>= 30;
  v <<= 30;
  *size = v;
  size_t bytes = v;
  void *memory = mmap(NULL, bytes, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_HUGETLB | MAP_HUGE_1GB | MAP_ANONYMOUS, -1, 0);
  if (memory == MAP_FAILED)
  {
    fprintf(stderr, "could not mmap %lu with 1GB pages: %d\n", bytes, errno);
    memory = mmap(NULL, bytes, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_HUGETLB | MAP_HUGE_2MB | MAP_ANONYMOUS, -1, 0);
    if (memory == MAP_FAILED)
    {
      fprintf(stderr, "could not mmap %lu with 2MB pages: %d\n", bytes, errno);
      memory = mmap(NULL, bytes, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
      if (memory == MAP_FAILED)
      {
        fprintf(stderr, "could not mmap %lu at all: %d\n", bytes, errno);
        return NULL;
      }
      else
      {
        fprintf(stderr, "did mmap %lu with 4kB pages\n", bytes);
      }
    }
    else
    {
      fprintf(stderr, "did mmap %lu with 2MB pages\n", bytes);
    }
  }
  else
  {
    fprintf(stderr, "did mmap %lu with 1GB pages\n", bytes);
  }
  return memory;
}

static void free_huge(void *memory, size_t size)
{
  munmap(memory, size);
}

typedef unsigned int N;
typedef unsigned long long int NN;
typedef int Z;
typedef long long int ZZ;
typedef double R;
typedef double complex C;
typedef float F;
typedef unsigned char B;

typedef struct { R x[5]; } V5; // z, c, 1
static const V5 v0 = { { -0.260365638451579340, 0, -0.368941235857094540, 0, 0 } };
typedef struct { R x[5][5]; } M55;

static inline M55 mul_m55_m55(const M55 *a, const M55 *b) {
  M55 o;
  for (Z i = 0; i < 5; ++i)
    for (Z j = 0; j < 5; ++j)
    {
      o.x[i][j] = 0;
      for (Z k = 0; k < 5; ++k)
        o.x[i][j] += a->x[i][k] * b->x[k][j];
    }
  return o;
}

static inline V5 mul_m55_v5(const M55 *a, const V5 *b) {
  V5 o = { { 0, 0, 0, 0, 0 } };
  for (Z i = 0; i < 5; ++i)
    for (Z j = 0; j < 5; ++j)
      o.x[i] += a->x[i][j] * b->x[j];
  return o;
}

typedef unsigned short int P;

#define DIM 13
typedef struct
{
  // c is implicit from coordinates
  // only one half of the set is stored due to symmetry
  C z[1 << (DIM - 1)][1 << DIM];
  P p[1 << (DIM - 1)][1 << DIM];
} cloud; // 9 GB
static inline C cloud_c(Z i, Z j, Z n)
{
  double x = ((i + 0.5) / (1 << n) - 0.5) * 4;
  double y = ((j + 0.5) / (1 << n) - 0.5) * 4;
  return x + I * y;
}

cloud *cloud_alloc(size_t *bytes)
{
  *bytes = sizeof(cloud);
  return malloc_huge(bytes);
}

void cloud_free(cloud *K, size_t bytes)
{
  free_huge(K, bytes);
}

void cloud_clear(cloud *K)
{
  memset(K, 0, sizeof(*K));
}

typedef struct { F x[4]; } V4;
#define MAXPERIOD (1 << 12)
typedef struct
{
  V4 *cz[MAXPERIOD];
  ZZ n[MAXPERIOD];
  ZZ i[MAXPERIOD];
  size_t bytes;
  size_t bytes_used;
} points;
static points *points_alloc(ZZ total)
{
  points *Q = calloc(1, sizeof(*Q));
  if (! Q)
    return 0;
  Q->bytes = total * sizeof(V4);
  Q->bytes_used = Q->bytes;
  Q->cz[0] = malloc_huge(&Q->bytes);
  if (! Q->cz[0])
  {
    free(Q);
    return 0;
  }
  return Q;
}
static void points_free_data(points *Q)
{
  free_huge(Q->cz[0], Q->bytes);
  memset(Q->cz, 0, sizeof(Q->cz));
  Q->bytes = 0;
}

#define WIDTH (1920)
#define HEIGHT (1080)

#if 0
typedef struct { F a[HEIGHT][WIDTH][4]; } accum;
accum *accum_alloc(void) { return malloc(sizeof(accum)); }
void accum_free(accum *A) { free(A); }
void accum_clear(accum *A) { memset(A, 0, sizeof(*A)); }
static inline void accum_plot(accum *A, R x0, R y0, F r, F g, F b, F a)
{
  R s[2] = { 1 - (x0 - floor(x0)), x0 - floor(x0) };
  R t[2] = { 1 - (y0 - floor(y0)), y0 - floor(y0) };
  if (-1 < x0 && x0 < WIDTH && -1 < y0 && y0 < HEIGHT)
  {
    for (Z j = 0; j < 2; ++j)
    {
      Z y = floor(y0) + j;
      if (0 <= y && y < HEIGHT)
      {
        for(Z i = 0; i < 2; ++i)
        {
          Z x = floor(x0) + i;
          if (0 <= x && x < WIDTH)
          {
            #pragma omp atomic update
            A->a[y][x][0] += s[i] * t[j] * a * r;
            #pragma omp atomic update
            A->a[y][x][1] += s[i] * t[j] * a * g;
            #pragma omp atomic update
            A->a[y][x][2] += s[i] * t[j] * a * b;
            #pragma omp atomic update
            A->a[y][x][3] += s[i] * t[j];
          }
        }
      }
    }
  }
}
#endif

typedef struct { B g[HEIGHT][WIDTH][3]; } image;
image *image_alloc(void) { return malloc(sizeof(image)); }
void image_free(image *G) { free(G); }

#if 0
void image_from_accum(image *G, const accum *A, ZZ total)
{
  R s = WIDTH * HEIGHT / (R) total;
  #pragma omp parallel for
  for (Z y = 0; y < HEIGHT; ++y) {
    for (Z x = 0; x < WIDTH; ++x) {
      F a = A->a[y][x][3];
      if (a) {
        R v = log2(1 + a * s) / a;
        for (Z c = 0; c < 3; ++c) {
          R u = A->a[y][x][c];
          Z o = fminf(fmaxf(255 * v * u, 0), 255);
          G->g[y][x][c] = 255 - o;
        }
      } else {
        for (Z c = 0; c < 3; ++c) {
          G->g[y][x][c] = 255;
        }
      }
    }
  }
}
#endif

void image_write(FILE *f, const image *G)
{
  fprintf(f, "P6\n%d %d\n255\n", WIDTH, HEIGHT);
  fwrite(&G->g[0][0][0], WIDTH * HEIGHT * 3, 1, f);
  fflush(f);
}

static inline R cnorm(C z) { return creal(z) * creal(z) + cimag(z) * cimag(z); }

static void hsv2rgb(F h, F s, F v, F *rp, F *gp, F *bp) {
  F i, f, p, q, t, r, g, b;
  Z ii;
  if (s == 0.0) {
    r = v;
    g = v;
    b = v;
  } else {
    h = h - floor(h);
    h = h * 6.0;
    i = floor(h);
    ii = i;
    f = h - i;
    p = v * (1 - s);
    q = v * (1 - (s * f));
    t = v * (1 - (s * (1 - f)));
    switch(ii) {
      case 0:  r = v; g = t; b = p; break;
      case 1:  r = q; g = v; b = p; break;
      case 2:  r = p; g = v; b = t; break;
      case 3:  r = p; g = q; b = v; break;
      case 4:  r = t; g = p; b = v; break;
      default: r = v; g = p; b = q; break;
    }
  }
  *rp = r;
  *gp = g;
  *bp = b;
}

#if 0
static ZZ accum_from_cloud(accum *A, const cloud *K, const M55 *T1, const M55 * T2)
{
  ZZ t = 0;
  #pragma omp parallel for schedule(dynamic, 1) reduction(+:t)
  for (Z j = 0; j < 1 << (DIM - 1); ++j)
  {
    for (Z i = 0; i < 1 << DIM; ++i)
    {
      P p = K->p[j][i];
      R phi5 = pow((sqrt(5) + 1) / 2, 5);
      R hue = (p - 1) / phi5;
      F r, g, b;
      hsv2rgb(hue, 0.5, 1, &r, &g, &b);
      for (Z k = 0; k < 2; ++k)
      {
        C c = cloud_c(i, j, DIM);
        C z = K->z[j][i];
        if (k)
        {
          c = conj(c);
          z = conj(z);
        }
        for (P n = 0; n < p; ++n)
        {
          V5 u = { { creal(c), cimag(c), creal(z), cimag(z), 1 } };
          u = mul_m55_v5(T1, &u);
          for (Z d = 0; d < 5; ++d)
          {
            u.x[d] /= u.x[4];
          }
          u = mul_m55_v5(T2, &u);
          for (Z d = 0; d < 5; ++d)
          {
            u.x[d] /= u.x[3];
          }
          C w = u.x[0] + I * u.x[1];
          R depth = (u.x[2] + 1) / 2; // near 0, far 1
          R a = -log(depth);
          if (a > 0 && depth > 0)
          {
            C pz0 = (WIDTH / 2.0 - 0.5) + I * (HEIGHT / 2.0 - 0.5);
            C pz = w * hypot(WIDTH, HEIGHT) / 4.0 + pz0;
            accum_plot(A, creal(pz), cimag(pz), r, g, b, a);
            t = t + 1;
          }
          z = z * z + c;
        }
      }
    }
  }
  return t;
}
#endif

int wucleus(C *z0, C c, N period) {
  R eps = nextafter(16, 17) - 16;
  R er2 = 16;
  C z = *z0;
  for (N j = 0; j < 256; ++j) {
    C dz = 1.0;
    for (N k = 0; k < period; ++k) {
      dz = 2.0 * dz * z;
      z = z * z + c;
    }
    R z2 = cabs(z);
    if (! (z2 < er2)) {
      break;
    }
    z = *z0 - (z - *z0) / (dz - 1.0);
    R e = cabs(z - *z0);
    *z0 = z;
    if (e <= eps) {
      return 1;
    }
  }
  return 0;
}

R interior_distance(C *w, C c, N period, R pixel_size) {
  if (wucleus(w, c, period)) {
    C z = *w;
    C dz = 1.0;
    C dzdz = 0.0;
    C dc = 0.0;
    C dcdz = 0.0;
    for (N j = 0; j < period; ++j) {
      dcdz = 2.0 * (z * dcdz + dz * dc);
      dc = 2.0 * z * dc + 1.0;
      dzdz = 2.0 * (dz * dz + z * dzdz);
      dz = 2.0 * z * dz;
      z = z * z + c;
    }
    return (1.0 - cnorm(dz)) / (cabs(dcdz + dzdz * dc / (1.0 - dz)) * pixel_size);
  }
  return -1.0;
}

R interior_distance_sign(C *w, C c, N period) {
  if (wucleus(w, c, period)) {
    C z = *w;
    C dz = 1.0;
    for (N j = 0; j < period; ++j) {
      dz = 2.0 * z * dz;
      z = z * z + c;
    }
    return 1.0 - cnorm(dz);
  }
  return -1.0;
}



static ZZ cloud_calculate_interior(cloud *K, N depth, Z i, Z j, C z, P p)
{
  C c = cloud_c(i, j, depth);
  wucleus(&z, c, p);
  if (depth == DIM)
  {
    K->z[j][i] = z;
    K->p[j][i] = p;
    return 1;
  }
  ZZ t = 0;
  for (Z jj = 2 * j; jj < 2 * j + 2; ++ jj)
    for (Z ii = 2 * i; ii < 2 * i + 2; ++ ii)
      t += cloud_calculate_interior(K, depth + 1, ii, jj, z, p);
  return t;
}

static ZZ cloud_calculate_unknown(cloud *K, N depth, Z i, Z j)
{
  C c = cloud_c(i, j, depth);
  C z = 0;
  C dz = 0;
  R mz2 = 1.0/0.0;
  ZZ t = 0;
  R grid_spacing = (4.0 / (1 << depth));
  if (depth == DIM)
  {
    for (P p = 1; p < MAXPERIOD; ++p)
    {
      dz = 2 * z * dz + 1;
      z = z * z + c;
      R z2 = cnorm(z);
      if (! (z2 < 65536))
      {
        break;
      }
      if (z2 < mz2)
      {
        mz2 = z2;
        C z1 = z;
        R de = interior_distance(&z1, c, p, grid_spacing);
        if (de > 0)
        {
          K->z[j][i] = z1;
          K->p[j][i] = p;
          t += 1;
          break;
        }
      }
    }
  }
  else
  {
    for (P p = 1; p < MAXPERIOD; ++p)
    {
      dz = 2 * z * dz + 1;
      z = z * z + c;
      R z2 = cnorm(z);
      if (! (z2 < 65536))
      {
        R de = sqrt(z2) * log(z2) / (cabs(dz) * grid_spacing);
        if (de < 4 * sqrt(2))
        {
          for (Z jj = 2 * j; jj < 2 * j + 2; ++ jj)
            for (Z ii = 2 * i; ii < 2 * i + 2; ++ ii)
              t += cloud_calculate_unknown(K, depth + 1, ii, jj);
        }
        break;
      }
      if (z2 < mz2)
      {
        mz2 = z2;
        C z1 = z;
        R de = interior_distance(&z1, c, p, grid_spacing);
        if (de > 0)
        {
          if (de > 0.25 * sqrt(2))
          {
            for (Z jj = 2 * j; jj < 2 * j + 2; ++ jj)
              for (Z ii = 2 * i; ii < 2 * i + 2; ++ ii)
                t += cloud_calculate_interior(K, depth + 1, ii, jj, z1, p);
          }
          else
          {
            for (Z jj = 2 * j; jj < 2 * j + 2; ++ jj)
              for (Z ii = 2 * i; ii < 2 * i + 2; ++ ii)
                t += cloud_calculate_unknown(K, depth + 1, ii, jj);
          }
          break;
        }
      }
    }
  }
  return t;
}

static ZZ cloud_calculate(cloud *K, N depth)
{
  ZZ t = 0;
  #pragma omp parallel for schedule(dynamic, 1) reduction(+:t)
  for (Z j = 0; j < 1 << (depth - 1); ++j)
    for (Z i = 0; i < 1 << depth; ++i)
      t += cloud_calculate_unknown(K, depth, i, j);
  return t;
}

static ZZ cloud_count(cloud *K)
{
  ZZ t = 0;
  #pragma omp parallel for schedule(static) reduction(+:t)
  for (Z j = 0; j < 1 << (DIM - 1); ++j)
    for (Z i = 0; i < 1 << DIM; ++i)
      t += K->p[j][i];
  return t;
}

static void points_from_cloud(points *Q, const cloud *K)
{
  #pragma omp parallel for schedule(static)
  for (Z j = 0; j < 1 << (DIM - 1); ++j)
  {
    for (Z i = 0; i < 1 << DIM; ++i)
    {
      Z p = K->p[j][i];
      if (0 < p && p < MAXPERIOD)
      {
        #pragma omp atomic update
        Q->n[p] += p;
      }
    }
  }
  for (Z p = 1; p < MAXPERIOD; ++p)
  {
    Q->cz[p] = Q->cz[p - 1] + Q->n[p - 1];
  }
  #pragma omp parallel for schedule(static)
  for (Z j = 0; j < 1 << (DIM - 1); ++j)
  {
    for (Z i = 0; i < 1 << DIM; ++i)
    {
      Z p = K->p[j][i];
      if (0 < p && p < MAXPERIOD)
      {
        ZZ k0 = 0;
        #pragma omp atomic capture
        k0 = Q->i[p] += p;
        k0 -= p;
        C c = cloud_c(i, j, DIM);
        C z = K->z[j][i];
        for (ZZ k = k0; k < k0 + p; ++k)
        {
          V4 v = { { creal(c), cimag(c), creal(z), cimag(z) } };
          Q->cz[p][k] = v;
          z = z * z + c;
        }
      }
    }
  }
}

typedef struct
{
  GLFWwindow *window;
  GLuint vbo[2];
  GLuint vao[2];
  GLuint tex[2];
  GLuint fbo;
  GLuint plot;
  GLint aspect;
  GLint eye;
  GLint shininess;
  GLint colour;
  GLint conjugate;
  GLint period;
  GLint transform1;
  GLint transform1_r;
  GLint transform1_b;
  GLint transform1_rb;
  GLint transform2;
  GLuint flat;
} graphics;

static void graphics_begin_plot(graphics *GL)
{
  glBindFramebuffer(GL_FRAMEBUFFER, GL->fbo);
  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glBindVertexArray(GL->vao[0]);
  glUseProgram(GL->plot);
  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE, GL_ONE);
}

static void graphics_set_transforms(graphics *GL, const M55 *T1, const M55 *T2, const V5 *eye)
{
  // pack
  F eye2[4];
  F transform2[4][4];
  F transform1[4][4], transform1_r[4], transform1_b[4], transform1_rb;
  for (Z i = 0; i < 4; ++i)
  {
    for (int j = 0; j < 4; ++j)
    {
      transform2[i][j] = T2->x[i][j];
      transform1[i][j] = T1->x[i][j];
    }
    transform1_r[i] = T1->x[4][i];
    transform1_b[i] = T1->x[i][4];
    eye2[i] = eye->x[i] / eye->x[4];
  }
  transform1_rb = T1->x[4][4];
  // upload
  glUniformMatrix4fv(GL->transform2, 1, GL_FALSE, &transform2[0][0]);
  glUniformMatrix4fv(GL->transform1, 1, GL_FALSE, &transform1[0][0]);
  glUniform4fv(GL->transform1_r, 1, &transform1_r[0]);
  glUniform4fv(GL->transform1_b, 1, &transform1_b[0]);
  glUniform1f(GL->transform1_rb, transform1_rb);
  glUniform4fv(GL->eye, 1, &eye2[0]);
}

static void graphics_plot_points(graphics *GL, const points *Q)
{
  R phi5 = pow((sqrt(5) + 1) / 2, 5);
  ZZ n = 0;
  for (Z p = 1; p < MAXPERIOD; ++p)
  {
    if (Q->n[p] > 0)
    {
      R hue = (p - 1) / phi5;
      F r, g, b;
      hsv2rgb(hue, 0.975, 1, &r, &g, &b);
      glUniform3f(GL->colour, r, g, b);
      glUniform1i(GL->period, p);
      for (Z q = 0; q < 2; ++q)
      {
        glUniform1i(GL->conjugate, q);
        glDrawArrays(GL_POINTS, n, Q->n[p]);
      }
      n += Q->n[p];
    }
  }
}

static void graphics_end_plot(graphics *GL)
{
  glDisable(GL_BLEND);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  glClearColor(0, 0, 1, 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glBindVertexArray(GL->vao[1]);
  glUseProgram(GL->flat);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

static const char *plot_vert =
  "#version 430 core\n"
  "uniform bool conjugate;\n"
  "uniform int period;\n"
  "uniform float aspect;\n"
  "uniform mat4 transform1;\n"
  "uniform vec4 transform1_r;\n"
  "uniform vec4 transform1_b;\n"
  "uniform float transform1_rb;\n"
  "uniform mat4 transform2;\n"
  "layout (location = 0) in vec4 cz;\n"
  "out vec4 position;\n"
  "out vec4 normal;\n"
  "float[5] mul(float[5][5] m, float[5] v)\n"
  "{\n"
  "  float[5] r;\n"
  "  for (int i = 0; i < 5; ++i)\n"
  "  {\n"
  "    float s = 0.0;\n"
  "    for (int j = 0; j < 5; ++j)\n"
  "      s += m[i][j] * v[j];\n"
  "    r[i] = s;\n"
  "  }\n"
  "  return r;\n"
  "}\n"
  "vec2 mul(vec2 a, vec2 b)\n"
  "{\n"
  "  return vec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);\n"
  "}\n"
  "void main(void)\n"
  "{\n"
  // pack transformation
  "  float[5][5] T1, N1;"
  "  for (int i = 0; i < 4; ++i)\n"
  "  {\n"
  "    for (int j = 0; j < 4; ++j)\n"
  "    {\n"
  "      T1[i][j] = transform1[i][j];\n"
  "      N1[i][j] = transform1[i][j];\n"
  "    }\n"
  "    T1[4][i] = transform1_r[i];\n"
  "    T1[i][4] = transform1_b[i];\n"
  "    N1[4][i] = 0.0;\n"
  "    N1[i][4] = 0.0;\n"
  "  }\n"
  "  T1[4][4] = transform1_rb;\n"
  "  N1[4][4] = 1.0;\n"
  "  vec2 c = cz.xy; if (conjugate) c.y = -c.y;\n"
  "  vec2 z = cz.zw; if (conjugate) z.y = -z.y;\n"
  // compute normal
#if 1
  "  vec2 dz = vec2(1.0, 0.0);\n"
  "  vec2 dc = vec2(0.0, 0.0);\n"
  "  for (int p = 0; p < period; ++p)\n"
  "  {\n"
  "    dz = 2.0 * mul(dz, z);\n"
  "    dc = 2.0 * mul(dc, z) + vec2(1.0, 0.0);\n"
  "    z = mul(z, z) + c;\n"
  "  }\n"
  "  float[5] n = float[5](dc.x, dc.y, dz.x - 1.0, dz.y, 0.0);\n"
  "  n = mul(N1, n);\n"
#endif
  // transform
  "  float[5] u = float[5](c.x, c.y, z.x, z.y, 1.0);\n"
  "  u = mul(T1, u);\n"
  "  position = vec4(u[0], u[1], u[2], u[3]);\n"
  "  normal = vec4(n[0], n[1], n[2], n[3]);\n"
  "  vec4 m, v;\n"
  "  for (int i = 0; i < 4; ++i)\n"
  "  {\n"
//  "    m[i] = n[i] / (n[4] != 0.0 ? n[4] : 1.0);\n"
  "    v[i] = u[i] / u[4];\n"
  "  }\n"
  "  if (all(lessThanEqual(abs(v), vec4(2.0))))\n"
  "  {\n"
  "    v[3] = 1.0;\n"
  "    v = transform2 * v;\n"
  "    v.x /= aspect;\n"
//  "  position = v.xyz / v.w;\n"
//  "  normal = m.xyz / (m.w != 0.0 ? m.w : 1.0);\n"
  "    gl_Position = v;\n"
  "  }\n"
  "  else\n"
  "    gl_Position = vec4(0.0/0.0);\n"
  "}\n"
  ;

static const char *plot_frag =
  "#version 430 core\n"
  "uniform vec3 colour;\n"
  "uniform float shininess;\n"
  "uniform int period;\n"
  "in vec4 position;\n"
  "in vec4 normal;\n"
  "layout (location = 0) out vec4 fcolour;\n"
  "void main(void)\n"
  "{\n"
#if 1
  "  vec4 eye = vec4(0.0, 0.0, 0.0, 0.0);\n"
  "  vec4 normalDir = normalize(normal);\n"
  "  vec4 viewDir = normalize(eye - position);\n"
  "  if (dot(viewDir, normalDir) < 0.0) normalDir = -normalDir;\n"
  "  float diffuse = 0.0;\n"
  "  float specular = 0.0;\n"
  "  for (int x = -2; x <= 2; x += 4)\n"
  "  for (int y = -2; y <= 2; y += 4)\n"
  "  for (int z = -2; z <= 2; z += 4)\n"
  "  for (int w = -2; w <= 2; w += 4)\n"
  "  {\n"
  "    vec4 light = vec4(x, y, z, w) * 2.0;\n"
  "    float distance = length(light - position) + length(eye - position);\n"
  "    float d = 1000.0 / pow(distance, 3.0);\n"
  "    vec4 lightDir = normalize(light - position);\n"
  "    float diffuse1 = max(dot(lightDir, normalDir), 0.0);\n"
  "    if (diffuse1 > 0.0)\n"
  "    {\n"
  "      vec4 halfDir = normalize(lightDir + viewDir);\n"
  "      specular += d * pow(max(dot(halfDir, normalDir), 0.0), shininess);\n"
  "      diffuse += d * diffuse1;\n"
  "    }\n"
  "  }\n"
  "  fcolour = vec4((diffuse * colour + specular * vec3(10.0)) / float(period), 1.0);\n"
#endif
//  "  fcolour = vec4(colour, 1.0);\n"
//  "  gl_FragDepth = depth;\n"
  "}\n"
  ;

static const char *flat_vert =
  "#version 430 core\n"
  "layout (location = 0) in vec4 p;\n"
  "out vec2 coord;\n"
  "void main(void)\n"
  "{\n"
  "  gl_Position = vec4(p.xy, 0.0, 1.0);\n"
  "  coord = p.zw;\n"
  "}\n"
  ;

static const char *flat_frag =
  "#version 430 core\n"
  "uniform sampler2D tex;\n"
  "in vec2 coord;\n"
  "layout (location = 0) out vec4 fcolour;\n"
  "void main(void)\n"
  "{\n"
  "  vec4 c = texture(tex, coord);\n"
  "  fcolour = vec4(pow(c.rgb / 1024.0, vec3(0.25)), 1.0);\n"
  "}\n"
  ;

void debug_program(GLuint program, const char *name) {
  if (program) {
    GLint linked = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (linked != GL_TRUE) {
      fprintf(stderr, "%s: OpenGL shader program link failed\n", name);
    }
    GLint length = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
    char *buffer = (char *) malloc(length + 1);
    glGetProgramInfoLog(program, length, NULL, buffer);
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "%s: OpenGL shader program info log\n", name);
      fprintf(stderr, "%s\n", buffer);
    }
    free(buffer);
  } else {
    fprintf(stderr, "%s: OpenGL shader program creation failed\n", name);
  }
}

void debug_shader(GLuint shader, GLenum type, const char *name) {
  const char *tname = 0;
  switch (type) {
    case GL_VERTEX_SHADER:   tname = "vertex";   break;
    case GL_GEOMETRY_SHADER: tname = "geometry"; break;
    case GL_FRAGMENT_SHADER: tname = "fragment"; break;
    default:                 tname = "unknown";  break;
  }
  if (shader) {
    GLint compiled = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (compiled != GL_TRUE) {
      fprintf(stderr, "%s: OpenGL %s shader compile failed\n", name, tname);
    }
    GLint length = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    char *buffer = (char *) malloc(length + 1);
    glGetShaderInfoLog(shader, length, NULL, buffer);
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "%s: OpenGL %s shader info log\n", name, tname);
      fprintf(stderr, "%s\n", buffer);
    }
    free(buffer);
  } else {
    fprintf(stderr, "%s: OpenGL %s shader creation failed\n", name, tname);
  }
}

void compile_shader(GLint program, GLenum type, const char *name, const GLchar *source) {
  GLuint shader = glCreateShader(type);
  glShaderSource(shader, 1, &source, NULL);
  glCompileShader(shader);
  debug_shader(shader, type, name);
  glAttachShader(program, shader);
  glDeleteShader(shader);
}

GLint compile_program(const char *name, const GLchar *vert, const GLchar *frag) {
  GLint program = glCreateProgram();
  if (vert) { compile_shader(program, GL_VERTEX_SHADER  , name, vert); }
  if (frag) { compile_shader(program, GL_FRAGMENT_SHADER, name, frag); }
  glLinkProgram(program);
  debug_program(program, name);
  return program;
}

void graphics_init(graphics *GL, Z width, Z height, points *Q)
{
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  glfwWindowHint(GLFW_DECORATED, GL_FALSE);
  GL->window = glfwCreateWindow(width, height, "Ultimate Anti-Buddhagram", 0, 0);
  glfwMakeContextCurrent(GL->window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew
  glViewport(0, 0, width, height);
  glDisable(GL_DEPTH_TEST);
//  glEnable(GL_PROGRAM_POINT_SIZE);
  glPointSize(1);
  GL->plot = compile_program("plot", plot_vert, plot_frag);
  glUseProgram(GL->plot);
  GL->transform1 = glGetUniformLocation(GL->plot, "transform1");
  GL->transform1_r = glGetUniformLocation(GL->plot, "transform1_r");
  GL->transform1_b = glGetUniformLocation(GL->plot, "transform1_b");
  GL->transform1_rb = glGetUniformLocation(GL->plot, "transform1_rb");
  GL->transform2 = glGetUniformLocation(GL->plot, "transform2");
  GL->conjugate = glGetUniformLocation(GL->plot, "conjugate");
  GL->period = glGetUniformLocation(GL->plot, "period");
  GL->colour = glGetUniformLocation(GL->plot, "colour");
  GL->shininess = glGetUniformLocation(GL->plot, "shininess");
  glUniform1f(GL->shininess, 100);
  GL->eye = glGetUniformLocation(GL->plot, "eye");
  GL->aspect = glGetUniformLocation(GL->plot, "aspect");
  glUniform1f(GL->aspect, width / (R) height);
  GL->flat = compile_program("flat", flat_vert, flat_frag);
  glGenTextures(2, &GL->tex[0]);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, GL->tex[1]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, GL->tex[0]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glGenFramebuffers(1, &GL->fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, GL->fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, GL->tex[0], 0);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, GL->tex[1], 0);
  glClear(GL_COLOR_BUFFER_BIT);
  glGenVertexArrays(2, &GL->vao[0]);
  glGenBuffers(2, &GL->vbo[0]);
  glBindVertexArray(GL->vao[0]);
  glBindBuffer(GL_ARRAY_BUFFER, GL->vbo[0]);
  glBufferData(GL_ARRAY_BUFFER, Q->bytes_used, Q->cz[0], GL_STATIC_DRAW);
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);
  glBindVertexArray(GL->vao[1]);
  glBindBuffer(GL_ARRAY_BUFFER, GL->vbo[1]);
  float vbo_data[16] =
    { -1, -1, 0, 0
    ,  1, -1, 1, 0
    , -1,  1, 0, 1
    ,  1,  1, 1, 1
    };
  glBufferData(GL_ARRAY_BUFFER, sizeof(vbo_data), vbo_data, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);
}


static void key_handler(GLFWwindow *window, int key, int scancode, int action, int mods) {
  if (action == GLFW_PRESS) {
    switch (key) {
      case GLFW_KEY_Q:
      case GLFW_KEY_ESCAPE:
        glfwSetWindowShouldClose(window, GL_TRUE);
        break;
    }
  }
  (void) scancode;
  (void) mods;
}

 extern int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  size_t bytes = 0;
  cloud *K = cloud_alloc(&bytes);
  if (! K)
    return 1;
  cloud_clear(K);
  ZZ points_c = cloud_calculate(K, DIM);
  ZZ points_z = cloud_count(K);
  fprintf(stderr, "%lld\n%lld\n", points_c, points_z);
#if 0
  accum *A = accum_alloc();
  image *G = image_alloc();
#endif

  R aspect = 1;//(WIDTH / (R) HEIGHT);
  R zoom = 1;
  R near = 2, far = 10, left = -1, right = 1, top = 1, bottom = -1, in = -1, out = 1;
  M55 offset2 =
    { { { 4, 0, 0,  0, 0 }
      , { 0, 4, 0,  0, 0 }
      , { 0, 0, 4, -6, 0 }
      , { 0, 0, 0,  1, 0 }
      , { 0, 0, 0,  0, 1 }
      }
    };
  M55 frustum2 =
    { { { 2 * near * aspect / (zoom * (right - left)), 0, (right + left) / (right - left), 0, 0 }
      , { 0, 2 * near / (zoom * (top - bottom)), (top + bottom) / (top - bottom), 0, 0 }
      , { 0, 0, -(far + near) / (far - near), -2 * far * near / (far - near), 0 }
      , { 0, 0, -1, 0, 0 }
      , { 0, 0, 0, 0, 1 }
      }
    };
  M55 T2 = mul_m55_m55(&frustum2, &offset2);
  struct quaternion pq0[2] = { { { 1, 0, 0, 0 } }, { { 1, 0, 0, 0 } } };
  struct quaternion pq[2] = { { { 1, 0, 0, 0 } }, { { 1, 0, 0, 0 } } };
  struct quaternion target[2];
  random_unit_quaternion(&target[0]);
  random_unit_quaternion(&target[1]);

  points *Q = points_alloc(points_z);
  points_from_cloud(Q, K);
  cloud_free(K, bytes);
  graphics GL;
  graphics_init(&GL, WIDTH, HEIGHT, Q);
  points_free_data(Q);

#define pipeline 8
  size_t ppmbytes = WIDTH * (HEIGHT * 3);
  GLsync syncs[pipeline];
  GLuint pbo[pipeline];
  glGenBuffers(pipeline, &pbo[0]);
  for (int i = 0; i < pipeline; ++i) {
    glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo[i]);
    glBufferStorage(GL_PIXEL_PACK_BUFFER, ppmbytes, 0, GL_MAP_READ_BIT);
  }

  glfwSetKeyCallback(GL.window, key_handler);
  glfwPollEvents();
  ZZ frame = 0;
  const int speed = 65 * 16;
  while (! glfwWindowShouldClose(GL.window))
  {
    if ((frame + pipeline) / speed >= 8) break;
    GLint e = glGetError();
    if (e) fprintf(stderr, "E: %d\n", e);
    for (Z q = 0; q < 2; ++q)
    {
      if (frame % speed == 0)
      {
        random_unit_quaternion(&target[q]);
      }
      qslerp(&pq[q], &pq[q], &target[q], 1e-3);
    }
    M55 center =
      { { { 1, 0, 0, 0, -v0.x[0] }
        , { 0, 1, 0, 0, -v0.x[1] }
        , { 0, 0, 1, 0, -v0.x[2] }
        , { 0, 0, 0, 1, -v0.x[3] }
        , { 0, 0, 0, 0, 1 }
        }
      };
    struct matrix4 rotate0;
    mquat2(&rotate0, &pq[0], &pq[1]);
    M55 rotate1 =
      { { { 1, 0, 0, 0, 0 }
        , { 0, 1, 0, 0, 0 }
        , { 0, 0, 1, 0, 0 }
        , { 0, 0, 0, 1, 0 }
        , { 0, 0, 0, 0, 1 }
        }
      };
    M55 rotate1T =
      { { { 1, 0, 0, 0, 0 }
        , { 0, 1, 0, 0, 0 }
        , { 0, 0, 1, 0, 0 }
        , { 0, 0, 0, 1, 0 }
        , { 0, 0, 0, 0, 1 }
        }
      };
    Z c = 0;
    for (Z a = 0; a < 4; ++a)
      for (Z b = 0; b < 4; ++b)
      {
        rotate1 .x[a][b] = rotate0.m[c];
        rotate1T.x[b][a] = rotate0.m[c];
        ++c;
      }
    R focus = (4 * fabs(((frame % speed) + 0.5) / speed - 0.5) - 1) * 2 + 6;
    M55 offset =
      { { { 1, 0, 0, 0,  0 }
        , { 0, 1, 0, 0,  0 }
        , { 0, 0, 1, 0,  0 }
        , { 0, 0, 0, 1, -6 }
        , { 0, 0, 0, 0,  1 }
        }
      };
    R depth = 32.0 / speed;
    near = focus - depth;
    far = focus + depth;
    M55 frustum =
      { { { 2 * near / (zoom * (right - left)), 0, 0, (right + left) / (right - left), 0 }
        , { 0, 2 * near / (zoom * (top - bottom)), 0, (top + bottom) / (top - bottom), 0 }
        , { 0, 0, 2 * near / (zoom * (in - out)), (in + out) / (in - out), 0 }
        , { 0, 0, 0, -(far + near) / (far - near), -2 * far * near / (far - near) }
        , { 0, 0, 0, -1, 0 }
        }
      };
    M55 ortho =
      { { { 2 / (right - left), 0, 0, - (right + left) / (right - left) }
        , { 0, 2 / (top - bottom), 0, 0, - (top + bottom) / (top - bottom) }
        , { 0, 0, 2 / (out - in), 0, - (out + in) / (out - in) }
        , { 0, 0, 0, -2 / (far - near), - (far + near) / (far - near) }
        , { 0, 0, 0, 0, 1 }
        }
      };
    M55 m = mul_m55_m55(&rotate1, &center);
    m = mul_m55_m55(&offset, &m);
    m = mul_m55_m55(&ortho, &m);
    V5 eye = { { 0, 0, 0, 6, 1 } };
    eye = mul_m55_v5(&rotate1T, &eye);
    M55 T1 = m;


#if 0
    accum_clear(A);
    ZZ total = accum_from_cloud(A, K, &T1, &T2);
//    fprintf(stderr, "%lld\n", total);
    image_from_accum(G, A, total);
#endif
    graphics_begin_plot(&GL);
    graphics_set_transforms(&GL, &T1, &T2, &eye);
    graphics_plot_points(&GL, Q);
    graphics_end_plot(&GL);
    int k = frame % pipeline;
    glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo[k]);
    GLsync s = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
    if (frame - pipeline >= 0) {
      glWaitSync(syncs[k], 0, GL_TIMEOUT_IGNORED);
      void *buf = glMapBufferRange(GL_PIXEL_PACK_BUFFER, 0, ppmbytes, GL_MAP_READ_BIT);
      fprintf(stdout, "P6\n%d %d\n%d\n", WIDTH, HEIGHT, 255);
      fwrite(buf, ppmbytes, 1, stdout);
      glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
    }
    syncs[k] = s;
    glReadPixels(0, 0, WIDTH, HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glfwSwapBuffers(GL.window);
    frame++;
    glfwPollEvents();
  }
#if 0
  accum_free(A);
  image_free(G);
#endif
  return 0;
}
