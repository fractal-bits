#version 330 core
#extension GL_ARB_explicit_uniform_location : require

layout (location = 1) uniform sampler2D tex;
layout (location = 2) uniform float mean;

out vec4 colour;

void main(void)
{
  // float mean = texelFetch(tex, ivec2(0, 0), level).x;
  vec2 self = texelFetch(tex, ivec2(gl_FragCoord.xy), 0).xy;
  vec2 v = vec2(0.0);
  if (self.x > 0.0)
  {
    v.x = sqrt(sqrt(log(1.0 + log(1.0 + self.x / mean)))) * 200.0 / 255.0;
  }
  if (self.y > 0.0)
  {
    v.y = sqrt(sqrt(log(1.0 + log(1.0 + self.y / mean)))) * 200.0 / 255.0;
  }
  colour = vec4(v.xyx, 1);
}
