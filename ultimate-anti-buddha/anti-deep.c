#define _POSIX_C_SOURCE 2

#include <assert.h>
#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <omp.h>

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

#if 0
typedef int32_t Z;
#define PRZ "d"
typedef uint32_t N;
#define PRN "u"
#else
typedef int64_t Z;
#define PRZ "ld"
typedef uint64_t N;
#define PRN "lu"
#endif

typedef double R;

typedef double _Complex C;

struct Box
{
  N k, j, i;
};

struct Ball
{
  C z; // nucleus for M.c, image of nucleus for M.z[k]
  R r; // size in parameter plane for M.c, dynamic plane for M.z[k]
  N n; // period for M.c, iteration for M.z[k]
};

struct Coord
{
  R v;
  N id;
};

struct Node
{
  R lo_x, lo_y, hi_x, hi_y;
  N leaf; // 0 for branches, > 0 for leaf
  N children[8];
};

struct Tree
{
  struct Node *nodes;
  N allocated;
  N count;
};

struct BallList
{
  struct Ball *data;
  N allocated;
  N count;
};

static inline
C csqr(C a)
{
  R ax = creal(a);
  R ay = cimag(a);
  R cx = (ax + ay) * (ax - ay);
  R cy = 2 * ax * ay;
  return cx + I * cy;
}

static inline
C rcmul(R a, C b)
{
  R bx = creal(b);
  R by = cimag(b);
  R cx = a * bx;
  R cy = a * by;
  return cx + I * cy;
}

static inline
C cmul(C a, C b)
{
  R ax = creal(a);
  R ay = cimag(a);
  R bx = creal(b);
  R by = cimag(b);
  R cx = ax * bx - ay * by;
  R cy = ax * by + ay * bx;
  return cx + I * cy;
}

static inline
C crecip(C b)
{
  R bx = creal(b);
  R by = cimag(b);
  R d = bx * bx + by * by;
  R cx = bx / d;
  R cy = -by / d;
  return cx + I * cy;
}

static inline
C cdiv(C a, C b)
{
  R ax = creal(a);
  R ay = cimag(a);
  R bx = creal(b);
  R by = cimag(b);
  R d = bx * bx + by * by;
  R cx = (ax * bx + ay * by) / d;
  R cy = (ay * bx - ax * by) / d;
  return cx + I * cy;
}

static inline
R cnorm(C z)
{
  R x = creal(z);
  R y = cimag(z);
  return x * x + y * y;
}

void hsv2rgb(R h, R s, R v, R *rp, R *gp, R *bp) {
  R i, f, p, q, t, r, g, b;
  Z ii;
  if (s == 0.0) {
    r = v;
    g = v;
    b = v;
  } else {
    h = h - floor(h);
    h = h * 6;
    i = floor(h);
    ii = i;
    f = h - i;
    p = v * (1 - s);
    q = v * (1 - (s * f));
    t = v * (1 - (s * (1 - f)));
    switch(ii) {
      case 0:  r = v; g = t; b = p; break;
      case 1:  r = q; g = v; b = p; break;
      case 2:  r = p; g = v; b = t; break;
      case 3:  r = p; g = q; b = v; break;
      case 4:  r = t; g = p; b = v; break;
      default: r = v; g = p; b = q; break;
    }
  }
  *rp = r;
  *gp = g;
  *bp = b;
}

const struct Ball zero = { 0, 0, 0 };

bool range_overlap(R alo, R ahi, R blo,  R bhi)
{
  return ! (alo > bhi || blo > ahi);
}

bool node_overlap(struct Node *a, struct Node *b)
{
  return
    range_overlap(a->lo_x, a->hi_x, b->lo_x, b->hi_x) &&
    range_overlap(a->lo_y, a->hi_y, b->lo_y, b->hi_y) ;
}

void tree_init(struct Tree *tree)
{
  tree->allocated = 1024;
  tree->nodes = malloc(sizeof(*tree->nodes) * tree->allocated);
  tree->nodes[0] = (struct Node)
    { -1.0/0.0, -1.0/0.0
    ,  1.0/0.0,  1.0/0.0
    , 0
    , {0,0,0,0,0,0,0,0}
    };
  tree->count = 1;
}

void tree_clear(struct Tree *tree)
{
  free(tree->nodes);
  tree->nodes = 0;
}

void node_clear(struct Node *node)
{
  *node = (struct Node)
    {  1.0/0.0,  1.0/0.0
    , -1.0/0.0, -1.0/0.0
    , 0
    , {0,0,0,0,0,0,0,0}
    };
}

void tree_clear_node(struct Tree *tree, N node)
{
  node_clear(&tree->nodes[node]);
}

N tree_alloc(struct Tree *tree)
{
  if (tree->count == tree->allocated)
  {
    tree->allocated <<= 1;
    tree->nodes = realloc(tree->nodes, sizeof(*tree->nodes) * tree->allocated);
  }
  tree_clear_node(tree, tree->count);
  return tree->count++;
}

void tree_insert(struct Tree *tree, N parent, N i, N child)
{
  tree->nodes[parent].children[i] = child;
  tree->nodes[parent].lo_x
    = fmin(tree->nodes[parent].lo_x, tree->nodes[child].lo_x);
  tree->nodes[parent].lo_y
    = fmin(tree->nodes[parent].lo_y, tree->nodes[child].lo_y);
  tree->nodes[parent].hi_x
    = fmax(tree->nodes[parent].hi_x, tree->nodes[child].hi_x);
  tree->nodes[parent].hi_y
    = fmax(tree->nodes[parent].hi_y, tree->nodes[child].hi_y);
}

int coords_cmp(const void *a, const void *b)
{
  const struct Coord *p = a, *q = b;
  R x = p->v;
  R y = q->v;
  return (x > y) - (y > x);
}

void tree_split_node(struct Tree *tree, N parent)
{
  struct Coord coords[8];
  for (N i = 0; i < 8; ++i)
  {
    coords[i].id = tree->nodes[parent].children[i];
    coords[i].v = tree->nodes[coords[i].id].lo_x;
  }
  qsort(coords, 8, sizeof(*coords), coords_cmp);
  for (N i = 0; i < 8; ++i)
  {
    coords[i].v = tree->nodes[coords[i].id].lo_y;
  }
  qsort(coords, 4, sizeof(*coords), coords_cmp);
  qsort(coords + 4, 4, sizeof(*coords), coords_cmp);
  N a = tree_alloc(tree);
  N b = tree_alloc(tree);
  N c = tree_alloc(tree);
  N d = tree_alloc(tree);
  tree_insert(tree, a, 0, coords[0].id);
  tree_insert(tree, a, 1, coords[1].id);
  tree_insert(tree, b, 0, coords[2].id);
  tree_insert(tree, b, 1, coords[3].id);
  tree_insert(tree, c, 0, coords[4].id);
  tree_insert(tree, c, 1, coords[5].id);
  tree_insert(tree, d, 0, coords[6].id);
  tree_insert(tree, d, 1, coords[7].id);
  tree_clear_node(tree, parent);
  tree_insert(tree, parent, 0, a);
  tree_insert(tree, parent, 1, b);
  tree_insert(tree, parent, 2, c);
  tree_insert(tree, parent, 3, d);
}

bool tree_insert_unique_recursive(struct Tree *tree, N parent, struct Node *child)
{
  R best_score = 1.0/0.0;
  N best_candidate = 0;
  for (N i = 0; i < 8; ++i)
  {
    N candidate = tree->nodes[parent].children[i];
    if (candidate == 0)
    {
      N id = tree_alloc(tree);
      tree->nodes[id] = *child;
      tree_insert(tree, parent, i, id);
      return true;
    }
    if (tree->nodes[candidate].leaf)
    {
      if (tree->nodes[candidate].leaf == child->leaf &&
          node_overlap(&tree->nodes[candidate], child))
      {
        // already in tree
        return false;
      }
      // can't insert into leaf
      continue;
    }
    R lo_x = fmin(tree->nodes[candidate].lo_x, child->lo_x);
    R lo_y = fmin(tree->nodes[candidate].lo_y, child->lo_y);
    R hi_x = fmax(tree->nodes[candidate].hi_x, child->hi_x);
    R hi_y = fmax(tree->nodes[candidate].hi_y, child->hi_y);
    R old_size
      = (tree->nodes[candidate].hi_x - tree->nodes[candidate].lo_x)
      * (tree->nodes[candidate].hi_y - tree->nodes[candidate].lo_y);
    R new_size = (hi_x - lo_x) * (hi_y - lo_y);
    R score = new_size - old_size;
    if (score < best_score)
    {
      best_score = score;
      best_candidate = candidate;
    }
  }
  if (best_candidate)
  {
    return tree_insert_unique_recursive(tree, best_candidate, child);
  }
  else
  {
    // node is full of leafs
    tree_split_node(tree, parent);
    return tree_insert_unique_recursive(tree, parent, child);
  }
}

bool tree_insert_unique_leaf(struct Tree *tree, R lo_x, R lo_y, R hi_x, R hi_y, N leaf)
{
  assert(leaf);
  struct Node node;
  node_clear(&node);
  node.lo_x = lo_x;
  node.lo_y = lo_y;
  node.hi_x = hi_x;
  node.hi_y = hi_y;
  node.leaf = leaf;
  return tree_insert_unique_recursive(tree, 0, &node);
}

bool tree_insert_unique_ball(struct Tree *tree, struct Ball ball)
{
  R x = creal(ball.z);
  R y = cimag(ball.z);
  R r = ball.r;
  return tree_insert_unique_leaf(tree, x - r, y - r, x + r, y + r, ball.n);
}

bool tree_contains_recursive(struct Tree *tree, N parent, struct Node *node)
{
  if (node_overlap(&tree->nodes[parent], node))
  {
    if (tree->nodes[parent].leaf == node->leaf)
    {
      return true;
    }
    if (tree->nodes[parent].leaf)
    {
      return false;
    }
    for (N i = 0; i < 8; ++i)
    {
      if (! tree->nodes[parent].children[i])
      {
        break;
      }
      if (tree_contains_recursive(tree, tree->nodes[parent].children[i], node))
      {
        return true;
      }
    }
  }
  return false;
}

bool tree_contains_leaf(struct Tree *tree, R lo_x, R lo_y, R hi_x, R hi_y, N leaf)
{
  assert(leaf);
  struct Node node;
  node_clear(&node);
  node.lo_x = lo_x;
  node.lo_y = lo_y;
  node.hi_x = hi_x;
  node.hi_y = hi_y;
  node.leaf = leaf;
  return tree_contains_recursive(tree, 0, &node);
}

bool tree_contains_ball(struct Tree *tree, struct Ball ball)
{
  R x = creal(ball.z);
  R y = cimag(ball.z);
  R r = ball.r;
  return tree_contains_leaf(tree, x - r, y - r, x + r, y + r, ball.n);
}

static inline
N max(N a, N b)
{
  return a > b ? a : b;
}

N tree_lookup_recursive(struct Tree *tree, N parent, R x, R y)
{
  if (tree->nodes[parent].lo_x <= x &&
      x <= tree->nodes[parent].hi_x &&
      tree->nodes[parent].lo_y <= y &&
      y <= tree->nodes[parent].hi_y)
  {
    if (tree->nodes[parent].leaf)
    {
      return tree->nodes[parent].leaf;
    }
    N leaf = 0;
    for (N i = 0; i < 8; ++i)
    {
      N child = tree->nodes[parent].children[i];
      if (! child)
      {
        break;
      }
      leaf = max(leaf, tree_lookup_recursive(tree, child, x, y));
    }
    return leaf;
  }
  return 0;
}

N tree_lookup(struct Tree *tree, C z)
{
  return tree_lookup_recursive(tree, 0, creal(z), cimag(z));
}

// highest leaf that contains whole region
N tree_lookup_region_recursive(struct Tree *tree, N parent, R lo_x, R lo_y, R hi_x, R hi_y)
{
  if (tree->nodes[parent].lo_x <= lo_x &&
      hi_x <= tree->nodes[parent].hi_x &&
      tree->nodes[parent].lo_y <= lo_y &&
      hi_y <= tree->nodes[parent].hi_y)
  {
    if (tree->nodes[parent].leaf)
    {
      return tree->nodes[parent].leaf;
    }
    N leaf = 0;
    for (N i = 0; i < 8; ++i)
    {
      N child = tree->nodes[parent].children[i];
      if (! child)
      {
        break;
      }
      leaf = max(leaf, tree_lookup_region_recursive(tree, child, lo_x, lo_y, hi_x, hi_y));
    }
    return leaf;
  }
  return 0;
}

N tree_lookup_region(struct Tree *tree, R lo_x, R lo_y, R hi_x, R hi_y)
{
  return tree_lookup_region_recursive(tree, 0, lo_x, lo_y, hi_x, hi_y);
}

struct Ball z_squared_plus_c(struct Ball z, struct Ball c)
{
  return (struct Ball)
    { z.z * z.z + c.z
    , (2 * sqrt(cnorm(z.z)) + z.r) * z.r + c.r
    , z.n + 1
    };
}

bool ball_intersects(const struct Ball a, const struct Ball b)
{
  R d2 = cnorm(a.z - b.z);
  R r = a.r + b.r;
  return d2 <= r * r;
}

bool ball_contains(const struct Ball a, const struct Ball b)
{
  R d2 = cnorm(a.z - b.z);
  R r = fmax(0, a.r - b.r);
  return d2 <= r * r;
}

const R eps2 = 1e-24;
const N nucleus_steps = 8;
bool find_nucleus(C *cp, N period)
{
  C c = *cp;
  for (N i = 0; i < nucleus_steps; ++i)
  {
#if 0
    C z = 0, dc = 0;
    for (N j = 0; j < period; ++j)
    {
      dc = rcmul(2, cmul(z, dc)) + 1;
      z = csqr(z) + c;
    }
#else
    R cx = creal(c);
    R cy = cimag(c);
    R zx = 0;
    R zy = 0;
    R dcx = 0;
    R dcy = 0;
    for (N j = 0; j < period; ++j)
    {
      R dcx_ = 2 * (zx * dcx - zy * dcy) + 1;
      R dcy_ = 2 * (zx * dcy + zy * dcx);
      R zx_ = (zx + zy) * (zx - zy) + cx;
      R zy_ = 2 * zx * zy + cy;
      dcx = dcx_;
      dcy = dcy_;
      zx = zx_;
      zy = zy_;
    }
    C z = zx + I * zy;
    C dc = dcx + I * dcy;
#endif
    C d = cdiv(z, dc);
    c -= d;
    R e = cnorm(d);
    if (e < eps2)
    {
      *cp = c;
      return true;
    }
    if (isnan(e) || isinf(e))
    {
      return false;
    }
  }
  return false;
}

#define origin (-2 - 2 * I)
#define diameter 4.0
#define rsqrt2 0.707106781186547
struct Ball box_bounding_ball(struct Box b)
{
  R d = diameter / ((N) 1 << b.k);
  C c = origin + rcmul(d, (b.i + 0.5) + I * (b.j + 0.5));
  return (struct Ball){ c, d * rsqrt2, 0 };
}

static inline
bool box_contains(struct Box b, C z)
{
  R d = diameter / ((N) 1 << b.k);
  C lower_left  = origin + rcmul(d, b.i + I * b.j);
  C upper_right = lower_left + (d + I * d);
  return
    creal(lower_left) <= creal(z) &&
    creal(z) < creal(upper_right) &&
    cimag(lower_left) <= cimag(z) &&
    cimag(z) < cimag(upper_right) ;
}

bool find_nucleus_in_box(C *cp, N period, struct Box b)
{
  R d = diameter / ((N) 1 << b.k);
  C lower_left = origin + rcmul(d, b.i + I * b.j);
  C upper_right = lower_left + (d + I * d);
  R lower_left_x = creal(lower_left);
  R lower_left_y = cimag(lower_left);
  R upper_right_x = creal(upper_right);
  R upper_right_y = cimag(upper_right);

  C c = *cp;
  for (N i = 0; i < nucleus_steps; ++i)
  {
#if 0
    C z = 0, dc = 0;
    for (N j = 0; j < period; ++j)
    {
      dc = rcmul(2, cmul(z, dc)) + 1;
      z = csqr(z) + c;
    }
#else
    R cx = creal(c);
    R cy = cimag(c);
    R zx = 0;
    R zy = 0;
    R dcx = 0;
    R dcy = 0;
    for (N j = 0; j < period; ++j)
    {
      R dcx_ = 2 * (zx * dcx - zy * dcy) + 1;
      R dcy_ = 2 * (zx * dcy + zy * dcx);
      R zx_ = (zx + zy) * (zx - zy) + cx;
      R zy_ = 2 * zx * zy + cy;
      dcx = dcx_;
      dcy = dcy_;
      zx = zx_;
      zy = zy_;
    }
    C z = zx + I * zy;
    C dc = dcx + I * dcy;
#endif
    C d = cdiv(z, dc);
    c -= d;
    cx = creal(c);
    cy = cimag(c);
    bool in_box =
      lower_left_x <= cx &&
      cx < upper_right_x &&
      lower_left_y <= cy &&
      cy < upper_right_y ;
    if (! in_box)
    {
      return false;
    }
    R e = cnorm(d);
    if (e < eps2)
    {
      *cp = c;
      return true;
    }
    if (isnan(e) || isinf(e))
    {
      return false;
    }
  }
  return false;
}


const N attractor_steps = 8;
bool find_attractor(C *zp, C c, N period, C *dzp) {
  R cx = creal(c);
  R cy = cimag(c);
  C z0 = *zp;
  for (N i = 0; i < attractor_steps; ++i)
  {
#if 0
    C z = z0;
    C dz = 1;
    for (N j = 0; j < period; ++j)
    {
      dz = rcmul(2, cmul(dz, z));
      z = csqr(z) + c;
    }
#else
    R zx = creal(z0);
    R zy = cimag(z0);
    R dzx = 1;
    R dzy = 0;
    for (N j = 0; j < period; ++j)
    {
      R dzx_ = 2 * (zx * dzx - zy * dzy);
      R dzy_ = 2 * (zx * dzy + zy * dzx);
      R zx_ = (zx + zy) * (zx - zy) + cx;
      R zy_ = 2 * zx * zy + cy;
      dzx = dzx_;
      dzy = dzy_;
      zx = zx_;
      zy = zy_;
    }
    C z = zx + I * zy;
    C dz = dzx + I * dzy;
#endif
    C d = cdiv(z - z0, dz - 1);
    z0 -= d;
    R e = cnorm(d);
    if (e < eps2)
    {
      *zp = z0;
      *dzp = dz;
      return true;
    }
    if (isnan(e) || isinf(e))
    {
      return false;
    }
  }
  return false;
}

R find_interior_distance(C z, C c, N period)
{
  C dz = 1;
  C dzdz = 0;
  C dc = 0;
  C dcdz = 0;
  for (N i = 0; i < period; ++i)
  {
    dcdz = rcmul(2, cmul(z, dcdz) + cmul(dz, dc));
    dc = rcmul(2, cmul(z, dc)) + 1;
    dzdz = rcmul(2, csqr(dz) + cmul(z, dzdz));
    dz = rcmul(2, cmul(z, dz));
    z = csqr(z) + c;
  }
  return (1 - cnorm(dz)) / sqrt(cnorm(dcdz + cmul(dzdz, cdiv(dc, 1 - dz))));
}

C find_size(C nucleus, N period)
{
  C l = 1;
  C b = 1;
  C z = 0;
  for (N i = 1; i < period; ++i) {
    z = csqr(z) + nucleus;
    l = rcmul(2, cmul(z, l));
    b = b + crecip(l);
  }
  return crecip(cmul(b, csqr(l)));
}

bool is_cardioid(C nucleus, N period)
{
  C z = nucleus;
  C dc = 1;
  C dz = 1;
  C dcdc = 0;
  C dcdz = 0;
  for (N i = 1; i < period; ++i)
  {
    dcdc = rcmul(2, cmul(z, dcdc) + csqr(dc));
    dcdz = rcmul(2, cmul(z, dcdz) + cmul(dc, dz));
    dc = rcmul(2, cmul(z, dc)) + 1;
    dz = rcmul(2, cmul(z, dz));
    z = csqr(z) + nucleus;
  }
  C shape = - cdiv(cdiv(dcdc, rcmul(2, dc)) + cdiv(dcdz, dz), cmul(dc, dz));
  return cnorm(shape) < cnorm(shape - 1);
}

Z ball_check_iterates(struct Ball view, struct Ball c)
{
  struct Ball z = zero;
  Z contained = 0;
  Z intersected = 0;
  for (N n = 0; n < c.n; ++n)
  {
    z = z_squared_plus_c(z, c);
    if (ball_contains(view, z))
    {
      ++contained;
      ++intersected;
      break;
    }
    else if (ball_intersects(view, z))
    {
      ++intersected;
    }
  }
  return contained > 0 ? contained : -intersected;
}

void balllist_append(struct BallList *list, struct Ball ball)
{
//  #pragma omp critical
  {
    if (list->count == list->allocated)
    {
      list->allocated <<= 1;
      list->data = realloc(list->data, sizeof(*list->data) * list->allocated);
    }
    list->data[list->count++] = ball;
  }
}

void balllist_init(struct BallList *list)
{
  list->allocated = 1024;
  list->data = malloc(sizeof(*list->data) * list->allocated);
  list->count = 0;
}

void balllist_clear(struct BallList *list)
{
  free(list->data);
  list->data = 0;
  list->allocated = 0;
  list->count = 0;
}


struct Context
{
  R lo_x, lo_y, hi_x, hi_y;
  struct Ball view;
  struct Tree tree;
  struct BallList list;
  N total_count;
  N intersected_count;
  N contained_count;
  N maximum_depth;
  N maximum_period;
  N threads;
  N thread_limit;
};

void context_init(struct Context *context)
{
  tree_init(&context->tree);
  balllist_init(&context->list);
  context->total_count = 0;
  context->intersected_count = 0;
  context->contained_count = 0;
  context->threads = 1;
}

void context_clear(struct Context *context)
{
  tree_clear(&context->tree);
  balllist_clear(&context->list);
}

void context_depth_first(struct Context *context, struct Box b)
{
  if (b.k > context->maximum_depth)
  {
    return;
  }
  struct Ball c = box_bounding_ball(b);
  struct Ball z = zero;
  N maximum_period = context->maximum_period;
  bool recurse = false;
  bool stop = false;
  R mz2 = 1.0 / 0.0;
  for (N period = 1; period <= maximum_period; ++period)
  {
    z = z_squared_plus_c(z, c);
    if (isnan(z.r) || isinf(z.r))
    {
      // escaped
      break;
    }
    R z2 = cnorm(z.z);
    if (! (z2 < 65536))
    {
      // escaped
      break;
    }
    if ((! recurse) && ball_intersects(context->view, z))
    {
      recurse = true;
    }
    if (ball_contains(z, zero))
    {
      C nucleus = c.z;
      if (find_nucleus_in_box(&nucleus, period, b))
      {
        if (is_cardioid(nucleus, period))
        {
          R size = sqrt(cnorm(find_size(nucleus, period)));
          struct Ball mini = { nucleus, 2 * size, period };
          Z intersections = ball_check_iterates(context->view, mini);
          if (intersections != 0)
          {
            if (tree_lookup(&context->tree, nucleus) == period)
            {
              // already plotted
            }
            else
            {
              tree_insert_unique_ball(&context->tree, mini);
              balllist_append(&context->list, mini);
            }
          }
          if (intersections > 0)
          {
            context->contained_count++;
          }
          if (intersections < 0)
          {
            context->intersected_count++;
          }
          context->total_count++;
        }
      }
      stop = true;
    } // if ball contains zero
    if (z2 < mz2)
    {
      // in atom domain
      if (8 * z2 < mz2)
      {
        // deep in atom domain
        C z1 = z.z, dz1;
        if (find_attractor(&z1, c.z, period, &dz1))
        {
          if (cnorm(dz1) < 1)
          {
            // interior
            if (find_interior_distance(z1, c.z, period) > 4 * c.r)
            {
              // far interior
              recurse = false;
            }
            break;
          }
        }
      }
      mz2 = z2;
    }
    if (stop)
    {
      break;
    }
  }
  if (recurse)
  {
    N threads;
//    #pragma omp atomic capture
    threads = context->threads += 3;
    bool parallel = threads <= context->thread_limit;
    if (! parallel)
    {
//      #pragma omp atomic
      context->threads -= 3;
    }
//    #pragma omp parallel for collapse(2) if (parallel)
    for (N dj = 0; dj < 2; ++dj)
    {
      for (N di = 0; di < 2; ++di)
      {
        context_depth_first(context, (struct Box)
          { b.k + 1
          , (b.j << 1) + dj
          , (b.i << 1) + di
          });
        if (parallel && di + dj > 0)
        {
//          #pragma omp atomic
          context->threads -= 1;
        }
      }
    }
  }
}

uint32_t hash(uint32_t a)
{
  a = (a+0x7ed55d16) + (a<<12);
  a = (a^0xc761c23c) ^ (a>>19);
  a = (a+0x165667b1) + (a<<5);
  a = (a+0xd3a2646c) ^ (a<<9);
  a = (a+0xfd7046c5) + (a<<3);
  a = (a^0xb55a4f09) ^ (a>>16);
  return a;
}

uint32_t hash4(uint32_t a, uint32_t b, uint32_t c, uint32_t d)
{
  uint32_t h = 0;
  h = hash(h ^ hash(a));
  h = hash(h ^ hash(b));
  h = hash(h ^ hash(c));
  h = hash(h ^ hash(d));
  return h;
}

C jitter(uint32_t seed)
{
  uint64_t x = ((uint64_t) hash(seed + hash(1)) << 32) + hash(seed + hash(2));
  uint64_t y = ((uint64_t) hash(seed + hash(3)) << 32) + hash(seed + hash(4));
  R u = x / 18446744073709551616.0;
  R v = y / 18446744073709551616.0;
  return u + I * v;
}

struct Histogram
{
  R *data;
  N width;
  N height;
  uint8_t *pixels;
};

void histogram_init(struct Histogram *histogram, N width, N height)
{
  histogram->data = calloc(1, sizeof(*histogram->data) * width * height * 4);
  histogram->pixels = malloc(sizeof(*histogram->pixels) * width * height * 3);
  histogram->width = width;
  histogram->height = height;
}

void histogram_clear(struct Histogram *histogram)
{
  free(histogram->data);
  free(histogram->pixels);
}

void histogram_tonemap(struct Histogram *histogram)
{
  R total = 0;
  for (N y = 0; y < histogram->height; ++y)
  {
    for (N x = 0; x < histogram->width; ++x)
    {
      N k = ((y * histogram->width) + x) << 2;
      total += histogram->data[k];
    }
  }
  R s = histogram->width * histogram->height / total;
  #pragma omp parallel for
  for (N y = 0; y < histogram->height; ++y)
  {
    for (N x = 0; x < histogram->width; ++x)
    {
      N k = ((y * histogram->width) + x) << 2;
      R a = histogram->data[k + 0];
      if (a > 0)
      {
        R v = sqrt(sqrt(log(1 + log(1 + a * s))));
        for (N c = 0; c < 3; ++c)
        {
          R u = histogram->data[k + c + 1] / a;
          R g = v * u * 200;
          N o = fminf(fmaxf(g, 0), 255);
          N j = ((y * histogram->width) + x) * 3 + c;
          histogram->pixels[j] = o;
        }
      } else {
        Z k = (y * histogram->width + x) * 3;
        histogram->pixels[k + 0] = 0;
        histogram->pixels[k + 1] = 0;
        histogram->pixels[k + 2] = 0;
      }
    }
  }
}

N plot_iterate(struct Histogram *histogram, struct Context *context, R weight, C z, C dz, N offset, N period)
{
  N total = 0;
  R x = creal(z);
  R y = cimag(z);
  R x0 = context->lo_x;
  R y0 = context->lo_y;
  R x1 = context->hi_x;
  R y1 = context->hi_y;
  R d = y1 - y0;
  if (x0 <= x && x <= x1 && y0 <= y && y <= y1)
  {
    C pixel = ((z - context->view.z) / d) * histogram->height + (histogram->width + I * histogram->height) / 2; // + jitter(hashc(z));
    Z i = creal(pixel);
    Z j = cimag(pixel);
    if (0 <= i && i < (Z) histogram->width && 0 <= j && j < (Z) histogram->height)
    {
      N k = (j * histogram->width + i) << 2;
      // colouring
      R r0, g0, b0, r1, g1, b1, r, g, b, v;
      hsv2rgb(offset / (R) period, cnorm(dz), 1, &r0, &g0, &b0);
      hsv2rgb(carg(dz) / (2 * M_PI), cnorm(dz), 1, &r1, &g1, &b1);
      r = (r0 + r1) * 0.5 * weight / period;
      g = (g0 + g1) * 0.5 * weight / period;
      b = (b0 + b1) * 0.5 * weight / period;
      v = weight / period;
      // accumuate
      #pragma omp atomic
      histogram->data[k + 0] += v;
      #pragma omp atomic
      histogram->data[k + 1] += r;
      #pragma omp atomic
      histogram->data[k + 2] += g;
      #pragma omp atomic
      histogram->data[k + 3] += b;
      total += 1;
    }
  }
  return total;
}

N plot_iterates(struct Histogram *histogram, struct Context *context, R weight, N base_period, N offset, N period, C c, C z, C dz)
{
  N total = 0;
  for (N i = 0; i < period; ++i)
  {
    if ((i % base_period) == offset)
    {
      total += plot_iterate(histogram, context, weight, z, dz, i, period);
    }
    z = z * z + c;
  }
  return total;
}

#define sqrt2 1.4142135623731
N render_ball_at_flat(N count, struct Histogram *histogram, struct Context *context, R weight, struct Ball c0)
{
  N total = 0;
//  #pragma omp parallel for collapse(2)
  for (N y = 0; y < count; ++y)
  for (N x = 0; x < count; ++x)
  {
    C pixel = (x + I * y) + jitter(hash4(x, y, count, c0.n));
    C c = c0.z + c0.r * 2 * (pixel / count - (0.5 + 0.5 * I));
    if (tree_lookup(&context->tree, c) > c0.n)
    {
      // plotted by a higher period
      continue;
    }
    C z = 0;
    R mz2 = 1.0 / 0.0;
    for (N i = 1; i < context->maximum_period; ++i)
    {
      z = z * z + c;
      R z2 = cnorm(z);
      if (! (z2 < 65536))
      {
        // exterior
        break;
      }
      if (z2 < mz2)
      {
        if (8 * z2 < mz2)
        {
          C w = z, dw;
          if (find_attractor(&w, c, i, &dw))
          {
            if (cnorm(dw) <= 1)
            {
              // interior
              N m = plot_iterates(histogram, context, weight, 1, 0, i, c, w, dw);
              #pragma omp atomic
              total += m;
              break;
            }
          }
        }
        z2 = mz2;
      }
    }
    // no conclusion reached
  }
  return total;
}

N render_ball_at_recursive(N depth, struct Histogram *histogram, struct Context *context, R weight, struct Ball c0, N offset, N period, C z1, struct Box b)
{
  R distance_threshold = (4 * sqrt2) * (c0.r * 2.0 / ((N) 1 << b.k));
  if (depth == 0)
  {
    C pixel = (b.i + I * b.j) + jitter(hash4(b.i, b.j, b.k, c0.n));
    C c = c0.z + c0.r * 2 * (pixel / ((N) 1 << b.k) - (0.5 + 0.5 * I));
    if (tree_lookup(&context->tree, c) > c0.n)
    {
      // plotted by a higher period
      return 0;
    }
    if (period)
    {
      // far interior
      C dz;
      if (find_attractor(&z1, c, period, &dz))
      {
        return plot_iterates(histogram, context, weight, /*c0.n*/ 1, /*offset*/ 0, period, c, z1, dz);
      }
      else
      {
        // should never happen
        return 0;
      }
    }
    else
    {
      // near boundary
      C z = 0;
      R mz2 = 1.0 / 0.0;
      for (N i = 1; i < context->maximum_period; ++i)
      {
        z = z * z + c;
        R z2 = cnorm(z);
        if (! (z2 < 65536))
        {
          // exterior
          return 0;
        }
        if (z2 < mz2)
        {
          // inside atom domain
          if (8 * z2 < mz2)
          {
            // far inside atom domain
            C w = z, dw;
            if (find_attractor(&w, c, i, &dw))
            {
              if (cnorm(dw) < 1)
              {
                // interior
                return plot_iterates(histogram, context, weight, 1, 0, i, c, w, dw);
              }
            }
          }
          mz2 = z2;
        }
      }
      // maximum iteration count exceeded
      return 0;
    }
  }
  else
  {
    C pixel = (b.i + I * b.j);
    C c  = c0.z + c0.r * 2 * ((pixel + (0.5 + 0.5 * I)) / ((N) 1 << b.k) - (0.5 + 0.5 * I));
    R r  = c0.r * 2 / ((N) (1 << b.k));
    C lo = c0.z + c0.r * 2 * ( pixel                    / ((N) 1 << b.k) - (0.5 + 0.5 * I));
    C hi = c0.z + c0.r * 2 * ((pixel + (1 + 1 * I))     / ((N) 1 << b.k) - (0.5 + 0.5 * I));
    if (tree_lookup_region(&context->tree, creal(lo), cimag(lo), creal(hi), cimag(hi)) > c0.n)
    {
      // already plotted by a higher period
      return 0;
    }
    struct Ball cb = { c, r * sqrt2, period };

    if (period)
    {
      if (! ball_check_iterates(context->view, cb))
      {
        // nothing falls in view
        return 0;
      }

      // far interior
      C dz;
      if (find_attractor(&z1, c, period, &dz))
      {
        N total = 0;
        total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, period, z1, (struct Box){ b.k + 1, (b.j << 1) + 0, (b.i << 1) + 0 });
        total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, period, z1, (struct Box){ b.k + 1, (b.j << 1) + 0, (b.i << 1) + 1 });
        total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, period, z1, (struct Box){ b.k + 1, (b.j << 1) + 1, (b.i << 1) + 0 });
        total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, period, z1, (struct Box){ b.k + 1, (b.j << 1) + 1, (b.i << 1) + 1 });
        return total;
      }
      else
      {
        // FIXME should never happen
        N total = 0;
        total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 0, (b.i << 1) + 0 });
        total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 0, (b.i << 1) + 1 });
        total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 1, (b.i << 1) + 0 });
        total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 1, (b.i << 1) + 1 });
        return total;
      }
    }

    else
    {
      struct Ball zb = zero;
      C dc = 0;
      R mz2 = 1.0 / 0.0;
      bool recurse = false;
      for (N i = 1; i < context->maximum_period; ++i)
      {
        dc = 2 * zb.z * dc + 1;
        zb = z_squared_plus_c(zb, cb);
        if ((! recurse) && ball_intersects(context->view, zb))
        {
          recurse = true;
        }
        R z2 = cnorm(zb.z);
        if (! (z2 < 65536))
        {
          // exterior
          R exterior_distance = log(z2) * sqrt(z2 / cnorm(dc));
          if (exterior_distance > distance_threshold)
          {
            // far exterior
            return 0;
          }
          else
          {
            // near exterior
            N total = 0;
            if (recurse)
            {
              total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 0, (b.i << 1) + 0 });
              total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 0, (b.i << 1) + 1 });
              total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 1, (b.i << 1) + 0 });
              total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 1, (b.i << 1) + 1 });
            }
            return total;
          }
        }
        if (z2 < mz2)
        {
          // inside atom domain
          if (8 * z2 < mz2)
          {
            // far inside atom domain
            C w = zb.z, dw;
            if (find_attractor(&w, c, i, &dw))
            {
              if (cnorm(dw) < 1)
              {
                if (! recurse)
                {
//                  return 0;
                }
                // interior
                if (find_interior_distance(w, c, i) > distance_threshold)
                {
                  // far interior
                  N total = 0;
                  total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, i, w, (struct Box){ b.k + 1, (b.j << 1) + 0, (b.i << 1) + 0 });
                  total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, i, w, (struct Box){ b.k + 1, (b.j << 1) + 0, (b.i << 1) + 1 });
                  total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, i, w, (struct Box){ b.k + 1, (b.j << 1) + 1, (b.i << 1) + 0 });
                  total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, i, w, (struct Box){ b.k + 1, (b.j << 1) + 1, (b.i << 1) + 1 });
                  return total;
                }
                else
                {
                  // near interior
                  N total = 0;
                  total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 0, (b.i << 1) + 0 });
                  total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 0, (b.i << 1) + 1 });
                  total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 1, (b.i << 1) + 0 });
                  total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 1, (b.i << 1) + 1 });
                  return total;
                }
              }
            }
          }
          mz2 = z2;
        }
      }
      // maximum iterations reached
      N total = 0;
      if (recurse)
      {
        total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 0, (b.i << 1) + 0 });
        total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 0, (b.i << 1) + 1 });
        total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 1, (b.i << 1) + 0 });
        total += render_ball_at_recursive(depth - 1, histogram, context, weight, c0, offset, 0, 0, (struct Box){ b.k + 1, (b.j << 1) + 1, (b.i << 1) + 1 });
      }
      return total;
    }
  }
}

N render_ball_at(struct Histogram *histogram, struct Context *context, R samples, struct Ball c, struct Ball z)
{
  R source_area = 4 * c.r * c.r;
  R total_area = 16;
  R output_area = 4 * z.r * z.r;
  R view_area = 4 * context->view.r * context->view.r;
  R samples_per_view = histogram->width * histogram->height * samples;
  R samples_per_source = samples_per_view / view_area * source_area / total_area;
  R desired_samples_per_output = samples_per_view / view_area * output_area;
  N count = fmax(0, ceil(sqrt(desired_samples_per_output)));
  R samples_per_output = count * count;
  R weight = samples_per_source / samples_per_output;
if (count > 1)
{
#pragma omp critical
fprintf(stderr, "%4d\t%.5g\t%.18f\t%.18f\t%.5e\t%"PRN "\t%"PRN "\n", omp_get_thread_num(), log2(count), creal(c.z), cimag(c.z), c.r, c.n, z.n);
}
  if (c.n == 1) return 0;
  if (count <= 0) return 0;
  return render_ball_at_flat(count, histogram, context, weight, c);
//  return render_ball_at_recursive(sampling_depth, histogram, context, weight, c, z.n, 0, 0, (struct Box){0,0,0});
}

N render_ball(struct Histogram *histogram, struct Context *context, R samples, struct Ball c)
{
  R total = 0;
  C z = 0;
  C dc = 0;
  for (N i = 0; i < c.n; ++i)
  {
    dc = 2 * z * dc + c.r;
    z = z * z + c.z;
//    z = z_squared_plus_c(z, c); // FIXME should just use dz/dc for size
    struct Ball zb = { z, sqrt(cnorm(dc)), i };
    if (ball_intersects(context->view, zb))
    {
      total += render_ball_at(histogram, context, samples, c, zb);
      break;
    }
  }
  return total;
}

N render(struct Histogram *histogram, struct Context *context, R samples)
{
  N total = 0;
//  R last_output = 0;
//  N sequence = 0;
  #pragma omp parallel for
  for (N i = 0; i < context->list.count; ++i)
  {
    N plotted = render_ball(histogram, context, samples, context->list.data[i]);
    #pragma omp atomic
    total += plotted;
/*
    if (total >= last_output + 1024)
    {
      histogram_tonemap(histogram);
      char filename[100];
      snprintf(filename, sizeof(filename), "pnmtopng -interlace -force -compression=9 > anti-%"PRN".png", sequence++);
      FILE *out = popen(filename, "w");
      fprintf(out, "P6\n%"PRN " %"PRN "\n255\n", histogram->width, histogram->height);
      fwrite(histogram->pixels, sizeof(*histogram->pixels) * histogram->width * histogram->height * 3, 1, out);
      pclose(out);
      last_output = total;
    }
*/
  }
  return total;
}

int main(int argc, char **argv)
{
  N width = 640;
  N height = 360;
  R samples = 4;
  C c = -1 + I * 0.564;
//  R r = 0.05;
//  C c = 0.56208345369277852973 + I * 0.00000016540178571429;
  R r = atof(argv[2]);//0.00000002;
  // initialize search context
  struct Context context;
  context_init(&context);
  context.maximum_depth = argc > 1 ? atoi(argv[1]) : 16;
  context.maximum_period = (N) 1 << 16;
  context.thread_limit = 20;
  context.lo_x = creal(c) - r * width / height;
  context.hi_x = creal(c) + r * width / height;
  context.lo_y = cimag(c) - r;
  context.hi_y = cimag(c) + r;
  context.view = (struct Ball){ c, hypot(r * width / height, r), 0 };

  // search for minis
  printf("searching...\n");
  fflush(stdout);
  context_depth_first(&context, (struct Box){ 0, 0, 0 });

  // output minis
  printf("%"PRN " %"PRN " %"PRN "\n", context.total_count, context.intersected_count, context.contained_count);
  fflush(stdout);

  FILE *out = fopen("anti.minis", "wb");
  fwrite(context.list.data, sizeof(*context.list.data) * context.list.count, 1, out);
  fclose(out);
#if 0
  for (N i = 0; i < context.list.count; ++i)
  {
    struct Ball b = context.list.data[i];
    fprintf(stderr, "%.18f %.18f %.5e %"PRN "\n", creal(b.z), cimag(b.z), b.r, b.n);
  }
#endif

  // accumulate
  struct Histogram histogram;
  histogram_init(&histogram, width, height);
  printf("rendering...\n");
  fflush(stdout);
  N total = render(&histogram, &context, samples);
  printf("%"PRN "\n", total);
  printf("tonemapping...\n");
  fflush(stdout);
  histogram_tonemap(&histogram);
  out = popen("pnmtopng -interlace -force -compression=9 > anti.png", "w");
  fprintf(out, "P6\n%"PRN " %"PRN "\n255\n", histogram.width, histogram.height);
  fwrite(histogram.pixels, sizeof(*histogram.pixels) * histogram.width * histogram.height * 3, 1, out);
  pclose(out);
  histogram_clear(&histogram);
  context_clear(&context);
  printf("done.\n");
  fflush(stdout);
  return 0;
}
