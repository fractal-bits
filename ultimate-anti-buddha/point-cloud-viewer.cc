#include <time.h>
#include <SDL.h>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>

using vec3 = glm::vec3;
using mat4 = glm::mat4;

GLuint shader(const char *vert_filename, const char *frag_filename)
{
  char buffer[1024];
  FILE *vert_file = fopen(vert_filename, "rb");
  fseek(vert_file, 0, SEEK_END);
  long vert_bytes = ftell(vert_file);
  rewind(vert_file);
  char *vert_src = (char *) malloc(vert_bytes + 1);
  fread(vert_src, vert_bytes, 1, vert_file);
  fclose(vert_file);
  vert_src[vert_bytes] = 0;
  FILE *frag_file = fopen(frag_filename, "rb");
  fseek(frag_file, 0, SEEK_END);
  long frag_bytes = ftell(frag_file);
  rewind(frag_file);
  char *frag_src = (char *) malloc(frag_bytes + 1);
  fread(frag_src, frag_bytes, 1, frag_file);
  fclose(frag_file);
  frag_src[frag_bytes] = 0;
  GLuint vert = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vert, 1, &vert_src, 0);
  glCompileShader(vert);
  glGetShaderInfoLog(vert, sizeof(buffer), 0, buffer);
  if (buffer[0]) printf("%s\n", buffer);
  GLuint frag = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(frag, 1, &frag_src, 0);
  glCompileShader(frag);
  glGetShaderInfoLog(frag, sizeof(buffer), 0, buffer);
  if (buffer[0]) printf("%s\n", buffer);
  GLuint program = glCreateProgram();
  glAttachShader(program, vert);
  glAttachShader(program, frag);
  glLinkProgram(program);
  glGetProgramInfoLog(program, sizeof(buffer), 0, buffer);
  if (buffer[0]) printf("%s\n", buffer);
  free(vert_src);
  free(frag_src);
  return program;
}

int main(int argc, char **argv)
{
  int record = 1;
  int stereo = 1;
  int width = 1920;
  int height = 1080;
  SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG); // Always required on Mac
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
  SDL_Window *window = SDL_CreateWindow("4D Point Cloud Viewer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
  SDL_GLContext gl_context = SDL_GL_CreateContext(window);
  SDL_GL_MakeCurrent(window, gl_context);
  SDL_GL_SetSwapInterval(1);
  glewInit();

  GLuint tonemap_program = shader("tonemap.vert", "tonemap.frag");
  GLuint tonemap_vao;
  glGenVertexArrays(1, &tonemap_vao);
  glBindVertexArray(tonemap_vao);
  float tonemap_data[] = { -1, -1, -1, 1, 1, -1, 1, 1 };
  GLuint tonemap_vbo;
  glGenBuffers(1, &tonemap_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, tonemap_vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(tonemap_data), tonemap_data, GL_STATIC_DRAW);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(1);
  GLuint texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexImage2D(GL_TEXTURE_2D, 0, stereo ? GL_RG32F : GL_R32F, width, height, 0, stereo ? GL_RG : GL_RED, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, 0);

  GLuint fbo;
  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);
  GLuint buffers[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, buffers);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  GLuint pointcloud_program = shader("pointcloud.vert", "pointcloud.frag");
  glUseProgram(pointcloud_program);
  float ratio  = width / (float) height;
  float focallength = 5;
  float near = focallength - 1;
  float far = focallength + 1;
  float wd2 = near / focallength;
  float ndfl = near / focallength;
  float eyesep = focallength / 30;
  mat4 projection3d[3];
  for (int eye = -1; eye <= 1; eye += 2)
  {
    projection3d[(eye + 1) / 2]
      = glm::frustum
          ( - ratio * wd2 - eye * 0.5f * eyesep * ndfl
          ,   ratio * wd2 - eye * 0.5f * eyesep * ndfl
          ,  wd2
          , -wd2
          , near
          , far
          ) *
        glm::lookAt
          ( vec3(eye * eyesep / 2, 0.0f, focallength)
          , vec3(eye * eyesep / 2, 0.0f, 1.0f)
          , vec3(0.0f, 1.0f, 0.0f)
          );
  }
  projection3d[2]
      = glm::frustum
          ( - ratio * wd2
          ,   ratio * wd2
          ,  wd2
          , -wd2
          , near
          , far
          ) *
        glm::lookAt
          ( vec3(0.0f, 0.0f, focallength)
          , vec3(0.0f, 0.0f, 1.0f)
          , vec3(0.0f, 1.0f, 0.0f)
          );
  glUniform1i(13, stereo);
  if (stereo)
  {
    glUniformMatrix4fv(5, 2, GL_FALSE, &projection3d[0][0][0]);
  }
  else
  {
    glUniformMatrix4fv(5, 1, GL_FALSE, &projection3d[2][0][0]);
  }
  GLuint pointcloud_vao;
  glGenVertexArrays(1, &pointcloud_vao);
  glBindVertexArray(pointcloud_vao);
  FILE *position_file = fopen("position.f4", "rb");
  fseek(position_file, 0, SEEK_END);
  long int position_bytes = ftell(position_file);
  rewind(position_file);
  FILE *colour_file = fopen("colour.f1", "rb");
  fseek(colour_file, 0, SEEK_END);
  long int colour_bytes = ftell(colour_file);
  rewind(colour_file);
  char *data = (char *) malloc(position_bytes + colour_bytes);
  fread(data, position_bytes, 1, position_file);
  fread(data + position_bytes, colour_bytes, 1, colour_file);
  fclose(position_file);
  fclose(colour_file);
  GLuint pointcloud_vbo;
  glGenBuffers(1, &pointcloud_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, pointcloud_vbo);
  glBufferData(GL_ARRAY_BUFFER, position_bytes + colour_bytes, data, GL_STATIC_DRAW);
  free(data);
  glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, (void *) ((char *) 0 +  position_bytes));
  glEnableVertexAttribArray(2);

  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_ONE, GL_ONE);
  glClearColor(0, 0, 0, 1);

  mat4 scale(0.5f);
  mat4 m0(1.0f);
  mat4 mconj(1.0f);
  mat4 rotation(1.0f);
  float duration = 2048;
  float bpm = 155;
  float fps = 60;
  int beats = 8;
  float beat = 0;
  float increment = bpm / 60 / fps / beats;
  int running = 1;
  time_t start = time(0);
  long int seed = start;
  srand(seed);
  int frames = 0;

  unsigned char *ppm;
  FILE *rec;
  if (record)
  {
    ppm = (unsigned char *) malloc(width * height * 3);
    char command[1024];
    snprintf(command, sizeof(command), "ffmpeg -r %g -framerate %g -i - -i audio.caf -shortest -b:a 384k -vf vflip -pix_fmt yuv420p -profile:v high -level:v 4.2 -crf:v 16 -bf:v 2 -g:v %d -movflags +faststart point-cloud-%ld.mp4", fps, fps, (int) fps / 2, seed);
    rec = popen(command, "w");
  }

  while (running && (record ? beat * beats < duration : 1))
  {
    SDL_Event e;
    while (SDL_PollEvent(&e))
    {
      switch (e.type)
      {
        case SDL_QUIT:
          running = 0;
          break;
      }
    }

    if (running)
    {
      float phase = beat - floor(beat);
      float t = 2 * M_PI * phase / 6.0;
      float c = cos(t);
      float s = sin(t);
      mat4 minc(c, -s, 0.0f, 0.0f, s,  c, 0.0f, 0.0f, 0.0f,  0.0f, c, -s, 0.0f,  0.0f, s,  c);
      if (floor(beat) != floor(beat - increment))
      {
        m0 = rotation;
        mconj = mat4(1.0);
        for (int u = 0; u < 4; ++u)
        {
          for (int v = u + 1; v < 4; ++v)
          {
            mat4 uv = mat4(1.0f);
            float t = 2 * M_PI * (rand() / (float) RAND_MAX);
            float c = cos(t);
            float s = sin(t);
            uv[u][u] = c;
            uv[u][v] = -s;
            uv[v][u] = s;
            uv[v][v] = c;
            mconj = mconj * uv;
          }
        }
      }
      rotation = transpose(mconj) * minc * mconj * m0;
      beat += increment;
      mat4 projection4d = scale * rotation;

      glBindFramebuffer(GL_FRAMEBUFFER, fbo);
      glBindVertexArray(pointcloud_vao);
      glUseProgram(pointcloud_program);
      glUniformMatrix4fv(1, 1, GL_FALSE, &projection4d[0][0]);
      glClear(GL_COLOR_BUFFER_BIT);
      glDrawArraysInstanced(GL_POINTS, 0, position_bytes / 16, stereo ? 2 : 1);

      glBindFramebuffer(GL_FRAMEBUFFER, 0);
//    glGenerateMipmap(GL_TEXTURE_2D);
      glBindVertexArray(tonemap_vao);
      glUseProgram(tonemap_program);
      glUniform1f(2, 16);
      glClear(GL_COLOR_BUFFER_BIT);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

      if (record)
      {
        glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, ppm);
        fprintf(rec, "P6\n%d %d\n255\n", width, height);
        fwrite(ppm, width * height * 3, 1, rec);
      }
      SDL_GL_SwapWindow(window);

      frames++;
    }
  }
  if (record)
  {
    pclose(rec);
  }
  time_t stop = time(0);
  printf("FPS: %g\n", frames / (double) (stop - start));
  return 0;
}
