set terminal pngcairo enhanced size 1280,720
set output "anti-quality.png"
set title "Percentage of boxes with exactly 1 hit"
set xlabel "Box size"
set ylabel "%"
set key inside top left Left reverse
set xtics 1
set grid
plot \
  for [i=0:30] sprintf("anti-%d.dat", i) u 1:(100 * $2) w lp s cs t sprintf("%s%d", i == 0 ? "Quality " : "", i) lc i dt i, \
  for [i=0:30] sprintf("anti-%d.dat", i) u 1:(100 * $2) w p t "" lc i
