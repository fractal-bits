#version 330 core

layout(location = 1) in vec2 v_position;

void main(void)
{
  gl_Position = vec4(v_position, 0, 1);
}
