#include <assert.h>
#include <complex.h>
#include <math.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <gsl/gsl_rng.h>
#include <mandelbrot-numerics.h>
#include <omp.h>

const uint64_t threads = 16;

// memory

uint64_t mem_allocated = 0;
uint64_t mem_limit;

void *mem_alloc(size_t bytes)
{
  if (mem_allocated + bytes > mem_limit)
  {
    return 0;
  }
  void *ptr = malloc(bytes);
  if (ptr)
  {
    mem_allocated += bytes;
  }
  return ptr;
}

void mem_free(void *ptr, size_t bytes)
{
  assert(ptr);
  assert(mem_allocated >= bytes);
  mem_allocated -= bytes;
  free(ptr);
}

double cnorm(double _Complex z)
{
  double x = creal(z);
  double y = cimag(z);
  return x * x + y * y;
}

double cabs_fast(double _Complex z)
{
  return sqrt(cnorm(z));
}

typedef struct
{
  double _Complex x;
  double r;
} Ball;

double ball_inf(Ball a)
{
  return cabs_fast(a.x) - a.r;
}

double ball_sup(Ball a)
{
  return cabs_fast(a.x) + a.r;
}

Ball ball_add(Ball a, Ball b)
{
  return (Ball){ a.x + b.x, a.r + b.r };
}

Ball ball_sub(Ball a, Ball b)
{
  return (Ball){ a.x - b.x, a.r + b.r };
}

Ball ball_mul(Ball a, Ball b)
{
  return (Ball){ a.x * b.x, a.r * b.r + a.r * cabs_fast(b.x) + cabs_fast(a.x) * b.r };
}

Ball ball_rmul(double a, Ball b)
{
  return (Ball){ a * b.x, a * b.r };
}

Ball ball_sqr(Ball a)
{
  return (Ball){ a.x * a.x, (2 * cabs_fast(a.x) + a.r) * a.r };
}

Ball ball_recip(Ball a)
{
  double x = cabs_fast(a.x);
  return (Ball){ 1 / a.x, a.r / (x * fmax(0, x - a.r)) };
}

Ball ball_div(Ball a, Ball b)
{
  return ball_mul(a, ball_recip(b));
}

typedef struct
{
  uint16_t i;
  uint16_t j;
} Quad16;

typedef struct
{
  uint32_t i;
  uint32_t j;
} Quad32;

typedef struct
{
  uint64_t i;
  uint64_t j;
} Quad64;

/*
typedef struct
{
  uint64_t level;
  uint64_t alloc;
  uint64_t count;
  union
  {
    Quad16 *quads16;
    Quad32 *quads32;
    Quad64 *quads64;
  } u;
} Quads;
*/

typedef bool (*QuadFilter)(void *context, Quad64 q);
typedef void (*QuadMap)(void *context, Quad64 q);

typedef struct
{
  uint64_t *bits;
  uint64_t words_allocated;
  uint64_t bits_used;
} Bits;

typedef struct
{
  uint64_t *bits;
  uint64_t word;
  uint64_t bit;
} BitsCursor;

typedef struct
{
  uint64_t level;
  Bits bits;
  BitsCursor cursor;
} BitsTree;

typedef struct
{
  uint64_t layers;
  uint64_t width;
  uint64_t height;
  double _Complex z;
  double z_radius;
  double _Complex z_origin;
  double z_scale;
  uint64_t *hist;
} Hist;

typedef struct
{
  Hist *hist;
//  Quads *quads;
  BitsTree *tree;
  double de_limit;
  uint64_t mu_limit;
  double c_scale;
  double _Complex c_origin;
  double _Complex z;
  double z_radius;
  uint64_t samples_target;
  uint64_t samples;
  double   samples_per_quad;
  uint64_t iterations;
  uint64_t hit_samples;
  uint64_t hit_threshold;
  uint64_t hit_mu_threshold;
  double hit_mu_scale;
} Context;

bool bit_read(BitsCursor *cursor)
{
  bool ret = cursor->bits[cursor->word] & (1ull << cursor->bit);
  ++cursor->bit;
  if (cursor->bit == 64)
  {
    cursor->bit = 0;
    ++cursor->word;
  }
  return ret;
}

void bit_write(BitsCursor *cursor, bool bit)
{
  if (bit)
  {
    cursor->bits[cursor->word] |= (1ull << cursor->bit);
  }
  else
  {
    cursor->bits[cursor->word] &= ~(1ull << cursor->bit);
  }
  ++cursor->bit;
  if (cursor->bit == 64)
  {
    cursor->bit = 0;
    ++cursor->word;
  }
}

void bitstree_rewind(BitsTree *t)
{
  t->cursor.bits = t->bits.bits;
  t->cursor.bit = 0;
  t->cursor.word = 0;
}

bool bitstree_init(BitsTree *t, uint64_t level, uint64_t count)
{
  t->bits.words_allocated = (count + 63) / 64;
  uint64_t bytes = sizeof(*t->bits.bits) * t->bits.words_allocated;
  t->bits.bits = mem_alloc(bytes);
  if (! t->bits.bits)
  {
    return false;
  }
  memset(t->bits.bits, 0, bytes);
  t->bits.bits_used = 0;
  t->level = level;
  bitstree_rewind(t);
  return true;
}

void bitstree_clear(BitsTree *t)
{
  uint64_t bytes = sizeof(*t->bits.bits) * t->bits.words_allocated;
  mem_free(t->bits.bits, bytes);
}

void bitstree_refine_rec(BitsCursor *out, BitsCursor *in, uint64_t bits_used, uint64_t level, Quad64 quad, QuadFilter filter, void *context)
{
  bool bit = bit_read(in);
  if (bit)
  {
    if (level == 0)
    {
      uint64_t pc = 6400 * in->word / bits_used;
      uint64_t pc1 = 6400 * (in->word - 1) / bits_used;
      if (pc != pc1)
      {
        fprintf(stderr, "%3ld\r", pc);
      }

      bool bits[2][2];
      for (uint64_t i = 0; i < 2; ++i)
      for (uint64_t j = 0; j < 2; ++j)
      {
        bits[i][j] = filter(context, (Quad64){ (quad.i << 1) + i, (quad.j << 1) + j });
      }
      if (bits[0][0] || bits[0][1] || bits[1][0] || bits[1][1])
      {
        bit_write(out, true);
        bit_write(out, bits[0][0]);
        bit_write(out, bits[0][1]);
        bit_write(out, bits[1][0]);
        bit_write(out, bits[1][1]);
      }
      else
      {
        bit_write(out, false);
      }
    }
    else
    {
      bit_write(out, true);
      for (uint64_t i = 0; i < 2; ++i)
      for (uint64_t j = 0; j < 2; ++j)
      {
        bitstree_refine_rec(out, in, bits_used, level - 1, (Quad64){ (quad.i << 1) + i, (quad.j << 1) + j }, filter, context);
      }
    }
  }
  else
  {
    bit_write(out, false);
  }
}

bool bitstree_refine(BitsTree *out, BitsTree *in, QuadFilter filter, void *context)
{
  if (! bitstree_init(out, in->level + 1, in->bits.bits_used * 5))
  {
    return false;
  }
  bitstree_rewind(in);
  bitstree_rewind(out);
  bitstree_refine_rec(&out->cursor, &in->cursor, in->bits.bits_used, in->level, (Quad64){0, 0}, filter, context);
  out->bits.bits_used = out->cursor.word * 64 + out->cursor.bit;
  return true;
}

void bitstree_map_rec(BitsCursor *in, uint64_t bits_used, uint64_t level, Quad64 quad, QuadMap map, void *context)
{
  bool bit = bit_read(in);
  if (bit)
  {
    if (level == 0)
    {
      uint64_t pc = 6400 * in->word / bits_used;
      uint64_t pc1 = 6400 * (in->word - 1) / bits_used;
      if (pc != pc1)
      {
        fprintf(stderr, "%3ld\r", pc);
      }

      map(context, quad);
    }
    else
    {
      for (uint64_t i = 0; i < 2; ++i)
      for (uint64_t j = 0; j < 2; ++j)
      {
        bitstree_map_rec(in, bits_used, level - 1, (Quad64){ (quad.i << 1) + i, (quad.j << 1) + j }, map, context);
      }
    }
  }
}

void bitstree_map(BitsTree *in, QuadMap filter, void *context)
{
  bitstree_rewind(in);
  bitstree_map_rec(&in->cursor, in->bits.bits_used, in->level, (Quad64){0, 0}, filter, context);
}

void bitstree_parmap_rec(BitsCursor *in, uint64_t thread, uint64_t bits_used, uint64_t level, Quad64 quad, QuadMap map, void *context)
{
  bool bit = bit_read(in);
  if (bit)
  {
    if (level == 0)
    {
      if (thread == 0)
      {
        uint64_t pc = 6400 * in->word / bits_used;
        uint64_t pc1 = 6400 * (in->word - 1) / bits_used;
        if (pc != pc1)
        {
          fprintf(stderr, "%3ld\r", pc);
        }
      }

      uint64_t which = in->bit % threads;
      if (which == thread)
      {
        map(context, quad);
      }
    }
    else
    {
      for (uint64_t i = 0; i < 2; ++i)
      for (uint64_t j = 0; j < 2; ++j)
      {
        bitstree_parmap_rec(in, thread, bits_used, level - 1, (Quad64){ (quad.i << 1) + i, (quad.j << 1) + j }, map, context);
      }
    }
  }
}

void bitstree_parmap(BitsTree *in, QuadMap filter, void *context)
{
  bitstree_rewind(in);
  #pragma omp parallel for schedule(static, 1)
  for (uint64_t thread = 0; thread < threads; ++thread)
  {
    BitsCursor cursor = in->cursor;
    bitstree_parmap_rec(&cursor, thread, in->bits.bits_used, in->level, (Quad64){0, 0}, filter, context);
  }
}

bool bitstree_save(const char *filename, BitsTree *t)
{
  FILE *f = fopen(filename, "wb");
  if (f)
  {
    if (1 != fwrite(&t->level, sizeof(t->level), 1, f))
    {
      fclose(f);
      return false;
    }
    if (1 != fwrite(&t->bits.bits_used, sizeof(t->bits.bits_used), 1, f))
    {
      fclose(f);
      return false;
    }
    uint64_t words_used = (t->bits.bits_used + 63) / 64;
    if (1 != fwrite(t->bits.bits, sizeof(*t->bits.bits) * words_used, 1, f))
    {
      fclose(f);
      return false;
    }
    fclose(f);
    return true;
  }
  return false;
}

bool bitstree_load(const char *filename, BitsTree *t)
{
  FILE *f = fopen(filename, "rb");
  if (f)
  {
    uint64_t level;
    if (1 == fread(&level, sizeof(level), 1, f))
    {
      uint64_t bits_used;
      if (1 == fread(&bits_used, sizeof(bits_used), 1, f))
      {
        uint64_t words_used = (bits_used + 63) / 64;
        if (bitstree_init(t, level, bits_used))
        {
          if (1 == fread(t->bits.bits, sizeof(*t->bits.bits) * words_used, 1, f))
          {
            fclose(f);
            t->level = level;
            t->bits.bits_used = bits_used;
            return true;
          }
          bitstree_clear(t);
        }
      }
    }
    fclose(f);
  }
  return false;
}

// exit

volatile bool running = true;

void signal_handler(int sig)
{
  (void) sig;
  running = false;
}

// util

uint64_t ilog2(uint64_t x)
{
  if (x == 0)
  {
    return -1;
  }
  for (uint64_t r = 0, n = 1; n; ++r, n <<= 1)
  {
    if (n > x)
    {
      return r - 1;
    }
  }
  return -1;
}

#if 0

bool quads_init(Quads *q, uint64_t level, uint64_t alloc)
{
  if (level <= 16)
  {
    if ((q->u.quads16 = mem_alloc(sizeof(*q->u.quads16) * alloc)))
    {
      q->level = level;
      q->alloc = alloc;
      q->count = 0;
      return true;
    }
  }
  else if (level <= 32)
  {
    if ((q->u.quads32 = mem_alloc(sizeof(*q->u.quads32) * alloc)))
    {
      q->level = level;
      q->alloc = alloc;
      q->count = 0;
      return true;
    }
  }
  else if (level <= 64)
  {
    if ((q->u.quads64 = mem_alloc(sizeof(*q->u.quads64) * alloc)))
    {
      q->level = level;
      q->alloc = alloc;
      q->count = 0;
      return true;
    }
  }
  return false;
}

void quads_clear(Quads *q)
{
  if (q->level <= 16)
  {
    if (q->u.quads16)
    {
      mem_free(q->u.quads16, sizeof(*q->u.quads16) * q->alloc);
    }
    q->u.quads16 = 0;
  }
  else if (q->level <= 32)
  {
    if (q->u.quads32)
    {
      mem_free(q->u.quads32, sizeof(*q->u.quads32) * q->alloc);
    }
    q->u.quads32 = 0;
  }
  else if (q->level <= 64)
  {
    if (q->u.quads64)
    {
      mem_free(q->u.quads64, sizeof(*q->u.quads64) * q->alloc);
    }
    q->u.quads64 = 0;
  }
  else
  {
    abort();
  }
}

Quad64 quads_get(const Quads *src, uint64_t q)
{
  if (src->level <= 16)
  {
    Quad16 i = src->u.quads16[q];
    return (Quad64){ i.i, i.j };
  }
  else if (src->level <= 32)
  {
    Quad32 i = src->u.quads32[q];
    return (Quad64){ i.i, i.j };
  }
  else if (src->level <= 64)
  {
    return src->u.quads64[q];
  }
  else
  {
    abort();
  }
}

void quads_set(const Quads *src, uint64_t q, Quad64 i)
{
  if (src->level <= 16)
  {
    src->u.quads16[q] = (Quad16){ i.i, i.j };
  }
  else if (src->level <= 32)
  {
    src->u.quads32[q] = (Quad32){ i.i, i.j };
  }
  else if (src->level <= 64)
  {
    src->u.quads64[q] = i;
  }
  else
  {
    abort();
  }
}

void quads_save(const char *filename, Quads *q)
{
  FILE *f = fopen(filename, "wb");
  fwrite(&q->level, sizeof(q->level), 1, f);
  fwrite(&q->count, sizeof(q->count), 1, f);
  if (q->level <= 16)
  {
    fwrite(q->u.quads16, sizeof(*q->u.quads16) * q->count, 1, f);
  }
  else if (q->level <= 32)
  {
    fwrite(q->u.quads32, sizeof(*q->u.quads32) * q->count, 1, f);
  }
  else if (q->level <= 64)
  {
    fwrite(q->u.quads64, sizeof(*q->u.quads64) * q->count, 1, f);
  }
  fclose(f);
}

bool quads_load(const char *filename, Quads *q)
{
  FILE *f = fopen(filename, "rb");
  if (f)
  {
    uint64_t level = 0;
    if (1 == fread(&level, sizeof(level), 1, f))
    {
      uint64_t count = 0;
      if (1 == fread(&count, sizeof(count), 1, f))
      {
        if (level <= 16)
        {
          Quad16 *ptr = mem_alloc(sizeof(*ptr) * count);
          if (ptr)
          {
            if (1 == fread(ptr, sizeof(*ptr) * count, 1, f))
            {
              q->alloc = count;
              q->count = count;
              q->level = level;
              q->u.quads16 = ptr;
              fclose(f);
              return true;
            }
            mem_free(ptr, sizeof(*ptr) * count);
          }
        }
        else if (level <= 32)
        {
          Quad32 *ptr = mem_alloc(sizeof(*ptr) * count);
          if (ptr)
          {
            if (1 == fread(ptr, sizeof(*ptr) * count, 1, f))
            {
              q->alloc = count;
              q->count = count;
              q->level = level;
              q->u.quads32 = ptr;
              fclose(f);
              return true;
            }
            mem_free(ptr, sizeof(*ptr) * count);
          }
        }
        else if (level <= 64)
        {
          Quad64 *ptr = mem_alloc(sizeof(*ptr) * count);
          if (ptr)
          {
            if (1 == fread(ptr, sizeof(*ptr) * count, 1, f))
            {
              q->alloc = count;
              q->count = count;
              q->level = level;
              q->u.quads64 = ptr;
              fclose(f);
              return true;
            }
            mem_free(ptr, sizeof(*ptr) * count);
          }
        }
      }
    }
    fclose(f);
  }
  return false;
}

void quads_visualize(const char *filename, Quads *src)
{
  uint64_t count[256][256];
  memset(count, 0, sizeof(count));
  uint64_t box_size = 1;
  uint64_t shift_l = 0;
  uint64_t shift_r = 0;
  if (src->level < 8)
  {
    box_size = 256 >> src->level;
    shift_l = 8 - src->level;
    shift_r = 0;
  }
  else
  {
    box_size = 1;
    shift_l = 0;
    shift_r = src->level - 8;
  }
  #pragma omp parallel for
  for (uint64_t q = 0; q < src->count; ++q)
  {
    Quad64 b = quads_get(src, q);
    uint64_t i0 = (b.i << shift_l) >> shift_r;
    uint64_t j0 = (b.j << shift_l) >> shift_r;
    for (uint64_t j = j0; j < j0 + box_size; ++j)
    for (uint64_t i = i0; i < i0 + box_size; ++i)
    {
      #pragma omp atomic
      count[j][i]++;
    }
  }
  shift_l = 8;
  shift_r <<= 1;
  unsigned char pgm[256][256];
  for (uint64_t j = 0; j < 256; ++j)
  for (uint64_t i = 0; i < 256; ++i)
  {
    uint64_t g = (count[j][i] << shift_l) >> shift_r;
    pgm[j][i] = g >= 256 ? 255 : g;
  }
  FILE *f = fopen(filename, "wb");
  fprintf(f, "P5\n256 256\n255\n");
  fwrite(pgm, sizeof(pgm), 1, f);
  fclose(f);
}

bool quads_filter(Quads *dst, const Quads *src, QuadFilter filter, void *context)
{
  if (! quads_init(dst, src->level, src->count))
  {
    return false;
  }
  uint64_t i = 0;
  uint64_t o = 0;
  #pragma omp parallel for schedule(static, 1)
  for (uint64_t t = 0; t < threads; ++t)
  {
    while (true)
    {
      uint64_t q;
      #pragma omp atomic capture
      q = i++;
      if (! (q < src->count))
      {
        break;
      }
      Quad64 target = quads_get(src, q);
      if (filter(context, target))
      {
        uint64_t me;
        #pragma omp atomic capture
        me = o++;
        quads_set(dst, me, target);
      }
    }
  }
  dst->count = o;
  return true;
}

bool quads_refine(Quads *dst, const Quads *src, QuadFilter filter, void *context)
{
  if (! quads_init(dst, src->level + 1, 4 * src->count))
  {
    return false;
  }
  uint64_t in = 0;
  uint64_t o = 0;
  #pragma omp parallel for schedule(static, 1)
  for (uint64_t t = 0; t < threads; ++t)
  {
    while (true)
    {
      uint64_t q;
      #pragma omp atomic capture
      q = in++;
      if (! (q < src->count))
      {
        break;
      }
      uint64_t pc = 100 * q / src->count;
      uint64_t pc1 = 100 * (q - 1) / src->count;
      if (pc != pc1)
      {
        #pragma omp critical
        fprintf(stderr, "%3ld%%\r", pc);
      }

      Quad64 parent = quads_get(src, q);
      for (uint64_t i = 0; i < 2; ++i)
      for (uint64_t j = 0; j < 2; ++j)
      {
        Quad64 child = { (parent.i << 1) + i, (parent.j << 1) + j };
        if (filter(context, child))
        {
          uint64_t me;
          #pragma omp atomic capture
          me = o++;
          quads_set(dst, me, child);
        }
      }
    }
  }
  dst->count = o;
  return true;
}

void quads_map(const Quads *src, QuadMap map, void *context)
{
  uint64_t in = 0;
  #pragma omp parallel for schedule(static, 1)
  for (uint64_t t = 0; t < threads; ++t)
  {
    while (true)
    {
      uint64_t q;
      #pragma omp atomic capture
      q = in++;
      if (! (q < src->count))
      {
        break;
      }

      uint64_t pc = 100 * q / src->count;
      uint64_t pc1 = 100 * (q - 1) / src->count;
      if (pc != pc1)
      {
        #pragma omp critical
        fprintf(stderr, "%3ld%%\r", pc);
      }

      Quad64 target = quads_get(src, q);
      map(context, target);
    }
  }
}

#endif

uint64_t mu_M(double _Complex c, uint64_t iterations)
{
  double cx = creal(c);
  double cy = cimag(c);
  if ((cx == -2    && cy ==  0)
  ||  (cx == -1.75 && cy ==  0)
//  ||  (cx == -1.5  && cy ==  0)
  ||  (cx == -1.25 && cy ==  0)
  ||  (cx == -1    && cy == -0.25)
  ||  (cx == -1    && cy ==  0.25)
  ||  (cx == -0.75 && cy ==  0)
  ||  (cx ==  0    && cy == -1)
  ||  (cx ==  0    && cy ==  1)
  ||  (cx ==  0.25 && cy ==  0)
  ||  (cx ==  0.25 && cy == -0.5)
  ||  (cx ==  0.25 && cy ==  0.5))
  {
    return 0;
  }
  double zx = cx, zy = cy, min_z2 = 1.0 / 0.0;
  for (uint64_t p = 1; p < iterations; ++p)
  {
    double x2 = zx * zx;
    double y2 = zy * zy;
    double xy = zx * zy;
    double z2 = x2 + y2;

    if (z2 > 4)
    {
      return p;
    }

    if (z2 < min_z2)
    {
      if (12 * z2 < min_z2)
      {
//        double de;
        double _Complex w;
        m_d_attractor(&w, zx + I * zy, c, p, 64);
        double _Complex dw = 1;
        for (uint64_t q = 0; q < p; ++q)
        {
          dw = 2 * w * dw;
          w = w * w + c;
        }
        if (cnorm(dw) <= 1)
        {
          return 0;
        }
/*
        if (m_d_interior_de(&de, &dz, zx + I * zy, cx + I * cy, p, 64))
        {
          if (de >= 0)
          {
            return 0;
          }
        }
*/
      }
      min_z2 = z2;
    }

    zx = x2 - y2 + cx;
    zy = 2 * xy + cy;
  }
  return 0;
}

double d_M(double _Complex c, uint64_t iterations)
{
  double cx = creal(c);
  double cy = cimag(c);
  if ((cx == -2    && cy ==  0)
  ||  (cx == -1.75 && cy ==  0)
//  ||  (cx == -1.5  && cy ==  0)
  ||  (cx == -1.25 && cy ==  0)
  ||  (cx == -1    && cy == -0.25)
  ||  (cx == -1    && cy ==  0.25)
  ||  (cx == -0.75 && cy ==  0)
  ||  (cx ==  0    && cy == -1)
  ||  (cx ==  0    && cy ==  1)
  ||  (cx ==  0.25 && cy ==  0)
  ||  (cx ==  0.25 && cy == -0.5)
  ||  (cx ==  0.25 && cy ==  0.5))
  {
    return 0;
  }
  double zx = cx, zy = cy, dx = 1, dy = 0, min_z2 = 1.0 / 0.0;
  for (uint64_t p = 1; p < iterations; ++p)
  {
    double x2 = zx * zx;
    double y2 = zy * zy;
    double xy = zx * zy;
    double z2 = x2 + y2;

    if (z2 > 65536)
    {
      return sqrt(z2) * log(z2) / sqrt(dx * dx + dy * dy);
    }

    if (z2 < min_z2)
    {
      if (4 * z2 < min_z2)
      {
        double de;
        double _Complex dz;
        if (m_d_interior_de(&de, &dz, zx + I * zy, cx + I * cy, p, 64))
        {
          if (de >= 0)
          {
            return de;
          }
        }
      }
      min_z2 = z2;
    }

    double dxn = 2 * (dx * zx - dy * zy) + 1;
    double dyn = 2 * (dx * zy + dy * zx);
    dx = dxn;
    dy = dyn;
    zx = x2 - y2 + cx;
    zy = 2 * xy + cy;
  }
  return -1;
}

bool d_M_less_than_de_limit_and_hits_greater_than_hit_threshold(Context *context, double _Complex c)
{
  double cx = creal(c);
  double cy = cimag(c);
  if ((cx == -2    && cy ==  0)
  ||  (cx == -1.75 && cy ==  0)
//  ||  (cx == -1.5  && cy ==  0)
  ||  (cx == -1.25 && cy ==  0)
  ||  (cx == -1    && cy == -0.25)
  ||  (cx == -1    && cy ==  0.25)
  ||  (cx == -0.75 && cy ==  0)
  ||  (cx ==  0    && cy == -1)
  ||  (cx ==  0    && cy ==  1)
  ||  (cx ==  0.25 && cy ==  0)
  ||  (cx ==  0.25 && cy == -0.5)
  ||  (cx ==  0.25 && cy ==  0.5))
  {
    return 0;
  }

  double cr = context->c_scale;
  double zr = context->hist->z_radius;
  double cr2 = cr * cr;
  double zr2 = zr * zr;
  double _Complex z0 = context->hist->z;
  double ox = creal(z0), oy = cimag(z0);
  uint64_t hit_threshold = context->hit_threshold;
  uint64_t mu_limit = context->mu_limit;
  double de_limit = context->de_limit;

  double zx = cx, zy = cy, dx = 1, dy = 0, min_z2 = 1.0 / 0.0;
  uint64_t hits = 0;
  for (uint64_t p = 1; p < mu_limit; ++p)
  {
    double x2 = zx * zx;
    double y2 = zy * zy;
    double xy = zx * zy;
    double z2 = x2 + y2;

    double ex = zx - ox;
    double ey = zy - oy;
    double e2 = ex * ex + ey * ey;
    double d2 = dx * dx + dy * dy;
    hits += e2 - zr2 < cr2 * d2;

    if (z2 > 65536)
    {
      return sqrt(z2) * log(z2) / sqrt(dx * dx + dy * dy) < de_limit && hits > hit_threshold;
    }

    if (z2 < min_z2)
    {
      if (12 * z2 < min_z2)
      {
        double de;
        double _Complex dz;
        if (m_d_interior_de(&de, &dz, zx + I * zy, cx + I * cy, p, 64))
        {
          if (de >= 0)
          {
            return de < de_limit && hits > hit_threshold;
          }
        }
      }
      min_z2 = z2;
    }

    double dxn = 2 * (dx * zx - dy * zy) + 1;
    double dyn = 2 * (dx * zy + dy * zx);
    dx = dxn;
    dy = dyn;
    zx = x2 - y2 + cx;
    zy = 2 * xy + cy;
  }
  return hits > hit_threshold;
}

double d_J(double _Complex c, double _Complex z, uint64_t iterations)
{
  double cx = creal(c);
  double cy = cimag(c);
  double zx = creal(z);
  double zy = cimag(z);
  double dx = 1, dy = 0;
  for (uint64_t p = 1; p < iterations; ++p)
  {
    double x2 = zx * zx;
    double y2 = zy * zy;
    double xy = zx * zy;
    double z2 = x2 + y2;

    if (z2 > 65536)
    {
      return sqrt(z2) * log(z2) / sqrt(dx * dx + dy * dy);
    }

    double dxn = 2 * (dx * zx - dy * zy);
    double dyn = 2 * (dx * zy + dy * zx);
    dx = dxn;
    dy = dyn;
    zx = x2 - y2 + cx;
    zy = 2 * xy + cy;
  }
  return -1;
}

bool hist_init(Hist *h, uint64_t layers, uint64_t width, uint64_t height, double _Complex z)
{
  if ((h->hist = mem_alloc(sizeof(*h->hist) * layers * width * height)))
  {
    h->layers = layers;
    h->width = width;
    h->height = height;
    h->z = z;
    return true;
  }
  return false;
}

void hist_clear(Hist *h)
{
  mem_free(h->hist, sizeof(*h->hist) * h->layers * h->width * h->height);
}

void hist_radius(Hist *h, double radius)
{
  h->z_radius = radius;
  h->z_scale = h->height / radius;
  h->z_origin = 0.5 * (h->width + I * h->height) - h->z * h->z_scale;
}

void hist_reset(Hist *h)
{
  memset(h->hist, 0, sizeof(*h->hist) * h->layers * h->width * h->height);
}

void hist_save(const char *filename, Hist *h)
{
  FILE *f = fopen(filename, "wb");
  fwrite(h->hist, sizeof(*h->hist) * h->layers * h->width * h->height, 1, f);
  fclose(f);
}

bool hist_load(const char *filename, Hist *h)
{
  FILE *f = fopen(filename, "rb");
  if (f)
  {
    if (1 == fread(h->hist, sizeof(*h->hist) * h->layers * h->width * h->height, 1, f))
    {
      fclose(f);
      return true;
    }
    fclose(f);
  }
  return false;
}

double hist_density(Hist *h)
{
  uint64_t total = 0;
  for (uint64_t i = 0; i < h->layers * h->width * h->height; ++i)
  {
    total += h->hist[i];
  }
  return total / ((double) h->width * h->height);
}

double hist_plot(Hist *h, uint64_t layer, double x, double y, double w)
{
  double _Complex z = (x + I * y) * h->z_scale + h->z_origin;
  x = creal(z);
  y = cimag(z);
  if (0 <= x && x < h->width && 0 <= y && y < h->height)
  {
    uint64_t i = x;
    uint64_t j = y;
    uint64_t k = (layer * h->height + j) * h->width + i;
    uint64_t increment = ceil(w);
    uint64_t new;
    #pragma omp atomic capture
    new = h->hist[k] += increment;
//  uint64_t old = new - increment;
    return w / new;
  }
  return 0;
}

struct Node;
typedef struct Node Node;

struct Node
{
  uint32_t child[2][2];
  double weight;
};

typedef struct
{
  uint64_t alloc;
  uint64_t count;
  Node *nodes;
  Node *free;
  Node *root;
} Tree;

bool tree_init(Tree *t, uint64_t count)
{
  if ((t->nodes = mem_alloc(sizeof(*t->nodes) * count)))
  {
    memset(t->nodes, 0, sizeof(*t->nodes) * count);
    t->alloc = count;
    t->count = 1;
    t->root = &t->nodes[0];
    t->root->weight = 1;
    t->free = 0;
    uint32_t ifree = 0;
    for (uint64_t n = 1; n < count; ++n)
    {
      t->nodes[n].child[0][0] = ifree;
      ifree = n;
    }
    t->free = &t->nodes[ifree];
    return true;
  }
  return false;
}

void tree_clear(Tree *t)
{
  mem_free(t->nodes, sizeof(*t->nodes) * t->alloc);
}

Node *tree_alloc(Tree *t)
{
  Node *n = t->free;
  if (n)
  {
    t->free = n->child[0][0] ? &t->nodes[n->child[0][0]] : 0;
    memset(n, 0, sizeof(*n));
    ++t->count;
    return n;
  }
  return 0;
}

void tree_free(Tree *t, Node *n)
{
  for (uint64_t i = 0; i < 2; ++i)
  for (uint64_t j = 0; j < 2; ++j)
  {
    if (n->child[i][j])
    {
      tree_free(t, &t->nodes[n->child[i][j]]);
    }
  }
  memset(n, 0, sizeof(*n));
  if (t->free)
  {
    n->child[0][0] = t->free - t->nodes;
  }
  t->free = n;
  --t->count;
}

void tree_save(const char *filename, Tree *t)
{
  assert(t->root == t->nodes);
  FILE *f = fopen(filename, "wb");
  fwrite(&t->alloc, sizeof(t->alloc), 1, f);
  uint64_t ifree = t->free ? t->free - t->nodes : 0;
  fwrite(&ifree, sizeof(ifree), 1, f);
  fwrite(t->nodes, sizeof(*t->nodes) * t->alloc, 1, f);
  fclose(f);
}

void tree_insert_rec(Tree *t, Node *n, double _Complex center, double radius, double _Complex target, double weight)
{
  assert(n);
  uint64_t i = creal(center) <= creal(target);
  uint64_t j = cimag(center) <= cimag(target);
  if (n->child[i][j])
  {
    tree_insert_rec(t, &t->nodes[n->child[i][j]], center + ((i - 0.5) + I * (j - 0.5)) * radius, 0.5 * radius, target, weight);
  }
  #pragma omp atomic
  n->weight += weight;
}

void tree_insert(Tree *t, double _Complex target, double weight)
{
  assert(weight > 0);
  tree_insert_rec(t, t->root, 0, 2, target, weight);
}


void tree_sample_rec(Tree *t, Node *n, gsl_rng *rng, double p, double _Complex center, double radius, double _Complex *c, double *chosen_probability)
{
  if (n->child[0][0])
  {
    for (uint64_t i = 0; i < 2; ++i)
    for (uint64_t j = 0; j < 2; ++j)
    {
      double child_p = t->nodes[n->child[i][j]].weight;
      if (p < child_p)
      {
        tree_sample_rec(t, &t->nodes[n->child[i][j]], rng, p, center + ((i - 0.5) + I * (j - 0.5)) * radius, 0.5 * radius, c, chosen_probability);
        return;
      }
      p -= child_p;
    }
  }
  *c = center + 2 * radius * ((gsl_rng_uniform(rng) - 0.5) + I * (gsl_rng_uniform(rng) - 0.5));
  *chosen_probability = n->weight / t->root->weight;
}

void tree_sample(Tree *t, gsl_rng *rng, double _Complex *c, double *w)
{
  double p;
  tree_sample_rec(t, t->root, rng, t->root->weight * gsl_rng_uniform(rng), 0, 2, c, &p);
  *w = 1 / p;
}

void tree_subdivide_rec(Tree *t, Node *n, uint64_t depth)
{
  if (depth == 0)
  {
    return;
  }
  if (n->child[0][0])
  {
    for (uint64_t i = 0; i < 2; ++i)
    for (uint64_t j = 0; j < 2; ++j)
    {
      tree_subdivide_rec(t, &t->nodes[n->child[i][j]], depth - 1);
    }
  }
  else
  {
    for (uint64_t i = 0; i < 2; ++i)
    for (uint64_t j = 0; j < 2; ++j)
    {
      Node *c = tree_alloc(t);
      assert(c);
      c->weight = n->weight / 4;
      n->child[i][j] = c - t->nodes;
      tree_subdivide_rec(t, c, depth - 1);
    }
  }
}

void tree_subdivide(Tree *t, uint64_t depth)
{
  tree_subdivide_rec(t, t->root, depth);
}

void tree_balance_rec(Tree *t, Node *n, double average)
{
  if (n->child[0][0])
  {
    if (n->weight < average / 4)
    {
      for (uint64_t i = 0; i < 2; ++i)
      for (uint64_t j = 0; j < 2; ++j)
      {
        tree_free(t, &t->nodes[n->child[i][j]]);
        n->child[i][j] = 0;
      }
    }
    else
    {
      for (uint64_t i = 0; i < 2; ++i)
      for (uint64_t j = 0; j < 2; ++j)
      {
        tree_balance_rec(t, &t->nodes[n->child[i][j]], average);
      }
    }
  }
  else
  {
    if (n->weight > average * 4)
    {
      for (uint64_t i = 0; i < 2; ++i)
      for (uint64_t j = 0; j < 2; ++j)
      {
        Node *c = tree_alloc(t);
        assert(c);
        c->weight = n->weight / 4;
        n->child[i][j] = c - t->nodes;
        tree_balance_rec(t, c, average);
      }
    }
  }
}

void tree_balance(Tree *t, uint64_t target_count)
{
  double average = t->root->weight / target_count;
  tree_balance_rec(t, t->root, average);
}

void tree_visualize_rec(Tree *t, Node *n, double count[256][256], uint64_t depth, uint64_t i0, uint64_t j0)
{
  if (depth == 0)
  {
    count[i0][j0] = n->weight / t->root->weight;
  }
  else
  {
    if (n->child[0][0])
    {
      for (uint64_t i = 0; i < 2; ++i)
      for (uint64_t j = 0; j < 2; ++j)
      {
        tree_visualize_rec(t, &t->nodes[n->child[i][j]], count, depth - 1, (i0 << 1) + i, (j0 << 1) + j);
      }
    }
    else
    {
      for (uint64_t i = 0; i < 1ull << depth; ++i)
      for (uint64_t j = 0; j < 1ull << depth; ++j)
      {
        count[(i0 << depth) + i][(j0 << depth) + j] = n->weight / t->root->weight / ((1ull << depth) * (1ull << depth));
      }
    }
  }
}

void tree_visualize(const char *filename, Tree *t)
{
  double count[256][256];
  memset(count, 0, sizeof(count));
  tree_visualize_rec(t, t->root, count, 8, 0, 0);

  unsigned char pgm[256][256];
  for (uint64_t i = 0; i < 256; ++i)
  for (uint64_t j = 0; j < 256; ++j)
  {
    pgm[j][i] = fmin(fmax(255 + 16 * log(count[i][j]), 0), 255);
  }
  FILE *f = fopen(filename, "wb");
  fprintf(f, "P5\n256 256\n255\n");
  fwrite(pgm, sizeof(pgm), 1, f);
  fclose(f);
}



void context_init(Context *context, Hist *hist, BitsTree *tree)
{
  context->hist = hist;
//  context->quads = quads;
  context->tree = tree;
  context->hit_samples = 256;
  context->hit_threshold = 0;
}

void context_level(Context *context, uint64_t level)
{
  // if iteration count is greater than mu_limit
  // then exterior de will be less than de_limit
  context->de_limit = 16 * 1.4142135623731 / (1ull << level);
  double mu_limit = 2.13 / sqrt(context->de_limit);
  context->mu_limit = 256 + 1.5 * mu_limit;
  context->c_scale = 4.0 / (1ull << level);
  context->c_origin = (-2 + -2 * I) + (0.5 + I * 0.5) * context->c_scale;
  context->hit_threshold = mu_limit / level;
  fprintf(stderr, "level %ld de %.3g mu %.3g hit %ld\n", level, context->de_limit, mu_limit, context->hit_threshold);
  context->hit_mu_scale = 0.9;
}

void context_clear(Context *context)
{
  (void) context;
}

gsl_rng *rngs[64];
gsl_rng *get_rng(void)
{
  uint64_t me = omp_get_thread_num();
  if (! rngs[me])
  {
    rngs[me] = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(rngs[me], rand());
  }
  return rngs[me];
}

bool boundary(void *context_, Quad64 q)
{
  Context *context = context_;
  double _Complex c = context->c_origin + context->c_scale * (q.i + I * q.j);
  return // d_M(c, context->mu_limit) <= context->de_limit &&
         d_J(c, context->hist->z, context->mu_limit) <= context->de_limit
      && d_M_less_than_de_limit_and_hits_greater_than_hit_threshold(context, c);
}

#define sqrt2 1.4142135623731
#define sqrt05 0.707106781186548

bool boundary_ballwise(void *context_, Quad64 q)
{
  Context *context = context_;
  Ball c = { context->c_origin + context->c_scale * (q.i + I * q.j), context->c_scale * sqrt05 };
  if (! (d_M(c.x, context->mu_limit) <= context->de_limit))
  {
    return false;
  }
  Ball z = { context->hist->z, context->hist->z_radius * sqrt2 };
  uint64_t mu_limit = context->mu_limit;
  for (uint64_t p = 0; p < mu_limit; ++p)
  {
    if (ball_inf(z) > 23.1406926327793)
    {
      return false;
    }
    z = ball_add(ball_sqr(z), c);
  }
  return true;
}

#if 0
void plot_julia(void *context, Quad64 q)
{
  Context *context = context_;
  Ball c = { context->c_origin + context->c_scale * (q.i + I * q.j), context->c_scale * sqrt05 };
  if (! (d_M(c.x, context->mu_limit) <= context->de_limit))
  {
    return false;
  }
  #pragma omp parallel for
  for (uint64_t j = 0; j < context->hist->height; ++j)
  for (uint64_t i = 0; i < context->hist->width; ++i)
  {
    Ball z = { z_origin + z_scale * (i + I * j), 
    
  }
}
#endif

#if 0
  double cr2 = cr * cr;
  double zr2 = zr * zr;
  double _Complex z0 = context->hist->z;
  double _Complex c = context->c_origin + context->c_scale * (q.i + I * q.j);
  double cx = creal(c), cy = cimag(c), zx = 0, zy = 0, dx = 0, dy = 0, ox = creal(z0), oy = cimag(z0);
  uint64_t hit_count = 0;
  uint64_t mu = context->mu_limit;
  for (uint64_t p = 0; p < 0.9 * mu; ++p)
  {
    double x2 = zx * zx;
    double y2 = zy * zy;
    double xy = zx * zy;
    double z2 = x2 + y2;
    if (z2 > 4) break;
    double ex = zx - ox;
    double ey = zy - oy;
    double e2 = ex * ex + ey * ey;
    double d2 = dx * dx + dy * dy;
    hit_count += e2 - zr2 < cr2 * d2;
    if (hit_count > context->hit_threshold)
    {
      return true;
    }
    double dxn = 2 * (zx * dx - zy * dy) + 1;
    double dyn = 2 * (zx * dy + zy * dx);
    dx = dxn;
    dy = dyn;
    zx = x2 - y2 + cx;
    zy = 2 * xy + cy;
  }
  return false;
#endif
#if 0
  gsl_rng *rng = get_rng();
  double ox = creal(context->hist->z_origin);
  double oy = cimag(context->hist->z_origin);
  double os = context->hist->z_scale;
  double width = context->hist->width;
  double height = context->hist->height;
  for (uint64_t s = 0; s < context->hit_samples; ++s)
  {
    double _Complex c = context->c_origin + context->c_scale *
      ((q.i + gsl_rng_uniform(rng) - 0.5) + I * (q.j + gsl_rng_uniform(rng) - 0.5));
    uint64_t mu = mu_M(c, context->iterations); // FIXME count while iterating once
    if (mu)
    {
      uint64_t hit_count = 0;
      double zx = 0, zy = 0, cx = creal(c), cy = cimag(c);
      for (uint64_t p = 0; p < 0.9 * mu; ++p)
      {
        double x = ox + os * zx;
        double y = oy + os * zy;
        hit_count += 0 <= x && x < width && 0 <= y && y < height;
        if (hit_count > context->hit_threshold)
        {
          return true;
        }
        double x2 = zx * zx;
        double y2 = zy * zy;
        double xy = zx * zy;
        zx = x2 - y2 + cx;
        zy = 2 * xy + cy;
      }
    }
  }
  return false;
}
#endif

void accumulate(void *context_, Quad64 q)
{
  Context *context = context_;
  gsl_rng *rng = get_rng();
  for (uint64_t s = 0; s < context->samples_per_quad; ++s)
  {
#if 0
    uint64_t me;
//    #pragma omp atomic capture
    me = context->samples++;
    uint64_t pc = 100 * me / context->samples_target;
    uint64_t pc1 = 100 * (me - 1) / context->samples_target;
    if (pc != pc1)
    {
//      #pragma omp critical
      fprintf(stderr, "%3ld%%\r", pc);
    }
/*
    if (me >= context->samples_target)
    {
      break;
    }
*/
#endif

    if (gsl_rng_uniform(rng) < context->samples_per_quad - s)
    {

    double _Complex c = context->c_origin + context->c_scale *
      ((q.i + gsl_rng_uniform(rng) - 0.5) + I * (q.j + gsl_rng_uniform(rng) - 0.5));
    uint64_t mu = mu_M(c, context->iterations);
    if (mu)
    {
      uint64_t inc = 1;
      uint64_t layer = ilog2(mu);
      double cx = creal(c), cy = cimag(c), zx = 0, zy = 0;
      for (uint64_t i = 0; i < mu; ++i)
      {
        hist_plot(context->hist, layer, zx, zy, inc);
        double x2 = zx * zx;
        double y2 = zy * zy;
        double xy = zx * zy;
        zx = x2 - y2 + cx;
        zy = 2 * xy + cy;
      }

      }
    }
  }
}

#if 0
void sample(void *context_, Quad64 q)
{
  gsl_rng *rng = get_rng();
  Context *context = context_;
  double _Complex c = context->c_origin + context->c_scale * (q.i + I * q.j);
  tree_insert(context->tree, c, 1);
/*
    ((q.i + gsl_rng_uniform(rng) - 0.5) + I * (q.j + gsl_rng_uniform(rng) - 0.5));
    uint64_t mu = mu_M(c, 1ull << 30);
    if (mu > 0)
    {
      double h = d_J(c, context->z, 1ull << 30);
      if (h > 0)
      {
        tree_insert(context->tree, c, 1 / h);
      }
    }
  }
*/
}
#endif

double iterate_and_accumulate(Hist *histogram, double _Complex c, double weight)
{
  double cx = creal(c);
  double cy = cimag(c);

  if ((cx == -2    && cy ==  0)
  ||  (cx == -1.75 && cy ==  0)
//  ||  (cx == -1.5  && cy ==  0)
  ||  (cx == -1.25 && cy ==  0)
  ||  (cx == -1    && cy == -0.25)
  ||  (cx == -1    && cy ==  0.25)
  ||  (cx == -0.75 && cy ==  0)
  ||  (cx ==  0    && cy == -1)
  ||  (cx ==  0    && cy ==  1)
  ||  (cx ==  0.25 && cy ==  0)
  ||  (cx ==  0.25 && cy == -0.5)
  ||  (cx ==  0.25 && cy ==  0.5))
  {
    return 0;
  }

  double zx = cx, zy = cy, dx = 1, dy = 0, min_z2 = 1.0 / 0.0;
  uint64_t iterations = 1ull << histogram->layers;
  double de = -1;
  uint64_t p = 1;

  for (; p < iterations; ++p)
  {
    double x2 = zx * zx;
    double y2 = zy * zy;
    double xy = zx * zy;
    double z2 = x2 + y2;

    if (z2 > 65536)
    {
      de = sqrt(z2) * log(z2) / sqrt(dx * dx + dy * dy);
      break;
    }

    if (z2 < min_z2)
    {
      if (12 * z2 < min_z2)
      {
        double _Complex w;
        m_d_attractor(&w, zx + I * zy, c, p, 64);
        double _Complex dw = 1;
        for (uint64_t q = 0; q < p; ++q)
        {
          dw = 2 * w * dw;
          w = w * w + c;
        }
        if (cnorm(dw) <= 1)
        {
          return 0;
        }
      }
      min_z2 = z2;
    }

    double dxn = 2 * (dx * zx - dy * zy) + 1;
    double dyn = 2 * (dx * zy + dy * zx);
    dx = dxn;
    dy = dyn;

    zx = x2 - y2 + cx;
    zy = 2 * xy + cy;
  }

  if (de > 0)
  {
    zx = cx;
    zy = cy;
/*
    dx = 1;
    dy = 0;
    double ox = creal(histogram->z);
    double oy = cimag(histogram->z);
    double or = histogram->z_radius;
    double o2 = or * or;
    double de2 = de * de;
*/
    uint64_t layer = ilog2(p);
    double hit = 0;
    for (uint64_t q = 1; q < p; ++q)
    {
      double x2 = zx * zx;
      double y2 = zy * zy;
      double xy = zx * zy;
/*
      double ex = zx - ox;
      double ey = zy - oy;
      double e2 = ex * ex + ey * ey;
      double r2 = de2 * (dx * dx + dy * dy);
*/
      hit += hist_plot(histogram, layer, zx, zy, (1ull << 16) * weight);
/*
      double dxn = 2 * (dx * zx - dy * zy) + 1;
      double dyn = 2 * (dx * zy + dy * zx);
      dx = dxn;
      dy = dyn;
*/
      zx = x2 - y2 + cx;
      zy = 2 * xy + cy;
    }
    return hit / (1ull << 16);
  }
  return 0;
}

int main(int argc, char **argv)
{
  srand(time(0));
  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);
  mem_limit = 128ull << 30; // GiB

  Tree importance_map;
  tree_init(&importance_map, 1ull << 24);
  importance_map.root->weight = 1;
//  tree_subdivide(&importance_map, 8);

  Hist histogram;
  hist_init(&histogram, 30, 256, 256, 0);//0.56208345369277852973 - 0.00000016540178571429 * I);
  hist_radius(&histogram, 4);//0.00000002);
  hist_reset(&histogram);

  gsl_rng *rng[threads];
  for (uint64_t thread = 0; thread < threads; ++thread)
  {
    rng[thread] = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(rng[thread], rand());
  }


  uint64_t level = 0;
  uint64_t orbits_target = threads << 1;
  uint64_t orbits = orbits_target >> 1;
  while (running)
  {
    #pragma omp parallel for schedule(static, 1)
    for (uint64_t thread = 0; thread < threads; ++thread)
    {
      while (running && orbits < orbits_target)
      {
        #pragma omp atomic
        orbits++;
        uint64_t pc = 100 * (orbits - (orbits_target >> 1)) / (orbits_target >> 1);
        uint64_t pc1 = 100 * (orbits - (orbits_target >> 1) -1 ) / (orbits_target >> 1);
        if (pc != pc1)
        {
          #pragma omp critical
          fprintf(stderr, "%3ld%%\r", pc);
        }

        double _Complex c;
        double weight;
        tree_sample(&importance_map, rng[thread], &c, &weight);
        double hit = iterate_and_accumulate(&histogram, c, weight);
        if (hit > 0)
        {
          tree_insert(&importance_map, c, hit);
        }
      }
    }
    tree_balance(&importance_map, 1ull << 16);
    tree_save("importance.tree", &importance_map);
    tree_visualize("importance.pgm", &importance_map);
    hist_save("histogram.map", &histogram);
    char command[100];
    snprintf(command, sizeof(command), "../bbcolourizelayers histogram.map | pnmtopng > histogram.png");
    system(command);
    fprintf(stderr, "count %ld, weight: %.3g, density: %3g\n", importance_map.count, importance_map.root->weight, hist_density(&histogram));
    orbits_target <<= 1;
  }
  return 0;

#if 0

  if (action == a_init)
  {
    BitsTree tree;
    bitstree_init(&tree, 0, 1);
    bit_write(&tree.cursor, true);
    tree.bits.bits_used = 1;
    printf("initialized level %02ld bits %ld\n", tree.level, tree.bits.bits_used);
    fflush(stdout);
    return 0;
  }

  BitsTree src;
  if (bitstree_load("bb.tree", &src))
  {
    printf("loaded level %02ld bits %ld\n", src.level, src.bits.bits_used);
    fflush(stdout);
  }
  else
  {
    return 1;
  }

  Hist hist;
  hist_init(&hist, 30, 256, 256, 0.56208345369277852973 - 0.00000016540178571429 * I);
  char filename[64];
  snprintf(filename, sizeof(filename), "bb.map.%02ld", src.level);
  double density;
  if (hist_load(filename, &hist))
  {
    density = hist_density(&hist);
    printf("loaded histogram with density %.3g\n", density);
    fflush(stdout);
  }
  else
  {
    density = 0;
    hist_reset(&hist);
    printf("initialized histogram with density %.3g\n", density);
    fflush(stdout);
  }

  Context context;
  context_init(&context, &hist, &src);

  if (action == a_refine)
  {

  while (src.level < 55 && running)
  {
    hist_radius(&hist, 4.0 / sqrt(1ull << (src.level + 1)));
    context_level(&context, src.level + 1);
    BitsTree dst;
    if (! bitstree_refine(&dst, &src, boundary_ballwise, &context))
    {
      break;
    }
    bitstree_clear(&src);
    src = dst;
    char filename[64];
    snprintf(filename, sizeof(filename), "bb.tree.%02ld", src.level);
    bitstree_save(filename, &src);
/*
    snprintf(filename, sizeof(filename), "bb.quads.%ld.pgm", src.level);
    quads_visualize(filename, &src);
*/
    printf("tree level %02ld bits %ld\n", src.level, src.bits.bits_used);
    fflush(stdout);
  }
  return 0;

  }
  else if (action == a_render)
  {

    hist_radius(&hist, 4.0 / sqrt(1ull << src.level));
    context_level(&context, src.level);
    context.iterations = 1ull << src.level;
//    double density_target = 2 * density;
//    fprintf(stderr, "%g %g %g\n", hist.z_scale, creal(hist.z_origin), cimag(hist.z_origin));
//    fprintf(stderr, "%g %g %g\n", context.c_scale, creal(context.c_origin), cimag(context.c_origin));
    context.samples_per_quad = 1; //context.samples_target / (double) src.bits.bits_used;
    while (running)
    {
//      context.samples = 0;
      //context.samples_target = ceil(hist.width * hist.height / (double) threads);
      bitstree_parmap(&src, accumulate, &context);
      density = hist_density(&hist);
      printf("accumulated %.3g orbits at density %.3g\n", context.samples_per_quad * src.bits.bits_used, density);
      fflush(stdout);
    //  if (density > density_target || ! running)
      {
        char filename[64];
        snprintf(filename, sizeof(filename), "bb.map.%02ld", src.level);
        hist_save(filename, &hist);
        char command[100];
        snprintf(command, sizeof(command), "../bbcolourizelayers bb.map.%02ld | pnmtopng > bb.map.%02ld.png", src.level, src.level);
        system(command);
      }
      context.samples_per_quad *= 2;
    }
    return 0;
  }
#endif
#if 0
/*
  tree_init(&tree, (mem_limit - mem_allocated) / sizeof(Node), 1);
  context.samples = tree.alloc / src.count;
  printf("phase 2\n");
  fflush(stdout);
  quads_map(&src, sample, &context);
  quads_clear(&src);
*/

  double density_target = 2 * density;
  const uint64_t threads = 16;
  hist_radius(&hist, 4.0 / sqrt(1ull << src.level));
  context.samples_target = ceil(hist.width * hist.height / (double) threads);
  context_level(&context, src.level);
  printf("phase 2\n");
  fflush(stdout);
  while (running)
  {
    printf("accumulating %.3g orbits at density %.3g\n", (double) context.samples_target, density);
    fflush(stdout);
    context.samples = 0;
    #pragma omp parallel for schedule(static, 1)
    for (uint64_t t = 0; t < threads; ++t)
    {
      accumulate(&context);
    }
    fflush(stdout);
    density = hist_density(&hist);
    if (density > density_target || ! running)
    {
//      tree_save("bb.tree", &tree);
      hist_save("bb.map", &hist);
      density_target = 2 * density;
//      tree.split_weight *= 2;
      printf("saved\n");
      fflush(stdout);
    }
    context.samples_target <<= 1;
  }
  //tree_save("bb.tree", &tree);

  hist_save("bb.map", &hist);
  printf("done\n");
  fflush(stdout);

  context_clear(&context);
  quads_clear(&src);
  hist_clear(&hist);

  return 0;
#endif
}
