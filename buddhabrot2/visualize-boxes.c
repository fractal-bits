#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef int64_t Z;
typedef uint64_t N;
typedef double R;
typedef double _Complex C;

typedef struct
{
  N i;
  N j;
  R pdf;
  R cdf;
} box_t;


box_t *boxes_src;
N boxes_src_level;
N boxes_src_count;
N boxes_src_alloc = 0;

bool boxes_load(void)
{
  N level, count;
  FILE *f = fopen("bb.box", "rb");
  if (! f)
  {
    return false;
  }
  if (1 == fread(&level, sizeof(level), 1, f) && 1 == fread(&count, sizeof(count), 1, f))
  {
    boxes_src_level = level;
    boxes_src_count = count;
    boxes_src_alloc = sizeof(*boxes_src) * boxes_src_count;
    boxes_src = malloc(boxes_src_alloc);
    if (boxes_src)
    {
      if (1 == fread(boxes_src, boxes_src_alloc, 1, f))
      {
        fclose(f);
        return true;
      }
      free(boxes_src);
      boxes_src = 0;
    }
  }
  fclose(f);
  boxes_src_level = 0;
  boxes_src_count = 0;
  boxes_src_alloc = 0;
  return false;
}

R raw[1024][1024];
unsigned char pgm[1024][1024];

void plot(void)
{
  N dim = 1ull << boxes_src_level;
  for (N src = 0; src < boxes_src_count; ++src)
  {
    box_t b = boxes_src[src];
    N i = b.i * 1024 / dim;
    N j = b.j * 1024 / dim;
    raw[j][i] += b.pdf;
  }
  for (N j = 0; j < 1024; ++j)
  for (N i = 0; i < 1024; ++i)
    pgm[j][i] = raw[j][i] == 0 ? 0 : fmin(fmax(255 + 8 * log(raw[j][i]), 1), 255);

}

int main(int argc, char **argv)
{
  boxes_load();
  plot();
  printf("P5\n%lld %lld\n255\n", 1024, 1024);
  fwrite(pgm, sizeof(pgm), 1, stdout);
  return 0;
}
