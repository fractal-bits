#include <math.h>
#include <complex.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <mandelbrot-numerics.h>

typedef double R;
typedef double _Complex C;
typedef unsigned char B;
typedef uint64_t N;

static inline
R cnorm(C z)
{
  R x = creal(z);
  R y = cimag(z);
  return x * x + y * y;
}

static inline
R cabs_fast(C z)
{
  return sqrt(cnorm(z));
}

static inline
R boxnorm(C z)
{
  R x = creal(z);
  R y = cimag(z);
  return fmax(fabs(x), fabs(y));
}

typedef struct
{
  C c;
  R r;
} ball_t;


#define LAYERS 30
#define SCANDIM (1ull << 14)
#define SCANITERS (1ull << 8)
#define MINIDIM (1ull << 5)
#define MINIITERS (1ull << 8)
#define MINISCOUNT (1ull << 16)
#define BALLCOUNT (1ull << 24)
#define PLOTDIM (1ull << 9)
#define PLOTITERS (1ull << 24)
#define DIM (1ull << 8)

ball_t balls[BALLCOUNT];
N ball = 0;

N histogram[LAYERS][DIM][DIM];
B pgm[DIM][DIM];

N hits[1024];

volatile bool running = true;

void signal_handler(int sig)
{
  (void) sig;
  running = false;
}

bool same_ball(ball_t a, ball_t b)
{
  return a.r < 2 * b.r && b.r < 2 * a.r && cnorm(a.c - b.c) < a.r * b.r;
}

bool insert_ball(ball_t *balls, N size, N *count, ball_t ball)
{
  if (*count >= size)
  {
    return false;
  }
  for (N b = 0; b < *count; ++b)
  {
    if (same_ball(balls[b], ball))
    {
      return false;
    }
  }
  balls[(*count)++] = ball;
  return true;
}

void scan_for_minis(ball_t *out, N *count, C c0, R cr, C z0, R zr)
{
  R crmin = 1e-15 * PLOTDIM;
  for (N j = 0; j < MINIDIM; ++j)
  for (N i = 0; i < MINIDIM; ++i)
  {
    C c = c0 + (((i + 0.5) + I * (j + 0.5)) / MINIDIM - (0.5 + 0.5 * I)) * 2 * cr;
    C z = c;
    C dc = 1;
    R min_z2 = 1.0/0.0;
    for (N k = 1; k < MINIITERS; ++k)
    {
      R z2 = cnorm(z);
      if (z2 > 16) break;
      if (z2 < min_z2)
      {
        C inc = z / dc;
        if (boxnorm(inc) < 2 * cr / MINIDIM)
        {
          C c1;
          m_d_nucleus(&c1, c, k, 64);
          if (boxnorm(c1 - c) < 2 * cr / MINIDIM)
          {
            C w = 0;
            N hit = 0;
            for (N l = 0; l < k; ++l)
            {
              hit += boxnorm(w - z0) < zr;
              w = w * w + c;
            }
            if (hit)
            {
              R c1r = cabs(m_d_size(c1, k));
              if (*count < MINISCOUNT && c1r > crmin)
              {
                insert_ball(out, MINISCOUNT, count, (ball_t){ c1, 2 * c1r });
              }
            }
          }
        }
        min_z2 = z2;
      }
      dc = 2 * z * dc + 1;
      z = z * z + c;
    }
  }
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

  C z0 = 0.56208345369277852973 - 0.00000016540178571429 * I;
  R r = 0.00000001 * 10000;
/*
  z0 = 0.1601376953125 - 0.443776100890625 * I;
  r = 5e-6;
*/
  R cr = 0.5 * 4.0 / SCANDIM;

  N progress = 0;
  #pragma omp parallel for schedule(dynamic, 1)
  for (N j = 0; j < SCANDIM; ++j)
  if (running)
  {
    N me;
    #pragma omp atomic capture
    me = progress++;
    N pc = 100 * me / SCANDIM ;
    N pc1 = 100 * (me - 1) / SCANDIM;
    if (pc != pc1)
    {
      #pragma omp critical
      fprintf(stderr, " %ld%% %ld\r", pc, ball);
    }

    for (N i = 0; i < SCANDIM; ++i)
    {
      ball_t myballs[MINISCOUNT];
      N myball = 0;

      C c = ((i + 0.5) / SCANDIM - 0.5) * 4 + ((j + 0.5) / SCANDIM - 0.5) * 4 * I;
      C z = c;
      C dc = 1;
      R min_z2 = 1.0 / 0.0;
      N p;
      for (p = 1; p < SCANITERS; ++p)
      {
        R z2 = cnorm(z);
        if (z2 > 16)
        {
          break;
        }

        if (z2 < min_z2)
        {
          if (12 * z2 < min_z2)
          {
            C w = z;
            m_d_attractor(&w, z, c, p, 16);
            C dw = 1;
            for (N q = 0; q < p; ++q)
            {
              dw = 2 * w * dw;
              w = w * w + c;
            }
            if (cnorm(dw) <= 1)
            {
              break;
            }
          }
          min_z2 = z2;
        }

        // find c1
        C inc = (z - z0) / dc;
        if (boxnorm(inc) < cr)
        {
          C c1 = c - inc;
          C w = 0;
          C dw = 0;
          for (N step = 0; step < 64; ++step)
          {
            w = 0;
            dw = 0;
            for (N q = 0; q < p; ++q)
            {
              dw = 2 * w * dw + 1;
              w = w * w + c1;
            }
            c1 -= (w - z0) / dw;
            if (! (boxnorm(c1 - c) < cr))
            {
              break;
            }
          }

          // filter by distance
          if (boxnorm(c1 - c) < cr)
          {
            R c1r = r / cabs(dw);
            w = c1;
            dw = 1;
            R min_w2 = 1.0/0.0;
            R de = -1;
            N hit = 0;
            for (N q = 1; q < PLOTITERS; ++q)
            {
              hit += boxnorm(w - z0) < r;
              R w2 = cnorm(w);
              if (w2 > 65536)
              {
                de = sqrt(w2) * log(w2) / cabs(dw);
                break;
              }

              if (w2 < min_w2)
              {
                if (12 * w2 < min_w2)
                {
                  C u, du;
                  m_d_attractor(&u, w, c1, q, 16);
                  if (m_d_interior_de(&de, &du, u, c1, q, 16))
                  {
                    if (de >= 0)
                    {
                      break;
                    }
                  }
                }
                min_w2 = w2;
              }

              dw = 2 * w * dw + 1;
              w = w * w + c1;
            }
            if (hit && 0 <= de && 4 * de < c1r)
            {
              scan_for_minis(myballs, &myball, c1, c1r, z0, r);
            }
          }
        }

        dc = 2 * z * dc + 1;
        z = z * z + c;
      }

      if (p < SCANITERS)
      {
        #pragma omp critical
        {
          for (N b = 0; b < myball && ball < BALLCOUNT; ++b)
          {
            insert_ball(balls, BALLCOUNT, &ball, myballs[b]);
          }
        }
      }
    }
  }

  if (! running) return 0;

  if (ball >= BALLCOUNT)
  {
    ball = BALLCOUNT;
  }
  R area = 0;
  for (N b = 0; b < ball && running; ++b)
  {
    area += balls[b].r * balls[b].r;
  }
  fprintf(stderr, "count %ld area %g\n", ball, area);

  N total_orbits = 0;
  N total_orbits_plotted = 0;
  N total_points_plotted = 0;
  for (N b = 0; b < ball && running; ++b)
  {
    C c0 = balls[b].c;
    R cr = balls[b].r;
    N weight = ceil((cr * cr) * ball / area * (1ull << 16));
    fprintf(stderr, " %ld/%ld %.3g%% %.3g #\r", b, ball, 100 * total_orbits_plotted / (R) total_orbits, total_points_plotted / (R) total_orbits_plotted);

#if 0
    char command[1024];
    snprintf(command, sizeof(command), "m-render ball-%08ld.png 512 512 %.18f %.18f %.5e 100100100 1", b, creal(c0), cimag(c0), cr);
    system(command);
#endif

    #pragma omp parallel for schedule(dynamic, 1)
    for (N i = 0; i < PLOTDIM; ++i)
    for (N j = 0; j < PLOTDIM; ++j)
    {
      total_orbits++;
      C c = c0 + cr * (((i + 0.5) / PLOTDIM - 0.5) + ((j + 0.5) / PLOTDIM - 0.5) * I);
      C z = c;
      R min_z2 = 1.0/0.0;
      N plot = 0;
      N hit = 0;
      N k;
      for (k = 1; k < PLOTITERS; ++k)
      {
        hit += boxnorm(z - z0) < r;

        R z2 = cnorm(z);

        if (z2 > 16)
        {
          plot = 1;
          break;
        }

        if (z2 < min_z2)
        {
          if (12 * z2 < min_z2)
          {
            C u, du = 1;
            m_d_attractor(&u, z, c, k, 16);
            for (N q = 0; q < k; ++q)
            {
              du = 2 * u * du;
              u = u * u + c;
            }
            if (cnorm(du) <= 1)
            {
              break;
            }
          }
          min_z2 = z2;
        }

        z = z * z + c;
      }

      z = 0;
      N plane = floor(log2(k + 0.9));
      if (hit > 1 && plot)
      {
        total_orbits_plotted++;
        N hit = 0;
        for (N l = 0; l < k; ++l)
        {
          C w = (z - z0) / r * DIM + 0.5 * (DIM + DIM * I);
          R x = creal(w);
          R y = cimag(w);
          if (0 <= x && x < DIM && 0 <= y && y < DIM)
          {
            N iy = y;
            N ix = x;
            #pragma omp atomic
            histogram[plane][iy][ix] += weight;
            hit++;
           }
           z = z * z + c;
         }
         if (hit > 1)
         {
           #pragma omp atomic
           total_points_plotted += hit;
           if (0 < hit && hit < 1024)
           {
             #pragma omp atomic
             hits[hit]++;
           }
           else
           {
             #pragma omp atomic
             hits[0]++;
           }
         }
       }
    }
  }

  fprintf(stderr, "\n");
  for (N i = 0; i < 1024; ++i)
  {
    if (hits[i])
    {
      //fprintf(stderr, "%ld %ld\n", i, hits[i]);
    }
  }

  fprintf(stderr, "\nsaving ... ");
  FILE *f = fopen("bb.map", "wb");
  fwrite(histogram, sizeof(histogram), 1, f);
  fclose(f);
  fprintf(stderr, "ok\n");
  return 0;
}
