void ball_print(ball_t z)
{
  printf("%.18e %.18e %.5e", creal(z.center), cimag(z.center), 
}

void backward(int depth, ball_t c, ball_t z)
{
  if (depth == 0)
  {
    ball_print(c);
    printf("\t");
    ball_print(z);
    printf("\t");
    return;
  }
  ball_t rz = ball_sqrt(ball_add_exact(z, 0.25));
  ball_t cp = ball_add_exact(rz, -0.5);
  ball_t cm = ball_add_exact(ball_neg(rz), -0.5);
  ball_t zpp = ball_sqrt(ball_sub(z, cp));
  ball_t zpm = ball_negate(zpp);
  ball_t zmp = ball_sqrt(ball_sub(z, cm));
  ball_t zmm = ball_negate(zpp);
  depth -= 1;
  backward(depth, cp, zpp);
  backward(depth, cp, zpm);
  backward(depth, cm, zmp);
  backward(depth, cm, zmm);
}



int main(int argc, char **argv)
{
}
