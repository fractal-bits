# theory

let $V = B(z, r) \subset \mathbb{C}$ be the view

the aim is to find largest possible
$U = \{ c \in \mathbb{C}  : \exists p \in \mathbb{N} . f_c^p(0) \in V \)$$

$f_c^p(0) = z$ has $2^{p-1}$ roots

the interesting ones have

$d_M(c) < r / d(f_c^p(0))/dc$

possibly using homotopy rootfinder
or Newton all-roots?

find c st
z^+c = z   c = z - z^2
(z^2+c)^2+c = z   z^4 + 2 z^2 c + c^2+c = z   c^2 + (1 + 2 z^2) c + (z^4 - z) = 0
c = (-(1+2z^2) +- sqrt((1+2z^2)^2 + 4(z - z^4)))/2
  = -1/2 - z^2 +- sqrt(1 + 4z^4 + 4 z^2 + 4z - 4z^4)/2
  = -1/2 - z^2 +- sqrt((2z+1)^2)/2
  = -1/2 - z^2 +- (z + 1/2)
  = z - z^2, -z^2 - z - 1




# importance map

initial

- 256x256 leafs all weight 1

weight increment

- sum area splat intersect target / area source

insert

- find leaf
- increase weight

balance

- average = weight root / target leaf count
- if weight node < average     then merge 
- if weight leaf > average * 4 then split

sample

- p leaf = weight leaf / weight root
- w leaf = 1 / (count leaf * p leaf)
