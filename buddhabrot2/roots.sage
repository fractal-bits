c = polygen(QQbar)
z = 0.56208345369277852973 - 0.00000016540178571429 * I
def f(n, w):
    return w if n == 0 else f(n -1, w^2+c)
for n in range(25):
    print(n)
    for r in (f(n, z) - z).roots():
        print(real(r[0]), imag(r[0]))
