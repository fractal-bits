#include <assert.h>
#include <complex.h>
#include <math.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <mandelbrot-numerics.h>

#include <gsl/gsl_rng.h>

typedef int64_t Z;
typedef uint64_t N;
typedef double R;
typedef double _Complex C;

static inline
R cnorm(C z)
{
  R x = creal(z);
  R y = cimag(z);
  return x * x + y * y;
}

static inline
R cabs_fast(C z)
{
  return sqrt(cnorm(z));
}

N ilog2(N x)
{
  if (x == 0)
  {
    return -1;
  }
  for (N r = 0, n = 1; n; ++r, n <<= 1)
  {
    if (n > x)
    {
      return r - 1;
    }
  }
  return -1;
}

// exit

volatile bool running = true;

void signal_handler(int sig)
{
  (void) sig;
  running = false;
}

// memory

N memory_limit;

N memory_allocated = 0;

void *allocate(N bytes)
{
  if (memory_allocated + bytes <= memory_limit)
  {
    memory_allocated += bytes;
    return calloc(1, bytes);
  }
  else
  {
    return 0;
  }
}

void deallocate(void *ptr, N bytes)
{
  assert(memory_allocated >= bytes);
  memory_allocated -= bytes;
  free(ptr);
}

// view

C z_center;
R z_radius;
R z_circumradius;
N maximum_iterations;
N zwidth, zheight;

N *histogram;

// box mapping

R c_diameter = 4;
C c_origin = -2 + I * -2;

const R sqrt05 = 0.707106781186548;

typedef struct
{
  N i;
  N j;
  R contribution;
  R cdf;
} box_t;

int box_cmp_contrib(const void *x, const void *y)
{
  const box_t *a = x;
  const box_t *b = y;
  R u = a->contribution;
  R v = b->contribution;
  return (v > u) - (u > v);
}

int boxes_binary_search(const box_t *boxes, N count, R p)
{
  N lo = 0;
  N hi = count;
  while (lo + 1 < hi)
  {
    N md = (lo + hi + 1) >> 1;
    R q = boxes[md].cdf;
    if (p < q)
    {
      hi = md;
    }
    else
    {
      lo = md;
    }
  }
  return (lo + hi + 1) >> 1;
}

C box_offset(box_t box, N level, C offset)
{
  return c_origin + c_diameter * ((box.i + I * box.j) + offset) / (1ull << level);
}

C box_center(box_t box, N level)
{
  return box_offset(box, level, 0.5 + I * 0.5);
}

R box_inradius(N level)
{
  return 0.5 * c_diameter / (1ull << level);
}

R box_circumradius(N level)
{
  return sqrt05 * c_diameter / (1ull << level);
}

// contribution checking

R box_contributes(box_t box, N level)
{
  R contributed = 0;

/*
  C c[5] =
    { box_offset(box, level, 0)
    , box_offset(box, level, 1)
    , box_offset(box, level, I)
    , box_offset(box, level, 1 + I)
    , box_offset(box, level, 0.5 + I * 0.5)
    };
  C z[5] = { c[0], c[1], c[2], c[3], c[4] };
  R min_z[4] = { 1./0., 1./0., 1./0., 1./0. };
  Z active[4] = { 0, 0, 0, 0 };
*/
  C c = box_center(box, level);
  C z = c;
  R cr = box_circumradius(level);
  R zr = cr;
  R min_z = 1.0/0.0;
  for (N i = 1; i < maximum_iterations && i < (1ull << (level + 8)); ++i)
  {
    if (cabs_fast(z - z_center) - zr < z_circumradius)
    {
      contributed += (cr / zr) * (cr / zr);
    }
    R abs_z = cabs_fast(z);
    if (abs_z - zr > 2)
    {
      // all escaped
      return contributed;
    }
    if (zr > 4)
    {
      // covers everything
      return contributed;
    }
/*
    bool any_active = false;
    Z min_active = maximum_iterations, max_active = - (Z) maximum_iterations;
    for (N k = 0; k < 4; ++k)
    {
      if (active[k] == 0)
      {
        R norm_z = creal(z[k]) * creal(z[k]) + cimag(z[k]) * cimag(z[k]);
        if (norm_z > 4)
        {
          active[k] = i;
        }
        else*/
        if (abs_z < min_z)
        {
          if (3 * abs_z < min_z)
          {
            R de;
            C dz;
            if (m_d_interior_de(&de, &dz, z, c, i, 64))
            {
              if (sqrt05 * 4 * cr < de)
              {
                return 0; // 100% interior
//                active[k] = - (Z) i;
              }
              else if (0 <= de)
              {
//                return contributed * maximum_iterations / i;
              }
            }
          }
          min_z = abs_z;
        }
/*
        z[k] = z[k] * z[k] + c[k];
      }
      any_active |= active[k] == 0;
      if (active[k] < min_active) min_active = active[k];
      if (active[k] > max_active) max_active = active[k];
    }
    if (! any_active)
    {
      if (max_active < 0)
      {
        // all interior
        if (min_active == max_active)
        {
          // all interior to same period => probably same component
          return 0;
        }
        else
        {
          // all interior, different periods => boundary is present
          return contributed * maximum_iterations / (-min_active);
        }
      }
      else
      {
        // max_active > 0
        if (min_active < 0)
        {
          // boundary is present
          return contributed * maximum_iterations / (-min_active);
        }
        else
        {
          // all exterior, but boundary might be in the middle
          return contributed;
        }
      }
    }
*/
    //zr = zr * (2 * abs_z + zr) + cr;
    zr = 2 * abs_z * zr;
    z = z * z + c;
  }
  return contributed;
}

// map refinement

box_t *boxes_src;
N boxes_src_level;
N boxes_src_count;
N boxes_src_alloc = 0;

box_t *boxes_dst;
N boxes_dst_level;
N boxes_dst_count;
N boxes_dst_alloc = 0;

void boxes_init(N level)
{
  boxes_src_level = level;
  boxes_src_count = (1ull << level) * (1ull << level);
  boxes_src_alloc = boxes_src_count * sizeof(*boxes_src);
  boxes_src = allocate(boxes_src_alloc);
  assert(boxes_src);
  #pragma omp parallel for collapse(2)
  for (N i = 0; i < 1ull << level; ++i)
  for (N j = 0; j < 1ull << level; ++j)
  {
    N k = (j << level) + i;
    boxes_src[k].i = i;
    boxes_src[k].j = j;
  }
}

bool boxes_load(void)
{
  N level, count;
  FILE *f = fopen("bb.box", "rb");
  if (! f)
  {
    return false;
  }
  if (1 == fread(&level, sizeof(level), 1, f) && 1 == fread(&count, sizeof(count), 1, f))
  {
    boxes_src_level = level;
    boxes_src_count = count;
    boxes_src_alloc = sizeof(*boxes_src) * boxes_src_count;
    boxes_src = allocate(boxes_src_alloc);
    if (boxes_src)
    {
      if (1 == fread(boxes_src, boxes_src_alloc, 1, f))
      {
        fclose(f);
        return true;
      }
      deallocate(boxes_src, boxes_src_alloc);
      boxes_src = 0;
    }
  }
  fclose(f);
  boxes_src_level = 0;
  boxes_src_count = 0;
  boxes_src_alloc = 0;
  return false;
}

void boxes_save(void)
{
  FILE *f = fopen("bb.box", "wb");
  assert(f);
  fwrite(&boxes_src_level, sizeof(boxes_src_level), 1, f);
  fwrite(&boxes_src_count, sizeof(boxes_src_count), 1, f);
  fwrite(boxes_src, sizeof(*boxes_src) * boxes_src_count, 1, f);
  fclose(f);
}

bool boxes_refine(void)
{
  if (boxes_src_count == 0 || boxes_src_level >= 6)
  {
    return false;
  }
  boxes_dst_level = boxes_src_level + 1;
  boxes_dst_count = boxes_src_count * 4;
  boxes_dst_alloc = boxes_dst_count * sizeof(*boxes_dst);
  boxes_dst = allocate(boxes_dst_alloc);
  if (! boxes_dst)
  {
    boxes_dst_count = 0;
    boxes_dst_alloc = 0;
    return false;
  }
  boxes_dst_count = 0;

  // keep contributing children
  N dst = 0;
  N progress = 0;
  N chunk = boxes_src_count * 2 * 2 / 256 + 1;
  #pragma omp parallel for collapse(3) schedule(dynamic, chunk)
  for (N src = 0; src < boxes_src_count; ++src)
  for (N i = 0; i < 2; ++i)
  for (N j = 0; j < 2; ++j)
  {
    box_t parent = boxes_src[src];
    box_t child = { (parent.i << 1) + i, (parent.j << 1) + j, 0, 0 };
    child.contribution = box_contributes(child, boxes_dst_level);
    if (0 < child.contribution)
    {
      N me;
      {
        #pragma omp atomic capture
        me = dst++;
      }
      boxes_dst[me] = child;
    }
    N me;
    {
      #pragma omp atomic capture
      me = progress++;
    }
    N pc = 100 * me / (4 * boxes_src_count);
    N pc1 = 100 * (me - 1) / (4 * boxes_src_count);
    if (pc > pc1)
    {
      #pragma omp critical
      fprintf(stderr, "%lld%%\r", pc);
    }
  }
  boxes_dst_count = dst;

  qsort(boxes_dst, boxes_dst_count, sizeof(*boxes_dst), box_cmp_contrib);
  R contribution = 0;
  for (N dst = 0; dst < boxes_dst_count; ++dst)
  {
    contribution += boxes_dst[dst].contribution;
  }
  printf("contrib\t%g\n", contribution);
  for (N dst = 0; dst < boxes_dst_count; ++dst)
  {
    boxes_dst[dst].contribution /= contribution;
  }
  R accum = 0;
  for (N dst = 0; dst < boxes_dst_count; ++dst)
  {
    accum = boxes_dst[dst].cdf = boxes_dst[dst].contribution + accum;
  }
//  N most = boxes_binary_search(boxes_dst, boxes_dst_count, 1) + 1;
//  printf("prune %lld -> %lld\n", boxes_dst_count, most);
//  boxes_dst_count = most;
  accum = 0;
  for (N dst = 0; dst < boxes_dst_count; ++dst)
  {
    accum = boxes_dst[dst].cdf = boxes_dst[dst].contribution + accum;
  }

  bool ret = boxes_dst_count > 0;
  if (ret)
  {
    // shift
    deallocate(boxes_src, boxes_src_alloc);
    boxes_src = boxes_dst;
    boxes_src_alloc = boxes_dst_alloc;
    boxes_src_count = boxes_dst_count;
    boxes_src_level = boxes_dst_level;
  }
  else
  {
    deallocate(boxes_dst, boxes_dst_alloc);
  }
  boxes_dst = 0;
  boxes_dst_alloc = 0;
  boxes_dst_count = 0;
  boxes_dst_level = 0;
  return ret;
}

// point iteration

N iterate_point(C c)
{
  N i = 1;
  R cx = creal(c), cy = cimag(c);
  R zx = cx, zy = cy;
  R min_z2 = 1.0 / 0.0;
  for (; i < maximum_iterations; ++i)
  {
    R x2 = zx * zx;
    R y2 = zy * zy;
    R xy = zx * zy;
    R z2 = x2 + y2;
    if (z2 > 4)
    {
      break;
    }
    if (z2 < min_z2)
    {
      // inside atom domain
      if (12 * z2 < min_z2)
      {
        // far inside atom domain
        C w;
        m_d_attractor(&w, zx + I * zy, c, i, 16);
        C dw = 1;
        for (N j = 0; j < i; ++j)
        {
          dw = 2 * w * dw;
          w = w * w + c;
        }
        if (cnorm(dw) <= 1)
        {
          // interior
          return maximum_iterations;
        }
      }
      min_z2 = z2;
    }
    zx = x2 - y2 + cx;
    zy = 2 * xy + cy;
  }
  return i;
}

R accumulate_point(C c, N iterations, R weight)
{
  R w = ceil((1ull << 16) * weight);
  N plane = ilog2(iterations);
  N contribution = 0;
  R zx = 0, zy = 0;
  R cx = creal(c), cy = cimag(c);
  R scale = zheight / z_radius;
  R offsetx = 0.5 * zwidth - scale * creal(z_center);
  R offsety = 0.5 * zheight - scale * cimag(z_center);
  for (N n = 0; n < iterations; ++n)
  {
    R x = scale * zx + offsetx;
    R y = scale * zy + offsety;
    if (0 <= x && x < zwidth && 0 <= y && y < zheight)
    {
      N i = x;
      N j = y;
      N k = (plane * zheight + j) * zwidth + i;
      {
        #pragma omp atomic
        histogram[k] += w;
      }
      contribution += w;
    }
    R x2 = zx * zx;
    R y2 = zy * zy;
    R xy = zx * zy;
    zx = x2 - y2 + cx;
    zy = 2 * xy + cy;
  }
  return contribution;
}

R accumulate_samples(N samples)
{
  R total_contributions = 0;
  N total_samples = 0;
  #pragma omp parallel for schedule(static, 1)
  for (N thread = 0; thread < 16; ++thread)
  {
    R my_contributions = 0;
    gsl_rng *rng = gsl_rng_alloc(gsl_rng_mt19937);
    gsl_rng_set(rng, rand());
    for (N s = 0; s < samples / 16; ++s)
    {
      N src = boxes_binary_search(boxes_src, boxes_src_count, gsl_rng_uniform(rng));
      C jitter = (gsl_rng_uniform(rng) + I * gsl_rng_uniform(rng)) / (R) RAND_MAX;
      C c = box_offset(boxes_src[src], boxes_src_level, jitter);
      N iterations = iterate_point(c);
      if (0 < iterations && iterations < maximum_iterations)
      {
        R contributions = accumulate_point(c, iterations, 1 / boxes_src[src].contribution);
        my_contributions += contributions;
      }
      N me;
      #pragma omp atomic capture
      me = total_samples++;
      N pc = 100 * me / samples;
      N pc1 = 100 * (me - 1) / samples;
      if (pc > pc1)
      {
        #pragma omp critical
        fprintf(stderr, "%lld%%\r", pc);
      }
    }
    gsl_rng_free(rng);
    #pragma omp atomic
    total_contributions += my_contributions;
  }
  return total_contributions;
}

// algorithm

void algorithm(void)
{
  N histogram_alloc = sizeof(*histogram) * zwidth * zheight * ilog2(maximum_iterations);
  histogram = allocate(histogram_alloc);

  if (boxes_load())
  {
    printf("boxes loaded\n");
  }
  else
  {
    boxes_init(0);
    printf("boxes initialized\n");
  }
  printf("level %lld\tboxes %lld\tinhabited %.3g%%\n", boxes_src_level, boxes_src_count, 100.0 * boxes_src_count / (1.0 * (1ull << boxes_src_level) * (1ull << boxes_src_level)));
  z_radius = sqrt(4 / (1ull << boxes_src_level));
  z_circumradius = z_radius * hypot(zwidth, zheight) / zheight;

  while (running && boxes_refine())
  {
    z_radius = sqrt(4 / (1ull << boxes_src_level));
    z_circumradius = z_radius * hypot(zwidth, zheight) / zheight;
    printf("level %lld\tboxes %lld\tinhabited %.3g%%\n", boxes_src_level, boxes_src_count, 100.0 * boxes_src_count / (1.0 * (1ull << boxes_src_level) * (1ull << boxes_src_level)));
    boxes_save();
  }
  z_radius = sqrt(4 / (1ull << boxes_src_level));
  z_circumradius = z_radius * hypot(zwidth, zheight) / zheight;

  if (! running || boxes_src_count == 0)
  {
    deallocate(histogram, histogram_alloc);
    histogram = 0;
    return;
  }
  printf("sampling\n");
  R total_contributions = 0;
  R density_target = 1;
  for (N sampling_level = 16; running; ++sampling_level)
  {
    N samples = 1ull << sampling_level;
    total_contributions += accumulate_samples(samples);
    R density = total_contributions / (1.0 * zwidth * zheight);
    printf("sampling %lld\tcontrib %.3g\tdensity %.3g\n", sampling_level, total_contributions, density);
    if (density >= density_target)
    {
      FILE *f = fopen("bb.map", "wb");
      if (f)
      {
        fwrite(histogram, histogram_alloc, 1, f);
        fclose(f);
        printf("saved bb.map\n");
      }
      else
      {
        running = 0;
      }
    }
  }
  deallocate(histogram, histogram_alloc);
  histogram = 0;
}

// entrypoint

int main(int argc, char **argv)
{
  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

  // parse arguments
  z_center = 0;
  z_radius = 2;
  // default values mean 3.75 GiB histogram
  maximum_iterations = 1ull << 30;
  zwidth = 1ull << 12;
  zheight = 1ull << 12;
  if (argc > 3)
  {
    z_center = atof(argv[1]) + I * atof(argv[2]);
    z_radius = atof(argv[3]);
  }
  if (argc > 4)
  {
    maximum_iterations = atoll(argv[4]);
  }
  if (argc > 6)
  {
    zwidth = atoi(argv[5]);
    zheight = atoi(argv[6]);
  }
  memory_limit = 5ull << 30; // GiB

  N seed = time(0);
  printf("seed %lld\n", seed);
  srand(seed);

  algorithm();
  return 0;
}
