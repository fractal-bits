{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}

--import Control.Concurrent.Spawn (parMapIO_)
import Control.Monad (join)
import Control.Monad.ST (runST)
import Control.Parallel.Strategies (parMap, rseq)
import Data.List (minimumBy, nub, transpose)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Monoid (Sum(..))
import Data.Ord (comparing)
import Data.Vector.Storable (unsafeFreeze, unsafeWith)
import qualified Data.Vector.Storable.Mutable as S
import qualified Data.Vector.Unboxed.Mutable  as U
import Data.Word (Word8)
import Numeric.Rounded (Rounded, RoundingMode(TowardNearest), Precision, reifyPrecision)
import System.Environment (getArgs)
import System.IO (hPutBuf, hPutStr, stdout, stderr)
import Debug.Trace (trace, traceShow)

diffabs :: (Ord a, Num a) => a -> a -> a
diffabs c d =
  if (c >= 0)
    then
      if (c + d >= 0)
        then d
        else -(2 * c + d)
    else
      if (c + d > 0)
        then 2 * c + d
        else -d

er :: Double
er = 65536

er2 :: Double
er2 = er^2

type REAL p = Rounded TowardNearest p

data Delta = Delta{ a, b, x, y :: !Double }

data Region p = RegionSingleton{ ref :: !(Reference p), ix :: !(Int, Int) }
            | RegionSeries{ ref :: !(Reference p), s, t :: !(BiSeries), interior, boundary :: !(Map (Int, Int) Delta) }
            | RegionPerturb{ ref :: !(Reference p), active :: !(Map (Int, Int) Delta) }
            | RegionFinished{ done :: !(Map (Int, Int) Result) }

size :: Region p -> (Char, Int)
size r@(RegionSingleton{}) = ('1', 1)
size r@(RegionSeries{}) = ('S', M.size (interior r) + M.size (boundary r))
size r@(RegionPerturb{}) = ('P', M.size (active r))
size r@(RegionFinished{}) = ('F', M.size (done r))

data Result = ReferenceEscaped | PerturbationGlitch | Result Double deriving Show

initial :: Precision p => Int -> Int -> Int -> REAL p -> REAL p -> Double -> Region p
initial w h order a0 b0 r0 = RegionSeries
  { ref = Reference a0 b0 0 0 0
  , s = zero order
  , t = zero order
  , interior = int
  , boundary = bry
  }
  where
    (bry, int) = M.partitionWithKey (\(i, j) _ -> i == 0 || j == 0 || i == w - 1 || j == h - 1) deltas
    deltas = M.fromList
      [ ((i, j), Delta u v 0 0)
      | i <- [0 .. w - 1]
      , j <- [0 .. h - 1]
      , let u = 2 * r0 * ((fromIntegral i + 0.5) / fromIntegral w - 0.5) * fromIntegral w / fromIntegral h
      , let v = 2 * r0 * ((fromIntegral j + 0.5) / fromIntegral h - 0.5)
      ]

burningShip :: Precision p => Int -> Double -> Int -> Region p -> [[Region p]]
burningShip order e si = iterate (concat . parMap rseq (burningShipReg order e si)) . pure

escaped r x y = not $ x * x + y * y < r

smapPair f (a, b) = ((,) $! a) $! f b

interpolate r = (biSeries (s r, t r) <$> interior r) `M.union` boundary r

inaccurate e r = not . (e >) . sqrt . maximum . fmap err . boundary $ r
  where
    count = fromIntegral . M.size . boundary $ r
    err d =
      let d' = biSeries (s r, t r) d
          dx = x d - x d'
          dy = y d - y d'
          sx = (x d + x d') / 2
          sy = (y d + y d') / 2
      in  case (dx ^ 2 + dy ^ 2) / (sx ^ 2 + sy ^ 2) of
            q | isNaN q -> 0
              | otherwise -> q

result m d = Result $ fromIntegral m + 1 - log (log (sqrt (x d ^ 2 + y d ^ 2))) / log 2

f <|$|> m = M.fromAscList . parMap rseq (smapPair f) . M.toAscList $ m
infixl 4 <|$|>

isFinished RegionFinished{} = True
isFinished _ = False

burningShipReg :: Precision p => Int -> Double -> Int -> Region p -> [Region p]

burningShipReg _ _ _ RegionFinished{} = []

burningShipReg _ _ _ r@(RegionSingleton{})
  | escaped er2 (x $ toDelta (ref r)) (y $ toDelta (ref r)) = [RegionFinished{ done = M.singleton (ix r) (result (n (ref r)) (toDelta (ref r))) }]
  | otherwise = [r{ ref = burningShipRef (ref r) }]

burningShipReg _ _ _ r@(RegionPerturb{})
  | escaped er2 xr0 yr0 = [RegionFinished{ done = const ReferenceEscaped <$> active r }]
  | M.null active'' = [RegionFinished{ done = done' }]
  | otherwise = [RegionFinished{ done = done' }, r{ ref = ref', active = active'' }]
  where
    ref' = burningShipRef (ref r)
    ref_ = toDelta (ref r)
    ref_1 = toDelta ref'
    xr0 = x ref_
    yr0 = y ref_
    xr = x ref_1
    yr = y ref_1
    (newDone, active')    = M.partition (\d -> escaped er2 (x d + xr) (y d + yr)) (burningShipDel ref_ <$> active r)
    (newGlitch, active'') = M.partition (\d -> xr^2 + yr^2 > 1000 * ((xr + x d)^2 + (yr + y d)^2)) active'
    done' = (result (n ref') <$> newDone) `M.union` (const PerturbationGlitch <$> newGlitch)

burningShipReg order e si r@(RegionSeries{})
  | snd (size r) == 1 = [RegionSingleton{ ref = ref r, ix = case M.keys (interior r) ++ M.keys (boundary r) of [ij] -> ij }]
  | escaped (er2) (x $ toDelta (ref r)) (y $ toDelta (ref r)) = [RegionFinished{ done = M.fromList [ (ij, ReferenceEscaped) | ij <- M.keys (interior r) ++ M.keys (boundary r)] }]
  | n (ref r) >= si = traceShow ("forcestop", M.size (interpolate r), n (ref r)) $ [RegionPerturb{ ref = ref r, active = interpolate r }]
  | otherwise = case nub . map (burningShipFolding (toDelta $ ref r)) . M.elems $ boundary r of
      [q] -> -- optimized singleton case
        let (s', t') = burningShipSeries order q (toDelta $ ref r) (s r, t r)
            r' = r{ ref = ref', s = s', t = t', boundary = bry' }
        in  if inaccurate e r' then traceShow ("inaccurate1", M.size (interpolate r), n (ref r)) $ [RegionPerturb{ ref = ref r, active = interpolate r }] else [r']
      qs -> map newRegion qs
  where
    ref' = burningShipRef (ref r)
    bry' = burningShipDel (toDelta $ ref r) <$> boundary r
    q0 = burningShipFolding (toDelta $ ref r) (Delta 0 0 0 0)
    newRegion q
{-  | q == q0 -}= let (s', t') = burningShipSeries order q (toDelta $ ref r) (s r, t r)
                      (bry, int) = M.partitionWithKey (\ix _ -> any (`M.notMember` region) (neighbours ix)) (burningShipDel (toDelta $ ref r) <$> region)
                      r' = r{ ref = ref', s = s', t = t', interior = int, boundary = bry }
                  in  if inaccurate e r' || M.null int then traceShow ("inaccurateQ", M.size region, n (ref r)) $ RegionPerturb{ ref = ref r, active = region } else traceShow ("okQ", M.size region, n (ref r)) $ r'
--      | otherwise =                           traceShow ("noncentral", M.size region, n (ref r)) $ RegionPerturb{ ref = ref r, active = region } 
      where
        region = M.filter ((q ==) . burningShipFolding (toDelta $ ref r)) (interpolate r)
--          region = M.filter ((q ==) . burningShipFolding ref_) deltas
{-
          -- compute center of region
          (Sum is, Sum js) = M.foldMapWithKey (\(i, j) _ -> (Sum (fromIntegral i), Sum (fromIntegral j))) region
          m = fromIntegral (M.size region)
          i0 = is / m
          j0 = js / m
          -- move reference to center of region
          ij = findKeyNear (i0, j0) (M.keys region)
          d = burningShipDel ref_ $ region M.! ij
          aa' = aa ref' + realToFrac (a d)
          bb' = bb ref' + realToFrac (b d)
          xx' = xx ref' + realToFrac (x d)
          yy' = yy ref' + realToFrac (y d)
-}
{-
          -- move series to new reference
          (s', t') = burningShipSeries q ref_ (s r, t r)
--          s' = biShift (-a d) (-b d) $ s r
--          t' = biShift (-a d) (-b d) $ t r
          (bry, int) = M.partitionWithKey (\ix _ -> any (`M.notMember` region) (neighbours ix)) (({- (\d' -> Delta (a d' - a d) (b d' - b d) (x d' - x d) (y d' - y d)) . -} burningShipDel ref_) <$> region)
      in  --traceShow (n ref', ix, M.size region, M.size int, M.size bry) $
          RegionSeries
            { ref = ref'--{ aa = aa', bb = bb', xx = xx', yy = yy' }
            , s = s'
            , t = t'
            , interior = int
            , boundary = bry
            }

-}

{-
uniShift c m = go (order - 1) m
  where
    go i m | i >= 0 = go (i - 1) (go2 i m)
           | otherwise = m
      where
        go2 j m | j < order = go2 (j + 1) $ M.insert j (m ! j + m ! (j + 1) * c) m
                | otherwise = m
    m ! j = case M.lookup j m of Nothing -> 0 ; Just x -> x

biShift x y
  = twiddle . pull . fmap (uniShift x) . push
  . twiddle . pull . fmap (uniShift y) . push

push :: (Ord a, Ord b) => Map (a, b) v -> Map a (Map b v)
push m = M.fromListWith M.union [ (i, M.singleton j e) | ((i,j), e) <- M.assocs m ]

pull :: (Ord a, Ord b) => Map a (Map b v) -> Map (a, b) v
pull m = M.fromList [((i,j),e) | (i, mj) <- M.assocs m, (j, e) <- M.assocs mj ]

twiddle :: (Ord a, Ord b) => Map (a, b) v -> Map (b, a) v
twiddle = M.mapKeys (\(i,j) -> (j,i))
-}
    
{-
horner shift
for (i = n - 2; i >= 0; i--)
for (j = i; j < n - 1; j++)
fmpz_addmul(poly + j, poly + j + 1, c); // poly[j] += poly[j+1]*c

a_0 x^0 + a_1 x^1 + a_2 x^2 + a_3 x^3
->
a_2 += a_3 * c

a_1 += a_2 * c
a_2 += a_3 * c

a_0 += a_1 * c
a_1 += a_2 * c
a_2 += a_3 * c
-}


neighbours (i, j) = [ (i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1) ]
findKeyNear (i0, j0) = minimumBy (comparing d2)
  where d2 (i, j) = let x = fromIntegral i - i0 ; y = fromIntegral j - j0 in x * x + y * y

data Reference p = Reference{ aa, bb, xx, yy :: !(REAL p), n :: !Int }

toDelta :: Precision p => Reference p -> Delta
toDelta r = Delta
  { a = realToFrac (aa r)
  , b = realToFrac (bb r)
  , x = realToFrac (xx r)
  , y = realToFrac (yy r)
  }

burningShipRef :: Precision p => Reference p -> Reference p
burningShipRef r = r
  { xx = xx r ^ 2 - yy r ^ 2 + aa r
  , yy = abs (2 * xx r * yy r) + bb r
  , n = n r + 1
  }

burningShipDel :: Delta -> Delta -> Delta
burningShipDel r d = d
  { x = 2 * x r * x d - 2 * y r * y d + x d ^ 2 - y d ^ 2 + a d
  , y = 2 * diffabs (x r * y r) (x r * y d + x d * y r + x d * y d) + b d
  }

burningShipFolding :: Delta -> Delta -> Int
burningShipFolding r d = diffabsIx (x r * y r) (x r * y d + x d * y r + x d * y d)
{-
  | x r + x d >= 0 && y r + y d >= 0 = 0
  | x r + x d <  0 && y r + y d >= 0 = 1
  | x r + x d <  0 && y r + y d <  0 = 2
  | x r + x d >= 0 && y r + y d <  0 = 3
  | otherwise = traceShow (x r, x d, x r + x d, y r, y d, y r + y d) 0
-}

type BiSeries = Map (Int, Int) Double

burningShipSeries order q r (s, t) = (s', t')
  where
    s' = M.fromList [((i,j),
      (if (i,j) == (1,0) then 1 else 0) +
      2 * xr * s M.! (i,j) - 2 * yr * t M.! (i,j) +
      sum [ s M.! (k, l) * s M.! (i - k, j - l)
          - t M.! (k, l) * t M.! (i - k, j - l)
          | k <- [0..i], l <- [0..j] ])
      | i <- [0..order], j <- [0..order], i + j <= order ]
    t' = M.fromList [((i,j),
      (if (i,j) == (0,1) then 1 else 0) +
      case q of
        0 ->  2 * (xr * t M.! (i, j) + s M.! (i, j) * yr + sum [ s M.! (k, l) * t M.! (i - k, j - l) | k <- [0..i], l <- [0..j] ])
        1 -> -2 * (xr * t M.! (i, j) + s M.! (i, j) * yr + sum [ s M.! (k, l) * t M.! (i - k, j - l) | k <- [0..i], l <- [0..j] ] + if (i,j) == (0,0) then 2 * xr * yr else 0)
        2 ->  2 * (xr * t M.! (i, j) + s M.! (i, j) * yr + sum [ s M.! (k, l) * t M.! (i - k, j - l) | k <- [0..i], l <- [0..j] ] + if (i,j) == (0,0) then 2 * xr * yr else 0)
        3 -> -2 * (xr * t M.! (i, j) + s M.! (i, j) * yr + sum [ s M.! (k, l) * t M.! (i - k, j - l) | k <- [0..i], l <- [0..j] ])
      )
      | i <- [0..order], j <- [0..order], i + j <= order ]
    xr = x r
    yr = y r

zero order = M.fromList [((i,j), 0) | i <- [0..order], j <- [0..order], i + j <= order ]

biSeries (s, t) d = d{ x = biSeries1 s (ad) (bd), y = biSeries1 t (ad) (bd) }
  where ad = a d
        bd = b d
biSeries1 s a b = sum [ e * a^i * b^j | ((i, j), e) <- M.assocs s ]

{-
substitute

x = sum_{i>=0,j>=0,i+j>0} S_{i,j} a^i b^j
y = sum_{i>=0,j>=0,i+j>0} T_{i,j} a^i b^j

into

x := 2 X x - 2 Y y + x^2 - y^2 + a
y := 2 * diffabs (X Y) (X y + x Y + x y) + b

diffabs :: (Ord a, Num a) => a -> a -> a
diffabs c d =
  if (c >= 0)
    then
      if (c + d >= 0)
        then d               -- y :=  2 * (        X y + x Y + x y) + b
        else -(2 * c + d)    -- y := -2 * (2 X Y + X y + x Y + x y) + b
    else
      if (c + d > 0)
        then 2 * c + d       -- y :=  2 * (2 X Y + X y + x Y + x y) + b
        else -d              -- y := -2 * (        X y + x Y + x y) + b


and equate coefficients gives iteration formulas for the series approximation
-}

diffabsIx :: (Ord a, Num a) => a -> a -> Int
diffabsIx c d =
  if (c >= 0)
    then
      if (c + d >= 0)
        then 0
        else 1
    else
      if (c + d > 0)
        then 2
        else 3

main' :: Precision p => Int -> Int -> Int -> Int -> Double -> Int -> Rounded TowardNearest p -> Rounded TowardNearest p -> Double -> proxy p -> [((Int, Int), Result)]
main' w h maxiters order threshold seriesiters a0 b0 r0 _prec
  = concatMap (M.toList . done)
  . filter isFinished
  . concat
  . takeWhile (not . null)
  . take maxiters
  . burningShip order threshold seriesiters
  $ initial w h order a0 b0 r0

plot1 w h maxiters u ((i, j), r) = let k = j * w + i in case r of
    ReferenceEscaped   -> U.write u k (-1/0)
    PerturbationGlitch -> U.write u k (-1/0)
    Result mu          -> U.write u k (mu)

colour1 w h u v (i, j) = do
  let i1 = if i == 0 then i + 1 else i - 1
      j1 = if j == 0 then j + 1 else j - 1
  (n00) <- U.read u (j  * w + i )
  (n01) <- U.read u (j  * w + i1)
  (n10) <- U.read u (j1 * w + i )
  (n11) <- U.read u (j1 * w + i1)
  let du =  (n00 - n11)
      dv =  (n01 - n10)
      de = du^2 + dv^2
      wo :: Word8
      l = 20 * log de
      wo = floor l 
      (r,g,b)
        | n00 == 0 = (0, 0, 0) -- unescaped
        | n00 == -1/0 = (255, 0, 0) -- glitch
        | isNaN de = (0, 0, 255) -- error?
        | otherwise = (wo, 128, floor n00)
      k = 3 * (j * w + i)
  S.write v (k + 0) r
  S.write v (k + 1) g
  S.write v (k + 2) b

clamp x lo hi = lo `max` x `min` hi

plot w h maxiters xs = runST (do
  u <- U.new (w * h)
  mapM_ (plot1 w h maxiters u) xs
  v <- S.new (w * h * 3)
  mapM_ (colour1 w h u v) [ (i,j) | i <- [0 .. w - 1], j <- [0 .. h - 1] ]
  unsafeFreeze v)

main :: IO ()
main = do
  [sw, sh, smaxiters, sorder, sthreshold, sseriesiters, sa0, sb0, sr0] <- getArgs
  let w = read sw
      h = read sh
{-
      a0s = "-1.765724070777579936114404613125203747548323196202122799408000137"
      b0s = "-0.041893714510242382733641617814193925623948756785781053295999999"
      r0 = 2 / 9.9035203142830852e27
      maxiters = 200100
-}
{-
  let a0 = -1.775015718882760856139042287908955655837994562692664464768
      b0 = -0.046245056219157170454983287250662144618728655141566663527
      r0 = 2 / 1.2980742146337048E33
      maxiters = 5100
-}
      maxiters = read smaxiters
      order = read sorder
      threshold = read sthreshold
      seriesiters = read sseriesiters
      r0 = read sr0
      prec = ceiling $ 24 - logBase 2 r0
      img = plot w h maxiters $ reifyPrecision prec $ main' w h maxiters order threshold seriesiters (read sa0) (read sb0) r0
  hPutStr stderr $ "precision bits: " ++ show prec ++ "\n"
  hPutStr stdout $ "P6\n" ++ show w ++ " " ++ show h ++ "\n255\n"
  unsafeWith img (\p -> hPutBuf stdout p (w * h * 3))
