/*
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}

import Control.Concurrent.Spawn (parMapIO_)
import Control.Monad (join)
import Control.Monad.ST (runST)
import Control.Parallel.Strategies (parMap, rseq)
import Data.List (minimumBy, nub, transpose)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.Monoid (Sum(..))
import Data.Ord (comparing)
import Data.Vector.Storable (unsafeFreeze, unsafeWith)
import qualified Data.Vector.Storable.Mutable as S
import qualified Data.Vector.Unboxed.Mutable  as U
import Data.Word (Word8)
import Numeric.Rounded (Rounded, RoundingMode(TowardNearest), Precision, reifyPrecision)
import System.Environment (getArgs)
import System.IO (hPutBuf, hPutStr, stdout, stderr)
import Debug.Trace (trace, traceShow)

diffabs :: (Ord a, Num a) => a -> a -> a
diffabs c d =
  if (c >= 0)
    then
      if (c + d >= 0)
        then d
        else -(2 * c + d)
    else
      if (c + d > 0)
        then 2 * c + d
        else -d
*/

#include <cassert>
#include <cstdint>
#include <cstdio>
#include <iostream>
#include <map>
#include <optional>
#include <set>
#include <vector>

#include <mpreal.h>

template <typename T>
T diffabs(const T &c, const T &d)
{
  if (c >= 0) if (c + d >= 0) return d; else return -(2 * c + d);
  else if (c + d > 0) return 2 * c + d; else return -d;
}

/*
er :: Double
er = 65536

er2 :: Double
er2 = er^2

type REAL p = Rounded TowardNearest p
*/

typedef uint8_t N_8;
typedef uint16_t N_16;
typedef int64_t Z_64;
typedef double R_lo;
typedef mpfr::mpreal R_hi;

/*
const R_lo er = 65536;
const R_lo er2 = er * er;
*/

typedef std::pair<N_16,N_16> Coord;

/*
neighbours (i, j) = [ (i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1) ]
*/

std::vector<Coord> neighbours(const Coord &ij)
{
  std::vector<Coord> r;
  auto i = ij.first;
  auto j = ij.second;
  r.push_back(Coord(i-1,j));
  r.push_back(Coord(i+1,j));
  r.push_back(Coord(i,j+1));
  r.push_back(Coord(i,j-1));
  return r;
}

/*
data Reference p = Reference{ aa, bb, xx, yy :: !(REAL p), n :: !Int }
*/

/**
struct Reference
{
  R_hi a, b, x, y;
  Z_64 n;
  Reference(R_hi a, R_hi b, R_hi x, R_hi y, Z_64 n) : a(a), b(b), x(x), y(y), n(n) { };
};

Reference burningShipRef(const Reference &r)
{
  return Reference(r.a, r.b, sqr(r.x) - sqr(r.y) + r.a, abs(2 * r.x * r.y) + r.b, r.n + 1);
}
*/

struct Orbit
{
  std::vector<R_lo> x, y, g;
  Orbit(R_hi a, R_hi b, Z_64 maxiters, R_lo glitch_threshold, R_lo er2)
  {
    R_hi zx = 0;
    R_hi zy = 0;
    x.reserve(maxiters);
    y.reserve(maxiters);
    g.reserve(maxiters);
    for (auto n = 0; n < maxiters; ++n)
    {
      R_lo rx(zx);
      R_lo ry(zy);
      R_lo r2 = (rx * rx + ry * ry) * glitch_threshold;
      x.push_back(rx);
      y.push_back(ry);
      g.push_back(r2);
      if (r2 > er2)
        break;
      R_hi xn = sqr(zx) - sqr(zy) + a;
      R_hi yn = abs(2 * zx * zy) + b;
      zx = xn;
      zy = yn;
    }
    x.shrink_to_fit();
    y.shrink_to_fit();
    g.shrink_to_fit();
  }
};

/*
data Delta = Delta{ a, b, x, y :: !Double }

toDelta :: Precision p => Reference p -> Delta
toDelta r = Delta
  { a = realToFrac (aa r)
  , b = realToFrac (bb r)
  , x = realToFrac (xx r)
  , y = realToFrac (yy r)
  }
*/

struct Delta
{
  R_lo a, b, x, y;
  Delta(R_lo a, R_lo b, R_lo x, R_lo y) : a(a), b(b), x(x), y(y) {};
//  Delta(const Reference &r) : a(r.a), b(r.b), x(r.x), y(r.y) {};
  Delta() : a(0), b(0), x(0), y(0) {};
};

/*
burningShipDel :: Delta -> Delta -> Delta
burningShipDel r d = d
  { x = 2 * x r * x d - 2 * y r * y d + x d ^ 2 - y d ^ 2 + a d
  , y = 2 * diffabs (x r * y r) (x r * y d + x d * y r + x d * y d) + b d
  }
*/

Delta burningShipDel(const Delta &r, const Delta &d)
{
  return Delta
    ( d.a
    , d.b
    , (2 * r.x + d.x) * d.x - (2 * r.y + d.y) * d.y + d.a
    , 2 * diffabs(r.x * r.y, r.x * d.y + d.x * r.y + d.x * d.y) + d.b
    );
}

enum Quadrant
{ Q0
, Q1
, Q2
, Q3
};

/*
burningShipFolding :: Delta -> Delta -> Int
burningShipFolding r d = diffabsIx (x r * y r) (x r * y d + x d * y r + x d * y d)
{-
  | x r + x d >= 0 && y r + y d >= 0 = 0
  | x r + x d <  0 && y r + y d >= 0 = 1
  | x r + x d <  0 && y r + y d <  0 = 2
  | x r + x d >= 0 && y r + y d <  0 = 3
  | otherwise = traceShow (x r, x d, x r + x d, y r, y d, y r + y d) 0
-}

diffabsIx :: (Ord a, Num a) => a -> a -> Int
diffabsIx c d =
  if (c >= 0)
    then
      if (c + d >= 0)
        then 0
        else 1
    else
      if (c + d > 0)
        then 2
        else 3
*/

Quadrant burningShipFolding(const Delta &r, const Delta &d)
{
  R_lo c = r.x * r.y;
  R_lo cd = c + r.x * d.y + d.x * r.y + d.x * d.y;
  if (c >= 0)
    if (cd >= 0)
      return Q0;
    else
      return Q1;
  else
    if (cd > 0)
      return Q2;
    else
      return Q3;
}

Quadrant burningShipFolding(const Delta &r) // specialized for d = 0 0
{
  if (r.x * r.y >= 0) return Q0; else return Q3;
}

typedef double Result;
const Result Unescaped          =  0;
const Result ReferenceEscaped   = -1;
const Result PerturbationGlitch = -2;

/*
result m d = Result $ fromIntegral m + 1 - log (log (sqrt (x d ^ 2 + y d ^ 2))) / log 2
*/

Result result(Z_64 m, R_lo x, R_lo y)
{
  return m + 1 - log2(log(sqrt(x * x + y * y)));
}


/*
type BiSeries = Map (Int, Int) Double
*/

typedef std::map<Coord,R_lo> BiSeries;

/*
zero order = M.fromList [((i,j), 0) | i <- [0..order], j <- [0..order], i + j <= order ]
*/

BiSeries ZeroSeries(N_16 order)
{
  BiSeries s;
  for (auto i = 0; i <= order; ++i)
    for (auto j = 0; j <= order - i; ++j)
      s[Coord(i,j)] = 0;
  return s;
}

/*
biSeries1 s a b = sum [ e * a^i * b^j | ((i, j), e) <- M.assocs s ]
*/

R_lo biSeries1(N_16 order, const BiSeries &s, R_lo a, R_lo b)
{
  R_lo an[order + 1];
  R_lo bn[order + 1];
  an[0] = 1;
  bn[0] = 1;
  for (auto i = 1; i <= order; ++i)
  {
    an[i] = an[i-1] * a;
    bn[i] = bn[i-1] * b;
  }
  R_lo total = 0;
  for (auto i = 0; i <= order; ++i)
  {
    R_lo subtotal = 0;
    for (auto j = 0; j <= order - i; ++j)
      subtotal += s.at(Coord(i,j)) * bn[j];
    total += subtotal * an[i];
  }
  return total;
}

/*
biSeries (s, t) d = d{ x = biSeries1 s (ad) (bd), y = biSeries1 t (ad) (bd) }
  where ad = a d
        bd = b d
*/

Delta biSeries(N_16 order, const BiSeries &s, const BiSeries &t, const Delta &d)
{
  return Delta
    ( d.a
    , d.b
    , biSeries1(order, s, d.a, d.b)
    , biSeries1(order, t, d.a, d.b)
    );
}

/*
burningShipSeries order q r (s, t) = (s', t')
  where
    s' = M.fromList [((i,j),
      (if (i,j) == (1,0) then 1 else 0) +
      2 * xr * s M.! (i,j) - 2 * yr * t M.! (i,j) +
      sum [ s M.! (k, l) * s M.! (i - k, j - l)
          - t M.! (k, l) * t M.! (i - k, j - l)
          | k <- [0..i], l <- [0..j] ])
      | i <- [0..order], j <- [0..order], i + j <= order ]
    t' = M.fromList [((i,j),
      (if (i,j) == (0,1) then 1 else 0) +
      case q of
        0 ->  2 * (xr * t M.! (i, j) + s M.! (i, j) * yr + sum [ s M.! (k, l) * t M.! (i - k, j - l) | k <- [0..i], l <- [0..j] ])
        1 -> -2 * (xr * t M.! (i, j) + s M.! (i, j) * yr + sum [ s M.! (k, l) * t M.! (i - k, j - l) | k <- [0..i], l <- [0..j] ] + if (i,j) == (0,0) then 2 * xr * yr else 0)
        2 ->  2 * (xr * t M.! (i, j) + s M.! (i, j) * yr + sum [ s M.! (k, l) * t M.! (i - k, j - l) | k <- [0..i], l <- [0..j] ] + if (i,j) == (0,0) then 2 * xr * yr else 0)
        3 -> -2 * (xr * t M.! (i, j) + s M.! (i, j) * yr + sum [ s M.! (k, l) * t M.! (i - k, j - l) | k <- [0..i], l <- [0..j] ])
      )
      | i <- [0..order], j <- [0..order], i + j <= order ]
    xr = x r
    yr = y r
*/

std::pair<BiSeries, BiSeries> burningShipSeries(N_16 order, Quadrant q, const Delta &r, const BiSeries &s, const BiSeries &t)
{
  BiSeries sn, tn;
  for (auto i = 0; i <= order; ++i)
  {
    for (auto j = 0; j <= order - i; ++j)
    {
      const Coord ij(i, j);
      // s
      R_lo total = 0;
      if (i == 1 && j == 0) total += 1;
      total += 2 * (r.x * s.at(ij) - r.y * t.at(ij));
      for (auto k = 0; k < i; ++k)
      {
        const auto ik = i - k;
        for (auto l = 0; l < j; ++l)
        {
          const auto jl = j - l;
          const Coord kl(k, l);
          const Coord ikjl(ik, jl);
          total += s.at(kl) * s.at(ikjl) - t.at(kl) * t.at(ikjl);
        }
      }
      sn[ij] = total;
      // t;
      total = 0;
      total += r.x * t.at(ij) + r.y * s.at(ij);
      for (auto k = 0; k < i; ++k)
      {
        const auto ik = i - k;
        for (auto l = 0; l < j; ++l)
        {
          const auto jl = j - l;
          const Coord kl(k, l);
          const Coord ikjl(ik, jl);
          total += s.at(kl) * t.at(ikjl);
        }
      }
      if (i == 0 && j == 0 && (q == Q1 || q == Q2)) total += 2 * r.x * r.y;
      total *= 2;
      if (q == Q1 || q == Q3) total = -total;
      if (i == 0 && j == 1) total += 1;
      tn[ij] = total;
    }
  }
  return std::pair<BiSeries,BiSeries>(sn, tn);
}

/*
data Region p = RegionSingleton{ ref :: !(Reference p), ix :: !(Int, Int) }
            | RegionSeries{ ref :: !(Reference p), s, t :: !(BiSeries), interior, boundary :: !(Map (Int, Int) Delta) }
            | RegionPerturb{ ref :: !(Reference p), active :: !(Map (Int, Int) Delta) }
            | RegionFinished{ done :: !(Map (Int, Int) Result) }
*/

enum RegionType
{
  RegionTypeFinished,
/**
  RegionTypeSingleton,
*/
  RegionTypePerturb,
  RegionTypeSeries
};

typedef std::map<Coord,Delta> Deltas;

typedef std::map<Coord,Result> RegionFinished;

/**
struct RegionSingleton
{
  const Orbit &orbit;
  Z_64 n;
  Coord ij;
  RegionSingleton(const Orbit &orbit, Z_64 n, Coord ij) : orbit(orbit), n(n), ij(ij) { };
};
*/

struct RegionPerturb
{
  const Orbit orbit;
  Z_64 n;
  Deltas active;
  RegionPerturb(const Orbit &orbit, Z_64 n, const Deltas &active) : orbit(orbit), n(n), active(active) { };
};

struct RegionSeries
{
  const Orbit orbit;
  Z_64 n;
  BiSeries s, t;
  Deltas interior, boundary;
  RegionSeries
  ( const Orbit &orbit
  , const Z_64 &n
  , const BiSeries &s
  , const BiSeries &t
  , const Deltas &interior
  , const Deltas &boundary
  )
  : orbit(orbit), n(n), s(s), t(t), interior(interior), boundary(boundary)
  { };
};

struct Region
{
  RegionType tag;
  std::optional<RegionFinished> Finished;
/**
  std::optional<RegionSingleton> Singleton;
*/
  std::optional<RegionPerturb> Perturb;
  std::optional<RegionSeries> Series;
  Region(const RegionFinished  &r) : tag(RegionTypeFinished),  Finished (r) { };
/**
  Region(const RegionSingleton &r) : tag(RegionTypeSingleton), Singleton(r) { };
*/
  Region(const RegionPerturb   &r) : tag(RegionTypePerturb),   Perturb  (r) { };
  Region(const RegionSeries    &r) : tag(RegionTypeSeries) ,   Series   (r) { };
};

/*
size :: Region p -> (Char, Int)
size r@(RegionSingleton{}) = ('1', 1)
size r@(RegionSeries{}) = ('S', M.size (interior r) + M.size (boundary r))
size r@(RegionPerturb{}) = ('P', M.size (active r))
size r@(RegionFinished{}) = ('F', M.size (done r))
*/


Z_64 size(const RegionFinished &r) { return r.size(); }
/**
Z_64 size(const RegionSingleton &r) { (void) r; return 1; }
*/
Z_64 size(const RegionPerturb &r) { return r.active.size(); }
Z_64 size(const RegionSeries &r) { return r.interior.size() + r.boundary.size(); }

Z_64 size(const Region &r)
{
  switch (r.tag)
  {
    case RegionTypeFinished:  return size(r.Finished .value());
/**
    case RegionTypeSingleton: return size(r.Singleton.value());
*/
    case RegionTypePerturb:   return size(r.Perturb  .value());
    case RegionTypeSeries:    return size(r.Series   .value());
  }
  assert(!"size(Region) tag");
}

/*
interpolate r = (biSeries (s r, t r) <$> interior r) `M.union` boundary r
*/

Deltas interpolate(N_16 order, const RegionSeries &r)
{
  Deltas rs;
  for (auto d : r.boundary)
    rs[d.first] = d.second;
  for (auto d : r.interior)
    rs[d.first] = biSeries(order, r.s, r.t, d.second);
  return rs;
}





/*
initial :: Precision p => Int -> Int -> Int -> REAL p -> REAL p -> Double -> Region p
initial w h order a0 b0 r0 = RegionSeries
  { ref = Reference a0 b0 0 0 0
  , s = zero order
  , t = zero order
  , interior = int
  , boundary = bry
  }
  where
    (bry, int) = M.partitionWithKey (\(i, j) _ -> i == 0 || j == 0 || i == w - 1 || j == h - 1) deltas
    deltas = M.fromList
      [ ((i, j), Delta u v 0 0)
      | i <- [0 .. w - 1]
      , j <- [0 .. h - 1]
      , let v = 2 * r0 * ((fromIntegral j + 0.5) / fromIntegral h - 0.5)
      ]
*/

RegionSeries initial(N_16 w, N_16 h, Z_64 maxiters, N_16 order, R_lo glitch_threshold, R_lo er2, R_hi a0, R_hi b0, R_lo r0)
{
  Deltas interior, boundary;
  R_lo y = 2 * r0;
  R_lo x = y * w / h;
  for (N_16 i = 0; i < w; ++i)
  {
    R_lo u = x * ((i + 0.5) / w - 0.5);
    for (N_16 j = 0; j < h; ++j)
    {
      R_lo v = y * ((j + 0.5) / h - 0.5);
      ((i == 0 || j == 0 || i == w - 1 || j == h - 1) ? boundary : interior)
      [Coord(i,j)] = Delta(u, v, 0, 0);
    }
  }
  return RegionSeries(Orbit(a0,b0,maxiters,glitch_threshold,er2),0,ZeroSeries(order),ZeroSeries(order),interior,boundary);
}

/*
burningShip :: Precision p => Int -> Double -> Int -> Region p -> [[Region p]]
burningShip order e si = iterate (concat . parMap rseq (burningShipReg order e si)) . pure

escaped r x y = not $ x * x + y * y < r

smapPair f (a, b) = ((,) $! a) $! f b

*/

bool escaped(R_lo er2, R_lo x, R_lo y)
{
  return ! (x * x + y * y < er2);
}

bool escaped(R_lo er2, const Delta &d)
{
  return escaped(er2, d.x, d.y);
}

bool inaccurate(N_16 order, R_lo threshold, const RegionSeries &r)
{
  R_lo error = 0;
  for (auto m : r.boundary)
  {
    const Delta &d = m.second;
    const Delta &s = biSeries(order, r.s, r.t, d);
    R_lo dx = d.x - s.x;
    R_lo dy = d.y - s.y;
    R_lo sx = (d.x + s.x)/2;
    R_lo sy = (d.y + s.y)/2;
    R_lo q = (dx * dx + dy * dy) / (sx * sx + sy * sy);
    if (std::isnan(q) || std::isinf(q)) q = 0;
    error = std::max(error, q);
  }
  error = sqrt(error);
  return !(error < threshold);
}

/*
inaccurate e r = not . (e >) . sqrt . maximum . fmap err . boundary $ r
  where
    count = fromIntegral . M.size . boundary $ r
    err d =
      let d' = biSeries (s r, t r) d
          dx = x d - x d'
          dy = y d - y d'
          sx = (x d + x d') / 2
          sy = (y d + y d') / 2
      in  case (dx ^ 2 + dy ^ 2) / (sx ^ 2 + sy ^ 2) of
            q | isNaN q || isInfinite q -> 0
              | otherwise -> q
*/

/*
f <|$|> m = M.fromAscList . parMap rseq (smapPair f) . M.toAscList $ m
infixl 4 <|$|>

isFinished RegionFinished{} = True
isFinished _ = False

burningShipReg :: Precision p => Int -> Double -> Int -> Region p -> [Region p]

burningShipReg _ _ _ RegionFinished{} = []

burningShipReg _ _ _ r@(RegionSingleton{})
  | escaped er2 (x $ toDelta (ref r)) (y $ toDelta (ref r)) = [RegionFinished{ done = M.singleton (ix r) (result (n (ref r)) (toDelta (ref r))) }]
  | otherwise = [r{ ref = burningShipRef (ref r) }]

*/

/**
std::vector<Region> burningShipReg(const RegionSingleton &r, R_lo er2)
{
  std::vector<Region> rs;
  Delta dr(r.ref);
  if (escaped(er2, dr))
  {
    RegionFinished r2;
    r2[r.ij] = result(r.ref.n, dr);
    rs.push_back(Region(r2));
  }
  else
  {
    RegionSingleton r2(r);
    r2.ref = burningShipRef(r.ref);
    rs.push_back(Region(r2));
  }
  return rs;
}
*/

/*
burningShipReg _ _ _ r@(RegionPerturb{})
  | escaped er2 xr0 yr0 = [RegionFinished{ done = const ReferenceEscaped <$> active r }]
  | M.null active'' = [RegionFinished{ done = done' }]
  | otherwise = [RegionFinished{ done = done' }, r{ ref = ref', active = active'' }]
  where
    ref' = burningShipRef (ref r)
    ref_ = toDelta (ref r)
    ref_1 = toDelta ref'
    xr0 = x ref_
    yr0 = y ref_
    xr = x ref_1
    yr = y ref_1
    (newDone, active')    = M.partition (\d -> escaped er2 (x d + xr) (y d + yr)) (burningShipDel ref_ <$> active r)
    (newGlitch, active'') = M.partition (\d -> xr^2 + yr^2 > 1000 * ((xr + x d)^2 + (yr + y d)^2)) active'
    done' = (result (n ref') <$> newDone) `M.union` (const PerturbationGlitch <$> newGlitch)
*/

const double glitch_threshold = 0.001;

std::vector<Region> burningShipReg(const RegionPerturb &r, Z_64 maxiters, R_lo er2)
{
  std::vector<Region> rs;
  if (escaped(er2, r.orbit.x[r.n], r.orbit.y[r.n]))
  {
std::cerr << "r.n " << r.n << std::endl;
    RegionFinished done;
    for (auto d : r.active)
    {
      done[d.first] = ReferenceEscaped;
    }
    rs.push_back(Region(done));
  }
  else
  {
    RegionFinished done;
//    Deltas active;
//    Reference ref2 = burningShipRef(r.ref);
//    Delta dr2(ref2);
//    R_lo gr2 = glitch_threshold * (dr2.x * dr2.x + dr2.y * dr2.y);
//    #pragma omp parallel for
    const Z_64 count = r.active.size();
std::cerr << "count " << count << std::endl;
    std::vector<Coord> ijs;
    std::vector<Delta> ds;
    ijs.reserve(count);
    ds.reserve(count);
    for (auto d : r.active)
    {
      ijs.push_back(d.first);
      ds.push_back(d.second);
    }
    const Z_64 osize = r.orbit.x.size();
    const Z_64 m = osize - 1;
std::cerr << "osize " << osize << std::endl;
std::cerr << "maxiters " << maxiters << std::endl;
    #pragma omp parallel for
    for (Z_64 ix = 0; ix < count; ++ix)
    {
      Coord ij = ijs[ix];
      Delta d = ds[ix];
      R_lo a = d.a;
      R_lo b = d.b;
      R_lo x = d.x;
      R_lo y = d.y;
      bool finished = false;
      for (Z_64 n = r.n; n < m; ++n)
      {
        R_lo X = r.orbit.x[n];
        R_lo Y = r.orbit.y[n];
        R_lo Xn = r.orbit.x[n+1];
        R_lo Yn = r.orbit.y[n+1];
        R_lo Gn = r.orbit.g[n+1];
        R_lo xn = (2 * X + x) * x - (2 * Y + y) * y + a;
        R_lo yn = 2 * diffabs(X * Y, X * y + x * Y + x * y) + b;
        R_lo xx = Xn + xn;
        R_lo yy = Yn + yn;
        R_lo z2 = xx * xx + yy * yy;
        x = xn;
        y = yn;
        if (! (z2 < er2))
        {
          #pragma omp critical
          done[ij] = result(n, xx, yy);
          finished = true;
          break;
        }
        else if (! (z2 > Gn))
        {
          #pragma omp critical
          done[ij] = PerturbationGlitch;
          finished = true;
          break;
        }
      }
      if (! finished)
      {
        #pragma omp critical
        done[ij] = osize == maxiters ? Unescaped : ReferenceEscaped;
      }
    }
    if (size(done) > 0)
      rs.push_back(Region(done));
/**
    if (active.size() > 0)
      rs.push_back(Region(RegionPerturb(ref2, active)));
*/
  }
  return rs;
}


/*
burningShipReg order e si r@(RegionSeries{})
  | snd (size r) == 1 = [RegionSingleton{ ref = ref r, ix = case M.keys (interior r) ++ M.keys (boundary r) of [ij] -> ij }]
  | escaped (er2) (x $ toDelta (ref r)) (y $ toDelta (ref r)) = [RegionFinished{ done = M.fromList [ (ij, ReferenceEscaped) | ij <- M.keys (interior r) ++ M.keys (boundary r)] }]
  | n (ref r) >= si = traceShow ("forcestop", M.size (interpolate r), n (ref r)) $ [RegionPerturb{ ref = ref r, active = interpolate r }]
  | otherwise = case nub . map (burningShipFolding (toDelta $ ref r)) . M.elems $ boundary r of
      [q] -> -- optimized singleton case
        let (s', t') = burningShipSeries order q (toDelta $ ref r) (s r, t r)
            r' = r{ ref = ref', s = s', t = t', boundary = bry' }
        in  if inaccurate e r' then traceShow ("inaccurate1", M.size (interpolate r), n (ref r)) $ [RegionPerturb{ ref = ref r, active = interpolate r }] else [r']
      qs -> map newRegion qs
  where
    ref' = burningShipRef (ref r)
    bry' = burningShipDel (toDelta $ ref r) <$> boundary r
    q0 = burningShipFolding (toDelta $ ref r) (Delta 0 0 0 0)
    newRegion q
      | q == q0 = let (s', t') = burningShipSeries order q (toDelta $ ref r) (s r, t r)
                      (bry, int) = M.partitionWithKey (\ix _ -> any (`M.notMember` region) (neighbours ix)) (burningShipDel (toDelta $ ref r) <$> region)
                      r' = r{ ref = ref', s = s', t = t', interior = int, boundary = bry }
                  in  if inaccurate e r' then traceShow ("inaccurateQ", M.size region, n (ref r)) $ RegionPerturb{ ref = ref r, active = region } else traceShow ("okQ", M.size region, n (ref r)) $ r'
      | otherwise =                           traceShow ("noncentral", M.size region, n (ref r)) $ RegionPerturb{ ref = ref r, active = region } 
      where
        region = M.filter ((q ==) . burningShipFolding (toDelta $ ref r)) (interpolate r)
*/

std::vector<Region> burningShipReg(const RegionSeries &r, N_16 order, R_lo series_threshold, Z_64 series_iters, R_lo er2)
{
  std::vector<Region> rs;
/**
  if (size(r) == 1)
  {
    for (auto d : r.interior)
      rs.push_back(Region(RegionSingleton(r.orbit, r.n, d.first)));
    for (auto d : r.boundary)
      rs.push_back(Region(RegionSingleton(r.orbit, r.n, d.first)));
  }
  else
*/
  {
    Delta dr(0, 0, r.orbit.x[r.n], r.orbit.y[r.n]);
    if (escaped(er2, dr))
    {
      RegionFinished done;
      for (auto d : r.boundary)
        done[d.first] = ReferenceEscaped;
      for (auto d : r.interior)
        done[d.first] = ReferenceEscaped;
      rs.push_back(Region(done));
    }
    else if (r.n >= series_iters)
    {
      rs.push_back(Region(RegionPerturb(r.orbit, r.n, interpolate(order, r))));
    }
    else
    {
      Deltas newBoundary;
      std::set<Quadrant> qs;
      Quadrant q;
      for (auto d : r.boundary)
      {
        newBoundary[d.first] = burningShipDel(dr, d.second);
        qs.insert(q = burningShipFolding(dr, d.second));
      }

      if (qs.size() == 1)
      {
        auto st = burningShipSeries(order, q, dr, r.s, r.t);
        RegionSeries r2(r.orbit, r.n + 1, st.first, st.second, r.interior, newBoundary);
        if (inaccurate(order, series_threshold, r2))
        {
          rs.push_back(Region(RegionPerturb(r.orbit, r.n, interpolate(order, r))));
        }
        else
        {
          rs.push_back(Region(r2));
        }
      } else {
        Quadrant q0 = burningShipFolding(dr);
        for (auto q : qs)
        {
          Deltas region;
          for (auto ds : interpolate(order, r))
            if (q == burningShipFolding(dr, ds.second))
              region[ds.first] = ds.second;
          if (q == q0)
          {
            auto st = burningShipSeries(order, q, dr, r.s, r.t);
            Deltas boundary, interior;
            for (auto ds : region)
            {
              Delta d = burningShipDel(dr, ds.second);
              bool isEdge = false;
              for (auto ix : neighbours(ds.first))
                if (! region.count(ix))
                  isEdge = true;
              (isEdge ? boundary : interior)[ds.first] = d;
            }
            RegionSeries r2(r.orbit, r.n + 1, st.first, st.second, interior, boundary);
            if (inaccurate(order, series_threshold, r2))
              rs.push_back(Region(RegionPerturb(r.orbit, r.n, region)));
            else
              rs.push_back(Region(r2));
          }
          else
          {
            rs.push_back(Region(RegionPerturb(r.orbit, r.n, region)));
          }
        }
      }
    }
  }
  return rs;
}


std::vector<Region> burningShipReg(const Region &r, Z_64 maxiters, N_16 order, R_lo series_threshold, Z_64 series_iters, R_lo er2, R_lo glitch_threshold)
{
  switch (r.tag)
  {
    case RegionTypeFinished: return std::vector<Region>();
/**
    case RegionTypeSingleton: return burningShipReg(r.Singleton.value(), er2);
*/
    case RegionTypePerturb: return burningShipReg(r.Perturb.value(), maxiters, er2);
    case RegionTypeSeries: return burningShipReg(r.Series.value(), order, series_threshold, series_iters, er2);
  }
  assert(!"burningShipReg Region.tag");
}


/*

--          region = M.filter ((q ==) . burningShipFolding ref_) deltas
{-
          -- compute center of region
          (Sum is, Sum js) = M.foldMapWithKey (\(i, j) _ -> (Sum (fromIntegral i), Sum (fromIntegral j))) region
          m = fromIntegral (M.size region)
          i0 = is / m
          j0 = js / m
          -- move reference to center of region
          ij = findKeyNear (i0, j0) (M.keys region)
          d = burningShipDel ref_ $ region M.! ij
          aa' = aa ref' + realToFrac (a d)
          bb' = bb ref' + realToFrac (b d)
          xx' = xx ref' + realToFrac (x d)
          yy' = yy ref' + realToFrac (y d)
-}
{-
          -- move series to new reference
          (s', t') = burningShipSeries q ref_ (s r, t r)
--          s' = biShift (-a d) (-b d) $ s r
--          t' = biShift (-a d) (-b d) $ t r
          (bry, int) = M.partitionWithKey (\ix _ -> any (`M.notMember` region) (neighbours ix)) (({- (\d' -> Delta (a d' - a d) (b d' - b d) (x d' - x d) (y d' - y d)) . -} burningShipDel ref_) <$> region)
      in  --traceShow (n ref', ix, M.size region, M.size int, M.size bry) $
          RegionSeries
            { ref = ref'--{ aa = aa', bb = bb', xx = xx', yy = yy' }
            , s = s'
            , t = t'
            , interior = int
            , boundary = bry
            }

-}

{-
uniShift c m = go (order - 1) m
  where
    go i m | i >= 0 = go (i - 1) (go2 i m)
           | otherwise = m
      where
        go2 j m | j < order = go2 (j + 1) $ M.insert j (m ! j + m ! (j + 1) * c) m
                | otherwise = m
    m ! j = case M.lookup j m of Nothing -> 0 ; Just x -> x

biShift x y
  = twiddle . pull . fmap (uniShift x) . push
  . twiddle . pull . fmap (uniShift y) . push

push :: (Ord a, Ord b) => Map (a, b) v -> Map a (Map b v)
push m = M.fromListWith M.union [ (i, M.singleton j e) | ((i,j), e) <- M.assocs m ]

pull :: (Ord a, Ord b) => Map a (Map b v) -> Map (a, b) v
pull m = M.fromList [((i,j),e) | (i, mj) <- M.assocs m, (j, e) <- M.assocs mj ]

twiddle :: (Ord a, Ord b) => Map (a, b) v -> Map (b, a) v
twiddle = M.mapKeys (\(i,j) -> (j,i))
-}
    
{-
horner shift
for (i = n - 2; i >= 0; i--)
for (j = i; j < n - 1; j++)
fmpz_addmul(poly + j, poly + j + 1, c); // poly[j] += poly[j+1]*c

a_0 x^0 + a_1 x^1 + a_2 x^2 + a_3 x^3
->
a_2 += a_3 * c

a_1 += a_2 * c
a_2 += a_3 * c

a_0 += a_1 * c
a_1 += a_2 * c
a_2 += a_3 * c
-}


findKeyNear (i0, j0) = minimumBy (comparing d2)
  where d2 (i, j) = let x = fromIntegral i - i0 ; y = fromIntegral j - j0 in x * x + y * y
*/

/*
{-
substitute

x = sum_{i>=0,j>=0,i+j>0} S_{i,j} a^i b^j
y = sum_{i>=0,j>=0,i+j>0} T_{i,j} a^i b^j

into

x := 2 X x - 2 Y y + x^2 - y^2 + a
y := 2 * diffabs (X Y) (X y + x Y + x y) + b

diffabs :: (Ord a, Num a) => a -> a -> a
diffabs c d =
  if (c >= 0)
    then
      if (c + d >= 0)
        then d               -- y :=  2 * (        X y + x Y + x y) + b
        else -(2 * c + d)    -- y := -2 * (2 X Y + X y + x Y + x y) + b
    else
      if (c + d > 0)
        then 2 * c + d       -- y :=  2 * (2 X Y + X y + x Y + x y) + b
        else -d              -- y := -2 * (        X y + x Y + x y) + b


and equate coefficients gives iteration formulas for the series approximation
-}
*/

/*
main' :: Precision p => Int -> Int -> Int -> Int -> Double -> Int -> Rounded TowardNearest p -> Rounded TowardNearest p -> Double -> proxy p -> [((Int, Int), Result)]
main' w h maxiters order threshold seriesiters a0 b0 r0 _prec
  = concatMap (M.toList . done)
  . filter isFinished
  . concat
  . takeWhile (not . null)
  . take maxiters
  . burningShip order threshold seriesiters
  $ initial w h order a0 b0 r0

plot1 w h maxiters u ((i, j), r) = let k = j * w + i in case r of
    ReferenceEscaped   -> U.write u k (-1/0)
    PerturbationGlitch -> U.write u k (-1/0)
    Result mu          -> U.write u k (mu)

clamp x lo hi = lo `max` x `min` hi

*/

void calculate(double *buffer, N_16 w, N_16 h, Z_64 maxiters, N_16 order, R_lo series_threshold, Z_64 series_iters, R_lo glitch_threshold, R_lo er2, const R_hi &a0, const R_hi &b0, R_lo r0)
{
  std::vector<Region> active;
  active.push_back(initial(w, h, maxiters, order, glitch_threshold, er2, a0, b0, r0));
  for (auto i = 0; i < maxiters; ++i)
  {
    if (active.empty()) break;
    std::vector<Region> next;
    for (auto source : active)
    {
      std::cerr << source.tag << std::endl;
      std::vector<Region> news = burningShipReg(source, maxiters, order, series_threshold, series_iters, er2, glitch_threshold);
      for (auto sink : news)
      {
        if (sink.tag == RegionTypeFinished)
        {
          for (auto p : sink.Finished.value())
          {
            Z_64 i = p.first.first;
            Z_64 j = p.first.second;
            Z_64 k = j * w + i;
            buffer[k] = p.second;
          }
        }
        else
        {
          next.push_back(sink);
        }
      }
    }
    std::swap(active, next);    
  }
}

/*

plot w h maxiters xs = runST (do
  u <- U.new (w * h)
  mapM_ (plot1 w h maxiters u) xs
  v <- S.new (w * h * 3)
  mapM_ (colour1 w h u v) [ (i,j) | i <- [0 .. w - 1], j <- [0 .. h - 1] ]
  unsafeFreeze v)

colour1 w h u v (i, j) = do
  let i1 = if i == 0 then i + 1 else i - 1
      j1 = if j == 0 then j + 1 else j - 1
  (n00) <- U.read u (j  * w + i )
  (n01) <- U.read u (j  * w + i1)
  (n10) <- U.read u (j1 * w + i )
  (n11) <- U.read u (j1 * w + i1)
  let du =  (n00 - n11)
      dv =  (n01 - n10)
      de = du^2 + dv^2
      wo :: Word8
      l = 20 * log de
      wo = floor l 
      (r,g,b)
        | n00 == 0 = (0, 0, 0) -- unescaped
        | n00 == -1/0 = (255, 0, 0) -- glitch
        | isNaN de = (0, 0, 255) -- error?
        | otherwise = (wo, 128, floor n00)
      k = 3 * (j * w + i)
  S.write v (k + 0) r
  S.write v (k + 1) g
  S.write v (k + 2) b
*/

void colour(N_8 *image, const double *buffer, N_16 w, N_16 h)
{
  #pragma omp parallel for
  for (Z_64 j = 0; j < h; ++j)
  {
    Z_64 j1 = j == 0 ? j + 1 : j - 1;
    for (Z_64 i = 0; i < w; ++i)
    {
      Z_64 i1 = i == 0 ? i + 1 : i - 1;
      {
        auto n00 = buffer[j  * w + i ];
        auto n01 = buffer[j  * w + i1];
        auto n10 = buffer[j1 * w + i ];
        auto n11 = buffer[j1 * w + i1];
        auto du = n00 - n11;
        auto dv = n01 - n10;
        auto de = du * du + dv * dv;
        auto l = 128 + 20 * log(de);
        N_8 r = l;
        N_8 g = 128;
        N_8 b = n00;
        if (n00 == 0) { r = g = b = 0; }
        if (n00 == PerturbationGlitch) { r = 255; g = b = 0; }
        if (n00 == ReferenceEscaped) { r = b = 255; g = 0; }
        if (std::isnan(de)) { r = g = 0; b = 255; }
        Z_64 k = (j * w + i) * 3;
        image[k + 0] = r;
        image[k + 1] = g;
        image[k + 2] = b;
      }
    }
  }
}


/*
main :: IO ()
main = do
  [sw, sh, smaxiters, sorder, sthreshold, sseriesiters, sa0, sb0, sr0] <- getArgs
  let w = read sw
      h = read sh
{-
      a0s = "-1.765724070777579936114404613125203747548323196202122799408000137"
      b0s = "-0.041893714510242382733641617814193925623948756785781053295999999"
      r0 = 2 / 9.9035203142830852e27
      maxiters = 200100
-}
{-
  let a0 = -1.775015718882760856139042287908955655837994562692664464768
      b0 = -0.046245056219157170454983287250662144618728655141566663527
      r0 = 2 / 1.2980742146337048E33
      maxiters = 5100
-}
      maxiters = read smaxiters
      order = read sorder
      threshold = read sthreshold
      seriesiters = read sseriesiters
      r0 = read sr0
      prec = ceiling $ 24 - logBase 2 r0
      img = plot w h maxiters $ reifyPrecision prec $ main' w h maxiters order threshold seriesiters (read sa0) (read sb0) r0
  hPutStr stderr $ "precision bits: " ++ show prec ++ "\n"
  hPutStr stdout $ "P6\n" ++ show w ++ " " ++ show h ++ "\n255\n"
  unsafeWith img (\p -> hPutBuf stdout p (w * h * 3))
*/

int main(int argc, char **argv)
{
  if (argc != 12)
  {
    fprintf(stderr, "usage: %s width height iterations series_order series_threshold series_iterations glitch_threshold escape_radius a0 b0 r0\n", argv[0]);
    return 1;
  }
  N_16 w = std::atoi(argv[1]);
  N_16 h = std::atoi(argv[2]);
  Z_64 maxiters = std::atol(argv[3]);
  N_16 order = std::atoi(argv[4]);
  R_lo series_threshold = std::strtold(argv[5], nullptr);
  Z_64 series_iters = std::atol(argv[6]);
  R_lo glitch_threshold = std::strtold(argv[7], nullptr);
  R_lo escape_radius = std::strtold(argv[8], nullptr);
  R_lo er2 = escape_radius * escape_radius;
  R_lo r0 = std::strtold(argv[11], nullptr);
  Z_64 prec = std::ceil(24 - std::log2(r0));
  R_hi::set_default_prec(prec);
  R_hi a0(argv[9]);
  R_hi b0(argv[10]);
  Z_64 bytes = size_t(w) * size_t(h);
  double *buffer = (double *) std::malloc(bytes * sizeof(double));
  calculate(buffer, w, h, maxiters, order, series_threshold, series_iters, glitch_threshold, er2, a0, b0, r0);
  N_8 *image = (N_8 *) std::malloc(bytes * 3);
  colour(image, buffer, w, h);
  std::free(buffer);
  std::printf("P6\n%d %d\n255\n", int(w), int(h));
  std::fwrite(image, bytes * 3, 1, stdout);
  std::free(image);
std::cerr << "\t" << w << std::endl;
std::cerr << "\t" << h << std::endl;
std::cerr << "\t" << maxiters << std::endl;
std::cerr << "\t" << order << std::endl;
std::cerr << "\t" << series_threshold << std::endl;
std::cerr << "\t" << series_iters << std::endl;
std::cerr << "\t" << glitch_threshold << std::endl;
std::cerr << "\t" << escape_radius << std::endl;
std::cerr << "\t" << a0 << std::endl;
std::cerr << "\t" << b0 << std::endl;
std::cerr << "\t" << r0 << std::endl;
  return 0;
}
