// an example of using C++ threads to render fractals interactively
// (c) 2020 Claude Heiland-Allen
// inspired by <https://fractalforums.org/programming/11/multi-threading-and-multiple-processors/3323>
// to compile:
// g++ -pthread -std=c++11 -Wall -Wextra -pedantic -O3 -march=native main.cc -o main
// to run:
// ./main
// type h<ENTER> for key binding help

#include <algorithm>
#include <atomic>
#include <cassert>
#include <chrono>
#include <complex>
#include <condition_variable>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

// no high precision in this example program
typedef double R;
typedef std::complex<R> C;

// forward declarations
struct event;    // keypresses from user, done signal from renderer
struct view;     // data containing viewport and maxiters
struct ui;       // abstract user interface (handles events, controls renderer)
struct interact; // user interaction
struct animator; // automatic animation
struct renderer; // multithreaded renderer (Mandelbrot set toy example)

// helper function to avoid mutual references
void send_event(ui *u, const event &e);

// rudimentary event class
// it would probably be better to have semantic events
// (eg, quit, show help, zoom, ...)
// instead of raw key values
struct event
{
  enum { e_render_done, e_key_pressed } type;
  union
  {
    bool render_done;
    int key_pressed;
  } u;
  event(int k)
  : type(e_key_pressed)
  {
    u.key_pressed = k;
  };
  event(bool b)
  : type(e_render_done)
  {
    u.render_done = b;
  };
};

// the view of the fractal to render
struct view
{
  C center;     // c-plane coordinates of the center of the image
  R radius;     // vertical distance from center to top edge of image
  int maxiters; // maximum iteration count
};

// multithreaded fractal renderer
struct renderer
{
private:

  // the quantum of work for the worker threads
  // currently this is just a cooked version of the pixel index
  struct job
  {
    int i;
    int j;
    // is this job valid?
    operator bool () { return i >= 0 && j >= 0; };
  };

  // image properties
  int width;
  int height;
  double aspect; // pixel aspect ratio

  // desired number of worker threads
  int nthreads;

  // npixels = width * height (as a wider type to support > 2 gigapixels)
  uint64_t npixels;

  // raw iteration count data for the image
  // -1 is stored to signify no value has been computed yet
  std::vector<int> raw;

  // when start is called, running is stored true
  // when stop is called, running is stored false
  // workers load running to know if they should abandon the work
  // this flag is only set as a result of ui events
  // when workers are running it can only transition monotonically from true to false
  bool running;

  // the number of active workers
  // when a worker exits
  // either due to the running flag transitioning to false
  // or due to all the work being complete
  // it decrements this count
  // when the count has reached zero, the worker knows it is the last to leave
  // so that worker (and that worker only) signals the ui with the done event
  // passing along the current value of the running flag
  // so that the ui knows if the work was completed or interrupted
  std::atomic<int> active;

  // the index of the next pixel in the queue
  // this is set to 0 before workers are spawned
  // it is incremented in get_job which is called by the worker threads
  // when it reaches npixels the work is done
  std::atomic<uint64_t> next_job;

  // the collection of currently running worker threads
  // threads are spawned by start() and reaped by stop()
  // before start() and after stop() this collection is empty
  // in between start() and stop() it has size nthreads
  std::vector<std::thread> workers;

  // the view that is being rendered
  view v;

  // the user interface that controls this renderer
  // this is needed as a target for render done events
  ui *u;

  // get the next job
  job get_job()
  {
    // next_job is atomic, which means orderings like this are forbidden:
    // worker a              worker b
    // read next_job
    //                       read next_job
    // increment             increment
    // write next_job
    //                       write next_job
    // INCORRECT RESULT: next_job is only incremented by 1 instead of 2
    // atomically each time it is called it returns a distinct value
    // these values correspond 1:1 to pixels in the image
    // distributed among the worker threads as first come, first served
    uint64_t k = next_job++;
    if (running && k < npixels)
    {
      // return the pixel coordinates
      return { (int) (k % width), (int) (k / width) };
    }
    else
    {
      // return an invalid job to signal no more work
      // either because of abandoned / interrupted (running false)
      // or all the work is completed (k >= npixels)
      return { -1, -1 };
    }
  }

  // called by workers when they have finished a job
  void set_pixel(int i, int j, int k)
  {
    assert(0 <= i);
    assert(i < width);
    assert(0 <= j);
    assert(j < height);
    raw[j * width + i] = k;
  }

  // worker thread
  static void worker(renderer *r)
  {
    // make local copies for efficiency
    int width = r->width;
    int height = r->height;
    double aspect = r->aspect;
    R radius = r->v.radius;
    C center = r->v.center;
    int maxiters = r->v.maxiters;

    // get a pixel to work on
    job b;
    while ((b = r->get_job()))
    {
      // calculate c-plane coordinates from pixel coordinates
      C c ( ((b.i + 0.5) / width - 0.5) * width / height * aspect
          , (0.5 - (b.j + 0.5) / height)
          );
      c *= radius * 2;
      c += center;
      // simple Mandelbrot set iterations
      C z(0, 0);
      int i;
      for (i = 0; i < maxiters; ++i)
      {
        if (norm(z) > 4) break;
        z = z * z + c;
      }
      // output results
      r->set_pixel(b.i, b.j, i);
    }
    // if we are the last worker to leave, signal the user interface
    if (--r->active == 0)
    {
      send_event(r->u, event(r->running));
    }
  }

public:

  // constructor
  renderer(int w, int h, double a, int n)
  : width(w)
  , height(h)
  , aspect(a)
  , nthreads(n)
  , npixels(width * (uint64_t) height)
  , raw(npixels)
  , running(false)
  , active(0)
  , next_job(-1)
  , workers()
  , v({ 0, 2, 128 })
  , u(nullptr)
  {
  };

  // destructor
  ~renderer()
  {
    stop();
  }

  // link to user interface
  // this should be called before start()
  void set_ui(ui *u0)
  {
    u = u0;
  }

  ui *get_ui()
  {
    return u;
  }

  // start rendering a view
  // stop() must be called before start() is called again
  // it is not safe to call start() from multiple threads simultaneously
  void start(const view &v0)
  {
    assert(! active);
    assert(! running);
    // copy view
    v = v0;
    // initialize memory to "not yet calculated" sentinel value
    std::fill(raw.begin(), raw.end(), -1);
    // reset the pixel job counter
    next_job = 0;
    // transition to true only when no worker threads are running
    running = true;
    // increase the active count all at once instead of inside the loop
    // this is so that fast workers don't signal done to the ui more than once
    // for example, this chain of events:
    // increment active
    // start worker
    //                     worker finishes all the work quickly
    //                     worker decrements active
    //                     worker sees active is 0 and signals done to ui
    // increment active
    // start worker
    //                     etc
    // etc
    active += nthreads;
    for (int i = 0; i < nthreads; ++i)
    {
      workers.push_back(std::thread(worker, this));
    }
  };

  // stop rendering
  // must be called even if the work was completed (done signalled to ui)
  // it is safe to call stop() more than once in sequence without an intervening start()
  // it is not safe to call stop() from multiple threads simultaneously
  void stop()
  {
    running = false;
    while (! workers.empty())
    {
      workers.back().join();
      workers.pop_back();
    }
    assert(! active);
  };

  // data accessors

  int get_width() const
  {
    return width;
  }

  int get_height() const
  {
    return height;
  }

  double get_aspect() const
  {
    return aspect;
  }

  C get_center() const
  {
    return v.center;
  }

  R get_radius() const
  {
    return v.radius;
  }

  int get_maxiters() const
  {
    return v.maxiters;
  }

  int get_pixel(int i, int j) const
  {
    // perhaps it would be nicer to return -1 instead of failing?
    assert(0 <= i);
    assert(i < width);
    assert(0 <= j);
    assert(j < height);
    return raw[j * width + i];
  };
};

void display(std::ostream &out, const renderer *r)
{
  int width = r->get_width();
  int height = r->get_height();
  int maxiters = r->get_maxiters();
  C center = r->get_center();
  R radius = r->get_radius();
  // display the rendered results as an ASCII image
  out << std::endl;
  // copy the pixel data
  std::vector<int> histogram;
  for (int j = 0; j < height; ++j)
  {
    for (int i = 0; i < width; ++i)
    {
      histogram.push_back(r->get_pixel(i, j));
    }
  }
  // sort it
  std::sort(histogram.begin(), histogram.end());
  // skip uncalculated pixels
  uint64_t start;
  for (start = 0; start < histogram.size(); ++start)
  {
    if (histogram[start] >= 0)
    {
      break;
    }
  }
  // skip unescaped pixels
  uint64_t end;
  for (end = histogram.size(); end > 0; --end)
  {
    if (histogram[end - 1] < maxiters)
    {
      break;
    }
  }
  // calculate thresholds
  std::vector<int> thresholds;
  thresholds.push_back(-1);
  for (int m = 0; m < 10; ++m)
  {
    if (start < end)
    {
      thresholds.push_back(histogram[start + (m + 1) * (end - 1 - start) / 10]);
    }
    else
    {
      thresholds.push_back(-1);
    }
  }
  thresholds.push_back(maxiters);
  const std::string characters = "? .,-+*^/\\O!";
  // equalize image while displaying it
  for (int j = 0; j < height; ++j)
  {
    for (int i = 0; i < width; ++i)
    {
      int k = r->get_pixel(i, j);
      // find index of first threshold that the count is less than
      unsigned int t;
      for (t = 0; t < thresholds.size(); ++t)
      {
        if (k <= thresholds[t])
        {
          out << characters[t];
          break;
        }
      }
      if (t == thresholds.size())
      {
        // not found, internal error?
        out << '$';
      }
    }
    // print key
    if ((unsigned) j < thresholds.size())
    {
      out << "    " << characters[j] << " " << thresholds[j];
    }
    out << std::endl;
  }
  // print coordinates
  out << std::fixed << std::setprecision(16) << real(center) << " + ";
  out << std::fixed << std::setprecision(16) << imag(center) << " i @ ";
  out << std::scientific << std::setprecision(6) << radius << std::endl;
}

// abstract user interface
struct ui
{
public:
  ui() { };
  virtual ~ui() { };
  virtual bool is_running() = 0;
  virtual void handle_event(const event &e) = 0;
};

// user interface
struct interact : public ui
{
private:
  // keep track of current location
  view here;

  // link to the renderer
  renderer *r;

  // set to false to signal desire to quit
  bool running;

  // events can arrive from multiple threads (main, workers)
  // this protects the code that can't be run simultaneously
  std::mutex critical;

public:

  // constructor
  interact(const view &here, renderer *r)
  : here(here)
  , r(r)
  , running(true)
  {
    // link renderer back to us
    r->set_ui(this);
    // start rendering the initial view
    r->start(here);
  }

  // data accessor
  bool is_running()
  {
    return running;
  }

  // main event
  void handle_event(const event &e)
  {
    // this is set to true if we need to restart rendering
    // see comments below
    bool restart = false;
    {
      // start of critical section
      // only one thread can be here simultaneously
      std::lock_guard<std::mutex> lock(critical);
      switch (e.type)
      {

        case event::e_render_done:
        {
          if (e.u.render_done)
          {
            display(std::cout, r);
          }
          else
          {
            // the render wasn't completed
            std::cout << "interrupted" << std::endl;
          }
          break;
        }

        // input
        case event::e_key_pressed:
        {
          double aspect = r->get_aspect();
          switch (e.u.key_pressed)
          {
            // quit
            case EOF:
            case 27: // ESC
            case 'q':
            case 'Q':
              // it is not safe to stop the renderer here
              // see comments below about deadlock
              running = false;
              break;
            // help
            case '?':
            case 'h':
            case 'H':
              std::cout << "-+=         adjust maxiters"   << std::endl
                        << "0           zoom out"          << std::endl
                        << "123456789   zoom in (numpad)"  << std::endl
                        << "rR          refresh"           << std::endl
                        << "z           slow animation"    << std::endl
                        << "Z           fast animation"    << std::endl
                        << "hH?         display this help" << std::endl
                        << "qQ ESC EOF  quit"              << std::endl
                        ;
              break;
            // refresh
            case 'r':
            case 'R':                                                                          restart = true; break;
            // adjust maxiters
            case '-': here.maxiters /= 2; if (here.maxiters < 1) { here.maxiters = 1; }        restart = true; break;
            case '=':
            case '+': here.maxiters *= 2;
                      if (here.maxiters > 1 << 30) { here.maxiters = 1 << 30; }                restart = true; break;
            // zoom out
            case '0': here.radius *= 2;                                                        restart = true; break;
            // zoom in (numpad matches screen layout)
            case '1': here.radius /= 2; here.center += C(-here.radius / aspect, -here.radius); restart = true; break;
            case '2': here.radius /= 2; here.center += C( 0,                    -here.radius); restart = true; break;
            case '3': here.radius /= 2; here.center += C( here.radius / aspect, -here.radius); restart = true; break;
            case '4': here.radius /= 2; here.center += C(-here.radius / aspect,  0          ); restart = true; break;
            case '5': here.radius /= 2; here.center += C( 0,                     0          ); restart = true; break;
            case '6': here.radius /= 2; here.center += C( here.radius / aspect,  0          ); restart = true; break;
            case '7': here.radius /= 2; here.center += C(-here.radius / aspect,  here.radius); restart = true; break;
            case '8': here.radius /= 2; here.center += C( 0,                     here.radius); restart = true; break;
            case '9': here.radius /= 2; here.center += C( here.radius / aspect,  here.radius); restart = true; break;
            // ignore
            case ' ':
            case '\t':
            case '\n':
            case '\r':
              break;
            // unrecognized
            default:
              // beep
              std::cout << '\a' << std::endl;
              break;
          }
          break;
        }
      }
      // end of critical section
    }
    if (restart)
    {
      // restarting must be outside the critical section
      // otherwise this scenario can ruin your day
      // main thread         worker thread
      // handle_event(key)   handle_event(done)
      // lock proceeds
      //                     lock blocks
      // stop
      // join worker
      // DEADLOCK
      // main waits forever for blocked worker to terminate
      //
      // because this is outside of the critical section,
      // and event done comes from the worker thread,
      // and the renderer stop/start are not thread safe:
      // handle done event must not trigger a restart
      // otherwise this scenario can ruin your day
      // main thread          worker thread
      // handle_event(key)    handle_event(done)
      // lock
      // restart requested
      // unlock
      //                      lock
      //                      restart requested
      //                      unlock
      // stop renderer
      //                      stop renderer
      // start renderer
      //                      start renderer
      // ASSERTION FAILURE
      // renderer is started more than once without an intervening stop
      // (this is best case, worst case is weird data corruption or
      // deadlock due to simultaneously executing stops and starts)
      r->stop();
      r->start(here);
    }
  };
};

// animation rendering
struct animator : public ui
{
private:

  // the current view during animation
  view here;

  // the renderer
  renderer *r;

  // events can arrive from multiple threads (main, workers)
  // from main: 'q' key to stop animation
  // from renderer workers: render done event

  // running starts true
  // transitions to false on interrupted render (should never happen)
  // or on 'q' event
  // or on completion of the whole animation
  bool running;

  // set to false before rendering each frame starts
  // transitions to true on successful render
  bool frame_done_variable;

  // mutex + condition to guard both running and frame_done
  // the mutex must be locked when reading or writing
  // otherwise changes are not notified properly
  std::mutex frame_done_mutex;
  std::condition_variable frame_done_condition;

public:

  // constructor
  animator(view here, renderer *r)
  : here(here)
  , r(r)
  , running(true)
  {
  };

  // thread
  void operator()(double ms_per_frame = 0)
  {
    // link renderer back to us
    r->stop();
    ui *old_ui = r->get_ui();
    r->set_ui(this);
    // start of animation
    auto start = std::chrono::high_resolution_clock::now();
    int frame = 0;
    // local copy of running, outside the mutex
    bool am_running;
    {
      // mutex must be held when reading
      std::unique_lock<std::mutex> lock(frame_done_mutex);
      am_running = running;
    }
    while (am_running && here.radius < 1024)
    {
      {
        // take the lock before starting the renderer because
        // the workers might be so quick that they all finish
        // before we have a chance to lock and wait, which means
        // we could be waiting forever for a notification that
        // already passed by to no-one
        std::unique_lock<std::mutex> lock(frame_done_mutex);
        frame_done_variable = false;
        r->start(here);
        // wait for notifications, with required predicate
        frame_done_condition.wait(lock, [this]{ return (! running) || frame_done_variable; });
      }
      // mutex must not be locked when stopping
      // because the last renderer worker sends a done event -> DEADLOCK
      r->stop();
      {
        // mutex must be locked when reading
        std::unique_lock<std::mutex> lock(frame_done_mutex);
        am_running = running;
      }
      if (am_running)
      {
        // slow down animation to approximate average speed (if ms_per_frame > 0)
        std::this_thread::sleep_until(start + ++frame * std::chrono::milliseconds(int(ms_per_frame)));
        // display render results
        display(std::cout, r);
      }
      else
      {
        // 'q' key was pressed to stop the animation
        std::cout << "interrupted" << std::endl;
      }
      // zoom out a bit
      here.radius *= pow(2, 1.0 / 25);
    }
    if (am_running)
    {
      // animation rendering completed
      std::cout << "done" << std::endl;
    }
    running = false;
    r->set_ui(old_ui);
  };

  virtual void handle_event(const event &e)
  {
    switch (e.type)
    {
      case event::e_render_done:
      {
        // mutex must be locked for writing
        std::lock_guard<std::mutex> lock(frame_done_mutex);
        if (e.u.render_done)
        {
          // frame complete - continue animation
          frame_done_variable = true;
        }
        else
        {
          // frame interrupted - abandon animation
          running = false;
        }
        // notify animator thread of changes
        frame_done_condition.notify_all();
        break;
      }
      case event::e_key_pressed:
      {
        switch (e.u.key_pressed)
        {
          case EOF:
          case 27:
          case 'q':
          case 'Q':
          {
            // mutex must be locked for writing
            std::lock_guard<std::mutex> lock(frame_done_mutex);
            // interrupt requested - abandon animation
            running = false;
            // notify animator thread of changes
            frame_done_condition.notify_all();
            break;
          }
          default:
            // ignore
            break;
        }
        break;
      }
    }
  };

  virtual bool is_running()
  {
    return running;
  };

};

// this helper function avoids the more painful hoops necessary
// for the mutually cross-referencing structs ui and renderer
void send_event(ui *u, const event &e)
{
  if (u)
  {
    u->handle_event(e);
  }
}

// this is the only POSIX-specific part of the code
// Windows might need its own implementation
#define TIMEOUT (2 * EOF)
#define ERROR (3 * EOF)
int getchar_with_timeout(int sec, int usec)
{
  fd_set rfds;
  FD_ZERO(&rfds);
  FD_SET(0, &rfds);
  struct timeval tv;
  tv.tv_sec = sec;
  tv.tv_usec = usec;
  int retval = select(1, &rfds, nullptr, nullptr, &tv);
  if (retval == -1)
  {
   return ERROR;
  }
  else if (retval)
  {
    // input available
   return std::getchar();
  }
  else
  {
   return TIMEOUT;
  }
}

// main program entry point
extern int main(int argc, char **argv)
{
  // suppress unused parameter warnings
  (void) argc;
  (void) argv;
  // typical console size
  int width = 60;
  int height = 20;
  // typical console character aspect
  double aspect = 0.5;
  // this may be (in)correct w.r.t. hyperthreading etc
  int nthreads = std::thread::hardware_concurrency();
  // initial view is fully zoomed out
  view initial = { 0, 2, 128 };
  // create the renderer
  renderer r(width, height, aspect, nthreads);
  // create the user interface
  interact i(initial, &r);
  // run the event loop
  while (i.is_running())
  {
    int c = std::getchar();
    switch (c)
    {
      // start noninteractive renderer
      case 'z':
      case 'Z':
      {
        // start from where the renderer was last
        // this is either the state of the interactive renderer
        // or the state of the renderer as it was after animating
        // use 'r' (or other navigation) to refresh the renderer
        // back to the state of the interact
        view v = { r.get_center(), r.get_radius(), r.get_maxiters() };
        // create the animator
        animator a(v, &r);
        // launch the animator thread
        std::thread t([&a,c]{ a(c == 'z' ? 1000.0 / 25.0 : 0); });
        // until the animation is done (either complete or abandoned)
        while (a.is_running())
        {
          // wait for input with a timeout, so that we can notice when
          // the animation has finished rendering
          // (with a slight delay, up to the specified timeout)
          int c2 = getchar_with_timeout(0, 40 * 1000);
          if (c2 != ERROR && c2 != TIMEOUT)
          {
            // the only relevant key is 'q' to abandon animation
            a.handle_event(event(c2));
          }
        }
        // cleanup
        t.join();
        break;
      }
      // interactive mode
      default:
      {
        // the renderer may also send events to the ui from other threads
        // getchar() blocks until a newline is entered
        // for better terminal interaction, look into ncurses
        i.handle_event(event(c));
        break;
      }
    }
  }
  return 0;
}
