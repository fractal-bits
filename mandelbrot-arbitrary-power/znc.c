#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <mpc.h>

// location
const char *Re = "-0.222460066039754914599379342888657258509030891982189";
const char *Im = "-0.938828979826320506114083686687650984584598169180445";
const char *Zoom = "7.7371252455336528E25";
const int Iterations = 4096;
const int Power = 100;

// reference location (unused, see initialisation of C below)
const char *RRe = "-0.22246006603975491459937938019403294825";
const char *RIm = "-0.9388289798263205061140836915833697177";

// image dimensions
const int Width = 640;
const int Height = 360;

enum op { mul, sqr, add };

double linear_to_srgb(double c)
{
  c = fmin(fmax(c, 0), 1);
  if (c <= 0.0031308)
  {
    return 12.92 * c;
  }
  else
  {
    return 1.055 * pow(c, 1.0 / 2.4) - 0.055;
  }
}

static inline double norm(double _Complex z)
{
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  // high escape radius
  const double escape_radius_hi_2 = 1e100;
  // initialize
  const double pixel_spacing = 4 / atof(Zoom) / Height;
  const int prec = fmax(53, 16 - log2(pixel_spacing));
  mpc_t C0, C;
  mpc_init2(C0, prec);
  mpc_init2(C, prec);
  mpfr_set_str(mpc_realref(C0), Re, 10, MPFR_RNDN);
  mpfr_set_str(mpc_imagref(C0), Im, 10, MPFR_RNDN);
  mpfr_set_str(mpc_realref(C), /*R*/Re, 10, MPFR_RNDN);
  mpfr_set_str(mpc_imagref(C), /*R*/Im, 10, MPFR_RNDN);
  mpc_sub(C0, C0, C, MPC_RNDNN);
  double _Complex c0 = mpc_get_dc(C0, MPC_RNDNN);

  // compute opcode table
  int M = 0;
  int n = Power;
  while (n > 1)
  {
    if (n & 1)
    {
      n -= 1;
    }
    else
    {
      n /= 2;
    }
    ++M;
  }
  ++M;
  enum op ops[M];
  int m = M;
  ops[--m] = add;
  n = Power;
  while (n > 1)
  {
    if (n & 1)
    {
      ops[--m] = mul;
      n -= 1;
    }
    else
    {
      ops[--m] = sqr;
      n /= 2;
    }
  }

  // compute reference
  double _Complex *Zptr = malloc(Iterations * M * sizeof(*Zptr));
  mpc_t Z0, Z;
  mpc_init2(Z0, prec);
  mpc_init2(Z, prec);
  mpc_set_dc(Z, 0, MPC_RNDNN);
  for (int n = 0; n < Iterations; ++n)
  {
    mpc_set(Z0, Z, MPC_RNDNN);
    for (int m = 0; m < M; ++m)
    {
      Zptr[n * M + m] = mpc_get_dc(Z, MPC_RNDNN);
      switch (ops[m])
      {
        case mul:
          mpc_mul(Z, Z, Z0, MPC_RNDNN);
          break;
        case sqr:
          mpc_sqr(Z, Z, MPC_RNDNN);
          break;
        case add:
          mpc_add(Z, Z, C, MPC_RNDNN);
          break;
      }
    }
  }

  // calculate pixels
  unsigned char *pgm = malloc(Width * Height);
  int progress = 0;
  #pragma omp parallel for schedule(dynamic, 1)
  for (int j = 0; j < Height; ++j)
  {
    for (int i = 0; i < Width; ++i)
    {
      double raw = 0;
      int Samples = 1;

      // for each sample
      const double _Complex c = c0 +
        ( (((i + 0.5)) / Width - 0.5) * Width
        + ((j + 0.5) / Height - 0.5) * Height * I
        ) * pixel_spacing;
      double _Complex z = 0;
      double _Complex dz = 0;
      int n = 0;
      int k = 0;
      bool escaped = false;
      while (k < Iterations)
      {
        double _Complex dz0 = dz;
        double _Complex z0 = z;
        double _Complex Z0 = Zptr[n * M + 0];
        double _Complex Zz0 = Z0 + z0;
        for (int m = 0; m < M; ++m)
        {

          // rebase
          double _Complex Z = Zptr[n * M + m];
          double _Complex Zz = Z + z;
          double Z2 = norm(Z);
          double Zz2 = norm(Zz);
          double z2 = norm(z);
          if (Zz2 < z2 || Z2 > escape_radius_hi_2)
          {
            z = Zz;
            z2 = Zz2;
            Z = 0;
            n = 0;
          }

          // bailout
          if (Zz2 > escape_radius_hi_2)
          {
            double de = sqrt(Zz2 / norm(dz)) * log(Zz2);
            raw += fmin(fmax(0.75 + 0.125 * log(4.0 * de), 0.0), 1.0);
            escaped = true;
            break;
          }

          // opcode
          switch (ops[m])
          {
            case mul:
              dz = dz0 * Zz + dz * Zz0;
              z = Z0 * z + Z * z0 + z * z0;
              break;
            case sqr:
              dz = 2 * dz * Zz;
              z = 2 * Z * z + z * z;
              break;
            case add:
              dz = dz + pixel_spacing;
              z = z + c;
              break;
          }

        }
        if (escaped)
        {
          break;
        }
        ++k;
        ++n;
      }
      // colour
      pgm[j * Width + i] = 255 * linear_to_srgb(raw / Samples);
    }
    #pragma omp critical
    fprintf(stderr, "\t%d/%d\r", ++progress, Height);
  }

  // output image
  fprintf(stdout, "P5\n%d %d\n255\n", Width, Height);
  fwrite(pgm, Width * Height, 1, stdout);
  fflush(stdout);
  return 0;
}
