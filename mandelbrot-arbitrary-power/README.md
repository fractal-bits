# z->z^n+c

## fast exponentiation

- O(log(n))
- z^{2n+1} = z z^{2n}
- z^{2n} = (z^n)^2

## reference orbit

- uses opcode table

## perturbation

- uses opcode table

## rebasing

Zhuoran's technique: when |Z+z|<|z|, reset z to Z+z and Z to 0.
