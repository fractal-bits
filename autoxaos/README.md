# autoxaos

## references

- "How to optimally adjust the probabilities for the random IFS algorithm?"
  <https://math.stackexchange.com/q/1900337>

- "A Multifractal Analysis of IFSP Invariant Measures with Application to Fractal Image Generation"
  <https://doi.org/10.1142/S0218348X96000042>
