#include <assert.h>
#include <complex.h>
#include <inttypes.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// similarity dimension of a collection of contracting similitudes
// satisfying the open set condition
double dimS(const double *r, int n)
{
  double s_lo = 0.0;
  double s_hi = 2.0; // FIXME hardcoded
  for (int bits = 0; bits < 64; ++bits)
  {
    double s = (s_lo + s_hi) / 2.0;
    double sum = 0;
    for (int i = 0; i < n; ++i)
    {
      sum += pow(r[i], s);
    }
    if (sum > 1.0)
    {
      s_lo = s;
    }
    if (sum < 1.0)
    {
      s_hi = s;
    }
  }
  return (s_lo + s_hi) / 2.0;
}

struct bin
{
  uint64_t count;
  double rgb[3];
};

struct histogram
{
  double _Complex m;
  double _Complex b;
  uint64_t width;
  uint64_t height;
  uint64_t total_buckets;
  uint64_t total_empty;
  uint64_t total_calculated;
  uint64_t total_plotted;
  struct bin *data[2];
};

struct histogram *histogram_new(int size)
{
  struct histogram *h = calloc(1, sizeof(*h));
  h->m = size * 0.5;
  h->b = (size + I * size) * 0.5;
  h->width = size;
  h->height = size;
  h->total_buckets = M_PI * 0.5 * size * 0.5 * size * 2; // FIXME count proper rasterization
  h->total_empty = h->total_buckets;
  h->data[0] = calloc(1, sizeof(*h->data[0]) * h->width * h->height);
  h->data[1] = calloc(1, sizeof(*h->data[1]) * h->width * h->height);
  return h;
}

void histogram_delete(struct histogram *h)
{
  free(h->data[1]);
  free(h->data[0]);
  free(h);
}

void histogram_clear(struct histogram *h)
{
  h->total_empty = h->total_buckets;
  h->total_calculated = 0;
  h->total_plotted = 0;
  memset(h->data[0], 0, sizeof(*h->data[0]) * h->width * h->height);
  memset(h->data[1], 0, sizeof(*h->data[1]) * h->width * h->height);
}

double histogram_plot(struct histogram *h, double _Complex x, double rgb[3])
{
  h->total_calculated++;
  int w = cabs(x) > 1;
  if (w)
  {
    x = 1 / x;
  }
  double _Complex y = h->m * x + h->b;
  double u = creal(y);
  double v = cimag(y);
  if (0 <= u && u < h->width && 0 <= v && v < h->height)
  {
    uint64_t i = u;
    uint64_t j = v;
    if (! (i < h->width && j < h->height))
    {
      return 1;
    }
    uint64_t k = j * h->width + i;
    h->data[w][k].rgb[0] += rgb[0];
    h->data[w][k].rgb[1] += rgb[1];
    h->data[w][k].rgb[2] += rgb[2];
    // keep track of number of empty buckets while incrementing
    if (h->data[w][k].count++ == 0 && h->total_empty)
    {
      h->total_empty--;
    }
    h->total_plotted++;
    // weight adjustment is 1.0 for perfectly uniform fill rate
    return ( (h->total_plotted / ((double) h->total_buckets - h->total_empty))
           / h->data[w][k].count
           );
  }
  return 1; // should never be reached?
}

struct moebius
{
  double _Complex A, B, C, D;
};

// uniform random on Riemann sphere via rejection sampling
double _Complex randC(void)
{
  double x, y, z, r2;
  do
  {
    x = rand() / (double) RAND_MAX * 2 - 1;
    y = rand() / (double) RAND_MAX * 2 - 1;
    z = rand() / (double) RAND_MAX * 2 - 1;
    r2 = x * x + y * y + z * z;
  } while (r2 > 1.0 || r2 == 0.0);
  double s = 1.0 / sqrt(r2);
  x *= s;
  y *= s;
  z *= s;
  return (x + I * y) / (1 - z);
}

void moebius_randomize(struct moebius *m)
{
  m->A = randC();
  m->B = randC();
  m->C = randC();
  m->D = randC();
}

void moebius_normalize(struct moebius *m)
{
  double s = 1 / sqrt(cabs(m->A * m->D - m->B * m->C));
  m->A *= s;
  m->B *= s;
  m->C *= s;
  m->D *= s;
}

double _Complex moebius_eval(const struct moebius *m, double _Complex x)
{
  if (isinf(creal(x)))
  {
    return m->A / m->C;
  }
  else
  {
    return cproj((m->A * x + m->B) / (m->C * x + m->D));
  }
}

struct flame
{
  int N;
  struct moebius *T;
  double **probability;
  double *colour;
  double *colour_speed;
};

struct flame *flame_new(int N)
{
  struct flame *f = calloc(1, sizeof(*f));
  f->N = N;
  f->T = calloc(1, sizeof(*f->T) * N);
  f->probability = calloc(1, sizeof(*f->probability) * N);
  for (int from = 0; from < N; ++from)
  {
    f->probability[from] = calloc(1, sizeof(*f->probability[from]) * N);
  }
  f->colour = calloc(1, sizeof(*f->colour) * N);
  f->colour_speed = calloc(1, sizeof(*f->colour_speed) * N);
  return f;
}

void flame_delete(struct flame *f)
{
  free(f->colour_speed);
  free(f->colour);
  for (int from = 0; from < f->N; ++from)
  {
    free(f->probability[from]);
  }
  free(f->T);
  free(f);
}

void flame_randomize(struct flame *f)
{
  for (int t = 0; t < f->N; ++t)
  {
    moebius_randomize(&f->T[t]);
    moebius_normalize(&f->T[t]);
  }
  for (int from = 0; from < f->N; ++from)
  {
    for (int to = 0; to < f->N; ++to)
    {
      f->probability[from][to] = 1.0 / f->N;
    }
  }
  for (int t = 0; t < f->N; ++t)
  {
    f->colour[t] = 15.0 * rand() / (double) RAND_MAX;
    f->colour_speed[t] = rand() / (double) RAND_MAX;
  }
}

void flame_interpolate(struct flame *g, const struct flame *f, double t)
{
  assert(g->N == f->N);
  const int N = f->N;
  for (int from = 0; from < N; ++from)
  {
    g->T[from].A = f->T[from].A * cpow(f->T[(from + 1) % N].A / f->T[from].A, t);
    g->T[from].B = f->T[from].B * cpow(f->T[(from + 1) % N].B / f->T[from].B, t);
    g->T[from].C = f->T[from].C * cpow(f->T[(from + 1) % N].C / f->T[from].C, t);
    g->T[from].D = f->T[from].D * cpow(f->T[(from + 1) % N].D / f->T[from].D, t);
    moebius_normalize(&g->T[from]);
    g->colour[from] = f->colour[from] + (f->colour[(from + 1) % N] - f->colour[from]) * t;
    g->colour_speed[from] = f->colour_speed[from] + (f->colour_speed[(from + 1) % N] - f->colour_speed[from]) * t;
  }
}

struct image
{
  uint64_t width;
  uint64_t height;
  unsigned char *data;
};

struct image *image_new(int size)
{
  struct image *i = calloc(1, sizeof(*i));
  i->width = size;
  i->height =  2 * size;
  i->data = calloc(1, i->width * i->height * 3);
  return i;
}

void image_delete(struct image *i)
{
  free(i->data);
  free(i);
}

void image_from_histogram(struct image *i, const struct histogram *h)
{
  assert(i->width == h->width);
  assert(i->height == 2 * h->height);
  for (uint64_t w = 0; w < 2; ++w)
  {
    for (uint64_t k = 0; k < i->width * h->height; ++k)
    {
      double L = 64 * log2(1 + log2(1 + h->data[w][k].count)) / (1 + h->data[w][k].count);
      for (uint64_t c = 0; c < 3; ++c)
      {
        i->data[(k + w * i->width * h->height) * 3 + c] = fmax(0, fmin(255, h->data[w][k].rgb[c] * L));
      }
    }
  }
}

void image_save_ppm(const struct image *i, const char *filename)
{
  FILE *o = fopen(filename, "wb");
  fprintf(o, "P6\n%" PRIu64 " %" PRIu64 "\n255\n", i->width, i->height);
  fwrite(i->data, i->width * i->height * 3, 1, o);
  fclose(o);
}

void autoxaos(struct image *i, struct histogram *h, struct flame *f, double p[16][3], int q, char n, int gd, unsigned int *seed)
{
  const int do_auto = n == 'a';
  const int generations = 16;
  const int fuse = 100;
  const int fill = fuse + 1 * h->total_buckets;
  const int iterations = fill + 64 * h->total_buckets;
  const double learning = 0.01;

  for (int generation = 0; generation < generations; ++generation)
  {
    histogram_clear(h);
    double colour = 0;
    double _Complex z = 0;
    int from = 0;
    for (int iteration = 0; iteration < iterations; ++iteration)
    {

      // pick random transformation
      if (! gd)
      {
        from = 0;
      }
      double r = rand_r(seed) / (double) RAND_MAX;
      int to = 0;
      for (int k = 0; k < f->N; ++k)
      {
        if (r <= f->probability[from][k])
        {
          to = k;
          break;
        }
        else
        {
          r -= f->probability[from][k];
        }
      }

      // update point
      z = moebius_eval(&f->T[to], z);
      colour = colour + (f->colour[to] - colour) * f->colour_speed[to];

      if (iteration < fuse)
      {
        from = to;
        continue;
      }

      // plot point
      int c = colour;
      float t = colour - c;
      double rgb[3];
      for (int i = 0; i < 3; ++i)
      {
        rgb[i] = p[c][i] + (p[c+1][i] - p[c][i]) * t;
      }
      double scale = histogram_plot(h, z, rgb);

      if (iteration < fill)
      {
        from = to;
        continue;
      }

      if (do_auto)
      {
        // update transform weight
        f->probability[from][to] = pow(f->probability[from][to], pow(scale, -learning));

        // normalize probabilities
        double sum = 0;
        for (int k = 0; k < f->N; ++k)
        {
          sum += f->probability[from][k];
        }
        for (int pass = 0; pass < 3; ++pass)
        {
          scale = 1 / sum;
          sum = 0;
          for (int k = 0; k < f->N; ++k)
          {
            if (f->probability[from][k] != 0)
            {
              f->probability[from][k] = fmax(0.00001, fmin(0.99999, f->probability[from][k] * scale));
            }
            sum += f->probability[from][k];
          }
        }
        scale = 1 / sum;
        for (int k = 0; k < f->N; ++k)
        {
          f->probability[from][k] *= scale;
        }

        from = to;
        continue;
      }
      from = to;        
    }
  }

  // save image
  if (n != 'x')
  {
    image_from_histogram(i, h);
    char filename[100];
    snprintf(filename, 100, "autoxaos-%04d-%c.ppm", q, n);
    image_save_ppm(i, filename);
  }
}

int main(int argc, char **argv)
{
  const int k = argc > 1 ? atoi(argv[1]) : 0;
  srand(0x1cedCafeu + k);
  unsigned int seed = 0xC01dCafeu + k;

  const int gd = argc > 2 ? atoi(argv[2]) : 0;

  // randomize palette
  double p[16][3];
  for (int i = 0; i < 16; ++i)
  {
    for (int c = 0; c < 3; ++c)
    {
      p[i][c] = rand() / (double) RAND_MAX;
    }
  }

  // randomize flame
  const int N = 2 + (k % 5);
  struct flame *f = flame_new(N);
  flame_randomize(f);
  
#if 0
  // regular/designed flame
  int ix = 0;
  for (int x = -1; x <= 1; x += 2)
  {
    for (int y = -1; y <= 1; y += 2)
    {
      f->T[ix].A = I;
      f->T[ix].B = x + I * y;
      f->T[ix].C = 0;
      f->T[ix].D = 2;
      f->colour[ix] = 16 * (ix + 0.5) / 4;
      f->colour_speed[ix] = 0.5;
      ix++;
    }
  }
  for (int x = -1; x <= 1; x += 1)
  {
    for (int y = -1; y <= 1; y += 1)
    {
      if (x * x == y * y)
      {
        continue;
      }
      f->T[ix].A = 1;
      f->T[ix].B = x + I * y;
      f->T[ix].C = 0;
      f->T[ix].D = -I;
      f->colour_speed[ix] = 0;
      ix++;
    }
  }
#endif

  const int size = 512;
  struct image *i = image_new(size);
  struct histogram *h = histogram_new(size);

  const char modes[3] = { 'r', 'a', 'u' };
  for (int m = 0; m < 3; ++m)
  {
    if (m == 0)
    {
      // random weights
      for (int from = 0; from < N; ++from)
      {
        double sum = 0;
        for (int to = 0; to < N; ++to)
        {
          f->probability[from][to] = pow(rand_r(&seed) / (double) RAND_MAX, 4);
          sum += f->probability[from][to];
        }
        for (int to = 0; to < N; ++to)
        {
          f->probability[from][to] /= sum;
        }
      }
    }
    else
    {
      // uniform weights
      for (int from = 0; from < N; ++from)
      {
        for (int to = 0; to < N; ++to)
        {
          f->probability[from][to] = 1.0 / N;
        }
      }
    }

    // main algorithm
    autoxaos(i, h, f, p, k, modes[m], gd, &seed);

    if (m == 1)
    {
      // print calculated weights
      for (int from = 0; from < N; ++from)
      {
        for (int to = 0; to < N; ++to)
        {
          fprintf(stdout, "\t%f", f->probability[from][to]);
        }
        fprintf(stdout, "\n");
      }
    }
  }

  // cleanup
  histogram_delete(h);
  image_delete(i);
  flame_delete(f);
  return 0;
}
