#include <cassert>
#include <complex>
#include <vector>

#include <mpfr.h>
#include <mpc.h>

std::complex<long double> mpc_get_complex(const mpc_t z, mpc_rnd_t rnd);
std::vector<std::complex<long double>> reference(mpc_t Z, mpc_t C, long double Zoom, size_t Iterations);

struct Error
{
	double mean;
	double rms;
	double maximum;
};

Error error(const std::vector<std::complex<long double>> &ref1, const std::vector<std::complex<long double>> &ref2);

template <typename T>
size_t memory(const std::vector<T> &ref)
{
	return sizeof(&ref) + ref.size() * sizeof(ref[0]);
}
