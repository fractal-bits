#pragma once

#include <complex>
#include <vector>

struct ImaginaWayPoint
{
	std::complex<long double> Z;
	uint64_t Iteration : 63;
	uint64_t Rebase : 1;
};

struct ImaginaCompressedRef
{
	std::vector<ImaginaWayPoint> WayPoints;
	std::vector<size_t> Rebases;
	std::complex<long double> c;
	size_t N;
};

ImaginaCompressedRef imaginaCompress(const std::vector<std::complex<long double>> &Z);

std::vector<std::complex<long double>> imaginaDecompress(const ImaginaCompressedRef &CR);

size_t memory(const ImaginaCompressedRef &CR);

