#include "reference.h"

std::complex<long double> mpc_get_complex(const mpc_t z, mpc_rnd_t rnd)
{
	(void) rnd; // FIXME
	return { mpfr_get_ld(mpc_realref(z), MPFR_RNDN), mpfr_get_ld(mpc_imagref(z), MPFR_RNDN) };
}

std::vector<std::complex<long double>> reference(mpc_t Z, mpc_t C, long double Zoom, size_t Iterations)
{
	long double Zoom2 = Zoom * Zoom;
	std::vector<std::complex<long double>> ref;
	ref.reserve(Iterations);
	ref.push_back(0);
	size_t n = 0;
	std::complex<long double> z = 0, dz = 0;
	while (n < Iterations)
	{
		dz = (long double) 2 * dz * z + (long double) 1;
		mpc_sqr(Z, Z, MPC_RNDNN);
		mpc_add(Z, Z, C, MPC_RNDNN);
		z = mpc_get_complex(Z, MPC_RNDNN);
		ref.push_back(z);
		++n;
		if (Zoom2 * std::norm(z) < std::norm(dz))
		{
			break;
		}
	}
	ref.resize(ref.size());
	return ref;
}

Error error(const std::vector<std::complex<long double>> &ref1, const std::vector<std::complex<long double>> &ref2)
{
	assert(ref1.size() == ref2.size());
	double total0 = 0, total1 = 0, total2 = 0, maximum = 0;;
	for (size_t i = 1; i < ref1.size(); ++i)
	{
		double e = std::max(0.5 * std::log2(std::norm((ref2[i] - ref1[i]) / ref1[i])) + 64, (long double) 0);
		if (std::isnan(e))
		{
			assert(ref1[i] == std::complex<long double>(0));
			continue;
		}
		total0 += 1;
		total1 += e;
		total2 += e * e;
		maximum = std::max(maximum, e);
	}
	return Error{ total1 / total0, std::sqrt(total2 / total0), maximum };
}
