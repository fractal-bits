#pragma once

#include "simple.h"

struct Image
{
	std::vector<unsigned char> data;
	int Width;
	int Height;
};

Image renderUncompressed(const std::vector<std::complex<long double>> &R, int Width, int Height, long double Zoom, size_t Iterations);

Image renderSimple(const SimpleCompressedRef &CR, int Width, int Height, long double Zoom, size_t Iterations);

void writePPM(const char *filename, const Image &img);
