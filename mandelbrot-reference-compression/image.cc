#include <cstdio>

#include "image.h"

Image renderUncompressed(const std::vector<std::complex<long double>> &R, int Width, int Height, long double Zoom, size_t Iterations)
{
	std::vector<unsigned char> img;
	img.resize(3 * Width * Height);
	long double EscapeRadius = exp(M_PI);
	long double EscapeRadius2 = EscapeRadius * EscapeRadius;
	#pragma omp parallel for
	for (int j = 0; j < Height; ++j)
	{
		for (int i = 0; i < Width; ++i)
		{
			std::complex<long double> delta_c = std::complex<long double>((i + 0.5 - 0.5 * Width), -(j + 0.5 - 0.5 * Height)) / (long double) Height * (long double) 4 / Zoom;
			std::complex<long double> delta_z = 0;
			size_t m = 0;
			size_t n = 0;
			std::complex<long double> ref_z = R[m++];
			while (n < Iterations)
			{
				std::complex<long double> orbit_z = ref_z + delta_z;
				if (std::norm(orbit_z) < std::norm(delta_z) || ! (m < R.size()))
				{
					delta_z = orbit_z;
					m = 0;
					ref_z = R[m++];
				}
				if (std::norm(orbit_z) > EscapeRadius2)
				{
					break;
				}
				++n;
				delta_z = ((long double) 2 * ref_z + delta_z) * delta_z + delta_c;
				ref_z = R[m++];
			}
			img[3 * (Width * j + i) + 0] = n;
			img[3 * (Width * j + i) + 1] = n >> 8;
			img[3 * (Width * j + i) + 2] = n >> 16;
		}
	}
	return Image{ img, Width, Height };
}

Image renderSimple(const SimpleCompressedRef &CR, int Width, int Height, long double Zoom, size_t Iterations)
{
	std::vector<unsigned char> img;
	img.resize(3 * Width * Height);
	long double EscapeRadius = exp(M_PI);
	long double EscapeRadius2 = EscapeRadius * EscapeRadius;
	#pragma omp parallel for
	for (int j = 0; j < Height; ++j)
	{
		SimpleDecompressor D = simpleDecompressor(CR);
		for (int i = 0; i < Width; ++i)
		{
			rewind(D);
			std::complex<long double> delta_c = std::complex<long double>((i + 0.5 - 0.5 * Width), -(j + 0.5 - 0.5 * Height)) / (long double) Height * (long double) 4 / Zoom;
			std::complex<long double> delta_z = 0;
			std::complex<long double> ref_z = next(D);
			size_t n = 0;
			while (n < Iterations)
			{
				std::complex<long double> orbit_z = ref_z + delta_z;
				if (std::norm(orbit_z) < std::norm(delta_z) || ! has_next(D))
				{
					delta_z = orbit_z;
					rewind(D);
					ref_z = next(D);
				}
				if (std::norm(orbit_z) > EscapeRadius2)
				{
					break;
				}
				++n;
				delta_z = ((long double) 2 * ref_z + delta_z) * delta_z + delta_c;
				ref_z = next(D);
			}
			img[3 * (Width * j + i) + 0] = n;
			img[3 * (Width * j + i) + 1] = n >> 8;
			img[3 * (Width * j + i) + 2] = n >> 16;
		}
	}
	return Image{ img, Width, Height };
}

void writePPM(const char *filename, const Image &img)
{
	FILE *out = std::fopen(filename, "wb");
	std::fprintf(out, "P6\n%d %d\n255\n", img.Width, img.Height);
	std::fwrite(&img.data[0], img.Width * img.Height * 3, 1, out);
	fclose(out);
}
