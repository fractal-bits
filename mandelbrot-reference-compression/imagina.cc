#include "imagina.h"
#include "reference.h"

ImaginaCompressedRef imaginaCompress(const std::vector<std::complex<long double>> &Z)
{
	long double Threshold = 1ULL << 24;
	long double Threshold2 = Threshold * Threshold;
	std::complex<long double> z, c = Z[1];
	std::vector<ImaginaWayPoint> WayPoints;
	std::vector<size_t> Rebases;
	size_t i = 1;
	z = c;
	for (; i < Z.size(); i++)
	{
		if (std::norm(Z[i]) < 0x1.0p-4)
		{
			z = Z[i];
			WayPoints.push_back(ImaginaWayPoint{ z, i, true });
			break;
		}
		else if (std::norm(z - Z[i]) * Threshold2 > std::norm(Z[i]))
		{
			z = Z[i];
			WayPoints.push_back(ImaginaWayPoint{ z, i, false });
		}
		z = z * z + c;
	}
	std::complex<long double> dz = z;
	size_t PrevWayPointIteration = i;
	dz = (Z[0] * (long double) 2 + dz) * dz;
	i++;
	size_t j = 1;
	for (; i < Z.size(); i++, j++)
	{
		z = dz + Z[j];
		if (j >= PrevWayPointIteration || std::norm(z - Z[i]) * Threshold2 > std::norm(Z[i])) // TODO: two thresholds?
		{
			PrevWayPointIteration = i;
			z = Z[i];
			dz = z - Z[j];
			if (std::norm(z) < std::norm(dz) || (i - j) * 4 < i) // TODO: Improve second condition
			{
				dz = z;
				j = 0;
				WayPoints.push_back(ImaginaWayPoint{ dz, i, true });
			}
			else
			{
				WayPoints.push_back(ImaginaWayPoint{ dz, i, false });
			}
		}
		else if (std::norm(z) < std::norm(dz) * 0x1.000001p0)
		{
			dz = z;
			j = 0;
			if (Rebases.size() && Rebases[Rebases.size() - 1] > WayPoints[WayPoints.size() - 1].Iteration)
			{
				Rebases[Rebases.size() - 1] = i;
			}
			else
			{
				Rebases.push_back(i);
			}
		}
		dz = (Z[j] * (long double) 2 + dz) * dz;
	}
	WayPoints.push_back(ImaginaWayPoint{ 0, ~0ull, false }); // Dummy
	Rebases.push_back(~0ull); // Dummy
	return ImaginaCompressedRef{ WayPoints, Rebases, c, Z.size() };
}

std::vector<std::complex<long double>> imaginaDecompress(const ImaginaCompressedRef &CR)
{
	std::vector<std::complex<long double>> Z;
	Z.reserve(CR.N);
	const std::vector<ImaginaWayPoint> &WayPoints = CR.WayPoints;
	const std::vector<size_t> &Rebases = CR.Rebases;
	const std::complex<long double> &c = CR.c;
	const size_t &N = CR.N;
	std::complex<long double> z = 0;
	size_t WayPointIndex = 0;
	size_t RebaseIndex = 0;
	ImaginaWayPoint NextWayPoint = WayPoints[0];
	size_t NextRebase = Rebases[0];
	auto CorrectOrbit = [&](size_t Begin, size_t End, std::complex<long double> diff) {
		std::complex<long double> dzdc = 1; // FIXME: scaling factor removed
		for (size_t i = End; i > Begin; )
		{
			i--;
			dzdc *= Z[i] * (long double) 2;
			Z[i] += diff /  dzdc;
		}
	};
	size_t i = 0;
	size_t UncorrectedOrbitBegin = 1;
	for (; i < N; i++)
	{
		if (i == NextWayPoint.Iteration)
		{
			CorrectOrbit(UncorrectedOrbitBegin, i, NextWayPoint.Z - z);
			UncorrectedOrbitBegin = i + 1;
			z = NextWayPoint.Z;
			bool Rebase = NextWayPoint.Rebase;
			WayPointIndex++;
			NextWayPoint = WayPoints[WayPointIndex];
			if (Rebase)
			{
				break;
			}
		}
		Z.push_back(z);
		z = z * z + c;
	}
	size_t j = 0;
	std::complex<long double> dz = z;
	for (; i < N; i++, j++) {
		z = dz + Z[j];
		if (i == NextWayPoint.Iteration)
		{
			if (NextWayPoint.Rebase)
			{
				dz = z;
				j = 0;
			}
			CorrectOrbit(UncorrectedOrbitBegin, i, NextWayPoint.Z - dz);
			UncorrectedOrbitBegin = i + 1;
			dz = NextWayPoint.Z;
			z = dz + Z[j];
			WayPointIndex++;
			NextWayPoint = WayPoints[WayPointIndex];
		}
		else if (i == NextRebase)
		{
			RebaseIndex++;
			NextRebase = Rebases[RebaseIndex];
			dz = z;
			j = 0;
		}
		else if (std::norm(z) < std::norm(dz))
		{
			dz = z;
			j = 0;
		}
		Z.push_back(z);
		dz = (Z[j] * (long double) 2 + dz) * dz;
	}
	return Z;
}

size_t memory(const ImaginaCompressedRef &CR)
{
	return memory(CR.WayPoints) + memory(CR.Rebases) + sizeof(CR.c) + sizeof(CR.N);
}
