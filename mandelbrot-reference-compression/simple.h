#pragma once

#include <complex>
#include <vector>

struct SimpleWayPoint
{
	std::complex<long double> Z;
	size_t Iteration;
};

struct SimpleCompressedRef
{
	std::vector<SimpleWayPoint> WayPoints;
	std::complex<long double> c;
	size_t N;
};

size_t memory(const SimpleCompressedRef &CR);

struct SimpleCompressor
{
	SimpleCompressedRef &CR;
	std::complex<long double> z;
	size_t n;
	long double threshold2;
};

SimpleCompressor simpleCompressor(SimpleCompressedRef &CR);

void next(SimpleCompressor &C, const std::complex<long double> &z);

struct SimpleDecompressor
{
	const SimpleCompressedRef &CR;
	std::complex<long double> z;
	size_t n;
	size_t k;
};

SimpleDecompressor simpleDecompressor(const SimpleCompressedRef &CR);

void rewind(SimpleDecompressor &D);

bool has_next(const SimpleDecompressor &D);

std::complex<long double> next(SimpleDecompressor &D);

SimpleCompressedRef simpleCompress(const std::vector<std::complex<long double>> &ref);

std::vector<std::complex<long double>> simpleDecompress(const SimpleCompressedRef &CR);
