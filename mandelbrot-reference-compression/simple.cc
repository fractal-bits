#include <cassert>

#include "simple.h"
#include "reference.h"

size_t memory(const SimpleCompressedRef &CR)
{
	return memory(CR.WayPoints) + sizeof(CR.c) + sizeof(CR.N);
}

SimpleCompressor simpleCompressor(SimpleCompressedRef &CR)
{
	return SimpleCompressor{ CR, 0, 0, 1ull << 48 };
}

void next(SimpleCompressor &C, const std::complex<long double> &z)
{
	if (C.n > 0)
	{
		if (! (std::norm(C.z - z) * C.threshold2 < std::norm(z)))
		{
			C.z = z;
			C.CR.WayPoints.push_back(SimpleWayPoint{ z, C.n });
		}
	}
	++C.n;
	C.z = C.z * C.z + C.CR.c;
}

SimpleDecompressor simpleDecompressor(const SimpleCompressedRef &CR)
{
	return SimpleDecompressor{ CR, 0, 0, 0 };
}

void rewind(SimpleDecompressor &D)
{
	D.z = 0;
	D.n = 0;
	D.k = 0;
}

bool has_next(const SimpleDecompressor &D)
{
	return D.n < D.CR.N;
}

std::complex<long double> next(SimpleDecompressor &D)
{
	std::complex<long double> z = D.z;
	assert(D.k < D.CR.WayPoints.size());
	assert(D.n < D.CR.WayPoints[D.k].Iteration);
	if (++D.n == D.CR.WayPoints[D.k].Iteration)
	{
		D.z = D.CR.WayPoints[D.k++].Z;
	}
	else
	{
		D.z = D.z * D.z + D.CR.c;
	}
	return z;
}

SimpleCompressedRef simpleCompress(const std::vector<std::complex<long double>> &ref)
{
	SimpleCompressedRef CR = { {}, ref[1], ref.size() };
	SimpleCompressor C = simpleCompressor(CR);
	for (const auto &z : ref)
	{
		next(C, z);
	}
	C.CR.WayPoints.push_back(SimpleWayPoint{ 0, ~0ull });
	return CR;
}

std::vector<std::complex<long double>> simpleDecompress(const SimpleCompressedRef &CR)
{
	std::vector<std::complex<long double>> ref;
	ref.reserve(CR.N);
	SimpleDecompressor D = simpleDecompressor(CR);
	while (has_next(D))
	{
		ref.push_back(next(D));
	}
	return ref;
}
