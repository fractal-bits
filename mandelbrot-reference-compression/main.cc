#include <cstdio>
#include <cstdlib>

#include "reference.h"
#include "simple.h"
#include "imagina.h"
#include "image.h"

int main(int argc, char **argv)
{
	if (argc != 7)
	{
		std::fprintf(stderr, "usage: %s Re Im Zoom Iterations Width Height\n", argv[0]);
		return 1;
	}
	long double Zoom = std::strtold(argv[3], 0);
	size_t prec = std::log2(Zoom) + 53;
	mpc_t C, Z;
	mpc_init2(C, prec);
	mpc_init2(Z, prec);
	mpfr_set_str(mpc_realref(C), argv[1], 10, MPFR_RNDN);
	mpfr_set_str(mpc_imagref(C), argv[2], 10, MPFR_RNDN);
	mpc_set_d(Z, 0, MPC_RNDNN);
	size_t Iterations = std::atol(argv[4]);
	int Width = std::atoi(argv[5]);
	int Height = std::atoi(argv[6]);
	std::printf("Computing reference...\n");
	std::fflush(stdout);
	std::vector<std::complex<long double>> ref_orig = reference(Z, C, Zoom, Iterations);
	std::printf("Compressing reference with Simple method...\n");
	std::fflush(stdout);
	SimpleCompressedRef SCR = simpleCompress(ref_orig);
	std::printf("Decompressing reference with Simple method...\n");
	std::fflush(stdout);
	std::vector<std::complex<long double>> ref_simple = simpleDecompress(SCR);
	std::printf("Compressing reference with Imagina method...\n");
	std::fflush(stdout);
	ImaginaCompressedRef ICR = imaginaCompress(ref_orig);
	std::printf("Decompressing reference with Imagina method...\n");
	std::fflush(stdout);
	std::vector<std::complex<long double>> ref_imagina = imaginaDecompress(ICR);
	std::printf("Compressing reference with Simple method...\n");
	std::fflush(stdout);
	SimpleCompressedRef BCR = simpleCompress(ref_imagina);
	std::printf("Decompressing reference with Simple method...\n");
	std::fflush(stdout);
	std::vector<std::complex<long double>> ref_both = simpleDecompress(BCR);
	std::printf("Computing error...\n");
	std::fflush(stdout);
	Error e_simple = error(ref_orig, ref_simple);
	Error e_imagina = error(ref_orig, ref_imagina);
	Error e_both = error(ref_orig, ref_both);
	size_t mem_orig = memory(ref_orig);
	size_t mem_simple = memory(SCR);
	size_t mem_imagina = memory(ICR);
	size_t mem_both = memory(BCR);
	size_t mem_C = (prec + 63) / 64 * 8 * 2;
	std::printf("Done\n\n");
	std::fflush(stdout);
	std::printf("Error (Simple): \t%g\t%g\t%g\n", e_simple.mean, e_simple.rms, e_simple.maximum);
	std::printf("Error (Imagina):\t%g\t%g\t%g\n", e_imagina.mean, e_imagina.rms, e_imagina.maximum);
	std::printf("Error (Both):   \t%g\t%g\t%g\n", e_both.mean, e_both.rms, e_both.maximum);
	std::printf("Memory (C);     \t\t\t%lu\n", mem_C);
	std::printf("Memory (Orig):  \t\t\t%lu\n", mem_orig);
	std::printf("Ratio (Simple): \t%g\t%g\t%lu\n", mem_orig / (double) mem_simple, mem_C / (double) mem_simple, mem_simple);
	std::printf("Ratio (Imagina):\t%g\t%g\t%lu\n", mem_orig / (double) mem_imagina, mem_C / (double) mem_imagina, mem_imagina);
	std::printf("Ratio (Both):   \t%g\t%g\t%lu\n", mem_orig / (double) mem_both, mem_C / (double) mem_both, mem_both);
	std::printf("\n");
	std::printf("Simple\n");
	std::printf("N\t%lu\t%lu\nC\t\t%.20Le\t%.20Le\n", SCR.N, SCR.WayPoints.size(), real(SCR.c), imag(SCR.c));
	for (const auto &w : SCR.WayPoints)
	{
		std::printf("%lu\t%.20Le\t%.20Le\n", w.Iteration, real(w.Z), imag(w.Z));
	}
	std::printf("\n");
	std::printf("Imagina\n");
	std::printf("N\t%lu\t%lu\nC\t\t%.20Le\t%.20Le\n", ICR.N, ICR.WayPoints.size(), real(ICR.c), imag(ICR.c));
	size_t i = 0;
	size_t j = 0;
	size_t n = std::min(ICR.WayPoints[i].Iteration, ICR.Rebases[j]);
	while (n < ICR.N)
	{
		if (n == ICR.WayPoints[i].Iteration)
		{
			ImaginaWayPoint w = ICR.WayPoints[i++];
			std::printf("%c\t%lu\t%.20Le\t%.20Le\n", w.Rebase ? '+' : '-', w.Iteration, real(w.Z), imag(w.Z));
		}
		else
		{
			std::printf("*\t%lu\n", ICR.Rebases[j++]);
		}
		n = std::min(ICR.WayPoints[i].Iteration, ICR.Rebases[j]);
	}
	std::printf("\n");
	std::printf("Both\n");
	std::printf("N\t%lu\t%lu\nC\t\t%.20Le\t%.20Le\n", SCR.N, SCR.WayPoints.size(), real(SCR.c), imag(SCR.c));
	for (const auto &w : SCR.WayPoints)
	{
		std::printf("%lu\t%.20Le\t%.20Le\n", w.Iteration, real(w.Z), imag(w.Z));
	}
	std::printf("\n");
	std::printf("Rendering image with Uncompressed reference...\n");
	std::fflush(stdout);
	Image img1 = renderUncompressed(ref_orig, Width, Height, Zoom, Iterations);
	std::printf("Saving image...\n");
	writePPM("uncompressed.ppm", img1);
	std::printf("Rendering image with Simple compressed reference...\n");
	std::fflush(stdout);
	Image img2 = renderSimple(SCR, Width, Height, Zoom, Iterations);
	std::printf("Saving image...\n");
	std::fflush(stdout);
	writePPM("simple.ppm", img2);
	std::printf("Rendering image with Both compressed reference...\n");
	std::fflush(stdout);
	Image img3 = renderSimple(BCR, Width, Height, Zoom, Iterations);
	std::printf("Saving image...\n");
	std::fflush(stdout);
	writePPM("both.ppm", img3);
	std::printf("Done.\n");
	std::fflush(stdout);
	return 0;
}
