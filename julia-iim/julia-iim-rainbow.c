// gcc -std=c99 -Wall -Wextra -pedantic -O3 -ffast-math -o julia-iim-rainbow julia-iim-rainbow.c -lm
// ./julia-iim-rainbow -0.125000000000000 0.649519052838329 > fat-rabbit.ppm

#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define dpi 300
#define inches 6

#define size (dpi * inches)
unsigned char ppm[size][size][3];

static const double pi = 3.141592653589793;

double cnorm(double _Complex z)
{
  double x = creal(z);
  double y = cimag(z);
  return x * x + y * y;
}

inline void hsv2rgb(double hue, double sat, double bri, int *r, int *g, int *b)
{
  hue -= floor(hue);
  hue *= 6;
  int i = (int) floor(hue);
  double f = hue - i;
  if (! (i & 1))
    f = 1.0 - f;
  double m = bri * (1.0 - sat);
  double n = bri * (1.0 - sat * f);
  switch (i)
  {
  default:
  case 0: *r = 255 * bri;
    *g = 255 * n;
    *b = 255 * m;
    break;
  case 1: *r = 255 * n;
    *g = 255 * bri;
    *b = 255 * m;
    break;
  case 2: *r = 255 * m;
    *g = 255 * bri;
    *b = 255 * n;
    break;
  case 3: *r = 255 * m;
    *g = 255 * n;
    *b = 255 * bri;
    break;
  case 4: *r = 255 * n;
    *g = 255 * m;
    *b = 255 * bri;
    break;
  case 5: *r = 255 * bri;
    *g = 255 * m;
    *b = 255 * n;
  }
}

struct node
{
  struct node *next;
  double _Complex z;
  double h;
};

static struct node *freelist = 0;
struct node *allocate()
{
  if (freelist)
  {
    struct node *n = freelist;
    freelist = freelist->next;
    n->next = 0;
    n->z = 0;
    n->h = 0;
    return n;
  }
  else
  {
    return calloc(1, sizeof(struct node));
  }
}
void deallocate(struct node *n)
{
  n->next = freelist;
  freelist = n;
}

int main(int argc, char **argv)
{
  double _Complex c;
  if (argc > 2)
    c = atof(argv[1]) + I * atof(argv[2]);
  else if (argc > 1)
    c = atof(argv[1]);
  else
    c = 0;
  // repelling fixed point
  memset(&ppm[0][0][0], 0, size * size * 3);
  double scale = size / 5.0;
  double small = (0.25 * 0.25) / (scale * scale);
  double big = (1.0 * 1.0) / (scale * scale);
  // initialize loop
  struct node *loop = 0;
  for (int i = 0; i < 1 << 4; ++i)
  {
    double h = (i + 0.5) / (1 << 4);
    struct node *n = allocate();
    n->next = loop;
    n->z = 4 * cexp(2 * pi * I * h);
    n->h = h;
    loop = n;
  }
  // iterate
  for (int j = 0; j < 1 << 8; ++j)
  {
    // step loop twice (two branches)
    struct node *new_loop = 0;
    double _Complex z1 = 4;
    for (int pass = 0; pass < 2; ++pass)
    {
      struct node *old_loop = loop;
      while (old_loop)
      {
        // pick the root nearest the previous point in the new loop
        double _Complex z = csqrt(old_loop->z - c);
        double d1 = cnorm(z1 - z);
        double d2 = cnorm(z1 + z);
        struct node *n = allocate();
        n->next = new_loop;
        n->z = d1 < d2 ? z : -z;
        n->h = (old_loop->h + pass) * 0.5;
        new_loop = n;
        struct node *next = old_loop->next;
        if (pass == 1)
          deallocate(old_loop);
        old_loop = next;
        z1 = n->z;
      }
    }
    struct node *old_loop = new_loop;
    new_loop = 0;
    double _Complex z0 = old_loop->z;
    double _Complex h0 = old_loop->h;
    old_loop = old_loop->next;
    while (old_loop)
    {
      if (cnorm(old_loop->z - z0) < small)
      {
        // prune points that are too closely spaced
        struct node *next = old_loop->next;
        deallocate(old_loop);
        old_loop = next;
      }
      else if (cnorm(old_loop->z - z0) < big)
      {
        // move points that are reasonably spaced from old to new loop
        z0 = old_loop->z;
        h0 = old_loop->h;
        struct node *next = old_loop->next;
        old_loop->next = new_loop;
        new_loop = old_loop;
        old_loop = next;
      }
      else
      {
        // interpolate points that too distantly spaced
        struct node *next = allocate();
        next->next = old_loop;
        next->z = (z0 + old_loop->z) * 0.5;
        next->h = (h0 + old_loop->h) * 0.5;
        old_loop = next;
      }
    }
    loop = new_loop;
  }
  // plot loop
  while (loop)
  {
    int x = floor((creal(loop->z) + 2.5) * scale);
    int y = floor((cimag(loop->z) + 2.5) * scale);
    int r, g, b;
    hsv2rgb(loop->h, 1, 1, &r, &g, &b);
    ppm[y][x][0] = r;
    ppm[y][x][1] = g;
    ppm[y][x][2] = b;
    struct node *next = loop->next;
    deallocate(loop);
    loop = next;
  }
  fprintf(stdout, "P6\n# Julia set for %.18f + %.18f i\n%d %d\n255\n", creal(c), cimag(c), size, size);
  fwrite(&ppm[0][0][0], size * size * 3, 1, stdout);
  return 0;
}
