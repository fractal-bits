// gcc -std=c99 -Wall -Wextra -pedantic -O3 -ffast-math -o julia-iim julia-iim.c -lm
// ./julia-iim > j.pgm

#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define dpi 300
#define inches 6

#define size (dpi * inches)
unsigned char pgm[size][size];

int main()
{
  memset(&pgm[0][0], 255, size * size);
  double _Complex z = 1;
  double scale = size / 3.0;
  for (int i = 0; i < 1 << 29; ++i)
  {
    int x = floor((creal(z) + 1.0) * scale);
    int y = floor((cimag(z) + 1.5) * scale);
    pgm[y][x] = 0;
    int coin = rand() > RAND_MAX/2;
    if (coin)
      z = 0.5 + csqrt(0.25 - z);
    else
      z = 0.5 - csqrt(0.25 - z);
  }
  fprintf(stdout, "P5\n%d %d\n255\n", size, size);
  fwrite(&pgm[0][0], size * size, 1, stdout);
  return 0;
}
