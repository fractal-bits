// gcc -std=c99 -Wall -Wextra -pedantic -O3 -ffast-math -o julia-lsm julia-lsm.c -lm -fopenmp
// ./julia-lsm > j.ppm

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define aa 1
#define dpi 256
#define inches 6

#define size (aa * dpi * inches)
int status[size][size];
unsigned char ppm[size][size][3];

int main()
{
  double r2 = (3.0 / size) * (3.0 / size);
  {
    double _Complex z = 0.5;
    for (int k = 0; k < 1 << 24; ++k)
    {
      z = z - z * z;
      double z2 = creal(z) * creal(z) + cimag(z) * cimag(z);
      if (z2 < r2)
      {
        r2 = z2;
        break;
      }
    }
  }
  #pragma omp parallel for
  for (int j = 0; j < size; ++j)
    for (int i = 0; i < size; ++i)
    {
      status[j][i] = 0;
      double _Complex z = ((i + 0.0) * 3.0 / size - 1) + I * ((j + 0.0) * 3.0 / size - 1.5);
      for (int k = 0; k < 1 << 24; ++k)
      {
        z = z - z * z;
        double z2 = creal(z) * creal(z) + cimag(z) * cimag(z);
        if (z2 > 600)
        {
          status[j][i] = cimag(z) > 0 ? (k + 1) : (k + 1 + (1 << 24));
          break;
        }
        if (z2 <= r2)
        {
          status[j][i] = -(k + 1);
          break;
        }
      }
    }
  memset(&ppm[0][0][0], 255, size * size * 3);
  #pragma omp parallel for
  for (int j = 0; j < size; ++j)
  {
    int jj = j;
    int jj1 = j-1;
    if (jj1 < 0) jj1 = 0;
    for (int i = 0; i < size; ++i)
    {
      int ii = i;
      int ii1 = i-1;
      if (ii1 < 0) ii1 = 0;
      bool level = status[jj][ii] != status[jj1][ii1] || status[jj][ii1] != status[jj1][ii];
      bool julia = (status[jj][ii]>0) != (status[jj1][ii1]>0) || (status[jj][ii1]>0) != (status[jj1][ii]>0);
      if (level)
      {
        if (status[jj][ii] > 0)
        {
          ppm[j][i][1] *= 0;
          ppm[j][i][2] *= 0;
        }
        else
        {
          ppm[j][i][0] *= 0;
          ppm[j][i][2] *= 0;
        }
      }
      if (julia)
#if 1
        for (int j2 = j - 2; j2 <= j + 2; ++j2)
          for (int i2 = i - 2; i2 <= i + 2; ++i2)
          {
            double r2 = (i2-i) * (i2-i) + (j2-j) * (j2-j);
            if (r2 <= 4)
            {
#else
            {
              int j2 = j;
              int i2 = i;
#endif
              ppm[j2][i2][0] *= 0;
              ppm[j2][i2][1] *= 0;
              ppm[j2][i2][2] *= 0;
            }
#if 1
          }
#endif
    }
  }
  fprintf(stdout, "P6\n%d %d\n255\n", size, size);
  fwrite(&ppm[0][0][0], size * size * 3, 1, stdout);
  return 0;
}
