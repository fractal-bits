#include "julia.h"

static _Thread_local
struct
{
  // constant
  struct JULIA * restrict julia;
  struct TREE * restrict tree;
  unsigned char * restrict visited;
  struct C bounds;
  struct FUNCTION_C function;
  // source traversal
  int64_t source_cursor;
  // sink traversal
  julia_node_t source;
  struct C target;
  int64_t sink_cursor;
  // output
  bool full;
} aef;

void JULIA_add_edges_from_tl(struct C bounds)
{
  if (C_outside(aef.target, bounds) || aef.full)
  {
    TREE_skip(aef.tree, aef.sink_cursor);
    aef.sink_cursor = TREE_skip_advance(aef.tree, aef.sink_cursor);
    return;
  }
  int64_t before = aef.sink_cursor;
  enum TREE_NODE node = TREE_read_node(aef.tree, aef.sink_cursor);
  aef.sink_cursor = TREE_read_node_advance(aef.tree, aef.sink_cursor);
  switch (node)
  {
    case WHITE:
    {
      if (! JULIA_visited_get_raw(aef.visited, aef.source))
      {
        JULIA_visited_set_raw(aef.visited, aef.source);
        aef.full |= JULIA_add_edge(aef.julia, aef.source, JULIA_EXTERIOR);
      }
      break;
    }
    case BLACK:
    case GREY:
    {
      julia_node_t sink = before;
      aef.full |= JULIA_add_edge(aef.julia, aef.source, sink);
      break;
    }
    case BRANCH:
      TREE_read_branch_enter(aef.tree, aef.sink_cursor);
      aef.sink_cursor = TREE_read_branch_enter_advance(aef.tree, aef.sink_cursor);
      double x = (bounds.x.lo + bounds.x.hi) * 0.5f;
      double y = (bounds.y.lo + bounds.y.hi) * 0.5f;
      JULIA_add_edges_from_tl((struct C){{bounds.x.lo, x}, {bounds.y.lo, y}});
      JULIA_add_edges_from_tl((struct C){{x, bounds.x.hi}, {bounds.y.lo, y}});
      JULIA_add_edges_from_tl((struct C){{bounds.x.lo, x}, {y, bounds.y.hi}});
      JULIA_add_edges_from_tl((struct C){{x, bounds.x.hi}, {y, bounds.y.hi}});
      TREE_read_branch_leave(aef.tree, aef.sink_cursor);
      aef.sink_cursor = TREE_read_branch_leave_advance(aef.tree, aef.sink_cursor);
      break;
    default:
      assert(! "valid node");
      break;
  }
}

void JULIA_add_edges_tl(struct C bounds)
{
  int64_t before = aef.source_cursor;
  enum TREE_NODE node = TREE_read_node(aef.tree, aef.source_cursor);
  aef.source_cursor = TREE_read_node_advance(aef.tree, aef.source_cursor);
  switch (node)
  {
    case BLACK:
    case WHITE:
      break;
    case GREY:
    {
      aef.source = before;
      aef.target = aef.function.f.f(&aef.function.f, bounds);
      if (! C_inside_or_equal(aef.target, aef.bounds))
      {
        JULIA_add_edge(aef.julia, aef.source, JULIA_EXTERIOR);
        JULIA_visited_set_raw(aef.visited, aef.source);
      }
      aef.sink_cursor = 0;
      JULIA_add_edges_from_tl(aef.bounds);
      break;
    }
    case BRANCH:
      TREE_read_branch_enter(aef.tree, aef.source_cursor);
      aef.source_cursor = TREE_read_branch_enter_advance(aef.tree, aef.source_cursor);
      double x = (bounds.x.lo + bounds.x.hi) * 0.5f;
      double y = (bounds.y.lo + bounds.y.hi) * 0.5f;
      JULIA_add_edges_tl((struct C){{bounds.x.lo, x}, {bounds.y.lo, y}});
      JULIA_add_edges_tl((struct C){{x, bounds.x.hi}, {bounds.y.lo, y}});
      JULIA_add_edges_tl((struct C){{bounds.x.lo, x}, {y, bounds.y.hi}});
      JULIA_add_edges_tl((struct C){{x, bounds.x.hi}, {y, bounds.y.hi}});
      TREE_read_branch_leave(aef.tree, aef.source_cursor);
      aef.source_cursor = TREE_read_branch_leave_advance(aef.tree, aef.source_cursor);
      break;
    default:
      assert(! "valid node");
      break;
  }
}

bool JULIA_add_edges(struct JULIA * restrict julia)
{
  aef.julia = julia;
  aef.tree = julia->tree;
  aef.visited = julia->graph->node_visited;
  aef.bounds = julia->bounds;
  aef.function = julia->function;
  aef.source_cursor = 0;
  aef.full = false;
  JULIA_add_edges_tl(julia->bounds);
  return aef.full;
}

bool JULIA_graph_new(struct JULIA * restrict julia)
{
  assert(julia);
  assert(! julia->graph);
  TREE_skip(julia->tree, 0);
  int64_t count = TREE_skip_advance(julia->tree, 0);
  julia->node_max = JULIA_to_GRAPH(count);
  julia->graph = GRAPH_new(julia->node_max);
  if (! julia->graph)
  {
    return false;
  }
  JULIA_visited_clear(julia);
  bool full = JULIA_add_edges(julia);
  if (full)
  {
    GRAPH_delete(julia->graph);
    julia->graph = NULL;
    return false;
  }
  GRAPH_sort(julia->graph);
  return true;
}

void JULIA_graph_delete(struct JULIA * restrict julia)
{
  assert(julia);
  assert(julia->graph);
  GRAPH_delete(julia->graph);
  julia->graph = NULL;
}

void JULIA_whiten_node(struct JULIA * restrict julia, julia_node_t source)
{
  assert(julia);
  graph_edge_t lo, hi;
  JULIA_succs(julia, source, &lo, &hi);
  bool all_white = true;
  for (graph_edge_t e = lo; e < hi; ++e)
  {
    julia_node_t sink = JULIA_succs_succ(julia, e);
    all_white &= sink == JULIA_EXTERIOR || WHITE == TREE_read_node(julia->tree, sink);
  }
  if (all_white)
  {
    TREE_write_node(julia->tree, source, WHITE);
    julia_node_t sink = source;
    JULIA_preds(julia, sink, &lo, &hi);
    for (graph_edge_t e = lo; e < hi; ++e)
    {
      julia_node_t source = JULIA_preds_pred(julia, e);
      if (GREY == TREE_read_node(julia->tree, source))
      {
        JULIA_whiten_node(julia, source);
      }
    }
  }
}

int64_t JULIA_whiten(struct JULIA * restrict julia, int64_t cursor)
{
  int64_t before = cursor;
  enum TREE_NODE node = TREE_read_node(julia->tree, cursor);
  cursor = TREE_read_node_advance(julia->tree, cursor);
  switch (node)
  {
    case GREY:
      JULIA_whiten_node(julia, before);
      break;
    case BLACK:
      break;
    case WHITE:
      break;
    case BRANCH:
      TREE_read_branch_enter(julia->tree, cursor);
      cursor = TREE_read_branch_enter_advance(julia->tree, cursor);
      cursor = JULIA_whiten(julia, cursor);
      cursor = JULIA_whiten(julia, cursor);
      cursor = JULIA_whiten(julia, cursor);
      cursor = JULIA_whiten(julia, cursor);
      TREE_read_branch_leave(julia->tree, cursor);
      cursor = TREE_read_branch_leave_advance(julia->tree, cursor);
      break;
  }
  return cursor;
}

int64_t JULIA_blacken_node(struct JULIA * restrict julia, julia_node_t source)
{
  assert(julia);
  graph_edge_t lo, hi;
  JULIA_succs(julia, source, &lo, &hi);
  bool any_white = false;
  for (graph_edge_t e = lo; e < hi; ++e)
  {
    julia_node_t sink = JULIA_succs_succ(julia, e);
    any_white |= sink == JULIA_EXTERIOR || WHITE == TREE_read_node(julia->tree, sink);
  }
  if (! any_white)
  {
    TREE_write_node(julia->tree, source, BLACK);
  }
  return TREE_write_node_advance(julia->tree, source);
}

int64_t JULIA_blacken(struct JULIA * restrict julia, int64_t cursor)
{
  int64_t before = cursor;
  enum TREE_NODE node = TREE_read_node(julia->tree, cursor);
  cursor = TREE_read_node_advance(julia->tree, cursor);
  switch (node)
  {
    case GREY:
    {
      julia_node_t source = before;
      if (! GRAPH_visited(julia->graph, JULIA_to_GRAPH(source)))
      {
        TREE_write_node(julia->tree, source, BLACK);
      }
      break;
    }
    case BLACK:
      break;
    case WHITE:
      break;
    case BRANCH:
      TREE_read_branch_enter(julia->tree, cursor);
      cursor = TREE_read_branch_enter_advance(julia->tree, cursor);
      cursor = JULIA_blacken(julia, cursor);
      cursor = JULIA_blacken(julia, cursor);
      cursor = JULIA_blacken(julia, cursor);
      cursor = JULIA_blacken(julia, cursor);
      TREE_read_branch_leave(julia->tree, cursor);
      cursor = TREE_read_branch_leave_advance(julia->tree, cursor);
      break;
  }
  return cursor;
}

bool JULIA_refine(struct JULIA * restrict julia)
{
  assert(julia);
  julia->tree = TREE_refine_destroy(julia->tree);
  if (! julia->tree)
  {
    return false;
  }
  if (! JULIA_graph_new(julia))
  {
    return false;
  }
  JULIA_whiten(julia, 0);
  GRAPH_visit_clear(julia->graph);
  GRAPH_visit_pred(julia->graph, JULIA_to_GRAPH(JULIA_EXTERIOR));
  JULIA_blacken(julia, 0);
  JULIA_graph_delete(julia);
  TREE_prune_in_place(julia->tree, false);
  TREE_compact(julia->tree);
  if (! julia->tree)
  {
    return false;
  }
  return true;
}

enum TREE_NODE JULIA_classify(struct JULIA * restrict julia)
{
  int64_t cursor = 0;
  return TREE_classify(julia->tree, &cursor, julia->bounds);
}

struct JULIA *JULIA_fread_raw(FILE * restrict in)
{
  assert(in);
  struct JULIA *julia = MEMORY_alloc(sizeof(*julia));
  if (! julia)
  {
    return NULL;
  }
  struct C c;
  int64_t count = 0;
  if (9 != fscanf
    ( in
    , "Trustworthy Julia z->z^2+c\n"
      "c = { { %lf , %lf }, { %lf, %lf } }\n"
      "z = { { %lf , %lf }, { %lf, %lf } }\n"
      "n = %ld\n"
      "binary data follows"
    , &c.x.lo, &c.x.hi
    , &c.y.lo, &c.y.hi
    , &julia->bounds.x.lo, &julia->bounds.x.hi
    , &julia->bounds.y.lo, &julia->bounds.y.hi
    , &count
    ))
  {
    MEMORY_free(julia, sizeof(*julia));
    return NULL;
  }
  if ('\n' != fgetc(in))
  {
    MEMORY_free(julia, sizeof(*julia));
    return NULL;
  }
  if ('\f' != fgetc(in))
  {
    MEMORY_free(julia, sizeof(*julia));
    return NULL;
  }
  julia->function = FUNCTION_quadratic(c);
  julia->tree = TREE_fread_raw(in, count);
  if (! julia->tree)
  {
    MEMORY_free(julia, sizeof(*julia));
    return NULL;
  }
  julia->graph = NULL;
  julia->node_max = 0;
  return julia;
}

int JULIA_fwrite_raw(FILE * restrict out, struct JULIA * restrict julia)
{
  assert(out);
  assert(julia);
  int64_t count[4] = {0,0,0,0};
  TREE_count_nodes(julia->tree, 0, count);
  if (0 > fprintf
    ( out
    , "Trustworthy Julia z->z^2+c\n"
      "c = { { %.24f , %.24f }, { %.24f, %.24f } }\n"
      "z = { { %.24f , %.24f }, { %.24f, %.24f } }\n"
      "n = %ld\n"
      "binary data follows\n\f"
    , julia->function.c.x.lo, julia->function.c.x.hi
    , julia->function.c.y.lo, julia->function.c.y.hi
    , julia->bounds.x.lo, julia->bounds.x.hi
    , julia->bounds.y.lo, julia->bounds.y.hi
    , count[BRANCH]
    ))
  {
    return 1;
  }
  return TREE_fwrite_raw(out, julia->tree);
}
