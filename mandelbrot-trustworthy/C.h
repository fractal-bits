#pragma once

#include <assert.h>
#include <stdlib.h>
#include "R.h"

struct C
{
  struct R x;
  struct R y;
};

static inline
struct C C_add(struct C a, struct C b)
{
  return (struct C){ R_add(a.x, b.x), R_add(a.y, b.y) };
}

static inline
struct C C_sub(struct C a, struct C b)
{
  return (struct C){ R_sub(a.x, b.x), R_sub(a.y, b.y) };
}

static inline
struct C C_neg(struct C z)
{
  return (struct C){ R_neg(z.x), R_neg(z.y) };
}

static inline
struct C C_conj(struct C z)
{
  return (struct C){ z.x, R_neg(z.y) };
}

static inline
struct C C_sqr(struct C z)
{
  struct R x = R_sub(R_sqr(z.x), R_sqr(z.y));
  struct R y = R_mul2(R_mul(z.x, z.y));
  return (struct C){ x, y };
}

static inline
struct C C_split(struct C z, int y, int x)
{
  return (struct C){ R_split(z.x, x), R_split(z.y, y) };
}

static inline
bool C_inside_or_equal(struct C a, struct C b)
{
  return R_inside_or_equal(a.x, b.x) && R_inside_or_equal(a.y, b.y);
}

static inline
bool C_outside(struct C a, struct C b)
{
  return R_outside(a.x, b.x) || R_outside(a.y, b.y);
}

static inline
bool C_outside_or_equal(struct C a, struct C b)
{
  return R_outside_or_equal(a.x, b.x) || R_outside_or_equal(a.y, b.y);
}

static inline
bool C_equal(struct C a, struct C b)
{
  return R_equal(a.x, b.x) && R_equal(a.y, b.y);
}

static inline
double C_area(struct C a)
{
  return R_length(a.x) * R_length(a.y);
}

static inline
struct C C_sqrt(struct C a, int branch)
{
  (void) a;
  (void) branch;
  assert(! "implemented");
  abort();
}
