#pragma once

#include <assert.h>
#include <stdio.h>
#include <string.h>

struct BITSTREAM
{
  unsigned char *data;
  int byte;
  int bit;
};

static inline
void BITSTREAM_out(struct BITSTREAM *bs, int bit)
{
  unsigned char mask = 1 << bs->bit++;
  if (bit)
  {
    bs->data[bs->byte] |= mask;
  }
  else
  {
    bs->data[bs->byte] &= ~mask;
  }
  if (bs->bit == 8)
  {
    bs->bit = 0;
    ++bs->byte;
  }
}

static inline
int BITSTREAM_in(struct BITSTREAM *bs)
{
  unsigned char mask = 1 << bs->bit++;
  int bit = bs->data[bs->byte] & mask;
  if (bs->bit == 8)
  {
    bs->bit = 0;
    ++bs->byte;
  }
  return !!bit;
}

static inline
int BITSTREAM_read(struct BITSTREAM *bs, void *data, int length)
{
  assert(bs->bit == 0);
  memcpy(data, &bs->data[bs->byte], length);
  bs->byte += length;
  return 0;
}

static inline
int BITSTREAM_match(struct BITSTREAM *bs, const char *data)
{
  assert(bs->bit == 0);
  int r = strncmp((char *) &bs->data[bs->byte], data, strlen(data));
  if (r == 0)
  {
    bs->byte += strlen(data);
  }
  return r;
}
