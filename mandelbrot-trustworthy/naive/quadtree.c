#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "quadtree.h"

static
const char *svgclass[3] = { "g", "b", "w" };

struct NODE *NODE_POOL_alloc(struct NODE_POOL **poolp)
{
  if (*poolp && (*poolp)->index < (*poolp)->count)
  {
    return &(*poolp)->nodes[(*poolp)->index++];
  }
  else
  {
    struct NODE_POOL *pool = calloc(1, sizeof(*pool));
    pool->count = 65536;
    pool->nodes = calloc(1, sizeof(*pool->nodes) * pool->count);
    pool->next = *poolp;
    *poolp = pool;
    return NODE_POOL_alloc(poolp);
  }
}

void NODE_POOL_free(struct NODE_POOL **poolp)
{
  while (*poolp)
  {
    struct NODE_POOL *next = (*poolp)->next;
    free((*poolp)->nodes);
    free(*poolp);
    *poolp = next;
  }
}

struct EDGE *EDGE_POOL_alloc(struct EDGE_POOL **poolp)
{
  if (*poolp && (*poolp)->index < (*poolp)->count)
  {
    return &(*poolp)->edges[(*poolp)->index++];
  }
  else
  {
    struct EDGE_POOL *pool = calloc(1, sizeof(*pool));
    pool->count = 65536;
    pool->edges = calloc(1, sizeof(*pool->edges) * pool->count);
    pool->next = *poolp;
    *poolp = pool;
    return EDGE_POOL_alloc(poolp);
  }
}

void EDGE_POOL_free(struct EDGE_POOL **poolp)
{
  while (*poolp)
  {
    struct EDGE_POOL *next = (*poolp)->next;
    free((*poolp)->edges);
    free(*poolp);
    *poolp = next;
  }
}

void NODE_grow(struct NODE_POOL **pool, struct NODE *tree)
{
  switch (tree->tag)
  {
    case LEAF:
    {
      switch (tree->u.leaf.colour)
      {
        case GREY:
        {
          memset(tree, 0, sizeof(*tree));
          tree->tag = BRANCH;
          for (int y = 0; y < 2; ++y)
          {
            for (int x = 0; x < 2; ++x)
            {
              tree->u.branch.child[y][x] = NODE_POOL_alloc(pool);
            }
          }
          break;
        }
        case BLACK:
        case WHITE:
        {
          break;
        }
      }
      break;
    }
    case BRANCH:
    {
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          NODE_grow(pool, tree->u.branch.child[y][x]);
        }
      }
      break;
    }
  }
}

int NODE_prune(struct NODE *tree)
{
  switch (tree->tag)
  {
    case LEAF:
    {
      return 1;
    }
    case BRANCH:
    {
      int leaves = 0;
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          leaves += NODE_prune(tree->u.branch.child[y][x]);
        }
      }
      if (leaves == 4)
      {
        assert(tree->u.branch.child[0][0]->tag == LEAF);
        assert(tree->u.branch.child[0][1]->tag == LEAF);
        assert(tree->u.branch.child[1][0]->tag == LEAF);
        assert(tree->u.branch.child[1][1]->tag == LEAF);
        enum COLOUR colour00 = tree->u.branch.child[0][0]->u.leaf.colour;
        enum COLOUR colour01 = tree->u.branch.child[0][1]->u.leaf.colour;
        enum COLOUR colour10 = tree->u.branch.child[1][0]->u.leaf.colour;
        enum COLOUR colour11 = tree->u.branch.child[1][1]->u.leaf.colour;
        if (GREY != colour00 && colour00 == colour01 && colour00 == colour10 && colour00 == colour11)
        {
#if 0
          for (int y = 0; y < 2; ++y)
          {
            for (int x = 0; x < 2; ++x)
            {
              free_node(tree->u.branch.child[y][x]);
            }
          }
#endif
          memset(tree, 0, sizeof(*tree));
          tree->u.leaf.colour = colour00;
          return 1;
        }
      }
      return 0;
    }
  }
  return 0;
}

void EDGE_connect(struct EDGE_POOL **pool, struct NODE *source, struct NODE *sink)
{
  assert(source->tag == LEAF);
  assert(sink->tag == LEAF);
  struct EDGE *edge = EDGE_POOL_alloc(pool);
  edge->source = source;
  edge->sink = sink;
  edge->forward = source->u.leaf.forward;
  edge->backward = sink->u.leaf.backward;
  source->u.leaf.forward = edge;
  sink->u.leaf.backward = edge;
}

void NODE_clear_visited_flags(struct NODE *tree)
{
  switch (tree->tag)
  {
    case LEAF:
    {
      tree->u.leaf.visited = false;
      struct EDGE *backward = tree->u.leaf.backward;
      while (backward)
      {
        backward->visited = false;
        backward = backward->backward;
      }
      break;
    }
    case BRANCH:
    {
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          NODE_clear_visited_flags(tree->u.branch.child[y][x]);
        }
      }
      break;
    }
  }
}

void NODE_whiten_forward(struct NODE *tree)
{
  switch (tree->tag)
  {
    case LEAF:
    {
      if (tree->u.leaf.colour == GREY)
      {
        bool all_visited = true;
        struct EDGE *forward = tree->u.leaf.forward;
        while (all_visited && forward)
        {
          assert(forward->sink->tag == LEAF);
          all_visited &= forward->sink->u.leaf.colour == WHITE;
          all_visited &= forward->visited;
          forward = forward->forward;
        }
        if (all_visited)
        {
          tree->u.leaf.colour = WHITE;
        }
      }
      break;
    }
    case BRANCH:
    {
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          NODE_whiten_forward(tree->u.branch.child[y][x]);
        }
      }
      break;
    }
  }
}

void NODE_visit_backward(struct NODE *tree)
{
  assert(tree->tag == LEAF);
  if (tree->u.leaf.visited) return;
  tree->u.leaf.visited = true;
  struct EDGE *backward = tree->u.leaf.backward;
  while (backward)
  {
    backward->visited = true;
    NODE_visit_backward(backward->source);
    backward = backward->backward;
  }
}

void NODE_blacken_forward(struct NODE *tree)
{
  switch (tree->tag)
  {
    case LEAF:
    {
      if (tree->u.leaf.colour == GREY)
      {
        if (! tree->u.leaf.visited)
        {
          tree->u.leaf.colour = BLACK;
        }
      }
      break;
    }
    case BRANCH:
    {
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          NODE_blacken_forward(tree->u.branch.child[y][x]);
        }
      }
      break;
    }
  }
}

struct AREA NODE_area(struct NODE *tree, struct C z)
{
  struct AREA a = { { 0, 0, 0 } };
  switch (tree->tag)
  {
    case LEAF:
    {
      a.area[tree->u.leaf.colour] = (z.y.hi - z.y.lo) * (z.x.hi - z.x.lo);
      return a;
    }
    case BRANCH:
    {
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          a = AREA_add(a, NODE_area(tree->u.branch.child[y][x], C_split(z, y, x)));
        }
      }
      return a;
    }
  }
  return a;
}

void NODE_count(struct COUNT *count, struct NODE *tree)
{
  switch (tree->tag)
  {
    case LEAF:
    {
      ++count->count[tree->u.leaf.colour];
      break;
    }
    case BRANCH:
    {
      ++count->count[3];
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          NODE_count(count, tree->u.branch.child[y][x]);
        }
      }
      break;
    }
  }
}

void NODE_svg(FILE *out, struct NODE *tree, struct C z)
{
  switch (tree->tag)
  {
    case LEAF:
    {
      int x = z.x.lo;
      int y = z.y.lo;
      int w = z.x.hi - z.x.lo;
      int h = z.y.hi - z.y.lo;
      const char *c = svgclass[tree->u.leaf.colour];
      if (tree->u.leaf.colour == GREY && tree->u.leaf.attempted)
      {
        c = "a";
      }
      fprintf(out, "<rect x='%d' y='%d' width='%d' height='%d' class='%s' />", x, y, w, h, c);
      break;
    }
    case BRANCH:
    {
      fprintf(out, "<g>");
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          NODE_svg(out, tree->u.branch.child[y][x], C_split(z, y, x));
        }
      }
      fprintf(out, "</g>");
    }
  }
}

void NODE_serialize(struct BITSTREAM *out, struct NODE *tree, bool with_attempted)
{
  switch (tree->tag)
  {
    case LEAF:
    {
      BITSTREAM_out(out, tree->u.leaf.colour & 2);
      BITSTREAM_out(out, tree->u.leaf.colour & 1);
      if (with_attempted && tree->u.leaf.colour == GREY)
      {
        BITSTREAM_out(out, tree->u.leaf.attempted);
      }
      break;
    }
    case BRANCH:
    {
      BITSTREAM_out(out, 1);
      BITSTREAM_out(out, 1);
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          NODE_serialize(out, tree->u.branch.child[y][x], with_attempted);
        }
      }
      break;
    }
  }
}

struct NODE *NODE_deserialize(struct NODE_POOL **pool, struct BITSTREAM *bs, bool with_attempted)
{
  int b0 = BITSTREAM_in(bs);
  int b1 = BITSTREAM_in(bs);
  int c = (b0 << 1) | b1;
  switch (c)
  {
    case 0:
    case 1:
    case 2:
    {
      struct NODE *tree = NODE_POOL_alloc(pool);
      tree->u.leaf.colour = c;
      if (with_attempted && tree->u.leaf.colour == GREY)
      {
        tree->u.leaf.attempted = BITSTREAM_in(bs);
      }
      return tree;
    }
    case 3:
    {
      struct NODE *tree = NODE_POOL_alloc(pool);
      tree->tag = BRANCH;
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          tree->u.branch.child[y][x] = NODE_deserialize(pool, bs, with_attempted);
        }
      }
      return tree;
    }
  }
  return NULL;
}
