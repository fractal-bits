#include <stdint.h>
#include <stdlib.h>
#include "mandelbrot.h"
#include "julia.h"

struct MANDELBROT
{
  struct C bounds_left;
  struct C bounds_right;
  bool complete;
  struct NODE tree_left;
  struct NODE tree_right;
  struct NODE_POOL *node_pool;
};

struct MANDELBROT *MANDELBROT_alloc(void)
{
  struct MANDELBROT *mandelbrot = calloc(1, sizeof(*mandelbrot));
  struct C bounds;
  bounds.x.lo = -2 + -0.75;
  bounds.x.hi = 2 + -0.75;
  bounds.y.lo = -2;
  bounds.y.hi = 2;
  mandelbrot->bounds_left = C_split(bounds, 0, 0);
  mandelbrot->bounds_right = C_split(bounds, 0, 1);
  return mandelbrot;
}

void MANDELBROT_free(struct MANDELBROT *mandelbrot)
{
  NODE_POOL_free(&mandelbrot->node_pool);
  free(mandelbrot);
}

void MANDELBROT_print(struct MANDELBROT *mandelbrot)
{
  struct AREA al = NODE_area(&mandelbrot->tree_left, mandelbrot->bounds_left);
  struct AREA ar = NODE_area(&mandelbrot->tree_right, mandelbrot->bounds_right);
  struct AREA a = AREA_add(al, ar);
  a.area[0] *= 2;
  a.area[1] *= 2;
  a.area[2] *= 2;
  printf("%.18f %.18f %.18f\n", a.area[0], a.area[1], a.area[2]);
}

struct ITEM
{
  struct NODE *tree;
  struct C c;
};

struct QUEUE
{
  struct ITEM *items;
  int index;
};

static
void QUEUE_run(struct QUEUE *queue, int levels, volatile bool *running)
{
  int progress = 0;
  #pragma omp parallel for schedule(dynamic, 1)
  for (int i = 0; i < queue->index; ++i)
  if (*running)
  {
    // progress reporting
    int me;
    {
      #pragma omp atomic capture
      me = progress++;
    }
    int old_percent = (me - 1) * 100 / queue->index;
    int percent = me * 100 / queue->index;
    if (old_percent < percent)
    {
      #pragma omp critical
      fprintf(stderr, "\t%d%%\r", percent);
    }
    // julia classification
    struct JULIA *julia = JULIA_alloc(queue->items[i].c);
    enum COLOUR k = GREY;
    for (int level = 0; *running && k == GREY && level < levels; ++level)
    {
      k = JULIA_refine(julia);
    }
    JULIA_free(julia);
    queue->items[i].tree->u.leaf.colour = k;
    queue->items[i].tree->u.leaf.attempted = *running;
  }
}

static
void MANDELBROT_degrey_(struct QUEUE *queue, struct NODE *tree, int levels, struct C c)
{
  switch (tree->tag)
  {
    case LEAF:
    {
      if (tree->u.leaf.colour == GREY && ! tree->u.leaf.attempted)
      {
        struct ITEM *item = &queue->items[queue->index++];
        item->tree = tree;
        item->c = c;
      }
      break;
    }
    case BRANCH:
    {
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          MANDELBROT_degrey_(queue, tree->u.branch.child[y][x], levels, C_split(c, y, x));
        }
      }
      break;
    }
  }
}

static
void MANDELBROT_degrey(struct MANDELBROT *mandelbrot, int levels, volatile bool *running)
{
  struct COUNT count = { { 0, 0, 0, 0 } };
  NODE_count(&count, &mandelbrot->tree_left);
  NODE_count(&count, &mandelbrot->tree_right);
  struct QUEUE queue;
  queue.items = calloc(1, sizeof(*queue.items) * count.count[GREY]);
  queue.index = 0;
  MANDELBROT_degrey_(&queue, &mandelbrot->tree_left, levels, mandelbrot->bounds_left);
  MANDELBROT_degrey_(&queue, &mandelbrot->tree_right, levels, mandelbrot->bounds_right);
  QUEUE_run(&queue, levels, running);
  free(queue.items);
}

void MANDELBROT_refine(struct MANDELBROT *mandelbrot, int levels, volatile bool *running)
{
  if (mandelbrot->complete)
  {
    NODE_grow(&mandelbrot->node_pool, &mandelbrot->tree_left);
    NODE_grow(&mandelbrot->node_pool, &mandelbrot->tree_right);
    mandelbrot->complete = false;
  }
  MANDELBROT_degrey(mandelbrot, levels, running);
  if (*running)
  {
    NODE_prune(&mandelbrot->tree_left);
    NODE_prune(&mandelbrot->tree_right);
    mandelbrot->complete = true;
  }
}

void MANDELBROT_svg(FILE *out, struct MANDELBROT *mandelbrot, int level)
{
  struct C bounds;
  bounds.x.lo = 0;
  bounds.x.hi = 8 << level;
  bounds.y.lo = 0;
  bounds.y.hi = 8 << level;
  int w = bounds.x.hi - bounds.x.lo;
  int h = bounds.y.hi - bounds.y.lo;
  double s = 1024.0 / w;
  fprintf(out, "<svg width='1024' height='1024'><defs><style>*{stroke:grey;}.g{fill:red;}.a{fill:orange;}.b{fill:black;}.w{fill:white;}</style><g id='m'>");
  NODE_svg(out, &mandelbrot->tree_left, C_split(bounds, 0, 0));
  NODE_svg(out, &mandelbrot->tree_right, C_split(bounds, 0, 1));
  fprintf(out, "</g></defs><g transform='scale(%.18f,%.18f)'><use href='#m' /><use transform='translate(0,%d) scale(1,-1)' href='#m' /></g></svg>\n", s, s, h);
}

static
const char *header = "Trustworthy Mandelbrot z->z^2+c\n\f";

void MANDELBROT_serialize(FILE *out, struct MANDELBROT *mandelbrot)
{
  struct COUNT count = { { 0, 0, 0, 0 } };
  NODE_count(&count, &mandelbrot->tree_left);
  NODE_count(&count, &mandelbrot->tree_right);
  int bits = (2 + ! mandelbrot->complete) * count.count[0] + 2 * (count.count[1] + count.count[2] + count.count[3]);
  int bytes = (bits + 7) / 8;
  struct BITSTREAM bs;
  bs.data = calloc(1, bytes);
  bs.byte = 0;
  bs.bit = 0;
  NODE_serialize(&bs, &mandelbrot->tree_left, ! mandelbrot->complete);
  NODE_serialize(&bs, &mandelbrot->tree_right, ! mandelbrot->complete);
  fprintf(out, "%s", header);
  unsigned char version = 0;
  fwrite(&version, sizeof(version), 1, out);
  uint32_t endian = 0x12345678;
  fwrite(&endian, sizeof(endian), 1, out);
  fwrite(&mandelbrot->bounds_left, sizeof(mandelbrot->bounds_left), 1, out);
  fwrite(&mandelbrot->bounds_right, sizeof(mandelbrot->bounds_right), 1, out);
  unsigned char with_attempted = ! mandelbrot->complete;
  fwrite(&with_attempted, sizeof(with_attempted), 1, out);
  fwrite(bs.data, bytes, 1, out);
  free(bs.data);
}

struct MANDELBROT *MANDELBROT_deserialize(struct BITSTREAM *bs)
{
  if (0 != BITSTREAM_match(bs, header))
  {
    return NULL;
  }
  unsigned char version;
  BITSTREAM_read(bs, &version, sizeof(version));
  if (version != 0)
  {
    return NULL;
  }
  uint32_t endian;
  BITSTREAM_read(bs, &endian, sizeof(endian));
  if (endian != 0x12345678)
  {
    return NULL;
  }
  struct MANDELBROT *mandelbrot = MANDELBROT_alloc();
  BITSTREAM_read(bs, &mandelbrot->bounds_left, sizeof(mandelbrot->bounds_left));
  BITSTREAM_read(bs, &mandelbrot->bounds_right, sizeof(mandelbrot->bounds_right));
  unsigned char with_attempted;
  BITSTREAM_read(bs, &with_attempted, sizeof(with_attempted));
  mandelbrot->complete = ! with_attempted;
  mandelbrot->tree_left = *NODE_deserialize(&mandelbrot->node_pool, bs, with_attempted);
  mandelbrot->tree_right = *NODE_deserialize(&mandelbrot->node_pool, bs, with_attempted);
  return mandelbrot;
}

struct MANDELBROT *MANDELBROT_load(const char *filename)
{
  FILE *in = fopen(filename, "rb");
  if (in)
  {
    struct BITSTREAM bs;
    bs.byte = 0;
    bs.bit = 0;
    fseek(in, 0, SEEK_END);
    long int bytes = ftell(in);
    fseek(in, 0, SEEK_SET);
    bs.data = calloc(1, bytes);
    fread(bs.data, bytes, 1, in);
    fclose(in);
    struct MANDELBROT *mandelbrot = MANDELBROT_deserialize(&bs);
    free(bs.data);
    return mandelbrot;
  }
  return NULL;
}
