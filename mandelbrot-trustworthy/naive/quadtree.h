#pragma once

#include <stdbool.h>
#include <stdio.h>

#include "bitstream.h"
#include "C.h"

enum COLOUR { GREY = 0, BLACK = 1, WHITE = 2 };

enum TAG { LEAF = 0, BRANCH = 1 };

struct NODE;
struct LEAF;
struct BRANCH;
struct EDGE;

struct EDGE
{
  struct NODE *source, *sink;
  struct EDGE *forward, *backward;
  bool visited;
};

struct LEAF
{
  struct EDGE *forward, *backward;
  enum COLOUR colour;
  bool visited;
  bool attempted;
};

struct BRANCH
{
  struct NODE *child[2][2];
};

struct NODE
{
  enum TAG tag;
  union
  {
    struct LEAF leaf;
    struct BRANCH branch;
  } u;
};

struct NODE_POOL
{
  struct NODE_POOL *next;
  struct NODE *nodes;
  int count;
  int index;
};

struct NODE *NODE_POOL_alloc(struct NODE_POOL **poolp);
void NODE_POOL_free(struct NODE_POOL **poolp);

struct EDGE_POOL
{
  struct EDGE_POOL *next;
  struct EDGE *edges;
  int count;
  int index;
};

struct EDGE *EDGE_POOL_alloc(struct EDGE_POOL **poolp);
void EDGE_POOL_free(struct EDGE_POOL **poolp);

void NODE_grow(struct NODE_POOL **pool, struct NODE *tree);
int NODE_prune(struct NODE *tree);

void EDGE_connect(struct EDGE_POOL **pool, struct NODE *source, struct NODE *sink);

void NODE_clear_visited_flags(struct NODE *tree);
void NODE_whiten_forward(struct NODE *tree);
void NODE_visit_backward(struct NODE *tree);
void NODE_blacken_forward(struct NODE *tree);

struct AREA
{
  double area[3];
};

static inline
struct AREA AREA_add(struct AREA a, struct AREA b)
{
  return (struct AREA){ { a.area[0] + b.area[0], a.area[1] + b.area[1], a.area[2] + b.area[2] } };
}

struct AREA NODE_area(struct NODE *tree, struct C z);

struct COUNT
{
  int count[4];
};

void NODE_count(struct COUNT *count, struct NODE *tree);
void NODE_svg(FILE *out, struct NODE *tree, struct C z);
void NODE_serialize(struct BITSTREAM *out, struct NODE *tree, bool with_attempted);
struct NODE *NODE_deserialize(struct NODE_POOL **pool, struct BITSTREAM *bs, bool with_attempted);
