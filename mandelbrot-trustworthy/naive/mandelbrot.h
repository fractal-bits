#pragma once

#include "quadtree.h"

struct MANDELBROT;

struct MANDELBROT *MANDELBROT_alloc(void);
void MANDELBROT_free(struct MANDELBROT *mandelbrot);
void MANDELBROT_print(struct MANDELBROT *mandelbrot);
void MANDELBROT_refine(struct MANDELBROT *mandelbrot, int level, volatile bool *running);
void MANDELBROT_svg(FILE *out, struct MANDELBROT *mandelbrot, int level);
void MANDELBROT_serialize(FILE *out, struct MANDELBROT *mandelbrot);
struct MANDELBROT *MANDELBROT_deserialize(struct BITSTREAM *bs);
struct MANDELBROT *MANDELBROT_load(const char *filename);
