#pragma once

static inline
double fmax(double a, double b)
{
  return a > b ? a : b;
}

static inline
double fmin(double a, double b)
{
  return a < b ? a : b;
}
