#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "julia.h"
#include "function.h"

struct JULIA
{
  struct C c;
  struct C bounds;
  struct C zero;
  struct NODE exterior, tree;
  struct NODE_POOL *node_pool;
  struct EDGE_POOL *edge_pool;
};

#define SIZE 4

struct JULIA *JULIA_alloc(struct C c)
{
  struct JULIA *julia = calloc(1, sizeof(*julia));
  julia->c = c;
  julia->bounds.x.lo = -SIZE;
  julia->bounds.x.hi = SIZE;
  julia->bounds.y.lo = -SIZE;
  julia->bounds.y.hi = SIZE;
  julia->exterior.u.leaf.colour = WHITE;
  return julia;
}

void JULIA_free(struct JULIA *julia)
{
  NODE_POOL_free(&julia->node_pool);
  EDGE_POOL_free(&julia->edge_pool);
  free(julia);
}

static
void JULIA_edges_add_to_(struct JULIA *julia, struct NODE *source, struct C z1, struct NODE *sink, struct C z0)
{
  if (! C_outside(z0, z1))
  {
    switch (sink->tag)
    {
      case LEAF:
      {
        EDGE_connect(&julia->edge_pool, source, sink);
        break;
      }
      case BRANCH:
      {
        for (int y = 0; y < 2; ++y)
        {
          for (int x = 0; x < 2; ++x)
          {
            JULIA_edges_add_to_(julia, source, z1, sink->u.branch.child[y][x], C_split(z0, y, x));
          }
        }
        break;
      }
    }
  }
}

static
void JULIA_edges_add_to(struct JULIA *julia, struct NODE *source, struct C z1)
{
  if (! (C_inside_or_equal(z1, julia->bounds)))
  {
    EDGE_connect(&julia->edge_pool, source, &julia->exterior);
  }
  JULIA_edges_add_to_(julia, source, z1, &julia->tree, julia->bounds);
}

static
void JULIA_edges_add_from(struct JULIA *julia, struct NODE *source, struct C z0)
{
  switch (source->tag)
  {
    case LEAF:
    {
      JULIA_edges_add_to(julia, source, function(z0, julia->c));
      break;
    }
    case BRANCH:
    {
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          JULIA_edges_add_from(julia, source->u.branch.child[y][x], C_split(z0, y, x));
        }
      }
      break;
    }
  }
}

static
void JULIA_edges_add(struct JULIA *julia)
{
  JULIA_edges_add_from(julia, &julia->tree, julia->bounds);
}

static
void JULIA_edges_delete_(struct JULIA *julia, struct NODE *tree)
{
  switch (tree->tag)
  {
    case LEAF:
    {
#if 0
      while (tree->u.leaf.forward)
      {
        struct EDGE *next = tree->u.leaf.forward->forward;
        free_edge(tree->u.leaf.forward);
        tree->u.leaf.forward = next;
      }
#endif
      tree->u.leaf.forward = NULL;
      tree->u.leaf.backward = NULL;
      break;
    }
    case BRANCH:
    {
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          JULIA_edges_delete_(julia, tree->u.branch.child[y][x]);
        }
      }
      break;
    }
  }
}

static
void JULIA_edges_delete(struct JULIA *julia)
{
  JULIA_edges_delete_(julia, &julia->tree);
  julia->exterior.u.leaf.backward = NULL;
  EDGE_POOL_free(&julia->edge_pool);
}

static
void JULIA_whiten(struct JULIA *julia)
{
  NODE_clear_visited_flags(&julia->exterior);
  NODE_clear_visited_flags(&julia->tree);
  NODE_visit_backward(&julia->exterior);
  NODE_whiten_forward(&julia->tree);
}

static
void JULIA_blacken(struct JULIA *julia)
{
  NODE_clear_visited_flags(&julia->exterior);
  NODE_clear_visited_flags(&julia->tree);
  NODE_visit_backward(&julia->exterior);
  NODE_blacken_forward(&julia->tree);
}

static
enum COLOUR JULIA_classify_(struct JULIA *julia, struct NODE *tree, struct C z)
{
  if (C_outside(z, julia->zero))
  {
    return GREY;
  }
  switch (tree->tag)
  {
    case LEAF:
    {
      if (C_inside_or_equal(julia->zero, z))
      {
        return tree->u.leaf.colour;
      }
      else
      {
        return GREY;
      }
      break;
    }
    case BRANCH:
    {
      int count[3] = { 0, 0, 0 };
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          count[JULIA_classify_(julia, tree->u.branch.child[y][x], C_split(z, y, x))]++;
        }
      }
      if (count[BLACK] && ! count[WHITE])
      {
        return BLACK;
      }
      else if (count[WHITE] && ! count[BLACK])
      {
        return WHITE;
      }
      else
      {
        return GREY;
      }
    }
  }
  return GREY;
}

enum COLOUR JULIA_classify(struct JULIA *julia)
{
  return JULIA_classify_(julia, &julia->tree, julia->bounds);
}

void JULIA_print(struct JULIA *julia)
{
  static const char *name[3] = { "unknown", "connected", "disconnected" };
  struct AREA a = NODE_area(&julia->tree, julia->bounds);
  printf("%.18f %.18f %.18f %s\n", a.area[0], a.area[1], a.area[2], name[JULIA_classify(julia)]);
}


enum COLOUR JULIA_refine(struct JULIA *julia)
{
  NODE_grow(&julia->node_pool, &julia->tree);
  JULIA_edges_add(julia);
  JULIA_whiten(julia);
  JULIA_blacken(julia);
  JULIA_edges_delete(julia);
  NODE_prune(&julia->tree);
  return JULIA_classify(julia);
}

void JULIA_svg(FILE *out, struct JULIA *julia, int level)
{
  struct C bounds;
  bounds.x.lo = 0;
  bounds.x.hi = 8 << level;
  bounds.y.lo = 0;
  bounds.y.hi = 8 << level;
  int w = bounds.x.hi - bounds.x.lo;
  int h = bounds.y.hi - bounds.y.lo;
  double s = 1024.0 / w;
  double t = 1024.0 / h;
  fprintf(out, "<svg width='1024' height='1024'><defs><style>*{stroke:grey;}.g{fill:red;}.b{fill:black;}.w{fill:white;}</style></defs><g id='j' transform='scale(%.18f,%.18f)'>", s, t);
  NODE_svg(out, &julia->tree, bounds);
  fprintf(out, "</g></svg>\n");
}

static
const char *header = "Trustworthy Julia z->z^2+c\n\f";

void JULIA_serialize(FILE *out, struct JULIA *julia)
{
  struct COUNT count = { { 0, 0, 0, 0 } };
  NODE_count(&count, &julia->tree);
  int total = count.count[0] + count.count[1] + count.count[2] + count.count[3];
  int bits = total * 2;
  int bytes = (bits + 7) / 8;
  struct BITSTREAM bs;
  bs.data = calloc(1, bytes);
  bs.byte = 0;
  bs.bit = 0;
  NODE_serialize(&bs, &julia->tree, false);
  fprintf(out, "%s", header);
  unsigned char version = 0;
  fwrite(&version, sizeof(version), 1, out);
  uint32_t endian = 0x12345678;
  fwrite(&endian, sizeof(endian), 1, out);
  fwrite(&julia->c, sizeof(julia->c), 1, out);
  fwrite(&julia->bounds, sizeof(julia->bounds), 1, out);
  fwrite(bs.data, bytes, 1, out);
  free(bs.data);
}

struct JULIA *JULIA_deserialize(struct BITSTREAM *bs)
{
  if (0 != BITSTREAM_match(bs, header))
  {
    return NULL;
  }
  unsigned char version;
  BITSTREAM_read(bs, &version, sizeof(version));
  if (version != 0)
  {
    return NULL;
  }
  uint32_t endian;
  BITSTREAM_read(bs, &endian, sizeof(endian));
  if (endian != 0x12345678)
  {
    return NULL;
  }
  struct C placeholder = { { 0, 0 }, { 0, 0 } };
  struct JULIA *julia = JULIA_alloc(placeholder);
  BITSTREAM_read(bs, &julia->c, sizeof(julia->c));
  BITSTREAM_read(bs, &julia->bounds, sizeof(julia->bounds));
  julia->tree = *NODE_deserialize(&julia->node_pool, bs, false);
  return julia;
}

struct JULIA *JULIA_load(const char *filename)
{
  FILE *in = fopen(filename, "rb");
  if (in)
  {
    struct BITSTREAM bs;
    bs.byte = 0;
    bs.bit = 0;
    fseek(in, 0, SEEK_END);
    long int bytes = ftell(in);
    fseek(in, 0, SEEK_SET);
    bs.data = calloc(1, bytes);
    fread(bs.data, bytes, 1, in);
    fclose(in);
    struct JULIA *julia = JULIA_deserialize(&bs);
    free(bs.data);
    return julia;
  }
  return NULL;
}
