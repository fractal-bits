#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include "mandelbrot.h"

volatile bool running = true;

void interrupt(int sig)
{
  (void) sig;
  running = false;
}

enum MODE { ERROR, HELP, CREATE, REFINE, INFO, SVG };

extern
int main(int argc, char **argv)
{
  signal(SIGINT, interrupt);
  signal(SIGTERM, interrupt);
  enum MODE mode = ERROR;
  if (argc > 1)
  {
    if (0 == strcmp("help", argv[1]))
    {
      mode = HELP;
    }
    else if (0 == strcmp("create", argv[1]))
    {
      if (argc > 2)
      {
        mode = CREATE;
      }
    }
    else if (0 == strcmp("refine", argv[1]))
    {
      if (argc > 4)
      {
        mode = REFINE;
      }
    }
    else if (0 == strcmp("info", argv[1]))
    {
      if (argc > 2)
      {
        mode = INFO;
      }
    }
    else if (0 == strcmp("svg", argv[1]))
    {
      if (argc > 4)
      {
        mode = SVG;
      }
    }
  }
  switch (mode)
  {
    case ERROR:
    case HELP:
    {
      fprintf
        ( mode == ERROR ? stderr : stdout
        , "usage:\n"
          "\t%s help\n"
          "\t%s create out.M\n"
          "\t%s refine level in.M out.M\n"
          "\t%s info in.M\n"
          "\t%s svg level in.M out.svg\n"
          , argv[0], argv[0], argv[0], argv[0], argv[0]
        );
      return mode == ERROR;
    }
    case CREATE:
    {
      FILE *out = fopen(argv[2], "wb");
      if (out)
      {
        struct MANDELBROT *mandelbrot = MANDELBROT_alloc();
        MANDELBROT_serialize(out, mandelbrot);
        MANDELBROT_free(mandelbrot);
        fclose(out);
        return 0;
      }
      else
      {
        return 1;
      }
    }
    case REFINE:
    {
      struct MANDELBROT *mandelbrot = MANDELBROT_load(argv[3]);
      if (! mandelbrot)
      {
        return 1;
      }
      MANDELBROT_refine(mandelbrot, atoi(argv[2]), &running);
      FILE *out = fopen(argv[4], "wb");
      if (out)
      {
        MANDELBROT_serialize(out, mandelbrot);
        fclose(out);
        MANDELBROT_free(mandelbrot);
        return 0;
      }
      else
      {
        MANDELBROT_free(mandelbrot);
        return 1;
      }
    }
    case INFO:
    {
      struct MANDELBROT *mandelbrot = MANDELBROT_load(argv[2]);
      if (! mandelbrot)
      {
        return 1;
      }
      MANDELBROT_print(mandelbrot);
      return 0;
    }
    case SVG:
    {
      struct MANDELBROT *mandelbrot = MANDELBROT_load(argv[3]);
      if (! mandelbrot)
      {
        return 1;
      }
      FILE *out = fopen(argv[4], "wb");
      if (out)
      {
        MANDELBROT_svg(out, mandelbrot, atoi(argv[2]));
        fclose(out);
      }
      MANDELBROT_free(mandelbrot);
      return ! out;
    }
  }
  return 0;
}
