#pragma once

#include "quadtree.h"

struct JULIA;

struct JULIA *JULIA_alloc(struct C c);
void JULIA_free(struct JULIA *julia);
enum COLOUR JULIA_refine(struct JULIA *julia);
enum COLOUR JULIA_classify(struct JULIA *julia);
void JULIA_print(struct JULIA *julia);
void JULIA_svg(FILE *out, struct JULIA *julia, int level);
void JULIA_serialize(FILE *out, struct JULIA *julia);
struct JULIA *JULIA_deserialize(struct BITSTREAM *bs);
struct JULIA *JULIA_load(const char *filename);
