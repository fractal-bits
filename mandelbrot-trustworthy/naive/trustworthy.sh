#!/bin/sh
set -e
M="$(readlink -e "${0}")"
M="${M%.sh}"
Jmul="${1}"
Jadd="${2}"
if [ "x${Jmul}" = "x" ]
then
  Jmul=1
fi
if [ "x${Jadd}" = "x" ]
then
  Jadd=4
fi
[ ! -e 0.M ] && "${M}" create 0.M
for level in $(seq 0 24)
do
  in="${level}.M"
  svg="${level}.svg"
  png="${level}.png"
  out="$((level + 1)).M"
  jlevel="$((Jmul * level + Jadd))"
  "${M}" info "${in}"
  [ ! "${svg}" -nt "${in}" ] && "${M}" svg "${level}" "${in}" "${svg}"
  [ ! "${png}" -nt "${svg}" ] && rsvg-convert "${svg}" -o "${png}"
  [ ! "${out}" -nt "${in}" ] && time "${M}" refine "${jlevel}" "${in}" "${out}"
done
