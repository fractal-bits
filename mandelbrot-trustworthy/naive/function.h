#pragma once

#include "C.h"

#ifndef POWER
#define POWER 2
#endif

#define LEVEL_MAX ((int)(53 - 4)/POWER)

#if POWER == 2
static inline
struct C function(struct C z, struct C c)
{
  return C_add(C_sqr(z), c);
}
#endif
