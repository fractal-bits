#pragma once

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "N.h"
#include "math.h"
#include "memory.h"
#include "sort.h"

struct EDGE8
{
  uint8_t source;
  uint8_t sink;
};

struct EDGE16
{
  uint16_t source;
  uint16_t sink;
};

struct EDGE24
{
  uint24_t source;
  uint24_t sink;
};

struct EDGE32
{
  uint32_t source;
  uint32_t sink;
};

enum EDGE_BITS
{
  EDGE_BITS_8  = 1,
  EDGE_BITS_16 = 2,
  EDGE_BITS_24 = 3,
  EDGE_BITS_32 = 4
};

typedef int (*cmp_t)(const void *, const void *);
extern const cmp_t EDGE_cmp_sink[5];
extern const cmp_t EDGE_cmp_source[5];

typedef uint32_t (*radix_t)(const void *);
extern const radix_t EDGE_key_sink[5];
extern const radix_t EDGE_key_source[5];

typedef int64_t graph_edge_t;
typedef uint64_t graph_node_t;

struct GRAPH
{
  graph_node_t node_max;
  graph_edge_t edge_count; // allocated
  graph_edge_t edge_ix; // used
  enum EDGE_BITS edge_bits;
  graph_edge_t stack_limit;
  graph_edge_t stack_ix;
  unsigned char *node_visited; // [(node_max + 8 / 8)]
  union // [edge_count]
  {
    void *edge;
    struct EDGE8  *edge8;
    struct EDGE16 *edge16;
    struct EDGE24 *edge24;
    struct EDGE32 *edge32;
  } reverse, forward;
  union // [edge_count]
  {
    void *stack;
    uint8_t *stack8;
    uint16_t *stack16;
    uint24_t *stack24;
    uint32_t *stack32;
  } stack;
};

static inline
int GRAPH_bits_required(graph_node_t node_max)
{
  return ceil(log2(((double) node_max) + 1));
}

static inline
enum EDGE_BITS EDGE_BITS_for_bits(int bits)
{
  if (bits <= 8)
  {
    return EDGE_BITS_8;
  }
  else if (bits <= 16)
  {
    return EDGE_BITS_16;
  }
#ifdef GRAPH_USE_UINT24
  else if (bits <= 24)
  {
    return EDGE_BITS_24;
  }
#endif
  else if (bits <= 32)
  {
    return EDGE_BITS_32;
  }
  else
  {
    return -1;
  }
}
static inline
uint64_t GRAPH_memory_required(graph_node_t node_max, graph_edge_t edge_count)
{
  int edge_bits = EDGE_BITS_for_bits(GRAPH_bits_required(node_max));
  return sizeof(struct GRAPH) + (2 * 2 + 1) * edge_bits * edge_count + (node_max + 8) / 8;
}

static inline
graph_edge_t GRAPH_maximum_edge_count(graph_node_t node_max, int64_t memory_available)
{
  int edge_bits = EDGE_BITS_for_bits(GRAPH_bits_required(node_max));
  return (memory_available - sizeof(struct GRAPH) - (node_max + 8) / 8) / ((2 * 2 + 1) * edge_bits);
}

static inline
struct GRAPH *GRAPH_new_for_edge_count(graph_node_t node_max, graph_edge_t edge_count)
{
  int bits = GRAPH_bits_required(node_max);
  enum EDGE_BITS edge_bits = EDGE_BITS_for_bits(bits);
  if (! (1 <= edge_bits && edge_bits <= 4))
  {
    return NULL;
  }
  struct GRAPH *graph = MEMORY_alloc(sizeof(*graph));
  if (! graph)
  {
    return NULL;
  }
  graph->node_max = node_max;
  graph->edge_count = edge_count;
  graph->edge_ix = 0;
  graph->edge_bits = edge_bits;
  int64_t bytes = 2 * edge_bits * edge_count;
  graph->reverse.edge = MEMORY_alloc(bytes);
  if (! graph->reverse.edge)
  {
    MEMORY_free(graph, sizeof(*graph));
    return NULL;
  }
  graph->forward.edge = MEMORY_alloc(bytes);
  if (! graph->forward.edge)
  {
    MEMORY_free(graph->reverse.edge, bytes);
    MEMORY_free(graph, sizeof(*graph));
    return NULL;
  }
  graph->node_visited = MEMORY_alloc((node_max + 8) / 8);
  if (! graph->node_visited)
  {
    MEMORY_free(graph->forward.edge, bytes);
    MEMORY_free(graph->reverse.edge, bytes);
    MEMORY_free(graph, sizeof(*graph));
    return NULL;
  }
  graph->stack_limit = graph->edge_count;
  graph->stack.stack = MEMORY_alloc(graph->edge_bits * graph->stack_limit);
  if (! graph->stack.stack)
  {
    MEMORY_free(graph->node_visited, (node_max + 8) / 8);
    MEMORY_free(graph->forward.edge, bytes);
    MEMORY_free(graph->reverse.edge, bytes);
    MEMORY_free(graph, sizeof(*graph));
    return NULL;
  }
  graph->stack_ix = 0;
  return graph;
}

static inline
struct GRAPH *GRAPH_new_for_memory(graph_node_t node_max, int64_t memory_available)
{
  return GRAPH_new_for_edge_count(node_max, GRAPH_maximum_edge_count(node_max, memory_available));
}

static inline
struct GRAPH *GRAPH_new(graph_node_t node_max)
{
  return GRAPH_new_for_memory(node_max, MEMORY_available() - 1024);
}

static inline
void GRAPH_delete(struct GRAPH * restrict graph)
{
  assert(graph);
  assert(graph->stack.stack);
  assert(graph->node_visited);
  assert(graph->forward.edge);
  assert(graph->reverse.edge);
  int64_t bytes = 2 * graph->edge_bits * graph->edge_count;
  MEMORY_free(graph->stack.stack, graph->edge_bits * graph->stack_limit);
  MEMORY_free(graph->node_visited, (graph->node_max + 8) / 8);
  MEMORY_free(graph->forward.edge, bytes);
  MEMORY_free(graph->reverse.edge, bytes);
  MEMORY_free(graph, sizeof(*graph));
}

static inline
graph_node_t GRAPH_preds_pred(struct GRAPH * restrict graph, graph_edge_t e)
{
  assert(0 <= e);
  assert(e < graph->edge_ix);
  switch (graph->edge_bits)
  {
    case EDGE_BITS_8:  return graph->reverse.edge8 [e].source;
    case EDGE_BITS_16: return graph->reverse.edge16[e].source;
    case EDGE_BITS_24: return uint64_from_uint24(graph->reverse.edge24[e].source);
    case EDGE_BITS_32: return graph->reverse.edge32[e].source;
    default: assert(! "valid edge bits");
  }
  abort();
}

static inline
graph_node_t GRAPH_preds_succ(struct GRAPH * restrict graph, graph_edge_t e)
{
  assert(0 <= e);
  assert(e < graph->edge_ix);
  switch (graph->edge_bits)
  {
    case EDGE_BITS_8:  return graph->reverse.edge8 [e].sink;
    case EDGE_BITS_16: return graph->reverse.edge16[e].sink;
    case EDGE_BITS_24: return uint64_from_uint24(graph->reverse.edge24[e].sink);
    case EDGE_BITS_32: return graph->reverse.edge32[e].sink;
    default: assert(! "valid edge bits");
  }
  abort();
}

static inline
graph_node_t GRAPH_succs_pred(struct GRAPH * restrict graph, graph_edge_t e)
{
  assert(0 <= e);
  assert(e < graph->edge_ix);
  switch (graph->edge_bits)
  {
    case EDGE_BITS_8:  return graph->forward.edge8 [e].source;
    case EDGE_BITS_16: return graph->forward.edge16[e].source;
    case EDGE_BITS_24: return uint64_from_uint24(graph->forward.edge24[e].source);
    case EDGE_BITS_32: return graph->forward.edge32[e].source;
    default: assert(! "valid edge bits");
  }
  abort();
}

static inline
graph_node_t GRAPH_succs_succ(struct GRAPH * restrict graph, graph_edge_t e)
{
  assert(0 <= e);
  assert(e < graph->edge_ix);
  switch (graph->edge_bits)
  {
    case EDGE_BITS_8:  return graph->forward.edge8 [e].sink;
    case EDGE_BITS_16: return graph->forward.edge16[e].sink;
    case EDGE_BITS_24: return uint64_from_uint24(graph->forward.edge24[e].sink);
    case EDGE_BITS_32: return graph->forward.edge32[e].sink;
    default: assert(! "valid edge bits");
  }
  abort();
}

static inline
void GRAPH_sort(struct GRAPH * restrict graph)
{
  assert(graph);
  assert(graph->reverse.edge);
  assert(graph->forward.edge);
  assert(graph->edge_ix <= graph->edge_count);
  memcpy(graph->forward.edge, graph->reverse.edge, 2 * graph->edge_bits * graph->edge_ix);
#if 0
  bsort(graph->reverse.edge, graph->edge_ix, 2 * graph->edge_bits, EDGE_cmp_sink[graph->edge_bits]);
  bsort(graph->forward.edge, graph->edge_ix, 2 * graph->edge_bits, EDGE_cmp_source[graph->edge_bits]);
#endif
#if 1
  rsort(graph->reverse.edge, graph->edge_ix, 2 * graph->edge_bits, EDGE_key_sink[graph->edge_bits], graph->node_max + 1);
  rsort(graph->forward.edge, graph->edge_ix, 2 * graph->edge_bits, EDGE_key_source[graph->edge_bits], graph->node_max + 1);
#endif

#if 1
  int64_t max_succs_pred = 0;
  graph_edge_t lo = 0, hi;
  while (lo < graph->edge_ix)
  {
    graph_node_t target = GRAPH_succs_pred(graph, lo);
    hi = lo + 1;
    while (hi < graph->edge_ix && GRAPH_succs_pred(graph, hi) == target)
    {
      ++hi;
    }
    int64_t range = hi - lo;
    max_succs_pred = max_succs_pred > range ? max_succs_pred : range;
    lo = hi;
  }
  static _Thread_local int64_t maximum = 100;
  if (max_succs_pred > maximum)
  {
    maximum = max_succs_pred;
    fprintf(stderr, "e:%ld\n", maximum);
  }
#endif
}

static inline
bool GRAPH_add(struct GRAPH * restrict graph, graph_node_t source, graph_node_t sink)
{
  assert(graph);
  assert(graph->reverse.edge);
  if (! (graph->edge_ix < graph->edge_count))
  {
    return true; // full
  }
  switch (graph->edge_bits)
  {
    case EDGE_BITS_8:
      graph->reverse.edge8[graph->edge_ix] = (struct EDGE8){ source, sink };
      break;
    case EDGE_BITS_16:
      graph->reverse.edge16[graph->edge_ix] = (struct EDGE16){ source, sink };
      break;
    case EDGE_BITS_24:
      graph->reverse.edge24[graph->edge_ix] = (struct EDGE24){ uint24_from_uint64(source), uint24_from_uint64(sink) };
      break;
    case EDGE_BITS_32:
      graph->reverse.edge32[graph->edge_ix] = (struct EDGE32){ source, sink };
      break;
    default:
      assert(! "valid EDGE_BITS");
      break;
  }
  ++graph->edge_ix;
  return false;
}

static inline
void GRAPH_preds(struct GRAPH * restrict graph, graph_node_t target, graph_edge_t * restrict begin, graph_edge_t * restrict end)
{
  graph_edge_t edge_ix = graph->edge_ix;
  graph_edge_t lo = 0;
  graph_edge_t hi = edge_ix;
  if (lo >= hi)
  {
    *begin = edge_ix;
    *end = edge_ix;
    return;
  }
  if (GRAPH_preds_succ(graph, lo) == target)
  {
    for (hi = lo; hi < edge_ix && GRAPH_preds_succ(graph, hi) == target; ++hi)
      ;
    *begin = lo;
    *end = hi;
    return;
  }
  while (lo + 1 < hi)
  {
    graph_edge_t md = (lo + hi) >> 1;
    graph_node_t candidate = GRAPH_preds_succ(graph, md);
    if (candidate < target)
    {
      lo = md;
    }
    else if (target < candidate)
    {
      hi = md;
    }
    else
    {
      break;
    }
  }
  graph_edge_t md = (lo + hi) >> 1;
  assert(GRAPH_preds_succ(graph, md) == target);
  for (lo = md; 0 <= lo && GRAPH_preds_succ(graph, lo) == target; --lo)
    ;
  ++lo;
  for (hi = md; hi < edge_ix && GRAPH_preds_succ(graph, hi) == target; ++hi)
    ;
  *begin = lo;
  *end = hi;
}

static inline
void GRAPH_succs(struct GRAPH * restrict graph, graph_node_t target, graph_edge_t * restrict begin, graph_edge_t * restrict end)
{
  graph_edge_t edge_ix = graph->edge_ix;
  graph_edge_t lo = 0;
  graph_edge_t hi = edge_ix;
  if (lo >= hi)
  {
    *begin = edge_ix;
    *end = edge_ix;
    return;
  }
  if (GRAPH_succs_pred(graph, lo) == target)
  {
    for (hi = lo; hi < edge_ix && GRAPH_succs_pred(graph, hi) == target; ++hi)
      ;
    *begin = lo;
    *end = hi;
    return;
  }
  while (lo + 1 < hi)
  {
    graph_edge_t md = (lo + hi) >> 1;
    graph_node_t candidate = GRAPH_succs_pred(graph, md);
    if (candidate < target)
    {
      lo = md;
    }
    else if (target < candidate)
    {
      hi = md;
    }
    else
    {
      break;
    }
  }
  graph_edge_t md = (lo + hi) >> 1;
  assert(GRAPH_succs_pred(graph, md) == target);
  for (lo = md; 0 <= lo && GRAPH_succs_pred(graph, lo) == target; --lo)
    ;
  ++lo;
  for (hi = md; hi < edge_ix && GRAPH_succs_pred(graph, hi) == target; ++hi)
    ;
  *begin = lo;
  *end = hi;
}

static inline
void GRAPH_visit_clear(struct GRAPH * restrict graph)
{
  memset(graph->node_visited, 0, (graph->node_max + 8) / 8);
}

static inline
bool GRAPH_visited(struct GRAPH * restrict graph, graph_node_t sink)
{
  uint64_t byte = sink >> 3;
  int bit = sink & 7;
  unsigned char mask = 1 << bit;
  return !!(graph->node_visited[byte] & mask);
}

void GRAPH_visit_pred(struct GRAPH * restrict graph, graph_node_t sink);
