#pragma once

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include "memory.h"

static inline
unsigned char *BITS_alloc(int64_t count)
{
  return MEMORY_alloc((count + 7) / 8);
}

static inline
void BITS_free(unsigned char * restrict data, int64_t count)
{
  assert(data);
  MEMORY_free(data, (count + 7) / 8);
}

static inline
void BITS_write_1(unsigned char * restrict data, int64_t cursor, int b)
{
  assert(data);
  assert(b == (b & 1));
  int64_t byte = cursor >> 3;
  int bit = cursor & 7;
  unsigned char mask = 1 << bit;
  if (b)
  {
    data[byte] |= mask;
  }
  else
  {
    data[byte] &= ~mask;
  }
}

static inline
void BITS_write_2(unsigned char * restrict data, int64_t cursor, int b)
{
  assert(data);
  assert((cursor & 1) == 0);
  assert(b == (b & 3));
  int64_t byte = cursor >> 3;
  int bit = cursor & 7;
  unsigned char mask = 3 << bit;
  unsigned char set = b << bit;
  unsigned char *valuep = &data[byte];
  unsigned char value = *valuep;
  value &= ~mask;
  value |= set;
  *valuep = value;
}

static inline
void BITS_write_4(unsigned char * restrict data, int64_t cursor, int b)
{
  assert(data);
  assert((cursor & 3) == 0);
  assert(b == (b & 15));
  int64_t byte = cursor >> 3;
  int bit = cursor & 7;
  unsigned char mask = 15 << bit;
  unsigned char set = b << bit;
  unsigned char *valuep = &data[byte];
  unsigned char value = *valuep;
  value &= ~mask;
  value |= set;
  *valuep = value;
}

static inline
void BITS_write_8(unsigned char * restrict data, int64_t cursor, int b)
{
  assert(data);
  assert((cursor & 7) == 0);
  assert(b == (b & 255));
  int64_t byte = cursor >> 3;
  data[byte] = b;
}

static inline
void BITS_write(unsigned char * restrict data, int64_t cursor, int n, uint64_t bits)
{
  assert(data);
  assert(0 <= n);
  assert(n <= 64);
#ifndef NDEBUG
  int64_t cursor_at_end = cursor + n;
#endif
  if (n >= 1 && (cursor & 1))
  {
    BITS_write_1(data, cursor, bits & 1);
    cursor += 1;
    n -= 1;
    bits >>= 1;
  }
  if ((n >= 2) && (cursor & 2))
  {
    BITS_write_2(data, cursor, bits & 3);
    cursor += 2;
    n -= 2;
    bits >>= 2;
  }
  if ((n >= 4) && (cursor & 4))
  {
    BITS_write_4(data, cursor, bits & 15);
    cursor += 4;
    n -= 4;
    bits >>= 4;
  }
  while (n >= 8)
  {
    BITS_write_8(data, cursor, bits & 255);
    cursor += 8;
    n -= 8;
    bits >>= 8;
  }
  if (n >= 4)
  {
    BITS_write_4(data, cursor, bits & 15);
    cursor += 4;
    n -= 4;
    bits >>= 4;
  }
  if (n >= 2)
  {
    BITS_write_2(data, cursor, bits & 3);
    cursor += 2;
    n -= 2;
    bits >>= 2;
  }
  if (n >= 1)
  {
    BITS_write_1(data, cursor, bits & 1);
    cursor += 1;
    n -= 1;
    bits >>= 1;
  }
  assert(cursor_at_end == cursor);
  assert(bits == 0);
}

static inline
int BITS_read_1(const unsigned char * restrict data, int64_t cursor)
{
  assert(data);
  int64_t byte = cursor >> 3;
  int bit = cursor & 7;
  unsigned char mask = 1 << bit;
  return !!(data[byte] & mask);
}

static inline
int BITS_read_2(const unsigned char * restrict data, int64_t cursor)
{
  assert(data);
  assert((cursor & 1) == 0);
  int64_t byte = cursor >> 3;
  int bit = cursor & 7;
  unsigned char mask = 3 << bit;
  return (data[byte] & mask) >> bit;
}

static inline
int BITS_read_4(const unsigned char * restrict data, int64_t cursor)
{
  assert(data);
  assert((cursor & 3) == 0);
  int64_t byte = cursor >> 3;
  int bit = cursor & 7;
  unsigned char mask = 15 << bit;
  return (data[byte] & mask) >> bit;
}

static inline
int BITS_read_8(const unsigned char * restrict data, int64_t cursor)
{
  assert(data);
  assert((cursor & 7) == 0);
  int64_t byte = cursor >> 3;
  return data[byte];
}

static inline
uint64_t BITS_read(const unsigned char * restrict data, int64_t cursor, int n)
{
  assert(data);
  assert(0 <= n);
  assert(n <= 64);
#ifndef NDEBUG
  int64_t cursor_at_end = cursor + n;
#endif
  uint64_t bits = 0;
  int b = 0;
  if ((n >= 1) && (cursor & 1))
  {
    bits |= ((uint64_t) BITS_read_1(data, cursor)) << b;
    cursor += 1;
    n -= 1;
    b += 1;
  }
  if ((n >= 2) && (cursor & 2))
  {
    bits |= ((uint64_t) BITS_read_2(data, cursor)) << b;
    cursor += 2;
    n -= 2;
    b += 2;
  }
  if ((n >= 4) && (cursor & 4))
  {
    bits |= ((uint64_t) BITS_read_4(data, cursor)) << b;
    cursor += 4;
    n -= 4;
    b += 4;
  }
  while (n >= 8)
  {
    bits |= ((uint64_t) BITS_read_8(data, cursor)) << b;
    cursor += 8;
    n -= 8;
    b += 8;
  }
  if (n >= 4)
  {
    bits |= ((uint64_t) BITS_read_4(data, cursor)) << b;
    cursor += 4;
    n -= 4;
    b += 4;
  }
  if (n >= 2)
  {
    bits |= ((uint64_t) BITS_read_2(data, cursor)) << b;
    cursor += 2;
    n -= 2;
    b += 2;
  }
  if (n >= 1)
  {
    bits |= ((uint64_t) BITS_read_1(data, cursor)) << b;
    cursor += 1;
    n -= 1;
    b += 1;
  }
  assert(cursor_at_end == cursor);
  return bits;
}

static inline
int BITS_fwrite_ascii(FILE * restrict out, const unsigned char * restrict data, int64_t count)
{
  assert(out);
  assert(data);
  for (int64_t cursor = 0; cursor < count; ++cursor)
  {
    fputc('0' + BITS_read_1(data, cursor), out);
  }
  return 0; // FIXME
}

static inline
int BITS_fwrite_raw(FILE * restrict out, const unsigned char * restrict data, int64_t count)
{
  assert(out);
  assert(data);
  return 1 != fwrite(data, (count + 7) / 8, 1, out);
}
