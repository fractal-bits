#!/bin/bash
trustworthy="$(dirname "$(readlink -e "$0")")/trustworthy"
GB="${1}"
threads="${2}"
jmul="${3}"
jadd="${4}"
refineonly="${5}"
timer=""
if [ "x${GB}" = "x" ]
then
  echo "usage: $0 memory_limit_in_GB [number_of_threads] [julia_level_multiplier] [julia_level_offset] [refine_only]"
  exit 1
fi
if [ "x${threads}" = "x" ]
then
  threads="$(nproc)"
fi
if [ "x${jmul}" = "x" ]
then
  jmul=1
fi
if [ "x${jadd}" = "x" ]
then
  jadd=4
fi
if [ "x${refineonly}" != "x" ]
then
  refineonly="refineonly"
  timer="time"
fi
src=0
dst=1
[ -e "${src}.M" ] || "${trustworthy}" mandelbrot create "${src}.M"
if [ "${refineonly}" != "refineonly" ]
then
  [ "${src}.svg" -nt "${src}.M" ] || "${trustworthy}" mandelbrot svg "${src}.M" "${src}.svg"
  [ "${src}.ppm" -nt "${src}.M" ] || "${trustworthy}" mandelbrot ppm "${src}.M" "${src}.ppm"
  "${trustworthy}" mandelbrot area "${src}.M"
fi
while (( $jmul * $dst + $jadd <= 24 ))
do
  mkdir -p "${dst}.M.resume"
  [ "${dst}.M" -nt "${src}.M" ] || ${timer} "${trustworthy}" mandelbrot refine "${src}.M" "${dst}.M" "${dst}.M.resume" "$((jmul * dst + jadd))" "${GB}" "${threads}"
  result="$?"
  if [ "${result}" = 0 ]
  then
    echo "${dst}"
    src="${dst}"
    dst="$((src + 1))"
    if [ "${refineonly}" != "refineonly" ]
    then
      if (( "${src}" <= 10 ))
      then
        [ "${src}.svg" -nt "${src}.M" ] || "${trustworthy}" mandelbrot svg "${src}.M" "${src}.svg"
      fi
      [ "${src}.ppm" -nt "${src}.M" ] || "${trustworthy}" mandelbrot ppm "${src}.M" "${src}.ppm"
      "${trustworthy}" mandelbrot area "${src}.M"
    fi
  elif [ "${result}" = 1 ]
  then
    echo error
    exit 1
  elif [ "${result}" = 2 ]
  then
    echo interrupted
    exit 2
  elif [ "${result}" = 3 ]
  then
    echo reducing concurrency
    threads="$((threads / 2))"
    if (( "${threads}" < 1 ))
    then
      echo insufficient memory
      exit 3
    fi
  else
    echo unexpected result "${result}"
    exit "${result}"
  fi
done
echo completed
