#pragma once

#include <stdbool.h>
#include <stdint.h>

bool MEMORY_init(int64_t bytes);
void MEMORY_clear(int64_t bytes);
void *MEMORY_alloc(int64_t bytes);
void MEMORY_free(void * restrict mem, int64_t bytes);
int64_t MEMORY_available(void);

enum MEMORY_DIR
{
  MEMORY_START,
  MEMORY_END
};

void MEMORY_set_dir(enum MEMORY_DIR dir);
