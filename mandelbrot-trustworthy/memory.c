#include <assert.h>
#include <stdlib.h>
#include "memory.h"

_Thread_local unsigned char *memory_base = NULL;
_Thread_local int64_t memory_size = 0;
_Thread_local int64_t memory_ix_start = 0;
_Thread_local int64_t memory_ix_end = 0;
_Thread_local enum MEMORY_DIR memory_dir = MEMORY_START;

const int64_t memory_alignment = 64;

int64_t MEMORY_align(int64_t bytes)
{
  return ((bytes + memory_alignment - 1) / memory_alignment) * memory_alignment;
}

bool MEMORY_init(int64_t bytes)
{
  assert(! memory_base);
  bytes = MEMORY_align(bytes);
  memory_base = aligned_alloc(memory_alignment, bytes);
  if (! memory_base)
  {
    return false;
  }
  memory_size = bytes;
  memory_ix_start = 0;
  memory_ix_end = bytes;
  return true;
}

void MEMORY_clear(int64_t bytes)
{
  (void) bytes;
  assert(memory_base);
  bytes = MEMORY_align(bytes);
  assert(memory_size == bytes);
  free(memory_base);
  memory_base = NULL;
  memory_size = 0;
  memory_ix_start = 0;
  memory_ix_end = 0;
}

void *MEMORY_alloc(int64_t bytes)
{
  assert(memory_base);
  bytes = MEMORY_align(bytes);
  if (memory_base)
  {
    if (memory_ix_start + bytes <= memory_ix_end)
    {
      switch (memory_dir)
      {
        case MEMORY_START:
        {
          void *mem = memory_base + memory_ix_start;
          memory_ix_start += bytes;
          return mem;
        }
        case MEMORY_END:
        {
          memory_ix_end -= bytes;
          void *mem = memory_base + memory_ix_end;
          return mem;
        }
      }
    }
  }
  return NULL;
}

int64_t MEMORY_available(void)
{
  return memory_ix_end - memory_ix_start;
}

void MEMORY_free(void * restrict mem, int64_t bytes)
{
  assert(memory_base);
  bytes = MEMORY_align(bytes);
  switch (memory_dir)
  {
    case MEMORY_START:
      if (mem == (void *) &memory_base[memory_ix_start - bytes])
      {
        memory_ix_start -= bytes;
      }
      else
      {
        assert(! "free nested correctly");
        abort();
      }
      break;
    case MEMORY_END:
      if (mem == (void *) &memory_base[memory_ix_end])
      {
        memory_ix_end += bytes;
      }
      else
      {
        assert(! "free nested correctly");
        abort();
      }
  }
}

void MEMORY_set_dir(enum MEMORY_DIR dir)
{
  memory_dir = dir;
}
