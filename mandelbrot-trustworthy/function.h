#pragma once

#include "C.h"

struct FUNCTION
{
  struct C (*f)(struct FUNCTION * restrict, struct C);
#if 0
  struct C (*f1)(struct FUNCTION * restrict, struct C, int branch);
  int branches;
#endif
};

struct FUNCTION_C
{
  struct FUNCTION f;
  struct C c;
};

struct FUNCTION_C FUNCTION_quadratic(struct C c);
