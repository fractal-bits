#include "function.h"

struct C FUNCTION_quadratic_f(struct FUNCTION * restrict f, struct C z)
{
  struct FUNCTION_C *q = (struct FUNCTION_C *) f;
  return C_add(C_sqr(z), q->c);
}

#if 0
struct C FUNCTION_quadratic_f1(struct FUNCTION * restrict f, struct C z, int branch)
{
  struct FUNCTION_C *q = (struct FUNCTION_C *) f;
  return C_sqrt(C_sub(z, q->c), branch);
}
#endif

struct FUNCTION_C FUNCTION_quadratic(struct C c)
{
  return (struct FUNCTION_C){ { FUNCTION_quadratic_f
#if 0
    , FUNCTION_quadratic_f1, 2
#endif
    }, c };
}
