#include <assert.h>
#include "sort.h"

// "Bitonic sorting network for n not a power of 2",
// H. W. Lang,
// 1998-2018,
// <https://www.inf.hs-flensburg.de/lang/algorithmen/sortieren/bitonic/oddn.htm>
// (retrieved 2023-05-31)

static inline
int64_t greatestPowerOfTwoLessThan(int64_t n)
{
  assert(n >= 2);
  int64_t k = 1;
  while (k > 0 && k < n)
  {
    k <<= 1;
  }
  return k >> 1;
}

static inline
void exchange(unsigned char *a, int64_t size, int64_t i, int64_t j)
{
  assert(i != j);
  unsigned char * restrict p = a + i * size;
  unsigned char * restrict q = a + j * size;
  for (int64_t k = 0; k < size; ++k)
  {
    unsigned char x = p[k];
    unsigned char y = q[k];
    p[k] = y;
    q[k] = x;
  }
}

static inline
void compare(unsigned char *a, int64_t size, int (*cmp)(const void *, const void *), int64_t i, int64_t j, bool dir)
{
  if (dir == (cmp(a + i * size, a + j * size) > 0))
  {
    exchange(a, size, i, j);
  }
}

static
void bitonicMerge(unsigned char *a, int64_t size, int (*cmp)(const void *, const void *), int64_t lo, int64_t n, bool dir)
{
  if (n > 1)
  {
    int m = greatestPowerOfTwoLessThan(n);
    for (int i = lo; i < lo + n - m; i++)
    {
      compare(a, size, cmp, i, i + m, dir);
    }
    bitonicMerge(a, size, cmp, lo, m, dir);
    bitonicMerge(a, size, cmp, lo + m, n - m, dir);
  }
}

static
void bitonicSort(unsigned char *a, int64_t size, int (*cmp)(const void *, const void *), int64_t lo, int64_t n, bool dir)
{
  if (n > 1)
  {
    int m = n /2;
    bitonicSort(a, size, cmp, lo, m, ! dir);
    bitonicSort(a, size, cmp, lo + m, n - m, dir);
    bitonicMerge(a, size, cmp, lo, n, dir);
  }
}

void bsort(void *a, size_t count, size_t size, int (*cmp)(const void *, const void *))
{
  bitonicSort(a, size, cmp, 0, count, true);
}

// "Radix sort: In-place MSD radix sort implementations"
// Wikipedia
// <https://en.wikipedia.org/wiki/Radix_sort#In-place_MSD_radix_sort_implementations>
// (retrieved 2023-05-31)

static inline
int64_t greatestPowerOfTwoLessThanOrEqual(int64_t n)
{
  if (n <= 1)
  {
    return 1;
  }
  int64_t k = 1;
  while (k > 0 && k <= n)
  {
    k <<= 1;
  }
  return k >> 1;
}

static
void radixSort(unsigned char *a, int64_t size, uint32_t (*key)(const void *), uint32_t mask, int64_t lo_in, int64_t hi_in)
{
  int64_t lo = lo_in;
  int64_t hi = hi_in;
  while (lo < hi)
  {
    if (key(a + lo * size) & mask)
    {
      --hi;
      if (lo != hi)
      {
        exchange(a, size, lo, hi);
      }
    }
    else
    {
      ++lo;
    }
  }
  mask >>= 1;
  if (mask)
  {
    radixSort(a, size, key, mask, lo_in, lo);
    radixSort(a, size, key, mask, hi, hi_in);
  }
}

void rsort(void *a, size_t count, size_t size, uint32_t (*key)(const void *), uint32_t max_key)
{
  radixSort(a, size, key, greatestPowerOfTwoLessThanOrEqual(max_key), 0, count);
}
