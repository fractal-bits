#pragma once

#include <complex.h>
#include "julia.h"

struct MANDELBROT
{
  struct FUNCTION_C f;
  struct TREE *tree;
  struct C bounds;
  struct C bounds_julia;
};

struct MANDELBROT *MANDELBROT_new(void);
void MANDELBROT_delete(struct MANDELBROT *mandelbrot);
bool MANDELBROT_refine(struct MANDELBROT *mandelbrot);

struct MANDELBROT_PROGRESS;
struct MANDELBROT_PROGRESS *MANDELBROT_PROGRESS_new(struct MANDELBROT *mandelbrot);
void MANDELBROT_PROGRESS_delete(struct MANDELBROT_PROGRESS *progress);
struct MANDELBROT_PROGRESS *MANDELBROT_PROGRESS_load(const char *infile);
int MANDELBROT_PROGRESS_save(struct MANDELBROT_PROGRESS *progress, const char *outfile);
int64_t MANDELBROT_PROGRESS_run(struct MANDELBROT_PROGRESS *progress, struct C bounds_julia, int level, int64_t memory_limit, int64_t thread_limit, const char *resume_dir, _Atomic bool *running);
bool MANDELBROT_PROGRESS_finish(struct MANDELBROT_PROGRESS *progress, struct MANDELBROT *mandelbrot);
void PROGRESS_report(FILE *out, const int64_t *total);

struct MANDELBROT *MANDELBROT_fread_raw(FILE *in);
int MANDELBROT_fwrite_raw(FILE *out, struct MANDELBROT *mandelbrot);

static inline
int MANDELBROT_fwrite_ppm(FILE *out, struct MANDELBROT *mandelbrot)
{
  int64_t cursor = 0;
  int level = TREE_depth(mandelbrot->tree, &cursor, 0);
  int dim = 1 << level;
  struct C bounds = { { 0, dim }, { 0, dim } };
  struct IMAGE *image = IMAGE_new(dim, dim);
  if (! image)
  {
    return 1;
  }
  enum TREE_NODE node = TREE_read_node(mandelbrot->tree, 0);
  cursor = TREE_read_node_advance(mandelbrot->tree, 0);
  (void) node;
  assert(node == BRANCH);
  TREE_read_branch_enter(mandelbrot->tree, cursor);
  cursor = TREE_read_branch_enter_advance(mandelbrot->tree, cursor);
  cursor = IMAGE_tree(image, mandelbrot->tree, cursor, C_split(bounds, 0, 0), 0.25 * C_area(bounds));
  cursor = IMAGE_tree(image, mandelbrot->tree, cursor, C_split(bounds, 0, 1), 0.25 * C_area(bounds));
  TREE_skip(mandelbrot->tree, cursor);
  cursor = TREE_skip_advance(mandelbrot->tree, cursor);
  TREE_skip(mandelbrot->tree, cursor);
  cursor = TREE_skip_advance(mandelbrot->tree, cursor);
  TREE_read_branch_leave(mandelbrot->tree, cursor);
  cursor = TREE_read_branch_leave_advance(mandelbrot->tree, cursor);
  for (int y = 0; y < image->height / 2; ++y)
  {
    memcpy(&image->data[(image->height - 1 - y) * image->width * 3], &image->data[y * image->width * 3], image->width * 3);
  }
  int r = IMAGE_fwrite_ppm(out, image);
  IMAGE_delete(image);
  return r;
}

static inline
int MANDELBROT_fwrite_svg(FILE *out, struct MANDELBROT *mandelbrot)
{
  int level = 10;
  int s = 8 << level;
  double t = 1024.0 / s;
  if (0 > fprintf
    ( out
    , "<svg width='1024' height='1024'>"
      "<defs>"
      "<style>*{stroke:grey}.g{fill:red;}.w{fill:white;}.b{fill:black;}</style>"
      "</defs>"
      "<g transform='scale(%.24f,%.24f)'>"
      "<g id='m'>"
    , t, t
    ))
  {
    return 1;
  }
  struct C bounds = { { 0, s }, { 0, s } };
  enum TREE_NODE node = TREE_read_node(mandelbrot->tree, 0);
  int64_t cursor = TREE_read_node_advance(mandelbrot->tree, 0);
  (void) node;
  assert(node == BRANCH);
  TREE_read_branch_enter(mandelbrot->tree, cursor);
  cursor = TREE_read_branch_enter_advance(mandelbrot->tree, cursor);
  int r1 = 0, r2 = 0;
  cursor = TREE_svg(out, mandelbrot->tree, cursor, C_split(bounds, 0, 0), &r1);
  cursor = TREE_svg(out, mandelbrot->tree, cursor, C_split(bounds, 0, 1), &r2);
  TREE_skip(mandelbrot->tree, cursor);
  cursor = TREE_skip_advance(mandelbrot->tree, cursor);
  TREE_skip(mandelbrot->tree, cursor);
  cursor = TREE_skip_advance(mandelbrot->tree, cursor);
  TREE_read_branch_leave(mandelbrot->tree, cursor);
  cursor = TREE_read_branch_leave_advance(mandelbrot->tree, cursor);
  if (r1 || r2)
  {
    return 1;
  }
  if (0 > fprintf
    ( out
    , "</g>"
      "<use transform='translate(0,%d) scale(1,-1)' href='#m' />"
      "</g>"
      "</svg>\n"
    , s
    ))
  {
    return 1;
  }
  return 0;
}

static inline
double MANDELBROT_interior_distance_estimate(double _Complex c, double _Complex z, int period)
{
  const double epsilon = 1e-14;
  const double cx = creal(c);
  const double cy = cimag(c);
  for (int step = 0; step < 64; ++step)
  {
    // Newton's method for f^period(z) = z
    double wx = creal(z);
    double wy = cimag(z);
    double dwx = 1;
    double dwy = 0;
    for (int i = 0; i < period; ++i)
    {
      double dwxn = 2 * (dwx * wx - dwy * wy);
      double dwyn = 2 * (dwx * wy + dwy * wx);
      double wxn = wx * wx - wy * wy + cx;
      double wyn = 2 * wx * wy + cy;
      dwx = dwxn;
      dwy = dwyn;
      wx = wxn;
      wy = wyn;
    }
    wx -= creal(z);
    wy -= cimag(z);
    dwx -= 1;
    double _Complex zn = z - (wx + I * wy) / (dwx + I * dwy);
    double d = cabs(z - zn);
    z = zn;
    if (d < epsilon)
    {
      break;
    }
    if (isnan(d) || isinf(d))
    {
      return -1.0 / 0.0;
    }
  }
  double _Complex dz = 1;
  double _Complex dzdz = 0;
  double _Complex dc = 0;
  double _Complex dcdz = 0;
  for (int i = 0; i < period; ++i)
  {
    dcdz = 2 * (z * dcdz + dz * dc);
    dc = 2 * z * dc + 1;
    dzdz = 2 * (dz * dz + z * dzdz);
    dz = 2 * z * dz;
    z = z * z + c;
  }
  return (1 - cabs(dz * dz)) / cabs(dcdz + dzdz * dc / (1 - dz));
}

static inline
enum TREE_NODE MANDELBROT_estimate(struct C box, int64_t iteration_limit)
{
  const double ER2 = 65536.0 * 65536.0;
  const double cx = R_midpoint(box.x);
  const double cy = R_midpoint(box.y);
  double r_outer = 0.5 * hypot(R_length(box.x), R_length(box.y));
  double dcx = 1;
  double dcy = 0;
  double zx = cx;
  double zy = cy;
  double zx2 = zx * zx;
  double zy2 = zy * zy;
  double zxy = zx * zy;
  double mz2 = 1.0 / 0.0;
  for (int64_t n = 1; n < iteration_limit; ++n)
  {
    double z2 = zx2 + zy2;
    if (z2 < mz2)
    {
      if (z2 < 0.25 * mz2)
      {
        double de = MANDELBROT_interior_distance_estimate(cx + I * cy, zx + I * zy, n);
        if (0 < de)
        {
          if (4 * r_outer < de)
          {
            return BLACK;
          }
          else
          {
            return GREY;
          }
        }
      }
      mz2 = z2;
    }
    if (z2 > ER2)
    {
      double de = sqrt(z2 / (dcx * dcx + dcy * dcy)) * log(z2);
      if (4 * r_outer < de)
      {
        return WHITE;
      }
      else
      {
        return GREY;
      }
    }
    double dcxn = 2 * (dcx * zx - dcy * zy) + 1;
    double dcyn = 2 * (dcx * zy + dcy * zx);
    double zxn = zx2 - zy2 + cx;
    double zyn = 2 * zxy + cy;
    dcx = dcxn;
    dcy = dcyn;
    zx = zxn;
    zy = zyn;
    zx2 = zx * zx;
    zy2 = zy * zy;
    zxy = zx * zy;
  }
  return GREY;
}

static inline
struct R MANDELBROT_area(struct MANDELBROT *mandelbrot)
{
  double area[3] = {0,0,0};
  enum TREE_NODE node = TREE_read_node(mandelbrot->tree, 0);
  int64_t cursor = TREE_read_node_advance(mandelbrot->tree, 0);
  (void) node;
  assert(node == BRANCH);
  TREE_read_branch_enter(mandelbrot->tree, cursor);
  cursor = TREE_read_branch_enter_advance(mandelbrot->tree, cursor);
  cursor = TREE_count_area(mandelbrot->tree, cursor, area, 8);
  cursor = TREE_count_area(mandelbrot->tree, cursor, area, 8);
  TREE_skip(mandelbrot->tree, cursor);
  cursor = TREE_skip_advance(mandelbrot->tree, cursor);
  TREE_skip(mandelbrot->tree, cursor);
  cursor = TREE_skip_advance(mandelbrot->tree, cursor);
  TREE_read_branch_leave(mandelbrot->tree, cursor);
  cursor = TREE_read_branch_leave_advance(mandelbrot->tree, cursor);
  return (struct R){ area[BLACK], area[BLACK] + area[GREY] };
}

enum PROGRESS
{
  PROGRESS_TODO = 0,
  PROGRESS_BLACK = 1,
  PROGRESS_WHITE = 2,
  PROGRESS_DE = 3,
  PROGRESS_LEVEL = 4,
  PROGRESS_MEMORY = 5
};

struct MANDELBROT_PROGRESS
{
  _Atomic int64_t started;
  _Atomic int64_t completed;
  _Atomic int64_t totals[6];
  int64_t count;
  enum PROGRESS *state;
  struct C *c;
  int64_t memory_limit;
  struct C bounds;
  int level;
  const char *resume_dir;
  _Atomic bool *running;
};
