#pragma once

#include <stdint.h>

typedef struct
{
  unsigned char byte[3];
} uint24_t;

static inline
uint32_t uint32_from_uint24(uint24_t x)
{
#ifdef UINT24_BIG_ENDIAN
  return
    (((uint32_t) x.byte[0]) << 16) |
    (((uint16_t) x.byte[1]) << 8) |
    (x.byte[2]) ;
#else
  return
    (x.byte[0]) |
    (((uint16_t) x.byte[1]) << 8) |
    (((uint32_t) x.byte[2]) << 16) ;
#endif
}

static inline
uint64_t uint64_from_uint24(uint24_t x)
{
  return uint32_from_uint24(x);
}

static inline
uint24_t uint24_from_uint64(uint32_t x)
{
  uint24_t r;
#ifdef UINT24_BIG_ENDIAN
  r.byte[0] = ((uint32_t) x) >> 16;
  r.byte[1] = ((uint16_t) x) >> 8;
  r.byte[2] = x;
#else
  r.byte[0] = x;
  r.byte[1] = ((uint16_t) x) >> 8;
  r.byte[2] = ((uint32_t) x) >> 16;
#endif
  return r;
}

static inline
int uint8_cmp(uint8_t a, uint8_t b)
{
  return (a > b) - (b > a);
}

static inline
int uint16_cmp(uint16_t a, uint16_t b)
{
  return (a > b) - (b > a);
}

static inline
int uint32_cmp(uint32_t a, uint32_t b)
{
  return (a > b) - (b > a);
}

static inline
int uint64_cmp(uint64_t a, uint64_t b)
{
  return (a > b) - (b > a);
}

static inline
int uint24_cmp(uint24_t a, uint24_t b)
{
  const unsigned char * restrict p = &a.byte[0];
  const unsigned char * restrict q = &b.byte[0];
#ifdef UINT24_BIG_ENDIAN
  int r;
  r = uint8_cmp(p[0], q[0]);
  if (r) return r;
  r = uint8_cmp(p[1], q[1]);
  if (r) return r;
  return uint8_cmp(p[2], q[2]);
#else
  int r;
  r = uint8_cmp(p[2], q[2]);
  if (r) return r;
  r = uint8_cmp(p[1], q[1]);
  if (r) return r;
  return uint8_cmp(p[0], q[0]);
#endif
}

static inline
int uint24_cmpp(const uint24_t *a, const uint24_t *b)
{
  const unsigned char *p = &a->byte[0];
  const unsigned char *q = &b->byte[0];
#ifdef UINT24_BIG_ENDIAN
  int r;
  r = uint8_cmp(p[0], q[0]);
  if (r) return r;
  r = uint8_cmp(p[1], q[1]);
  if (r) return r;
  return uint8_cmp(p[2], q[2]);
#else
  int r;
  r = uint8_cmp(p[2], q[2]);
  if (r) return r;
  r = uint8_cmp(p[1], q[1]);
  if (r) return r;
  return uint8_cmp(p[0], q[0]);
#endif
}
