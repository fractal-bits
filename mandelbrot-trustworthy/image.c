#include "image.h"

int64_t IMAGE_tree(struct IMAGE * restrict image, struct TREE * restrict tree, int64_t cursor, struct C bounds, double bounds_area)
{
  if (bounds_area <= 1)
  {
    double area[3] = {0,0,0};
    cursor = TREE_count_area(tree, cursor, area, bounds_area);
    unsigned char rgb[3];
    rgb[0] = 255 * sqrt(area[WHITE] + area[GREY]);
    rgb[1] = 255 * sqrt(area[WHITE]);
    rgb[2] = 255 * sqrt(area[WHITE]);
    IMAGE_plot(image, bounds.x.lo, bounds.y.lo, rgb);
  }
  else
  {
    unsigned char rgb[3][3] = {{255,0,0},{0,0,0},{255,255,255}};
    enum TREE_NODE node = TREE_read_node(tree, cursor);
    cursor = TREE_read_node_advance(tree, cursor);
    switch (node)
    {
      case GREY:
      case BLACK:
      case WHITE:
        IMAGE_rect(image, bounds.x.lo, bounds.y.lo, R_length(bounds.x), R_length(bounds.y), rgb[node]);
        break;
      case BRANCH:
        bounds_area *= 0.25;
        TREE_read_branch_enter(tree, cursor);
        cursor = TREE_read_branch_enter_advance(tree, cursor);
        cursor = IMAGE_tree(image, tree, cursor, C_split(bounds, 0, 0), bounds_area);
        cursor = IMAGE_tree(image, tree, cursor, C_split(bounds, 0, 1), bounds_area);
        cursor = IMAGE_tree(image, tree, cursor, C_split(bounds, 1, 0), bounds_area);
        cursor = IMAGE_tree(image, tree, cursor, C_split(bounds, 1, 1), bounds_area);
        TREE_read_branch_leave(tree, cursor);
        cursor = TREE_read_branch_leave_advance(tree, cursor);
        break;
    }
  }
  return cursor;
}
