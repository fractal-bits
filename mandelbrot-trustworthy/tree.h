#pragma once

#include "bits.h"
#include "C.h"

#ifdef TREE_BITPACKED
#define TREE_NODE_SIZE 2 // bits
#else
#define TREE_NODE_SIZE 1 // bytes
#endif

#define TREE_SIZE_SIZE_MAX 48
#define TREE_SIZE_MAX ((int64_t)1 << TREE_SIZE_SIZE_MAX)
#define TREE_BRANCH_SIZE_MIN (4 * TREE_NODE_SIZE)
#define TREE_DEPTH_MAX 64

enum TREE_NODE
{
  GREY = 0,
  BLACK = 1,
  WHITE = 2,
  BRANCH = 3,
};

struct TREE
{
  unsigned char * restrict data;
  int64_t count;
  int64_t nbranches; // maximum number of branches
  int size_size; // number of bits/bytes to store size (must be even if bits);
  int64_t branch_size[TREE_DEPTH_MAX];
  int branch_ix;
};

static inline
int64_t TREE_size_required(int64_t nbranches, int size_size)
{
  int64_t nleaves = 3 * nbranches + 1;
  return TREE_NODE_SIZE * nleaves + (TREE_NODE_SIZE + size_size) * nbranches;
}

static inline
int TREE_size_size_for_tree_size(int64_t nbranches)
{
  if (nbranches >= TREE_SIZE_MAX)
  {
    return -1;
  }
  for (int size_size = 0; size_size <= TREE_SIZE_SIZE_MAX; size_size += TREE_NODE_SIZE)
  {
    int64_t maximum_size = ((int64_t) 1) << (size_size + (TREE_NODE_SIZE >> 1));
    if (TREE_size_required(nbranches, size_size) < maximum_size)
    {
      return size_size;
    }
  }
  return -1;
}

struct TREE *TREE_new(int64_t nbranches);
void TREE_delete(struct TREE * restrict tree);

static inline
struct TREE *TREE_fread_raw(FILE * restrict in, int64_t nbranches)
{
  struct TREE *tree = TREE_new(nbranches);
  if (! tree)
  {
    return NULL;
  }
  int64_t count = tree->count;
#ifdef TREE_BITPACKED
  count = (count + 7) / 8;
#endif
  if (1 != fread(tree->data, count, 1, in))
  {
    TREE_delete(tree);
    return NULL;
  }
  return tree;
}

static inline
void TREE_write_node(struct TREE * restrict tree, int64_t cursor, enum TREE_NODE node)
{
  assert(tree);
#ifdef TREE_BITPACKED
  BITS_write_2(tree->data, cursor, node);
#else
  tree->data[cursor] = node;
#endif
}

static inline
int64_t TREE_write_node_advance(struct TREE * restrict tree, int64_t cursor)
{
  (void) tree;
  return cursor + TREE_NODE_SIZE;
}

static inline
void TREE_write_branch_begin(struct TREE * restrict tree, int64_t cursor)
{
  assert(tree);
  assert(tree->branch_ix < TREE_DEPTH_MAX);
  TREE_write_node(tree, cursor, BRANCH);
  tree->branch_size[tree->branch_ix++] = TREE_write_node_advance(tree, cursor);
}

static inline
int64_t TREE_write_branch_begin_advance(struct TREE * restrict tree, int64_t cursor)
{
  return TREE_write_node_advance(tree, cursor) + tree->size_size;
}

static inline
void TREE_write_branch_end(struct TREE * restrict tree, int64_t cursor)
{
  assert(tree);
  assert(tree->branch_ix > 0);
  int64_t size_cursor = tree->branch_size[--tree->branch_ix];
  int64_t size = cursor - size_cursor - tree->size_size;
  assert(TREE_BRANCH_SIZE_MIN <= size);
  assert(size < tree->count);
#ifdef TREE_BITPACKED
  assert(0 == (size & 1));
  size >>= 1;
  BITS_write(tree->data, size_cursor, tree->size_size, size);
#else
  BITS_write(tree->data, size_cursor << 3, tree->size_size << 3, size);
#endif
}

static inline
int64_t TREE_write_branch_end_advance(struct TREE * restrict tree, int64_t cursor)
{
  (void) tree;
  return cursor;
}

static inline
enum TREE_NODE TREE_read_node(struct TREE * restrict tree, int64_t cursor)
{
  assert(tree);
#ifdef TREE_BITPACKED
  return BITS_read_2(tree->data, cursor);
#else
  return tree->data[cursor];
#endif
}

static inline
int64_t TREE_read_node_advance(struct TREE * restrict tree, int64_t cursor)
{
  (void) tree;
  return cursor + TREE_NODE_SIZE;
}

static inline
void TREE_read_branch_enter(struct TREE * restrict tree, int64_t cursor)
{
  (void) tree;
  (void) cursor;
  assert(tree);
#ifndef NDEBUG
  assert(tree->branch_ix < TREE_DEPTH_MAX);
  int64_t size_size = tree->size_size;
#ifdef TREE_BITPACKED
  int64_t size = BITS_read(tree->data, cursor, size_size) << 1;
#else
  int64_t size = BITS_read(tree->data, cursor << 3, size_size << 3);
#endif
  tree->branch_size[tree->branch_ix++] = cursor + size_size + size;
#endif
}

static inline
int64_t TREE_read_branch_enter_advance(struct TREE * restrict tree, int64_t cursor)
{
  return cursor + tree->size_size;
}

static inline
void TREE_read_branch_leave(struct TREE * restrict tree, int64_t cursor)
{
  (void) tree;
  (void) cursor;
  assert(tree->branch_ix > 0);
  assert(tree->branch_size[--tree->branch_ix] == cursor);
}

static inline
int64_t TREE_read_branch_leave_advance(struct TREE * restrict tree, int64_t cursor)
{
  (void) tree;
  return cursor;
}

static inline
void TREE_branch_skip(struct TREE * restrict tree, int64_t cursor)
{
  (void) tree;
  (void) cursor;
}

static inline
int64_t TREE_branch_skip_advance(struct TREE * restrict tree, int64_t cursor)
{
  assert(tree);
#ifdef TREE_BITPACKED
  int64_t size = BITS_read(tree->data, cursor, tree->size_size) << 1;
#else
  int64_t size = BITS_read(tree->data, cursor << 3, tree->size_size << 3);
#endif
  return cursor + tree->size_size + size;
}

static inline
void TREE_skip(struct TREE * restrict tree, int64_t cursor)
{
  (void) tree;
  (void) cursor;
}

static inline
int64_t TREE_skip_advance(struct TREE * restrict tree, int64_t cursor)
{
  enum TREE_NODE node = TREE_read_node(tree, cursor);
  cursor = TREE_read_node_advance(tree, cursor);
  if (BRANCH == node)
  {
    TREE_branch_skip(tree, cursor);
    cursor = TREE_branch_skip_advance(tree, cursor);
  }
  return cursor;
}

struct TREE *TREE_refine_destroy(struct TREE * restrict in);
void TREE_refine(struct TREE *restrict out, int64_t * restrict out_cursor, struct TREE * restrict in, int64_t * restrict incursor);
void TREE_prune(struct TREE * restrict out, int64_t * restrict out_cursor, struct TREE * restrict in, int64_t * restrict incursor, bool prune_grey);
void TREE_prune_in_place(struct TREE * restrict tree, bool prune_grey);
void TREE_compact(struct TREE * restrict tree);
int64_t TREE_count_nodes(struct TREE * restrict tree, int64_t cursor, int64_t *count);
int64_t TREE_count_area(struct TREE * restrict tree, int64_t cursor, double * restrict area, double current_area);
int64_t TREE_count_area_within(struct TREE * restrict tree, int64_t cursor, struct C bounds, double area_bounds, struct C target, double area_target, double * restrict area);
enum TREE_NODE TREE_classify(struct TREE * restrict tree, int64_t * restrict cursor, struct C z);
int TREE_fwrite_raw(FILE * restrict out, struct TREE * restrict tree);
int TREE_fwrite_svg(FILE * restrict out, struct TREE * restrict tree, int level);
int64_t TREE_svg(FILE *out, struct TREE * restrict tree, int64_t cursor, struct C bounds, int * restrict error);
int64_t TREE_read_greys(struct TREE * restrict tree, int64_t cursor, struct C c, int64_t * restrict index, struct C * restrict cs);
int64_t TREE_write_greys(struct TREE * restrict tree, int64_t cursor, int64_t * restrict index, enum TREE_NODE * restrict nodes);
void TREE_copy(struct TREE * restrict out, int64_t * restrict out_cursor, struct TREE * restrict in, int64_t * restrict in_cursor);
int TREE_depth(struct TREE * restrict in, int64_t * restrict cursor, int depth);
