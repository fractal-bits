#pragma once

#include <unistd.h>
#include "function.h"
#include "graph.h"
#include "image.h"

typedef int64_t julia_node_t;

struct JULIA
{
  struct FUNCTION_C function;
  struct C bounds;
  struct TREE *tree;
  struct GRAPH *graph;
  graph_node_t node_max;
};

static inline
struct JULIA *JULIA_new(struct C c, struct C bounds)
{
  struct JULIA *julia = MEMORY_alloc(sizeof(*julia));
  if (! julia)
  {
    return NULL;
  }
  julia->function = FUNCTION_quadratic(c);
  julia->bounds = bounds;
  julia->tree = TREE_new(0);
  if (! julia->tree)
  {
    MEMORY_free(julia, sizeof(*julia));
    return NULL;
  }
  TREE_write_node(julia->tree, 0, GREY);
  julia->graph = NULL;
  return julia;
}

static inline
void JULIA_delete(struct JULIA * restrict julia)
{
  assert(julia);
  assert(julia->tree);
  if (julia->graph)
  {
    GRAPH_delete(julia->graph);
  }
  TREE_delete(julia->tree);
  MEMORY_free(julia, sizeof(*julia));
}

#define JULIA_EXTERIOR ((julia_node_t)(-1))

static inline
graph_node_t JULIA_to_GRAPH(julia_node_t s)
{
  if (s != JULIA_EXTERIOR)
  {
    assert(0 == (s & 1));
    s >>= 1;
  }
  s += 1;
  return s;
}

static inline
julia_node_t JULIA_from_GRAPH(graph_node_t s)
{
  if (s == 0)
  {
    return JULIA_EXTERIOR;
  }
  else
  {
    return (s - 1) << 1;
  }
}

static inline
void JULIA_visited_clear(struct JULIA * restrict julia)
{
  assert(julia);
  GRAPH_visit_clear(julia->graph);
}

static inline
void JULIA_visited_set_raw(unsigned char * restrict visited, julia_node_t s)
{
  assert(visited);
  BITS_write_1(visited, JULIA_to_GRAPH(s), 1);
}

static inline
bool JULIA_visited_get_raw(unsigned char * restrict visited, julia_node_t s)
{
  assert(visited);
  return BITS_read_1(visited, JULIA_to_GRAPH(s));
}

static inline
void JULIA_succs(struct JULIA * restrict julia, julia_node_t source, graph_edge_t * restrict begin, graph_edge_t * restrict end)
{
  assert(julia);
  GRAPH_succs(julia->graph, JULIA_to_GRAPH(source), begin, end);
}

static inline
void JULIA_preds(struct JULIA * restrict julia, julia_node_t sink, graph_edge_t * restrict begin, graph_edge_t * restrict end)
{
  assert(julia);
  GRAPH_preds(julia->graph, JULIA_to_GRAPH(sink), begin, end);
}

static inline
julia_node_t JULIA_preds_pred(struct JULIA * restrict julia, graph_edge_t e)
{
  assert(julia);
  return JULIA_from_GRAPH(GRAPH_preds_pred(julia->graph, e));
}

static inline
julia_node_t JULIA_preds_succ(struct JULIA * restrict julia, graph_edge_t e)
{
  assert(julia);
  return JULIA_from_GRAPH(GRAPH_preds_succ(julia->graph, e));
}

static inline
julia_node_t JULIA_succs_pred(struct JULIA * restrict julia, graph_edge_t e)
{
  assert(julia);
  return JULIA_from_GRAPH(GRAPH_succs_pred(julia->graph, e));
}

static inline
julia_node_t JULIA_succs_succ(struct JULIA * restrict julia, graph_edge_t e)
{
  assert(julia);
  return JULIA_from_GRAPH(GRAPH_succs_succ(julia->graph, e));
}

static inline
bool JULIA_add_edge(struct JULIA * restrict julia, julia_node_t source, julia_node_t sink)
{
  assert(julia->graph);
  return GRAPH_add(julia->graph, JULIA_to_GRAPH(source), JULIA_to_GRAPH(sink));
}

bool JULIA_refine(struct JULIA * restrict julia);

enum TREE_NODE JULIA_classify(struct JULIA * restrict julia);

struct JULIA *JULIA_fread_raw(FILE * restrict in);
int JULIA_fwrite_raw(FILE * restrict out, struct JULIA * restrict julia);

enum JULIA_REASON
{
  JULIA_OK,
  JULIA_LEVEL_LIMIT,
  JULIA_MEMORY_LIMIT,
};

static inline
enum TREE_NODE JULIA_calculate(const char *filename, struct C c, struct C bounds, int level_limit, enum JULIA_REASON *reason)
{
  *reason = JULIA_OK;
  struct JULIA *julia = NULL;
  int level = level_limit;
  char command[1000];
  snprintf(command, sizeof(command), "gunzip -f '%s.gz' >/dev/null 2>/dev/null", filename); // FIXME SECURITY unsafe shell quoting
  system(command);
  FILE *in = fopen(filename, "rb");
  if (in)
  {
    julia = JULIA_fread_raw(in);
    fclose(in);
    int64_t cursor = 0;
    level = TREE_depth(julia->tree, &cursor, 0);
  }
  if (! julia)
  {
    julia = JULIA_new(c, bounds);
    level = 0;
  }
  if (! julia)
  {
    *reason = JULIA_MEMORY_LIMIT;
    return GREY;
  }
  enum TREE_NODE tag;
  while (GREY == (tag = JULIA_classify(julia)))
  {
    ++level;
    if (level > level_limit)
    {
      *reason = JULIA_LEVEL_LIMIT;
      break;
    }
    if (! JULIA_refine(julia))
    {
      *reason = JULIA_MEMORY_LIMIT;
      break;
    }
  }
  if (*reason == JULIA_MEMORY_LIMIT)
  {
    FILE *out = fopen(filename, "wb");
    if (out)
    {
      JULIA_fwrite_raw(out, julia);
      fclose(out);
      char command[1000];
      snprintf(command, sizeof(command), "gzip -9 -f '%s'", filename); // FIXME SECURITY unsafe shell quoting
      system(command);
    }
  }
  else
  {
    unlink(filename);
  }
  JULIA_delete(julia);
  return tag;
}

static inline
int JULIA_fwrite_ppm(FILE * restrict out, struct JULIA * restrict julia)
{
  int level = 10; // TREE_depth(julia->tree, &cursor, 0); // FIXME
  int dim = 1 << level;
  struct C bounds = { { 0, dim }, { 0, dim } };
  struct IMAGE *image = IMAGE_new(dim, dim);
  if (! image)
  {
    return 1;
  }
  IMAGE_tree(image, julia->tree, 0, bounds, C_area(bounds));
  int r = IMAGE_fwrite_ppm(out, image);
  IMAGE_delete(image);
  return r;
}

static inline
int JULIA_fwrite_svg(FILE * restrict out, struct JULIA * restrict julia)
{
  return TREE_fwrite_svg(out, julia->tree, 10);
}

static inline
struct R JULIA_area(struct JULIA * restrict julia)
{
  double area[3] = {0,0,0};
  TREE_count_area(julia->tree, 0, area, C_area(julia->bounds));
  return (struct R){ area[BLACK], area[BLACK] + area[GREY] };
}
