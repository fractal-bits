#pragma once

#include <stdbool.h>
#include "math.h"

struct R
{
  double lo;
  double hi;
};

static inline
struct R R_add(struct R a, struct R b)
{
  return (struct R){ a.lo + b.lo, a.hi + b.hi };
}

static inline
struct R R_sub(struct R a, struct R b)
{
  return (struct R){ a.lo - b.hi, a.hi - b.lo };
}

static inline
struct R R_neg(struct R x)
{
  double lo = x.lo;
  double hi = x.hi;
  x.lo = -hi;
  x.hi = -lo;
  return x;
}

static inline
struct R R_sqr(struct R a)
{
  if (a.lo <= 0 && 0 <= a.hi)
  {
    return (struct R){ 0.0, fmax(a.lo * a.lo, a.hi * a.hi) };
  }
  else
  {
    double lo2 = a.lo * a.lo;
    double hi2 = a.hi * a.hi;
    return (struct R){ fmin(lo2, hi2), fmax(lo2, hi2) };
  }
}

static inline
struct R R_mul(struct R a, struct R b)
{
  double ll = a.lo * b.lo;
  double lh = a.lo * b.hi;
  double hl = a.hi * b.lo;
  double hh = a.hi * b.hi;
  return (struct R){ fmin(fmin(ll, lh), fmin(hl, hh)), fmax(fmax(ll, lh), fmax(hl, hh)) };
}

static inline
struct R R_mul2(struct R a)
{
  return (struct R){ a.lo * 2, a.hi * 2 };
}

static inline
double R_midpoint(struct R z)
{
  return (z.lo + z.hi) * 0.5;
}

static inline
struct R R_split(struct R z, int w)
{
  double md = R_midpoint(z);
  if (w) return (struct R){ md, z.hi };
  else return (struct R){ z.lo, md };
}

static inline
bool R_inside_or_equal(struct R a, struct R b)
{
  return b.lo <= a.lo && a.hi <= b.hi;
}

static inline
bool R_outside(struct R a, struct R b)
{
  return a.hi < b.lo || b.hi < a.lo;
}

static inline
bool R_outside_or_equal(struct R a, struct R b)
{
  return a.hi <= b.lo || b.hi <= a.lo;
}

static inline
bool R_equal(struct R a, struct R b)
{
  return a.lo == b.lo && a.hi == b.hi;
}

static inline
double R_length(struct R a)
{
  return a.hi - a.lo;
}
