#pragma once

#include <string.h>
#include "tree.h"

struct IMAGE
{
  unsigned char *data;
  int64_t width;
  int64_t height;
};

static inline
struct IMAGE *IMAGE_new(int width, int height)
{
  struct IMAGE *image = MEMORY_alloc(sizeof(*image));
  if (! image)
  {
    return NULL;
  }
  image->width = width;
  image->height = height;
  image->data = MEMORY_alloc(image->width * image->height * 3);
  if (! image->data)
  {
    MEMORY_free(image, sizeof(*image));
    return NULL;
  }
  memset(image->data, 128, image->width * image->height * 3);
  return image;
}

static inline
void IMAGE_delete(struct IMAGE * restrict image)
{
  assert(image);
  assert(image->data);
  MEMORY_free(image->data, image->width * image->height * 3);
  MEMORY_free(image, sizeof(*image));
}

static inline
int IMAGE_fwrite_ppm(FILE * restrict out, struct IMAGE * restrict image)
{
  assert(image);
  assert(image->data);
  if (0 > fprintf(out, "P6\n%ld %ld\n255\n", image->width, image->height))
  {
    return 1;
  }
  if (1 != fwrite(image->data, 3 * image->width * image->height, 1, out))
  {
    return 1;
  }
  return 0;
}

static inline
int IMAGE_save_ppm(struct IMAGE * restrict image, const char * restrict filename)
{
  FILE *out = fopen(filename, "wb");
  if (! out)
  {
    return 1;
  }
  int r = IMAGE_fwrite_ppm(out, image);
  fclose(out);
  return r;
}

static inline
void IMAGE_plot(struct IMAGE * restrict image, int x, int y, const unsigned char * restrict rgb)
{
  assert(image);
  assert(image->data);
  assert(0 <= x);
  assert(x < image->width);
  assert(0 <= y);
  assert(y < image->height);
  assert(rgb);
  unsigned char *px = &image->data[(y * image->width + x) * 3];
  px[0] = rgb[0];
  px[1] = rgb[1];
  px[2] = rgb[2];
}

static inline
void IMAGE_rect(struct IMAGE * restrict image, int x, int y, int w, int h, const unsigned char * restrict rgb)
{
  for (int j = y; j < y + h; ++j)
  {
    for (int i = x; i < x + w; ++i)
    {
      IMAGE_plot(image, i, j, rgb);
    }
  }
}

int64_t IMAGE_tree(struct IMAGE * restrict image, struct TREE * restrict tree, int64_t cursor, struct C bounds, double bounds_area);
