#!/bin/bash
trustworthy="$(dirname "$(readlink -e "$0")")/trustworthy"
GB="${1}"
re="${2}"
im="${3}"
refineonly="${4}"
timer=""
if [ "x${GB}" = "x" -o "x${re}" = "x" -o "x${im}" = "x" ]
then
  echo "usage: $0 memory_limit_in_GB re im [refineonly]"
  exit 1
fi
if [ "x${refineonly}" != "x" ]
then
  refineonly="refineonly"
  timer="time"
fi
src=0
dst=1
[ -e "${src}.J" ] || "${trustworthy}" julia create "${src}.J" "${re}" "${re}" "${im}" "${im}"
if [ "${refineonly}" != refineonly ]
then
  [ "${src}.svg" -nt "${src}.J" ] || "${trustworthy}" julia svg "${src}.J" "${src}.svg"
  [ "${src}.ppm" -nt "${src}.J" ] || "${trustworthy}" julia ppm "${src}.J" "${src}.ppm"
  "${trustworthy}" julia classify "${src}.J"
  "${trustworthy}" julia area "${src}.J"
fi
while [ "${dst}.J" -nt "${src}.J" ] || ${timer} "${trustworthy}" julia refine "${src}.J" "${dst}.J" "${GB}"
do
  echo "${dst}"
  src="${dst}"
  dst="$((src + 1))"
  if [ "${refineonly}" != "refineonly" ]
  then
    if (( "${src}" <= 10 ))
    then
      [ "${src}.svg" -nt "${src}.J" ] || "${trustworthy}" julia svg "${src}.J" "${src}.svg"
    fi
    [ "${src}.ppm" -nt "${src}.J" ] || "${trustworthy}" julia ppm "${src}.J" "${src}.ppm"
    "${trustworthy}" julia classify "${src}.J"
    "${trustworthy}" julia area "${src}.J"
  fi
  if (( "${src}" >= 24 ))
  then
    break
  fi
done
