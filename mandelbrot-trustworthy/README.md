# Trustworthy Fractals

Mathematically proven images:

white

: 100% exterior

black

: 100% interior

otherwise

: not yet determined (zone of unknown)

## References

Images of Julia sets that you can trust.
Luiz Henrique de Figueiredo,
Diego Nehab,
Jorge Stolfi,
João Batista S. de Oliveira.
2013.
<http://webdoc.sub.gwdg.de/ebook/serien/e/IMPA_A/721.pdf>

Rigorous bounds for polynomial Julia sets.
Luiz Henrique de Figueiredo,
Diego Nehab,
Jorge Stolfi,
João Batista S. de Oliveira.
Journal of Computational Dynamics, 2016, 3(2): 113-137.
doi: 10.3934/jcd.2016006

## Usage

### Julia set

```
./trustworthy-julia.sh memory-limit-in-GB re im [refine-only]
```

Computes a Julia set via successive refinement.

Best to run it from inside a new empty directory, unless resuming.

The maximum refinement level is about 24,
beyond that the arithmetic is not exact and the result is not proven.

Conjectured memory requirement is O(2^(fractal dimension × level)).

Single-threaded.

### Mandelbrot set

```
./trustworthy-mandelbrot.sh memory-limit-in-GB [number-of-threads] [julia-level-mul] [julia-level-add] [refine-only]
```

Computes the Mandelbrot set via successive refinement.

Best to run it from inside a new empty directory, unless resuming.

Ctrl-C interrupts and saves state, re-run the command to resume.

The maximum refinement level for Julia set classification is

```
julia-level-mul * mandelbrot-level + julia-level-add
```

The default values for the julia-level controls are 1 and 4 respectively.

The maximum memory limit for Julia sets is
the specified memory limit divided by the number of threads.
By default, one thread is launched per logical CPU.

The maximum refinement level for the Mandelbrot sets is about 20
with default settings, corresponding to level 24 Julia sets.

### Benchmark

```
mkdir tmp
cd tmp
../benchmark.sh memory-limit-in-GB number-of-threads
```

Benchmarks Mandelbrot first (multithreaded).
Hit Ctrl-C when sufficient benchmarking has been performed.
Then Julia set benchmarking will take place (single threaded).

## Benchmarks

Wall-clock elapsed time, maximum resident size.

### AMD 2700X CPU (gcc)

8 cores, 16 threads, 32GB physical RAM, 24GB memory limit.

#### Julia sets

These figures are out of date.

##### Dust c = 1

- level 21: 1.73s, 151MB
- level 22: 3.17s, 251MB
- level 23: 5.88s, 433MB
- level 24: 11.49s, 737MB

##### Circle c = 0

- level 21: 30.61s, 1.96GB
- level 22: 48.50s, 3.92GB
- level 23: 115.54s, 7.84GB
- level 24: 256.85s, 15.7GB

##### Basilica c = -1

- level 17: 16.24s, 1.21GB
- level 18: 40.14s, 2.91GB
- level 19: 99.35s, 7.00GB
- level 20: 238.26s, 16.8GB

##### Cauliflower c = 0.25

- level 12: 1.19s, 155MB
- level 13: 4.87s, 586MB
- level 14: 26.55s, 2.32GB
- level 15: 101.54s, 9.20GB

##### Fat Basilica c = -0.75

- level 13: 3.81s, 379MB
- level 14: 15.90s, 1.47GB
- level 15: 60.49s, 5.78GB
- level 16: 310.36s, 23.0GB

#### Mandelbrot set

qsort:

- level 5: 0.91s, 226MB
- level 6: 8.42s, 966MB
- level 7: 93.31s, 2.92GB
- level 8: 1057.96, 9.63GB

rsort:

- level 5: 1.32s, 188MB
- level 6: 10.82s, 667MB
- level 7: 110.91s, 2.20GB (2 minutes)
- level 8: 1166.52s, 8.04GB (20 minutes)
- level 9: 8176s (16 threads, 20.9GB) + 5892s (8 threads, 15.8GB, OOM recalculated 2402 items) (4 hours)
- level 10: 32340s (16 threads, 21.0GB) + 885.63s (8 threads, 16.0GB), OOM disk cache 20916 items 2.8GB (9 hours)
- level 11: 323729s (8 threads, 20.9GB) + 53878s (4 threads, 10.4GB) + 7762s (1 thread, 9.22GB), OOM disk cache 73468 items 30GB (4.5 days)

### ARM Cortex A53 CPU (gcc)

4 cores, 1 thread, 2GB physical RAM, 1GB memory limit.

#### Julia sets

##### Dust c = 1

- level 21: 12.54s, 142MB
- level 22: 22.33s, 245MB
- level 23: 39.95s, 423MB
- level 24: 71.30s, 729MB

##### Circle c = 0

- level 17: 8.63s, 123MB
- level 18: 18.16s, 246MB
- level 19: 37.97s, 490MB
- level 20: 79.41s, 979MB

##### Basilica c = -1

- level 14: 6.26s, 86.8MB
- level 15: 15.91s, 208MB
- level 16: 41.18s, 501MB
- level 17: 106.29s, 1.18GB

##### Cauliflower c = 0.25

- level 10: 0.51s, 10.4MB
- level 11: 2.11s, 38.2MB
- level 12: 8.84s, 148MB
- level 13: 37.76s, 581MB

##### Fat Basilica c = -0.75

- level 10: 0.38s, 7.50MB
- level 11: 1.51s, 25.9MB
- level 12: 6.11s, 96.7MB
- level 13: 25.89s, 373MB

#### Mandelbrot set

- level 3: 1.23s, 1.21MB
- level 4: 6.92s, 3.53MB
- level 5: 58.57s, 10.6MB
- level 6: 579.83s, 70.8MB

## Contents

README.md

: documentation

Makefile

: build system

includes.dot

: source dependency graph

  ![Source code dependency graph.](includes.svg)

C.h

: complex intervals

N.h

: additional uint sizes

R.h

: real intervals

bits.h

: packed bit arrays

function.c, function.h

: quadratic polynomial

graph.c, graph.h

: directed graphs

image.c, image.h

: rgb raster

julia.c, julia.h

: Julia sets via cell mapping

main.c

: command line program

mandelbrot.c, mandelbrot.h

: Mandelbrot set via Julia sets

math.h

: mathematical functions

memory.c, memory.h

: allocation tracking

sort.c, sort.h

: in place array sorting

tree.c, tree.h

: compact quad tree via bit arrays

benchmark.sh, trustworthy-julia.sh, trustworthy-mandelbrot.sh

: example scripts
