#include <stdio.h>
#include <string.h>

#include "julia.h"

void J_init(J j, constC c)
{
  memset(j, 0, sizeof(*j));
  mpfr_prec_t prec = 2 * C_get_prec(c);
  C_tmp_init(j->ctmp, prec);
  C_init(j->bounds, prec);
  mpfr_set_si(j->bounds->x->lo, -J_SIZE, MPFR_RNDD);
  mpfr_set_si(j->bounds->x->hi, J_SIZE, MPFR_RNDU);
  mpfr_set_si(j->bounds->y->lo, -J_SIZE, MPFR_RNDD);
  mpfr_set_si(j->bounds->y->hi, J_SIZE, MPFR_RNDU);
  C_init(j->c, prec);
  C_set(j->c, c);
  C_init(j->z, prec);
  int n = 1 << j->level;
  B_init(j->known[j->level], n, n);
  B_init(j->state[j->level], n, n);
  B_fill(j->known[j->level], false);
  B_fill(j->state[j->level], false);
}

void J_clear(J j)
{
  for (; j->level >= 0; --j->level)
  {
    B_clear(j->state[j->level]);
    B_clear(j->known[j->level]);
  }
  C_clear(j->z);
  C_clear(j->c);
  C_clear(j->bounds);
  C_tmp_clear(j->ctmp);
}

void J_box(constJ j, int level, C z, int x, int y)
{
  mpfr_set_si(z->x->lo, x, MPFR_RNDD);
  mpfr_mul_2exp(z->x->lo, z->x->lo, J_OFFSET, MPFR_RNDD);
  mpfr_div_2exp(z->x->lo, z->x->lo, level, MPFR_RNDD);
  mpfr_add_si(z->x->lo, z->x->lo, -J_SIZE, MPFR_RNDD);
  mpfr_set_si(z->x->hi, x + 1, MPFR_RNDU);
  mpfr_mul_2exp(z->x->hi, z->x->hi, J_OFFSET, MPFR_RNDU);
  mpfr_div_2exp(z->x->hi, z->x->hi, level, MPFR_RNDU);
  mpfr_add_si(z->x->hi, z->x->hi, -J_SIZE, MPFR_RNDU);
  mpfr_set_si(z->y->lo, y, MPFR_RNDD);
  mpfr_mul_2exp(z->y->lo, z->y->lo, J_OFFSET, MPFR_RNDD);
  mpfr_div_2exp(z->y->lo, z->y->lo, level, MPFR_RNDD);
  mpfr_add_si(z->y->lo, z->y->lo, -J_SIZE, MPFR_RNDD);
  mpfr_set_si(z->y->hi, y + 1, MPFR_RNDU);
  mpfr_mul_2exp(z->y->hi, z->y->hi, J_OFFSET, MPFR_RNDU);
  mpfr_div_2exp(z->y->hi, z->y->hi, level, MPFR_RNDU);
  mpfr_add_si(z->y->hi, z->y->hi, -J_SIZE, MPFR_RNDU);
}

void J_unbox(constJ j, int level, C z, int *x0, int *x1, int *y0, int *y1)
{
  mpfr_sub_si(z->x->lo, z->x->lo, -J_SIZE, MPFR_RNDD);
  mpfr_mul_2exp(z->x->lo, z->x->lo, level, MPFR_RNDD);
  mpfr_div_2exp(z->x->lo, z->x->lo, J_OFFSET, MPFR_RNDD);
  *x0 = mpfr_get_si(z->x->lo, MPFR_RNDD);
  mpfr_sub_si(z->x->hi, z->x->hi, -J_SIZE, MPFR_RNDU);
  mpfr_mul_2exp(z->x->hi, z->x->hi, level, MPFR_RNDU);
  mpfr_div_2exp(z->x->hi, z->x->hi, J_OFFSET, MPFR_RNDU);
  *x1 = mpfr_get_si(z->x->hi, MPFR_RNDU);
  mpfr_sub_si(z->y->lo, z->y->lo, -J_SIZE, MPFR_RNDD);
  mpfr_mul_2exp(z->y->lo, z->y->lo, level, MPFR_RNDD);
  mpfr_div_2exp(z->y->lo, z->y->lo, J_OFFSET, MPFR_RNDD);
  *y0 = mpfr_get_si(z->y->lo, MPFR_RNDD);
  mpfr_sub_si(z->y->hi, z->y->hi, -J_SIZE, MPFR_RNDU);
  mpfr_mul_2exp(z->y->hi, z->y->hi, level, MPFR_RNDU);
  mpfr_div_2exp(z->y->hi, z->y->hi, J_OFFSET, MPFR_RNDU);
  *y1 = mpfr_get_si(z->y->hi, MPFR_RNDU);
}

bool J_refine(J j)
{
  if (j->level < J_LEVEL_MAX - 1)
  {
    int n1 = 1 << j->level;
    ++j->level;
    int n = 1 << j->level;
    B_init(j->known[j->level], n, n);
    B_init(j->state[j->level], n, n);
    for (int y = 0; y < n; ++y)
    {
      for (int x = 0; x < n; ++x)
      {
        bool known = B_get(j->known[j->level - 1], x >> 1, y >> 1);
        bool state = B_get(j->state[j->level - 1], x >> 1, y >> 1);
	B_set(j->known[j->level], x, y, known);
	B_set(j->state[j->level], x, y, state);
      }
    }
for (int pass = 0; pass < 2; ++pass)
{
    bool changed = true;
    while (changed)
    {
      changed = false;
      for (int y = 0; y < n; ++y)
      {
        for (int x = 0; x < n; ++x)
        {
          bool known = B_get(j->known[j->level], x, y);
          bool state = B_get(j->state[j->level], x, y);
          if (! known)
          {
            J_box(j, j->level, j->z, x, y);
//fprintf(stderr, "%d %d -> %.18f %.18f %.18f %.18f -> ", x, y, mpfr_get_d(j->z->x->lo, MPFR_RNDD), mpfr_get_d(j->z->x->hi, MPFR_RNDU), mpfr_get_d(j->z->y->lo, MPFR_RNDD), mpfr_get_d(j->z->y->hi, MPFR_RNDU));
            C_sqr(j->z, j->z, j->ctmp);
            C_add(j->z, j->z, j->c);
//fprintf(stderr, "%.18f %.18f %.18f %.18f", mpfr_get_d(j->z->x->lo, MPFR_RNDD), mpfr_get_d(j->z->x->hi, MPFR_RNDU), mpfr_get_d(j->z->y->lo, MPFR_RNDD), mpfr_get_d(j->z->y->hi, MPFR_RNDU));
            int x0, x1, y0, y1;
            J_unbox(j, j->level, j->z, &x0, &x1, &y0, &y1);
//fprintf(stderr, " -> %d %d %d %d\n", x0, x1, y0, y1);
            if (n1 <= x0 || x1 < 0 || n1 <= y0 || y1 < 0)
            {
              known = true;
              state = true;
            }
            else
            {
              bool all_known = true;
              bool any_known = false;
              bool all_exterior = true;
              bool any_exterior = false;
              bool any_marked = false;
              if (x0 < 0)
              {
                any_known = true;
                any_exterior = true;
                x0 = 0;
              }
              if (y0 < 0)
              {
                any_known = true;
                any_exterior = true;
                y0 = 0;
              }
              if (x1 > n1 - 1)
              {
                any_known = true;
                any_exterior = true;
                x1 = n1 - 1;
              }
              if (y1 > n1 - 1)
              {
                any_known = true;
                any_exterior = true;
                y1 = n1 - 1;
              }
              for (int yy = y0; yy <= y1; ++yy)
              for (int xx = x0; xx <= x1; ++xx)
              {
                bool cell_known = B_get(j->known[j->level], xx, yy);
                bool cell_state = B_get(j->state[j->level], xx, yy);
                bool exterior = cell_known && cell_state;
                bool marked = ! cell_known && cell_state;
                all_known &= cell_known;
                any_known |= cell_known;
                all_exterior &= exterior;
                any_exterior |= exterior;
                any_marked |= marked;;
              }
if (pass == 0)
{
              if (all_known && all_exterior)
              {
                known = true;
                state = true;
              }
              else
              {
                known = false;
                state = false;
              }
}
else
{
              if (any_exterior || any_marked)
              {
                known = false;
                state = true;
              }
              else
              {
                known = false;
                state = false;
              }
}
            }
            B_set(j->known[j->level], x, y, known);
            B_set(j->state[j->level], x, y, state);
if (pass == 0)
{
            if (known)
            {
              changed = true;
            }
}
else
{
  if (state)
  {
    changed = true;
  }
}
          }
        }
      }
    }
}
    for (int y = 0; y < n; ++y)
    {
      for (int x = 0; x < n; ++x)
      {
        bool known = B_get(j->known[j->level], x, y);
        bool state = B_get(j->state[j->level], x, y);
        if (! known && state)
        {
          known = true;
          state = false;
          B_set(j->known[j->level], x, y, known);
          B_set(j->state[j->level], x, y, state);
        }
      }
    }
    for (int y = 0; y < n; ++y)
    {
      for (int x = 0; x < n; ++x)
      {
        bool known = B_get(j->known[j->level], x, y);
        bool state = B_get(j->state[j->level], x, y);
        putchar(known ? state ? 'X' : 'I' : state ? '!' : '?');
      }
      putchar('\n');
    }
    putchar('\n');
    return true;
  }
  else
  {
    return false;
  }
}

int J_class(J j)
{
  if (j->level > 0)
  {
    int n = (1 << j->level) >> 1;
    bool all_known = true;
    bool any_known = false;
    bool all_exterior = true;
    bool any_exterior = false;
    for (int yy = n - 1; yy <= n; ++yy)
    for (int xx = n - 1; xx <= n; ++xx)
    {
      bool cell_known = B_get(j->known[j->level], xx, yy);
      bool cell_state = B_get(j->state[j->level], xx, yy);
      bool exterior = cell_known && cell_state;
      all_known &= cell_known;
      any_known |= cell_known;
      all_exterior &= exterior;
      any_exterior |= exterior;
    }
    if (all_known && all_exterior)
    {
      return J_EXTERIOR;
    }
    else if (all_known && ! any_exterior)
    {
      return J_INTERIOR;
    }
    else
    {
      return J_UNKNOWN;
    }
  }
  else
  {
    return J_UNKNOWN;
  }
}
