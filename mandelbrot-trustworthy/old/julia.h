#pragma once

#include "interval.h"
#include "bitmap.h"

#define J_UNKNOWN 0
#define J_INTERIOR 1
#define J_EXTERIOR 2

// level 24 would take 64 TB of RAM
#define J_LEVEL_MAX 24

#define J_OFFSET 3
#define J_SIZE 4

struct J_s
{
  int level;
  C_tmp ctmp;
  C bounds;
  C c;
  C z;
  B known[J_LEVEL_MAX];
  B state[J_LEVEL_MAX];
};
typedef struct J_s J[1];
typedef const struct J_s constJ[1];

void J_init(J j, constC c);
void J_clear(J j);
void J_box(constJ j, int level, C z, int x, int y);
void J_unbox(constJ j, int level, C z, int *x0, int *x1, int *y0, int *y1);
bool J_refine(J j);
int J_class(J j);
