#pragma once

#include <stdbool.h>
#include <mpfr.h>

struct R_s
{
  mpfr_t lo, hi;
};
typedef struct R_s R[1];
typedef const struct R_s constR[1];

void R_init(R o, mpfr_prec_t prec);
void R_clear(R o);
mpfr_prec_t R_get_prec(constR a);
void R_set(R o, constR a);
void R_add(R o, constR a, constR b);

struct R_tmp_s
{
  mpfr_t lo, hi, lolo, lohi, hilo, hihi;
};
typedef struct R_tmp_s R_tmp[1];

void R_tmp_init(R_tmp tmp, mpfr_prec_t prec);
void R_tmp_clear(R_tmp tmp);
void R_sub(R o, constR a, constR b, R_tmp tmp);
void R_sqr(R o, constR a, R_tmp tmp);
void R_mul(R o, constR a, constR b, R_tmp tmp);
void R_mul_2exp(R o, constR a, const int b);
bool R_inside(constR a, constR b);
bool R_inside_or_equal(constR a, constR b);
bool R_outside(constR a, constR b);
bool R_finite_p(constR a);

struct C_s
{
  R x, y;
};
typedef struct C_s C[1];
typedef const struct C_s constC[1];

void C_init(C o, mpfr_prec_t prec);
void C_clear(C o);
mpfr_prec_t C_get_prec(constC a);
void C_set(C o, constC a);
void C_add(C o, constC a, constC b);
void C_sub(C o, constC a, constC b, R_tmp tmp);

struct C_tmp_s
{
  R x, y, x2, y2;
  R_tmp r;
};
typedef struct C_tmp_s C_tmp[1];

void C_tmp_init(C_tmp tmp, mpfr_prec_t prec);
void C_tmp_clear(C_tmp tmp);
void C_sqr(C o, constC a, C_tmp tmp);
void C_clamp(C o, constC a, constC b);
bool C_inside(constC a, constC b);
bool C_inside_or_equal(constC a, constC b);
bool C_outside(constC a, constC b);
bool C_finite_p(constC a);
