#include <complex.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "julia.h"

#define M_UNKNOWN 0
#define M_INTERIOR 1
#define M_EXTERIOR 2

#define ITERATIONS 1024
#define LEVELS 6

struct M_s
{
  C z[ITERATIONS];
  C bounds;
  C huge;
  C c;
  C_tmp ctmp;
};
typedef struct M_s M[1];

void M_init(M tmp, mpfr_prec_t prec)
{
  for (int i = 0; i < ITERATIONS; ++i)
  {
    C_init(tmp->z[i], prec);
  }
  C_init(tmp->bounds, prec);
  C_init(tmp->huge, prec);
  C_init(tmp->c, prec);
  C_tmp_init(tmp->ctmp, prec);
  mpfr_set_si(tmp->bounds->x->lo, -2, MPFR_RNDD);
  mpfr_set_si(tmp->bounds->x->hi, 2, MPFR_RNDU);
  mpfr_set_si(tmp->bounds->y->lo, -2, MPFR_RNDD);
  mpfr_set_si(tmp->bounds->y->hi, 2, MPFR_RNDU);
  mpfr_set_si(tmp->huge->x->lo, -65536, MPFR_RNDD);
  mpfr_set_si(tmp->huge->x->hi, 65536, MPFR_RNDU);
  mpfr_set_si(tmp->huge->y->lo, -65536, MPFR_RNDD);
  mpfr_set_si(tmp->huge->y->hi, 65536, MPFR_RNDU);
  mpfr_set_si(tmp->z[0]->x->lo, 0, MPFR_RNDD);
  mpfr_set_si(tmp->z[0]->x->hi, 0, MPFR_RNDU);
  mpfr_set_si(tmp->z[0]->y->lo, 0, MPFR_RNDD);
  mpfr_set_si(tmp->z[0]->y->hi, 0, MPFR_RNDU);
}

void M_clear(M tmp)
{
  C_tmp_clear(tmp->ctmp);
  C_clear(tmp->c);
  C_clear(tmp->huge);
  C_clear(tmp->bounds);
  for (int i = ITERATIONS; i > 0; --i)
  {
    C_clear(tmp->z[i - 1]);
  }
}

int M_classify(M tmp)
{
  int result = 0;
  int i;
  for (i = 1; i < ITERATIONS; ++i)
  {
    C_sqr(tmp->z[i], tmp->z[i-1], tmp->ctmp);
    C_add(tmp->z[i], tmp->z[i], tmp->c);
    if (! C_finite_p(tmp->z[i]))
    {
      result |= 1 << M_UNKNOWN;
      --i;
      break;
    }
    if (! C_inside_or_equal(tmp->z[i], tmp->bounds))
    {
      result |= 1 << M_EXTERIOR;
    }
    if (C_outside(tmp->z[i], tmp->bounds))
    {
      result |= 1 << M_EXTERIOR;
      return result;
    }
  }
  // this seems to work for near-parabolic and super-attracting
  for (int j = i - 1; j > 0; --j)
  {
    if (C_inside(tmp->z[i - 2], tmp->z[j - 1]))
    {
      result |= 1 << M_INTERIOR;
      return result;
    }
  }
  // interior not guaranteed
  result |= 1 << M_UNKNOWN;
  return result;
}

#ifdef USE_ASCII
#define HEIGHT 64
#define WIDTH 128
#define PAR 0.5
#else
#define HEIGHT 1080
#define WIDTH 1920
#define PAR 1
#endif

char palette[2][2][2] = {{{' ','X'},{'I','@'}},{{'?','x'},{'i','#'}}};
char image[HEIGHT+1][WIDTH+1];
unsigned char colours[2][2][2][3] =
{{{{128,128,128}
  ,{128,255,255}}
 ,{{255,255,128}
  ,{0,0,0}}}
,{{{255,0,0}
  ,{0,128,128}}
 ,{{128,128,0}
  ,{0,0,0}}}};
unsigned char rgb[HEIGHT][WIDTH][3];

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  int iterations = 1000;
  mpfr_prec_t prec = 100;
  M m;
  M_init(m, prec);
	double _Complex c0 = -0.75;
	double r = 1.5;
	memset(image, ' ', sizeof(image));
	for (int SUPER = 1; SUPER <= 256; SUPER <<= 1)
	{
		for (int y = 0; y < HEIGHT; ++y)
		{
			for (int x = 0; x < WIDTH; ++x)
			{
        if (y < HEIGHT / 2 || cimag(c0) != 0)
        {
          if (strchr(" ?ix", image[y][x]))
          {
            int p[3] = { 0, 0, 0 };
            for (int j = 0; j < SUPER; ++j)
            {
              for (int i = 0; i < SUPER; ++i)
              {
                double u0 = creal(c0) + r * ((SUPER * x + i + 0.0) / (SUPER * WIDTH) - 0.5) * 2 * WIDTH / HEIGHT * PAR;
                double u1 = creal(c0) + r * ((SUPER * x + i + 1.0) / (SUPER * WIDTH) - 0.5) * 2 * WIDTH / HEIGHT * PAR;
                double v0 = cimag(c0) + r * (0.5 - (SUPER * y + j + 0.0) / (SUPER * HEIGHT)) * 2;
                double v1 = cimag(c0) + r * (0.5 - (SUPER * y + j + 1.0) / (SUPER * HEIGHT)) * 2;
                mpfr_set_d(m->c->x->lo, u0, MPFR_RNDD);
                mpfr_set_d(m->c->x->hi, u1, MPFR_RNDU);
                mpfr_set_d(m->c->y->lo, v0, MPFR_RNDD);
                mpfr_set_d(m->c->y->hi, v1, MPFR_RNDU);
                J julia;
                J_init(julia, m->c);
                int k = 0;
                for (int level = 0; level < LEVELS; ++level)
                {
                  k = J_class(julia);
                  if (k)
                  {
                    break;
                  }
                  if (! J_refine(julia))
                  {
                    break;
                  }
                }
                J_clear(julia);
                p[M_UNKNOWN ] += k == J_UNKNOWN;
                p[M_INTERIOR] += k == J_INTERIOR;
                p[M_EXTERIOR] += k == J_EXTERIOR;
                if (p[M_INTERIOR] && p[M_EXTERIOR])
                  break;
              }
              if (p[M_INTERIOR] && p[M_EXTERIOR])
                break;
            }
            image[y][x] = palette[!!p[M_UNKNOWN]][!!p[M_INTERIOR]][!!p[M_EXTERIOR]];
            for (int i = 0; i < 3; ++i)
              rgb[y][x][i] = colours[!!p[M_UNKNOWN]][!!p[M_INTERIOR]][!!p[M_EXTERIOR]][i];
          }
        }
        else
        {
          image[y][x] = image[HEIGHT-1-y][x];
          for (int i = 0; i < 3; ++i)
            rgb[y][x][i] = rgb[HEIGHT-1-y][x][i];
        }
			}
			image[y][WIDTH] = '\n';
		}
		image[HEIGHT][0] = '\n';
		image[HEIGHT][1] = 0;
#ifdef USE_ASCII
		puts(image);
#else
		printf("P6\n%d %d\n255\n", WIDTH, HEIGHT);
		fwrite(rgb, sizeof(rgb), 1, stdout);
#endif
		fflush(stdout);
	}
	return 0;
}
