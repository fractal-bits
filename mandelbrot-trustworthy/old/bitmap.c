#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bitmap.h"

void B_init(B b, int width, int height)
{
  b->width = width;
  b->height = height;
  b->bytes = sizeof(*b->data) * (((size_t) width * height + B_LIMB_BITS - 1) >> B_LIMB_BITS_SHIFT);
  b->data = malloc(b->bytes);
}

void B_clear(B b)
{
  free(b->data);
}

void B_fill(B b, bool value)
{
  memset(b->data, value ? 0xFF : 0x00, b->bytes);
}

size_t B_index(constB b, int x, int y)
{
  return (size_t) b->width * y + x;
}

B_limb B_mask(constB b, size_t ix)
{
  return (B_limb) 1 << (ix & B_LIMB_BITS_MASK);
}

bool B_get(constB b, int x, int y)
{
  size_t ix = B_index(b, x, y);
  B_limb mask = B_mask(b, ix);
  ix >>= B_LIMB_BITS_SHIFT;
  bool r = !!(b->data[ix] & mask);
//fprintf(stderr, "%d=B[%d]&%x\n", r, ix, mask);
  return r;
}

void B_set(B b, int x, int y, bool value)
{
  size_t ix = B_index(b, x, y);
  B_limb mask = B_mask(b, ix);
  ix >>= B_LIMB_BITS_SHIFT;
//fprintf(stderr, "B[%d]&%x=%d\n", ix, mask, value);
  if (value)
  {
    b->data[ix] |= mask;
  }
  else
  {
    b->data[ix] &= ~mask;
  }
}
