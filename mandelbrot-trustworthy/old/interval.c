#include "interval.h"

void R_init(R o, mpfr_prec_t prec)
{
  mpfr_init2(o->lo, prec);
  mpfr_init2(o->hi, prec);
}

void R_clear(R o)
{
  mpfr_clear(o->lo);
  mpfr_clear(o->hi);
}

mpfr_prec_t R_get_prec(constR a)
{
  return mpfr_get_prec(a->lo);
}

void R_set(R o, constR a)
{
  mpfr_set(o->lo, a->lo, MPFR_RNDD);
  mpfr_set(o->hi, a->hi, MPFR_RNDU);
}

void R_add(R o, constR a, constR b)
{
  mpfr_add(o->lo, a->lo, b->lo, MPFR_RNDD);
  mpfr_add(o->hi, a->hi, b->hi, MPFR_RNDU);
}

void R_tmp_init(R_tmp tmp, mpfr_prec_t prec)
{
  mpfr_init2(tmp->lo, prec);
  mpfr_init2(tmp->hi, prec);
  mpfr_init2(tmp->lolo, prec);
  mpfr_init2(tmp->lohi, prec);
  mpfr_init2(tmp->hilo, prec);
  mpfr_init2(tmp->hihi, prec);
}

void R_tmp_clear(R_tmp tmp)
{
  mpfr_clear(tmp->hihi);
  mpfr_clear(tmp->hilo);
  mpfr_clear(tmp->lohi);
  mpfr_clear(tmp->lolo);
  mpfr_clear(tmp->hi);
  mpfr_clear(tmp->lo);
}

void R_sub(R o, constR a, constR b, R_tmp tmp)
{
  mpfr_sub(tmp->lo, a->lo, b->hi, MPFR_RNDD);
  mpfr_sub(tmp->hi, a->hi, b->lo, MPFR_RNDU);
  mpfr_set(o->lo, tmp->lo, MPFR_RNDD);
  mpfr_set(o->hi, tmp->hi, MPFR_RNDU);
}

void R_sqr(R o, constR a, R_tmp tmp)
{
  if (mpfr_sgn(a->lo) <= 0)
  {
    if (0 <= mpfr_sgn(a->hi))
    {
      mpfr_set_ui(tmp->lo, 0, MPFR_RNDZ);
      if (mpfr_cmpabs(a->lo, a->hi) >= 0)
        mpfr_sqr(tmp->hi, a->lo, MPFR_RNDU);
      else
        mpfr_sqr(tmp->hi, a->hi, MPFR_RNDU);
    }
    else
    {
      mpfr_sqr(tmp->lo, a->hi, MPFR_RNDD);
      mpfr_sqr(tmp->hi, a->lo, MPFR_RNDU);
    }
  }
  else
  {
    mpfr_sqr(tmp->lo, a->lo, MPFR_RNDD);
    mpfr_sqr(tmp->hi, a->hi, MPFR_RNDU);
  }
  mpfr_set(o->lo, tmp->lo, MPFR_RNDD);
  mpfr_set(o->hi, tmp->hi, MPFR_RNDU);
}

void R_mul(R o, constR a, constR b, R_tmp tmp)
{
  mpfr_mul(tmp->lolo, a->lo, b->lo, MPFR_RNDD);
  mpfr_mul(tmp->lohi, a->lo, b->hi, MPFR_RNDD);
  mpfr_mul(tmp->hilo, a->hi, b->lo, MPFR_RNDD);
  mpfr_mul(tmp->hihi, a->hi, b->hi, MPFR_RNDD);
  mpfr_min(tmp->lo, tmp->lolo, tmp->lohi, MPFR_RNDD);
  mpfr_min(tmp->lo, tmp->lo, tmp->hilo, MPFR_RNDD);
  mpfr_min(tmp->lo, tmp->lo, tmp->hihi, MPFR_RNDD);
  mpfr_mul(tmp->lolo, a->lo, b->lo, MPFR_RNDU);
  mpfr_mul(tmp->lohi, a->lo, b->hi, MPFR_RNDU);
  mpfr_mul(tmp->hilo, a->hi, b->lo, MPFR_RNDU);
  mpfr_mul(tmp->hihi, a->hi, b->hi, MPFR_RNDU);
  mpfr_max(tmp->hi, tmp->lolo, tmp->lohi, MPFR_RNDU);
  mpfr_max(tmp->hi, tmp->hi, tmp->hilo, MPFR_RNDU);
  mpfr_max(tmp->hi, tmp->hi, tmp->hihi, MPFR_RNDU);
  mpfr_set(o->lo, tmp->lo, MPFR_RNDD);
  mpfr_set(o->hi, tmp->hi, MPFR_RNDU);
}

void R_mul_2exp(R o, constR a, const int b)
{
  mpfr_mul_2exp(o->lo, a->lo, b, MPFR_RNDD);
  mpfr_mul_2exp(o->hi, a->hi, b, MPFR_RNDU);
}

bool R_inside(constR a, constR b)
{
  return mpfr_less_p(b->lo, a->lo) && mpfr_less_p(a->hi, b->hi);
}

bool R_inside_or_equal(constR a, constR b)
{
  return mpfr_lessequal_p(b->lo, a->lo) && mpfr_lessequal_p(a->hi, b->hi);
}

bool R_outside(constR a, constR b)
{
  return mpfr_less_p(a->hi, b->lo) || mpfr_less_p(b->hi, a->lo);
}

bool R_finite_p(constR a)
{
  return mpfr_number_p(a->lo) && mpfr_number_p(a->hi);
}

void C_init(C o, mpfr_prec_t prec)
{
  R_init(o->x, prec);
  R_init(o->y, prec);
}

void C_clear(C o)
{
  R_clear(o->x);
  R_clear(o->y);
}

mpfr_prec_t C_get_prec(constC a)
{
  return R_get_prec(a->x);
}

void C_set(C o, constC a)
{
  R_set(o->x, a->x);
  R_set(o->y, a->y);
}

void C_add(C o, constC a, constC b)
{
  R_add(o->x, a->x, b->x);
  R_add(o->y, a->y, b->y);
}

void C_sub(C o, constC a, constC b, R_tmp tmp)
{
  R_sub(o->x, a->x, b->x, tmp);
  R_sub(o->y, a->y, b->y, tmp);
}

void C_tmp_init(C_tmp tmp, mpfr_prec_t prec)
{
  R_init(tmp->x, prec);
  R_init(tmp->y, prec);
  R_init(tmp->x2, prec);
  R_init(tmp->y2, prec);
  R_tmp_init(tmp->r, prec);
}

void C_tmp_clear(C_tmp tmp)
{
  R_tmp_clear(tmp->r);
  R_clear(tmp->y2);
  R_clear(tmp->x2);
  R_clear(tmp->y);
  R_clear(tmp->x);
}

void C_sqr(C o, constC a, C_tmp tmp)
{
  R_sqr(tmp->x2, a->x, tmp->r);
  R_sqr(tmp->y2, a->y, tmp->r);
  R_sub(tmp->x, tmp->x2, tmp->y2, tmp->r);
  R_mul(tmp->y, a->x, a->y, tmp->r);
  R_mul_2exp(tmp->y, tmp->y, 1);
  R_set(o->x, tmp->x);
  R_set(o->y, tmp->y);
}

void C_clamp(C o, constC a, constC b)
{
  mpfr_max(o->x->lo, a->x->lo, b->x->lo, MPFR_RNDD);
  mpfr_min(o->x->hi, a->x->hi, b->x->hi, MPFR_RNDU);
  mpfr_max(o->y->lo, a->y->lo, b->y->lo, MPFR_RNDD);
  mpfr_min(o->y->hi, a->y->hi, b->y->hi, MPFR_RNDU);
}

bool C_inside(constC a, constC b)
{
  return R_inside(a->x, b->x) && R_inside(a->y, b->y);
}

bool C_inside_or_equal(constC a, constC b)
{
  return R_inside_or_equal(a->x, b->x) && R_inside_or_equal(a->y, b->y);
}

bool C_outside(constC a, constC b)
{
  return R_outside(a->x, b->x) || R_outside(a->y, b->y);
}

bool C_finite_p(constC a)
{
  return R_finite_p(a->x) && R_finite_p(a->y);
}
