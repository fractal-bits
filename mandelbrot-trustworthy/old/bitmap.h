#pragma once

#include <stdbool.h>

#define B_LIMB_BITS 8
#define B_LIMB_BITS_MASK 7
#define B_LIMB_BITS_SHIFT 3
typedef unsigned char B_limb;

struct B_s
{
  B_limb *data;
  size_t bytes;
  int width;
  int height;
};
typedef struct B_s B[1];
typedef const struct B_s constB[1];

void B_init(B b, int width, int height);
void B_clear(B b);
void B_fill(B b, bool value);
size_t B_index(constB b, int x, int y);
B_limb B_mask(constB b, size_t ix);
bool B_get(constB b, int x, int y);
void B_set(B b, int x, int y, bool value);
