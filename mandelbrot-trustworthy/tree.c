#include <string.h>
#include "tree.h"

struct TREE *TREE_new(int64_t nbranches)
{
  struct TREE *tree = MEMORY_alloc(sizeof(*tree));
  if (! tree)
  {
    return NULL;
  }
  tree->nbranches = nbranches;
  tree->size_size = TREE_size_size_for_tree_size(tree->nbranches);
  if (tree->size_size <= 0)
  {
    MEMORY_free(tree, sizeof(*tree));
    return NULL;
  }
  tree->count = TREE_size_required(tree->nbranches, tree->size_size);
#ifdef TREE_BITPACKED
  tree->data = BITS_alloc(tree->count);
#else
  tree->data = MEMORY_alloc(tree->count);
#endif
  if (! tree->data)
  {
    MEMORY_free(tree, sizeof(*tree));
    return NULL;
  }
  tree->branch_ix = 0;
  return tree;
}

struct TREE *TREE_clone(const struct TREE * restrict tree)
{
  struct TREE *clone = MEMORY_alloc(sizeof(*clone));
  if (! clone)
  {
    return NULL;
  }
  clone->nbranches = tree->nbranches;
  clone->size_size = tree->size_size;
  clone->count = tree->count;
#ifdef TREE_BITPACKED
  clone->data = BITS_alloc(clone->count);
#else
  clone->data = MEMORY_alloc(clone->count);
#endif
  if (! clone->data)
  {
    MEMORY_free(clone, sizeof(*clone));
    return NULL;
  }
  clone->branch_ix = 0;
  int64_t bytes = clone->count;
#ifdef TREE_BITPACKED
  bytes = (bytes + 7) / 8;
#endif
  memcpy(clone->data, tree->data, bytes);
  return clone;
}

void TREE_delete(struct TREE * restrict tree)
{
  assert(tree);
#ifdef TREE_BITPACKED
  BITS_free(tree->data, tree->count);
#else
  MEMORY_free(tree->data, tree->count);
#endif
  MEMORY_free(tree, sizeof(*tree));
}

void TREE_refine(struct TREE * restrict out, int64_t * restrict out_cursorp, struct TREE * restrict in, int64_t * restrict in_cursorp)
{
  int64_t out_cursor = *out_cursorp;
  int64_t in_cursor = *in_cursorp;
  enum TREE_NODE node = TREE_read_node(in, in_cursor);
  in_cursor = TREE_read_node_advance(in, in_cursor);
  switch (node)
  {
    case GREY:
      TREE_write_branch_begin(out, out_cursor);
      out_cursor = TREE_write_branch_begin_advance(out, out_cursor);
      TREE_write_node(out, out_cursor, GREY);
      out_cursor = TREE_write_node_advance(out, out_cursor);
      TREE_write_node(out, out_cursor, GREY);
      out_cursor = TREE_write_node_advance(out, out_cursor);
      TREE_write_node(out, out_cursor, GREY);
      out_cursor = TREE_write_node_advance(out, out_cursor);
      TREE_write_node(out, out_cursor, GREY);
      out_cursor = TREE_write_node_advance(out, out_cursor);
      TREE_write_branch_end(out, out_cursor);
      out_cursor = TREE_write_branch_end_advance(out, out_cursor);
      break;
    case BLACK:
      TREE_write_node(out, out_cursor, BLACK);
      out_cursor = TREE_write_node_advance(out, out_cursor);
      break;
    case WHITE:
      TREE_write_node(out, out_cursor, WHITE);
      out_cursor = TREE_write_node_advance(out, out_cursor);
      break;
    case BRANCH:
      TREE_read_branch_enter(in, in_cursor);
      in_cursor = TREE_read_branch_enter_advance(in, in_cursor);
      TREE_write_branch_begin(out, out_cursor);
      out_cursor = TREE_write_branch_begin_advance(out, out_cursor);
      *out_cursorp = out_cursor;
      *in_cursorp = in_cursor;
      TREE_refine(out, out_cursorp, in, in_cursorp);
      TREE_refine(out, out_cursorp, in, in_cursorp);
      TREE_refine(out, out_cursorp, in, in_cursorp);
      TREE_refine(out, out_cursorp, in, in_cursorp);
      out_cursor = *out_cursorp;
      in_cursor = *in_cursorp;
      TREE_write_branch_end(out, out_cursor);
      out_cursor = TREE_write_branch_end_advance(out, out_cursor);
      TREE_read_branch_leave(in, in_cursor);
      in_cursor = TREE_read_branch_leave_advance(in, in_cursor);
      break;
    default:
      assert(! "valid node");
      break;
  }
  *out_cursorp = out_cursor;
  *in_cursorp = in_cursor;
}

void TREE_prune(struct TREE * restrict out, int64_t * restrict out_cursorp, struct TREE * restrict in, int64_t * restrict in_cursorp, bool prune_grey)
{
  int64_t out_cursor = *out_cursorp;
  int64_t in_cursor = *in_cursorp;
  enum TREE_NODE node = TREE_read_node(in, in_cursor);
  in_cursor = TREE_read_node_advance(in, in_cursor);
  switch (node)
  {
    case GREY:
    case BLACK:
    case WHITE:
    {
      TREE_write_node(out, out_cursor, node);
      out_cursor = TREE_write_node_advance(out, out_cursor);
      break;
    }
    case BRANCH:
    {
      int64_t before = out_cursor;
      TREE_write_branch_begin(out, out_cursor);
      out_cursor = TREE_write_branch_begin_advance(out, out_cursor);
      TREE_read_branch_enter(in, in_cursor);
      in_cursor = TREE_read_branch_enter_advance(in, in_cursor);
      *out_cursorp = out_cursor;
      *in_cursorp = in_cursor;
      TREE_prune(out, out_cursorp, in, in_cursorp, prune_grey);
      TREE_prune(out, out_cursorp, in, in_cursorp, prune_grey);
      TREE_prune(out, out_cursorp, in, in_cursorp, prune_grey);
      TREE_prune(out, out_cursorp, in, in_cursorp, prune_grey);
      out_cursor = *out_cursorp;
      in_cursor = *in_cursorp;
      TREE_read_branch_leave(in, in_cursor);
      in_cursor = TREE_read_branch_leave_advance(in, in_cursor);
      TREE_write_branch_end(out, out_cursor);
      out_cursor = TREE_write_branch_end_advance(out, out_cursor);
      out_cursor = before;
      node = TREE_read_node(out, out_cursor);
      out_cursor = TREE_read_node_advance(out, out_cursor);
      assert(node == BRANCH);
      TREE_read_branch_enter(out, out_cursor);
      out_cursor = TREE_read_branch_enter_advance(out, out_cursor);
      enum TREE_NODE b0 = TREE_read_node(out, out_cursor);
      out_cursor = TREE_read_node_advance(out, out_cursor);
      enum TREE_NODE b1 = TREE_read_node(out, out_cursor);
      out_cursor = TREE_read_node_advance(out, out_cursor);
      enum TREE_NODE b2 = TREE_read_node(out, out_cursor);
      out_cursor = TREE_read_node_advance(out, out_cursor);
      enum TREE_NODE b3 = TREE_read_node(out, out_cursor);
      out_cursor = TREE_read_node_advance(out, out_cursor);
      if (b0 == BLACK && b1 == BLACK && b2 == BLACK && b3 == BLACK)
      {
        TREE_read_branch_leave(out, out_cursor);
        out_cursor = TREE_read_branch_leave_advance(out, out_cursor);
        out_cursor = before;
        TREE_write_node(out, out_cursor, BLACK);
        out_cursor = TREE_write_node_advance(out, out_cursor);
      }
      else if (b0 == WHITE && b1 == WHITE && b2 == WHITE && b3 == WHITE)
      {
        TREE_read_branch_leave(out, out_cursor);
        out_cursor = TREE_read_branch_leave_advance(out, out_cursor);
        out_cursor = before;
        TREE_write_node(out, out_cursor, WHITE);
        out_cursor = TREE_write_node_advance(out, out_cursor);
      }
      else if (prune_grey && b0 == GREY && b1 == GREY && b2 == GREY && b3 == GREY)
      {
        TREE_read_branch_leave(out, out_cursor);
        out_cursor = TREE_read_branch_leave_advance(out, out_cursor);
        out_cursor = before;
        TREE_write_node(out, out_cursor, GREY);
        out_cursor = TREE_write_node_advance(out, out_cursor);
      }
      else
      {
        out_cursor = before;
        TREE_skip(out, out_cursor);
        out_cursor = TREE_skip_advance(out, out_cursor);
        TREE_read_branch_leave(out, out_cursor);
        out_cursor = TREE_read_branch_leave_advance(out, out_cursor);
      }
      break;
    }
    default:
      assert(! "valid node");
      break;
  }
  *out_cursorp = out_cursor;
  *in_cursorp = in_cursor;
}

void TREE_prune_in_place(struct TREE * restrict tree, bool prune_grey)
{
  MEMORY_set_dir(MEMORY_END);
  struct TREE * restrict copy = TREE_clone(tree);
  MEMORY_set_dir(MEMORY_START);
  assert(copy);
  if (! copy)
  {
    abort();
  }
  int64_t copy_cursor = 0;
  int64_t tree_cursor = 0;
  TREE_prune(tree, &tree_cursor, copy, &copy_cursor, prune_grey);
  MEMORY_set_dir(MEMORY_END);
  TREE_delete(copy);
  MEMORY_set_dir(MEMORY_START);
}

void TREE_copy(struct TREE * restrict out, int64_t * restrict out_cursorp, struct TREE * restrict in, int64_t * restrict in_cursorp)
{
  assert(out);
  assert(in);
  int64_t out_cursor = *out_cursorp;
  int64_t in_cursor = *in_cursorp;
  enum TREE_NODE node = TREE_read_node(in, in_cursor);
  in_cursor = TREE_read_node_advance(in, in_cursor);
  switch (node)
  {
    case GREY:
    case BLACK:
    case WHITE:
      TREE_write_node(out, out_cursor, node);
      out_cursor = TREE_write_node_advance(out, out_cursor);
      break;
    case BRANCH:
      TREE_read_branch_enter(in, in_cursor);
      in_cursor = TREE_read_branch_enter_advance(in, in_cursor);
      TREE_write_branch_begin(out, out_cursor);
      out_cursor = TREE_write_branch_begin_advance(out, out_cursor);
      *out_cursorp = out_cursor;
      *in_cursorp = in_cursor;
      TREE_copy(out, out_cursorp, in, in_cursorp);
      TREE_copy(out, out_cursorp, in, in_cursorp);
      TREE_copy(out, out_cursorp, in, in_cursorp);
      TREE_copy(out, out_cursorp, in, in_cursorp);
      out_cursor = *out_cursorp;
      in_cursor = *in_cursorp;
      TREE_write_branch_end(out, out_cursor);
      out_cursor = TREE_write_branch_end_advance(out, out_cursor);
      TREE_read_branch_leave(in, in_cursor);
      in_cursor = TREE_read_branch_leave_advance(in, in_cursor);
      break;
    default:
      assert(! "valid node");
  }
  *out_cursorp = out_cursor;
  *in_cursorp = in_cursor;
}

void TREE_compact(struct TREE * restrict tree)
{
  int in_size = tree->size_size;
  int64_t count[4] = {0,0,0,0};
  TREE_count_nodes(tree, 0, count);
  int out_size = TREE_size_size_for_tree_size(count[BRANCH]);
  assert(out_size <= in_size);
  if (out_size < in_size)
  {
    MEMORY_set_dir(MEMORY_END);
    struct TREE * restrict copy = TREE_clone(tree);
    MEMORY_set_dir(MEMORY_START);
    assert(copy);
    if (! copy)
    {
      abort();
    }
    int64_t copy_cursor = 0;
    int64_t tree_cursor = 0;
    tree->size_size = out_size;
    TREE_copy(tree, &tree_cursor, copy, &copy_cursor);
    MEMORY_set_dir(MEMORY_END);
    TREE_delete(copy);
    MEMORY_set_dir(MEMORY_START);
  }
}

int64_t TREE_count_nodes(struct TREE * restrict tree, int64_t cursor, int64_t * restrict count)
{
  enum TREE_NODE node = TREE_read_node(tree, cursor);
  cursor = TREE_read_node_advance(tree, cursor);
  switch (node)
  {
    case GREY:   ++(count[0]); break;
    case BLACK:  ++(count[1]); break;
    case WHITE:  ++(count[2]); break;
    case BRANCH: ++(count[3]);
      TREE_read_branch_enter(tree, cursor);
      cursor = TREE_read_branch_enter_advance(tree, cursor);
      cursor = TREE_count_nodes(tree, cursor, count);
      cursor = TREE_count_nodes(tree, cursor, count);
      cursor = TREE_count_nodes(tree, cursor, count);
      cursor = TREE_count_nodes(tree, cursor, count);
      TREE_read_branch_leave(tree, cursor);
      cursor = TREE_read_branch_leave_advance(tree, cursor);
      break;
    default:
      assert(! "valid node");
      break;
  }
  return cursor;
}

int64_t TREE_count_area(struct TREE * restrict tree, int64_t cursor, double * restrict area, double current_area)
{
  enum TREE_NODE node = TREE_read_node(tree, cursor);
  cursor = TREE_read_node_advance(tree, cursor);
  switch (node)
  {
    case GREY:   area[0] += current_area; break;
    case BLACK:  area[1] += current_area; break;
    case WHITE:  area[2] += current_area; break;
    case BRANCH:
      current_area *= 0.25;
      TREE_read_branch_enter(tree, cursor);
      cursor = TREE_read_branch_enter_advance(tree, cursor);
      cursor = TREE_count_area(tree, cursor, area, current_area);
      cursor = TREE_count_area(tree, cursor, area, current_area);
      cursor = TREE_count_area(tree, cursor, area, current_area);
      cursor = TREE_count_area(tree, cursor, area, current_area);
      TREE_read_branch_leave(tree, cursor);
      cursor = TREE_read_branch_leave_advance(tree, cursor);
      break;
    default:
      assert(! "valid node");
      break;
  }
  return cursor;
}

// this function only works for targets that are exactly quad tree cells
int64_t TREE_count_area_within(struct TREE * restrict tree, int64_t cursor, struct C bounds, double area_bounds, struct C target, double area_target, double * restrict area)
{
  if (C_outside_or_equal(bounds, target))
  {
    TREE_skip(tree, cursor);
    return TREE_skip_advance(tree, cursor);
  }
  if (C_inside_or_equal(bounds, target))
  {
    return TREE_count_area(tree, cursor, area, area_bounds);
  }
  if (C_inside_or_equal(target, bounds))
  {
    enum TREE_NODE node = TREE_read_node(tree, cursor);
    cursor = TREE_read_node_advance(tree, cursor);
    switch (node)
    {
      case GREY:   area[0] += area_target; break;
      case BLACK:  area[1] += area_target; break;
      case WHITE:  area[2] += area_target; break;
      case BRANCH:
        area_bounds *= 0.25;
        TREE_read_branch_enter(tree, cursor);
        cursor = TREE_read_branch_enter_advance(tree, cursor);
        cursor = TREE_count_area_within(tree, cursor, C_split(bounds, 0, 0), area_bounds, target, area_target, area);
        cursor = TREE_count_area_within(tree, cursor, C_split(bounds, 0, 1), area_bounds, target, area_target, area);
        cursor = TREE_count_area_within(tree, cursor, C_split(bounds, 1, 0), area_bounds, target, area_target, area);
        cursor = TREE_count_area_within(tree, cursor, C_split(bounds, 1, 1), area_bounds, target, area_target, area);
        TREE_read_branch_leave(tree, cursor);
        cursor = TREE_read_branch_leave_advance(tree, cursor);
        break;
    default:
      assert(! "valid node");
      break;
    }
    return cursor;
  }
  assert(! "quad aligned");
  abort();
}

int64_t TREE_dump(struct TREE * restrict tree, int64_t cursor)
{
  enum TREE_NODE node = TREE_read_node(tree, cursor);
  cursor = TREE_read_node_advance(tree, cursor);
  switch (node)
  {
    case GREY: putchar('*'); break;
    case BLACK: putchar('0'); break;
    case WHITE: putchar('1'); break;
    case BRANCH:
      putchar('(');
      TREE_read_branch_enter(tree, cursor);
      cursor = TREE_read_branch_enter_advance(tree, cursor);
      cursor = TREE_dump(tree, cursor);
      cursor = TREE_dump(tree, cursor);
      cursor = TREE_dump(tree, cursor);
      cursor = TREE_dump(tree, cursor);
      TREE_read_branch_leave(tree, cursor);
      cursor = TREE_read_branch_leave_advance(tree, cursor);
      putchar(')');
      break;
    default:
      putchar('?');
      break;
  }
  return cursor;
}

int SVG_rect(FILE * restrict out, struct C bounds, const char *c)
{
  int x = bounds.x.lo;
  int y = bounds.y.lo;
  int w = bounds.x.hi - bounds.x.lo;
  int h = bounds.y.hi - bounds.y.lo;
  return 0 > fprintf(out, "<rect class='%s' x='%d' y='%d' width='%d' height='%d' />", c, x, y, w, h);
}

int64_t TREE_svg(FILE * restrict out, struct TREE * restrict tree, int64_t cursor, struct C bounds, int * restrict error)
{
  enum TREE_NODE node = TREE_read_node(tree, cursor);
  cursor = TREE_read_node_advance(tree, cursor);
  switch (node)
  {
    case GREY:  SVG_rect(out, bounds, "g"); break;
    case BLACK: SVG_rect(out, bounds, "b"); break;
    case WHITE: SVG_rect(out, bounds, "w"); break;
    case BRANCH:
      if (0 > fprintf(out, "<g>"))
      {
        *error = 1;
        return 0;
      }
      TREE_read_branch_enter(tree, cursor);
      cursor = TREE_read_branch_enter_advance(tree, cursor);
      cursor = TREE_svg(out, tree, cursor, C_split(bounds, 0, 0), error);
      if (*error) return 0;
      cursor = TREE_svg(out, tree, cursor, C_split(bounds, 0, 1), error);
      if (*error) return 0;
      cursor = TREE_svg(out, tree, cursor, C_split(bounds, 1, 0), error);
      if (*error) return 0;
      cursor = TREE_svg(out, tree, cursor, C_split(bounds, 1, 1), error);
      if (*error) return 0;
      TREE_read_branch_leave(tree, cursor);
      cursor = TREE_read_branch_leave_advance(tree, cursor);
      if (0 > fprintf(out, "</g>"))
      {
        *error = 1;
        return 0;
      }
  }
  return cursor;
}

int TREE_fwrite_raw(FILE * restrict out, struct TREE *tree)
{
  int64_t cursor = 0;
  TREE_skip(tree, cursor);
  cursor = TREE_skip_advance(tree, cursor);
#ifdef TREE_BITPACKED
  cursor = (cursor + 7) / 8;
#endif
  return 1 != fwrite(tree->data, cursor, 1, out);
}

int TREE_fwrite_svg(FILE * restrict out, struct TREE * restrict tree, int level)
{
  int s = 8 << level;
  double t = 1024.0 / s;
  if (0 > fprintf(out, "<svg width='1024' height='1024'><defs><style>*{stroke:grey}.g{fill:red;}.w{fill:white;}.b{fill:black;}</style></defs><g transform='scale(%.18f,%.18f)'>", t, t))
  {
    return 1;
  }
  struct C svgbounds = { { 0, s }, { 0, s } };
  int error = 0;
  TREE_svg(out, tree, 0, svgbounds, &error);
  if (error)
  {
    return 1;
  }
  if (0 > fprintf(out, "</g></svg>\n"))
  {
    return 1;
  }
  return 0;
}

enum TREE_NODE TREE_classify(struct TREE * restrict tree, int64_t * restrict cursorp, struct C z)
{
  static const struct C zero = { { 0, 0 }, { 0, 0 } };
  int64_t cursor = *cursorp;
  if (C_outside(z, zero))
  {
    TREE_skip(tree, cursor);
    cursor = TREE_skip_advance(tree, cursor);
    *cursorp = cursor;
    return GREY;
  }
  enum TREE_NODE node = TREE_read_node(tree, cursor);
  cursor = TREE_read_node_advance(tree, cursor);
  switch (node)
  {
    case BLACK:
    case WHITE:
    case GREY:
    {
      *cursorp = cursor;
      if (C_inside_or_equal(zero, z))
      {
        return node;
      }
      else
      {
        return GREY;
      }
      break;
    }
    case BRANCH:
    {
      TREE_read_branch_enter(tree, cursor);
      cursor = TREE_read_branch_enter_advance(tree, cursor);
      int count[3] = { 0, 0, 0 };
      *cursorp = cursor;
      for (int y = 0; y < 2; ++y)
      {
        for (int x = 0; x < 2; ++x)
        {
          count[TREE_classify(tree, cursorp, C_split(z, y, x))]++;
        }
      }
      cursor = *cursorp;
      TREE_read_branch_leave(tree, cursor);
      cursor = TREE_read_branch_leave_advance(tree, cursor);
      *cursorp = cursor;
      if (count[BLACK] && ! count[WHITE])
      {
        return BLACK;
      }
      else if (count[WHITE] && ! count[BLACK])
      {
        return WHITE;
      }
      else
      {
        return GREY;
      }
    }
  }
  return GREY;
}

int64_t TREE_read_greys(struct TREE * restrict tree, int64_t cursor, struct C c, int64_t * restrict index, struct C * restrict cs)
{
  enum TREE_NODE node = TREE_read_node(tree, cursor);
  cursor = TREE_read_node_advance(tree, cursor);
  switch (node)
  {
    case BLACK:
    case WHITE:
      break;
    case GREY:
      cs[(*index)++] = c;
      break;
    case BRANCH:
      TREE_read_branch_enter(tree, cursor);
      cursor = TREE_read_branch_enter_advance(tree, cursor);
      cursor = TREE_read_greys(tree, cursor, C_split(c, 0, 0), index, cs);
      cursor = TREE_read_greys(tree, cursor, C_split(c, 0, 1), index, cs);
      cursor = TREE_read_greys(tree, cursor, C_split(c, 1, 0), index, cs);
      cursor = TREE_read_greys(tree, cursor, C_split(c, 1, 1), index, cs);
      TREE_read_branch_leave(tree, cursor);
      cursor = TREE_read_branch_leave_advance(tree, cursor);
      break;
  }
  return cursor;
}

int64_t TREE_write_greys(struct TREE * restrict tree, int64_t cursor, int64_t * restrict index, enum TREE_NODE * restrict nodes)
{
  int64_t before = cursor;
  enum TREE_NODE node = TREE_read_node(tree, cursor);
  cursor = TREE_read_node_advance(tree, cursor);
  switch (node)
  {
    case BLACK:
    case WHITE:
      break;
    case GREY:
      cursor = before;
      TREE_write_node(tree, cursor, nodes[(*index)++]);
      cursor = TREE_write_node_advance(tree, cursor);
      break;
    case BRANCH:
      TREE_read_branch_enter(tree, cursor);
      cursor = TREE_read_branch_enter_advance(tree, cursor);
      cursor = TREE_write_greys(tree, cursor, index, nodes);
      cursor = TREE_write_greys(tree, cursor, index, nodes);
      cursor = TREE_write_greys(tree, cursor, index, nodes);
      cursor = TREE_write_greys(tree, cursor, index, nodes);
      TREE_read_branch_leave(tree, cursor);
      cursor = TREE_read_branch_leave_advance(tree, cursor);
      break;
  }
  return cursor;
}

struct TREE *TREE_refine_destroy(struct TREE * restrict in)
{
  assert(in);
  int64_t count[4] = {0,0,0,0};
  TREE_count_nodes(in, 0, count);
  MEMORY_set_dir(MEMORY_END);
  struct TREE * restrict copy = TREE_clone(in);
  MEMORY_set_dir(MEMORY_START);
  TREE_delete(in);
  if (! copy)
  {
    return NULL;
  }
  struct TREE * restrict out = TREE_new(count[3] + count[0]);
  if (! out)
  {
    return NULL;
  }
  int64_t copy_cursor = 0;
  int64_t out_cursor = 0;
  TREE_refine(out, &out_cursor, copy, &copy_cursor);
  MEMORY_set_dir(MEMORY_END);
  TREE_delete(copy);
  MEMORY_set_dir(MEMORY_START);
  return out;
}

static inline
int max(int a, int b)
{
  return a > b ? a : b;
}

int TREE_depth(struct TREE * restrict tree, int64_t * restrict cursorp, int depth_in)
{
  assert(tree);
  assert(cursor);
  int64_t cursor = *cursorp;
  enum TREE_NODE node = TREE_read_node(tree, cursor);
  cursor = TREE_read_node_advance(tree, cursor);
  switch (node)
  {
    case BLACK:
    case WHITE:
    case GREY:
      *cursorp = cursor;
      return depth_in;
    case BRANCH:
      TREE_read_branch_enter(tree, cursor);
      cursor = TREE_read_branch_enter_advance(tree, cursor);
      *cursorp = cursor;
      int depth_out = depth_in;
      ++depth_in;
      depth_out = max(depth_out, TREE_depth(tree, cursorp, depth_in));
      depth_out = max(depth_out, TREE_depth(tree, cursorp, depth_in));
      depth_out = max(depth_out, TREE_depth(tree, cursorp, depth_in));
      depth_out = max(depth_out, TREE_depth(tree, cursorp, depth_in));
      cursor = *cursorp;
      TREE_read_branch_leave(tree, cursor);
      cursor = TREE_read_branch_leave_advance(tree, cursor);
      *cursorp = cursor;
      return depth_out;
    default:
      assert(! "valid node");
      abort();
  }
}
