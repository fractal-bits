#include <assert.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mandelbrot.h"

const struct C julia_bounds = { { -4, 4 }, { -4, 4 } };

void main_help(FILE *out, const char *program)
{
  fprintf
  (out,
    "usage:\n"
    "\t%s help\n"
    "\t%s mandelbrot create out.M\n"
    "\t%s mandelbrot refine in.M out.M inout.resume level GB threads\n"
    "\t\t0 -> ok; 1 -> error; 2 -> resume; 3 -> increase memory\n"
    "\t%s mandelbrot ppm in.M out.ppm\n"
    "\t%s mandelbrot svg in.M out.svg\n"
    "\t%s mandelbrot area in.M\n"
    "\t%s julia create out.J xlo xhi ylo yhi\n"
    "\t%s julia refine in.J out.J GB\n"
    "\t%s julia ppm in.J out.ppm\n"
    "\t%s julia svg in.J out.svg\n"
    "\t%s julia area in.J\n"
    "\t%s julia classify in.J\n"
    "\t\t0 -> unknown; 1 -> disconnected; 2 -> connected\n"
  , program, program, program, program, program, program
  , program, program, program, program, program, program
  );
}

int main_julia_save(const char *outfile, struct JULIA *julia)
{
  if (! julia)
  {
    return 1 ;
  }
  FILE *out = fopen(outfile, "wb");
  if (! out)
  {
    JULIA_delete(julia);
    return 1;
  }
  int r = JULIA_fwrite_raw(out, julia);
  fclose(out);
  JULIA_delete(julia);
  return r;
}

struct JULIA *main_julia_load(const char *infile)
{
  FILE *in = fopen(infile, "rb");
  if (! in)
  {
    return NULL;
  }
  struct JULIA *julia = JULIA_fread_raw(in);
  fclose(in);
  return julia;
}

int main_julia_create(const char *outfile, struct C c)
{
  int64_t memory_limit = 1024 * 1024; // FIXME
  if (! MEMORY_init(memory_limit))
  {
    return 1;
  }
  int r = main_julia_save(outfile, JULIA_new(c, julia_bounds));
  MEMORY_clear(memory_limit);
  return r;
}

int main_julia_refine(const char *infile, const char *outfile, uint64_t memory_limit)
{
  if (! MEMORY_init(memory_limit))
  {
    return 1;
  }
  struct JULIA *julia = main_julia_load(infile);
  if (! julia)
  {
    MEMORY_clear(memory_limit);
    return 1;
  }
  if (! JULIA_refine(julia))
  {
    JULIA_delete(julia);
    MEMORY_clear(memory_limit);
    return 1;
  }
  int r = main_julia_save(outfile, julia);
  MEMORY_clear(memory_limit);
  return r;
}

int main_julia_ppm(const char *infile, const char *outfile)
{
  int64_t memory_limit = 1.0 * 1024 * 1024 * 1024; // FIXME
  if (! MEMORY_init(memory_limit))
  {
    return 1;
  }
  struct JULIA *julia = main_julia_load(infile);
  if (! julia)
  {
    MEMORY_clear(memory_limit);
    return 1;
  }
  FILE *out = fopen(outfile, "wb");
  if (! out)
  {
    JULIA_delete(julia);
    MEMORY_clear(memory_limit);
    return 1;
  }
  int r = JULIA_fwrite_ppm(out, julia);
  fclose(out);
  JULIA_delete(julia);
  MEMORY_clear(memory_limit);
  return r;
}

int main_julia_svg(const char *infile, const char *outfile)
{
  int64_t memory_limit = 1.0 * 1024 * 1024 * 1024; // FIXME
  if (! MEMORY_init(memory_limit))
  {
    return 1;
  }
  struct JULIA *julia = main_julia_load(infile);
  if (! julia)
  {
    MEMORY_clear(memory_limit);
    return 1;
  }
  FILE *out = fopen(outfile, "wb");
  if (! out)
  {
    JULIA_delete(julia);
    MEMORY_clear(memory_limit);
    return 1;
  }
  int r = JULIA_fwrite_svg(out, julia);
  fclose(out);
  JULIA_delete(julia);
  MEMORY_clear(memory_limit);
  return r;
}

int main_julia_area(const char *infile)
{
  int64_t memory_limit = 1.0 * 1024 * 1024 * 1024; // FIXME
  if (! MEMORY_init(memory_limit))
  {
    return 1;
  }
  struct JULIA *julia = main_julia_load(infile);
  if (! julia)
  {
    MEMORY_clear(memory_limit);
    return 1;
  }
  struct R area = JULIA_area(julia);
  JULIA_delete(julia);
  printf("%.24f %.24f\n", area.lo, area.hi);
  MEMORY_clear(memory_limit);
  return 0;
}

int main_julia_classify(const char *infile)
{
  int64_t memory_limit = 1.0 * 1024 * 1024 * 1024; // FIXME
  if (! MEMORY_init(memory_limit))
  {
    return 0;
  }
  struct JULIA *julia = main_julia_load(infile);
  if (! julia)
  {
    MEMORY_clear(memory_limit);
    return 0;
  }
  enum TREE_NODE k = JULIA_classify(julia);
  JULIA_delete(julia);
  MEMORY_clear(memory_limit);
  switch (k)
  {
    case WHITE: printf("disconnected\n"); return 1;
    case BLACK: printf("connected\n"); return 2;
    default: printf("unknown\n"); return 0;
  }
}

int main_mandelbrot_save(const char *outfile, struct MANDELBROT *mandelbrot)
{
  if (! mandelbrot)
  {
    return 1;
  }
  FILE *out = fopen(outfile, "wb");
  if (! out)
  {
    MANDELBROT_delete(mandelbrot);
    return 1;
  }
  int r = MANDELBROT_fwrite_raw(out, mandelbrot);
  fclose(out);
  MANDELBROT_delete(mandelbrot);
  return r;
}

int main_mandelbrot_create(const char *outfile)
{
  int64_t memory_limit = 1024 * 1024; // FIXME
  if (! MEMORY_init(memory_limit))
  {
    return 1;
  }
  int r = main_mandelbrot_save(outfile, MANDELBROT_new());
  MEMORY_clear(memory_limit);
  return r;
}

struct MANDELBROT *main_mandelbrot_load(const char *infile)
{
  FILE *in = fopen(infile, "rb");
  if (! in)
  {
    return NULL;
  }
  struct MANDELBROT *mandelbrot = MANDELBROT_fread_raw(in);
  fclose(in);
  return mandelbrot;
}

_Atomic bool running;

void main_interrupt(int sig)
{
  (void) sig;
  running = false;
}

int main_mandelbrot_refine(const char *infile, const char *outfile, const char *resume_dir, int level, int64_t memory_limit, int64_t thread_limit)
{
  int64_t my_memory_limit = 1.0 * 1024 * 1024 * 1024; // FIXME
  if (! MEMORY_init(my_memory_limit))
  {
    return 1;
  }
  struct MANDELBROT *mandelbrot = main_mandelbrot_load(infile);
  if (! mandelbrot)
  {
    MEMORY_clear(my_memory_limit);
    return 1;
  }
  if (! MANDELBROT_refine(mandelbrot))
  {
    MANDELBROT_delete(mandelbrot);
    MEMORY_clear(my_memory_limit);
    return 1;
  }
  char progressfile[1000];
  snprintf(progressfile, sizeof(progressfile), "%s/M.resume", resume_dir);
  struct MANDELBROT_PROGRESS *progress = MANDELBROT_PROGRESS_load(progressfile);
  if (! progress)
  {
    progress = MANDELBROT_PROGRESS_new(mandelbrot);
  }
  if (! progress)
  {
    MANDELBROT_delete(mandelbrot);
    MEMORY_clear(my_memory_limit);
    return 1;
  }
  running = true;
  sig_t sigint = signal(SIGINT, main_interrupt);
  sig_t sigterm = signal(SIGTERM, main_interrupt);
  int64_t todo = MANDELBROT_PROGRESS_run(progress, mandelbrot->bounds_julia, level, memory_limit, thread_limit, resume_dir, &running);
  MANDELBROT_PROGRESS_save(progress, progressfile);
  int r;
  if (running)
  {
    if (todo > 0)
    {
      MANDELBROT_PROGRESS_delete(progress);
      MANDELBROT_delete(mandelbrot);
      r = 3;
    }
    else
    {
      MANDELBROT_PROGRESS_finish(progress, mandelbrot);
      MANDELBROT_PROGRESS_delete(progress);
      r = main_mandelbrot_save(outfile, mandelbrot);
    }
  }
  else
  {
    MANDELBROT_PROGRESS_delete(progress);
    MANDELBROT_delete(mandelbrot);
    r = 2;
  }
  signal(SIGTERM, sigterm);
  signal(SIGINT, sigint);
  MEMORY_clear(my_memory_limit);
  return r;
}

int main_mandelbrot_ppm(const char *infile, const char *outfile)
{
  int64_t memory_limit = 1.0 * 1024 * 1024 * 1024; // FIXME
  if (! MEMORY_init(memory_limit))
  {
    return 1;
  }
  struct MANDELBROT *mandelbrot = main_mandelbrot_load(infile);
  if (! mandelbrot)
  {
    MEMORY_clear(memory_limit);
    return 1;
  }
  FILE *out = fopen(outfile, "wb");
  if (! out)
  {
    MANDELBROT_delete(mandelbrot);
    MEMORY_clear(memory_limit);
    return 1;
  }
  int r = MANDELBROT_fwrite_ppm(out, mandelbrot);
  fclose(out);
  MANDELBROT_delete(mandelbrot);
  MEMORY_clear(memory_limit);
  return r;
}

int main_mandelbrot_svg(const char *infile, const char *outfile)
{
  int64_t memory_limit = 1.0 * 1024 * 1024 * 1024; // FIXME
  if (! MEMORY_init(memory_limit))
  {
    return 1;
  }
  struct MANDELBROT *mandelbrot = main_mandelbrot_load(infile);
  if (! mandelbrot)
  {
    MEMORY_clear(memory_limit);
    return 1;
  }
  FILE *out = fopen(outfile, "wb");
  if (! out)
  {
    MANDELBROT_delete(mandelbrot);
    MEMORY_clear(memory_limit);
    return 1;
  }
  int r = MANDELBROT_fwrite_svg(out, mandelbrot);
  fclose(out);
  MANDELBROT_delete(mandelbrot);
  return r;
}

int main_mandelbrot_area(const char *infile)
{
  int64_t memory_limit = 1.0 * 1024 * 1024 * 1024; // FIXME
  if (! MEMORY_init(memory_limit))
  {
    return 1;
  }
  struct MANDELBROT *mandelbrot = main_mandelbrot_load(infile);
  if (! mandelbrot)
  {
    MEMORY_clear(memory_limit);
    return 1;
  }
  struct R area = MANDELBROT_area(mandelbrot);
  MANDELBROT_delete(mandelbrot);
  MEMORY_clear(memory_limit);
  printf("%.24f %.24f\n", area.lo, area.hi);
  return 0;
}

int main(int argc, char **argv)
{
  if (argc >= 2)
  {
    if (strcmp("help", argv[1]) == 0)
    {
      main_help(stdout, argv[0]);
      return 0;
    }
    if (strcmp("julia", argv[1]) == 0)
    {
      if (argc >= 3)
      {
        if (strcmp("create", argv[2]) == 0)
        {
          if (argc == 8)
          {
            return main_julia_create(argv[3], (struct C){ { atof(argv[4]), atof(argv[5]) }, { atof(argv[6]), atof(argv[7]) } });
          }
        }
        if (strcmp("refine", argv[2]) == 0)
        {
          if (argc == 6)
          {
            return main_julia_refine(argv[3], argv[4], atof(argv[5]) * 1024 * 1024 * 1024);
          }
        }
        if (strcmp("ppm", argv[2]) == 0)
        {
          if (argc == 5)
          {
            return main_julia_ppm(argv[3], argv[4]);
          }
        }
        if (strcmp("svg", argv[2]) == 0)
        {
          if (argc == 5)
          {
            return main_julia_svg(argv[3], argv[4]);
          }
        }
        if (strcmp("area", argv[2]) == 0)
        {
          if (argc == 4)
          {
            return main_julia_area(argv[3]);
          }
        }
        if (strcmp("classify", argv[2]) == 0)
        {
          if (argc == 4)
          {
            return main_julia_classify(argv[3]);
          }
        }
      }
    }
    if (strcmp("mandelbrot", argv[1]) == 0)
    {
      if (argc >= 3)
      {
        if (strcmp("create", argv[2]) == 0)
        {
          if (argc == 4)
          {
            return main_mandelbrot_create(argv[3]);
          }
        }
        if (strcmp("refine", argv[2]) == 0)
        {
          if (argc == 9)
          {
            return main_mandelbrot_refine(argv[3], argv[4], argv[5], atoi(argv[6]), atof(argv[7]) * 1024 * 1024 * 1024, atoi(argv[8]));
          }
        }
        if (strcmp("ppm", argv[2]) == 0)
        {
          if (argc == 5)
          {
            return main_mandelbrot_ppm(argv[3], argv[4]);
          }
        }
        if (strcmp("svg", argv[2]) == 0)
        {
          if (argc == 5)
          {
            return main_mandelbrot_svg(argv[3], argv[4]);
          }
        }
        if (strcmp("area", argv[2]) == 0)
        {
          if (argc == 4)
          {
            return main_mandelbrot_area(argv[3]);
          }
        }
      }
    }
  }
  main_help(stderr, argv[0]);
  return 1;
}
