#!/bin/bash
set -e
julia="$(dirname "$(readlink -e "$0")")/trustworthy-julia.sh"
mandelbrot="$(dirname "$(readlink -e "$0")")/trustworthy-mandelbrot.sh"
memory_limit="${1}"
thread_count="${2}"
if [ "x${memory_limit}" = "x" -o "x${thread_count}" = "x" ]
then
  echo "usage: $0 memory-limit-in-GB thread-count"
  exit 1
fi
echo "Testing Mandelbrot set (multi-threaded)"
echo "this will run indefinitely, interrupt with Ctrl-C"
mkdir -p M
pushd M
time "${mandelbrot}" "${memory_limit}" "${thread_count}" 1 4 refineonly
popd
echo "Testing Julia sets (single-threaded)"
mkdir -p J
pushd J
mkdir -p dust
pushd dust
time "${julia}" "${memory_limit}" 1 0 refineonly
popd
mkdir -p circle
pushd circle
time "${julia}" "${memory_limit}" 0 0 refineonly
popd
mkdir -p basilica
pushd basilica
time "${julia}" "${memory_limit}" -1 0 refineonly
popd
mkdir -p cauliflower
pushd cauliflower
time "${julia}" "${memory_limit}" 0.25 0 refineonly
popd
mkdir -p fat-basilica
pushd fat-basilica
time "${julia}" "${memory_limit}" -0.75 0 refineonly
popd
popd
