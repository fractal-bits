#include <threads.h>
#include <unistd.h>
#include "mandelbrot.h"

struct MANDELBROT *MANDELBROT_new(void)
{
  struct MANDELBROT *mandelbrot = MEMORY_alloc(sizeof(*mandelbrot));
  if (! mandelbrot)
  {
    return NULL;
  }
  mandelbrot->tree = TREE_new(1);
  int64_t cursor = 0;
  TREE_write_branch_begin(mandelbrot->tree, cursor);
  cursor = TREE_write_branch_begin_advance(mandelbrot->tree, cursor);
  TREE_write_node(mandelbrot->tree, cursor, GREY);
  cursor = TREE_write_node_advance(mandelbrot->tree, cursor);
  TREE_write_node(mandelbrot->tree, cursor, GREY);
  cursor = TREE_write_node_advance(mandelbrot->tree, cursor);
  TREE_write_node(mandelbrot->tree, cursor, BLACK);
  cursor = TREE_write_node_advance(mandelbrot->tree, cursor);
  TREE_write_node(mandelbrot->tree, cursor, BLACK);
  cursor = TREE_write_node_advance(mandelbrot->tree, cursor);
  TREE_write_branch_end(mandelbrot->tree, cursor);
  cursor = TREE_write_branch_end_advance(mandelbrot->tree, cursor);
  mandelbrot->bounds = (struct C){{-2.75, 1.25},{-2,2}};
  mandelbrot->bounds_julia = (struct C){{-4,4},{-4,4}};
  mandelbrot->f = FUNCTION_quadratic(mandelbrot->bounds_julia);
  return mandelbrot;
}

void MANDELBROT_delete(struct MANDELBROT *mandelbrot)
{
  assert(mandelbrot);
  TREE_delete(mandelbrot->tree);
  MEMORY_free(mandelbrot, sizeof(*mandelbrot));
}

struct MANDELBROT *MANDELBROT_fread_raw(FILE *in)
{
  assert(in);
  struct MANDELBROT *mandelbrot = MEMORY_alloc(sizeof(*mandelbrot));
  if (! mandelbrot)
  {
    return NULL;
  }
  int64_t count;
  if (5 != fscanf
    ( in
    , "Trustworthy Mandelbrot z->z^2+c\n"
      "c = { { %lf , %lf }, { %lf, %lf } };\n"
      "n = %ld\n"
      "binary data follows"
    , &mandelbrot->bounds.x.lo, &mandelbrot->bounds.x.hi
    , &mandelbrot->bounds.y.lo, &mandelbrot->bounds.y.hi
    , &count
    ))
  {
    MEMORY_free(mandelbrot, sizeof(*mandelbrot));
    return NULL;
  }
  if ('\n' != fgetc(in))
  {
    MEMORY_free(mandelbrot, sizeof(*mandelbrot));
    return NULL;
  }
  if ('\f' != fgetc(in))
  {
    MEMORY_free(mandelbrot, sizeof(*mandelbrot));
    return NULL;
  }
  mandelbrot->tree = TREE_fread_raw(in, count);
  if (! mandelbrot->tree)
  {
    MEMORY_free(mandelbrot, sizeof(*mandelbrot));
    return NULL;
  }
  mandelbrot->bounds_julia = (struct C){{-4,4},{-4,4}};
  mandelbrot->f = FUNCTION_quadratic(mandelbrot->bounds_julia);
  return mandelbrot;
}

int MANDELBROT_fwrite_raw(FILE *out, struct MANDELBROT *mandelbrot)
{
  assert(out);
  assert(mandelbrot);
  int64_t count[4] = {0,0,0,0};
  TREE_count_nodes(mandelbrot->tree, 0, count);
  if (0 > fprintf
    ( out
    , "Trustworthy Mandelbrot z->z^2+c\n"
      "c = { { %.24f , %.24f }, { %.24f, %.24f } };\n"
      "n = %ld\n"
      "binary data follows\n\f"
    , mandelbrot->bounds.x.lo, mandelbrot->bounds.x.hi
    , mandelbrot->bounds.y.lo, mandelbrot->bounds.y.hi
    , count[BRANCH]
    ))
  {
    return 1;
  }
  return TREE_fwrite_raw(out, mandelbrot->tree);
}

bool MANDELBROT_refine(struct MANDELBROT *mandelbrot)
{
  mandelbrot->tree = TREE_refine_destroy(mandelbrot->tree);
  return !!mandelbrot->tree;
}

struct MANDELBROT_PROGRESS *MANDELBROT_PROGRESS_new(struct MANDELBROT *mandelbrot)
{
  assert(mandelbrot);
  struct MANDELBROT_PROGRESS *progress;
  if ((progress = MEMORY_alloc(sizeof(*progress))))
  {
    int64_t count[4] = {0,0,0,0};
    TREE_count_nodes(mandelbrot->tree, 0, count);
    progress->count = count[GREY];
    if ((progress->state = MEMORY_alloc(sizeof(*progress->state) * progress->count)))
    {
      if ((progress->c = MEMORY_alloc(sizeof(*progress->c) * progress->count)))
      {
        for (int64_t ix = 0; ix < progress->count; ++ix)
        {
          progress->state[ix] = PROGRESS_TODO;
        }
        int64_t grey_ix = 0;
        TREE_read_greys(mandelbrot->tree, 0, mandelbrot->bounds, &grey_ix, progress->c);
        assert(grey_ix == progress->count);
        return progress;
      }
      MEMORY_free(progress->state, sizeof(*progress->state) * progress->count);
    }
    MEMORY_free(progress, sizeof(*progress));
  }
  return NULL;
}

void MANDELBROT_PROGRESS_delete(struct MANDELBROT_PROGRESS *progress)
{
  assert(progress);
  assert(progress->state);
  assert(progress->c);
  MEMORY_free(progress->c, sizeof(*progress->c) * progress->count);
  MEMORY_free(progress->state, sizeof(*progress->state) * progress->count);
  MEMORY_free(progress, sizeof(*progress));
}

struct MANDELBROT_PROGRESS *MANDELBROT_PROGRESS_load(const char *infile)
{
  struct MANDELBROT_PROGRESS *progress = MEMORY_alloc(sizeof(*progress));
  if (progress)
  {
    FILE *in = fopen(infile, "rb");
    if (in)
    {
      if (1 == fscanf(in, "Trustworthy Mandelbrot z->z^+c\nrefinement progress\ncount = %ld\nbinary data follows", &progress->count))
      {
        if ('\n' == fgetc(in))
        {
          if ('\f' == fgetc(in))
          {
            if ((progress->state = MEMORY_alloc(sizeof(*progress->state) * progress->count)))
            {
              if ((progress->c = MEMORY_alloc(sizeof(*progress->c) * progress->count)))
              {
                if (1 == fread(progress->state, sizeof(*progress->state) * progress->count, 1, in))
                {
                  if (1 == fread(progress->c, sizeof(*progress->c) * progress->count, 1, in))
                  {
  int64_t totals[6] = {0,0,0,0,0,0};
  enum PROGRESS *state = progress->state;
  int64_t count = progress->count;
  for (int64_t ix = 0; ix < count; ++ix)
  {
    totals[state[ix]]++;
  }
  if (totals[PROGRESS_TODO] == 0 && totals[PROGRESS_MEMORY] > 0)
  {
    fprintf(stderr, "retry\n");
    for (int64_t ix = 0; ix < count; ++ix)
    {
      if (state[ix] == PROGRESS_MEMORY)
      {
        state[ix] = PROGRESS_TODO;
      }
    }
  }
                    return progress;
                  }
                }
                MEMORY_free(progress->c, sizeof(*progress->c) * progress->count);
              }
              MEMORY_free(progress->state, sizeof(*progress->state) * progress->count);
            }
          }
        }
      }
      fclose(in);
    }
    MEMORY_free(progress, sizeof(*progress));
  }
  return NULL;
}

int MANDELBROT_PROGRESS_save(struct MANDELBROT_PROGRESS *progress, const char *outfile)
{
  FILE *out = fopen(outfile, "wb");
  if (! out)
  {
    return 1;
  }
  if (0 > fprintf(out, "Trustworthy Mandelbrot z->z^+c\nrefinement progress\ncount = %ld\nbinary data follows\n\f", progress->count))
  {
    fclose(out);
    return 1;
  }
  if (1 != fwrite(progress->state, sizeof(*progress->state) * progress->count, 1, out))
  {
    fclose(out);
    return 1;
  }
  if (1 != fwrite(progress->c, sizeof(*progress->c) * progress->count, 1, out))
  {
    fclose(out);
    return 1;
  }
  fclose(out);
  return 0;
}

int MANDELBROT_PROGRESS_thread(void *data)
{
  assert(data);
  struct MANDELBROT_PROGRESS *progress = data;
  int64_t count = progress->count;
  enum PROGRESS *state = progress->state;
  struct C *cs = progress->c;
  struct C bounds = progress->bounds;
  _Atomic bool *running = progress->running;
  int level = progress->level;
  const char *resume_dir = progress->resume_dir;
  if (MEMORY_init(progress->memory_limit))
  {
    while (*running)
    {
      int64_t ix = progress->started++;
      if (! (0 <= ix && ix < count))
      {
        break;
      }
      enum PROGRESS p = state[ix];
      if (p == PROGRESS_TODO)
      {
        struct C c = cs[ix];
        enum TREE_NODE n = MANDELBROT_estimate(c, ((int64_t) 1) << level);
        if (n == GREY)
        {
          p = PROGRESS_DE;
        }
        else
        {
          enum JULIA_REASON jreason = JULIA_OK;
          char filename[1000];
          snprintf(filename, sizeof(filename), "%s/%ld.J", resume_dir, ix);
          n = JULIA_calculate(filename, c, bounds, level, &jreason);
          if (n == BLACK)
          {
            p = PROGRESS_BLACK;
          }
          else if (n == WHITE)
          {
            p = PROGRESS_WHITE;
          }
          else
          {
            assert(n == GREY);
            switch (jreason)
            {
              case JULIA_LEVEL_LIMIT:
                p = PROGRESS_LEVEL;
                break;
              case JULIA_MEMORY_LIMIT:
                p = PROGRESS_MEMORY;
                break;
              default:
                assert(! "Julia reason understood");
                abort();
            }
          }
        }
        state[ix] = p;
      }
    }
    MEMORY_clear(progress->memory_limit);
  }
  else
  {
    abort();
  }
  progress->completed++;
  return 0;
}

int64_t MANDELBROT_PROGRESS_run(struct MANDELBROT_PROGRESS *progress, struct C bounds_julia, int level, int64_t memory_limit, int64_t thread_limit, const char *resume_dir, _Atomic bool *running)
{
  assert(progress);
  assert(progress->node);
  assert(progress->c);
  progress->memory_limit = memory_limit / thread_limit;
  progress->started = 0;
  progress->completed = 0;
  progress->bounds = bounds_julia;
  progress->level = level;
  progress->resume_dir = resume_dir;
  progress->running = running;
  int64_t count = thread_limit < progress->count ? thread_limit : progress->count;
  thrd_t *threads = MEMORY_alloc(sizeof(*threads) * count);
  if (threads)
  {
    for (int64_t ix = 0; ix < count; ++ix)
    {
      if (thrd_create(&threads[ix], MANDELBROT_PROGRESS_thread, progress) != thrd_success)
      {
        abort(); // FIXME
      }
    }
    int oldpc = -1 - count;
    while (*running && progress->completed < count)
    {
      int64_t ix = progress->started - count;
      int pc = ix * 100 / progress->count;
      if (pc > oldpc)
      {
        oldpc = pc;
        fprintf(stderr, "\t%3d%%\r", pc > 100 ? 100 : pc < 0 ? 0 : pc);
      }
      usleep(100000);
    }
    if (*running)
    {
      fprintf(stderr, "\t%3d%%\r", 100);
    }
    for (int64_t ix = 0; ix < count; ++ix)
    {
      thrd_join(threads[ix], NULL);
    }
    MEMORY_free(threads, sizeof(*threads) * count);
  }
  else
  {
    abort();
  }
  int64_t totals[6] = {0,0,0,0,0,0};
  enum PROGRESS *state = progress->state;
  count = progress->count;
  for (int64_t ix = 0; ix < count; ++ix)
  {
    totals[state[ix]]++;
  }
  PROGRESS_report(stderr, totals);
  return totals[PROGRESS_TODO] + totals[PROGRESS_MEMORY];
}

bool MANDELBROT_PROGRESS_finish(struct MANDELBROT_PROGRESS *progress, struct MANDELBROT *mandelbrot)
{
  assert(progress);
  assert(mandelbrot);
  int64_t grey_ix = 0;
  int64_t count = progress->count;
  enum PROGRESS *state = progress->state;
  enum TREE_NODE *node = (enum TREE_NODE *) state;
  enum TREE_NODE transform[6] = { GREY, GREY, GREY, GREY, GREY, GREY };
  transform[PROGRESS_BLACK] = BLACK;
  transform[PROGRESS_WHITE] = WHITE;
  for (int64_t ix = 0; ix < count; ++ix)
  {
    node[ix] = transform[state[ix]];
  }
  TREE_write_greys(mandelbrot->tree, 0, &grey_ix, node);
  assert(grey_ix == count);
  TREE_prune_in_place(mandelbrot->tree, false);
  TREE_compact(mandelbrot->tree);
  return true;
}

void PROGRESS_report(FILE *out, const int64_t *totals)
{
  fprintf(out, "TODO[%ld] ", totals[PROGRESS_TODO]);
  fprintf(out, "BLACK[%ld] ", totals[PROGRESS_BLACK]);
  fprintf(out, "WHITE[%ld] ", totals[PROGRESS_WHITE]);
  fprintf(out, "DE[%ld] ", totals[PROGRESS_DE]);
  fprintf(out, "LEVEL[%ld] ", totals[PROGRESS_LEVEL]);
  fprintf(out, "MEMORY[%ld]\n", totals[PROGRESS_MEMORY]);
}
