#pragma once

// glibc qsort may malloc O(count * size) heap memory -> OOM
// these sorters work in-place, no extra allocations

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

void bsort(void *data, size_t count, size_t size, int (*cmp)(const void *, const void *));
void rsort(void *data, size_t count, size_t size, uint32_t (*key)(const void *), uint32_t max_key);
