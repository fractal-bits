#pragma once

#define fmax fmax_redirect
#define fmin fmin_redirect
#include <math.h>
#undef fmin
#undef fmax

static inline
double fmax(double a, double b)
{
  return a > b ? a : b;
}

static inline
double fmin(double a, double b)
{
  return a < b ? a : b;
}
