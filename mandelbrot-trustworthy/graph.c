#include <stdio.h>
#include "graph.h"

int EDGE8_cmp_source(const void *a, const void *b)
{
  const struct EDGE8 *x = a;
  const struct EDGE8 *y = b;
  return uint8_cmp(x->source, y->source);
}

int EDGE8_cmp_sink(const void *a, const void *b)
{
  const struct EDGE8 *x = a;
  const struct EDGE8 *y = b;
  return uint8_cmp(x->sink, y->sink);
}

int EDGE16_cmp_source(const void *a, const void *b)
{
  const struct EDGE16 *x = a;
  const struct EDGE16 *y = b;
  return uint16_cmp(x->source, y->source);
}

int EDGE16_cmp_sink(const void *a, const void *b)
{
  const struct EDGE16 *x = a;
  const struct EDGE16 *y = b;
  return uint16_cmp(x->sink, y->sink);
}

int EDGE24_cmp_source(const void *a, const void *b)
{
  const struct EDGE24 *x = a;
  const struct EDGE24 *y = b;
  return uint24_cmpp(&x->source, &y->source);
}

int EDGE24_cmp_sink(const void *a, const void *b)
{
  const struct EDGE24 *x = a;
  const struct EDGE24 *y = b;
  return uint24_cmpp(&x->sink, &y->sink);
}

int EDGE32_cmp_source(const void *a, const void *b)
{
  const struct EDGE32 *x = a;
  const struct EDGE32 *y = b;
  return uint32_cmp(x->source, y->source);
}

int EDGE32_cmp_sink(const void *a, const void *b)
{
  const struct EDGE32 *x = a;
  const struct EDGE32 *y = b;
  return uint32_cmp(x->sink, y->sink);
}

const cmp_t EDGE_cmp_sink[5] = { NULL, EDGE8_cmp_sink, EDGE16_cmp_sink, EDGE24_cmp_sink, EDGE32_cmp_sink };
const cmp_t EDGE_cmp_source[5] = { NULL, EDGE8_cmp_source, EDGE16_cmp_source, EDGE24_cmp_source, EDGE32_cmp_source };

uint32_t EDGE8_key_source(const void *a)
{
  const struct EDGE8 *x = a;
  return x->source;
}

uint32_t EDGE8_key_sink(const void *a)
{
  const struct EDGE8 *x = a;
  return x->sink;
}

uint32_t EDGE16_key_source(const void *a)
{
  const struct EDGE16 *x = a;
  return x->source;
}

uint32_t EDGE16_key_sink(const void *a)
{
  const struct EDGE16 *x = a;
  return x->sink;
}

uint32_t EDGE24_key_source(const void *a)
{
  const struct EDGE24 *x = a;
  return uint32_from_uint24(x->source);
}

uint32_t EDGE24_key_sink(const void *a)
{
  const struct EDGE24 *x = a;
  return uint32_from_uint24(x->sink);
}

uint32_t EDGE32_key_source(const void *a)
{
  const struct EDGE32 *x = a;
  return x->source;
}

uint32_t EDGE32_key_sink(const void *a)
{
  const struct EDGE32 *x = a;
  return x->sink;
}

const radix_t EDGE_key_sink[5] = { NULL, EDGE8_key_sink, EDGE16_key_sink, EDGE24_key_sink, EDGE32_key_sink };
const radix_t EDGE_key_source[5] = { NULL, EDGE8_key_source, EDGE16_key_source, EDGE24_key_source, EDGE32_key_source };

static inline
void GRAPH_visit_push(struct GRAPH * restrict graph, graph_node_t item)
{
  assert(graph);
  assert(graph->stack.stack);
  assert(graph->stack_ix < graph->stack_limit);
  switch (graph->edge_bits)
  {
    case EDGE_BITS_8:  graph->stack.stack8 [graph->stack_ix++] = item; break;
    case EDGE_BITS_16: graph->stack.stack16[graph->stack_ix++] = item; break;
    case EDGE_BITS_24: graph->stack.stack24[graph->stack_ix++] = uint24_from_uint64(item); break;
    case EDGE_BITS_32: graph->stack.stack32[graph->stack_ix++] = item; break;
    default: assert(! "valid edge bits"); break;
  }
}

static inline
bool GRAPH_visit_empty(struct GRAPH * restrict graph)
{
  assert(graph);
  return ! graph->stack_ix;
}

static inline
graph_node_t GRAPH_visit_pop(struct GRAPH * restrict graph)
{
  assert(graph);
  assert(graph->stack.stack);
  assert(0 < graph->stack_ix);
  switch (graph->edge_bits)
  {
    case EDGE_BITS_8:  return graph->stack.stack8 [--graph->stack_ix];
    case EDGE_BITS_16: return graph->stack.stack16[--graph->stack_ix];
    case EDGE_BITS_24: return uint64_from_uint24(graph->stack.stack24[--graph->stack_ix]);
    case EDGE_BITS_32: return graph->stack.stack32[--graph->stack_ix];
    default: assert(! "valid edge bits");
  }
  abort();
}

void GRAPH_visit_pred(struct GRAPH * restrict graph, graph_node_t sink)
{
  assert(graph);
  assert(graph->node_visited);
  GRAPH_visit_push(graph, sink);
  while (! GRAPH_visit_empty(graph))
  {
    sink = GRAPH_visit_pop(graph);
    uint64_t byte = sink >> 3;
    int bit = sink & 7;
    unsigned char mask = 1 << bit;
    if (graph->node_visited[byte] & mask)
    {
      continue;
    }
    graph->node_visited[byte] |= mask;
    graph_edge_t lo, hi;
    GRAPH_preds(graph, sink, &lo, &hi);
    for (graph_edge_t e = lo; e < hi; ++e)
    {
      graph_node_t source = GRAPH_preds_pred(graph, e);
      GRAPH_visit_push(graph, source);
    }
  }
}
