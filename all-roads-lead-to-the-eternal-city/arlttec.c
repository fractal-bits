// All Roads Lead To The Eternal City
// 2021 Claude Heiland-Allen

/*
gcc -std=c99 -Wall -Wextra -pedantic -O3 -march=native -fopenmp -o arlttec arlttec.c -lm
./arlttec | pnmsplit - %d.pgm
# wait until enough (~1000) output frames then interrupt with Ctrl-C
rename s/^/0/ ?.pgm
rename s/^/0/ ??.pgm
rename s/^/0/ ???.pgm
find -maxdepth 1 -iname "*.pgm" | sort -g | tac | cat -n | while read n file ; do ln -s $file arlttec-$(printf %05d $n).pgm ; done
ffmpeg -framerate 30 -r 30 -t 30 -i arlttec-%05d.pgm -pix_fmt yuv420p -profile:v high -level:v 4.1 -crf:v 20 -movflags +faststart arlttec.mp4
*/

#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <omp.h>

int cmp_double(const void *a, const void *b)
{
  const double *p = a;
  const double *q = b;
  double x = *p;
  double y = *q;
  return (x > y) - (y > x);
}

struct pixel
{
  float val;
  int ix;
  int rnd;
};

int cmp_pixel_val(const void *a, const void *b)
{
  const struct pixel *p = a;
  const struct pixel *q = b;
  return (p->val > q->val) - (q->val > p->val);
}

int cmp_pixel_ix(const void *a, const void *b)
{
  const struct pixel *p = a;
  const struct pixel *q = b;
  return (p->ix > q->ix) - (q->ix > p->ix);
}

int cmp_pixel_rnd(const void *a, const void *b)
{
  const struct pixel *p = a;
  const struct pixel *q = b;
  return (p->rnd > q->rnd) - (q->rnd > p->rnd);
}

// crop to circle
void crop(double *grid, int dim, double radius)
{
  int dim1 = dim >> 1;
  int radius2 = ceil(radius * dim1 * radius * dim1);
  for (int j = 0; j < dim; ++j)
  {
    for (int i = 0; i < dim; ++i)
    {
      int k = j * dim + i;
      int dj = j - dim1;
      int di = i - dim1;
      int d2 = di * di + dj * dj;
      if (d2 > radius2)
      {
        grid[k] = 0;
      }
    }
  }
}

uint64_t xorshift(uint64_t *a)
{
  uint64_t x = *a;
	x ^= x << 13;
	x ^= x >> 7;
	x ^= x << 17;
  *a = x;
	return x;
}

double uniform(uint64_t *a)
{
  return xorshift(a) / (double)(~(uint64_t)0);
}

// upscale 2x with stochastic jitter
void upscale2x(double *dst, const double *src, int dim, double spread, uint64_t *prng)
{
  int dim1 = dim >> 1;
  for (int j = 0; j < dim; ++j)
  {
    for (int i = 0; i < dim; ++i)
    {
      int k = j * dim + i;
      int i1 = round((i >> 1) + (uniform(prng) - 0.5) * spread * 2);
      if (i1 < 0)
      {
        i1 = 0;
      }
      if (i1 >= dim1)
      {
        i1 = dim1 - 1;
      }
      int j1 = round((j >> 1) + (uniform(prng) - 0.5) * spread * 2);
      if (j1 < 0)
      {
        j1 = 0;
      }
      if (j1 >= dim1)
      {
        j1 = dim1 - 1;
      }
      int k1 = j1 * dim1 + i1;
      dst[k] = src[k1];
    }
  }
}

// find minimum
void minimum(const double *grid, int dim, int *i0, int *j0)
{
  double mi = 1.0 / 0.0;
  *i0 = dim >> 1;
  *j0 = dim >> 1;
  for (int j = 0; j < dim; ++j)
  {
    for (int i = 0; i < dim; ++i)
    {
      int k = j * dim + i;
      if (grid[k] < mi)
      {
        *i0 = i;
        *j0 = j;
        mi = grid[k];
      }
    }
  }
}

// zoom in to center of image with stochastic jitter
void zoomin(double *dst, const double *src, int dim, double spread, double zoom, double i0, double j0, uint64_t *prng)
{
  for (int j = 0; j < dim; ++j)
  {
    for (int i = 0; i < dim; ++i)
    {
      int i1 = round((i + (uniform(prng) - 0.5) * spread + 0.5 - i0) * zoom + i0);
      int j1 = round((j + (uniform(prng) - 0.5) * spread + 0.5 - j0) * zoom + j0);
      int k1 = j1 * dim + i1;
      int k = j * dim + i;
      dst[k] = src[k1];
    }
  }
}

// separable blur
void blur(double *grid, int dim, double sharpness)
{
  double f = 1 / (1 + sharpness + 1);
  for (int j = 0; j < dim; ++j)
  {
    double g2 = 0;
    double g1 = grid[j * dim + 0];
    for (int i = 1; i <= dim; ++i)
    {
      int i1 = i == dim ? i - 1 : i;
      int k = j * dim + i1;
      int k1 = j * dim + i - 1;
      double g0 = i == dim ? 0 : grid[k];
      grid[k1] = (g2 + sharpness * g1 + g0) * f;
      g2 = g1;
      g1 = g0;
    }
  }
  for (int i = 0; i < dim; ++i)
  {
    double g2 = 0;
    double g1 = grid[i + dim * 0];
    for (int j = 1; j <= dim; ++j)
    {
      int j1 = j == dim ? j - 1 : j;
      int k = j1 * dim + i;
      int k1 = (j - 1) * dim + i;
      int g0 = j == dim ? 0 : grid[k];
      grid[k1] = (g2 + sharpness * g1 + g0) * f;
      g2 = g1;
      g1 = g0;
    }
  }
}

// normalize
void normalize(double *grid, int dim)
{
  double sum = 0;
  for (int k = 0; k < dim * dim; ++k)
  {
    sum += grid[k];
  }
  double factor = (dim * dim) / sum;
  for (int k = 0; k < dim * dim; ++k)
  {
    grid[k] *= factor;
  }
}

// blend with upscaling
void blend2x(double *dst, const double *src, int dim, double mix)
{
  int dim1 = dim >> 1;
  for (int j = 0; j < dim; ++j)
  {
    for (int i = 0; i < dim; ++i)
    {
      int jj = j >> 1;
      int ii = i >> 1;
      int kk = jj * dim1 + ii;
      int k = j * dim + i;
      dst[k] += mix * (src[kk] - dst[k]);
    }
  }
}

// blend
void blend(double *dst, const double *src, int dim, double mix)
{
  for (int j = 0; j < dim; ++j)
  {
    for (int i = 0; i < dim; ++i)
    {
      int k = j * dim + i;
      dst[k] += mix * (src[k] - dst[k]);
    }
  }
}

// tone mapping
void tonemap(unsigned char *pgm, const double *grid, int dim, double *peak, double smoothing)
{
  double ma = 0;
  for (int k = 0; k < dim * dim; ++k)
  {
    ma = grid[k] > ma ? grid[k] : ma;
  }
  double mi = 0;
  for (int k = 0; k < dim * dim; ++k)
  {
    double l = log2(grid[k] / ma);
    if (isnan(l) || isinf(l))
    {
      continue;
    }
    mi = l < mi ? l : mi;
  }
  *peak += smoothing * (mi - *peak);
  for (int k = 0; k < dim * dim; ++k)
  {
    double l = log2(grid[k] / ma) / *peak;
    if (isnan(l) || isinf(l))
    {
      l = 1;
    }
    pgm[k] = fmin(fmax(255 * l, 0), 255);
  }
}

// median filter
void median(unsigned char *pgm, const double *grid, int dim, int hood)
{
  int diameter = 2 * hood + 1;
  for (int j = 0; j < dim; ++j)
  {
    for (int i = 0; i < dim; ++i)
    {
      int sum = 0;
      int count = 0;
      double neighbourhood[diameter * diameter];
      int k = 0;
      for (int dj = -hood; dj <= hood; ++dj)
      {
        for (int di = -hood; di <= hood; ++di)
        {
          int ii = i + di;
          int jj = j + dj;
          if (ii < 0 || dim <= ii)
          {
            continue;
          }
          if (jj < 0 || dim <= jj)
          {
            continue;
          }
          neighbourhood[k++] = grid[jj * dim + ii];
        }
      }
      qsort(neighbourhood, k, sizeof(*neighbourhood), cmp_double);
      double me = grid[j * dim + i];
      for (int l = 0; l < k; ++l)
      {
        if (neighbourhood[l] == me)
        {
          pgm[j * dim + i] = 255 * (l + 0.5) / k;
          break;
        }
      }
    }
  }
}

// histogram equalization
void equalize(unsigned char *pgm, const double *grid, int dim, uint64_t *prng, struct pixel *image)
{
  for (int k = 0; k < dim * dim; ++k)
  {
    image[k].val = grid[k];
    image[k].ix = k;
    image[k].rnd = xorshift(prng);
  }
  qsort(image, dim * dim, sizeof(*image), cmp_pixel_rnd);
  qsort(image, dim * dim, sizeof(*image), cmp_pixel_val);
  for (int k = 0; k < dim * dim; ++k)
  {
    image[k].val = (k + 0.5) / (dim * dim);
  }
  qsort(image, dim * dim, sizeof(*image), cmp_pixel_ix);
  for (int k = 0; k < dim * dim; ++k)
  {
    pgm[k] = 255 * image[k].val;
  }
}

// PGM output
void writeP5(FILE *out, unsigned char *pgm, int dim)
{
  fprintf(out, "P5\n%d %d\n255\n", dim, dim);
  fwrite(pgm, dim * dim, 1, out);
  fflush(out);
}

int main(int argc, char **argv)
{
  int seed = time(0);
  fprintf(stderr, "seed = %d\n", seed);
  double FPS = 30;
  double speed = 10;
  double spread = 1;
  double sharpness = 2;
  double mix = 0;
  double radius = 0.9;
  double threshold = 0.5;
  double crowness = 0.2;
  double linearity = 0.1;
  double smoothing = 0.5;
  int adjacents = 1;
  int diagonals = 0;
  int levels = 10;
  int doblend = 0;

  int neighbourhood = 0;
  int neighbour_i[9];
  int neighbour_j[9];
  for (int dj = -1; dj <= 1; ++dj)
  {
    for (int di = -1; di <= 1; ++di)
    {
      if (di == 0 && dj == 0)
      {
        continue;
      }
      if (di != 0 && dj != 0 && ! diagonals)
      {
        continue;
      }
      if ((di != 0) != (dj != 0) && ! adjacents)
      {
        continue;
      }
      neighbour_i[neighbourhood] = di;
      neighbour_j[neighbourhood] = dj;
      neighbourhood++;
    }
  }

  double zoom = pow(1 / speed, 1 / FPS);
  srand(seed);
  uint64_t prng = (((uint64_t) rand()) << 32) + rand();
  double *grid[2] = { 0, 0 };
  double peak = 0;
  int maxdim = 1 << levels;
  unsigned char *pgm = malloc(maxdim * maxdim);
  struct pixel *image = malloc(sizeof(*image) * maxdim * maxdim);
  for (int frame = -levels; ; ++frame)
  {
    int src = frame & 1;
    int dst = ! src;
    int level = frame < 0 ? frame + levels : levels;
    int dim = 1 << level;
    int dim1 = dim >> 1;
    grid[dst] = malloc(sizeof(*grid[dst]) * dim * dim);
    if (level)
    {
      if (frame <= 0)
      {
        upscale2x(grid[dst], grid[src], dim, spread, &prng);
      }
      else
      {
        int i0 = dim >> 1, j0 = dim >> 1;
        // minimum(grid[dst], dim, &i0, &j0);
        zoomin(grid[dst], grid[src], dim, spread, zoom, i0, j0, &prng);
      }
//      crop(grid[dst], dim, radius);
      blur(grid[dst], dim, sharpness);
      if (doblend)
      {
        if (frame > 0)
        {
          memcpy(grid[src], grid[dst], sizeof(*grid[src]) * dim * dim);
        }
      }
      else
      {
        free(grid[src]);
        grid[src] = 0;
      }
    }
    else
    {
      grid[dst][0] = 1;
    }

    int npaths = sqrt((double) dim * dim * dim);
    int *paths = malloc(sizeof(*paths) * dim * dim * omp_get_max_threads());
    #pragma omp parallel for if (level > 5) schedule(dynamic, 1024)
    for (int p = 0; p < npaths; ++p)
    {
      // seed random number generator
      uint64_t tprng = prng + p + 1;
      tprng += ! tprng;
      for (int i = 0; i < 64; ++i)
      {
        xorshift(&tprng);
      }

      int *path = paths + dim * dim * omp_get_thread_num();
      memset(path, 0, sizeof(*path) * dim * dim);
      int current_i = (int) (dim * uniform(&tprng)) & (dim - 1);
      int current_j = (int) (dim * uniform(&tprng)) & (dim - 1);
      double distance = exp(log(dim) * uniform(&tprng));
      double distance2 = distance * distance;
      int target_i, target_j, d2;
      do
      {
        do
        {
          target_i = current_i + 2 * distance * (uniform(&tprng) - 0.5);
        }
        while (target_i < 0 || dim <= target_i);
        do
        {
          target_j = current_j + 2 * distance * (uniform(&tprng) - 0.5);
        }
        while (target_j < 0 || dim <= target_j);
        int di = target_i - current_i;
        int dj = target_j - current_j;
        d2 = di * di + dj * dj;
      }
      while (d2 > distance2);
      int last_i = current_i;
      int last_j = current_j;
      while (current_i != target_i && current_j != target_j)
      {
        double best = 1.0 / 0.0;
        int best_i = current_i;
        int best_j = current_j;
        for (int d = 0; d < neighbourhood; ++d)
        {
          int di = neighbour_i[d];
          int next_i = current_i + di;
          if (next_i < 0 || dim <= next_i)
          {
            continue;
          }
          int dj = neighbour_j[d];
          int next_j = current_j + dj;
          if (next_j < 0 || dim <= next_j)
          {
            continue;
          }
          int delta_i = target_i - next_i;
          int delta_j = target_j - next_j;
          int delta = delta_i * delta_i + delta_j * delta_j;
          double road = grid[dst][next_j * dim + next_i];
          int linear = (next_i - current_i == current_i - last_i) && (next_j - current_j == current_j - last_j);
          double score = crowness * delta - road + 100000 * path[next_j * dim + next_i] + linearity * linear;
          if (score < best || (score == best && uniform(&tprng) > 0.5))
          {
            best = score;
            best_i = next_i;
            best_j = next_j;
          }
        }
        if (best < 1.0 / 0.0)
        {
          path[best_j * dim + best_i]++;
          last_i = current_i;
          last_j = current_j;
          current_i = best_i;
          current_j = best_j;
        }
        else
        {
          goto giveup;
        }
      }
      for (int k = 0; k < dim * dim; ++k)
      {
        if (path[k] > 0)
        {
//          #pragma omp atomic
          grid[dst][k] += 1;
        }
      }
giveup:
      ;
    }
    free(paths);

//    normalize(grid[dst], dim);
    if (doblend)
    {
      if (frame > 0)
      {
        blend(grid[dst], grid[src], dim, mix);
      }
      else
      {
        if (level)
        {
          blend2x(grid[dst], grid[src], dim, mix);
        }
      }
      free(grid[src]);
      grid[src] = 0;
    }

    if (1)//frame >= 0)
    {
      //tonemap(pgm, grid[dst], dim, &peak, smoothing);
      equalize(pgm, grid[dst], dim, &prng, image);
      //median(pgm, grid[dst], dim, 3);
      writeP5(stdout, pgm, dim);
    }
  }
  free(pgm);
  free(image);
  return 0;
}
