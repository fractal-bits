#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include <samplerate.h>
#include <sndfile.h>

struct fractal1d
{
  double sample_rate;
  uint32_t block_size;
  uint32_t overlap;
  uint32_t block_count;
  uint32_t *index;
  float *mul;
  float *add;
};

struct fractal1d *fractal1d_compress(const uint32_t block_size, const uint32_t overlap, const float *input, const size_t frames, const double sample_rate)
{
  struct fractal1d *F = calloc(1, sizeof(*F));
  F->sample_rate = sample_rate;
  F->block_size = block_size;
  F->overlap = overlap;
  F->block_count = (frames - (overlap - 1) * ((block_size + overlap - 1) / overlap)) / (block_size / overlap);
  F->index = calloc(1, sizeof(*F->index) * F->block_count);
  F->mul = calloc(1, sizeof(*F->mul) * F->block_count);
  F->add = calloc(1, sizeof(*F->add) * F->block_count);
  // downsample
  size_t frames1 = ((size_t) F->block_count + overlap - 1) * (block_size / overlap);
  assert(frames1 <= frames);
  size_t frames2 = (((size_t) F->block_count + 1) / 2 + overlap - 1) * (block_size / overlap);
  SRC_DATA src_data;
  memset(&src_data, 0, sizeof(src_data));
  src_data.data_in = input;
  src_data.data_out = calloc(1, sizeof(float) * frames2);
  src_data.input_frames = frames1;
  src_data.output_frames = frames2;
  src_data.src_ratio = 0.5;
  src_simple(&src_data, SRC_SINC_BEST_QUALITY, 1);
  fprintf(stderr, "ratio %f\n", F->block_count * (sizeof(uint32_t) + sizeof(float) + sizeof(float)) / (double) (frames * sizeof(float)));
  double window[block_size];
  double sw = 0;
  for (uint32_t i = 0; i < block_size; ++i)
  {
    sw += window[i] = overlap == 1 ? 1.0 : 0.5 * (1 - cos(2 * 3.141592653589793 * (i + 0.5) / block_size));
  }
  for (uint32_t dst = 0; dst < F->block_count; ++dst)
  {
    fprintf(stderr, "\t%u / %u\r", dst, F->block_count);
    double best = 1.0 / 0.0;
    for (uint32_t src = 0; src < (F->block_count + 1) / 2; ++src)
    {
      double sx = 0;
      double sy = 0;
      double sxx = 0;
      double sxy = 0;
      for (uint32_t i = 0; i < block_size; ++i)
      {
        double w = window[i];
        double x = src_data.data_out[src * (block_size / overlap) + i];
        double y = src_data.data_in [dst * (block_size / overlap) + i];
        sx += w * x;
        sy += w * y;
        sxx += w * x * x;
        sxy += w * x * y;
      }
      double mul = - (sx * sy - block_size * sw * sxy) / (sx * sx - block_size * sw * sxx);
      double add = - (mul * sx + sy) / block_size;
      double err = 0;
      for (uint32_t i = 0; i < block_size; ++i)
      {
        double w = window[i];
        double x = src_data.data_out[src * (block_size / overlap) + i];
        double y = src_data.data_in [dst * (block_size / overlap) + i];
        double e = y - (mul * x + add);
        err += w * e * e;
      }
      if (err < best)
      {
        best = err;
        F->index[dst] = src;
        F->mul[dst] = mul;
        F->add[dst] = add;
      }
    }
  }
  return F;
}

void fractal1d_save(const char *filename, const struct fractal1d *F)
{
  FILE *out = fopen(filename, "wb");
  uint32_t magic = 0x4631640a; // "F1d\n"
  uint32_t version = 0;
  fwrite(&magic, sizeof(magic), 1, out);
  fwrite(&version, sizeof(version), 1, out);
  fwrite(&F->sample_rate, sizeof(F->sample_rate), 1, out);
  fwrite(&F->block_size, sizeof(F->block_size), 1, out);
  fwrite(&F->overlap, sizeof(F->overlap), 1, out);
  fwrite(&F->block_count, sizeof(F->block_count), 1, out);
  fwrite(F->index, sizeof(*F->index) * F->block_count, 1, out);
  fwrite(F->mul, sizeof(*F->mul) * F->block_count, 1, out);
  fwrite(F->add, sizeof(*F->add) * F->block_count, 1, out);
  fclose(out);
}

struct fractal1d *fractal1d_load(const char *filename)
{
  FILE *in = fopen(filename, "rb");
  uint32_t magic = -1;
  fread(&magic, sizeof(magic), 1, in);
  assert(magic == 0x4631640a);
  uint32_t version = -1;
  fread(&version, sizeof(version), 1, in);
  assert(version == 0);
  struct fractal1d *F = calloc(1, sizeof(*F));
  fread(&F->sample_rate, sizeof(F->sample_rate), 1, in);
  fread(&F->block_size, sizeof(F->block_size), 1, in);
  fread(&F->overlap, sizeof(F->overlap), 1, in);
  fread(&F->block_count, sizeof(F->block_count), 1, in);
  F->index = calloc(1, sizeof(*F->index) * F->block_count);
  F->mul = calloc(1, sizeof(*F->mul) * F->block_count);
  F->add = calloc(1, sizeof(*F->add) * F->block_count);
  fread(F->index, sizeof(*F->index) * F->block_count, 1, in);
  fread(F->mul, sizeof(*F->mul) * F->block_count, 1, in);
  fread(F->add, sizeof(*F->add) * F->block_count, 1, in);
  fclose(in);
  return F;
}

void fractal1d_decompress(const struct fractal1d *F, const char *filename)
{
  // downsample
  size_t frames1 = ((size_t) F->block_count + F->overlap - 1) * (F->block_size / F->overlap);
  size_t frames2 = (((size_t) F->block_count + 1) / 2 + F->overlap - 1) * (F->block_size / F->overlap);
  SRC_DATA src_data;
  memset(&src_data, 0, sizeof(src_data));
  float *in1 = calloc(1, sizeof(float) * frames1);
  float *in2 = calloc(1, sizeof(float) * frames1);
  src_data.data_in = in1;
  src_data.data_out = calloc(1, sizeof(float) * frames2);
  src_data.input_frames = frames1;
  src_data.output_frames = frames2;
  src_data.src_ratio = 0.5;
  double window[F->block_size];
  double sw = 0;
  for (uint32_t i = 0; i < F->block_size; ++i)
  {
    sw += window[i] = F->overlap == 1 ? 1 : 0.5 * (1 - cos(2 * 3.141592653589793 * (i + 0.5) / F->block_size));
  }
  double error = 1.0 / 0.0;
  for (int pass = 0; error > 0; ++pass)
  {
    src_simple(&src_data, SRC_SINC_BEST_QUALITY, 1);
    memset(in2, 0, sizeof(float) * frames1);
    for (uint32_t dst = 0; dst < F->block_count; ++dst)
    {
      int src = F->index[dst];
      double mul = F->mul[dst];
      double add = F->add[dst];
      for (uint32_t i = 0; i < F->block_size; ++i)
      {
        double w = window[i];
        double x = src_data.data_out[src * (F->block_size / F->overlap) + i];
        double y = mul * x + add;
        in2[dst * F->block_size / F->overlap + i] += w * y;
      }
    }
    error = 0;
    for (size_t i = 0; i < frames1; ++i)
    {
      double e = in1[i] - in2[i];
      in1[i] = in2[i];
      error += e * e;
    }
    error /= frames1;
    error = sqrt(error);
    fprintf(stderr, "\tpass %d error %g |\r", pass, error);
  }
  fprintf(stderr, "\nwriting...\n");
  SF_INFO info = { 0, F->sample_rate, 1, SF_FORMAT_WAV | SF_FORMAT_FLOAT, 0, 0 };
  SNDFILE *ofile = sf_open(filename, SFM_WRITE, &info);
  sf_writef_float(ofile, in1, frames1);
  sf_close(ofile);
}

void usage(const char *argv0)
{
  fprintf
    ( stderr
    , "ERROR: usage:\n"
      "\t%s compress input.wav output.f1d [blocksize=16 [overlap=1]]\n"
      "\t%s decompress input.f1d output.wav\n"
      "\t%s resample inplace.f1d newblocksize\n"
    , argv0, argv0, argv0
    );
}

int main(int argc, char **argv)
{
  if (! (argc >= 2))
  {
    usage(argv[0]);
    return 1;
  }
  if (0 == strcmp("compress", argv[1]))
  {
    if (! (argc >= 4))
    {
      usage(argv[0]);
      return 1;
    }
    const char *input = argv[2];
    const char *output = argv[3];
    int block_size = argc > 4 ? atoi(argv[4]) : 16;
    int overlap = argc > 5 ? atoi(argv[5]) : 1;
    fprintf(stderr, "reading...\n");
    SF_INFO info;
    SNDFILE *ifile;
    memset(&info, 0, sizeof(info));
    ifile = sf_open(input, SFM_READ, &info);
    assert(info.channels == 1);
    size_t bytes = sizeof(float) * info.frames;
    float *data = malloc(bytes);
    sf_readf_float(ifile, data, info.frames);
    sf_close(ifile);
    fprintf(stderr, "compressing...\n");
    struct fractal1d *F = fractal1d_compress(block_size, overlap, data, info.frames, info.samplerate);
    fprintf(stderr, "writing...\n");
    fractal1d_save(output, F);
    return 0;
  }
  else if (0 == strcmp("decompress", argv[1]))
  {
    if (! (argc >= 4))
    {
      usage(argv[0]);
      return 1;
    }
    const char *input = argv[2];
    const char *output = argv[3];
    fprintf(stderr, "reading...\n");
    struct fractal1d *F = fractal1d_load(input);
    fprintf(stderr, "decompressing...\n");
    fractal1d_decompress(F, output);
    fprintf(stderr, "done\n");
  }
  else if (0 == strcmp("resample", argv[1]))
  {
    // FIXME really do this in place instead of loading/saving
    if (! (argc >= 4))
    {
      usage(argv[0]);
      return 1;
    }
    const char *inplace = argv[2];
    int block_size = atoi(argv[3]);
    fprintf(stderr, "reading...\n");
    struct fractal1d *F = fractal1d_load(inplace);
    F->sample_rate *= block_size / (double) F->block_size;
    F->block_size = block_size;
    fprintf(stderr, "writing...\n");
    fractal1d_save(inplace, F);
    fprintf(stderr, "done\n");
    return 0;
  }
  else
  {
    usage(argv[0]);
    return 1;
  }
  return 1;
}
