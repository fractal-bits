
/*
Z -> Z^2 + C
Z + z -> (Z + z)^2 + (C + c)
z -> 2Zz + z^2 + c
Z -> (1 + e)Z
z + d -> 2 (1 + e) Z (z + d) + (z + d)^2 + c
d -> d^2 + d (2 Z + 2 Z e + 2 z) + 2 Z z e
d is absolute error in z (forwards)
d / (dz_n/dz_1) is corresponding absolute error in c (backwards)
error metric:
E = log2( d / (dz_n/dz_1) * Zoom * Height )
when E < 0, error is less than a pixel
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <mpfr.h>
#include <mpc.h>

#define Height 512
#define Width 512
unsigned char ppm[Height][Width][3];

int main(int argc, char **argv)
{
  if (argc < 5) return 1;
  mpfr_t Zoom;
  mpfr_init2(Zoom, 53);
  mpfr_set_str(Zoom, argv[3], 10, MPFR_RNDN);
  int prec = 16 + mpfr_get_exp(Zoom);
  mpc_t Center;
  mpc_init2(Center, prec);
  mpfr_set_str(mpc_realref(Center), argv[1], 10, MPFR_RNDN);
  mpfr_set_str(mpc_imagref(Center), argv[2], 10, MPFR_RNDN);
  int Iterations = atoi(argv[4]);
  mpfr_t R;
  mpfr_init2(R, 53);
  mpfr_const_pi(R, MPFR_RNDN);
  mpfr_exp(R, R, MPFR_RNDN);
  #pragma omp parallel for collapse(2) schedule(dynamic, 1)
  for (int j = 0; j < Height; ++j)
  for (int i = 0; i < Width; ++i)
  {
    mpc_t Z, Zz, dz, dc;
    mpfr_t d, aZ, az, aZe;
    mpc_init2(Z, prec);
    mpc_init2(Zz, 24);
    mpc_init2(dc, 24);
    mpc_init2(dz, 24);
    mpfr_init2(d, 24);
    mpfr_init2(aZ, 24);
    mpfr_init2(az, 24);
    mpfr_init2(aZe, 24);
    const int Precisions[] = { 24, 49, 74, 99 };
    int p;
    double de = 0;
    for (p = 0; p < 4; ++p)
    {
      mpc_set(Z, Center, MPC_RNDNN);
      int Precision = Precisions[p];
      mpc_t c, z, ZZz;
      mpc_init2(c, Precision);
      mpc_init2(z, Precision);
      mpc_init2(ZZz, Precision);
      mpc_set_d_d(c, ((i + 0.5) - (Width / 2.0)) / (Height / 2.0), ((Height / 2.0) - (j + 0.5)) / (Height / 2.0), MPC_RNDNN);
      mpfr_div(mpc_realref(c), mpc_realref(c), Zoom, MPC_RNDNN);
      mpfr_div(mpc_imagref(c), mpc_imagref(c), Zoom, MPC_RNDNN);
      mpc_set(z, c, MPC_RNDNN);
      mpc_set_ui_ui(dz, 1, 0, MPC_RNDNN);
      mpc_set_ui_ui(dc, 1, 0, MPC_RNDNN);
      mpfr_set_ui(d, 0, MPFR_RNDN);
      long e = 0;
      int k;
      for (k = 1; k < Iterations; ++k)
      {
        // E = log2( d / (dz_n/dz_1) * Zoom * Height )
        mpc_abs(az, dz, MPFR_RNDN);
        mpfr_div(az, d, az, MPFR_RNDN);
        mpfr_mul(az, az, Zoom, MPFR_RNDN);
        mpfr_mul_d(az, az, Height / 2.0, MPFR_RNDN);
        mpfr_get_d_2exp(&e, az, MPFR_RNDN);
        if (e > 0)
        {
          break;
        }
        // escape test
        mpc_add(Zz, Z, z, MPC_RNDNN);
        mpc_abs(aZ, Zz, MPFR_RNDN);
        if (mpfr_greater_p(aZ, R))
        {
          mpc_abs(aZe, dc, MPFR_RNDN);
          mpfr_div(aZe, aZe, Zoom, MPFR_RNDN);
          mpfr_div_d(aZe, aZe, Height / 2.0, MPFR_RNDN);
          de = mpfr_get_d(aZ, MPFR_RNDN) * log(mpfr_get_d(aZ, MPFR_RNDN)) / mpfr_get_d(aZe, MPFR_RNDN);
          break;
        }
        // rebase
        mpc_abs(az, z, MPFR_RNDN);
        if (mpfr_less_p(aZ, az))
        {
          mpc_set_ui_ui(Z, 0, 0, MPC_RNDNN);
          mpc_set(z, Zz, MPC_RNDNN);
          mpfr_set(az, aZ, MPFR_RNDN);
        }
        // dz = 2 (Z + z) dz
        mpc_mul(dz, dz, Zz, MPC_RNDNN);
        mpfr_mul_2exp(mpc_realref(dz), mpc_realref(dz), 1, MPFR_RNDN);
        mpfr_mul_2exp(mpc_imagref(dz), mpc_imagref(dz), 1, MPFR_RNDN);
        // dc = 2 (Z + z) dc + 1
        mpc_mul(dc, dc, Zz, MPC_RNDNN);
        mpfr_mul_2exp(mpc_realref(dc), mpc_realref(dc), 1, MPFR_RNDN);
        mpfr_mul_2exp(mpc_imagref(dc), mpc_imagref(dc), 1, MPFR_RNDN);
        mpfr_add_ui(mpc_realref(dc), mpc_realref(dc), 1, MPFR_RNDN);
        // d -> d (2 Z + 2 Z e + 2 z + d) + 2 Z z e
        mpc_abs(aZ, Z, MPFR_RNDN);
        mpfr_div_2exp(aZe, aZ, Precision, MPFR_RNDN);
        mpfr_add(aZe, aZ, aZe, MPFR_RNDN);
        mpfr_add(aZe, aZe, az, MPFR_RNDN);
        mpfr_mul_2exp(aZe, aZe, 1, MPFR_RNDN);
        mpfr_add(aZe, aZe, d, MPFR_RNDN);
        mpfr_mul(d, d, aZe, MPFR_RNDN);
        mpfr_mul(aZe, aZ, az, MPFR_RNDN);
        mpfr_div_2exp(aZe, aZe, Precision - 1, MPFR_RNDN);
        mpfr_add(d, d, aZe, MPFR_RNDN);
        // z -> (2 Z + z) z + c
        mpfr_mul_2exp(mpc_realref(ZZz), mpc_realref(Z), 1, MPFR_RNDN);
        mpfr_mul_2exp(mpc_imagref(ZZz), mpc_imagref(Z), 1, MPFR_RNDN);
        mpc_add(ZZz, ZZz, z, MPC_RNDNN);
        mpc_mul(z, ZZz, z, MPC_RNDNN);
        mpc_add(z, z, c, MPC_RNDNN);
        // Z -> Z^2 + C
        mpc_sqr(Z, Z, MPC_RNDNN);
        mpc_add(Z, Z, Center, MPC_RNDNN);
      }
      mpc_clear(c);
      mpc_clear(z);
      mpc_clear(ZZz);
      if (k < Iterations)
      {
        if (e > 0)
        {
          continue;
        }
        else
        {
          break;
        }
      }
      else
      {
        de = 0;
        p = -1;
        break;
      }
    }
    double v = (1 + fmax(0, tanh(1 + log(de)))) / 2;
    unsigned char palette[6][3] =
      { { 0, 0, 0 }
      , { 0, 0, 255 }
      , { 0, 255, 255 }
      , { 0, 255, 0 }
      , { 255, 255, 0 }
      , { 255, 0, 0 }
      };
    ppm[j][i][0] = palette[p + 1][0] * v;
    ppm[j][i][1] = palette[p + 1][1] * v;
    ppm[j][i][2] = palette[p + 1][2] * v;
    mpc_clear(Z);
    mpfr_clear(d);
    mpc_clear(dz);
    mpfr_clear(aZ);
    mpfr_clear(az);
    mpfr_clear(aZe);
    mpc_clear(Zz);
  }
  printf("P6\n%d %d\n255\n", Width, Height);
  fwrite(ppm, sizeof(ppm), 1, stdout);
  return 0;
}
