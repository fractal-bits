#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define W (2560/4)
#define H (1440/4)
#define S 256
#define N 256

uint8_t ppm[H][W][3];

uint64_t xorshift64(uint64_t *a)
{
  uint64_t x = *a;
  x ^= x << 13;
  x ^= x >> 7;
  x ^= x << 17;
  return *a = x;
}

int main(int argc, char **argv)
{
  if (argc < 3)
  {
    return 1;
  }
  uint64_t p[2];
  p[0] = atof(argv[1]) * (1ull << 32) * (1ull << 32);
  p[1] = atof(argv[2]) * (1ull << 32) * (1ull << 32);
  double r = 535.491655524765;
  #pragma omp parallel for schedule(dynamic)
  for (uint64_t y = 0; y < H; ++y)
  {
    double cim = ((y + 0.5) / H - 0.5) * 4;
    for (uint64_t x = 0; x < W; ++x)
    {
      double cre = ((x + 0.5) / W - 0.5) * 4 * W / H;
      uint64_t a = ((x << 32) ^ y) ^ 0x1cedC01dCafeC01a;
      for (uint64_t s = 0; s < 64; ++s)
      {
        xorshift64(&a);
      }
      uint64_t acc[3] = {0,0,0};
      for (uint64_t s = 0; s < S; ++s)
      {
        double re = 0, im = 0;
        for (uint64_t n = 0; n < N; ++n)
        {
          double re2 = re * re;
          double im2 = im * im;
          double z2 = re2 + im2;
          if (z2 > r)
          {
            uint8_t c[3] = { 5 * n, 2 * n, 3 * n };
            acc[0] += c[0];
            acc[1] += c[1];
            acc[2] += c[2];
            break;
          }
          if (xorshift64(&a) <= p[n & 1])
          {
            re = re < 0 ? -re : re;
            im = im < 0 ? -im : im;
          }
          im = 2 * re * im + cim;
          re = re2 - im2 + cre;
        }
      }
      ppm[y][x][0] = acc[0] / S;
      ppm[y][x][1] = acc[1] / S;
      ppm[y][x][2] = acc[2] / S;
    }
  }
  printf("P6\n%d %d\n255\n", W, H);
  fwrite(ppm, sizeof(ppm), 1, stdout);
  return 0;
}
