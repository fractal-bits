import Control.Monad (forM)
import Data.Complex (cis, Complex((:+)))
import Data.List (intercalate)
import Data.Ratio (denominator, numerator)
import System.Environment (getArgs)
import System.IO (hFlush, stdout)
import Data.Vector.Unboxed (foldr', forM_, fromListN, generate, Vector)
import qualified Data.Vector.Unboxed as V

import Laurent (withLaurent)

main :: IO ()
main = do
  [sn, sm, sr, sw, ss] <- getArgs
  n <- readIO sn
  m <- readIO sm
  bs <- withLaurent $ forM [-1 .. m]
  let v = fromListN (fromInteger m + 2) $ map fromRational bs
      c = case sw of
        "equi" -> circle n (1 + 0.5 ** read sr)
        "ray" -> ray n (read (replace '/' '%' sr))
      j = case ss of
        "M" -> V.map (jungreis v) c
        "D" -> c
        "B" -> V.map (:+ 0) v
  forM_ j $ \(x:+y) -> putStrLn (show x ++ " " ++ show y)

replace x with (y:ys) | x == y = with : replace x with ys
                      | otherwise = y : replace x with ys
replace _ _ _ = []

circle :: Int -> Double -> Vector (Complex Double)
circle n r = generate n (\i -> fmap (r *) . cis $ 2 * pi * (fromIntegral i + 0.5) / fromIntegral n)

ray :: Int -> Rational -> Vector (Complex Double)
ray n r = generate n (\i -> fmap ((1 + 0.5 ** (53 * fromIntegral i / fromIntegral n)) *) . cis $ 2 * pi * fromRational r)

horner :: Complex Double -> Vector Double -> Complex Double
horner x = foldr' (\a s -> (a :+ 0) + x * s) 0

jungreis :: Vector Double -> Complex Double -> Complex Double
jungreis u w = w * horner (recip w) u
