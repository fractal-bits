set term pngcairo size 2000,1000 font "LMSans10" fontscale 2
set output "DM.png"
unset key
set grid
set multiplot layout 1,2
set title "Unit disk D with external rays and circles"
plot [-2:2][-2:2] "D-equi.dat" w l, "D-ray.dat" w l
set title "Image under Psi_M(w) truncated to 1000 terms"
plot [-2.5:1.5][-2:2] "M-equi.dat" w l, "M-ray.dat" w l
unset multiplot
