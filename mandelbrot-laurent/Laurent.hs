{-# LANGUAGE OverloadedStrings #-}
module Laurent (withLaurent) where

import Control.Monad (forM)
import Data.Text (Text)
import Data.Memo.Sqlite

betaF_ :: ((Integer, Integer) -> IO Rational) -> ((Integer, Integer) -> IO Rational)
betaF_ betaF (n, m)
  | m == 0 = return 1
  | 0 < n && m < nnn = return 0
  | otherwise = do
      a <- betaF (n + 1, m)
      s <- fmap sum . forM [nnn .. m - nnn] $ \k -> do
        nk <- betaF (n, k)
        nmk <- betaF (n, m - k)
        return $! nk * nmk
      b <- betaF (0, m - nnn)
      return $! (a - s - b) / 2
  where
    nnn = 2 ^ (n + 1) - 1

withLaurent :: ((Integer -> IO Rational) -> IO r) -> IO r
withLaurent act = do
  let Just bft = table "betaF"
  (bclean, betaF) <- memoRec' readShow "betaF.sqlite" bft betaF_
  let b (-1) = return 1
      b m = betaF (0, m + 1)
  r <- act b
  bclean
  return r
