#!/bin/bash
Re="$(dos2unix < $1 | grep ^Re | sed s/^.*\ //)"
Im="$(dos2unix < $1 | grep ^Im | sed s/^.*\ //)"
Zoom="$(dos2unix < $1 | grep ^Zoom | sed s/^.*\ //)"
Iterations="$(dos2unix < $1 | grep ^Iterations | sed s/^.*\ //)"
Period="$(dos2unix < $1 | grep ^Period | sed s/^.*\ //)"
./mandelbrot-perturbation-error $Re $Im $Zoom $Iterations $Period > $1.dat
