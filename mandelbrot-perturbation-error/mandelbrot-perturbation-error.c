#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <mpc.h>

static
inline
long double
cnorml(long double _Complex z)
{
  long double x = creall(z);
  long double y = cimagl(y);
  return x * x + y * y;
}

extern
int
main(int argc, char **argv)
{
  long double Zoom = strtold(argv[3], 0);
  int prec;
  frexpl(Zoom, &prec);
  prec += 53;
  mpc_t C;
  mpc_init2(C, prec);
  mpfr_set_str(mpc_realref(C), argv[1], 10, MPFR_RNDN);
  mpfr_set_str(mpc_imagref(C), argv[2], 10, MPFR_RNDN);
  long long int Iterations = atoll(argv[4]);
  long long int Period = atoll(argv[5]);

  // compute reference orbit and round to low precision
  long double _Complex *Zlo = malloc(sizeof(*Zlo) * (Period + 1));
  mpc_t Zhi;
  mpc_init2(Zhi, prec);
  mpc_set_ldc(Zhi, 0, MPC_RNDNN);
  for (long long int n = 0; n <= Period; ++n)
  {
    Zlo[n] = mpc_get_ldc(Zhi, MPC_RNDNN);
    mpc_sqr(Zhi, Zhi, MPC_RNDNN);
    mpc_add(Zhi, Zhi, C, MPC_RNDNN);
  }
  mpc_clear(Zhi);
  mpc_clear(C);

  // compute accuracy achieved at different zoom levels
  long double accuracy_threshold = 1e-8; // tune to taste
  long double R2 = exp(2 * 3.14159265358979);
  long double _Complex c0[100];
  for (int i = 0; i < 100; ++i)
  {
    c0[i] = exp2l(rand() / (long double) RAND_MAX) * cexpl(I * 2 * 3.14159265358979 * rand() / (long double) RAND_MAX);
  }
  int bits_array[2] = { 24, 53 };
  for (int b = 0; b < 2; ++b)
  {
    int bits = bits_array[b];
    for (int e = 0; e < prec - 53 && e < 1024; ++e)
    {
      long double accuracy[100];
      #pragma omp parallel for
      for (int i = 0; i < 100; ++i)
      {
        long double _Complex c = ldexpl(creall(c0[i]), -e) + I * ldexpl(cimagl(c0[i]), -e);
        long long int m = 0;
        long long int n = 0;
        long double _Complex z = 0;
        long double _Complex dzdc = 0;
        long double dz = 0;
        long double epsc = ldexpl(sqrtl(cnorml(c)), -bits);
        while (n < Iterations)
        {
          long double _Complex Z = Zlo[m];
          long double _Complex Zz = Z + z;
          long double z2 = cnorml(z);
          long double Zz2 = cnorml(Zz);
          if (Zz2 < z2 || m == Period)
          {
            m = 0;
            z = Zz;
            z2 = Zz2;
            Z = Zlo[m];
          }
          if (Zz2 > R2)
          {
            break;
          }
          long double Z1 = sqrtl(cnorml(Z));
          long double epsZ1 = ldexpl(Z1, -bits);
          long double z1 = sqrtl(z2);
          long double epsz1 = ldexpl(z1, -bits);
          dzdc = 2 * Zz * dzdc + 1;
          dz = 2 * (epsZ1 * (z1 + epsz1) + (Z1 + epsZ1 + z1 + epsz1 + dz) * dz + epsc);
          z = (2 * Z + z) * z + c;
          ++n;
          ++m;
        }
        if (n < Iterations)
        {
          accuracy[i] = ldexpl(dz / cabsl(dzdc), e);
        }
        else
        {
          accuracy[i] = -1;
        }
      }
      int j = 0, k = 0;
      for (int i = 0; i < 100; ++i)
      {
        j += (accuracy[i] < accuracy_threshold);
        k += (accuracy[i] < 0);
      }
      long double percentile = 100 * (j - k) / (long double) (100 - k);
      printf("%d %Lf\n", e, percentile);
      fflush(stdout);
    }
    printf("\n\n");
    fflush(stdout);
  }
  return 0;
}
