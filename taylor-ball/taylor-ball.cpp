/*

Taylor ball arithmetic applied to detection of roots in the Mandelbrot set

references:

Rigorous Analysis of Nonlinear Motion in Particle Accelerators
Kyoko Makino
PhD thesis
Department of Physics and Astronomy
Michigan State University
1998

Ball interval arithmetic and the Mandelbrot set
knighty
draft note
<https://drive.google.com/file/d/12elEl6d5f3u-jbgQ6pOgHEeo8DvH0mbk/view>

example:

#!/bin/bash
make
re=-2
im=0
for e in $(seq 12)
do
  echo $e ===
  m-box-period double $re $im 1e-$e 100   # mandelbrot-numerics
  m-ball-period double $re $im 1e-$e 100   # mandelbrot-numerics
  ./taylor-ball $re $im 1e-$e 100
done

*/

#include <cassert>
#include <complex>
#include <cstdlib>
#include <iostream>

typedef double R;
typedef std::complex<R> C;

template <unsigned N>
struct tb
{
  // polynomial part
  C P[N+1];
  // order bound intervals and remainder bound (I[N+1])
  // ball interval radii, centered on P[0]
  R I[N+2];
};

tb<0> order0(const C &c, const R &I = 0)
{
  tb<0> r;
  r.P[0] = c;
  r.I[0] = 0;
  r.I[1] = I;
  return r;
}

tb<1> order1(const C &c, const R &I = 0)
{
  tb<1> r;
  r.P[0] = c;
  r.P[1] = 1;
  r.I[0] = 0;
  r.I[1] = 0;
  r.I[2] = I * I;
  return r;
}

template <unsigned N>
tb<N - 1> D(const tb<N> &t)
{
  tb<N - 1> r;
  for (unsigned i = 0; i < N; ++i)
  {
    r.P[i] = R(i + 1) * t.P[i + 1];
  }
  for (unsigned i = 0; i < N + 1; ++i)
  {
    r.I[i] = R(i + 1) * t.I[i + 1]; // FIXME check this
  }
  return r;
}

template <unsigned N>
C center(const tb<N> &t)
{
  return t.P[0];
}

template <unsigned N>
R radius(const tb<N> &t, const R &r)
{
  R d = 0;
  for (unsigned i = N; i > 0; --i)
  {
    d += std::abs(t.P[i]);
    d *= r;
  }
  return d + t.I[N + 1];
}

template <unsigned N>
R inf(const tb<N> &t, const R &r)
{
  return std::abs(center(t)) - radius(t, r);
}

template <unsigned N>
R sup(const tb<N> &t, const R &r)
{
  return std::abs(center(t)) + radius(t, r);
}

template <unsigned M, unsigned N>
tb<N> truncate(const tb<M> &t)
{
  tb<N> r;
  for (unsigned i = 0; i < N + 1; ++i)
  {
    r.P[i] = t.P[i];
  }
  for (unsigned i = 0; i < N + 1; ++i)
  {
    r.I[i] = t.I[i];
  }
  r.I[N + 1] = 0;
  for (unsigned i = N + 1; i < M + 2; ++i)
  {
    r.I[N + 1] += t.I[i];
  }
  return r;
}

template <unsigned N>
tb<N> operator+(const tb<N> &f, const tb<N> &g)
{
  tb<N> r;
  for (unsigned i = 0; i < N + 1; ++i)
  {
    r.P[i] = f.P[i] + g.P[i];
  }
  for (unsigned i = 0; i < N + 2; ++i)
  {
    r.I[i] = f.I[i] + g.I[i];
  }
  return r;
}

template <unsigned N>
tb<N> operator-(const tb<N> &f)
{
  tb<N> r;
  for (unsigned i = 0; i < N + 1; ++i)
  {
    r.P[i] = -f.P[i];
  }
  for (unsigned i = 0; i < N + 2; ++i)
  {
    r.I[i] = f.I[i];
  }
  return r;
}

template <unsigned N>
tb<N> operator-(const tb<N> &f, const tb<N> &g)
{
  tb<N> r;
  for (unsigned i = 0; i < N + 1; ++i)
  {
    r.P[i] = f.P[i] - g.P[i];
  }
  for (unsigned i = 0; i < N + 2; ++i)
  {
    r.I[i] = f.I[i] + g.I[i];
  }
  return r;
}

template <unsigned N>
tb<N> operator*(const tb<N> &f, const tb<N> &g)
{
  tb<N> r;
  for (unsigned i = 0; i < N + 1; ++i)
  {
    r.P[i] = 0;
    for (unsigned j = 0; j <= i; ++j)
    {
      r.P[i] += f.P[j] * g.P[i - j];
    }
  }
  for (unsigned i = 0; i < N + 1; ++i)
  {
    r.I[i] = 0;
    for (unsigned j = 0; j <= i; ++j)
    {
      r.I[i] += std::abs(f.P[0]) * g.I[i - j]
             +  std::abs(g.P[0]) * f.I[j]
             +  f.I[j] * g.I[i - j];
    }
  }
  // equation 5.3
  R If = 0;
  R Ig = 0;
  R Sf = 0;
  R Sg = 0;
  for (unsigned j1 = N + 2; j1 > 0; --j1)
  {
    unsigned j = j1 - 1;
    unsigned i = N + 1 - j;
    Sf += f.I[j];
    Sg += g.I[j];
    If += std::abs(f.P[0]) * Sg
       +  std::abs(g.P[0]) * f.I[i]
       +  f.I[i] * Sg;
    Ig += std::abs(g.P[0]) * Sf
       +  std::abs(f.P[0]) * g.I[i]
       +  g.I[i] * Sf;
  }
  r.I[N + 1] = std::min(If, Ig);
  return r;
}

template <unsigned N>
tb<N> inv(const tb<N> &t)
{ // <https://mathoverflow.net/a/53402>
  tb<N> r;
  r.P[0] = 1 / t.P[0];
  for (int i = 1; i < N + 1; ++i)
  {
    r.P[i] = 0;
    for (int j = 0; j < i; ++j)
    {
      r.P[i] -= r.P[j] * t.P[i - j];
    }
    r.P[i] *= r.P[0];
  }
  assert(! "inv bounds implemented"); // FIXME TODO
  return r;
}

int main(int argc, char **argv)
{
  if (argc != 5)
  {
    std::cerr << "usage: " << argv[0] << " real imag radius maxperiod" << std::endl;
    return 1;
  }
  auto r = std::atof(argv[3]);
  auto c = order1(C(std::atof(argv[1]), std::atof(argv[2])), r);
  auto z = c;
  int maxperiod = std::atoi(argv[4]);
  for (int i = 1; i <= maxperiod; ++i)
  {
    if (inf(z, r) > 2)
    {
      // escaped
      break;
    }
    if (inf(z, r) < 0)
    {
      // found root
      bool nrok = std::abs(center(z)) < inf(D(z), r) * r;
      std::cout << i << "\t" << nrok << std::endl;
      return 0;
    }
    z = z * z + c;
  }
  return 1;
}
