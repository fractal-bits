{-
Claude Heiland-Allen
mathr.co.uk

translated from C code by

Adam Majewski
fraktal.republika.pl

-}

{-# LANGUAGE BangPatterns, MagicHash #-}
module Main (main) where

import GHC.Prim ((-#), (==#), (<##), (-##), (+##), (*##))
import GHC.Types (Int(..), Double(..))
import GHC.Conc (getNumCapabilities)
import Control.Parallel.Strategies (parBuffer, rseq, using)
import qualified Data.ByteString as BSS
import qualified Data.ByteString.Lazy as BS
import qualified Data.ByteString.Lazy.Char8 as BSC
import Data.Complex (Complex((:+)), magnitude)
import Data.Word (Word8)
import System.IO (stdout, stderr, hPutStrLn)
import System.Environment (getArgs)
import System.Exit (exitFailure)

type B = Word8
type N = Int
type R = Double
type C = Complex R

width :: N
width  = 500

height :: N
height = 500

maxIters :: N
maxIters = 100000

zSize :: C
zSize = 1.5

lambda :: R
lambda = 1.5

pixelSize :: R
pixelSize = magnitude (zSize / fromIntegral width)

lambdaPixel2 :: R
lambdaPixel2 = lambda * lambda * pixelSize * pixelSize

er :: R
er = 2

er2 :: R
er2 = er * er

alfa :: C -> C
alfa (cx :+ cy) = ax :+ ay
  where
    dx = 1 - 4 * cx
    dy = -4 * abs cy
    d = dx :+ dy
    r = magnitude d
    sx = sqrt ((r + dx) / 2)
    sy = sqrt ((r - dx) / 2)
    ax = 0.5 - sx / 2
    ay = signum cy * sy / 2

coord :: N -> N -> C
coord j i =
  zSize *   (fromIntegral i / fromIntegral width
  :+ negate (fromIntegral j / fromIntegral height))

data Pixel = Interior | Boundary | Exterior

colour :: Pixel -> B
colour Interior = 192
colour Boundary = 0
colour Exterior = 255

pixel :: C -> C -> C -> Pixel
pixel (D# ax :+ D# ay) (D# cx :+ D# cy) (D# x :+ D# y) = go maxIters' 1.0## 0.0## x y
  where
    !(I# maxIters') = maxIters
    !(D# lambdaPixel2') = lambdaPixel2
    !(D# er2') = er2
    go !n !dzx !dzy !zx !zy
      | n      ==# 0#  = Interior
      | er2'   <## z2  = Exterior
      | 1e60## <## dz2 = Interior
      | d2     <## r2  = Boundary
      | otherwise  = go (n -# 1#) dzx' dzy' zx' zy'
      where
        dzx' = 2.0## *## ((zx *## dzx) -## (zy *## dzy))
        dzy' = 2.0## *## ((zx *## dzy) +## (zy *## dzx))
        zx'  = (zx2 -## zy2) +## cx
        zy'  = (2.0## *## (zx *## zy)) +## cy
        zx2 = zx *## zx
        zy2 = zy *## zy
        z2  = zx2 +## zy2
        dz2 = (dzx *## dzx) +## (dzy *## dzy)
        azx = ax -## zx
        azy = ay -## zy
        d2  = (azx *## azx) +## (azy *## azy)
        r2  = lambdaPixel2' *## dz2

raster :: N -> C -> BS.ByteString
raster cores c = bulk `BS.append` axis `BS.append` BS.reverse bulk
  where
    bs = ((-height, -width), (-1, width))
    cs = ((0, -width), (0, width))
    bulk = render bs
    axis = render cs
    a = alfa c
    go = colour . pixel a c . uncurry coord
    render = BS.fromChunks . mapP cores (BSS.pack . map go) . ranges

ranges :: ((N, N), (N, N)) -> [[(N, N)]]
ranges ((ly, lx), (hy, hx)) = [[(y, x) | x <- [y + lx - y .. hx]] | y <- [ly .. hy]]

mapP :: N -> (a -> b) -> [a] -> [b]
mapP cores f xs = map f xs `using` parBuffer cores rseq

main' :: N -> C -> BS.ByteString
main' cores c = pgm `BS.append` raster cores c

pgm :: BS.ByteString
pgm = BSC.pack $ "P5\n" ++ show (2 * width + 1) ++ " " ++ show (2 * height + 1) ++ "\n255\n"

main :: IO ()
main = do
  cores <- getNumCapabilities
  args <- getArgs
  case args of
    [sx, sy] -> BS.hPut stdout (main' cores (read sx :+ read sy))
    _ -> hPutStrLn stderr "usage: ./pjulia cx cy > out.pgm" >> exitFailure
