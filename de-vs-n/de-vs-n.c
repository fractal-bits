#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <mandelbrot-numerics.h>

typedef int Z;
typedef double R;
typedef double _Complex C;

R cnorm(C z)
{
  R x = creal(z);
  R y = cimag(z);
  return x * x + y * y;
}

Z histogram[24 * 16][24 * 16];

unsigned char pgm[24 * 16][24 * 16][3];

void coords(R de, R mu, Z *i, Z *j)
{
        *i = fmin(fmax(-16 * log2(de), 0), 24 * 16 - 1);
        *j = fmin(fmax( 16 * log2(mu), 0), 24 * 16 - 1);
}

int iterate(C c, R *de, R *mu)
{
    C z = c;
    C dc = 1;
    R minz2 = 1.0/0.0;
    for (Z n = 1; n < 1 << 24; ++n)
    {
      R z2 = cnorm(z);
      if (z2 > 65536)
      {
        *de = 0.5 * sqrt(z2) * log(z2) / cabs(dc);
        *mu = n + 2 - log2(log(z2));
        return 1;
        break;
      }
      if (z2 < minz2)
      {
        if (16 * z2 < minz2)
        {
          C w = z, dw;
          for (Z s = 0; s < 8; ++s)
          {
            C w0 = w;
            dw = 1;
          for (Z m = 0; m < n; ++m)
          {
            dw = 2 * w * dw;
            w = w * w + c;
          }
          w = w0 - (w - w0) / (dw - 1);
          }
          if (cnorm(dw) <= 1) break;
        }
        minz2 = z2;
      }
      dc = 2 * z * dc + 1;
      z = z * z + c;
    }
    return 0;
}

int main(int argc, char **argv)
{
  for (Z sample = 0; sample < 1 << 16; ++ sample)
  {
    C c = (rand() + I * rand()) / (R) RAND_MAX * 4 - (2 + I * 2);
    R de, mu;
    if (iterate(c, &de, &mu))
    {
      Z i, j;
      coords(de, mu, &i, &j);
      histogram[i][j]++;
    }
/*
    Z pc = 100 * sample / (1 << 16);
    Z pc1 = 100 * (sample - 1) / (1 << 16);
    if (pc != pc1)
      fprintf(stderr, "%4d%%\r", pc);
*/
  }
  Z total = 0;
  for (Z i = 0; i < 24 * 16; ++i)
  for (Z j = 0; j < 24 * 16; ++j)
  {
    total += histogram[i][j];
  }
  R scale = 24 * 16 * 24 * 16 / (R) total;
  for (Z i = 0; i < 24 * 16; ++i)
  for (Z j = 0; j < 24 * 16; ++j)
  {
    pgm[i][j][2] = 255 * (histogram[i][j] > 0);//fmin(fmax(64 * log1p(log1p(histogram[i][j] * scale)), 0), 255);;
  }
  for (Z e = 0; e < 24 * 16 * 4; ++e)
  {
    R eps = pow(0.5, e / 32.0);
    R de, mu;
    if (iterate(-2-eps, &de, &mu))
    {
      Z i, j;
      coords(de, mu, &i, &j);
      fprintf(stderr, "%g %g\n", de, mu);
      pgm[i][j][1] = 255;
    }
/*
    if (iterate(0.25 + eps, &de, &mu))
    {
      Z i, j;
      coords(de, mu, &i, &j);
      pgm[i][j][0] = 255;
      pgm[i][j][1] = 255;
    }
*/
  }
  fprintf(stderr, "\n\n");
  mpq_t q;
  mpq_init(q);
  mpq_set_ui(q, 1, 3);
  m_d_exray_in *ray = m_d_exray_in_new(q, 8);
  for (Z e = 0; e < 8 * 1024 * 4; ++e)
  {
    m_d_exray_in_step(ray, 64);
    R de, mu;
    if (iterate(m_d_exray_in_get(ray), &de, &mu))
    {
      Z i, j;
      coords(de, mu, &i, &j);
      if ((e & (e - 1)) == 0)
      {
        fprintf(stderr, "%g %g\n", de, mu);
      }
      pgm[i][j][0] = 255;
    }
  }
  m_d_exray_in_delete(ray);
  mpq_clear(q);
  printf("P6\n%d %d\n 255\n", 24 * 16, 24 * 16);
  fwrite(pgm, sizeof(pgm), 1, stdout);
}
