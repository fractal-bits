#include <assert.h>
#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

typedef unsigned long int N;
typedef double R;
typedef double complex C;

struct
{
  R box_size;
  R box_offset;
  R distance_hi;   // de > dhi => box disjoint dM
  R distance_lo;   // de < dlo => box intersects dM
  R iteration_hi;  // mu > Nhi => de < dlo
  R iteration_lo;  // mu < Nlo => de > dhi
  R period_hi;     // p  > phi => de < dlo
  N maximum_iterations;
  N exterior_result[2][2];
  N interior_result[2][2];
  N guaranteed_exterior;
  N guaranteed_interior;
  N maximum_iterations_reached;
} status[64];

void initialize(void)
{
  //randomize(seed);
  memset(status, 0, sizeof(status));
  for (N level = 0; level < 64; ++level)
  {
    status[level].box_size = 2.0 * pow(0.5, level);
    status[level].box_offset = 0.5 * status[level].box_size;;
    status[level].distance_hi = status[level].box_size * 4.0 * sqrt(2.0);
    status[level].distance_lo = status[level].box_size / 4.0;
    status[level].iteration_hi = 2.129 / sqrt(status[level].distance_lo);
    status[level].iteration_lo = log(48 / status[level].distance_hi) / log(4);
    status[level].period_hi = 2 / sqrt(status[level].distance_lo); // ?
    status[level].maximum_iterations = 2100100100;
  }
}

void report(N levels)
{
  printf("# level\ttotal\tint\tbdry\text\tdim\text00\text01\text10\text11\tint00\tint01\tint10\tint11\tmaxn\n");
  N interior = 0;
  N exterior = 0;
  N total = 1;
  N old_boundary = 0;
  for (N level = 0; level < levels; ++level)
  {
    interior += status[level].guaranteed_interior;
    exterior += status[level].guaranteed_exterior;
    N boundary = total - interior - exterior;
    R dimension = log2(boundary / (R) old_boundary);
    printf
      ( "%ld\t%ld\t%ld\t%ld\t%ld\t%.3f\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\n"
      , level, total, interior, boundary, exterior, dimension
      , status[level].exterior_result[0][0]
      , status[level].exterior_result[0][1]
      , status[level].exterior_result[1][0]
      , status[level].exterior_result[1][1]
      , status[level].interior_result[0][0]
      , status[level].interior_result[0][1]
      , status[level].interior_result[1][0]
      , status[level].interior_result[1][1]
      , status[level].maximum_iterations_reached
      );
    interior *= 4;
    exterior *= 4;
    total *= 4;
    old_boundary = boundary;
  }
}

static inline R cabs2(C z) { return creal(z) * creal(z) + cimag(z) * cimag(z); }

static inline int attractor(C *z0, C c, N period) {
  R eps2 = 1.9721522630525295e-31;
  C z = *z0;
  for (N j = 0; j < 256; ++j) {
    C dz = 1;
    for (N k = 0; k < period; ++k) {
      dz = 2 * dz * z;
      z = z * z + c;
    }
    z = *z0 - (z - *z0) / (dz - 1.0);
    R e = cabs2(z - *z0);
    *z0 = z;
    if (e <= eps2) {
      return 1;
    }
    if (isnan(e) || isinf(e))
    {
      return 0;
    }
  }
  return 1;
}

static inline R interior_distance(C z_guess, C c, N period) {
  C z = z_guess;
  if (attractor(&z, c, period)) {
    C dz = 1;
    C dzdz = 0;
    C dc = 0;
    C dcdz = 0;
    for (N j = 0; j < period; ++j) {
      dcdz = 2.0 * (z * dcdz + dz * dc);
      dc = 2.0 * z * dc + 1.0;
      dzdz = 2.0 * (dz * dz + z * dzdz);
      dz = 2.0 * z * dz;
      z = z * z + c;
    }
    return (1.0 - cabs2(dz)) / (cabs(dcdz + dzdz * dc / (1.0 - dz)));
  }
  return -1.0;
}

static void subdivide(N levels, N level, C c)
{
  if (level >= levels)
  {
    return;
  }
  if (cimag(c) < 0)
  {
    return;
  }
  C z = 0;
  C dc = 0;
  R mz2 = 1.0/0.0;
  for (N i = 1; i < status[level].maximum_iterations; ++i)
  {
    dc = 2 * z * dc + 1;
    z = z * z + c;
    R z2 = cabs2(z);

    if (! (z2 < 65536))
    {
      // exterior
      R de = sqrt(z2) * log(z2) / (cabs(dc));
      R mu = i + 2 - log2(log(z2));
      N x = de < status[level].distance_lo;
      N y = mu < status[level].iteration_hi;
      if (x == 0 && y == 0)
      {
        fprintf(stderr, "counterexample (exterior): c = %.18f + %.18f i ; de = %.18g ; mu = %.18g\n", creal(c), cimag(c), de, mu);
      }
      status[level].exterior_result[x][y]++;
      if (de < status[level].distance_hi)
      {
        // near exterior, subdivisions may be interior
        subdivide(levels, level + 1, c + (-1-I) * status[level].box_offset);
        subdivide(levels, level + 1, c + (-1+I) * status[level].box_offset);
        subdivide(levels, level + 1, c + (+1-I) * status[level].box_offset);
        subdivide(levels, level + 1, c + (+1+I) * status[level].box_offset);
      }
      else
      {
        // far exterior, subdivisions guaranteed exterior
        status[level].guaranteed_exterior += cimag(c) == 0 ? 1 : 2;
      }
      return;
    }

    if (z2 < mz2)
    {
//      if (2 * z2 < mz2)
      {
        R de = interior_distance(z, c, i);
        if (de >= 0)
        {
          // interior
      N x = de < status[level].distance_lo;
      N y = i < status[level].period_hi;
      if (x == 0 && y == 0)
      {
        fprintf(stderr, "counterexample (interior): c = %.18f + %.18f i ; de = %.18g ; p = %ld\n", creal(c), cimag(c), de, i);
      }
          status[level].interior_result[x][y]++;
          if (de < status[level].distance_hi)
          {
            // near interior, subdivisions may be exterior
            subdivide(levels, level + 1, c + (-1-I) * status[level].box_offset);
            subdivide(levels, level + 1, c + (-1+I) * status[level].box_offset);
            subdivide(levels, level + 1, c + (+1-I) * status[level].box_offset);
            subdivide(levels, level + 1, c + (+1+I) * status[level].box_offset);
          }
          else
          {
            // far interior, subdivisions guaranteed interior
            status[level].guaranteed_interior += cimag(c) == 0 ? 1 : 2;
          }
          return;
        }
        mz2 = z2;
      }
    }
  }
  status[level].maximum_iterations_reached++;
  // maximum iteration count reached without any decision
  fprintf(stderr, "counterexample (unescaped): n = %ld ; c = %.18f + %.18f i ; z = %.18e + %.18e i ; dc = %.18e + %.18e i ; mz2 = %.18e\n", status[level].maximum_iterations, creal(c), cimag(c), creal(z), cimag(z), creal(dc), cimag(dc), mz2);
  subdivide(levels, level + 1, c + (-1-I) * status[level].box_offset);
  subdivide(levels, level + 1, c + (-1+I) * status[level].box_offset);
  subdivide(levels, level + 1, c + (+1-I) * status[level].box_offset);
  subdivide(levels, level + 1, c + (+1+I) * status[level].box_offset);
}

extern int main(int argc, char **argv)
{
  N levels = 12;
  if (argc > 1)
  {
    levels = atoi(argv[1]);
  }
  initialize();
  subdivide(levels, 0, 0);
  report(levels);
  return 0;
}
