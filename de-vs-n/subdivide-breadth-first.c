#include <assert.h>
#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <signal.h>

#ifndef M_PI
#define M_PI 3.14159265358979
#endif

typedef unsigned long int N;
typedef double R;
typedef double complex C;

const N threads = 16;

typedef struct
{
  unsigned int i, j; // 32bit to save space
} box_t;

box_t *src, *dst;
N src_count, dst_count;
N src_level, dst_level;
C src_origin, dst_origin;
R src_size, dst_size;

struct
{
  R box_size;
  R box_offset;
  R distance_hi;   // de > dhi => box disjoint dM
  R distance_lo;   // de < dlo => box intersects dM
  R iteration_hi;  // mu > Nhi => de < dlo
  R iteration_lo;  // mu < Nlo => de > dhi
  R period_hi;     // p  > phi => de < dlo
  N maximum_iterations;
  N exterior_result[2][2];
  N interior_result[2][2];
  N guaranteed_exterior;
  N guaranteed_interior;
  N maximum_iterations_reached;
} status[64];

void initialize(void)
{
  //randomize(seed);
  memset(status, 0, sizeof(status));
  for (N level = 0; level < 64; ++level)
  {
    status[level].box_size = 2.0 * pow(0.5, level);
    status[level].box_offset = 0.5 * status[level].box_size;;
    status[level].distance_hi = status[level].box_size * 4.0 * sqrt(2.0);
    status[level].distance_lo = status[level].box_size / 4.0;
    status[level].iteration_hi = 2.129 / sqrt(status[level].distance_lo);
    status[level].iteration_lo = log(48 / status[level].distance_hi) / log(4);
    status[level].period_hi = 2 / sqrt(status[level].distance_lo); // ?
    status[level].maximum_iterations = 2100100100;
  }
  src_level = 1;
  src = calloc(2, sizeof(*src));
  src[1].i = 1;
  src_count = 2;
  src_origin = -1-I;
  src_size = 2;
}

static void subdivide(void);

void report(N levels)
{
  printf("# level\ttotal\tint\tbdry\text\tdim\text00\text01\text10\text11\tint00\tint01\tint10\tint11\tmaxn\n");
  N interior = 0;
  N exterior = 0;
  N total = 1;
  N old_boundary = 0;
  for (N level = 0; level < levels; ++level)
  {
    if (level == src_level)
    {
      dst_level = src_level + 1;
      dst = malloc(src_count * 4 * sizeof(*dst));
      dst_count = 0;
      dst_size = 0.5 * src_size;
      dst_origin = src_origin - (1+I) * 0.5 * dst_size;
      subdivide();
      free(src);
      src = dst;
      src_level = dst_level;
      src_count = dst_count;
      src_origin = dst_origin;
      src_size = dst_size;
    }

    interior += 2 * status[level].guaranteed_interior;
    exterior += 2 * status[level].guaranteed_exterior;
    N boundary = total - interior - exterior;
    R dimension = log2(boundary / (R) old_boundary);
    printf
      ( "%ld\t%ld\t%ld\t%ld\t%ld\t%.3f\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\t%ld\n"
      , level, total, interior, boundary, exterior, dimension
      , status[level].exterior_result[0][0]
      , status[level].exterior_result[0][1]
      , status[level].exterior_result[1][0]
      , status[level].exterior_result[1][1]
      , status[level].interior_result[0][0]
      , status[level].interior_result[0][1]
      , status[level].interior_result[1][0]
      , status[level].interior_result[1][1]
      , status[level].maximum_iterations_reached
      );
    fflush(stdout);
    interior *= 4;
    exterior *= 4;
    total *= 4;
    old_boundary = boundary;
  }
}

static inline R cabs2(C z) { return creal(z) * creal(z) + cimag(z) * cimag(z); }

static inline int attractor(C *z0, C c, N period) {
  R eps2 = 1.9721522630525295e-31;
  C z = *z0;
  for (N j = 0; j < 256; ++j) {
    C dz = 1;
    for (N k = 0; k < period; ++k) {
      dz = 2 * dz * z;
      z = z * z + c;
    }
    z = *z0 - (z - *z0) / (dz - 1.0);
    R e = cabs2(z - *z0);
    *z0 = z;
    if (e <= eps2) {
      return 1;
    }
    if (isnan(e) || isinf(e))
    {
      return 0;
    }
  }
  return 1;
}

static inline R interior_distance(C z_guess, C c, N period) {
  C z = z_guess;
  if (attractor(&z, c, period)) {
    C dz = 1;
    C dzdz = 0;
    C dc = 0;
    C dcdz = 0;
    for (N j = 0; j < period; ++j) {
      dcdz = 2.0 * (z * dcdz + dz * dc);
      dc = 2.0 * z * dc + 1.0;
      dzdz = 2.0 * (dz * dz + z * dzdz);
      dz = 2.0 * z * dz;
      z = z * z + c;
    }
    return (1.0 - cabs2(dz)) / (cabs(dcdz + dzdz * dc / (1.0 - dz)));
  }
  return -1.0;
}

static void subdivide(void)
{
  N bin = 0, bout = 0, level = src_level;
  #pragma omp parallel for schedule(static, 1)
  for (N t = 0; t < threads; ++t)
  {
    while (1)
    {
      N in;
      #pragma omp atomic capture
      in = bin++;
      if (! (in < src_count))
      {
       break;
      }

  C c = src_origin + src_size * (src[in].i + I * src[in].j);
  C z = 0;
  C dc = 0;
  R mz2 = 1.0/0.0;
  for (N i = 1; i < status[level].maximum_iterations; ++i)
  {
    dc = 2 * z * dc + 1;
    z = z * z + c;
    R z2 = cabs2(z);

    if (! (z2 < 65536))
    {
      // exterior
      R de = sqrt(z2) * log(z2) / (cabs(dc));
      R mu = i + 2 - log2(log(z2));
      N x = de < status[level].distance_lo;
      N y = mu < status[level].iteration_hi;
      if (x == 0 && y == 0)
      {
        #pragma omp critical
        fprintf(stderr, "counterexample (exterior): c = %.18f + %.18f i ; de = %.18g ; mu = %.18g\n", creal(c), cimag(c), de, mu);
      }
      #pragma omp atomic
      status[level].exterior_result[x][y]++;
      if (de < status[level].distance_hi)
      {

            // near exterior, subdivisions may be interior
N out;
#pragma omp atomic capture
out = bout++;
for (N x = 0; x < 2; ++x)
for (N y = 0; y < 2; ++y)
  dst[4 * out + x * 2 + y] = (box_t){ (src[in].i << 1) + x, (src[in].j << 1) + y };

      }
      else
      {
        // far exterior, subdivisions guaranteed exterior
        #pragma omp atomic
        status[level].guaranteed_exterior++;
      }
      goto done;;
    }

    if (z2 < mz2)
    {
//      if (2 * z2 < mz2)
      {
        R de = interior_distance(z, c, i);
        if (de >= 0)
        {
          // interior
      N x = de < status[level].distance_lo;
      N y = i < status[level].period_hi;
      if (x == 0 && y == 0)
      {
        #pragma omp critical
        fprintf(stderr, "counterexample (interior): c = %.18f + %.18f i ; de = %.18g ; p = %ld\n", creal(c), cimag(c), de, i);
      }
          #pragma omp atomic
          status[level].interior_result[x][y]++;
          if (de < status[level].distance_hi)
          {

            // near interior, subdivisions may be exterior
N out;
#pragma omp atomic capture
out = bout++;
for (N x = 0; x < 2; ++x)
for (N y = 0; y < 2; ++y)
  dst[4 * out + x * 2 + y] = (box_t){ (src[in].i << 1) + x, (src[in].j << 1) + y };

          }
          else
          {
            // far interior, subdivisions guaranteed interior
            #pragma omp atomic
            status[level].guaranteed_interior++;
          }
          goto done;;
        }
        mz2 = z2;
      }
    }
  }
  #pragma omp atomic
  status[level].maximum_iterations_reached++;
  // maximum iteration count reached without any decision

  #pragma omp critical
  fprintf(stderr, "counterexample (unescaped): n = %ld ; c = %.18f + %.18f i ; z = %.18e + %.18e i ; dc = %.18e + %.18e i ; mz2 = %.18e\n", status[level].maximum_iterations, creal(c), cimag(c), creal(z), cimag(z), creal(dc), cimag(dc), mz2);

N out;
#pragma omp atomic capture
out = bout++;
for (N x = 0; x < 2; ++x)
for (N y = 0; y < 2; ++y)
  dst[4 * out + x * 2 + y] = (box_t){ (src[in].i << 1) + x, (src[in].j << 1) + y };

done:
      ;
    }
  }
  dst_count = 4 * bout;
}

extern int main(int argc, char **argv)
{
  N levels = 12;
  if (argc > 1)
  {
    levels = atoi(argv[1]);
  }
  initialize();
  report(levels);
  return 0;
}
