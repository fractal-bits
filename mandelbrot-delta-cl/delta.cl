__kernel void prefixsum
  ( __global const int *input
  , __global int *output
  , const int count
  , const int passbit
  ) {
  int i = get_global_id(0);
  if (0 <= i && i < count) {
    int x = input[i];
    if (i & passbit) {
      int j = (i | (passbit - 1)) - passbit;
      if (0 <= j && j < count) {
        x += input[j];
      }
    }
    output[i] = x;
  }
}

__kernel void permute
  ( __global const double *input
  , __global const int *keep
  , __global const int *index
  , __global double *output
  , const int count
  , const int elements
  ) {
  int i = get_global_id(0);
  if (0 <= i && i < count) {
    int k = keep[i];
    if (k) {
      int j = index[i] - 1;
      if (0 <= j && j < count) {
        int ii = i * elements;
        int jj = j * elements;
        for (int k = 0; k < elements; ++k) {
          output[jj + k] = input[ii + k];
        }
      }
    }
  }
}

__kernel void initialize
  ( __global double *output
  ,          const int      width
  ,          const int      height
  , __global const double *reference
  ,          const double  deltaX
  ,          const double  deltaY
  ) {
  int count = width * height;
  int i = get_global_id(0);
  if (0 <= i && i < count) {
    double x = (deltaX + (i % width) - (width /2))/(height/2);
    double y = (deltaY + (i / width) - (height/2))/(height/2);
    double xn = x;
    double yn = y;
    double dx = 0.0;
    double dy = 0.0;
    double ex = 0.0;
    double ey = 0.0;
    int k = 0;
    for (int j = 0; j < 12; ++j) {
      double cx = reference[k++];
      double cy = reference[k++];
      dx += cx * xn - cy * yn;
      dy += cx * yn + cy * xn;
      cx = reference[k++];
      cy = reference[k++];
      ex += cx * xn - cy * yn;
      ey += cx * yn + cy * xn;
      double xn1 = x * xn - y * yn;
      double yn1 = x * yn + y * xn;
      xn = xn1;
      yn = yn1;
    }
    const int elements = 5;
    int jj = i * elements;
    output[jj + 0] = i;
    output[jj + 1] = dx;
    output[jj + 2] = dy;
    output[jj + 3] = ex;
    output[jj + 4] = ey;
  }
}

__kernel void iterate
  ( __global const double *reference // { zx, zy, ax, ay }
  , __global const double *input     // { idx, dx, dy, lx, ly }
  , __global        double *output    // { idx, dx, dy, lx, ly }
  , __global int           *keep      // { !escaped }
  , __global float         *image     // { dwell, angle, distance, atomperiod }
  ,          const int     count
  ,          const int     width
  ,          const int     height
  ,          const double deltaX
  ,          const double deltaY
  ,          const double pixelSpacing
  ,          const int     start
  ,          const int     iters
  ,          const int     period
  ) {
  int i = get_global_id(0);
  if (0 <= i && i < count) {
    // constants
    const double er2 = 65536.0;
    const int     elements = 5;
    const int     channels = 4;
    // unpack state
    int ii = i * elements;
    double index = input[ii + 0];
    int i0 = index;
    double d0x = (deltaX + ((i0 % width) - (width /2))) * pixelSpacing;
    double d0y = (deltaY + ((i0 / width) - (height/2))) * pixelSpacing;
    double dx    = input[ii + 1];
    double dy    = input[ii + 2];
    double lx    = input[ii + 3];
    double ly    = input[ii + 4];
    bool escaped = false;
    int n = start;
    double yx = 0.0;
    double yy = 0.0;
    double y2 = 0.0;
    double dydcx = 0.0;
    double dydcy = 0.0;
    for (int m = 0; m < iters; ++m) {
      int p = n % period;
      double zx = reference[4*p+0];
      double zy = reference[4*p+1];
      double ax = reference[4*p+2];
      double ay = reference[4*p+3];
      // y <- z + d
      yx = zx + dx;
      yy = zy + dy;
      y2 = yx * yx + yy * yy;
      ++n;
      if (! (y2 <= er2)) {
        escaped = true;
        dydcx = (ax + lx) * pixelSpacing;
        dydcy = (ay + ly) * pixelSpacing;
        break;
      }
      // d <- 2 z d + d d + d0
      double tx = 2.0 * (zx * dx - zy * dy) + (dx * dx - dy * dy) + d0x;
      double ty = 2.0 * (zx * dy + zy * dx) + (2.0 * dx * dy) + d0y;
      // l <- 2 (a d + l d + l z)
      double sx = 2.0 * ((ax * dx - ay * dy) + (lx * dx - ly * dy) + (lx * zx - ly * zy));
      double sy = 2.0 * ((ax * dy + ay * dx) + (lx * dy + ly * dx) + (lx * zy + ly * zx));
      dx = tx;
      dy = ty;
      lx = sx;
      ly = sy;
    }
    // pack state
    if (escaped) {
      keep[i] = 0;
      keep[count] = 1;
      int jj = index * channels;
      double ly2 = log(y2);
      double ler2 = log(er2);
      double dydc2 = dydcx * dydcx + dydcy * dydcy;
      image[jj + 0] = n - log2(ly2 / ler2);
      image[jj + 1] = atan2(yy, yx);
      image[jj + 2] = sqrt(y2) * 0.5 * ly2 / sqrt(dydc2);
      image[jj + 3] = 0.0;
    } else {
      keep[i] = 1;
      output[ii + 0] = index;
      output[ii + 1] = dx;
      output[ii + 2] = dy;
      output[ii + 3] = lx;
      output[ii + 4] = ly;
    }
  }
}

__kernel void clear
  ( __global float *image
  ,          const int count
  ) {
  int i = get_global_id(0);
  if (0 <= i && i < count) {
    int jj = 4 * i;
    image[jj + 0] = -1.0;
    image[jj + 1] =  0.0;
    image[jj + 2] = -1.0;
    image[jj + 3] = -1.0;
  }
}

// linear interpolation by Robert Munafo mrob.com
float lin_interp
  ( float x
  , float domain_low
  , float domain_hi
  , float range_low
  , float range_hi
  ) {
  if ((x >= domain_low) && (x <= domain_hi)) {
    x = (x - domain_low) / (domain_hi - domain_low);
    x = range_low + x * (range_hi - range_low);
  }
  return x;
}

// perceptually balanced hue adjustment by Robert Munafo mrob.com
float pvp_adjust_3(float x) {
  x = lin_interp(x, 0.00, 0.125, -0.050, 0.090);
  x = lin_interp(x, 0.125, 0.25,  0.090, 0.167);
  x = lin_interp(x, 0.25, 0.375,  0.167, 0.253);
  x = lin_interp(x, 0.375, 0.50,  0.253, 0.383);
  x = lin_interp(x, 0.50, 0.625,  0.383, 0.500);
  x = lin_interp(x, 0.625, 0.75,  0.500, 0.667);
  x = lin_interp(x, 0.75, 0.875,  0.667, 0.800);
  x = lin_interp(x, 0.875, 1.00,  0.800, 0.950);
  return(x);
}

void hsv2rgb
  ( float h
  , float s
  , float v
  , float *rp
  , float *gp
  , float *bp
  ) {
  float i, f, p, q, t, r, g, b;
  if (s == 0.0) {
    r = v;
    g = v;
    b = v;
  } else {
    h = pvp_adjust_3(h);
    h = h - floor(h);
    h = h * 6.0;
    i = floor(h);
    f = h - i;
    p = v*(1.0 - s);
    q = v*(1.0 - (s*f));
    t = v*(1.0 - (s*(1.0 - f)));
    if (i < 1.0) { r = v; g = t; b = p; } else
    if (i < 2.0) { r = q; g = v; b = p; } else
    if (i < 3.0) { r = p; g = v; b = t; } else
    if (i < 4.0) { r = p; g = q; b = v; } else
    if (i < 5.0) { r = t; g = p; b = v; } else
                 { r = v; g = p; b = q; }
  }
  *rp = r;
  *gp = g;
  *bp = b;
}

__kernel void colour
  ( __global const float *image
  , __global unsigned char *rgb
  ,          const int count
  ) {
  int i = get_global_id(0);
  if (0 <= i && i < count) {
    int ii = 4 * i;
    int jj = 3 * i;
    float dwell = image[ii + 0];
    float distance = image[ii + 2];
    if (distance > 0.0) {
      float v = tanh(pow(distance/4.0f, 0.5f));
      float s = 0.5;
      float h = pow(dwell, 0.5f);
      h -= floor(h);
      float r, g, b;
      hsv2rgb(h, s, v, &r, &g, &b);
      rgb[jj + 0] = min(max(255.0 * r, 0.0), 255.0);
      rgb[jj + 1] = min(max(255.0 * g, 0.0), 255.0);
      rgb[jj + 2] = min(max(255.0 * b, 0.0), 255.0);
    } else {
      rgb[jj + 0] = 255;
      rgb[jj + 1] = 255;
      rgb[jj + 2] = 255;
    }
  }
}
