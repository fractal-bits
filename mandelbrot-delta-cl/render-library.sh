#!/bin/bash
platform=0
width=512
height=512
out=sft-library
maxiters=1000000
mkdir -p "${out}"
while read name
do
  read s
  read r
  read i
  read ignore
  echo "${name}"
  time ./mandelbrot-delta-cl "${platform}" "${width}" "${height}" "${r:2}" "${i:2}" "${s:2}" 1 "${maxiters}" > "${out}/${name}.ppm"
done
