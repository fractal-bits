// mandelbrot-delta-cl -- Mandelbrot set perturbation renderer using OpenCL
// Copyright: (C) 2013 Claude Heiland-Allen <claude@mathr.co.uk>
// License: http://www.gnu.org/licenses/gpl.html GPLv3+


// ==== [[[ config [[[ ====

// #define USE_DOUBLE to use double precision (highly recommended)
// #undef  USE_DOUBLE to use single precision (only for testing)
#define USE_DOUBLE

// #define USE_CLGL to use OpenCL -> OpenGL interoperation
// #undef  USE_CLGL to use copying fallback (eg: for CPU implementation)
#undef  USE_CLGL

// ==== ]]] config ]]] ====


#include <complex>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <boost/multiprecision/mpfr.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/glx.h>
#include <CL/cl.h>
#include <CL/cl_gl.h>

#include "cl-extra.cc"

using namespace boost::multiprecision;
typedef mpfr_float R;
typedef std::complex<R> C;
#ifdef USE_DOUBLE
typedef double F;
#else
typedef float F;
#endif
typedef std::complex<F> CF;

// the OpenCL source

#ifdef USE_DOUBLE
#ifdef USE_CLGL
#define CL(cl) "#pragma OPENCL EXTENSION cl_khr_gl_sharing : enable\n#pragma OPENCL EXTENSION cl_khr_fp64: enable\n" cl
#else
#define CL(cl) "#pragma OPENCL EXTENSION cl_khr_fp64: enable\n" cl
#endif
#else
#ifdef USE_CLGL
#define CL(cl) "#pragma OPENCL EXTENSION cl_khr_gl_sharing : enable\n" cl
#else
#define CL(cl) #cl
#endif
#endif

const char *src = CL("#include \"delta.cl\"");

int findPeriod(C c0, R r, int maxIters) {
  C c[4], z[4];
  c[0] = c0 + C(-r, -r);
  c[1] = c0 + C( r, -r);
  c[2] = c0 + C( r,  r);
  c[3] = c0 + C(-r,  r);
  z[0] = z[1] = z[2] = z[3] = C(0.0, 0.0);
  for (int p = 1; p < maxIters; ++p) {
    for (int i = 0; i < 4; ++i) {
      z[i] = z[i] * z[i] + c[i];
    }
    int preal = 0;
    for (int i = 0; i < 4; ++i) {
      int j = (i + 1) % 4;
      if (!(z[i].imag() < 0 && z[j].imag() < 0) && !(z[i].imag() > 0 && z[j].imag() > 0)) {
        C d = z[j] - z[i];
        if ((d.imag() * z[i].real() - d.real() * z[i].imag()) * d.imag() > 0) {
          ++preal;
        }
      }
    }
    if (preal & 1) {
      return p;
    }
  }
  return 0;
}

int muatom(int period, mpfr_t x, mpfr_t y, mpfr_t z) {
  const mpfr_prec_t accuracy = 12;
  mpfr_prec_t bits = std::max(mpfr_get_prec(x), mpfr_get_prec(y));
#define VARS nx, ny, bx, by, wx, wy, zz, Ax, Ay, Bx, By, Cx, Cy, Dx, Dy, Ex, Ey, t1, t2, t3, t4, t5, t6, t7, t8, t9, t0, t01, t02, t03, t04
  mpfr_t VARS;
  mpfr_inits2(bits, VARS, (mpfr_ptr) 0);
  mpfr_set(nx, x, MPFR_RNDN);
  mpfr_set(ny, y, MPFR_RNDN);
  mpfr_set_prec(zz, mpfr_get_prec(z));
  int n_ok, w_ok, b_ok;
  int r = 0;
  //progressReport('\n');
  do { //progressReport('X');
    mpfr_set_prec(t0,  bits - accuracy);
    mpfr_set_prec(t01, bits - accuracy);
    mpfr_set_prec(t02, bits - accuracy);
    mpfr_set_prec(t03, bits - accuracy);
    mpfr_set_prec(t04, bits - accuracy);
    int limit = 40;
    do { //progressReport('n');
      // refine nucleus
      mpfr_set_ui(Ax, 0, MPFR_RNDN);
      mpfr_set_ui(Ay, 0, MPFR_RNDN);
      mpfr_set_ui(Dx, 0, MPFR_RNDN);
      mpfr_set_ui(Dy, 0, MPFR_RNDN);
      for (int i = 0; i < period; ++i) {
        // D <- 2AD + 1  : Dx <- 2 (Ax Dx - Ay Dy) + 1 ; Dy = 2 (Ax Dy + Ay Dx)
        mpfr_mul(t1, Ax, Dx, MPFR_RNDN);
        mpfr_mul(t2, Ay, Dy, MPFR_RNDN);
        mpfr_mul(t3, Ax, Dy, MPFR_RNDN);
        mpfr_mul(t4, Ay, Dx, MPFR_RNDN);
        mpfr_sub(Dx, t1, t2, MPFR_RNDN);
        mpfr_mul_2ui(Dx, Dx, 1, MPFR_RNDN);
        mpfr_add_ui(Dx, Dx, 1, MPFR_RNDN);
        mpfr_add(Dy, t3, t4, MPFR_RNDN);
        mpfr_mul_2ui(Dy, Dy, 1, MPFR_RNDN);
        // A <- A^2 + n  : Ax <- Ax^2 - Ay^2 + nx ; Ay <- 2 Ax Ay + ny
        mpfr_sqr(t1, Ax, MPFR_RNDN);
        mpfr_sqr(t2, Ay, MPFR_RNDN);
        mpfr_mul(t3, Ax, Ay, MPFR_RNDN);
        mpfr_sub(Ax, t1, t2, MPFR_RNDN);
        mpfr_add(Ax, Ax, nx, MPFR_RNDN);
        mpfr_mul_2ui(t3, t3, 1, MPFR_RNDN);
        mpfr_add(Ay, t3, ny,MPFR_RNDN);
      }
      // n <- n - A / D  = (nx + ny i) - ((Ax Dx + Ay Dy) + (Ay Dx - Ax Dy)i) / (Dx^2 + Dy^2)
      mpfr_sqr(t1, Dx, MPFR_RNDN);
      mpfr_sqr(t2, Dy, MPFR_RNDN);
      mpfr_add(t1, t1, t2, MPFR_RNDN);
  
      mpfr_mul(t2, Ax, Dx, MPFR_RNDN);
      mpfr_mul(t3, Ay, Dy, MPFR_RNDN);
      mpfr_add(t2, t2, t3, MPFR_RNDN);
      mpfr_div(t2, t2, t1, MPFR_RNDN);
      mpfr_sub(t2, nx, t2, MPFR_RNDN);
  
      mpfr_mul(t3, Ay, Dx, MPFR_RNDN);
      mpfr_mul(t4, Ax, Dy, MPFR_RNDN);
      mpfr_sub(t3, t3, t4, MPFR_RNDN);
      mpfr_div(t3, t3, t1, MPFR_RNDN);
      mpfr_sub(t3, ny, t3, MPFR_RNDN);
  
      mpfr_set(t01, t2, MPFR_RNDN);
      mpfr_set(t02, t3, MPFR_RNDN);
      mpfr_set(t03, nx, MPFR_RNDN);
      mpfr_set(t04, ny, MPFR_RNDN);
      mpfr_sub(t01, t01, t03, MPFR_RNDN);
      mpfr_sub(t02, t02, t04, MPFR_RNDN);
      n_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);
  
      mpfr_set(nx, t2, MPFR_RNDN);
      mpfr_set(ny, t3, MPFR_RNDN);
    } while (! n_ok && --limit);
    if (! limit) goto cleanup;
    mpfr_set(wx, nx, MPFR_RNDN);
    mpfr_set(wy, ny, MPFR_RNDN);
    mpfr_set(bx, nx, MPFR_RNDN);
    mpfr_set(by, ny, MPFR_RNDN);
    limit = 40;
    do { //progressReport('b');
      // refine bond
      mpfr_set(Ax, wx, MPFR_RNDN);
      mpfr_set(Ay, wy, MPFR_RNDN);
      mpfr_set_ui(Bx, 1, MPFR_RNDN);
      mpfr_set_ui(By, 0, MPFR_RNDN);
      mpfr_set_ui(Cx, 0, MPFR_RNDN);
      mpfr_set_ui(Cy, 0, MPFR_RNDN);
      mpfr_set_ui(Dx, 0, MPFR_RNDN);
      mpfr_set_ui(Dy, 0, MPFR_RNDN);
      mpfr_set_ui(Ex, 0, MPFR_RNDN);
      mpfr_set_ui(Ey, 0, MPFR_RNDN);
      for (int i = 0; i < period; ++i) {
        // E <- 2 ( A E + B D )
        mpfr_mul(t1, Ax, Ex, MPFR_RNDN);
        mpfr_mul(t2, Ay, Ey, MPFR_RNDN);
        mpfr_sub(t1, t1, t2, MPFR_RNDN);
        mpfr_mul(t2, Ax, Ey, MPFR_RNDN);
        mpfr_mul(t3, Ay, Ex, MPFR_RNDN);
        mpfr_add(t2, t2, t3, MPFR_RNDN);
        mpfr_mul(t3, Bx, Dx, MPFR_RNDN);
        mpfr_mul(t4, By, Dy, MPFR_RNDN);
        mpfr_sub(t3, t3, t4, MPFR_RNDN);
        mpfr_add(Ex, t1, t3, MPFR_RNDN);
        mpfr_mul(t3, Bx, Dy, MPFR_RNDN);
        mpfr_mul(t4, By, Dx, MPFR_RNDN);
        mpfr_add(t3, t3, t4, MPFR_RNDN);
        mpfr_add(Ey, t2, t3, MPFR_RNDN);
        mpfr_mul_2ui(Ex, Ex, 1, MPFR_RNDN);
        mpfr_mul_2ui(Ey, Ey, 1, MPFR_RNDN);
        // D <- 2 A D + 1
        mpfr_mul(t1, Ax, Dx, MPFR_RNDN);
        mpfr_mul(t2, Ay, Dy, MPFR_RNDN);
        mpfr_mul(t3, Ax, Dy, MPFR_RNDN);
        mpfr_mul(t4, Ay, Dx, MPFR_RNDN);
        mpfr_sub(Dx, t1, t2, MPFR_RNDN);
        mpfr_mul_2ui(Dx, Dx, 1, MPFR_RNDN);
        mpfr_add_ui(Dx, Dx, 1, MPFR_RNDN);
        mpfr_add(Dy, t3, t4, MPFR_RNDN);
        mpfr_mul_2ui(Dy, Dy, 1, MPFR_RNDN);
        // C <- 2 ( B^2 + A C )
        mpfr_sqr(t1, Bx, MPFR_RNDN);
        mpfr_sqr(t2, By, MPFR_RNDN);
        mpfr_sub(t1, t1, t2, MPFR_RNDN);
        mpfr_mul(t2, Bx, By, MPFR_RNDN);
        mpfr_mul_2ui(t2, t2, 1, MPFR_RNDN);
        mpfr_mul(t3, Ax, Cx, MPFR_RNDN);
        mpfr_mul(t4, Ay, Cy, MPFR_RNDN);
        mpfr_sub(t3, t3, t4, MPFR_RNDN);
        mpfr_add(t1, t1, t3, MPFR_RNDN);
        mpfr_mul(t3, Ax, Cy, MPFR_RNDN);
        mpfr_mul(t4, Ay, Cx, MPFR_RNDN);
        mpfr_add(t3, t3, t4, MPFR_RNDN);
        mpfr_add(t2, t2, t3, MPFR_RNDN);
        mpfr_mul_2ui(Cx, t1, 1, MPFR_RNDN);
        mpfr_mul_2ui(Cy, t2, 1, MPFR_RNDN);
        // B <- 2 A B
        mpfr_mul(t1, Ax, Bx, MPFR_RNDN);
        mpfr_mul(t2, Ay, By, MPFR_RNDN);
        mpfr_mul(t3, Ax, By, MPFR_RNDN);
        mpfr_mul(t4, Ay, Bx, MPFR_RNDN);
        mpfr_sub(Bx, t1, t2, MPFR_RNDN);
        mpfr_mul_2ui(Bx, Bx, 1, MPFR_RNDN);
        mpfr_add(By, t3, t4, MPFR_RNDN);
        mpfr_mul_2ui(By, By, 1, MPFR_RNDN);
        // A <- A^2 + b
        mpfr_sqr(t1, Ax, MPFR_RNDN);
        mpfr_sqr(t2, Ay, MPFR_RNDN);
        mpfr_mul(t3, Ax, Ay, MPFR_RNDN);
        mpfr_sub(Ax, t1, t2, MPFR_RNDN);
        mpfr_add(Ax, Ax, bx, MPFR_RNDN);
        mpfr_mul_2ui(t3, t3, 1, MPFR_RNDN);
        mpfr_add(Ay, t3, by,MPFR_RNDN);
      }
      // (w) <- (w) - (B-1  D)^-1 (A - w)
      // (b) <- (b)   ( C   E)    (B + 1)
      //
      // det = (B-1)E - CD
      // inv = (E    -D)
      //       (-C (B-1) / det
      //
      // w - (E(A-w) - D(B+1))/det
      // b - (-C(A-w) + (B-1)(B+1))/det   ; B^2 - 1
      //
      // A -= w
      mpfr_sub(Ax, Ax, wx, MPFR_RNDN);
      mpfr_sub(Ay, Ay, wy, MPFR_RNDN);
      // (t1,t2) = B^2 - 1
      mpfr_mul(t1, Bx, Bx, MPFR_RNDN);
      mpfr_mul(t2, By, By, MPFR_RNDN);
      mpfr_sub(t1, t1, t2, MPFR_RNDN);
      mpfr_sub_ui(t1, t1, 1, MPFR_RNDN);
      mpfr_mul(t2, Bx, By, MPFR_RNDN);
      mpfr_mul_2ui(t2, t2, 1, MPFR_RNDN);
      // (t1,t2) -= C(A-w)
      mpfr_mul(t3, Cx, Ax, MPFR_RNDN);
      mpfr_mul(t4, Cy, Ay, MPFR_RNDN);
      mpfr_sub(t3, t3, t4, MPFR_RNDN);
      mpfr_sub(t1, t1, t3, MPFR_RNDN);
      mpfr_mul(t3, Cx, Ay, MPFR_RNDN);
      mpfr_mul(t4, Cy, Ax, MPFR_RNDN);
      mpfr_add(t3, t3, t4, MPFR_RNDN);
      mpfr_sub(t2, t2, t3, MPFR_RNDN);
      // (t3, t4) = (B-1)E - CD
      mpfr_sub_ui(t3, Bx, 1, MPFR_RNDN);
      mpfr_mul(t4, t3, Ex, MPFR_RNDN);
      mpfr_mul(t5, By, Ey, MPFR_RNDN);
      mpfr_sub(t4, t4, t5, MPFR_RNDN);
      mpfr_mul(t5, t3, Ey, MPFR_RNDN);
      mpfr_mul(t6, By, Ex, MPFR_RNDN);
      mpfr_sub(t5, t5, t6, MPFR_RNDN);
      mpfr_mul(t6, Cx, Dx, MPFR_RNDN);
      mpfr_mul(t7, Cy, Dy, MPFR_RNDN);
      mpfr_sub(t6, t6, t7, MPFR_RNDN);
      mpfr_sub(t3, t4, t6, MPFR_RNDN);
      mpfr_mul(t6, Cx, Dy, MPFR_RNDN);
      mpfr_mul(t7, Cy, Dx, MPFR_RNDN);
      mpfr_add(t6, t6, t7, MPFR_RNDN);
      mpfr_sub(t4, t5, t6, MPFR_RNDN);
      // (t3, t4) = 1/(t3, t4)  ; z^-1 = z* / (z z*)
      mpfr_sqr(t5, t3, MPFR_RNDN);
      mpfr_sqr(t6, t4, MPFR_RNDN);
      mpfr_add(t5, t5, t6, MPFR_RNDN);
      mpfr_div(t3, t3, t5, MPFR_RNDN);
      mpfr_div(t4, t4, t5, MPFR_RNDN);
      mpfr_neg(t4, t4, MPFR_RNDN);
      // (t1, t2) *= (t3, t4)
      mpfr_mul(t5, t1, t3, MPFR_RNDN);
      mpfr_mul(t6, t2, t4, MPFR_RNDN);
      mpfr_mul(t7, t1, t4, MPFR_RNDN);
      mpfr_mul(t8, t2, t3, MPFR_RNDN);
      mpfr_sub(t1, t5, t6, MPFR_RNDN);
      mpfr_add(t2, t7, t8, MPFR_RNDN);
  
      // (t1, t2) = b - (t1, t2)
      mpfr_sub(t1, bx, t1, MPFR_RNDN);
      mpfr_sub(t2, by, t2, MPFR_RNDN);
  
      mpfr_set(t01, t1, MPFR_RNDN);
      mpfr_set(t02, t2, MPFR_RNDN);
      mpfr_set(t03, bx, MPFR_RNDN);
      mpfr_set(t04, by, MPFR_RNDN);
      mpfr_sub(t01, t01, t03, MPFR_RNDN);
      mpfr_sub(t02, t02, t04, MPFR_RNDN);
      b_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);
  
      // (t5, t6) = E (A-w)
      mpfr_mul(t5, Ex, Ax, MPFR_RNDN);
      mpfr_mul(t6, Ey, Ay, MPFR_RNDN);
      mpfr_sub(t5, t5, t6, MPFR_RNDN);
      mpfr_mul(t6, Ex, Ay, MPFR_RNDN);
      mpfr_mul(t7, Ey, Ax, MPFR_RNDN);
      mpfr_add(t6, t6, t7, MPFR_RNDN);
      // B += 1
      mpfr_add_ui(Bx, Bx, 1, MPFR_RNDN);
      // (t7, t8) = D (B+1)
      mpfr_mul(t7, Dx, Bx, MPFR_RNDN);
      mpfr_mul(t8, Dy, By, MPFR_RNDN);
      mpfr_sub(t7, t7, t8, MPFR_RNDN);
      mpfr_mul(t8, Dx, By, MPFR_RNDN);
      mpfr_mul(t9, Dy, Bx, MPFR_RNDN);
      mpfr_add(t8, t8, t9, MPFR_RNDN);
      // (t5, t6) -= (t7, t8)
      mpfr_sub(t5, t5, t7, MPFR_RNDN);
      mpfr_sub(t6, t6, t8, MPFR_RNDN);
      // (t7, t8) = (t5, t6) * (t3, t4)
      mpfr_mul(t7, t3, t5, MPFR_RNDN);
      mpfr_mul(t8, t4, t6, MPFR_RNDN);
      mpfr_sub(t7, t7, t8, MPFR_RNDN);
      mpfr_mul(t8, t3, t6, MPFR_RNDN);
      mpfr_mul(t9, t4, t5, MPFR_RNDN);
      mpfr_add(t8, t8, t9, MPFR_RNDN);
  
      // (t3, t4) = w - (t7, t8)
      mpfr_sub(t3, wx, t7, MPFR_RNDN);
      mpfr_sub(t4, wy, t8, MPFR_RNDN);
  
      mpfr_set(t01, t3, MPFR_RNDN);
      mpfr_set(t02, t4, MPFR_RNDN);
      mpfr_set(t03, wx, MPFR_RNDN);
      mpfr_set(t04, wy, MPFR_RNDN);
      mpfr_sub(t01, t01, t03, MPFR_RNDN);
      mpfr_sub(t02, t02, t04, MPFR_RNDN);
      w_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);
  
      mpfr_set(t01, t3, MPFR_RNDN);
      mpfr_set(t02, t4, MPFR_RNDN);
      mpfr_set(t03, wx, MPFR_RNDN);
      mpfr_set(t04, wy, MPFR_RNDN);
      mpfr_sub(t01, t01, t03, MPFR_RNDN);
      mpfr_sub(t02, t02, t04, MPFR_RNDN);
      w_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);
  
      mpfr_set(bx, t1, MPFR_RNDN);
      mpfr_set(by, t2, MPFR_RNDN);
      mpfr_set(wx, t3, MPFR_RNDN);
      mpfr_set(wy, t4, MPFR_RNDN);
    } while (!(w_ok && b_ok) && --limit);
    if (! limit) goto cleanup;
    // t1 = |n - b|
    mpfr_sub(t1, nx, bx, MPFR_RNDN);
    mpfr_sqr(t1, t1, MPFR_RNDN);
    mpfr_sub(t2, ny, by, MPFR_RNDN);
    mpfr_sqr(t2, t2, MPFR_RNDN);
    mpfr_add(t1, t1, t2, MPFR_RNDN);
    mpfr_sqrt(t1, t1, MPFR_RNDN);
    bits = 2 * bits;
    mpfr_prec_round(nx, bits, MPFR_RNDN);
    mpfr_prec_round(ny, bits, MPFR_RNDN);
    mpfr_prec_round(wx, bits, MPFR_RNDN);
    mpfr_prec_round(wy, bits, MPFR_RNDN);
    mpfr_prec_round(bx, bits, MPFR_RNDN);
    mpfr_prec_round(by, bits, MPFR_RNDN);
    mpfr_set(zz, t1, MPFR_RNDN);
    mpfr_set_prec(Ax, bits);
    mpfr_set_prec(Ay, bits);
    mpfr_set_prec(Bx, bits);
    mpfr_set_prec(By, bits);
    mpfr_set_prec(Cx, bits);
    mpfr_set_prec(Cy, bits);
    mpfr_set_prec(Dx, bits);
    mpfr_set_prec(Dy, bits);
    mpfr_set_prec(Ex, bits);
    mpfr_set_prec(Ey, bits);
    mpfr_set_prec(t1, bits);
    mpfr_set_prec(t2, bits);
    mpfr_set_prec(t3, bits);
    mpfr_set_prec(t4, bits);
    mpfr_set_prec(t5, bits);
    mpfr_set_prec(t6, bits);
    mpfr_set_prec(t7, bits);
    mpfr_set_prec(t8, bits);
    mpfr_set_prec(t9, bits);
  } while (mpfr_zero_p(zz) || bits + mpfr_get_exp(zz) < mpfr_get_prec(zz) + accuracy);

  // copy to output
  r = 1;
  mpfr_set_prec(x, bits);
  mpfr_set_prec(y, bits);
  mpfr_set(x, nx, MPFR_RNDN);
  mpfr_set(y, ny, MPFR_RNDN);
  mpfr_set(z, zz, MPFR_RNDN);
cleanup:
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
  return r;
}

struct atom {
  C nucleus;
  R radius;
  int period;
};

struct atom *findAtom(C c, R r, int period) {
  struct atom *a = 0;
  mpfr_t x;
  mpfr_t y;
  mpfr_t z;
  mpfr_inits2(mpfr_get_prec(c.real().backend().data()), x, y, (mpfr_ptr) 0);
  mpfr_inits2(mpfr_get_prec(r.backend().data()), z, (mpfr_ptr) 0);
  mpfr_set(x, c.real().backend().data(), MPFR_RNDN);
  mpfr_set(y, c.imag().backend().data(), MPFR_RNDN);
  mpfr_set(z, r.backend().data(), MPFR_RNDN);
  int ok = muatom(period, x, y, z);
  if (ok) {
    int prec = int(ceil(log10(2.0) * mpfr_get_prec(x)));
    R::default_precision(prec);
    a = new atom;
    a->nucleus.real().precision(prec);
    a->nucleus.imag().precision(prec);
    a->nucleus = C(R(x), R(y));
    a->radius  = R(z);
    a->period  = period;
  }
  mpfr_clears(x, y, z, (mpfr_ptr) 0);
  return a;
}

// the main program

int main(int argc, char **argv) {

  // read command line arguments
  if (argc != 9) {
    fprintf(stderr, "usage: %s  platform  width height  re im size  polyiters maxiters >out.ppm\n", argv[0]);
    return 1;
  }
  cl_uint PLATFORM = atoi(argv[1]);
  cl_int w(atoi(argv[2]));
  cl_int h(atoi(argv[3]));
  R::default_precision(20);
  F csize(atof(argv[6]));
  R::default_precision(int(20 - log10(csize)));
  F pixelSpacing = csize / (h/2);
  R cx(argv[4]);
  R cy(argv[5]);
  C c(cx, cy);
  cl_int precount(atoi(argv[7]));
  cl_int maxIters(atoi(argv[8]));

  // compute period
  cl_int period = findPeriod(c, csize, maxIters);
  if (! period) {
    fprintf(stderr, "failed to find period\n");
    return 1;
  }
  fprintf(stderr, "period = %d\n", period);

  // compute atom
  struct atom *ref = 0;
  if (! (ref = findAtom(c, csize, period))) {
    fprintf(stderr, "failed to find reference\n");
    return 1;
  }
  mpfr_fprintf(stderr
    , "real = %Re\nimag = %Re\nsize = %Re\n"
    , ref->nucleus.real().backend().data()
    , ref->nucleus.imag().backend().data()
    , ref->radius.backend().data()
    );

  R::default_precision(ref->nucleus.real().precision());
  C refc = ref->nucleus;
  cl_int iters = 0;
  cl_int stepIters = 1024;

  // compute one period at high precision
  int per_bytes = 4 * period * sizeof(F);
  F *per_zs = (F *) calloc(1, per_bytes);
  {
    C c0(refc);
    C z(c0);
    C a(1.0, 0.0);
    for (int i = 0; i < period; ++i) {
      if (i == period - 1) {
        z = C(0.0, 0.0);
      }
      per_zs[4 * i + 0] = F(z.real());
      per_zs[4 * i + 1] = F(z.imag());
      per_zs[4 * i + 2] = F(a.real());
      per_zs[4 * i + 3] = F(a.imag());
      a = C(2.0, 0.0) * z * a + C(1.0, 0.0);
      z = z * z + c0;
    }
  }

  // storage for order-12 polynomial approximation
  int pre_bytes = 12 * 4 * sizeof(F);
  F *pre_zs = (F *) calloc(1, pre_bytes);

  // initialize OpenGL
  if (! glfwInit()) {
    fprintf(stderr, "glfwInit() failed\n");
    return 1;
  };
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(w, h, "MandelbrotDeltaCL", 0, 0);
  if (! window) {
    fprintf(stderr, "glfwOpenWindow() failed\n");
    return 1;
  }
  glfwMakeContextCurrent(window);
  glewInit();

  // enumerate OpenCL platforms
  cl_platform_id platform_id[64];
  cl_uint platform_ids;
  E(clGetPlatformIDs(64, &platform_id[0], &platform_ids));
  if (! (PLATFORM < platform_ids)) {
    fprintf(stderr, "invalid platform: %d (range is 0 to %d)\n", PLATFORM, platform_ids - 1);
    return 1;
  }
  for (cl_uint i = 0; i < platform_ids; ++i) {
    fprintf(stderr, "platform %d%s\n", i, i == PLATFORM ? " *** SELECTED ***" : "");
    char buf[1024];
    cl_platform_info info[5] =
      { CL_PLATFORM_PROFILE
      , CL_PLATFORM_VERSION
      , CL_PLATFORM_NAME
      , CL_PLATFORM_VENDOR
      , CL_PLATFORM_EXTENSIONS
      };
    for (int j = 0; j < 5; ++j) {
      buf[0] = 0;
      E(clGetPlatformInfo(platform_id[i], info[j], 1024, &buf[0], NULL));
      fprintf(stderr, "\t%s\n", buf);
    }
  }

  // create context
  cl_device_id device_id;
  E(clGetDeviceIDs(platform_id[PLATFORM], CL_DEVICE_TYPE_ALL, 1, &device_id, NULL));
  cl_context_properties properties[] =
    {
#ifdef USE_CLGL
      CL_GL_CONTEXT_KHR, (cl_context_properties) glXGetCurrentContext(),
      CL_GLX_DISPLAY_KHR, (cl_context_properties) glXGetCurrentDisplay(),
#endif
      CL_CONTEXT_PLATFORM, (cl_context_properties) platform_id[PLATFORM]
    , 0
    };
  cl_int err;
  cl_context context = clCreateContext(properties, 1, &device_id, NULL, NULL, &err);
  if (! context) { E(err); }
  cl_command_queue commands = clCreateCommandQueue(context, device_id, 0, &err);
  if (! commands) { E(err); }

  // compile program
  cl_program program = clCreateProgramWithSource(context, 1, &src, NULL, &err);
  if (! program) { E(err); }
  err = clBuildProgram(program, 1, &device_id, "-I. -cl-finite-math-only -cl-no-signed-zeros"
#ifdef USE_DOUBLE
    ""
#else
    " -Ddouble=float"
#endif
    , NULL, NULL);
  if (err != CL_SUCCESS) {
    char *buf = (char *) malloc(1000000);
    buf[0] = 0;
    E(clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 1000000, &buf[0], NULL));
    fprintf(stderr, "build failed:\n%s\n", buf);
    free(buf);
    E(err);
  }
  
  // create kernels
  cl_kernel prefixsum = clCreateKernel(program, "prefixsum", &err);
  if (! prefixsum) { E(err); }
  cl_kernel permute = clCreateKernel(program, "permute", &err);
  if (! permute) { E(err); }
  cl_kernel initialize = clCreateKernel(program, "initialize", &err);
  if (! initialize) { E(err); }
  cl_kernel iterate = clCreateKernel(program, "iterate", &err);
  if (! iterate) { E(err); }
  cl_kernel clear = clCreateKernel(program, "clear", &err);
  if (! clear) { E(err); }
  cl_kernel colour = clCreateKernel(program, "colour", &err);
  if (! colour) { E(err); }

  // create buffers
  cl_mem in_pre_zs = clCreateBuffer(context, CL_MEM_READ_ONLY, pre_bytes, NULL, &err);
  if (! in_pre_zs) { E(err); }

  cl_mem in_per_zs = clCreateBuffer(context, CL_MEM_READ_ONLY, per_bytes, NULL, &err);
  if (! in_per_zs) { E(err); }
  E(clEnqueueWriteBuffer(commands, in_per_zs, CL_TRUE, 0, per_bytes, &per_zs[0], 0, NULL, NULL));

  cl_int elements = 5;
  int ebytes = w * h * elements * sizeof(F);
  cl_mem state[2];
  state[0] = clCreateBuffer(context, CL_MEM_READ_WRITE, ebytes, NULL, &err);
  if (! state[0]) { E(err); }
  state[1] = clCreateBuffer(context, CL_MEM_READ_WRITE, ebytes, NULL, &err);
  if (! state[1]) { E(err); }

  int kbytes = (w * h + 1) * sizeof(cl_int);
  cl_mem keep = clCreateBuffer(context, CL_MEM_READ_WRITE, kbytes, NULL, &err);
  if (! keep) { E(err); }

  int sbytes = (w * h) * sizeof(cl_int);
  cl_mem sums[2];
  sums[0] = clCreateBuffer(context, CL_MEM_READ_WRITE, sbytes, NULL, &err);
  if (! sums[0]) { E(err); }
  sums[1] = clCreateBuffer(context, CL_MEM_READ_WRITE, sbytes, NULL, &err);
  if (! sums[1]) { E(err); }

  int channels = 4;
  int ibytes = w * h * channels * sizeof(float);
  cl_mem image = clCreateBuffer(context, CL_MEM_WRITE_ONLY, ibytes, NULL, &err);
  if (! image) { E(err); }

  int gbytes = 3 * w * h;
  unsigned char *rgb = (unsigned char *) calloc(1, gbytes);
  GLuint texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
  glEnable(GL_TEXTURE_2D);
#ifdef USE_CLGL
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, vbo);
  glBufferData(GL_PIXEL_UNPACK_BUFFER, gbytes, 0, GL_DYNAMIC_DRAW);
  err = glGetError();
  glFinish();
  cl_mem out_rgb = clCreateFromGLBuffer(context, CL_MEM_READ_WRITE, vbo, &err);
  if (! out_rgb) { E(err); }
  cl_event acquired;
  E(clEnqueueAcquireGLObjects(commands, 1, &out_rgb, 0, NULL, &acquired));
  clWaitForEvents(1, &acquired);
#else
  cl_mem out_rgb = clCreateBuffer(context, CL_MEM_READ_WRITE, gbytes, NULL, &err);
  if (! out_rgb) { E(err); }
#endif

  C delta(c - refc);
  F deltaX(R(delta.real() / R(pixelSpacing)));
  F deltaY(R(delta.imag() / R(pixelSpacing)));
  R radius((abs(delta) + abs(C(1.0 * w, 1.0 * h))) * pixelSpacing);

  // print viewing parameters
  mpfr_fprintf(stderr, "# %dx%d\n# re = ", w, h);
  mpfr_out_str(stderr, 10, 0, c.real().backend().data(), MPFR_RNDN);
  mpfr_fprintf(stderr, "\n# im = ");
  mpfr_out_str(stderr, 10, 0, c.imag().backend().data(), MPFR_RNDN);
  mpfr_fprintf(stderr, "\n# @    %g    P%d p%d\n# rre ", csize, period, precount);
  mpfr_out_str(stderr, 10, 0, refc.real().backend().data(), MPFR_RNDN);
  mpfr_fprintf(stderr, "\n# rim ");
  mpfr_out_str(stderr, 10, 0, refc.imag().backend().data(), MPFR_RNDN);
  mpfr_fprintf(stderr, "\n");

  // compute high precision polynomial approximation and round to F53
  {
    C c0(refc);
    C z(c0);

    C a(1.0, 0.0);
    C b(0.0, 0.0);
    C c(0.0, 0.0);
    C d(0.0, 0.0);
    C e(0.0, 0.0);
    C f(0.0, 0.0);
    C g(0.0, 0.0);
    C h(0.0, 0.0);
    C i(0.0, 0.0);
    C j(0.0, 0.0);
    C k(0.0, 0.0);
    C l(0.0, 0.0);
    C aa(0.0, 0.0);
    C bb(0.0, 0.0);
    C cc(0.0, 0.0);
    C dd(0.0, 0.0);
    C ee(0.0, 0.0);
    C ff(0.0, 0.0);
    C gg(0.0, 0.0);
    C hh(0.0, 0.0);
    C ii(0.0, 0.0);
    C jj(0.0, 0.0);
    C kk(0.0, 0.0);
    C ll(0.0, 0.0);

    R r(csize);
    R rr(r);
    R two(2.0);
    C one(1.0, 0.0);
    for (int iii = 0; iii < precount; ++iii) {
      if (iii % period == period - 1) {
        z = C(0.0, 0.0);
      }
      ll = two * (ll*z+a*l+a*kk+aa*k+b*jj+bb*j+c*ii+cc*i+d*hh+dd*h+e*gg+ee*g+f*ff);
      kk = two * (kk*z+a*k+a*jj+aa*j+b*ii+bb*i+c*hh+cc*h+d*gg+dd*g+e*ff+ee*f);
      jj = two * (jj*z+a*j+a*ii+aa*i+b*hh+bb*h+c*gg+cc*g+d*ff+dd*f+e*ee);
      ii = two * (ii*z+a*i+a*hh+aa*h+b*gg+bb*g+c*ff+cc*f+d*ee+dd*e);
      hh = two * (hh*z+a*h+a*gg+aa*g+b*ff+bb*f+c*ee+cc*e+d*dd);
      gg = two * (gg*z+a*g+a*ff+aa*f+b*ee+bb*e+c*dd+cc*d);
      ff = two * (ff*z+a*f+a*ee+aa*e+b*dd+bb*d+c*cc);
      ee = two * (ee*z+a*e+a*dd+aa*d+b*cc+bb*c);
      dd = two * (dd*z+a*d+a*cc+aa*c+b*bb);
      cc = two * (cc*z+a*c+a*bb+aa*b);
      bb = two * (bb*z+a*b+a*aa);
      aa = two * (aa*z+a*a);
      l = two * (l*z+a*k+b*j+c*i+d*h+e*g) + f*f;
      k = two * (k*z+a*j+b*i+c*h+d*g+e*f);
      j = two * (j*z+a*i+b*h+c*g+d*f) + e*e;
      i = two * (i*z+a*h+b*g+c*f+d*e);
      h = two * (h*z+a*g+b*f+c*e) + d*d;
      g = two * (g * z + a * f + b * e + c * d);
      f = two * (f * z + a * e + b * d) + c * c;
      e = two * (e * z + a * d + b * c);
      d = two * (d * z + a * c) + b * b;
      c = two * (c * z + a * b);
      b = two * b * z + a * a;
      a = two * a * z + one;
      z = z * z + c0;
    }
    int idx = 0;
#define S(U,V) \
          pre_zs[idx++]=F(R(U.real()*rr)); \
          pre_zs[idx++]=F(R(U.imag()*rr)); \
          pre_zs[idx++]=F(R(V.real()*rr)); \
          pre_zs[idx++]=F(R(V.imag()*rr)); \
          rr *= r;
    S(a,aa)S(b,bb)S(c,cc)S(d,dd)S(e,ee)S(f,ff)S(g,gg)S(h,hh)S(i,ii)S(j,jj)S(k,kk)S(l,ll)
#undef S
  }
  E(clEnqueueWriteBuffer(commands, in_pre_zs, CL_TRUE, 0, pre_bytes, &pre_zs[0], 0, NULL, NULL));

  // initialize kernel arguments
  cl_int count = w * h;
  size_t local, global;

  E(clSetKernelArg(clear, 0, sizeof(cl_mem), &image));
  E(clSetKernelArg(clear, 1, sizeof(cl_int), &count));
  E(clGetKernelWorkGroupInfo(clear, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL));
  global = ((count + local - 1)/local) * local;
  cl_event cleared;
  E(clEnqueueNDRangeKernel(commands, clear, 1, NULL, &global, &local, 0, NULL, &cleared));

  int state_which = 0;
  E(clSetKernelArg(initialize, 0, sizeof(cl_mem), &state[state_which]));
  E(clSetKernelArg(initialize, 1, sizeof(cl_int), &w));
  E(clSetKernelArg(initialize, 2, sizeof(cl_int), &h));
  E(clSetKernelArg(initialize, 3, sizeof(cl_mem), &in_pre_zs));
  E(clSetKernelArg(initialize, 4, sizeof(F), &deltaX));
  E(clSetKernelArg(initialize, 5, sizeof(F), &deltaY));
  E(clGetKernelWorkGroupInfo(initialize, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL));
  global = ((count + local - 1)/local) * local;
  cl_event initialized;
  E(clEnqueueNDRangeKernel(commands, initialize, 1, NULL, &global, &local, 1, &cleared, &initialized));

  E(clSetKernelArg(iterate, 0, sizeof(cl_mem), &in_per_zs));
  E(clSetKernelArg(iterate, 3, sizeof(cl_mem), &keep));
  E(clSetKernelArg(iterate, 4, sizeof(cl_mem), &image));
  E(clSetKernelArg(iterate, 5, sizeof(cl_int), &count));
  E(clSetKernelArg(iterate, 6, sizeof(cl_int), &w));
  E(clSetKernelArg(iterate, 7, sizeof(cl_int), &h));
  E(clSetKernelArg(iterate, 8, sizeof(F), &deltaX));
  E(clSetKernelArg(iterate, 9, sizeof(F), &deltaY));
  E(clSetKernelArg(iterate, 10, sizeof(F), &pixelSpacing));
  E(clSetKernelArg(iterate, 12, sizeof(cl_int), &stepIters));
  E(clSetKernelArg(iterate, 13, sizeof(cl_int), &period));
  
  E(clSetKernelArg(permute, 1, sizeof(cl_mem), &keep));
  E(clSetKernelArg(permute, 5, sizeof(cl_int), &elements));
  
  cl_event caniterate = initialized;
  iters = precount;

  // iterate until rate of escapes is low
  bool anyEscape = false;
  bool recentEscape = false;
  cl_int targetIters = iters + stepIters;
  cl_int oldCount = count;
  bool done = false;
  while (! done) {
    fprintf(stderr, "%16d%16d%16d\n", count, iters, state_which);

    E(clSetKernelArg(iterate, 1, sizeof(cl_mem), &state[state_which]));
    E(clSetKernelArg(iterate, 2, sizeof(cl_mem), &state[1 - state_which]));
    E(clSetKernelArg(permute, 0, sizeof(cl_mem), &state[1 - state_which]));
    E(clSetKernelArg(permute, 3, sizeof(cl_mem), &state[state_which]));

    // iterate
    cl_int zero = 0;
    E(clEnqueueWriteBuffer(commands, keep, CL_TRUE, count * sizeof(cl_int), sizeof(cl_int), &zero, 0, NULL, NULL));
    E(clSetKernelArg(iterate, 5, sizeof(cl_int), &count));
    E(clSetKernelArg(iterate, 11, sizeof(cl_int), &iters));
    E(clGetKernelWorkGroupInfo(iterate, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL));
    global = ((count + local - 1)/local) * local;
    cl_event iterated;
    E(clEnqueueNDRangeKernel(commands, iterate, 1, NULL, &global, &local, 1, &caniterate, &iterated));
    cl_int any_escaped = 0;
    E(clEnqueueReadBuffer(commands, keep, CL_TRUE, count * sizeof(cl_int), sizeof(cl_int), &any_escaped, 1, &iterated, NULL));
    iters += stepIters;

    // compact working set if necessary, otherwise ping pong state buffers
    if (any_escaped) {
      // prefix sum
      E(clSetKernelArg(prefixsum, 0, sizeof(cl_mem), &keep));
      E(clSetKernelArg(prefixsum, 2, sizeof(cl_int), &count));
      E(clGetKernelWorkGroupInfo(prefixsum, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL));
      global = ((count + local - 1)/local) * local;
      int pswhich = 0;
      cl_event lastsummed = iterated;
      for (cl_int passbit = 1; passbit <= count; passbit <<= 1) {
        E(clSetKernelArg(prefixsum, 1, sizeof(cl_mem), &sums[pswhich]));
        E(clSetKernelArg(prefixsum, 3, sizeof(cl_int), &passbit));
        cl_event summed;
        E(clEnqueueNDRangeKernel(commands, prefixsum, 1, NULL, &global, &local, 1, &lastsummed, &summed));
        lastsummed = summed;
        E(clSetKernelArg(prefixsum, 0, sizeof(cl_mem), &sums[pswhich]));
        E(clSetKernelArg(permute, 2, sizeof(cl_mem), &sums[pswhich]));
        pswhich = 1 - pswhich;
      }
      // permute
      E(clSetKernelArg(permute, 4, sizeof(cl_int), &count));
      E(clGetKernelWorkGroupInfo(permute, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL));
      global = ((count + local - 1)/local) * local;
      cl_event permuted;
      E(clEnqueueNDRangeKernel(commands, permute, 1, NULL, &global, &local, 1, &lastsummed, &permuted));
      // read back count
      E(clEnqueueReadBuffer(commands, sums[1 - pswhich], CL_TRUE, (count - 1) * sizeof(cl_int), sizeof(cl_int), &count, 1, &lastsummed, NULL));
      caniterate = permuted;
    } else {
      state_which = 1 - state_which;
      caniterate = iterated;
    }

    // check if done
    if (count < oldCount) {
      anyEscape = true;
      recentEscape = true;
    }
    done = count == 0;
    if (targetIters <= iters) {
      done = count == 0 || (anyEscape ? !recentEscape : false);
      oldCount = count;
      recentEscape = false;
      targetIters <<= 1;
    }
    if (maxIters <= iters) {
      done = true;
    }
  }

  // colourize
  count = w * h;
  E(clSetKernelArg(colour, 0, sizeof(cl_mem), &image));
  E(clSetKernelArg(colour, 1, sizeof(cl_mem), &out_rgb));
  E(clSetKernelArg(colour, 2, sizeof(cl_int), &count));
  E(clGetKernelWorkGroupInfo(colour, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL));
  global = ((count + local - 1)/local) * local;
  cl_event coloured;

  // copy image to OpenGL
#ifdef USE_CLGL
  E(clEnqueueNDRangeKernel(commands, colour, 1, NULL, &global, &local, 1, &acquired, &coloured));
  cl_event released;
  E(clEnqueueReleaseGLObjects(commands, 1, &out_rgb, 1, &coloured, &released));
  clWaitForEvents(1, &released);
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, 0);
#else
  E(clEnqueueNDRangeKernel(commands, colour, 1, NULL, &global, &local, 1, &caniterate, &coloured));
  E(clEnqueueReadBuffer(commands, out_rgb, CL_TRUE, 0, gbytes, rgb, 1, &coloured, 0));
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, rgb);
#endif

  // display image
  glBegin(GL_QUADS); {
#define V(x,y) glTexCoord2f(x, y);glVertex2f(2*x-1, 2*y-1);
    V(0,0)V(1,0)V(1,1)V(0,1)
#undef V
  } glEnd();
  glfwSwapBuffers(window);

  // copy image to memory
  cl_event readrgb;
#ifdef USE_CLGL
  glBindTexture(GL_TEXTURE_2D, 0);
  glFinish();
  cl_event acquired2;
  E(clEnqueueAcquireGLObjects(commands, 1, &out_rgb, 0, NULL, &acquired2));
  E(clEnqueueReadBuffer(commands, out_rgb, CL_TRUE, 0, gbytes, &rgb[0], 1, &acquired2, &readrgb));
  E(clEnqueueReleaseGLObjects(commands, 1, &out_rgb, 1, &readrgb, NULL));
  glBindTexture(GL_TEXTURE_2D, texture);
#else
  E(clEnqueueReadBuffer(commands, out_rgb, CL_TRUE, 0, gbytes, &rgb[0], 0, NULL, &readrgb));
#endif

  // output PPM on stdout
  mpfr_fprintf(stdout, "P6\n%d %d\n# re = ", w, h);
  mpfr_fprintf(stdout, "# %dx%d\n# re = ", w, h);
  mpfr_out_str(stdout, 10, 0, c.real().backend().data(), MPFR_RNDN);
  mpfr_fprintf(stdout, "\n# im = ");
  mpfr_out_str(stdout, 10, 0, c.imag().backend().data(), MPFR_RNDN);
  mpfr_fprintf(stdout, "\n# @    %g    P%d p%d\n# rre ", csize, period, precount);
  mpfr_out_str(stdout, 10, 0, refc.real().backend().data(), MPFR_RNDN);
  mpfr_fprintf(stdout, "\n# rim ");
  mpfr_out_str(stdout, 10, 0, refc.imag().backend().data(), MPFR_RNDN);
  mpfr_fprintf(stdout, "\n255\n");
  fwrite(rgb, gbytes, 1, stdout);
  fflush(stdout);

  // clean up
  free(rgb);
  free(pre_zs);
  free(per_zs);
  delete ref;
  clReleaseMemObject(in_pre_zs);
  clReleaseMemObject(in_per_zs);
  clReleaseMemObject(state[0]);
  clReleaseMemObject(state[1]);
  clReleaseMemObject(keep);
  clReleaseMemObject(sums[0]);
  clReleaseMemObject(sums[1]);
  clReleaseMemObject(image);
  clReleaseMemObject(out_rgb);
  clReleaseProgram(program);
  clReleaseKernel(initialize);
  clReleaseKernel(iterate);
  clReleaseKernel(prefixsum);
  clReleaseKernel(permute);
  clReleaseKernel(colour);
  clReleaseCommandQueue(commands);
  clReleaseContext(context);
  glfwTerminate();
  return 0;
}
