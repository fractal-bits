// mandelbrot-delta-cl -- Mandelbrot set perturbation renderer using OpenCL
// Copyright: (C) 2013 Claude Heiland-Allen <claude@mathr.co.uk>
// License: http://www.gnu.org/licenses/gpl.html GPLv3+

// some systems list available platforms in different orders
// some platforms do not support double precision
// change this line to suit your system's platforms
// platforms are listed to stderr on startup
#define PLATFORM 0

#include <complex>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <boost/multiprecision/mpfr.hpp>
#include <CL/cl.h>
#include "cl-extra.cc"

using namespace boost::multiprecision;
typedef mpfr_float R;
typedef std::complex<R> C;
typedef double F53;
typedef std::complex<F53> C53;

// the OpenCL kernel

#define CL(cl) "#pragma OPENCL EXTENSION cl_khr_fp64: require\n" #cl

const char *src = CL(

__kernel void delta
  ( __global double* zs
  , const unsigned int period
  , __global float* output // { normalizeddistanceestimate, continuousdwell, atomperioddomain }
  , const unsigned int w
  , const unsigned int h
  , const unsigned int j
  , const double r
  , const unsigned int chunk
  ) {
  int i = get_global_id(0);
  int x = i % w;
  int y = i / w;
  if (y < chunk) {
    // d0 <- pixel coordinates
    double ii = x;
    double jj = j + y;
    double pixelSpacing = 16.0 * pow(r / 64.0, jj / h) - 16.0 * pow(r / 64.0, (jj+1) / h);
    double d0x = cos(2.0 * 3.141592653 * ii / w) * 16.0 * pow(r / 64.0, jj / h); //(ii - w/2) * r / (h/2);
    double d0y = sin(2.0 * 3.141592653 * ii / w) * 16.0 * pow(r / 64.0, jj / h); //(jj - h/2) * r / (h/2);
    // d <- d0
    double dx = d0x;
    double dy = d0y;
    // dydc <- 1
    double dydcx = 1.0;
    double dydcy = 0.0;
    double er2 = 65536.0;
    float distance = -1.0;
    float dwell = -1.0;
    float atomperiod = 1.0;
    double my2 = er2;
    for (int k = 0; k < 0x1000; ++k) { // FIXME hardcoded maxiters
      int p = k % period;
      double zx = zs[2*p+0];
      double zy = zs[2*p+1];
      // y <- z + d
      double yx = zx + dx;
      double yy = zy + dy;
      double y2 = yx * yx + yy * yy;
      double ly2 = log(y2);
      if (! (y2 <= er2)) {
        // distance <- |y| log|y| / |dydc|
        double y = sqrt(y2);
        double dydc2 = dydcx * dydcx + dydcy * dydcy;
        double dydc = sqrt(dydc2);
        distance = y * ly2/2.0 / (dydc * pixelSpacing) / 4.0;
        dwell = k - log2(ly2 / log(er2));
        break;
      }
//      ly2 -= 0.5 * (k + 1);
      if (ly2 < my2) {// && (k + 1) % period != 0) {
        my2 = ly2;
        atomperiod = k + 1.0;
      }
      // d <- 2 z d + d d + d0
      double tx = 2.0 * (zx * dx - zy * dy) + (dx * dx - dy * dy) + d0x;
      double ty = 2.0 * (zx * dy + zy * dx) + (2.0 * dx * dy) + d0y;
      // dydc <- 2 y dydc + 1
      double sx = 2.0 * (yx * dydcx - yy * dydcy) + 1.0;
      double sy = 2.0 * (yx * dydcy + yy * dydcx);
      dx = tx;
      dy = ty;
      dydcx = sx;
      dydcy = sy;
    }
    output[3*i+0] = distance;
    output[3*i+1] = dwell;
    output[3*i+2] = atomperiod;
  }
}

);

// the main program

int main(int argc, char **argv) {

  // read command line arguments
  if (argc != 7) {
    fprintf(stderr, "usage: %s width height real imag size period > out.pgm", argv[0]);
    return 1;
  }
  unsigned int w(atoi(argv[1]));
  unsigned int period(atoi(argv[6]));
  F53 r(atof(argv[5]));
  R::default_precision(10 - log10(r));
  C c = C(R(argv[3]), R(argv[4]));
  unsigned int h = w * (-log(r / 64.0) / (2.0 * 3.141592653)); //(atoi(argv[2]));
  unsigned int chunk = (((1 << 16) - 1) + w) / w;

  // print viewing parameters
  mpfr_fprintf(stderr, "# %dx%d\n# re = ", w, h);
  mpfr_out_str(stderr, 10, 0, c.real().backend().data(), MPFR_RNDN);
  mpfr_fprintf(stderr, "\n# im = ");
  mpfr_out_str(stderr, 10, 0, c.imag().backend().data(), MPFR_RNDN);
  mpfr_fprintf(stderr, "\n# @    %g\n# P    %d\n", r, period);

  // compute high precision zs and round to F53
  int zbytes = sizeof(F53) * 2 * period;
  F53 *zs = (F53 *) calloc(1, zbytes);
  C z(0.0, 0.0);
  for (unsigned int i = 0; i < period; ++i) {
    z = z * z + c;
    zs[2*i+0] = F53(z.real());
    zs[2*i+1] = F53(z.imag());
  }

  // enumerate OpenCL platforms
  cl_platform_id platform_id[64];
  cl_uint platform_ids;
  E(clGetPlatformIDs(64, &platform_id[0], &platform_ids));
  for (cl_uint i = 0; i < platform_ids; ++i) {
    fprintf(stderr, "platform %d\n", i);
    char buf[1024];
    cl_platform_info info[5] =
      { CL_PLATFORM_PROFILE
      , CL_PLATFORM_VERSION
      , CL_PLATFORM_NAME
      , CL_PLATFORM_VENDOR
      , CL_PLATFORM_EXTENSIONS
      };
    for (int w = 0; w < 5; ++w) {
      E(clGetPlatformInfo(platform_id[i], info[w], 1024, &buf[0], NULL));
      fprintf(stderr, "\t%s\n", buf);
    }
  }

  // create context
  cl_device_id device_id;
  E(clGetDeviceIDs(platform_id[PLATFORM], CL_DEVICE_TYPE_ALL, 1, &device_id, NULL));
  cl_context_properties properties[] =
    { CL_CONTEXT_PLATFORM, (cl_context_properties) platform_id[PLATFORM]
    , 0
    };
  int err;
  cl_context context = clCreateContext(properties, 1, &device_id, NULL, NULL, &err);
  if (! context) { E(err); }
  cl_command_queue commands = clCreateCommandQueue(context, device_id, 0, &err);
  if (! commands) { E(err); }

  // compile program
  cl_program program = clCreateProgramWithSource(context, 1, &src, NULL, &err);
  if (! program) { E(err); }
  err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
  if (err != CL_SUCCESS) {
    char buf[1024]; buf[0] = 0;
    clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 1024, &buf[0], NULL);
    fprintf(stderr, "build failed:\n%s\n", buf);
    E(err);
  }
  cl_kernel kernel = clCreateKernel(program, "delta", &err);
  if (! kernel) { E(err); }

  // create buffers
  unsigned int count = w * chunk;
  cl_mem in_zs = clCreateBuffer(context, CL_MEM_READ_ONLY, zbytes, NULL, NULL);
  if (! in_zs) { exit(1); }
  cl_mem out_output = clCreateBuffer(context, CL_MEM_WRITE_ONLY, 3*count*sizeof(float), NULL, NULL);
  if (! out_output) { exit(1); }
  E(clEnqueueWriteBuffer(commands, in_zs, CL_TRUE, 0, zbytes, &zs[0], 0, NULL, NULL));
  unsigned int obytes = 3 * count * ((h + chunk - 1)/chunk) * sizeof(float);
  float *output = (float *) calloc(1, obytes);

  // set arguments
  unsigned int j = 0;
  E(clSetKernelArg(kernel, 0, sizeof(cl_mem), &in_zs));
  E(clSetKernelArg(kernel, 1, sizeof(unsigned int), &period));
  E(clSetKernelArg(kernel, 2, sizeof(cl_mem), &out_output));
  E(clSetKernelArg(kernel, 3, sizeof(unsigned int), &w));
  E(clSetKernelArg(kernel, 4, sizeof(unsigned int), &h));
  E(clSetKernelArg(kernel, 5, sizeof(unsigned int), &j));
  E(clSetKernelArg(kernel, 6, sizeof(double), &r));
  E(clSetKernelArg(kernel, 7, sizeof(unsigned int), &chunk));
  size_t local;
  E(clGetKernelWorkGroupInfo(kernel, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL));
  size_t global = ((count + local - 1)/local) * local;

  // run the kernel for each row
  for (j = 0; j < h; j += chunk) {
    fprintf(stderr, "%8d\r", j);
    E(clSetKernelArg(kernel, 5, sizeof(unsigned int), &j));
    E(clEnqueueNDRangeKernel(commands, kernel, 1, NULL, &global, &local, 0, NULL, NULL));
    E(clEnqueueReadBuffer(commands, out_output, CL_TRUE, 0, 3*w*chunk*sizeof(float), &output[3*w*j], 0, NULL, NULL));
  }
  clFinish(commands);

/*
  // analyse data
  FILE *fatom = fopen("atom.dat", "wb");
  for (unsigned int i = 2; i < 3 * w * h; i += 3) {
    fprintf(fatom, "%d\n", int(output[i]));
  }
  fclose(fatom);
*/
  // convert to image
  unsigned char *rgb = (unsigned char *) calloc(1, 3 * w * h);
//  unsigned char *grey = (unsigned char *) calloc(1, w * h);
  float pi = 3.141592653;
  float phi = (sqrt(5.0) + 1.0) / 2.0;
  float modulus = 24.0 + 1.0/(phi*phi);
  for (unsigned int i = 0; i < 3 * w * h; i += 3) {
//    grey[i/3] = fmin(fmax(255.0 * sqrt(output[i+0]), 0.0), 255.0);
    float value = fmin(fmax(sqrt(output[i+0]), 0.0), 1.0) * 0.5 + 0.125;
    float chroma = 2.0 * pi * fmod(output[i+2], modulus)/modulus;
    float y = value;
    float u = 0.5 * value * cos(chroma);
    float v = 0.5 * value * sin(chroma);
    float r = y + 1.13983 * v;
    float g = y - 0.39465 * u - 0.58060 * v;
    float b = y + 2.03211 * u;
    if (output[i+0] < 0.0 || output[i+0] > w) {
      rgb[i+0] = rgb[i+1] = rgb[i+2] = 255;
    } else {
      rgb[i+0] = fmin(fmax(r * 255.0, 0.0), 255.0);
      rgb[i+1] = fmin(fmax(g * 255.0, 0.0), 255.0);
      rgb[i+2] = fmin(fmax(b * 255.0, 0.0), 255.0);
    }
  }

/*
  FILE *fdist = fopen("dist.dat", "wb");
  for (unsigned int i = 0; i < 3 * w * h; i += 3) {
    fprintf(fdist, "%e\n", output[i+0]);
  }
  fclose(fdist);
*/
/*
  FILE *fraw = fopen("out.raw", "wb");
  fwrite(output, obytes, 1, fraw);
  fclose(fraw);
*/
/*
  FILE *fgrey = fopen("out.pgm", "wb");
  fprintf(fgrey, "P5\n%d %d\n255\n", w, h);
  fwrite(grey, w * h, 1, fgrey);
  fclose(fgrey);
*/
  // output PPM data on stdout
  mpfr_fprintf(stdout, "P6\n%d %d\n# re = ", w, h);
  mpfr_out_str(stdout, 10, 0, c.real().backend().data(), MPFR_RNDN);
  mpfr_fprintf(stdout, "\n# im = ");
  mpfr_out_str(stdout, 10, 0, c.imag().backend().data(), MPFR_RNDN);
  mpfr_fprintf(stdout, "\n# @    %g\n# P    %d\n255\n", r, period);
  fwrite(rgb, 3 * w * h, 1, stdout);

  // clean up
  clReleaseMemObject(in_zs);
  clReleaseMemObject(out_output);
  clReleaseProgram(program);
  clReleaseKernel(kernel);
  clReleaseCommandQueue(commands);
  clReleaseContext(context);

  return 0;
}
