#define _GNU_SOURCE
#define _FILE_OFFSET_BITS 64
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#ifndef MAP_HUGE_2MB
#define MAP_HUGE_2MB (21 << MAP_HUGE_SHIFT)
#endif
#ifndef MAP_HUGE_1GB
#define MAP_HUGE_1GB (30 << MAP_HUGE_SHIFT)
#endif

#include <cstdlib>
#include <ctime>
#include <atomic>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iterator>
#include <vector>

#include <glm/glm.hpp>


float srgb(float c)
{
  c = glm::clamp(c, 0.0f, 1.0f);
  const float a = 0.055;
  if (c <= 0.0031308)
    return 12.92 * c;
  else
    return (1.0 + a) * std::pow(c, 1.0 / 2.4) - a;
}

glm::vec3 srgb(const glm::vec3 &c)
{
  return glm::vec3(srgb(c.x), srgb(c.y), srgb(c.z));
}

uint32_t hash(uint32_t a)
{
  a = (a+0x7ed55d16u) + (a<<12u);
  a = (a^0xc761c23cu) ^ (a>>19u);
  a = (a+0x165667b1u) + (a<<5u);
  a = (a+0xd3a2646cu) ^ (a<<9u);
  a = (a+0xfd7046c5u) + (a<<3u);
  a = (a^0xb55a4f09u) ^ (a>>16u);
  return a;
}

float uniform(uint32_t h)
{
  return h / (float) UINT_MAX * 2 - 1;
}

unsigned char dither8(float c, uint32_t h)
{
  return std::floor(glm::clamp(float(255 * c + h / (float) UINT_MAX), 0.f, 255.f));
}

glm::vec4 unit_quaternion(uint32_t seed)
{
  glm::vec4 q;
  uint32_t k = 0;
  do
  {
    q = glm::vec4(uniform(hash((k + 0) ^ seed)), uniform(hash((k + 1) ^ seed)), uniform(hash((k + 2) ^ seed)), uniform(hash((k + 3) ^ seed)));
    k += 4;
  } while (glm::length(q) > 1);
  return glm::normalize(q);
}

glm::mat4 quaternion_matrix_l(const glm::vec4 &q)
{
  return glm::mat4
    ( q.x, -q.y, -q.z, -q.w
    , q.y,  q.x, -q.w,  q.z
    , q.z,  q.w,  q.x, -q.y
    , q.w, -q.z,  q.y,  q.x
    );
}

glm::mat4 quaternion_matrix_r(const glm::vec4 &q)
{
  return glm::mat4
    ( q.x, -q.y, -q.z, -q.w
    , q.y,  q.x,  q.w, -q.z
    , q.z, -q.w,  q.x,  q.y
    , q.w,  q.z, -q.y,  q.x
    );
}

size_t z_order(size_t x, size_t y, size_t z, size_t w)
{
  size_t o = 0;
  for (size_t i = 0; i < sizeof(x) * CHAR_BIT / 4; i++)
  {
    o |= (x & 1LLU << i) << (3 * i + 0)
      |  (y & 1LLU << i) << (3 * i + 1)
      |  (z & 1LLU << i) << (3 * i + 2)
      |  (w & 1LLU << i) << (3 * i + 3)
      ;
  }
  return o;
}

int main(int argc, char **argv)
{
  if (! (argc > 1))
  {
    std::fprintf(stderr, "usage: %s level\n", argv[0]);
    return 1;
  }
  uint32_t seed0 = std::time(0);
  std::fprintf(stderr, "seed %08x\n", seed0);
  size_t n = std::atoll(argv[1]);
  size_t bytes = 2LLU << (4 * n - 3);
  std::ostringstream blackfile;
  blackfile << n << "/black.raw";
  int fd_black = open(blackfile.str().c_str(), O_RDONLY | O_NOATIME);
  if (fd_black < 0)
  {
    fprintf(stderr, "could not open %s\n", blackfile.str().c_str());
    return 1;
  }
  std::ostringstream grayfile;
  grayfile << n << "/gray.raw";
  int fd_gray = open(grayfile.str().c_str(), O_RDONLY | O_NOATIME);
  if (fd_gray < 0)
  {
    fprintf(stderr, "could not open %s\n", grayfile.str().c_str());
    return 1;
  }
  unsigned char *voxels;
  voxels = (unsigned char *) mmap(nullptr, bytes, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_HUGETLB | MAP_HUGE_1GB | MAP_ANONYMOUS, -1, 0);
  if (voxels == MAP_FAILED)
  {
    fprintf(stderr, "could not mmap with 1GB pages\n");
    voxels = (unsigned char *) mmap(nullptr, bytes, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_HUGETLB | MAP_HUGE_2MB | MAP_ANONYMOUS, -1, 0);
    if (voxels == MAP_FAILED)
    {
      fprintf(stderr, "could not mmap with 2MB pages\n");
      voxels = (unsigned char *) mmap(nullptr, bytes, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
      if (voxels == MAP_FAILED)
      {
        fprintf(stderr, "could not mmap at all\n");
        return 1;
      }
      else
      {
        fprintf(stderr, "did mmap with 4kB pages\n");
      }
    }
    else
    {
      fprintf(stderr, "did mmap with 2MB pages\n");
    }
  }
  else
  {
    fprintf(stderr, "did mmap with 1GB pages\n");
  }
  int fd[2] = { fd_black, fd_gray };
  for (int file = 0; file < 2; ++file)
  {
    size_t read_bytes = 0;
    do
    {
      /*
       On  Linux,  read()  (and  similar  system  calls) will transfer at most
       0x7ffff000 (2,147,479,552) bytes, returning the number of  bytes  actu-
       ally transferred.  (This is true on both 32-bit and 64-bit systems.)
      */
      ssize_t result = read(fd[file], voxels + (bytes >> 1) * file + read_bytes, (bytes >> 1) - read_bytes);
      if (result <= 0)
      {
        break;
      }
      read_bytes += result;
    } while (read_bytes < (bytes >> 1));
    close(fd[file]);
    if (read_bytes != (bytes >> 1))
    {
      fprintf(stderr, "did not read voxel data %ld != %lu\n", read_bytes, bytes);
      return 1;
    }
  }
  const glm::vec4 eye(0, 0, 0, -8);
  const glm::mat4 ql(quaternion_matrix_l(unit_quaternion(hash(-1 ^ seed0))));
  const glm::mat4 qr(quaternion_matrix_r(unit_quaternion(hash(-2 ^ seed0))));
  const float xmin = -2, xmax = 2;
  const float ymin = -2, ymax = 2;
  const float zmin = -2, zmax = 2;
  const float wmin = -2, wmax = 2;
  const size_t x_tiles = 16, y_tiles = 9;
  const size_t x_steps = 120*2, y_steps = 120*2;
  const size_t z_steps = x_tiles * y_tiles;
  const size_t w_steps = 1 << n;
  const size_t image_width  = x_tiles * x_steps;
  const size_t image_height = y_tiles * y_steps;
  const size_t subframes = 1;
  const float f50 = std::pow(0.01f, 1.0f / w_steps);
  const float f75 = std::pow(0.1f,  1.0f / w_steps);
  glm::vec4 colours[4] =
    { glm::vec4(f50, f75, 1.f, 1.f) // "black"  -> blue
    , glm::vec4(1.f, f75, f50, 1.f) // "gray"   -> orange
    , glm::vec4(1.f, 1.f, 1.f, 1.f) // "white"  -> white
    , glm::vec4(1.f, 0.f, 0.f, 1.f) // "marked" -> red
    };
  size_t m = 1LLU << n;
  std::vector<unsigned char> raster;
  raster.reserve(image_width * image_height * 3);
  for (size_t frame = 0; frame < 360; frame += 3)
  {
    for (size_t tile_y = 0; tile_y < y_tiles; ++tile_y)
    {
      #pragma omp parallel for
      for (size_t sub_y = 0; sub_y < y_steps; ++sub_y)
      {
        for (size_t tile_x = 0; tile_x < x_tiles; ++tile_x)
        {
          for (size_t sub_x = 0; sub_x < x_steps; ++sub_x)
          {
            size_t sub_z = tile_y * x_tiles + tile_x;
            glm::vec4 accumulated(0, 0, 0, 0);
            uint32_t seed1 =
                hash(sub_x ^
                hash(tile_x ^
                hash(sub_y ^
                hash(tile_y ^
                hash(frame ^
                hash(seed0))))));
            for (size_t subframe = 0; subframe < subframes; ++subframe)
            {
              uint32_t seed = hash(subframe ^ seed1);
              float t = glm::radians(float(frame + uniform(hash(1 ^ seed))));
              float co = std::cos(t);
              float si = std::sin(t);
              glm::mat4 rot
                ( 1.0f, 0.0f, 0.0f, 0.0f
                , 0.0f, 1.0f, 0.0f, 0.0f
                , 0.0f, 0.0f,  co, si
                , 0.0f, 0.0f, -si, co
                );
              float x = glm::mix(xmin, xmax, (sub_x + uniform(hash(2 ^ seed))) / x_steps);
              float y = glm::mix(ymin, ymax, (sub_y + uniform(hash(3 ^ seed))) / y_steps);
              float z = glm::mix(zmin, zmax, (sub_z + uniform(hash(4 ^ seed))) / z_steps);
              glm::vec4 target(x, y, z, 0);
              glm::vec4 dir = glm::normalize(target - eye);
              // (eye + begin * dir).w = w
              float begin = (wmin - eye.w) / dir.w;
              float end   = (wmax - eye.w) / dir.w;
              glm::vec4 colour(1, 1, 1, 1);
              for (size_t sub_w = 0; sub_w < w_steps; ++sub_w)
              {
                float d = glm::mix(begin, end, (sub_w + uniform(hash((5 + sub_w) ^ seed))) / w_steps);
                glm::vec4 q(eye + d * dir);
                glm::vec4 p(rot * ql * q * qr);
                glm::vec4 r(float(m) * (p / 2.0f + glm::vec4(1.f, 1.f, 1.f, 1.f)) / 2.0f);
                if ( 0 <= r.x && r.x < m &&
                     0 <= r.y && r.y < m &&
                     0 <= r.z && r.z < m &&
                     0 <= r.w && r.w < m )
                {
                  const size_t i = r.x;
                  const size_t j = r.y;
                  const size_t k = r.z;
                  const size_t l = r.w;
                  const size_t ix = z_order(i, j, k, l);
                  const size_t byte = ix >> 3;
                  const size_t bit = ix & 7;
                  const unsigned char mask = 1u << bit;
                  const bool is_black = !!(voxels[byte               ] & mask);
                  const bool is_gray  = !!(voxels[byte + (bytes >> 1)] & mask);
                  const int c = is_black ? 0 : is_gray ? 1 : 2;
                  colour *= colours[c];
                }
              }
              accumulated += colour;
            }
            glm::vec3 rgb = srgb(glm::vec3(accumulated) / accumulated.w);
            size_t i = tile_x * x_steps + sub_x;
            size_t j = tile_y * y_steps + sub_y;
            size_t k = 3 * (image_width * j + i);
            raster[k++] = dither8(rgb.x, hash((subframes + 0) ^ seed1));
            raster[k++] = dither8(rgb.y, hash((subframes + 1) ^ seed1));
            raster[k++] = dither8(rgb.z, hash((subframes + 2) ^ seed1));
          }
        }
      }
    }
    std::fprintf(stdout, "P6\n%lu %lu\n255\n", image_width, image_height);
    std::fwrite(&raster[0], image_width * image_height * 3, 1, stdout);
    std::fflush(stdout);
  }
  munmap(voxels, bytes);
  return 0;
}
