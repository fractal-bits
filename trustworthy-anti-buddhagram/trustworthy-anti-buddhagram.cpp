#include <algorithm>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>

typedef float real;
real sqr(const real &x)
{
  return x * x;
}

template <typename R>
class complex
{
  R x, y;
public:
  complex(const R &x, const R &y)
  : x(x), y(y)
  { }
  const R &real() const
  {
    return x;
  }
  const R &imag() const
  {
    return y;
  }
};

template <typename R>
complex<R> sqr(const complex<R> &z)
{
  return complex<R>(sqr(z.real()) - sqr(z.imag()), 2 * z.real() * z.imag());
}

template <typename R>
complex<R> operator+(const complex<R> &a, const complex<R> &b)
{
  return complex<R>(a.real() + b.real(), a.imag() + b.imag());
}

template <typename R>
complex<R> operator-(const complex<R> &a, const complex<R> &b)
{
  return complex<R>(a.real() - b.real(), a.imag() - b.imag());
}

enum color
{
  black  = 0,
  gray   = 1,
  white  = 2,
  marked = 3
};

const color unmarked[4] = { black, black, white, gray };
const color inherit[4] = { black, gray, white, marked };
const unsigned char colorrgb[4][3] = { { 0, 0, 0 }, { 128, 128, 128 }, { 255, 255, 255 }, { 255, 0, 0 } };

class index
{
public:
  size_t x, y;
  index(size_t x, size_t y)
  : x(x), y(y)
  {
  }
  bool operator<(const index &that) const
  {
    return
      x != that.x
    ? x <  that.x
    : y != that.y
    ? y <  that.y
    : false; // equal
  }
  bool operator!=(const index &that) const
  {
    return *this < that || that < *this;
  }
  size_t operator()(size_t n) const
  {
    return (x << n) + y;
  }
};

const real infinity = 1.0 / 0.0;

class range
{
  real r[2];
public:
  range() // empty
  {
    r[0] = infinity;
    r[1] = -infinity;
  }
  range(const real &a)
  {
    r[0] = a;
    r[1] = a;
  }
  range(const real &a, const real &b)
  {
    r[0] = a;
    r[1] = b;
  }
  const real &min() const
  {
    return r[0];
  }
  const real &max() const
  {
    return r[1];
  }
  bool operator<(const range &that) const
  {
    return
      min() != that.min()
    ? min() <  that.min()
    : max() != that.max()
    ? max() <  that.max()
    : false;
  }
  bool operator!=(const range &that) const
  {
    return *this < that || that < *this;
  }
};

bool contains(const range &a, const real &x)
{
  return a.min() <= x && x <= a.max();
}

real midpoint(const range &a)
{
  return (a.max() + a.min()) / 2;
}

real radius(const range &a)
{
  return (a.max() - a.min()) / 2;
}

range midpoint_radius(const real &a, const real &r)
{
  return range(a - r, a + r);
}

range abs(const range &a)
{
  auto x = std::abs(a.min());
  auto y = std::abs(a.max());
  return range(std::min(x, y), std::max(x, y));
}

range operator|(const range &a, const range &b)
{
  return range(std::min(a.min(), b.min()), std::max(a.max(), b.max()));
}

range operator+(const range &a, const range &b)
{
  return range(a.min() + b.min(), a.max() + b.max());
}

range operator-(const range &a)
{
  return range(-a.max(), -a.min());
}

range operator-(const range &a, const range &b)
{
  return a + -b;
}

range operator/(const range &a, const int &b)
{
  // precondition: b is a power of two
  return range(a.min() / b, a.max() / b);
}

range operator*(const int &b, const range &a)
{
  // precondition: b is a power of two
  return range(b * a.min(), b * a.max());
}

range operator*(const range &a, const range &b)
{
  // pessimisation by 1ulp if multiplication is exact
  real x = a.min() * b.min();
  real y = a.min() * b.max();
  real z = a.max() * b.min();
  real w = a.max() * b.max();
  real mi = std::min(std::min(x, y), std::min(z, w));
  real ma = std::max(std::max(x, y), std::max(z, w));
  return range(mi, ma);
#if 0
    ( std::nextafter(mi, -infinity)
    , std::nextafter(ma,  infinity)
    );
#endif
}

range operator/(const range &x, const range &y)
{
  // pessimisation by 1ulp if division is exact
  if (0 <= y.min())
    return range(std::nextafter(x.min() / y.max(), -infinity), std::nextafter(x.max() / y.min(), infinity));
  else if (y.max() <= 0)
    return range(std::nextafter(x.min() / y.min(), -infinity), std::nextafter(x.max() / y.max(), infinity));
  else
    return range(-infinity, infinity); // division by 0
}

range sqr(const range &a)
{
  auto lo = a.min();
  auto hi = a.max();
  auto lo2 = sqr(lo);
  auto hi2 = sqr(hi);
  auto olo = std::min(lo2, hi2);
  auto ohi = std::max(lo2, hi2);
  if (lo <= 0 && 0 <= hi) olo = 0;
  return range(olo, ohi);
}

class box
{
  range b[2];
public:
  box() // empty
  {
    b[0] = range();
    b[1] = range();
  }
  box(const range &x, const range &y)
  {
    b[0] = x;
    b[1] = y;
  }
  const range &x() const { return b[0]; }
  const range &y() const { return b[1]; }
  bool operator<(const box &that) const
  {
    return
      x() != that.x()
    ? x() <  that.x()
    : y() != that.y()
    ? y() <  that.y()
    : false; // equal
  }
  bool operator!=(const box &that)
  {
    return *this < that || that < *this;
  }
};

bool inside(const range &a, const range &b)
{
  return b.min() < a.min() && a.max() < b.max();
}

bool inside(const box &a, const box &b)
{
  return
    inside(a.x(), b.x()) &&
    inside(a.y(), b.y());
}

box operator|(const box &a, const box &b)
{
  return box(a.x() | b.x(), a.y() | b.y());
}

typedef std::vector<box> (*function)(const box &, const box &);

typedef std::pair<function, function> bifunction;

std::vector<box> quadratic_forward(const box &zb, const box &cb)
{
  complex<range> z(zb.x(), zb.y());
  complex<range> c(cb.x(), cb.y());
  z = sqr(z) + c;
  std::vector<box> result;
  result.push_back(box(z.real(), z.imag()));
  return result;
}

range sqrt(const range &x)
{
  // 1ulp pessimistic if sqrt was exact
  return range
    ( std::nextafter(std::sqrt(x.min()), 0)
    , std::nextafter(std::sqrt(x.max()), infinity)
    );
}

range abs(const complex<range> &x)
{
  return sqrt(sqr(x.real()) + sqr(x.imag()));
}

complex<range> sqrt(const complex<range> &x)
{
  auto a = x.real();
  auto b = x.imag();
  if (b.min() == 0 && b.max() == 0)
  {
    if (a.min() >= 0)
      return complex<range>(sqrt(a), b);
    if (a.max() <= 0)
      return complex<range>(b, sqrt(-a));
  }
  if (a.min() == 0 && a.max() == 0)
  {
    if (b.min() >= 0)
    {
      auto c = sqrt( b / 2);
      return complex<range>(c,  c);
    }
    if (b.max() <= 0)
    {
      auto c = sqrt(-b / 2);
      return complex<range>(c, -c);
    }
  }
  auto r = abs(x);
  if (midpoint(a) >= 0)
  {
    auto t = r + a;
    auto u = sqrt(2 * t);
    return complex<range>(u / 2, b / u);
  }
  if (! contains(b, 0))
  {
    auto u = r - a;
    auto t = sqrt(2 * u);
    auto c = abs(b / t);
    auto d = t / 2;
    if (midpoint(b) < 0) d = -d;
    return complex<range>(c, d);
  }
  auto t = (r + a) / 2;
  auto u = (r - a) / 2;
  auto c = sqrt(t);
  auto d = sqrt(u);
  if (b.max() >= 0)
    ; // nop
  else if (b.min() >= 0)
    d = -d;
  else
    d = t | -t;
  return complex<range>(c, d);
}

std::vector<box> quadratic_reverse(const box &zb, const box &cb)
{
  complex<range> z(zb.x(), zb.y());
  complex<range> c(cb.x(), cb.y());
  z = sqrt(z - c);
  std::vector<box> result;
  result.push_back(box( z.real(),  z.imag()));
  result.push_back(box(-z.real(), -z.imag()));
  return result;
}


const bifunction quadratic(quadratic_forward, quadratic_reverse);

class cell_ref
{
  std::vector<color> *lsb;
  size_t index;
  cell_ref(cell_ref &) = delete; // no copy
public:
  cell_ref(std::vector<color> &lsb, size_t index)
  : lsb(&lsb), index(index)
  { }
  cell_ref(cell_ref &&c) noexcept // move
  : lsb(std::exchange(c.lsb, nullptr))
  , index(std::exchange(c.index, 0))
  {
  }
  cell_ref &operator=(const color &c)
  {
    (*lsb)[index] = c;
    return *this;
  }
  operator color () const
  {
    return (*lsb)[index];
  }
};

class const_cell_ref
{
  const std::vector<color> &lsb;
  size_t index;
  const_cell_ref(const_cell_ref &) = delete; // no copy
public:
  const_cell_ref(const std::vector<color> &lsb, size_t index)
  : lsb(lsb), index(index)
  { }
  const_cell_ref(const_cell_ref &&c) noexcept // move
  : lsb(std::move(c.lsb))
  , index(std::exchange(c.index, 0))
  {
  }
  operator color () const
  {
    return lsb[index];
  }
};

real lerp(const range &a, real x)
{
  // precondition: multiplications are exact
  return a.min() * (1 - x) + x * a.max();
}

range subrange(const range &a, real lo, real hi)
{
  return range(lerp(a, lo), lerp(a, hi));
}

std::pair<real, real> suprange(const range &bounds, const range &sub, size_t d)
{
  // precondition: all maths is exact
  real diameter = bounds.max() - bounds.min();
  real lo = std::floor((sub.min() - bounds.min()) / diameter * d);
  real hi = std::ceil ((sub.max() - bounds.min()) / diameter * d);
  return std::pair<real, real>(lo, hi);
}

class world
{
  box bounds;
  size_t n;
  std::vector<color> lsb;
  range cx, cy;
public:
  world(const box &bounds, size_t n, const range &cx, const range &cy) // 0
  : bounds(bounds)
  , n(n)
  , lsb((1LLU << (2 * n)) + 1)
  , cx(cx)
  , cy(cy)
  {
    fill(gray);
    outside() = white;
  }
  world(const world &w, size_t k, size_t l)
  : bounds(w.bounds)
  , n(w.n + 1)
  , lsb((1LLU << (2 * (w.n + 1))) + 1)
  , cx(subrange(w.cx, k / 2.0, (k + 1) / 2.0))
  , cy(subrange(w.cy, l / 2.0, (l + 1) / 2.0))
  {
    for (size_t i = 0; i < 1LLU << n; ++i)
    for (size_t j = 0; j < 1LLU << n; ++j)
    {
      index a(i, j);
      index b(i >> 1, j >> 1);
      cell(a) = inherit[w.cell(b)];
    }
    outside() = white;
  }
  
  world &fill(const color &c)
  {
    std::fill(lsb.begin(), lsb.end(), c);
    return *this;
  }
  cell_ref cell(const index &ix)
  {
    return cell_ref(lsb, ix(n));
  }
  color cell(const index &ix) const
  {
    return const_cell_ref(lsb, ix(n));
  }
  cell_ref outside()
  {
    return cell_ref(lsb, 1LLU << (2 * n));
  }

  color outside() const
  {
    return const_cell_ref(lsb, 1LLU << (2 * n));
  }

  box box_for(const index &a)
  {
    real d = 1LLU << n;
    return box
      ( subrange(bounds.x(), a.x / d, (a.x + 1) / d)
      , subrange(bounds.y(), a.y / d, (a.y + 1) / d)
      );
  }

  std::vector<index> cells_for(const box &b, real expand = 0)
  {
    size_t d = 1LLU << n;
    std::vector<index> result;
    if (! (inside(b, bounds))) result.push_back(index(1LLU << n, 0)); // exterior
    std::pair<real, real> x = suprange(bounds.x(), b.x(), d);
    std::pair<real, real> y = suprange(bounds.y(), b.y(), d);
    for (real i = x.first - expand; i <= x.second + expand; i += 1)
    {
      if (i < 0) continue;
      if (i >= d) break;
      for (real j = y.first - expand; j <= y.second + expand; j += 1)
      {
        if (j < 0) continue;
        if (j >= d) break;
        result.push_back(index(size_t(i), size_t(j)));
      }
    }
    return result;
  }

  bool whiten(const bifunction &f)
  {
    bool any_changed = false;
    for (size_t i = 0; i < 1LLU << n; ++i)
    for (size_t j = 0; j < 1LLU << n; ++j)
    {
      index a(i, j);
      any_changed |= whiten(f, a);
    }
    return any_changed;
  }

  bool whiten(const bifunction &f, const index &a)
  {
    const box c0(cx, cy);
    bool any_changed = false;
    if (cell(a) == gray)
    {
      auto me = box_for(a);
      bool all_white = true;
      for (const auto &b : f.first(me, c0))
      {
        for (const auto &c : cells_for(b))
        {
          all_white &= (cell(c) == white);
          if (! all_white) break;
        }
        if (! all_white) break;
      }
      if (all_white)
      {
        cell(a) = white;
        any_changed |= true;
#if 0
        for (const auto &b : f.second(me, c0))
        {
          for (const auto &c : cells_for(b))
          {
            any_changed |= whiten(f, c);
          }
        }
#endif
      }
    }
    return any_changed;
  }

  bool blacken(const bifunction &f)
  {
    bool any_changed = false;
    for (size_t i = 0; i < 1LLU << n; ++i)
    for (size_t j = 0; j < 1LLU << n; ++j)
    {
      index a(i, j);
      any_changed |= blacken(f, a);
    }
    return any_changed;
  }

  bool blacken(const bifunction &f, const index &a)
  {
    const box c0(cx, cy);
    bool any_changed = false;
    if (cell(a) == gray)
    {
      auto me = box_for(a);
      bool any_white_or_marked = false;
      for (const auto &b : f.first(me, c0))
      {
        for (const auto &c : cells_for(b, 1))
        {
          any_white_or_marked |= (cell(c) == white || cell(c) == marked);
          if (any_white_or_marked) break;
        }
        if (any_white_or_marked) break;
      }
      if (any_white_or_marked)
      {
        cell(a) = marked;
        any_changed |= true;
#if 0
        for (const auto &b : f.second(me, c0))
        {
          for (const auto &c : cells_for(b, 1))
          {
            any_changed |= blacken(f, c);
          }
        }
#endif
      }
    }
    return any_changed;
  }

  void unmark()
  {
    for (size_t i = 0; i < 1LLU << n; ++i)
    for (size_t j = 0; j < 1LLU << n; ++j)
    {
      index a(i, j);
      cell(a) = unmarked[cell(a)];
    }
  }

  bool step(const bifunction &f)
  {
    const std::vector<color> lsb_backup = lsb;
    while (whiten(f))
      ;
    while (blacken(f))
      ;
    unmark();
    // gray -> marked -> gray is possible, so do it brute
    return lsb_backup != lsb;
  }

  std::string ppm_header()
  {
    std::ostringstream os;
    os << "P6\n" << (1LLU << n) << " " << (1LLU << n) << "\n# "
       << cx.min() << "  " << cx.max() << "\n# "
       << cy.min() << "  " << cy.max() << "\n255\n";
    return os.str();
  }

  std::vector<unsigned char> raster()
  {
    std::vector<unsigned char> rgb(3LLU << (2 * n));
    for (size_t i = 0; i < 1LLU << n; ++i)
    for (size_t j = 0; j < 1LLU << n; ++j)
    {
      size_t ix = 3 * ((i << n) + j);
      const color c = cell(index(i, j));
      rgb[ix++] = colorrgb[c][0];
      rgb[ix++] = colorrgb[c][1];
      rgb[ix++] = colorrgb[c][2];
    }
    return rgb;
  }

};

size_t z_order(size_t x, size_t y, size_t z, size_t w)
{
#define CHAR_BIT 8
  size_t o = 0;
  for (size_t i = 0; i < sizeof(x) * CHAR_BIT / 4; i++)
  {
    o |= (x & 1LLU << i) << (3 * i + 0)
      |  (y & 1LLU << i) << (3 * i + 1)
      |  (z & 1LLU << i) << (3 * i + 2)
      |  (w & 1LLU << i) << (3 * i + 3)
      ;
  }
  return o;
#undef CHAR_BIT
}

void depth_first(size_t &counter, unsigned char *voxels[2], world &w, size_t max_n, size_t n, size_t x, size_t y)
{
  while (w.step(quadratic))
    ;
  if (n == max_n)
  {
    counter += 1;
    std::cerr << "\t" << counter << "\r";
    for (size_t i = 0; i < 1LLU << n; ++i)
    for (size_t j = 0; j < 1LLU << n; ++j)
    {
      const color c = w.cell(index(i, j));
      const size_t ix = z_order(x, y, i, j);
      const size_t byte = ix >> 3;
      const size_t bit = ix & 7;
      voxels[0][byte] |= (c == black ? 1 : 0) << bit;
      voxels[1][byte] |= (c == gray  ? 1 : 0) << bit; 
    }
  }
  if (n <  max_n)
  {
    for (size_t k = 0; k < 2; ++k)
    for (size_t l = 0; l < 2; ++l)
    {
      auto w2 = world(w, k, l);
      depth_first(counter, voxels, w2, max_n, n + 1, (x << 1) + k, (y << 1) + l);
    }
  }
}


int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  range r(-2, 2);
  box bounds(r, r);
  world w(bounds, 0, r, r);
  size_t n = 9;
  size_t bytes = 1LLU << (4 * n - 3);
  unsigned char *voxels[2] =
    { (unsigned char *) calloc(1, bytes)
    , (unsigned char *) calloc(1, bytes)
    };
  size_t counter = 0;
  depth_first(counter, voxels, w, n, 0, 0, 0);
  std::FILE *f;
  f = std::fopen("black.raw", "wb");
  std::fwrite(voxels[0], bytes, 1, f);
  std::fclose(f);
  f = std::fopen("gray.raw", "wb");
  std::fwrite(voxels[1], bytes, 1, f);
  std::fclose(f);
  return 0;
}
