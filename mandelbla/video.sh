#!/bin/bash
make && time ./mandelbla | ffmpeg -r 10 -i - -s 640x360 -pix_fmt yuv420p -profile:v high -level:v 4.1 -crf:v 20 -movflags +faststart out/$(date --iso=s | sed s/:/-/g).mp4
