# mandelbla

Mandelbrot set via bivariate linear approximation.

## theory

### mandelbrot set:

High precision reference orbit:

$$Z_{m+1} = Z_m^2 + C$$

$m$ starts at $0$ with $Z_0 = 0$.

### perturbation

Low precision deltas relative to high precision orbit.
Pixel orbit $Z_m + z_n$, $C + c$.

$$z_{n+1} = 2 Z_m z_n + z_n^2 + c$$

$m$ and $n$ start at $0$ with $z_0 = 0$.

### rebasing

Rebasing to avoid glitches: when
$$|Z_m + z_n| < |z_n|$$
replace $z_n$ with $Z_m + z_n$ and
reset the reference iteration count $m$ to $0$.

Reference: <https://fractalforums.org/fractal-mathematics-and-new-theories/28/another-solution-to-perturbation-glitches/4360/msg29835#msg29835>

### bivariate linear approximation

When $Z$ is large and $z$ is small, the iterations can be approximated
by bivariate linear function;

$$z \to A z + B c$$

This is valid when
$$|Z + z| >> |z|$$
which implies
$$|z| << (|Z| - |B| |c|) / (|A| + 1)$$
by ball interval arithmetic.

### single step

$$A = 2 Z_m$$
$$B = 1$$
$$R = \max\left\{ 0, 0.001 \frac{|Z_m| - |B| |c|}{|A|} \right\}$$

The "fudge factor" $\approx 0.001$ seems to work but is not rigourous \ldots

### merging steps

If  $T_x$ skips $l_x$ iterations from iteration $m_x$ when $|z| < R_x$
and $T_y$ skips $l_y$ iterations from iteration $m_x + l_x$ when $|z| < R_y$
then $T_z = T_y \circ T_x$ skips $l_x + l_y$ iterations from iteration $m_x$ when $|z| < R_z$:

$$z \to A_y (A_x z + B_x c) + B_y c = A_z z + B_z c$$
$$A_z = A_y A_x$$
$$B_z = A_y B_x + B_y$$
$$R_z = \min\{ R_x, \frac{R_y - |B_x| |c|}{|A_x|} \}$$

### table construction

Suppose the reference has $M$ iterations.  Create $M$ BLAs each skipping
$1$ iteration (this can be done in parallel).  Then merge neighbours
without overlap to create $\left\lceil \frac{M}{2} \right\rceil$ each
skipping $2$ iterations (except for perhaps the last which skips less).
Repeat until there is only $1$ BLA skipping $M-1$ iterations: it's best
to start the merge from iteration $1$ because reference iteration $0$
always corresponds to a non-linear perturbation step as $Z = 0$.

The resulting table as $O(M)$ elements.

### table lookup

Find the BLA starting from iteration $m$ that has the largest skip $l$
satisfying $|z| < R$.  If there is none, do a perturbation iteration.
Check for rebasing opportunities after each BLA application or
perturbation step.

### error bounds

the above has a conceptual flaw, needing an arbitrary "fudge factor"

### remainder bound

Taylor remainder provides rigourous bounds:

$$z_{n+l} = A_{m,l} z_n + B_{m,l} c + D_{m,l} z_n^2 + E_{m,l} z_n c + F_{m,l} c^2 + O(\cdot^3)$$

$$R_{m,l} = \max\left\{ |D_{m,l}|, \frac{|E_{m,l}|}{2}, |F_{m,l}| \right\}$$

$$z_{n+l} = A_{m,l} z_n + B_{m,l} c + a_{z_{n,m,l}}$$

$$\left|a_{z_{n,m,l}}\right| < R_{m,l} (|z_n|^2 + |z_n| |c| + |c|^2)$$

$a_{z_{n,m,l}}$ is absolute error in $z_{n+l}$.  Relative error is
$$e_{z_{n,m,l}} = \frac{a_{z_{n,m,l}}}{z_{n+l}}$$

### single step

$$\begin{aligned}
l &= 1 \\
A &= 2 Z_m \\
B &= 1 \\
D &= 1 \\
E &= 0 \\
F &= 0
\end{aligned}$$

### merging steps

$$\begin{aligned}
m_y &= m_x + l_x \\
m_{y \circ x} &= m_x \\
l_{y \circ x} &= l_x + l_y \\
A_{y \circ x} &= A_y A_x \\
B_{y \circ x} &= A_y B_x + B_y \\
D_{y \circ x} &= A_y D_x + D_y A_x^2 \\
E_{y \circ x} &= A_y E_x + 2 D_y A_x B_x + E_y A_x \\
F_{y \circ x} &= A_y F_x + D_y B_x^2 + E_y B_x + F_y
\end{aligned}$$

### backward error

Suppose $e_c$ is absolute error in $c$.  For an accurate image, we want
bounds on $z$ such that $e_z$, the absolute error introduced into $z$ by
the bilinear approximation, corresponds to $|e_c| < k$, where $k$ is the
pixel spacing.  As we will be chaining multiple transformations, better
to scale $k$ by a small number so that $|e_c| < k$ still holds after all
of them: doing $L$ transformations means $|e_c| < \frac{k}{L}$ is
certainly good enough.

The absolute error in $z$ is the remainder bound multiplied by
$O(\cdot^2)$:
$$e_z = \max\left\{ |D|, \frac{|E|}{2}, |F| \right\} (|z|^2 + |z| |c| + |c|^2)$$
The forward error from a change in $c$ is $B e_c$, so we want

$$|B| |e_c| = \frac{|B| k}{L} > |e_z| = \max\left\{ |D|, \frac{|E|}{2}, |F| \right\} (|z|^2 + |z| |c| + |c|^2)$$
so
$$|z|^2 + |c| |z| + \left(|c|^2 - \frac{|B| k}{L \max\left\{ |D|, \frac{|E|}{2}, |F| \right\}}\right) < 0$$
which is a quadratic in $|z|$ so can be solved; we need to minimize the
result for all $c$ in the view of radius
$r_c = k \frac{\sqrt{w^2 + h^2}}{2}$, which gives:

$$|z| < \min_{|c|<r_c} \left\{\frac{-|c| + \sqrt{|c|^2 - 4 (|c|^2 - \frac{|B| k}{L \max\left\{ |D|, \frac{|E|}{2}, |F| \right\}})}}{2}\right\}$$

In the limit $|c| \to 0$ this simplifies to
$$|z| < \sqrt{\frac{|B| k}{L \max\left\{ |D|, \frac{|E|}{2}, |F| \right\}}}$$

## implementation

### input

- $C$ := view center (high precision)
- $M$ := reference count limit, can be lower than $N$ if periodic
- $N$ := iteration count limit
- $h$, $w$ := image size in pixels
- $k$ := pixel spacing ($k$ = $4 / (h \times \text{Zoom factor})$)

### reference calculation

```
Z[M] low precision array
Z := 0 (high precision)
m := 0
while |Z| < 2 && m < M
  Z[m] := round(Z)
  Z := Z^2 + c
  m := m + 1
```

### table data

```
struct TableItem
{
  l : int
  r : real
  A, B : complex
}
dmax := O(log_2(M))
Table[dmax] : ptr TableItem
```

### table initialization

```
Table[0] := allocate(M - 1, TableItem)
parallel for m := 1 to M - 1
  A := 2 Z[m]
  B := 1
  r := max( 0, |Z[m]| - |B| k h) / (|A| + 1) )
  Table[0][m-1] = { l, r, A, B }
```

### table merging

```
for d := 0 to dmax
  Table[d + 1] := allocate((count(Table[d]) + 1) / 2, TableItem)
  parallel for j := 1 to count(Table[d + 1])
    jx := 2 (j - 1);
    jy := jx + 1;
    if (jy < count(Table[d]))
      x := Table[d][jx]
      y := Table[d][jy]
      A := y.A * x.A
      B := y.A * x.B + y.B
      r := min( x.r, max( 0, (y.r - |x.B| h k) / |x.A| ) )
      Table[d + 1][j - 1] := { l, r, A, B }
    else
      Table[d + 1][j - 1] := Table[d][jx]
  if count(Table[d + 1]) == 1
    break
```

### table lookup

Find `Table[d][j]` with largest `l`
such that `m = (j << d) + 1` and `r > |z|`.
Result may be `null` if no such `TableItem`.

### pixel iteration

```
while
    iteration < Iterations &&
    perturb_iteration < PerturbIterations &&
    |Z+z| < EscapeRadius
  while
      iteration < Iterations &&
      perturb_iteration < PerturbIterations &&
      |Z+z| < EscapeRadius &&
      t := lookup table with reference iteration and |z|
    // bla iteration
    // rebase if required
  // perturbation iteration
  // rebase if required
```

## benchmarks

AMD Ryzen 7 2700X Eight-Core Processor desktop with 16 CPU threads.
1920x1080 image resolution.
 
### Dinkydau's "Flake"

- mandelbla: 0.5s
- kf-2.15.5~git (prerelease): 2.6s
- kf-2.15.4: 5.1s

### Dinkydau's "Evolution of Trees"

- mandelbla: 2.3s
- kf-2.15.5~git (prerelease): 37s
- kf-2.15.4: 1m21s

### Fractal Universe "Hard Location"

- mandelbla: 39s
- kf-2.15.5~git (prerelease): not completed, estimated 50h
- kf-2.15.4: not attempted
