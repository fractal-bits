#include <assert.h>
#include <inttypes.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <threads.h>

#include <mpfr.h>

#undef NONLINEAR

struct bla
{
  double r2, Ax, Ay, Bx, By;
#ifdef NONLINEAR
  double Dx, Dy, Ex, Ey, Fx, Fy;
#endif
  int64_t l;
};

struct blas
{
  int64_t M;
  int64_t L;
  struct bla **b;
};

void blas_init1(struct blas *B, const double *Zxp, const double *Zyp, const double h, const double k, double L)
{
  const int64_t M = B->M;
  #pragma omp parallel for
  for (int64_t m = 1; m < M; ++m)
  {
    int64_t l = 1;
    double Ax = 2 * Zxp[m];
    double Ay = 2 * Zyp[m];
    double Bx = 1;
    double By = 0;
#ifdef NONLINEAR
    double Dx = 1;
    double Dy = 0;
    double Ex = 0;
    double Ey = 0;
    double Fx = 0;
    double Fy = 0;
    double R = fmax(fmax(hypot(Dx, Dy), hypot(Ex, Ey) / 2), hypot(Fx, Fy));
    double d = (hypot(Bx, By) / R) * (1 / L) - (h * k) * (h * k);
    double r = 0;
    if (d > 0)
    {
      r = fmax(0, sqrt(d) - (h * k) / 2);
    }
    double r2 = r * r;
    struct bla b = { r2, Ax, Ay, Bx, By, Dx, Dy, Ex, Ey, Fx, Fy, l };
#else
    double mZ = hypot(Zxp[m], Zyp[m]);
    double mA = hypot(Ax, Ay);
    double mB = hypot(Bx, By);
    double r = fmax(0, (mZ - mB * h * k) / (mA + 1)) / L;
    double r2 = r * r;
    struct bla b = { r2, Ax, Ay, Bx, By, l };
#endif
    B->b[0][m - 1] = b;
  }
}

void blas_merge(struct blas *B, const double h, const double k, const double L)
{
  int64_t M = B->M;
  int64_t src = 0;
  for (int64_t msrc = M - 1; msrc > 1; msrc = (msrc + 1) >> 1)
  {
    int64_t dst = src + 1;
    int64_t mdst = (msrc + 1) >> 1;
    #pragma omp parallel for
    for (int64_t m = 0; m < mdst; ++m)
    {
      int64_t mx = m * 2;
      int64_t my = m * 2 + 1;
      if (my < msrc)
      {
        struct bla x = B->b[src][mx];
        struct bla y = B->b[src][my];
        int64_t l = x.l + y.l;
        // A = y.A * x.A
        double Ax = y.Ax * x.Ax - y.Ay * x.Ay;
        double Ay = y.Ax * x.Ay + y.Ay * x.Ax;
        // B = y.A * x.B + y.B
        double Bx = y.Ax * x.Bx - y.Ay * x.By + y.Bx;
        double By = y.Ax * x.By + y.Ay * x.Bx + y.By;
#ifdef NONLINEAR
        // ...
        double xA2x = x.Ax * x.Ax - x.Ay * x.Ay;
        double xA2y = 2 * x.Ax * x.Ay;
        double Dx = y.Ax * x.Dx - y.Ay * x.Dy + y.Dx * xA2x - y.Dy * xA2y;
        double Dy = y.Ax * x.Dy + y.Ay * x.Dx + y.Dx * xA2y + y.Dy * xA2x;
        double xABx = x.Ax * x.Bx - x.Ay * x.By;
        double xABy = x.Ax * x.By + x.Ay * x.Bx;;
        double Ex = y.Ax * x.Ex - y.Ay * x.Ey + 2 * (y.Dx * xABx - y.Dy * xABy) + y.Ex * x.Ax - y.Ey * x.Ay;
        double Ey = y.Ax * x.Ey + y.Ay * x.Ex + 2 * (y.Dx * xABy + y.Dy * xABx) + y.Ex * x.Ay + y.Ey * x.Ax;
        double xB2x = x.Bx * x.Bx - x.By * x.By;
        double xB2y = 2 * x.Bx * x.By;
        double Fx = y.Ax * x.Fx - y.Ay * x.Fy + y.Dx * xB2x - y.Dy * xB2y + y.Ex * x.Bx - y.Ey * x.By + y.Fx;
        double Fy = y.Ax * x.Fy + y.Ay * x.Fx + y.Dx * xB2y + y.Dy * xB2x + y.Ex * x.By + y.Ey * x.Bx + y.Fy;

        double R = fmax(fmax(hypot(x.Dx, x.Dy), hypot(x.Ex, x.Ey) / 2), hypot(x.Fx, x.Fy));
        double e = hypot(x.Ax, x.Ay) / (6 * R);
        double d = (sqrt(y.r2) - hypot(x.Bx, x.By) * (h * k)) / (3 * R) + e * e;
        double r = 0;
        if (d > 0)
        {
          r = fmax(0, sqrt(d) - e);
        }
        r = fmin(sqrt(x.r2), r);
        double r2 = r * r;
        struct bla b = { r2, Ax, Ay, Bx, By, Dx, Dy, Ex, Ey, Fx, Fy, l };
#else
        double xA = hypot(x.Ax, x.Ay);
        double xB = hypot(x.Bx, x.By);
        double r = fmin(sqrt(x.r2), fmax(0, (sqrt(y.r2) - xB * h * k) / xA));
        double r2 = r * r;
        struct bla b = { r2, Ax, Ay, Bx, By, l };
#endif
        B->b[dst][m] = b;
      }
      else
      {
        B->b[dst][m] = B->b[src][mx];
      }
    }
    src++;
  }
}

struct blas *blas_new(const int64_t M, const double *Zxp, const double *Zyp, const double h, const double k, const double L)
{
  struct blas *B = malloc(sizeof(*B));
  B->M = M;
  int64_t total = 1;
  int64_t count = 1;
  int64_t m = M - 1;
  for ( ; m > 1; m = (m + 1) >> 1)
  {
    total += m;
    count++;
  }
  B->L = count;
  B->b = malloc(count * sizeof(*B->b));
  B->b[0] = malloc(total * sizeof(*B->b[0]));
  int64_t ix = 1;
  m = M - 1;
  for ( ; m > 1; m = (m + 1) >> 1)
  {
    B->b[ix] = B->b[ix - 1] + m;
    ix++;
  }
  blas_init1(B, Zxp, Zyp, h, k, L);
  blas_merge(B, h, k, L);
  return B;
}

void blas_delete(struct blas *B)
{
  free(B->b[0]);
  free(B->b);
  free(B);
}

const struct bla *blas_lookup(const struct blas *B, const int64_t m, const double z2)
{
  if (m == 0)
  {
    return 0;
  }
  assert(m < B->M);
  struct bla *b = 0;
  int64_t ix = m - 1;
  for (int64_t L = 0; L < B->L; ++L)
  {
    int64_t ixm = (ix << L) + 1;
    if (m == ixm && z2 < B->b[L][ix].r2)
    {
      b = &B->b[L][ix];
    }
    else
    {
      break;
    }
    ix = ix >> 1;
  }
  return b;
}

static inline double hypot2(double x, double y)
{
  return x * x + y * y;
}

int main(int argc, char **argv)
{
  const int64_t width = 1920;
  const int64_t height = 1080;

#if 0
  if (argc != 5)
  {
    fprintf(stderr, "usage: %s re im zoom iterations > out.ppm\n", argv[0]);
    return 1;
  }
  const char *Re = argv[1];
  const char *Im = argv[2];
  double Zoom = atof(argv[3]);
  double ZoomPrec = Zoom;
  int64_t Iterations = atoi(argv[4]);
  int64_t MaximumReferenceIterations = Iterations;
#else
#if 1
  // Dinkydau - Flake
  const char *Re = "-1.999966194450370304184346885063505796755312415407248515117619229448015842423426843813761297788689138122870464065609498643538105757447721664856724960928039200953321766548438990841";
  const char *Im = "3.001382436790938324072497303977592498734683119077333527017425728012047497561482358118564729928841407551922418650497818162547852894554815712214577268132087243118270209466408349072e-34";
  double Zoom = 2.5620330788506154104770818136626E157;
  double ZoomPrec = 1e171;
  int64_t Iterations = 1100100;
  int64_t PerturbIterations = 1000;
  int64_t ReferencePeriod = 7884;
  int64_t MaximumReferenceIterations = ReferencePeriod + 1;
#else
#if 1
  // Dinkydau - Evolution of Trees
  const char *Re = "-0.74962449737876168207862620426836138684529527812364025754481571424558286479672801698750203976779135608148687747196595174858125388297577788573082753210469806739721377901297451172624762450940529814713048377873612297220313016947039287469999999999999999";
  const char *Im = "0.03427010874046016945172951545749474868051534672439111853174854738370595772935930171842999778713222794215453436628998200591914208207632674780780978496784843807690510401829301309069542198413574392425885166367231192087416338497065495509999999999999999";
  double Zoom = 7.58065474756E227;
  double ZoomPrec = Zoom;
  int64_t Iterations = 1200000;
  int64_t PerturbIterations = 8000;
  int64_t ReferencePeriod = 567135;
  int64_t MaximumReferenceIterations = ReferencePeriod + 1;
#else
  // Fractal Universe - Hard Location
  const char *Re = "-1.74992248092759992827133368754228945303043302447370334500650852139592486065065408129935547375121997659867849111435922542786389338654238247523763456e+00";
  const char *Im = "-9.59502198314327569948975707202650233401883670299418141500240641361234506320676962536124684582340235944852850785763764700482870741065313152008753180e-13";
  double Zoom = 6.49E137;
  double ZoomPrec = 6.367062888622018e138;
  int64_t Iterations = 2100100100;
  int64_t PerturbIterations = 2000;
  int64_t ReferencePeriod = 43292334;
  int64_t MaximumReferenceIterations = ReferencePeriod + 1;
#endif
#endif
#endif

  // calculate reference
  double *Zxp = calloc(1, MaximumReferenceIterations * sizeof(*Zxp));
  double *Zyp = calloc(1, MaximumReferenceIterations * sizeof(*Zyp));
  int64_t M = MaximumReferenceIterations - 1;
  {
    double pixel_spacing = 4 / ZoomPrec / height;
    mpfr_prec_t prec = ceil(24 - log2(pixel_spacing));
    mpfr_t Cx, Cy, Zx, Zy, Zx2, Zy2, z_0, Z2, ER2;
    mpfr_init2(Cx, prec);
    mpfr_init2(Cy, prec);
    mpfr_init2(Zx, prec);
    mpfr_init2(Zy, prec);
    mpfr_init2(Zx2, prec);
    mpfr_init2(Zy2, prec);
    mpfr_init2(z_0, prec);
    mpfr_init2(Z2, prec);
    mpfr_init2(ER2, 53);
    mpfr_set_d(ER2, 4, MPFR_RNDN);
    mpfr_set_str(Cx, Re, 10, MPFR_RNDN);
    mpfr_set_str(Cy, Im, 10, MPFR_RNDN);
    mpfr_set_d(Zx, 0, MPFR_RNDN);
    mpfr_set_d(Zy, 0, MPFR_RNDN);
    mpfr_sqr(Zx2, Zx, MPFR_RNDN);
    mpfr_sqr(Zy2, Zy, MPFR_RNDN);
    mpfr_add(Z2, Zx2, Zy2, MPFR_RNDN);
    // calculate reference in high precision
    for (int64_t i = 0; i < MaximumReferenceIterations; ++i)
    {
      int64_t progress = i;
      if ((progress - 1) * 100 / MaximumReferenceIterations != progress * 100 / MaximumReferenceIterations)
      {
        fprintf(stderr, ".");
      }
      Zxp[i] = mpfr_get_d(Zx, MPFR_RNDN);
      Zyp[i] = mpfr_get_d(Zy, MPFR_RNDN);
      // z = z^2 + c
      mpfr_add(z_0, Zx, Zy, MPFR_RNDN);
      mpfr_sqr(z_0, z_0, MPFR_RNDN);
      mpfr_sub(Zx, Zx2, Zy2, MPFR_RNDN);
      mpfr_sub(Zy, z_0, Z2, MPFR_RNDN);
      mpfr_add(Zx, Zx, Cx, MPFR_RNDN);
      mpfr_add(Zy, Zy, Cy, MPFR_RNDN);
      mpfr_sqr(Zx2, Zx, MPFR_RNDN);
      mpfr_sqr(Zy2, Zy, MPFR_RNDN);
      mpfr_add(Z2, Zx2, Zy2, MPFR_RNDN);
      // if |z|^2 > er2
      if (mpfr_greater_p(Z2, ER2))
      {
        M = i;
        break;
      }
    }
    mpfr_clear(Cx);
    mpfr_clear(Cy);
    mpfr_clear(Zx);
    mpfr_clear(Zy);
    mpfr_clear(Zx2);
    mpfr_clear(Zy2);
    mpfr_clear(z_0);
    mpfr_clear(Z2);
    mpfr_clear(ER2);
  }
  fprintf(stderr, "\n");

  // compute pixels
  const double ER2 = 65536. * 65536.;
  unsigned char *pgm = malloc(width * height);
#ifdef VIDEO
  for (Zoom = 1./4096; Zoom < ZoomPrec; Zoom *= 2)
#endif
  {
    // initialize table
    const double pixel_spacing = 4 / Zoom / height;
    const double step_count = 1000; // FIXME
    struct blas *B = blas_new(M, Zxp, Zyp, hypot(width, height), pixel_spacing, step_count);
    for (int64_t L = 0; L < B->L; ++L)
    {
      fprintf(stderr, "%" PRId64 "\t%.2e\n", B->b[L][0].l, sqrt(B->b[L][0].r2));
    }
    int64_t total_bla_steps = 0;
    int64_t total_bla_iterations = 0;
    int64_t total_perturb_iterations = 0;
    int64_t total_rebases = 0;
    int64_t total_iterations = 0;
    int64_t total_pixels = 0;
    int64_t minimum_iterations = 0x7fffFFFFffffFFFFLL;;
    int64_t maximum_iterations = 0;
    #pragma omp parallel for reduction(+:total_bla_steps) reduction(+:total_bla_iterations) reduction(+:total_perturb_iterations) reduction(+:total_iterations) reduction(+:total_rebases) reduction(min:minimum_iterations) reduction(max:maximum_iterations)
    for (int64_t j = 0; j < height; ++j)
    for (int64_t i = 0; i < width; ++i)
    {
      int64_t bla_steps = 0;
      int64_t bla_iterations = 0;
      int64_t perturb_iterations = 0;
      int64_t rebases = 0;
      const double cx = ((i + 0.5) / width - 0.5) * width * pixel_spacing;
      const double cy = (0.5 - (j + 0.5) / height) * height * pixel_spacing;
      int64_t m = 0;
      int64_t n = 0;
      double Zx = Zxp[0];
      double Zy = Zyp[0];
      double zx = 0;
      double zy = 0;
      double z2 = zx * zx + zy * zy;
      double dZdCx = 0;
      double dZdCy = 0;
      double Zzx = Zx + zx;
      double Zzy = Zy + zy;
      double Zz2 = Zzx * Zzx + Zzy * Zzy;
      if (i == 0 && j == 0)
      {
        fprintf(stderr, "[ %.2e ", 2 / Zoom);
      }

      while (n < Iterations && Zz2 < ER2 && perturb_iterations < PerturbIterations)
      {
        // bla steps
        const struct bla *b = 0;
        while (n < Iterations && Zz2 < ER2 && (b = blas_lookup(B, m, z2)))
        {
          double Ax = b->Ax;
          double Ay = b->Ay;
          double Bx = b->Bx;
          double By = b->By;
          int64_t l = b->l;
          // z = A * z + B * c
          double zxn = Ax * zx - Ay * zy + Bx * cx - By * cy;
          double zyn = Ax * zy + Ay * zx + Bx * cy + By * cx;
          // dZdC = A * dZdC + B * pixel_spacing
          double dZdCxn = Ax * dZdCx - Ay * dZdCy + Bx * pixel_spacing;
          double dZdCyn = Ax * dZdCy + Ay * dZdCx + By * pixel_spacing;
          zx = zxn;
          zy = zyn;
          z2 = zx * zx + zy * zy;
          dZdCx = dZdCxn;
          dZdCy = dZdCyn;
          n += l;
          m += l;
          bla_steps++;
          bla_iterations += l;
          if (ReferencePeriod)
          {
            while (m >= ReferencePeriod)
            {
              m -= ReferencePeriod;
            }
          }
          if (i == 0 && j == 0)
          {
            fprintf(stderr, "%" PRId64 " ", l);
          }

          // rebase
          assert(m < M);
          double Zx = Zxp[m];
          double Zy = Zyp[m];
          double Zzx = Zx + zx;
          double Zzy = Zy + zy;
          Zz2 = Zzx * Zzx + Zzy * Zzy;
          if (Zz2 < z2 || (ReferencePeriod == 0 && m == M - 1))
          {
            zx = Zzx;
            zy = Zzy;
            m = 0;
            rebases++;
            if (i == 0 && j == 0)
            {
              fprintf(stderr, "B ");
            }
          }
        }

        // perturbation iteration
        {
          assert(m < M);
          double Zx = Zxp[m];
          double Zy = Zyp[m];
          double Zzx = Zx + zx;
          double Zzy = Zy + zy;
          // z = (2 Z + z) z + c
          double ZZzx = 2 * Zx + zx;
          double ZZzy = 2 * Zy + zy;
          double zxn = ZZzx * zx - ZZzy * zy + cx;
          double zyn = ZZzx * zy + ZZzy * zx + cy;
          // dZdC = 2 dZdC Zz + pixel_spacing
          double dZdCxn = 2 * (dZdCx * Zzx - dZdCy * Zzy) + pixel_spacing;
          double dZdCyn = 2 * (dZdCx * Zzy + dZdCy * Zzx);
          zx = zxn;
          zy = zyn;
          z2 = zx * zx + zy * zy;
          dZdCx = dZdCxn;
          dZdCy = dZdCyn;
          n++;
          m++;
          perturb_iterations++;
          if (ReferencePeriod)
          {
            while (m >= ReferencePeriod)
            {
              m -= ReferencePeriod;
            }
          }
          if (i == 0 && j == 0)
          {
            fprintf(stderr, "p ");
          }
        }

        // rebase
        {
          assert(m < M);
          double Zx = Zxp[m];
          double Zy = Zyp[m];
          double Zzx = Zx + zx;
          double Zzy = Zy + zy;
          Zz2 = Zzx * Zzx + Zzy * Zzy;
          if (Zz2 < z2 || (ReferencePeriod == 0 && m == M - 1))
          {
            zx = Zzx;
            zy = Zzy;
            z2 = Zz2;
            m = 0;
            rebases++;
            if (i == 0 && j == 0)
            {
              fprintf(stderr, "b ");
            }
          }
        }
      }

      // output
      if (i == 0 && j == 0)
      {
        fprintf(stderr, "%" PRId64 " ]\n", n);
        fprintf(stderr, "estimated speedup: %gx (%" PRId64 " rebases)\n", n * 1.0 / (n - bla_iterations), rebases);
      }
      // compute colour
      double de = 0.5 * sqrt(Zz2) / hypot(dZdCx, dZdCy) * log(Zz2);
      int g = fmin(fmax(220 + 16 * log(de), 0), 255);
      //g = iteration;
      if (Zz2 < ER2 || isnan(de) || isinf(de))
      {
        g = 0;
      }
      pgm[j * width + i] = g;

      // accumulate statistics
      total_bla_iterations += bla_iterations;
      total_bla_steps += bla_steps;
      total_iterations += n;
      total_perturb_iterations += perturb_iterations;
      total_rebases += rebases;
      maximum_iterations = maximum_iterations > n ? maximum_iterations : n;
      minimum_iterations = minimum_iterations < n ? minimum_iterations : n;

      // progress report
      int64_t progress;
      #pragma omp atomic capture
      progress = total_pixels++;
      int pcold = (progress - 1) * 100 / (width * height);
      int pc = progress * 100 / (width * height);
      if (pc > pcold)
      {
        fprintf(stderr, ".");
      }
    }
    fprintf(stderr, "\n");

    // write PGM image
    fprintf(stdout, "P5\n%" PRId64 " %" PRId64 "\n255\n", width, height);
    fwrite(pgm, width * height, 1, stdout);
    fflush(stdout);

    // output statistics
    fprintf(stderr, "%f minimum iterations\n", (double) minimum_iterations);
    fprintf(stderr, "%f maximum iterations\n", (double) maximum_iterations);
    fprintf(stderr, "%f iters per pixel\n", total_iterations / (double) total_pixels);
    fprintf(stderr, "%f bla iters per pixel\n", total_bla_iterations / (double) total_pixels);
    fprintf(stderr, "%f ptb iters per pixel\n", total_perturb_iterations / (double) total_pixels);
    fprintf(stderr, "%f steps per pixel\n", (total_perturb_iterations + total_bla_steps) / (double) total_pixels);
    fprintf(stderr, "%f bla steps per pixel\n", total_bla_steps / (double) total_pixels);
    fprintf(stderr, "%f rebases per pixel\n", total_rebases / (double) total_pixels);
    fprintf(stderr, "%f iters per bla\n", total_bla_iterations / (double) total_bla_steps);
    fprintf(stderr, "speedup: %gx\n", total_iterations * 1.0 / (total_perturb_iterations + total_bla_steps));

    blas_delete(B);
  }
  free(Zxp);
  free(Zyp);
  free(pgm);

  return 0;
}
