#version 330 core

#include "TwoD.frag"

uniform int Iterations; slider[1,100,1000]
uniform float Radius; slider[1,100,1000]

float ER2 = Radius * Radius;

struct Dual { float x; float dx[2]; };
struct Hyper { float x; Dual dx[2]; };
struct Complex { Hyper x, y; };

Dual neg(Dual a)
{
  return Dual(-a.x, float[2](-a.dx[0], -a.dx[1]));
}

Dual add(Dual a, Dual b)
{
  return Dual(a.x + b.x, float[2](a.dx[0] + b.dx[0], a.dx[1] + b.dx[1]));
}

Dual mul(Dual a, float b)
{
  return Dual(a.x * b, float[2](a.dx[0] * b, a.dx[1] * b));
}

Dual mul(float a, Dual b)
{
  return Dual(a * b.x, float[2](a * b.dx[0], a * b.dx[1]));
}

Dual mul(Dual a, Dual b)
{
  return Dual(a.x * b.x, float[2](a.dx[0] * b.x + a.x * b.dx[0], a.dx[1] * b.x + a.x * b.dx[1]));
}

Dual sub(Dual a, Dual b)
{
  return add(a, neg(b));
}

Hyper neg(Hyper a)
{
  return Hyper(-a.x, Dual[2](neg(a.dx[0]), neg(a.dx[1])));
}

Hyper add(Hyper a, Hyper b)
{
  return Hyper(a.x + b.x, Dual[2](add(a.dx[0], b.dx[0]), add(a.dx[1], b.dx[1])));
}

Hyper mul(Hyper a, Hyper b)
{
  return Hyper(a.x * b.x, Dual[2](add(mul(a.dx[0], b.x), mul(a.x, b.dx[0])), add(mul(a.dx[1], b.x), mul(a.x, b.dx[1]))));
}

Hyper sub(Hyper a, Hyper b)
{
  return add(a, neg(b));
}

Complex neg(Complex a)
{
  return Complex(neg(a.x), neg(a.y));
}

Complex add(Complex a, Complex b)
{
  return Complex(add(a.x, b.x), add(a.y, b.y));
}

Complex mul(Complex a, Complex b)
{
  return Complex(sub(mul(a.x, b.x), mul(a.y, b.y)), add(mul(a.x, b.y), mul(a.y, b.x)));
}

Complex sub(Complex a, Complex b)
{
  return add(a, neg(b));
}

float norm(Complex a)
{
  return a.x.x * a.x.x + a.y.x * a.y.x;
}

vec3 color(vec2 p, vec2 dx, vec2 dy)
{
  Complex c = Complex(Hyper(p.x, Dual[2](Dual(dx.x, float[2](0,0)), Dual(dx.y, float[2](0,0)))), Hyper(p.y, Dual[2](Dual(dy.x, float[2](0,0)), Dual(dy.y, float[2](0,0)))));
  Complex z = Complex(Hyper(0, Dual[2](Dual(0, float[2](0,0)), Dual(0, float[2](0,0)))), Hyper(0, Dual[2](Dual(0, float[2](0,0)), Dual(0, float[2](0,0)))));
  for (int i = 0; i < Iterations; ++i)
  {
    if (! (norm(z) < ER2)) break;
    z = add(mul(z, z), c);
  }
  if (norm(z) < ER2)
  {
    return vec3(0);
  }
  else
  {
    float X = z.x.x;
    float Y = z.y.x;
    vec2 Z = vec2(X, Y);
    float Xx = z.x.dx[0].x;
    float Xy = z.x.dx[1].x;
    float Yx = z.y.dx[0].x;
    float Yy = z.y.dx[1].x;
    float Xxx = z.x.dx[0].dx[0];
    float Xxy = z.x.dx[0].dx[1];
//  float Xyx = z.x.dx[1].dx[0];
    float Xyy = z.x.dx[1].dx[1];
    float Yxx = z.y.dx[0].dx[0];
    float Yxy = z.y.dx[0].dx[1];
//  float Yyx = z.y.dx[1].dx[0];
    float Yyy = z.y.dx[1].dx[1];
    // de
    float Z2 = X * X + Y * Y;
    float Dx = X * Xx + Y * Yx;
    float Dy = X * Xy + Y * Yy;
    float D2 = Dx * Dx + Dy * Dy;
    float U =  log(Z2) * Z2 * Dx / D2;
    float V = -log(Z2) * Z2 * Dy / D2;
    vec2 de = vec2(U, V);
    // dde
    float Uxx = Y * Yxx + Yx * Yx + X * Xxx + Xx * Xx;
    float Uxy = Yx * Yy + Y * Yxy + Xx * Xy + X * Xxy;
    float Uyy = Y * Yyy + Yy * Yy + X * Xyy + Xy * Xy;
    float Ux =   Uxx * Z2 * log(Z2) / (2 * D2) + (Dx * Dx * Z2 * (1 + log(Z2)) / D2) - (Dy * Uxy + Dx * Uxx) * Dx * Z2 * log(Z2) / (D2 * D2);
    float Uy =   Uxy * Z2 * log(Z2) / (2 * D2) + (Dx * Dy * Z2 * (1 + log(Z2)) / D2) - (Dy * Uyy + Dx * Uxy) * Dx * Z2 * log(Z2) / (D2 * D2);
    float Vx = - Uxy * Z2 * log(Z2) / (2 * D2) - (Dx * Dy * Z2 * (1 + log(Z2)) / D2) + (Dy * Uxy + Dx * Uxx) * Dy * Z2 * log(Z2) / (D2 * D2);
    float Vy = - Uyy * Z2 * log(Z2) / (2 * D2) - (Dy * Dy * Z2 * (1 + log(Z2)) / D2) + (Dy * Uyy + Dx * Uxy) * Dy * Z2 * log(Z2) / (D2 * D2);
    mat2 dde = transpose(mat2(Ux, Uy, Vx, Vy));
    vec3 N = vec3(de * dde, 1);
    vec3 L = vec3(cos(2.0*3.141592653*Time/10.0), sin(2.0*3.141592653*Time/10.0), 1);
    float v = dot(N, L) / sqrt(dot(N, N) * dot(L, L));
    return v > 0 ? mix(vec3(1), vec3(1,0.7,0), v) : mix(vec3(1), vec3(0,0.7,1), -v);
  }
}
