{-# LANGUAGE RankNTypes #-}
import Control.Monad (forM_)
import Data.Char (chr)
import Data.Complex
import Data.List (minimumBy)
import Data.Ord (comparing)
import System.IO (BufferMode(NoBuffering), hFlush, hSetBinaryMode, hSetBuffering, stdout)

import Compat

import Debug.Trace (traceShow)
debug :: Show x => x -> y -> y
debug x y = y -- traceShow x y
--debug _ x = x

i :: RealFloat f => Complex f
i = 0 :+ 1

newtype Polynomial f = P { unP :: [f] }
  deriving (Eq, Ord, Read, Show)

instance Num f => Num (Polynomial f) where
  fromInteger n = constant (fromInteger n)
  negate (P xs) = P (map negate xs)
  P xs + P ys = P (zipWith0 (+) xs ys)
    where
      zipWith0 f [] bs = bs
      zipWith0 f as [] = as
      zipWith0 f (a:as) (b:bs) = f a b : zipWith0 f as bs
  xp@(P xs) * yp@(P ys) =
      P [ sum (zipWith (*) xs (shift (deg_y - k) (reverse_ys)))
        | k <- [0 .. deg_x + deg_y]
        ]
    where
      deg_x = deg xp
      deg_y = deg yp
      reverse_ys = reverse ys
      shift k xs
        | k < 0 = replicate (negate k) 0 ++ xs
        | otherwise  = drop k xs
  abs = error "P.abs"
  signum = error "P.signum"

constant :: Num f => f -> Polynomial f
constant n = P [n]

deg :: Polynomial f -> Int
deg (P xs) = length xs - 1

polynomial :: Num f => Polynomial f -> f -> f
polynomial (P as) z = sum [ a * z^k | (a, k) <- zip as [0 ..] ]

terms :: Int
terms = 16

newtype Series f = S{ unS :: Polynomial f }
  deriving (Eq, Ord, Read, Show)

instance Num f => Num (Series f) where
  fromInteger n = S (fromInteger n)
  negate (S xs) = S (negate xs)
  abs (S xs) = S (abs xs)
  signum (S xs) = S (signum xs)
  S xs + S ys = S (xs + ys)
  S xs * S ys = let P zs = xs * ys in S (P (take terms zs))

at :: Num f => Series f -> f -> f
at (S a) z = polynomial a z

compose :: Num f => Series f -> Series f -> Series f
compose (S (P as)) z = sum [ S (P [a]) * z^k | (a, k) <- zip as [0..] ]

instance Num f => Semigroup (Series f) where
  (<>) = compose

d :: Num f => Series f -> Series f
d (S (P (_:as))) = S (P [ fromInteger i * a | (a, i) <- zip as [1..] ])

x :: Num f => Series f
x = S (P [0, 1])

bash :: RealFloat f => f -> f
bash u
  | isNaN u = 0
  | isInfinite u = 100
  | otherwise = u

data Formula f = F
  { r :: forall x . Num x => x -> x
  , dr :: forall x . Num x => x -> x
  , rv :: forall x . Num x => x -> x
  , maxiters :: Integer
  , inU :: Complex f -> Bool
  , inE :: Complex f -> Bool
  , inO :: Complex f -> Bool
  , inA :: Complex f -> Bool
  , nearPP :: Complex f -> Bool
  , mag :: Complex f-> f
  , de :: Complex f -> Complex f -> Maybe f
  }

cauliflower :: RealFloat f => f -> Formula f
cauliflower pixelSpacing = f
 where
  f = F
        { r = \z -> z + z^2
        , dr = \z -> 1 + 2 * z
        , rv = r f
        , maxiters = 2^64
        , inU = \w -> not (or [ magnitude (w - cp) <= ir | cp <- cps ] || er <= magnitude w)
        , inE = \w -> mag f w < e
        , inO = \w -> mag f w < o
        , inA = \w ->
            if nearPP f w
              then snd $ minimumBy (comparing fst) [ (magnitude (w - pp), abs (phase ((-(w - pp))^v)) < a) | pp <- pps ]
              else abs (phase (((w - p)^v))) < a
        , nearPP = \w -> minimum [ magnitude (w - pp) | pp <- pps ] < magnitude (w - p)
        , mag = \w -> minimum [ magnitude (w - pp) | pp <- pps ] `min` magnitude (w - p)
        , de = \w dw -> case () of
            _ | inO f w -> Just $ bash (mag f w / magnitude dw)
              | inE f w && not (inA f w) && not (nearPP f w) -> Just $ bash (let u = (w - p)^v in if realPart u < 0 then magnitude u ** (1/fromInteger v) / magnitude dw else abs (imagPart u) * magnitude u ** (1 / fromInteger v - 1) / fromInteger v / magnitude dw)
              | or [ magnitude (w - cp) <= ir | cp <- cps ] -> Just $ bash (mag f w / magnitude dw)
              | magnitude w >= er -> Just $ bash (magnitude w * log (magnitude w) / magnitude dw)
              | otherwise -> Nothing
        }
  er = 1e+3
  p = 0
  pps = [ -1 ]
  cps = [ (p + pp) / 2 | pp <- pps ]
  ir = minimum [ mag f cp | cp <- cps ] * 0.9
  o = pixelSpacing / 2
  e = 0.05
  a = pi / 16
  v = 1

basilica :: RealFloat f => f -> Formula f
basilica pixelSpacing = f
 where
  f = F
        { r = \z -> z + z^3
        , dr = \z -> 1 + 3 * z^2
        , rv = r f . r f
        , maxiters = 2^64
        , inU = \w -> not (or [ magnitude (w - cp) <= ir | cp <- cps ] || er <= magnitude w)
        , inE = \w -> mag f w < e
        , inO = \w -> mag f w < o
        , inA = \w ->
            if nearPP f w
              then snd $ minimumBy (comparing fst) [ (magnitude (w - pp), abs (phase ((-(w - pp))^v)) < a) | pp <- pps ]
              else abs (phase (((w - p)^v))) < a
        , nearPP = \w -> minimum [ magnitude (w - pp) | pp <- pps ] < magnitude (w - p)
        , mag = \w -> minimum [ magnitude (w - pp) | pp <- pps ] `min` magnitude (w - p)
        , de = \w dw -> case () of
            _ | inO f w -> Just $ bash (mag f w / magnitude dw)
              | inE f w && not (inA f w) && not (nearPP f w) -> Just $ bash (let u = (w - p)^v in if realPart u < 0 then magnitude u ** (1/fromInteger v) / magnitude dw else abs (imagPart u) * magnitude u ** (1 / fromInteger v - 1) / fromInteger v / magnitude dw)
              | or [ magnitude (w - cp) <= ir | cp <- cps ] -> Just $ bash (mag f w / magnitude dw)
              | magnitude w >= er -> Just $ bash (magnitude w * log (magnitude w) / magnitude dw)
              | otherwise -> Nothing
        }
  er = 1e+3
  p = 0
  pps = [ i, -i ]
  cps = [ (p + pp) / 2 | pp <- pps ]
  ir = minimum [ mag f cp | cp <- cps ] * 0.9
  pp = i
  cp = (pp + p) / 2
  o = pixelSpacing / 2
  e = 0.1
  a = pi / 64
  v = 2

rabbit :: RealFloat f => f -> Formula f
rabbit pixelSpacing = f
 where
  f = F
        { r = \z -> z + z^4
        , dr = \z -> 1 + 4 * z^3
        , rv = r f . r f . r f
        , maxiters = 2^64
        , inU = \w -> not (or [ magnitude (w - cp) <= ir | cp <- cps ] || er <= magnitude w)
        , inE = \w -> mag f w < if nearPP f w then e / 4 else e
        , inO = \w -> mag f w < o
        , inA = \w ->
            if nearPP f w
              then snd $ minimumBy (comparing fst) [ (magnitude (w - pp), abs (phase ((-(w - pp))^v)) < a) | pp <- pps ]
              else abs (phase (((w - p)^v))) < a
        , nearPP = \w -> minimum [ magnitude (w - pp) | pp <- pps ] < magnitude (w - p)
        , mag = \w -> minimum [ magnitude (w - pp) | pp <- pps ] `min` magnitude (w - p)
        , de = \w dw -> case () of
            _ | inO f w -> Just $ bash (mag f w / magnitude dw)
              | inE f w && not (inA f w) && not (nearPP f w) -> Just $ bash (let u = (w - p)^v in if realPart u < 0 then magnitude u ** (1/fromInteger v) / magnitude dw else abs (imagPart u) * magnitude u ** (1 / fromInteger v - 1) / fromInteger v / magnitude dw)
              | or [ magnitude (w - cp) <= ir | cp <- cps ] -> Just $ bash (mag f w / magnitude dw)
              | magnitude w >= er -> Just $ bash (magnitude w * log (magnitude w) / magnitude dw)
              | otherwise -> Nothing
        }
  er = 1e+3
  p = 0
  pps = [ -1, (1 / 2) :+ (sqrt 3 / 2), (1 / 2) :+ (- sqrt 3 / 2) ]
  cps = [ (p + pp) / 2 | pp <- pps ]
  ir = minimum [ mag f cp | cp <- cps ] * 0.5
  o = pixelSpacing / 2
  e = 0.2
  a = pi / 64
  v = 3

blobble :: RealFloat f => Integer -> f -> f -> Formula f
blobble v e pixelSpacing = f
 where
  f = F
        { r = \z -> z + z^(v + 1)
        , dr = \z -> 1 + fromInteger (v + 1) * z^v
        , rv = \z -> stimes v (r f x) `at` z
        , maxiters = 2^64
        , inU = \w -> not (or [ magnitude (w - cp) <= ir | cp <- cps ] || er <= magnitude w)
        , inE = \w -> mag f w < if nearPP f w then e / fromInteger v else e
        , inO = \w -> mag f w < o
        , inA = \w ->
            if nearPP f w
              then snd $ minimumBy (comparing fst) [ (magnitude (w - pp), abs (phase ((-(w - pp))^v)) < a) | pp <- pps ]
              else abs (phase (((w - p)^v))) < a
        , nearPP = \w -> minimum [ magnitude (w - pp) | pp <- pps ] < magnitude (w - p)
        , mag = \w -> minimum [ magnitude (w - pp) | pp <- pps ] `min` magnitude (w - p)
        , de = \w dw -> case () of
            _ | inO f w -> Just $ bash (mag f w / magnitude dw)
              | inE f w && not (inA f w) && not (nearPP f w) -> Just $ bash (let u = (w - p)^v in if realPart u < 0 then magnitude u ** (1/fromInteger v) / magnitude dw else abs (imagPart u) * magnitude u ** (1 / fromInteger v - 1) / fromInteger v / magnitude dw)
              | or [ magnitude (w - cp) <= ir | cp <- cps ] -> Just $ bash (mag f w / magnitude dw)
              | magnitude w >= er -> Just $ bash (magnitude w * log (magnitude w) / magnitude dw)
              | otherwise -> Nothing
        }
  er = 1e+3
  p = 0
  pps = [ cis (2 * pi * (fromInteger t + 1/2) / fromInteger v) | t <- [0 .. v - 1] ]
  cps = [ (p + pp) / 2 | pp <- pps ]
  ir = minimum [ mag f cp | cp <- cps ] * (0.5 ** (1/3)) ^ v
  o = pixelSpacing / 2
  a = pi / 64

julia :: (Show f, RealFloat f) => Formula f -> Integer -> Complex f -> Complex f -> f -- (Bool, Integer)
julia f k w dw
  | not inU_ && not inE_ = debug 3 $ case de f w dw of -- 3
     Just de_ -> de_
--       | de_ < 1 -> (True, k)
--       | otherwise -> (False, k)
     _ -> onestep
  | inU_ && not inE_ = debug 4 $ onestep -- 4
  | inO_ = debug 5 $ case de f w dw of
 --     Just de_ -> de_
      _ -> 0 -- (True, k) -- 5
  | inE_ && nearPP_ = debug 6 $ onestep -- 6
  | inE_ && not inA_ = debug 7 $ case de f w dw of
    Just de_ -> de_
--      | de_ < 1 -> (True, k)
--      | otherwise -> (False, k)
    _ -> onestep
  | inE_ && inA_ && not nearPP_ = debug (8, w) $ searchUp lo0 -- 8 long iteration
  | otherwise = debug 9 $ onestep
  where
    onestep = julia f (k + 1) (r f w) (dr f w * dw)
    inU_ = inU f w
    inO_ = inO f w
    inE_ = inE f w
    inA_ = inA f w
    nearPP_ = nearPP f w
    lo0 = (1, mag f (rv f w))
    searchUp lo@(lok, lod)
      | inE f wh && inA f wh = searchUp hi
      | otherwise = search lo hi
      where
        hi = (hik, mag f wh)
        hik = 2 * lok
        wl = stimes lok (rv f x) `at` w
        wh = stimes hik (rv f x) `at` w
    search lo@(lok, lod) hi@(hik, hid)
      | hik - lok <= 1 =
          julia f (k + lok) (l `at` w) ((d l `at` w) * dw)
      | inE f wm && inA f wm = search md hi
      | otherwise = search lo md
      where
        wm = stimes mdk (rv f x) `at` w
        l = stimes lok (rv f x)
        md = (mdk, mdd)
        mdk = (lok + hik + 1) `div` 2
        mdd = mag f wm

image :: (Show f, RealFloat f) => (f -> Formula f) -> Complex f -> f -> String
image f c r = "P5\n" ++ show w ++ " " ++ show h ++ "\n" ++ "255\n" ++ (concat . parallel)
  [ [ case julia (f pixelSpacing) 0 w dw of
        d -> chr (round (255 * tanh d))
    | i <- [0 .. w - 1]
    , let u = ((fromInteger i + 0.5) / fromInteger w - 0.5) * (fromInteger w / fromInteger h)
    , let w = c + (r * u :+ r * v)
    , let dw = pixelSpacing :+ 0
    ]
  | j <- reverse [0 .. h - 1]
  , let v = (fromInteger j + 0.5) / fromInteger h - 0.5
  ]
  where
    w = h
    h = 512
    pixelSpacing = r / fromInteger h

main :: IO ()
main = do
  hSetBinaryMode stdout True
  hSetBuffering stdout NoBuffering
  forM_ [1..8] $ \v -> do
    putStr $ image (blobble v ([0.05, 0.1, 0.2, 0.3, 0.3, 0.4, 0.4, 0.5] !! fromInteger (v - 1))) 0 3
{-
  putStr $ image cauliflower (-0.5) 3
  putStr $ image basilica 0 3
  putStr $ image rabbit 0 3
  putStr $ image cauliflower 0 0.3
  putStr $ image basilica 0 0.3
  putStr $ image rabbit 0 0.3
  putStr $ image cauliflower 0 0.03
  putStr $ image basilica 0 0.03
  putStr $ image rabbit 0 0.03

putStrSlow [] = return ()
putStrSlow (c:cs) = do
  putChar c
  hFlush stdout
  putStrSlow cs
-}
