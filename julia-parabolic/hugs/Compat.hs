module Compat where

class Semigroup g where
  (<>) :: g -> g -> g
  stimes :: Integral n => n -> g -> g
  stimes 1 s = s
  stimes n s = stimes m s <> stimes (n - m) s
    where m = div n 2

parallel :: [String] -> [String]
parallel = id
