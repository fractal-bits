# julia-parabolic

Implementation based on ideas in the paper:

> "Parabolic Julia sets are polynomial time computable"
> Mark Braverman
> <https://arxiv.org/abs/math/0505036>

