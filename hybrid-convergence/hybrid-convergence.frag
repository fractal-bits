#version 330 core

#include "TwoD.frag"

uniform int Iterations; slider[1,100,1000]
uniform float Radius; slider[1,100,1000]
uniform float radius; slider[0,0.001,1]


float ER2 = Radius * Radius;
float er2 = radius * radius;

struct Dual { float v, dzx, dzy, dcx, dcy; };
struct Complex { Dual x, y; };

Dual neg(Dual a)
{
  return Dual
    ( -a.v
    , -a.dzx
    , -a.dzy
    , -a.dcx
    , -a.dcy
    );
}

Dual add(Dual a, Dual b)
{
  return Dual
    ( a.v + b.v
    , a.dzx + b.dzx
    , a.dzy + b.dzy
    , a.dcx + b.dcx
    , a.dcy + b.dcy)
    ;
}

Dual mul(Dual a, Dual b)
{
  return Dual
    ( a.v * b.v
    , a.dzx * b.v + a.v * b.dzx
    , a.dzy * b.v + a.v * b.dzy
    , a.dcx * b.v + a.v * b.dcx
    , a.dcy * b.v + a.v * b.dcy
    );
}

Dual sub(Dual a, Dual b)
{
  return add(a, neg(b));
}

Complex neg(Complex a)
{
  return Complex(neg(a.x), neg(a.y));
}

Complex add(Complex a, Complex b)
{
  return Complex(add(a.x, b.x), add(a.y, b.y));
}

Complex mul(Complex a, Complex b)
{
  return Complex(sub(mul(a.x, b.x), mul(a.y, b.y)), add(mul(a.x, b.y), mul(a.y, b.x)));
}

Complex sub(Complex a, Complex b)
{
  return add(a, neg(b));
}

float normz(Complex a)
{
  return a.x.v * a.x.v + a.y.v * a.y.v;
}

float normdz(Complex a)
{
  return a.x.dzx * a.x.dzx + a.x.dzy * a.x.dzy + a.y.dzx * a.y.dzx + a.y.dzy * a.y.dzy;
}

vec3 color(vec2 p, vec2 px, vec2 py)
{
  Complex c = Complex(Dual(p.x, 0, 0, px.x, px.y), Dual(p.y, 0, 0, py.x, py.y));
  Complex z = Complex(Dual(p.x, 1, 0, px.x, px.y), Dual(p.y, 0, 1, py.x, py.y));
  vec3 s = vec3(0);
  int i;
  for (i = 0; i < Iterations; ++i)
  {
    if (! (normz(z) < ER2)) break;
    if (! (normdz(z) > er2)) break;
    s.x += z.x.v / sqrt(normz(z));
    s.y += z.y.v / sqrt(normz(z));
    s.z += 0.0;
    if ((i & 3) == 1)
    {
      if (z.x.v < 0) z.x = neg(z.x);
      if (z.y.v < 0) z.y = neg(z.y);
      z.y = neg(z.y);
    }
    z = add(mul(z, mul(z, mul(z, z))), c);
  }
  if (! (normz(z) < ER2))
  {
    float X = z.x.v;
    float Y = z.y.v;
    vec2 Z = vec2(X, Y);
    float Xx = z.x.dcx;
    float Xy = z.x.dcy;
    float Yx = z.y.dcx;
    float Yy = z.y.dcy;
    // de
    float Z2 = X * X + Y * Y;
    mat2 J = (mat2(Xx, Yx, Xy, Yy));
    vec2 de = log(Z2) * Z2 / length(Z * J) * normalize(Z*J);
    float v = tanh(clamp(length(de), 0, 4));
    return v *(0.5 * vec3(normalize(de),0) +vec3(0.5));
  }
  if (! (normdz(z) > er2))
  {
    return 0.5 * s/float(i+1) + vec3(0.5);
  }
  return vec3(0.0);
}
