// gcc -std=c99 -Wall -pedantic -Wextra -O3 -fopenmp -o bbrender bbrender.c `PKG_CONFIG_PATH=${HOME}/opt/lib/pkgconfig pkg-config --cflags --libs mandelbrot-numerics` -lm
// ./bbrender | pnmsplit - bbrender-%d.pgm

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <mandelbrot-numerics.h>

typedef unsigned char B;
typedef float F;
typedef uint64_t N;
typedef int64_t Z;
typedef double R;
typedef double _Complex C;

#define unlikely(x) __builtin_expect(x, 0)
#undef BB_BOUNDS_CHECKS

struct partial {
  C z;
  Z p;
};


struct compute {
  Z tag, bias;
  struct partial *partials;
  Z npartials, np, n;
  R er2, mz2;
  C c, z;
};


static struct compute *compute_new(Z npartials) {
  struct compute *px = malloc(sizeof(*px));
  if (! px) {
    return 0;
  }
  if (npartials > 0) {
    px->partials = malloc(npartials * sizeof(*(px->partials)));
    px->npartials = npartials;
    if (! px->partials) {
      free(px);
      return 0;
    }
  } else {
    px->partials = 0;
    px->npartials = 0;
  }
  px->tag = 0;
  return px;
}


static void compute_delete(struct compute *px) {
  if (px) {
    if (px->partials) {
      free(px->partials);
    }
    free(px);
  }
}


static void compute_init(struct compute *px, Z bias, C c) {
  if (! px) { return; }
  px->tag = 0;
  px->bias = bias;
  px->mz2 = 1.0 / 0.0;
  px->c = c;
  px->z = 0;
  px->n = 0;
  px->np = 0;
}

static bool is_interior(C c, C z, Z p, Z steps)
{
  C z00 = 0;
  if (m_failed != m_d_attractor(&z00, z, c, p, steps)) {
    C z0 = z00;
    C dz0 = 1;
    for (Z j = 0; j < p; ++j)
    {
      dz0 = 2.0 * z0 * dz0;
      z0 = z0 * z0 + c;
    }
    return creal(dz0)*creal(dz0) + cimag(dz0)*cimag(dz0) <= 1;
  }
  return false;
}

extern bool compute_step(struct compute *px, Z steps)
{
  static const double er2 = 4;
  if (! px) {
    return false;
  }
  if (px->tag != 0) {
    return true;
  }
  C c = px->c;
  C z = px->z;
  R mz2 = px->mz2;
  Z n = px->n;
  R cx = creal(c);
  R cy = cimag(c);
  R zx = creal(z);
  R zy = cimag(z);
  R zx2 = zx * zx;
  R zy2 = zy * zy;
  R zxy = zx * zy;
  if (px->bias < 0)
  {
    for (Z i = 1; i <= steps; ++i)
    {
      zx = zx2 - zy2 + cx;
      zy = zxy + zxy + cy;
      zx2 = zx * zx;
      zy2 = zy * zy;
      zxy = zx * zy;
      R z2 = zx2 + zy2;
      if (unlikely(z2 < mz2))
      {
        mz2 = z2;
        if (is_interior(c, zx + I * zy, n + i, 16))
        {
          px->tag = -1;
          return true;
        }
      }
      if (unlikely(z2 >= er2))
      {
        px->tag = 1;
        px->n = n + i;
        return true;
      }
    }
    px->tag = 0;
    px->z = zx + I * zy;
    px->mz2 = mz2;
    px->n = n + steps;
    return false;
  }
  else
  {
    for (Z i = 1; i <= steps; ++i)
    {
      zx = zx2 - zy2 + cx;
      zy = zxy + zxy + cy;
      zx2 = zx * zx;
      zy2 = zy * zy;
      zxy = zx * zy;
      R z2 = zx2 + zy2;
      if (unlikely(z2 < mz2))
      {
        mz2 = z2;
        if (px->partials && px->np < px->npartials)
        {
          px->partials[px->np].z = z;
          px->partials[px->np].p = n + i;
          px->np = px->np + 1;
        }
      }
      if (unlikely(z2 >= er2))
      {
        px->tag = 1;
        px->n = n + i;
        return true;
      }
    }
    for (Z i = 0; i < px->np; ++i)
    {
      z = px->partials[i].z;
      Z p = px->partials[i].p;
      if (is_interior(c, z, p, 16))
      {
        px->tag = -1;
        px->z = z;
        px->n = p;
        return true;
      }
    }
    px->tag = 0;
    px->z = zx + I * zy;
    px->mz2 = mz2;
    px->n = n + steps;
    return false;
  }
  return false;
}


#define LEVELS 14


struct image
{
  N *n[LEVELS];
  B *b[LEVELS];
  F *f[2][LEVELS];
};


static struct image *image_new()
{
  struct image *img = calloc(1, sizeof(*img));
  for (Z level = 0; level < LEVELS; ++level)
  {
    Z grid = 1 << level;
    Z bytes = grid * grid;
    img->n[level] = calloc(1, sizeof(N) * bytes);
    img->b[level] = calloc(1, bytes);
    img->f[0][level] = calloc(1, sizeof(F) * bytes);
    img->f[1][level] = calloc(1, sizeof(F) * bytes);
  }
  return img;
}


static void image_delete(struct image *img)
{
  for (Z level = 0; level < LEVELS; ++level)
  {
    free(img->n[level]);
    free(img->b[level]);
    free(img->f[0][level]);
    free(img->f[1][level]);
  }
  free(img);
}


static inline void image_plot(struct image *img, R zx, R zy)
{
  // flipped along main diagonal
#ifdef BB_BOUNDS_CHECKS
  Z y = floor((1 << (LEVELS-1)) * (zx + 2.0) / 4.0);
  Z x = floor((1 << (LEVELS-1)) * (zy + 2.0) / 4.0);
  if (0 <= x && x < (1 << (LEVELS-1)) && 0 <= y && y < (1 << (LEVELS-1)))
  {
#else
  Z y = (1 << (LEVELS-1)) * (zx + 2.0) / 4.0;
  Z x = (1 << (LEVELS-1)) * (zy + 2.0) / 4.0;
#endif
    Z k = (y << (LEVELS - 1)) + x;
    N *p = img->n[LEVELS - 1] + k;
    #pragma omp atomic
    *p += 1;
#ifdef BB_BOUNDS_CHECKS
  }
#endif
}


static void image_post(struct image *img)
{
  for (Z src = LEVELS - 1; src > 0; --src)
  {
    Z dst = src - 1;
    N *srcp = img->n[src];
    N *dstp = img->n[dst];
    #pragma omp parallel for
    for (Z y = 0; y < (1 << dst); ++y)
      for (Z x = 0; x < (1 << dst); ++x)
      {
        N s = 0;
        for (Z dy = 0; dy < 2; ++dy)
          for (Z dx = 0; dx < 2; ++dx)
          {
            Z k = (((y << 1) + dy) << src) + ((x << 1) + dx);
            s += srcp[k];
          }
        Z k = (y << dst) + x;
        dstp[k] = s;
      }
  }
  N total = img->n[0][0];
  for (Z level = 0; level < LEVELS; ++level)
  {
    Z pixels = 1 << level;
    pixels *= pixels;
    R average = total / (R) pixels;
    R norm = 1.0 / average;
    R scale = 255.0 / (16.0 * average);
    N *n = img->n[level];
    F *f = img->f[0][level];
    F *g = img->f[1][level];
    B *b = img->b[level];
    #pragma omp parallel for
    for (Z k = 0; k < pixels; ++k)
    {
      Z x = k & ((1 << level) - 1);
      Z y = k >> level;
      R dither = (((y * 237 + x) * 119) & 255) / 256.0;
      g[k] = f[k];
      f[k] = norm * n[k];
      b[k] = floor(fmin(fmax(scale * n[k] + dither, 0.0), 255.0));
    }
  }
}


static R image_rms_diff(struct image *img, Z level)
{
  Z pixels = 1 << level;
  pixels *= pixels;
  F *f = img->f[0][level];
  F *g = img->f[1][level];
  R sum = 0;
  #pragma omp parallel for reduction(+:sum)
  for (Z k = 0; k < pixels; ++k)
  {
    R d = f[k] - g[k];
    d *= d;
    sum = sum + d;
  }
  R rms = sqrt(sum / pixels);
  return rms;
}


static void image_save(struct image *img, Z level, Z depth)
{
  Z pixels = 1 << level;
  pixels *= pixels;
  char filename[100];
  snprintf(filename, 100, "bb-%02d-%02d.pgm", (int) depth, (int) level);
  FILE *out = fopen(filename, "wb");
  fprintf(out, "P5\n%d %d\n255\n", 1 << level, 1 << level);
  fwrite(img->b[level], pixels, 1, out);
  fflush(out);
  fclose(out);
  snprintf(filename, 100, "bb-%02d-%02d.u64", (int) depth, (int) level);
  out = fopen(filename, "wb");
  fwrite(img->n[level], sizeof(N) * pixels, 1, out);
  fflush(out);
  fclose(out);
}


static void render(struct image *img, Z maxiters, Z grid, C mul, C add)
{
  #pragma omp parallel for schedule(dynamic, 1)
  for (Z j = 0; j < grid; ++j)
  {
    struct compute *px = compute_new(maxiters);
    for (Z i = 0; i < grid; ++i)
    {
      C c = mul * (i + I * j) + add;
      compute_init(px, px->tag, c);
      compute_step(px, maxiters);
      if (px->tag > 0)
      {
        R zx = 0;
        R zy = 0;
        R cx = creal(c);
        R cy = cimag(c);
        Z count = px->n - 1;
        R zx2 = zx * zx;
        R zy2 = zy * zy;
        R zxy = zx * zy;
        for (Z n = 0; n < count; ++n)
        {
          zx = zx2 - zy2 + cx;
          zy = zxy + zxy + cy;
          zx2 = zx * zx;
          zy2 = zy * zy;
          zxy = zx * zy;
          image_plot(img, zx, zy);
        }
      }
    }
    compute_delete(px);
  }
}


extern int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  srand(0xbeefcafe);
  static const R threshold = 0.05;
  for (Z level = LEVELS - 1; level < LEVELS; ++level)
  {
    for (Z depth = 20; depth <= 20; ++depth)
    {
      Z maxiters = 1 << depth;
      Z pixels = 0;
      struct image *img = image_new();
      for (Z pass = 0; 1; ++pass)
      {
        for (Z count = 0; count < 1 << pass; ++count)
        {
          fprintf(stderr, "\r %ld, %ld : %ld : %ld : %ld ", level, depth, pass, ((Z)1) << pass, count);
          fflush(stderr);
          // randomize grid
          Z n = exp2(10 + (rand() / (R) RAND_MAX));
          n += (n & 1); // ensure even
          R l = 2.0 / (n/2 - 1) + (2.0/(n/2-3) - 2.0/(n/2-1)) * (rand() / (R) RAND_MAX);
          l *= 3.6;
          R x = (rand() / (R) RAND_MAX);
          R y = (rand() / (R) RAND_MAX);
          R t = 2 * 3.141592653589793 * (rand() / (R) RAND_MAX);
          C mul = l * cexp(I * t);
          C add = - mul * ((n/2 + x) + I * ((n/2) + y));
          // calculate
          render(img, maxiters, n, mul, add);
          // output
          pixels += n * n;
        }
        image_post(img);
        fprintf(stderr, "\r saving... ");
        fflush(stderr);
        image_save(img, level, depth);
        fprintf(stderr, "saved! ");
        fflush(stderr);
        if (pass)
        {
          R rms = image_rms_diff(img, level);
          if (rms < threshold)
          {
            fprintf(stderr, "\n");
            fflush(stderr);
            printf("%ld %ld %ld %.18e %ld %lu\n", level, depth, pass, rms, pixels, img->n[0][0]);
            fflush(stdout);
            break;
          }
        }
      }
      image_delete(img);
    }
  }
  return 0;
}


// END
