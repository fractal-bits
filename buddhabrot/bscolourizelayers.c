#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int64_t Z;
typedef uint64_t N;


int cmp_N(const void *a, const void *b)
{
  const Z *p = a;
  const Z *q = b;
  Z x = *p;
  Z y = *q;
  return (x > y) - (x < y);
}

int cmp_float(const void *a, const void *b)
{
  const float *p = a;
  const float *q = b;
  float x = *p;
  float y = *q;
  return (x > y) - (x < y);
}



#define LOG2SIZE 12
#define SIZE (1 << LOG2SIZE)
#define LAYERS 30


struct image
{
  N n[LAYERS][SIZE * SIZE];
};


FILE *image_file = 0;


static struct image *image_map()
{
  image_file = fopen("bb.map", "rb");
  if (! image_file)
  {
    exit(1);
  }
  struct image *img = malloc(sizeof(struct image));
  if (! img)
  {
    exit(1);
  }
  fread(img, sizeof(struct image), 1, image_file);
  return img;
}


static void image_unmap(struct image *img)
{
  fclose(image_file);
  image_file = 0;
  free(img);
}


static double xyz2lab_f(double t)
{
  static const double e = 0.008856;
  static const double k = 903.3;
  if (t > e)
    return cbrt(t);
  else
    return (k * t + 16) / 116;
}
static void xyz2lab(double x, double y, double z, double *l, double *a, double *b)
{
  static const double xn = 0.95047;
  static const double yn = 1.00000;
  static const double zn = 1.08883;
  x /= xn;
  y /= yn;
  z /= zn;
  x = xyz2lab_f(x);
  y = xyz2lab_f(y);
  z = xyz2lab_f(z);
  *l = 116 * y - 16;
  *a = 500 * (x - y);
  *b = 200 * (y - z);
}

static double lab2xyz_f1(double t)
{
  static const double e = 0.008856;
  static const double k = 903.3;
  if (t * t * t > e)
    return t * t * t;
  else
    return (116 * t - 16) / k;
}
static double lab2xyz_f2(double l)
{
  static const double e = 0.008856;
  static const double k = 903.3;
  if (l > k * e)
  {
    double t = (l + 16) / 116;
    return t * t * t;
  }
  else
    return l / k;
}
static void lab2xyz(double l, double a, double b, double *x, double *y, double *z)
{
  static const double xn = 0.95047;
  static const double yn = 1.00000;
  static const double zn = 1.08883;
  double fy = (l + 16) / 116;
  double fz = fy - b / 200;
  double fx = fy + a / 500;
  *x = xn * lab2xyz_f1(fx);
  *y = yn * lab2xyz_f2(l);
  *z = zn * lab2xyz_f1(fz);
}


static double xyz2srgb_f(double c)
{
  if (c < 0.0031308)
    return 12.92 * c;
  else
    return 1.055 * pow(c, 1/2.4) - 0.055;
}
static void xyz2srgb(double x, double y, double z, double *r, double *g, double *b)
{
  static const double m[3][3] =
    { {  3.2406, -1.5372, -0.4986 }
    , { -0.9689,  1.8758,  0.0415 }
    , {  0.0557, -0.2040,  1.0570 }
    };
  *r = xyz2srgb_f(m[0][0] * x + m[0][1] * y + m[0][2] * z);
  *g = xyz2srgb_f(m[1][0] * x + m[1][1] * y + m[1][2] * z);
  *b = xyz2srgb_f(m[2][0] * x + m[2][1] * y + m[2][2] * z);
}


static double srgb2xyz_f(double c)
{
  if (c < 0.04045)
    return c / 12.92;
  else
    return pow((c + 0.055) / 1.055, 2.4);
}
static void srgb2xyz(double r, double g, double b, double *x, double *y, double *z)
{
  static const double m[3][3] =
    { { 0.4124, 0.3576, 0.1805 }
    , { 0.2126, 0.7152, 0.0722 }
    , { 0.0193, 0.1192, 0.9505 }
    };
  r = srgb2xyz_f(r);
  g = srgb2xyz_f(g);
  b = srgb2xyz_f(b);
  *x = m[0][0] * r + m[0][1] * g + m[0][2] * b;
  *y = m[1][0] * r + m[1][1] * g + m[1][2] * b;
  *z = m[2][0] * r + m[2][1] * g + m[2][2] * b;
}


unsigned char spectrum[30][3];

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  {
    FILE *sf = fopen("spectrum.ppm", "rb");
    fseek(sf, -LAYERS * 3, SEEK_END);
    fread(spectrum, LAYERS * 3, 1, sf);
    fclose(sf);
  }
  struct image *img = image_map();
  float *raw = calloc(1, sizeof(float) * 3 * SIZE * SIZE);
#if 1
  float *histogram = malloc(sizeof(float) * SIZE * SIZE);
#endif
  unsigned char *ppm = malloc(3 * SIZE * SIZE);
  N total = 0;
  for (int layer = 0; layer < LAYERS; ++layer)
    #pragma omp parallel for reduction(+:total)
    for (Z k = 0; k < SIZE * SIZE; ++k)
      total += img->n[layer][k];
  double scale = SIZE * SIZE * 1.0 / total;
  double scale2 = 255 * scale;
  scale = 1/log(1 + 1/scale);
  printf("%lu %e\n", total, scale);
  for (int layer = 0; layer < LAYERS; ++layer)
  {
    char filename[100];
    snprintf(filename, 100, "layer-%02d.pgm", layer);
    double l, a, b;
    { // convert from srgb to lab
      double r, g, bb;
      double x, y, z;
      printf("  %02d\r", layer);
      fflush(stdout);
      r  = spectrum[layer][0]/255.0;
      g  = spectrum[layer][1]/255.0;
      bb = spectrum[layer][2]/255.0;
//      printf("r g b: %f %f %f\n", r, g, bb);
      srgb2xyz(r, g, bb, &x, &y, &z);
//      printf("x y z: %f %f %f\n", x, y, z);
      xyz2lab(x, y, z, &l, &a, &b);
//      printf("l a b: %f %f %f\n", l, a, b);
//      lab2xyz(l, a, b, &x, &y, &z);
//      printf("x y z: %f %f %f\n", x, y, z);
//      xyz2srgb(x, y, z, &r, &g, &bb);
//      printf("r g b: %f %f %f\n", r, g, bb);
      l /= LAYERS;
      a /= LAYERS;
      b /= LAYERS;
    }
    #pragma omp parallel for
    for (Z j = 0; j < SIZE; ++j)
    for (Z i = 0; i < SIZE; ++i)
    {
      Z k = j * SIZE + i;
      double x = scale * log(1 + img->n[layer][k]);
#if 1
      double dither = ((((layer * 67 + j) * 236 + i) * 119) & 255) / 256.0;
      double y = 255 * x + dither;
      y = y > 255 ? 255 : y;
      ppm[k] = y;
#endif
      raw[3 * k + 0] += x * l;
      raw[3 * k + 1] += x * a;
      raw[3 * k + 2] += x * b;
    }
#if 1
    FILE *f = fopen(filename, "wb");
    fprintf(f, "P5\n%d %d\n255\n", SIZE, SIZE);
    fwrite(ppm, SIZE * SIZE, 1, f);
    fclose(f);
#endif
  }
  { // clamp
    #pragma omp parallel for
    for (Z k = 0; k < SIZE * SIZE; ++k)
    {
      double l = raw[3 * k + 0];
      if (l > 100)
      {
        double s = 100 / l;
        double a = raw[3 * k + 1];
        double b = raw[3 * k + 2];
        l *= s;
        a *= s;
        b *= s;
        raw[3 * k + 0] = l;
        raw[3 * k + 1] = a;
        raw[3 * k + 2] = b;
      }
    }
  }

#if 1
  {
    // auto white balance (make average colour white)
    double l = 0, a = 0, b = 0;
    #pragma omp parallel for reduction(+:l) reduction(+:a) reduction(+:b)
    for (Z k = 0; k < SIZE * SIZE; ++k)
    {
      l += raw[3 * k + 0];
      a += raw[3 * k + 1];
      b += raw[3 * k + 2];
    }
    l /= SIZE * SIZE;
    a /= SIZE * SIZE;
    b /= SIZE * SIZE;
    const double scale = 1.0 / 12 * 100 / l;
    const double shift = 1.0 / 100;
    #pragma omp parallel for
    for (Z k = 0; k < SIZE * SIZE; ++k)
    {
      double t = shift * raw[3 * k + 0];
      raw[3 * k + 1] -= a * t;
      raw[3 * k + 2] -= b * t;
      raw[3 * k + 0] *= scale;
      raw[3 * k + 1] *= scale;
      raw[3 * k + 2] *= scale;
    }
  }
#endif

#if 1
  { // auto-levels
    double quantile_lo = 0.001;
    double quantile_hi = 0.999;
    #pragma omp parallel for
    for (Z k = 0; k < SIZE * SIZE; ++k)
    {
      double l = raw[3 * k + 0];
      double a = raw[3 * k + 1];
      double b = raw[3 * k + 2];
      double x, y, z;
      double r, g, bb;
      lab2xyz(l, a, b, &x, &y, &z);
      xyz2srgb(x, y, z, &r, &g, &bb);
      raw[3 * k + 0] = r;
      raw[3 * k + 1] = g;
      raw[3 * k + 2] = bb;
    }
    for (int c = 0; c < 3; ++c)
    {
      #pragma omp parallel for
      for (Z k = 0; k < SIZE * SIZE; ++k)
        histogram[k] = raw[3 * k + c];
      qsort(histogram, SIZE * SIZE, sizeof(float), cmp_float);
      double lo = histogram[(Z) (SIZE * SIZE * quantile_lo)];
      double hi = histogram[(Z) (SIZE * SIZE * quantile_hi)];
      double range = 1.0 / (hi - lo);
      if (range > 0) // nan check
      {
        #pragma omp parallel for
        for (Z k = 0; k < SIZE * SIZE; ++k)
        {
          double v = raw[3 * k + c];
          v -= lo;
          v *= range;
          v = v < 0 ? 0 : v;
          v = v > 1 ? 1 : v;
          raw[3 * k + c] = v;
        }
      }
    }
    #pragma omp parallel for
    for (Z k = 0; k < SIZE * SIZE; ++k)
    {
      double r  = raw[3 * k + 0];
      double g  = raw[3 * k + 1];
      double bb = raw[3 * k + 2];
      double x, y, z;
      double l, a, b;
      srgb2xyz(r, g, bb, &x, &y, &z);
      xyz2lab(x, y, z, &l, &a, &b);
      raw[3 * k + 0] = l;
      raw[3 * k + 1] = a;
      raw[3 * k + 2] = b;
    }
  }
#endif


#if 1
  { // increase saturation
    // https://math.stackexchange.com/questions/586424/adjust-saturation-in-cie-lab-space
    const double saturation = 2;
    const double limit = 0.95;
    #pragma omp parallel for
    for (Z k = 0; k < SIZE * SIZE; ++k)
    {
      double l = raw[3 * k + 0];
      double a = raw[3 * k + 1];
      double b = raw[3 * k + 2];
      double c = hypot(a, b);
      double s = c / hypot(l, c);
      s *= saturation;
      if (s > limit) s = limit;
      double t = s * l / sqrt((a * a + b * b) * (1 - s * s));
      if (t > 0) // nan check
      {
        a *= t;
        b *= t;
      }
      raw[3 * k + 1] = a;
      raw[3 * k + 2] = b;
    }
  }
#endif

#if 1
  {
    // auto white balance again (make average colour white)
    double l = 0, a = 0, b = 0;
    #pragma omp parallel for reduction(+:l) reduction(+:a) reduction(+:b)
    for (Z k = 0; k < SIZE * SIZE; ++k)
    {
      l += raw[3 * k + 0];
      a += raw[3 * k + 1];
      b += raw[3 * k + 2];
    }
    l /= SIZE * SIZE;
    a /= SIZE * SIZE;
    b /= SIZE * SIZE;
    const double scale = 1.0 / 12 * 100 / l * 5 / 2;
    const double shift = 1.0 / 100;
    #pragma omp parallel for
    for (Z k = 0; k < SIZE * SIZE; ++k)
    {
      double t = shift * raw[3 * k + 0];
      raw[3 * k + 1] -= a * t;
      raw[3 * k + 2] -= b * t;
      raw[3 * k + 0] *= scale;
      raw[3 * k + 1] *= scale;
      raw[3 * k + 2] *= scale;
    }
  }
#endif

  { // lab to 8bit srgb
    #pragma omp parallel for
    for (Z j = 0; j < SIZE; ++j)
    for (Z i = 0; i < SIZE; ++i)
    {
      Z k = j * SIZE + i;
#if 1
      double l = raw[3 * k + 0];
      double a = raw[3 * k + 1];
      double b = raw[3 * k + 2];
      double x, y, z, r, g;
      lab2xyz(l, a, b, &x, &y, &z);
      xyz2srgb(x, y, z, &r, &g, &b);
#else
      double r = raw[3 * k + 0];
      double g = raw[3 * k + 1];
      double b = raw[3 * k + 2];
#endif
      double dither;
      dither = ((((0 * 67 + j) * 236 + i) * 119) & 255) / 256.0;
      ppm[3 * k + 0] = fmin(fmax(r * 255 + dither, 0), 255);
      dither = ((((1 * 67 + j) * 236 + i) * 119) & 255) / 256.0;
      ppm[3 * k + 1] = fmin(fmax(g * 255 + dither, 0), 255);
      dither = ((((2 * 67 + j) * 236 + i) * 119) & 255) / 256.0;
      ppm[3 * k + 2] = fmin(fmax(b * 255 + dither, 0), 255);
    }
  }
  FILE *f = fopen("colourized.ppm", "wb");
  fprintf(f, "P6\n%d %d\n255\n", SIZE, SIZE);
  fwrite(ppm, 3 * SIZE * SIZE, 1, f);
  fclose(f);
  free(ppm);
  image_unmap(img);
}
