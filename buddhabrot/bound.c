// gcc -std=c99 -Wall -pedantic -Wextra -O3 -fopenmp -o bound bound.c `PKG_CONFIG_PATH=${HOME}/opt/lib/pkgconfig pkg-config --cflags --libs mandelbrot-graphics`
// ./bound | tee bound.dat

#include <complex.h>
#include <math.h>
#include <stdio.h>

#include <mandelbrot-graphics.h>

typedef uint64_t N;
typedef int64_t Z;
typedef double R;
typedef double _Complex C;

static void render(N depth) {
  N maxiters = ((N)1) << depth;
  N grid = ((N)1) << depth;
  R s = 0;
  #pragma omp parallel for schedule(dynamic, 1) reduction(+:s)
  for (N j = 0; j < grid/2; ++j) {
    R ls = 0;
    m_d_compute *px = m_d_compute_alloc(maxiters);
    for (N i = 0; i < grid; ++i) {
      C c = (-2.5 - 2.5 * I) + 5.0 * ((i + 0.5) / grid + I * (j + 0.5) / grid);
      m_d_compute_init(px, m_d_compute_get_tag(px), 600, c, 0);
      m_d_compute_step(px, maxiters);
      if (m_d_compute_get_tag(px) == m_exterior)
      {
        R d = m_d_compute_get_de(px);
        Z n = m_d_compute_get_n(px);
        R a = n * d * d / (grid/2.0 * grid);
        ls += a;
      }
    }
    m_d_compute_free(px);
    s = s + ls;
  }
  printf("%d %.18f\n", (int)depth, s);
  fflush(stdout);
}


extern int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  for (N depth = 8; 1; ++depth)
    render(depth);
  return 0;
}


// END
