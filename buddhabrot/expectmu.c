// gcc -std=c99 -Wall -pedantic -Wextra -O3 -fopenmp -o expectmu expectmu.c -lm

#include <assert.h>
#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef int64_t Z;
typedef double R;
typedef double _Complex C;

#define unlikely(x) __builtin_expect(x, 0)

struct partial {
  C z;
  Z p;
};

struct compute {
  Z tag, bias;
  struct partial *partials;
  Z npartials, np, n;
  R er2, mz2;
  C c, z;
};

static struct compute *compute_new(Z npartials) {
  struct compute *px = malloc(sizeof(*px));
  if (! px) {
    return 0;
  }
  if (npartials > 0) {
    px->partials = malloc(npartials * sizeof(*(px->partials)));
    px->npartials = npartials;
    if (! px->partials) {
      free(px);
      return 0;
    }
  } else {
    px->partials = 0;
    px->npartials = 0;
  }
  px->tag = 0;
  return px;
}

static void compute_delete(struct compute *px) {
  if (px) {
    if (px->partials) {
      free(px->partials);
    }
    free(px);
  }
}

static void compute_init(struct compute *px, Z bias, C c) {
  if (! px) { return; }
  px->tag = 0;
  px->bias = bias;
  px->mz2 = 1.0 / 0.0;
  px->c = c;
  px->z = 0;
  px->n = 0;
  px->np = 0;
}

static bool cisfinite(C z)
{
  return isfinite(creal(z)) && isfinite(cimag(z));
}

static Z attractor_step(C *z, C z_guess, C c, Z period) {
  // one newton step for solving z = f^p(z, c)
  R epsilon = nextafter(1, 2) - 1;
  C zz = z_guess;
  C dzz = 1;
  for (Z i = 0; i < period; ++i) {
    dzz = 2 * zz * dzz;
    zz = zz * zz + c;
  }
  if (cabs(zz - z_guess) <= epsilon) {
    *z = z_guess;
    return 0; // converged
  }
  C z_new = z_guess - (zz - z_guess) / (dzz - 1);
  C d = z_new - zz;
  if (cabs(d) <= epsilon) {
    *z = z_new;
    return 0; // converged
  }
  if (cisfinite(d)) {
    *z = z_new;
    return 1; // stepped
  } else {
    *z = z_guess;
    return -1; // failed
  }
}

static Z attractor(C *z_out, C z_guess, C c, Z period, Z maxsteps) {
  // newton's method
  Z result = -1;
  C z = z_guess;
  for (Z i = 0; i < maxsteps; ++i) {
    if (1 != (result = attractor_step(&z, z, c, period))) {
      break;
    }
  }
  *z_out = z;
  return result;
}

static bool is_interior(C c, C z, Z p, Z steps)
{
  // find attractor
  C z00 = 0;
  if (-1 != attractor(&z00, z, c, p, steps)) {
    // compute derivative
    C z0 = z00;
    C dz0 = 1;
    for (Z j = 0; j < p; ++j)
    {
      dz0 = 2.0 * z0 * dz0;
      z0 = z0 * z0 + c;
    }
    // numerator of interior distance forumula is positive?
    return cabs(dz0) <= 1;
  }
  return false;
}

static bool compute_step(struct compute *px, Z steps)
{
  static const double er2 = 4;
  if (! px) {
    return false;
  }
  if (px->tag != 0) {
    return true;
  }
  px->np = 0;
  // load state
  C c = px->c;
  C z = px->z;
  R mz2 = px->mz2;
  Z n = px->n;
  R cx = creal(c);
  R cy = cimag(c);
  R zx = creal(z);
  R zy = cimag(z);
  R zx2 = zx * zx;
  R zy2 = zy * zy;
  R zxy = zx * zy;
  // if last pixel was interior, perform interior checks as we go
  if (px->bias < 0)
  {
    for (Z i = 1; i <= steps; ++i)
    {
      // step
      zx = zx2 - zy2 + cx;
      zy = zxy + zxy + cy;
      zx2 = zx * zx;
      zy2 = zy * zy;
      zxy = zx * zy;
      R z2 = zx2 + zy2;
      if (unlikely(z2 < mz2))
      {
        // check interior
        mz2 = z2;
        if (is_interior(c, zx + I * zy, n + i, 64))
        {
          px->tag = -1;
          return true;
        }
      }
      if (unlikely(z2 >= er2))
      {
        // escaped
        px->tag = 1;
        px->n = n + i;
        px->z = zx + I * zy;
        return true;
      }
    }
    // save state, no conclusions yet
    px->tag = 0;
    px->z = zx + I * zy;
    px->mz2 = mz2;
    px->n = n + steps;
    return false;
  }
  // if last pixel was not interior, postpone interior checks to the end
  else
  {
    for (Z i = 1; i <= steps; ++i)
    {
      // step
      zx = zx2 - zy2 + cx;
      zy = zxy + zxy + cy;
      zx2 = zx * zx;
      zy2 = zy * zy;
      zxy = zx * zy;
      R z2 = zx2 + zy2;
      if (unlikely(z2 < mz2))
      {
        // save for later
        mz2 = z2;
        if (px->partials && px->np < px->npartials)
        {
          px->partials[px->np].z = zx + I * zy;
          px->partials[px->np].p = n + i;
          px->np = px->np + 1;
        }
      }
      if (unlikely(z2 >= er2))
      {
        // escaped
        px->tag = 1;
        px->n = n + i;
        px->z = zx + I * zy;
        return true;
      }
    }
    // perform postponed interior checks
    for (Z i = 0; i < px->np; ++i)
    {
      // check interior
      z = px->partials[i].z;
      Z p = px->partials[i].p;
      if (is_interior(c, z, p, 64))
      {
        px->tag = -1;
        px->z = z;
        px->n = p;
        return true;
      }
    }
    // save state, no conclusions yet
    px->tag = 0;
    px->z = zx + I * zy;
    px->mz2 = mz2;
    px->n = n + steps;
    return false;
  }
  return false;
}

static void render(Z grid, C mul, C add, Z *o_s0, R *o_s1)
{
  R smu = 0;
  Z scount = 0;
  #pragma omp parallel for schedule(dynamic, 1) reduction(+:smu) reduction(+:scount)
  for (Z j = 0; j < grid; ++j)
  {
    R mu = 0;
    Z count = 0;
    // allocate space for at most 1<<24 partials (for interiority checks)
    struct compute *px = compute_new(1 << 24);
    for (Z i = 0; i < grid; ++i)
    {
      C c = mul * (i + I * j) + add;
      // only consider points inside D(0,2), reject real axis part and pre-periodic 0+1i
      if (cabs(c) <= 2 && !(cimag(c) == 0 && -2 <= creal(c) && creal(c) <= 0.25) && !(c == I))
      {
        compute_init(px, px->tag, c);
        // performs interiority checks after every doubling of iterations
        for (Z log2iters = 10; 1; log2iters += 1)
        {
          assert(log2iters < 63); // FIXME
          compute_step(px, ((Z)1) << log2iters);
          if (px->tag != 0)
            break;
        }
        if (px->tag > 0)
        {
          R u = px->n;
          if (u > 0)
          {
            // accumulate output
            mu += u;
            count += 1;
          }
        }
      }
    }
    compute_delete(px);
    smu = smu + mu;
    scount = scount + count;
  }
  *o_s0 += scount;
  *o_s1 += smu;
}

extern int main()
{
  // set random seed
  // TOOD use a reproducible generator from libgsl or similar
  srand(0x1cedcafe);
  Z s0 = 0;
  R s1 = 0;
  R s2 = 0;
  while (1)
  {
    Z ls0 = 0;
    R ls1 = 0;
    // accumulate at least 1<<30 points from complement(M) `intersect` D(0,2)
    while (ls0 < 1 << 30)
    {
      // randomize grid
      Z n = exp2(13 + (rand() / (R) RAND_MAX));
      n += (n & 1); // ensure even
      R l = 2.0 / (n/2 - 1) + (2.0/(n/2-3) - 2.0/(n/2-1)) * (rand() / (R) RAND_MAX);
      l *= 3.6;
      R x = (rand() / (R) RAND_MAX);
      R y = (rand() / (R) RAND_MAX);
      R t = 2 * 3.141592653589793 * (rand() / (R) RAND_MAX);
      C mul = l * cexp(I * t);
      C add = - mul * ((n/2 + x) + I * ((n/2) + y));
      // calculate
      render(n, mul, add, &ls0, &ls1);
      fprintf(stderr, "%4d\r", (int) (ls0 * 100.0 / (1 << 30)));
    }
    // output statistics
    R s = ls1 / ls0;
    s0 += 1;
    s1 += s;
    s2 += s * s;
    R mean = s1 / s0;
    R stddev = sqrt((s0 * s2 - s1 * s1) / (s0 * (s0 - 1)));
    fprintf(stdout, "%.20f %.20f %.20f\n", s, mean, stddev);
    fflush(stdout);
  }
  return 0;
}

// END
