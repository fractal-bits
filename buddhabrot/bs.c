#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define MAXITERS (1LL << 30)

#define l 12
unsigned char state[1<<l][1<<l];
unsigned char priority[1<<l][1<<l];
#define escaped 255
#define unescaped 0
#define unknown 128

int main(int argc, char **argv)
{
  memset(state, unknown, (1 << l) * (1 << l));
  memset(priority, 0, (1 << l) * (1 << l));
  for (int i = 0; i < 1 << l; ++i)
  {
    priority[0][i] = 1;
    priority[i][0] = 1;
    priority[(1<<l)-1][i] = 1;
    priority[i][(1<<l)-1] = 1;
  }
  int passes = 0;
  bool have_some_queued;
  do
  {
    have_some_queued = false;
    fprintf(stderr, "%d\n", passes++);
    #pragma omp parallel for schedule(dynamic, 1)
    for (int i = 0; i < 1 << l; ++i)
      for (int j = 0; j < 1 << l; ++j)
      {
        if (priority[i][j] && state[i][j] != escaped && state[i][j] != unescaped)
        {
          have_some_queued = true;
          // flipped along main diagonal
          double cy = 4.0 * (i + 0.5) / (1 << l) - 2.0;
          double cx = 4.0 * (j + 0.5) / (1 << l) - 2.0;
          double zx = 0;
          double zy = 0;
          double zx2 = zx * zx;
          double zy2 = zy * zy;
          double zxy = fabs(zx * zy);
          int64_t count;
          int e = 0;
          for (count = 1; count < MAXITERS; ++count)
          {
            zx = zx2 - zy2 + cx;
            zy = zxy + zxy + cy;
            zx2 = zx * zx;
            zy2 = zy * zy;
            zxy = fabs(zx * zy);
            if ((zx2 + zy2 > 4))
            {
              e = 1;
              break;
            }
          }
          state[i][j] = e ? escaped : unescaped;
          if (e)
          {
            for (int ii = i - 1; ii  <= i + 1; ++ii)
            {
              for (int jj = j - 1; jj <= j + 1; ++jj)
              {
                if (0 <= ii && ii < 1 << l && 0 <= jj && jj < 1 << l)
                {
//                  #pragma omp atomic
                  priority[ii][jj] += 1;
                }
              }
            }
          }
        }
      }
  } while (have_some_queued);
  fprintf(stdout, "P5\n%d %d\n255\n", 1 << l, 1 << l);
  fwrite(&state[0][0], (1 << l) * (1 << l), 1, stdout);
  return 0;
}
