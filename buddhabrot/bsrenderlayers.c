// gcc -std=c99 -Wall -pedantic -Wextra -O3 -fopenmp -o bsrenderlayers bsrenderlayers.c `PKG_CONFIG_PATH=${HOME}/opt/lib/pkgconfig pkg-config --cflags --libs mandelbrot-numerics` -lm
// dd if=/dev/zero of=bs.map bs=$((1024 * 1024)) count=512
// ./bsrenderlayers

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <time.h>

typedef unsigned char B;
typedef float F;
typedef uint64_t N;
typedef int64_t Z;
typedef double R;
typedef double _Complex C;


#define unlikely(x) __builtin_expect(x, 0)


Z ilog2(Z n)
{
  Z l = 0;
  while (n > 0)
  {
    n >>= 1;
    l += 1;
  }
  return l;
}

#define LOG2SIZE 12
#define SIZE (1 << LOG2SIZE)
#define LAYERS 30

B mask[SIZE][SIZE];

struct image
{
  N n[LAYERS][SIZE * SIZE];
};


FILE *image_file = 0;


static struct image *image_map()
{
  bool mask_ok = false;
  FILE *mask_file = fopen("bs.pgm", "rb");
  int mask_width = 0;
  int mask_height = 0;
  if (2 == fscanf(mask_file, "P5\n%d %d\n255", &mask_width, &mask_height))
  {
    if ('\n' == fgetc(mask_file))
    {
      if (mask_width == SIZE && mask_height == SIZE)
      {
        if (1 == fread(&mask[0][0], SIZE * SIZE, 1, mask_file))
        {
          mask_ok = true;
        }
      }
    }
  }
  fclose(mask_file);
  if (! mask_ok)
  {
    memset(&mask[0][0], 255, SIZE * SIZE);
  }
  image_file = fopen("bs.map", "r+b");
  if (! image_file)
  {
    exit(1);
  }
  struct image *img = malloc(sizeof(struct image));
  if (! img)
  {
    exit(1);
  }
  fread(img, sizeof(struct image), 1, image_file);
  return img;
}


static void image_sync(struct image *img)
{
  rewind(image_file);
  fwrite(img, sizeof(struct image), 1, image_file);
}


static void image_unmap(struct image *img)
{
  image_sync(img);
  fclose(image_file);
  image_file = 0;
  free(img);
}


static inline void image_plot_with_bounds_checks(struct image *img, Z layer, R zx, R zy)
{
  // flipped along main diagonal
  Z y = floor(SIZE * (zx + 2.0) / 4.0);
  Z x = floor(SIZE * (zy + 2.0) / 4.0);
  if (0 <= x && x < SIZE && 0 <= y && y < SIZE)
  {
    Z k = (y << LOG2SIZE) + x;
    N *p = img->n[layer] + k;
    #pragma omp atomic
    *p += 1;
  }
}


static inline void image_plot_without_bounds_checks(struct image *img, Z layer, R zx, R zy)
{
  // flipped along main diagonal
  Z y = SIZE * (zx + 2.0) / 4.0;
  Z x = SIZE * (zy + 2.0) / 4.0;
  Z k = (y << LOG2SIZE) + x;
  N *p = img->n[layer] + k;
  #pragma omp atomic
  *p += 1;
}


static Z render(struct image *img, Z maxiters, Z grid, C mul, C add)
{
  Z total = 0;
  #pragma omp parallel for schedule(dynamic, 1) reduction(+:total)
  for (Z j = 0; j < grid; ++j)
  {
    Z stotal = 0;
    for (Z i = 0; i < grid; ++i)
    {
        C c = mul * (i + I * j) + add;
        R cx = creal(c);
        R cy = cimag(c);
        Z mi = floor(SIZE * (cy + 2.0) / 4.0);
        Z mj = floor(SIZE * (cx + 2.0) / 4.0);
        if (0 <= mi && mi < SIZE && 0 <= mj && mj < SIZE)
          if (mask[mi][mj] != 255)
            continue;
        R zx = 0;
        R zy = 0;
        R zx2 = zx * zx;
        R zy2 = zy * zy;
        R zxy = fabs(zx * zy);
        Z count;
        for (count = 1; count < maxiters; ++count)
        {
          zx = zx2 - zy2 + cx;
          zy = zxy + zxy + cy;
          zx2 = zx * zx;
          zy2 = zy * zy;
          zxy = fabs(zx * zy);
          if (unlikely(zx2 + zy2 > 4))
            break;
        }
        if (count < maxiters)
        {
          zx = 0;
          zy = 0;
          cx = creal(c);
          cy = cimag(c);
          Z layer = ilog2(count - 1);
          if (! (0 <= layer && layer < LAYERS))
            continue;
          zx2 = zx * zx;
          zy2 = zy * zy;
          zxy = fabs(zx * zy);
          Z n;
          for (n = 1; n < count; ++n)
          {
            zx = zx2 - zy2 + cx;
            zy = zxy + zxy + cy;
            zx2 = zx * zx;
            zy2 = zy * zy;
            zxy = fabs(zx * zy);
            image_plot_without_bounds_checks(img, layer, zx, zy);
          }
          while (zx2 + zy2 < 16.0 && n++ < count + 16)
          {
            zx = zx2 - zy2 + cx;
            zy = zxy + zxy + cy;
            zx2 = zx * zx;
            zy2 = zy * zy;
            zxy = fabs(zx * zy);
            image_plot_with_bounds_checks(img, layer, zx, zy);
          }
          stotal += count;
        }
    }
    total = total + stotal;
  }
  return total;
}


static volatile bool running = true;


static void handler(int sig)
{
  (void) sig;
  running = false;
}


extern int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  Z seed = time(0);
  fprintf(stdout, "starting with seed %016lx\n", seed);
  fflush(stdout);
  srand(seed);
  Z depth = LAYERS;
  Z maxiters = 1LL << depth;
  Z pixels = 0;
  Z total = 0;
  struct image *img = image_map();
  time_t last = time(0);
  signal(SIGINT, handler);
  signal(SIGTERM, handler);
  while (running)
  {
    // randomize grid
    Z n = exp2(10 + (rand() / (R) RAND_MAX));
    n += (n & 1); // ensure even
    R l = 2.0 / (n/2 - 1) + (2.0/(n/2-3) - 2.0/(n/2-1)) * (rand() / (R) RAND_MAX);
    l *= sqrt(2);
    R x = (rand() / (R) RAND_MAX);
    R y = (rand() / (R) RAND_MAX);
    R t = 2 * 3.141592653589793 * (rand() / (R) RAND_MAX);
    C mul = l * cexp(I * t);
    C add = - mul * ((n/2 + x) + I * ((n/2) + y));
    // calculate
    total += render(img, maxiters, n, mul, add);
    pixels += n * n;
    // output
    fprintf(stdout, "%18ld / %18ld = %.18f\n", total, pixels, total / (double) pixels);
    fflush(stdout);
    time_t now = time(0);
    if (now - last >= 60) // 1hr
    {
      fprintf(stdout, "syncing...\n");
      fflush(stdout);
      image_sync(img);
      last = time(0);
      fprintf(stdout, "...synced\n");
      fflush(stdout);
    }
  }
  fprintf(stdout, "exiting\n");
  fflush(stdout);
  image_unmap(img);
  return 0;
}


// scalar 1476008714
//        1477998632
// END
