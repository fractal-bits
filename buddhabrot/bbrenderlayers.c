// gcc -std=c99 -Wall -pedantic -Wextra -O3 -fopenmp -o bbrenderlayers bbrenderlayers.c `PKG_CONFIG_PATH=${HOME}/opt/lib/pkgconfig pkg-config --cflags --libs mandelbrot-numerics` -lm
// dd if=/dev/zero of=bb.map bs=$((1024 * 1024)) count=1024
// ./bbrenderlayers

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <mandelbrot-numerics.h>


typedef unsigned char B;
typedef float F;
typedef uint64_t N;
typedef int64_t Z;
typedef double R;
typedef double _Complex C;


#define unlikely(x) __builtin_expect(x, 0)


Z ilog2(Z n)
{
  Z l = 0;
  while (n > 0)
  {
    n >>= 1;
    l += 1;
  }
  return l;
}


struct partial {
  C z;
  Z p;
};


struct compute {
  Z tag, bias;
  struct partial *partials;
  Z npartials, np, n;
  R er2, mz2;
  C c, z;
};


static struct compute *compute_new(Z npartials) {
  struct compute *px = malloc(sizeof(*px));
  if (! px) {
    return 0;
  }
  if (npartials > 0) {
    px->partials = malloc(npartials * sizeof(*(px->partials)));
    px->npartials = npartials;
    if (! px->partials) {
      free(px);
      return 0;
    }
  } else {
    px->partials = 0;
    px->npartials = 0;
  }
  px->tag = 0;
  return px;
}


static void compute_delete(struct compute *px) {
  if (px) {
    if (px->partials) {
      free(px->partials);
    }
    free(px);
  }
}


static void compute_init(struct compute *px, Z bias, C c) {
  if (! px) { return; }
  px->tag = 0;
  px->bias = bias;
  px->mz2 = 1.0 / 0.0;
  px->c = c;
  px->z = 0;
  px->n = 0;
  px->np = 0;
}


static bool is_interior(C c, C z, Z p, Z steps)
{
  C z00 = 0;
  if (m_failed != m_d_attractor(&z00, z, c, p, steps)) {
    C z0 = z00;
    C dz0 = 1;
    for (Z j = 0; j < p; ++j)
    {
      dz0 = 2.0 * z0 * dz0;
      z0 = z0 * z0 + c;
    }
    return creal(dz0)*creal(dz0) + cimag(dz0)*cimag(dz0) <= 1;
  }
  return false;
}


static bool compute_step(struct compute *px, Z steps)
{
  static const double er2 = 4;
  if (! px) {
    return false;
  }
  if (px->tag != 0) {
    return true;
  }
  C c = px->c;
  C z = px->z;
  R mz2 = px->mz2;
  Z n = px->n;
  R cx = creal(c);
  R cy = cimag(c);
  R zx = creal(z);
  R zy = cimag(z);
  R zx2 = zx * zx;
  R zy2 = zy * zy;
  R zxy = zx * zy;
  if (px->bias < 0)
  {
    for (Z i = 1; i <= steps; ++i)
    {
      zx = zx2 - zy2 + cx;
      zy = zxy + zxy + cy;
      zx2 = zx * zx;
      zy2 = zy * zy;
      zxy = zx * zy;
      R z2 = zx2 + zy2;
      if (unlikely(z2 < mz2))
      {
        mz2 = z2;
        if (is_interior(c, zx + I * zy, n + i, 16))
        {
          px->tag = -1;
          return true;
        }
      }
      if (unlikely(z2 >= er2))
      {
        px->tag = 1;
        px->n = n + i;
        return true;
      }
    }
    px->tag = 0;
    px->z = zx + I * zy;
    px->mz2 = mz2;
    px->n = n + steps;
    return false;
  }
  else
  {
    for (Z i = 1; i <= steps; ++i)
    {
      zx = zx2 - zy2 + cx;
      zy = zxy + zxy + cy;
      zx2 = zx * zx;
      zy2 = zy * zy;
      zxy = zx * zy;
      R z2 = zx2 + zy2;
      if (unlikely(z2 < mz2))
      {
        mz2 = z2;
        if (px->partials && px->np < px->npartials)
        {
          px->partials[px->np].z = z;
          px->partials[px->np].p = n + i;
          px->np = px->np + 1;
        }
      }
      if (unlikely(z2 >= er2))
      {
        px->tag = 1;
        px->n = n + i;
        return true;
      }
    }
    for (Z i = 0; i < px->np; ++i)
    {
      z = px->partials[i].z;
      Z p = px->partials[i].p;
      if (is_interior(c, z, p, 16))
      {
        px->tag = -1;
        px->z = z;
        px->n = p;
        return true;
      }
    }
    px->tag = 0;
    px->z = zx + I * zy;
    px->mz2 = mz2;
    px->n = n + steps;
    return false;
  }
  return false;
}


#define LOG2SIZE 12
#define SIZE (1 << LOG2SIZE)
#define LAYERS 30


struct image
{
  N n[LAYERS][SIZE * SIZE];
};


FILE *image_file = 0;


static struct image *image_map()
{
  image_file = fopen("bb.map", "r+b");
  if (! image_file)
  {
    exit(1);
  }
  struct image *img = malloc(sizeof(struct image));
  if (! img)
  {
    exit(1);
  }
  fread(img, sizeof(struct image), 1, image_file);
  return img;
}


static void image_sync(struct image *img)
{
  rewind(image_file);
  fwrite(img, sizeof(struct image), 1, image_file);
}


static void image_unmap(struct image *img)
{
  image_sync(img);
  fclose(image_file);
  image_file = 0;
  free(img);
}


static inline void image_plot_with_bounds_checks(struct image *img, Z layer, R zx, R zy)
{
  // flipped along main diagonal
  Z y = floor(SIZE * (zx + 2.0) / 4.0);
  Z x = floor(SIZE * (zy + 2.0) / 4.0);
  if (0 <= x && x < SIZE && 0 <= y && y < SIZE)
  {
    Z k = (y << LOG2SIZE) + x;
    N *p = img->n[layer] + k;
    #pragma omp atomic
    *p += 1;
  }
}


static inline void image_plot_without_bounds_checks(struct image *img, Z layer, R zx, R zy)
{
  // flipped along main diagonal
  Z y = SIZE * (zx + 2.0) / 4.0;
  Z x = SIZE * (zy + 2.0) / 4.0;
  Z k = (y << LOG2SIZE) + x;
  N *p = img->n[layer] + k;
  #pragma omp atomic
  *p += 1;
}


static Z render(struct image *img, Z maxiters, Z grid, C mul, C add)
{
  Z maxpartials = 65536;
  Z total = 0;
  #pragma omp parallel for schedule(dynamic, 1) reduction(+:total)
  for (Z j = 0; j < grid; ++j)
  {
    struct compute *px = compute_new(maxpartials);
    Z stotal = 0;
    for (Z i = 0; i < grid; ++i)
    {
      C c = mul * (i + I * j) + add;
      compute_init(px, px->tag, c);
      for (Z iters = 256; iters < maxiters; iters <<= 1)
      {
        compute_step(px, iters);
        if (px->tag > 0)
        {
          R zx = 0;
          R zy = 0;
          R cx = creal(c);
          R cy = cimag(c);
          Z count = px->n - 1;
          Z layer = ilog2(count);
          if (! (0 <= layer && layer < LAYERS))
            continue;
          R zx2 = zx * zx;
          R zy2 = zy * zy;
          R zxy = zx * zy;
          for (Z n = 0; n < count; ++n)
          {
            zx = zx2 - zy2 + cx;
            zy = zxy + zxy + cy;
            zx2 = zx * zx;
            zy2 = zy * zy;
            zxy = zx * zy;
            image_plot_without_bounds_checks(img, layer, zx, zy);
          }
          while (zx2 + zy2 < 16.0)
          {
            zx = zx2 - zy2 + cx;
            zy = zxy + zxy + cy;
            zx2 = zx * zx;
            zy2 = zy * zy;
            zxy = zx * zy;
            image_plot_with_bounds_checks(img, layer, zx, zy);
          }
          stotal += count;
        }
        if (px->tag < 0)
          break;
      }
    }
    compute_delete(px);
    total = total + stotal;
  }
  return total;
}


static volatile bool running = true;


static void handler(int sig)
{
  (void) sig;
  running = false;
}


extern int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  Z seed = time(0);
  fprintf(stdout, "starting with seed %016lx\n", seed);
  fflush(stdout);
  srand(seed);
  Z depth = 30;
  Z maxiters = 1LL << depth;
  Z pixels = 0;
  Z total = 0;
  struct image *img = image_map();
  time_t last = time(0);
  signal(SIGINT, handler);
  signal(SIGTERM, handler);
  while (running)
  {
    // randomize grid
    Z n = exp2(14 + (rand() / (R) RAND_MAX));
    n += (n & 1); // ensure even
    R l = 2.0 / (n/2 - 1) + (2.0/(n/2-3) - 2.0/(n/2-1)) * (rand() / (R) RAND_MAX);
    l *= sqrt(2);
    R x = (rand() / (R) RAND_MAX);
    R y = (rand() / (R) RAND_MAX);
    R t = 2 * 3.141592653589793 * (rand() / (R) RAND_MAX);
    C mul = l * cexp(I * t);
    C add = - mul * ((n/2 + x) + I * ((n/2) + y));
    // calculate
    total += render(img, maxiters, n, mul, add);
    pixels += n * n;
    // output
    fprintf(stdout, "%18ld / %18ld = %.18f\n", total, pixels, total / (double) pixels);
    fflush(stdout);
    time_t now = time(0);
    if (now - last >= 60 * 60) // 1hr
    {
      fprintf(stdout, "syncing...\n");
      fflush(stdout);
      image_sync(img);
      last = time(0);
      fprintf(stdout, "...synced\n");
      fflush(stdout);
    }
  }
  fprintf(stdout, "exiting\n");
  fflush(stdout);
  image_unmap(img);
  return 0;
}


// END
