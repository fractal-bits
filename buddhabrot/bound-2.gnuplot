set terminal pngcairo enhanced font "LMSans10,18" size 1920,1080
set output 'bound-2d.png'
set title 'Is the Buddhabrot well-defined?  More data needed...'
set xlabel 'Grid size (pixels per edge)'
set ylabel 'Sum (n A_n) (iteration count times area)'
set key bottom right
set grid
set log x 2
set xtics 2
set fit errorvariables
fit A+B*log(x) 'bound-2.dat' u 3:1:(1./$3) via A,B
fit C+D/log(x) 'bound-2.dat' u 3:1:(1./$3) via C,D
plot [256:131072][40:100]\
  'x.dat' u 1:((A-A_err)+(B-B_err)*log($1)):((A+A_err)+(B+B_err)*log($1)) w filledcurves lc rgb '#80ff0000' t 'fit error bounds', \
  'x.dat' u 1:((C-C_err)+(D-D_err)/log($1)):((C+C_err)+(D+D_err)/log($1)) w filledcurves lc rgb '#8000ff00' t 'fit error bounds', \
  'bound-2.dat' u 3:1 w p lt 6 lc -1 t 'computed grids', \
  A+B*log(x) w l lc rgb '#ff0000' t 'fit (A + B * log x)', \
  C+D/log(x) w l lc rgb '#00ff00' t 'fit (C + D / log x)', \
  C w l lc rgb '#408040' t 'fit asymptote (C)'
