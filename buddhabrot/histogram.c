#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  int w = 0;
  int h = 0;
  int n = scanf("P5\n%d %d\n255", &w, &h);
  int c = fgetc(stdin);
  if (n == 2 && c == '\n' && w > 0 && h > 0)
  {
    unsigned char *pgm = malloc(w * h);
    fread(pgm, w * h, 1, stdin);
    int *hist = calloc(1, 256 * sizeof(*hist));
    for (int k = 0; k < w * h; ++k)
      hist[pgm[k]]++;
    unsigned char *out = malloc(128 *  256);
    memset(out, 255, 128 * 256);
    double scale = 128 / log(1 + w * h);
    for (int i = 0; i < 256; ++i)
    {
      int y = log(1 + hist[i]) * scale;
      for (int j = 0; j < y && j < 128; ++j)
        out[(127 - j) * 256 + i] = 0;
    }
    printf("P5\n%d %d\n255\n", 256, 128);
    fwrite(out, 256 * 128, 1, stdout);
  }
  return 0;
}
