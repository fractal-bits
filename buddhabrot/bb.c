// gcc -std=c99 -Wall -pedantic -Wextra -O3 -fopenmp -lm -o bb bb.c
// ./bb | pnmsplit - %d.pgm

#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char B;
typedef unsigned int N;
typedef int Z;
typedef double R;
typedef double _Complex C;


#define S 1024
R accum[S][S];
B img[S][S];


static inline C to_screen(C x)
{
  return S * (0.2 * x + (0.5 + I * 0.5));
}


static inline C from_screen(C x)
{
  return (x / S - (0.5 + I * 0.5)) * 5.0;
}


static inline R cabs2(C z) { return creal(z) * creal(z) + cimag(z) * cimag(z); }


static void clear()
{
  #pragma omp parallel for
  for (Z y = 0; y < S; ++y)
    for (Z x = 0; x < S; ++x)
      accum[y][x] = 0;
}


static inline void plot(Z x, Z y, R f)
{
  if (! (isnan(f) || isinf(f)))
  {
    #pragma omp atomic update
    accum[y][x] += f;
  }
}


static void post()
{
  R m = 0;
  for (Z y = 0; y < S/2; ++y)
    for (Z x = 0; x < S; ++x)
      m = fmax(m, accum[y][x] + accum[S-1-y][x]);
  fprintf(stderr, "%.18f\n", m);
  m = 255 / m;
  #pragma omp parallel for
  for (Z y = 0; y < S/2; ++y)
    for (Z x = 0; x < S; ++x)
      img[y][x] = m * (accum[y][x] + accum[S-1-y][x]);
  #pragma omp parallel for
  for (Z y = 0; y < S/2; ++y)
    for (Z x = 0; x < S; ++x)
      img[S-1-y][x] = img[y][x];
}


static void save()
{
  fprintf(stdout, "P5\n%d %d\n255\n", S, S);
  fwrite(&img[0][0], S * S, 1, stdout);
  fflush(stdout);
}


static inline R cross(C a, C b)
{
  return creal(a) * cimag(b) - cimag(a) * creal(b);
}


static inline N inside(C a, C b, C c)
{
  return cross(a - b, c - b) < 0;
}


// https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection#Given_two_points_on_each_line
static inline C intersect(C a, C b, C c, C d)
{
  R x1 = creal(a);
  R y1 = cimag(a);
  R x2 = creal(b);
  R y2 = cimag(b);
  R x3 = creal(c);
  R y3 = cimag(c);
  R x4 = creal(d);
  R y4 = cimag(d);
  R x = (x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4);
  R y = (x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4);
  R z = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
  if (z == 0)
    return a;
  return (x + I * y) / z;
}


// https://en.wikipedia.org/wiki/Sutherland%E2%80%93Hodgman_algorithm#Pseudo_code
static R coverage(C a, C b, C c)
{
  C clip_polygon[3][2] = { { a, b }, { b, c }, { c, a } };
  R nan = 0.0 / 0.0;
  C subject[2][16] =
    { { 0, 1, 1 + I, I,     nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan }
    , { nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan, nan }
    };
  N count = 4;
  N source_count = 4;
  N source = 1;
  for (N edge = 0; edge < 3; ++edge)
  {
    source = 1 - source;
    source_count = count;
    count = 0;
    C e0 = clip_polygon[edge][0];
    C e1 = clip_polygon[edge][1];
    C s = 0;
    if (source_count > 0)
      s = subject[source][source_count - 1];
    for (N i = 0; i < source_count; ++i)
    {
      C e = subject[source][i];
      if (inside(e, e0, e1))
      {
        if (! inside(s, e0, e1))
          subject[1-source][count++] = intersect(s, e, e0, e1);
        subject[1-source][count++] = e;
      }
      else if (inside(s, e0, e1))
        subject[1-source][count++] = intersect(s, e, e0, e1);
      s = e;
    }
  }
  source = 1 - source;
  // clipped polgon in subject[source][[0..count)]
  R cover = 0;
  C p = 0.5 + 0.5 * I;
  C s = 0;
  if (count > 0)
    s = subject[source][count - 1];
  for (N i = 0; i < count; ++i)
  {
    C e = subject[source][i];
    cover += cross(e - p, s - p);
    s = e;
  }
  return fabs(0.5 * cover);
}


// http://www.sunshine2k.de/coding/java/TriangleRasterization/TriangleRasterization.html
static void rasterize(C a, C b, C c, R a0)
{
  R r = 16;
  if (cabs2(a) < r || cabs2(b) < r || cabs2(c) < r)
  {
    R a1 = cross(b - a, c - a)*0.5;
    R f = a0 / a1;
    a = to_screen(a);
    b = to_screen(b);
    c = to_screen(c);
    C AB = b - a;
    C AC = c - a;
    R ABC = 1 / cross(AB, AC);
    Z x0 = floor(fmin(fmin(creal(a), creal(b)), creal(c)));
    Z x1 = ceil (fmax(fmax(creal(a), creal(b)), creal(c)));
    Z y0 = floor(fmin(fmin(cimag(a), cimag(b)), cimag(c)));
    Z y1 = ceil (fmax(fmax(cimag(a), cimag(b)), cimag(c)));
    if (x0 == x1 && y0 == y1)
    {
      // small triangle entirely inside pixel
      if (0 <= x0 && x0 < S && 0 <= y0 && y0 < S)
        plot(x0, y0, f * 2.0 / ABC);
    }
    else
    {
      x0 = fmax(x0, 0);
      x1 = fmin(x1, S - 1);
      y0 = fmax(y0, 0);
      y1 = fmin(y1, S - 1);
      for (Z y = y0; y <= y1; ++y)
        for (Z x = x0; x <= x1; ++x)
        {
          Z inside = 1;
          for (Z dy = 0; dy <= 1; ++dy)
            for (Z dx = 0; dx <= 1; ++dx)
            {
              C P = (x + dx) + I * (y + dy) - a;
              R s = cross(P, AC) * ABC;
              R t = cross(AB, P) * ABC;
              Z v = (s >= 0 && t >= 0 && s + t <= 1);
              inside &= v;
            }
          if (inside)
            // pixel entirely inside triangle
            plot(x, y, f);
          else
          {
            C p = x + I * y;
            R g = coverage(a - p, b - p, c - p);
            plot(x, y, f * g);
          }
        }
    }
  }
}


static void plot_iterates(C c0, R dc, N n) {
  C z[4] = { 0, 0, 0, 0 };
  C c[4] = { c0 - dc - I * dc, c0 + dc - I * dc, c0 + dc + I * dc, c0 - dc + I * dc };
  R a0 = dc * dc * 0.5;
  for (N i = 0; i < n; ++i)
  {
    for (N k = 0; k < 4; ++k)
      z[k] = z[k] * z[k] + c[k];
    rasterize(z[0], z[1], z[2], a0);
    rasterize(z[0], z[2], z[3], a0);
  }
}


static int attractor(C *z0, C c, N period) {
  R eps = 1e-12;
  R er2 = 16;
  C z = *z0;
  for (N j = 0; j < 256; ++j) {
    C dz = 1.0;
    for (N k = 0; k < period; ++k) {
      dz = 2.0 * dz * z;
      z = z * z + c;
    }
    R z2 = cabs(z);
    if (! (z2 < er2)) {
      break;
    }
    z = *z0 - (z - *z0) / (dz - 1.0);
    R e = cabs(z - *z0);
    *z0 = z;
    if (e < eps) {
      return 1;
    }
  }
  return 0;
}


static R interior_distance(C *w, C c, N period, R pixel_size) {
  if (attractor(w, c, period)) {
    C z = *w;
    C dz = 1.0;
    C dzdz = 0.0;
    C dc = 0.0;
    C dcdz = 0.0;
    for (N j = 0; j < period; ++j) {
      dcdz = 2.0 * (z * dcdz + dz * dc);
      dc = 2.0 * z * dc + 1.0;
      dzdz = 2.0 * (dz * dz + z * dzdz);
      dz = 2.0 * z * dz;
      z = z * z + c;
    }
    return (1.0 - cabs2(dz)) / (cabs(dcdz + dzdz * dc / (1.0 - dz)) * pixel_size);
  }
  return -1.0;
}


static void render_recursive(C c, R grid_spacing, N maxiters, N depth) {
  R sqrt2 = sqrt(2);
  C z = 0;
  C dz = 0;
  R mz2 = 1.0/0.0;
  for (N i = 1; i < maxiters; ++i)
  {
    dz = 2 * z * dz + 1;
    z = z * z + c;
    R z2 = cabs2(z);
    if (! (z2 < 65536))
    {
      R de = sqrt(z2) * log(z2) / (cabs(dz) * grid_spacing);
      if (de < sqrt2)
      {
        if (depth > 0)
        {
          render_recursive(c + 1 * 0.5 * grid_spacing + I * 0.5 * grid_spacing, 0.5 * grid_spacing, maxiters, depth - 1);
          render_recursive(c - 1 * 0.5 * grid_spacing + I * 0.5 * grid_spacing, 0.5 * grid_spacing, maxiters, depth - 1);
          render_recursive(c - 1 * 0.5 * grid_spacing - I * 0.5 * grid_spacing, 0.5 * grid_spacing, maxiters, depth - 1);
          render_recursive(c + 1 * 0.5 * grid_spacing - I * 0.5 * grid_spacing, 0.5 * grid_spacing, maxiters, depth - 1);
        }
      }
      else
      {
        plot_iterates(c, grid_spacing, i);
      }
      break;
    }
    if (z2 < mz2) {
      mz2 = z2;
      C z1 = z;
      R de = interior_distance(&z1, c, i, grid_spacing);
      if (de > 0) 
      {
        if (de < sqrt2)
        {
          if (depth > 0)
          {
            render_recursive(c + 1 * 0.5 * grid_spacing + I * 0.5 * grid_spacing, 0.5 * grid_spacing, maxiters, depth - 1);
            render_recursive(c - 1 * 0.5 * grid_spacing + I * 0.5 * grid_spacing, 0.5 * grid_spacing, maxiters, depth - 1);
            render_recursive(c - 1 * 0.5 * grid_spacing - I * 0.5 * grid_spacing, 0.5 * grid_spacing, maxiters, depth - 1);
            render_recursive(c + 1 * 0.5 * grid_spacing - I * 0.5 * grid_spacing, 0.5 * grid_spacing, maxiters, depth - 1);
          }
        }
        break;
      }
    }
  }
}


static void render(N depth) {
  clear();
  N maxiters = 1 << (8 + depth);
  N progress = 0;
  N grid = 256;
  R grid_spacing = 5.0 / grid;
  #pragma omp parallel for schedule(dynamic, 1)
  for (N y = 0; y < grid / 2; ++y) {
    for (N x = 0; x < grid; ++x) {
      C c = grid_spacing * ((x + 0.5 - grid/2.0) + I * (y + 0.5 - grid/2.0));
      render_recursive(c, grid_spacing, maxiters, depth);
    }
    #pragma omp critical
    fprintf(stderr, "%8d\r", ++progress);
  }
  post();
  save();
}


extern int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  for (N depth = 0; 1; ++depth)
    render(depth);
  return 0;
}


// END
