// gcc -std=c99 -Wall -pedantic -Wextra -O3 -lm -o tip tip.c
// ./tip | tee tip.dat

#include <complex.h>
#include <math.h>
#include <stdio.h>

typedef unsigned int N;
typedef double R;
typedef double _Complex C;

static inline R cabs2(C z) { return creal(z) * creal(z) + cimag(z) * cimag(z); }

static void render(N depth) {
  const R pi = 3.141592653589793;
  C c = -2 - pow(0.5, depth);
  R a = 0;
  C z = 0;
  C dc = 0;
  printf("%d %.18e %.18e ", depth, creal(c), cimag(c));
  fflush(stdout);
  while (cabs2(z) < 65536)
  {
    a = a + 1;
    dc = 2 * z * dc + 1;
    z = z * z + c;
  }
  R de = 2 * cabs(z) * log(cabs(z)) / cabs(dc);
  printf("%.18e %.18e ", a, de);
  a = pi * de * de * a;
  printf("%.18e\n", a);
  fflush(stdout);
}


extern int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  for (N depth = 0; 1; ++depth)
    render(depth);
  return 0;
}

// END
