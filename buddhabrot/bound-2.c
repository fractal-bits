// gcc -std=c99 -Wall -pedantic -Wextra -O3 -fopenmp -o bound-2 bound-2.c `PKG_CONFIG_PATH=${HOME}/opt/lib/pkgconfig pkg-config --cflags --libs mandelbrot-graphics` -lm
// ./bound-2 | tee bound-2.dat

#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <mandelbrot-graphics.h>

typedef uint64_t N;
typedef int64_t Z;
typedef double R;
typedef double _Complex C;

static R render(N maxiters, N grid, C mul, C add) {
  R s = 0;
  R side = cabs(mul) * grid;
  R area = side * side;
  R count = grid * grid;
  R scale = area / count;
  #pragma omp parallel for schedule(dynamic, 1) reduction(+:s)
  for (N j = 0; j < grid; ++j) {
    R ls = 0;
    m_d_compute *px = m_d_compute_alloc(maxiters);
    for (N i = 0; i < grid; ++i) {
      C c = mul * (i + I * j) + add;
      m_d_compute_init(px, m_d_compute_get_tag(px), 2, c, 0);
      m_d_compute_step(px, maxiters);
      if (m_d_compute_get_tag(px) == m_exterior)
      {
        Z n = m_d_compute_get_n(px) - 1;
        R a = n * scale;
        ls += a;
      }
    }
    m_d_compute_free(px);
    s = s + ls;
  }
  printf("%.18f %d %d %.18f %.18f %.18f %.18f\n", s, (int) maxiters, (int) grid, creal(mul), cimag(mul), creal(add), cimag(add));
  fflush(stdout);
  return s;
}


extern int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  srand(0xbeefcafe);
  printf("# total maxiters grid mulre mulim addre addim\n");
  for (N depth = 8; depth < 17; ++depth)
  {
    N count = 1024 >> (depth - 8);
    for (N i = 0; i < count; ++i)
    {
      N n = exp2(depth + (rand() / (R) RAND_MAX));
      n += (n & 1); // ensure even
      R l = 2.0 / (n/2 - 1) + (2.0/(n/2-3) - 2.0/(n/2-1)) * (rand() / (R) RAND_MAX);
      R x = (rand() / (R) RAND_MAX);
      R y = (rand() / (R) RAND_MAX);
      R t = 2 * 3.141592653589793 * (rand() / (R) RAND_MAX);
/*
      mul * ((n/2 + x) + I * (n/2 + y)) + add = 0;
      abs mul = l
      arg mul = t
      add = - mul(...)
*/
      C mul = l * cexp(I * t);
      C add = - mul * ((n/2 + x) + I * ((n/2) + y));
      render(256 * n, n, mul, add);
    }
  }
  return 0;
}


// END
