#pragma once

#include "subdiv.h"

#define subdiv_quad_list_magic 0x535542444956514Cull

struct subdiv_quad_list_t;
typedef struct subdiv_quad_list_t subdiv_quad_list_t;

subdiv_error_t subdiv_quad_list_create(const char *filename);
subdiv_error_t subdiv_quad_list_map(subdiv_quad_list_t *ptr, size_t size, int threads, subdiv_map_t map, void *data);
subdiv_error_t subdiv_quad_list_subdivide(const char *filename, subdiv_quad_list_t *ptr, size_t size, int threads, subdiv_filter_t filter, void *data);
