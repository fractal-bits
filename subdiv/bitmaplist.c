#include "bitmaplist.h"

typedef struct
{
  uint32_t i;
  uint32_t j;
  uint8_t bitmap[8];
} subdiv_bitmap_list_quad_t;

struct subdiv_bitmap_list_t
{
  uint64_t magic;
  uint64_t level;
  uint64_t count;
  subdiv_bitmap_list_quad_t quads[];
};

subdiv_error_t subdiv_bitmap_list_create(const char *filename)
{
  subdiv_bitmap_list_t *out;
  size_t bytes = sizeof(*out) + sizeof(subdiv_bitmap_list_quad_t);
  void *ptr;
  int fd;
  subdiv_error_t err = subdiv_mmap_write(filename, &fd, &ptr, bytes);
  if (err)
  {
    return err;
  }
  out = ptr;
  out->magic = subdiv_bitmap_list_magic;
  out->level = 3;
  out->count = 1;
  out->quads[0].i = 0;
  out->quads[0].j = 0;
  for (int i = 0; i < 8; ++i)
  {
    out->quads[0].bitmap[i] = 255;
  }
  return subdiv_munmap(fd, ptr, bytes, bytes);
}

subdiv_error_t subdiv_bitmap_list_map(subdiv_bitmap_list_t *ptr, size_t size, int threads, subdiv_map_t map, void *data)
{
  (void) size;
  int level = ptr->level;
  if (threads > 1)
  {
    uint64_t k_shared = 0;
    #pragma omp parallel for schedule(static, 1) num_threads(threads)
    for (int t = 0; t < threads; ++t)
    {
      while (true)
      {
        uint64_t k;
        #pragma omp atomic capture
        k = k_shared++;
        if (k < ptr->count)
        {
          for (int i = 0; i < 8; ++i)
          for (int j = 0; j < 8; ++j)
          {
            if (ptr->quads[k].bitmap[i] & (1 << j))
            {
              map(data, level, ptr->quads[k].i * 8 + i, ptr->quads[k].j * 8 + j);
            }
          }
        }
        else
        {
          break;
        }
      }
    }
  }
  else
  {
    for (uint64_t k = 0; k < ptr->count; ++k)
    {
      for (int i = 0; i < 8; ++i)
      for (int j = 0; j < 8; ++j)
      {
        if (ptr->quads[k].bitmap[i] & (1 << j))
        {
          map(data, level, ptr->quads[k].i * 8 + i, ptr->quads[k].j * 8 + j);
        }
      }
    }
  }
  return subdiv_success;
}

subdiv_error_t subdiv_bitmap_list_subdivide(const char *filename, subdiv_bitmap_list_t *in, size_t isize, int threads, subdiv_filter_t filter, void *data)
{
  (void) isize;
  subdiv_bitmap_list_t *out;
  size_t osize = sizeof(*out) + 4 * in->count * sizeof(subdiv_bitmap_list_quad_t);
  void *optr;
  int ofd;
  subdiv_error_t err = subdiv_mmap_write(filename, &ofd, &optr, osize);
  if (err)
  {
    return err;
  }
  out = optr;
  out->magic = subdiv_bitmap_list_magic;
  int level = out->level = in->level + 1;
  if (threads > 1)
  {
    uint64_t in_count = in->count;
    uint64_t k_shared = 0;
    uint64_t o_shared = 0;
    #pragma omp parallel for schedule(static, 1) num_threads(threads)
    for (int t = 0; t < threads; ++t)
    {
      while (true)
      {
        uint64_t k;
        #pragma omp atomic capture
        k = k_shared++;
        if (k < in_count)
        {
          uint64_t i0 = in->quads[k].i;
          uint64_t j0 = in->quads[k].j;
          for (uint64_t ii = 0; ii < 2; ++ii)
          for (uint64_t jj = 0; jj < 2; ++jj)
          {
            subdiv_bitmap_list_quad_t q;
            q.i = 2 * i0 + ii;
            q.j = 2 * j0 + jj;
            uint8_t any = 0;
            for (int i = 0; i < 8; ++i)
            {
              q.bitmap[i] = 0;
              for (int j = 0; j < 8; ++j)
              {
                if (in->quads[k].bitmap[ii * 4 + i / 2] & (1 << (jj * 4 + j / 2)))
                {
                  q.bitmap[i] |= filter(data, level, 8 * q.i + i, 8 * q.j + j) ? (1 << j) : 0;
                }
              }
              any |= q.bitmap[i];
            }
            if (any)
            {
              uint64_t o;
              #pragma omp atomic capture
              o = o_shared++;
              out->quads[o] = q;
            }
          }
        }
        else
        {
          break;
        }
      }
    }
    out->count = o_shared;
  }
  else
  {
    uint64_t o = 0;
    for (uint64_t k = 0; k < in->count; ++k)
    {
          uint64_t i0 = in->quads[k].i;
          uint64_t j0 = in->quads[k].j;
          for (uint64_t ii = 0; ii < 2; ++ii)
          for (uint64_t jj = 0; jj < 2; ++jj)
          {
            subdiv_bitmap_list_quad_t q;
            q.i = 2 * i0 + ii;
            q.j = 2 * j0 + jj;
            uint8_t any = 0;
            for (int i = 0; i < 8; ++i)
            {
              q.bitmap[i] = 0;
              for (int j = 0; j < 8; ++j)
              {
                if (in->quads[k].bitmap[ii * 4 + i / 2] & (1 << (jj * 4 + j / 2)))
                {
                  q.bitmap[i] |= filter(data, level, 8 * q.i + i, 8 * q.j + j) ? (1 << j) : 0;
                }
              }
              any |= q.bitmap[i];
            }
            if (any)
            {
              out->quads[o++] = q;
            }
          }
    }
    out->count = o;
  }
  size_t osize2 = sizeof(*out) + out->count * sizeof(subdiv_bitmap_list_quad_t);
  return subdiv_munmap(ofd, optr, osize, osize2);
}
