#include <assert.h>
#include <complex.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "subdiv.h"

typedef struct
{
  int level;
  double _Complex origin;
  double size;
  double distance;
  uint64_t iterations;
  uint64_t count;
} mandelbrot_t;

static inline double cnorm(double _Complex z)
{
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

static inline bool attractor(double _Complex *z0, double _Complex c, uint64_t period)
{
  const double eps2 = 1.9721522630525295e-31;
  double _Complex z = *z0;
  for (uint64_t j = 0; j < 256; ++j)
  {
    double _Complex dz = 1;
    for (uint64_t k = 0; k < period; ++k)
    {
      dz = 2 * dz * z;
      z = z * z + c;
    }
    z = *z0 - (z - *z0) / (dz - 1);
    double e = cnorm(z - *z0);
    *z0 = z;
    if (e <= eps2)
    {
      return true;
    }
    if (isnan(e) || isinf(e))
    {
      return false;
    }
  }
  return true;
}

static inline double interior_de(double _Complex z_guess, double _Complex c, uint64_t period)
{
  double _Complex z = z_guess;
  if (attractor(&z, c, period))
  {
    double _Complex dz = 1;
    double _Complex dzdz = 0;
    double _Complex dc = 0;
    double _Complex dcdz = 0;
    for (uint64_t j = 0; j < period; ++j)
    {
      dcdz = 2 * (z * dcdz + dz * dc);
      dzdz = 2 * (z * dzdz + dz * dz);
      dc = 2 * z * dc + 1;
      dz = 2 * z * dz;
      z = z * z + c;
    }
    return (1 - cnorm(dz)) / (cabs(dcdz + dzdz * dc / (1 - dz)));
  }
  return -1;
}

bool mandelbrot_boundary(void *data, int level, uint64_t i, uint64_t j)
{
  mandelbrot_t *m = data;
  assert(m->level == level);
  double _Complex c = m->origin + m->size * (i + I * j);
  if (cimag(c) > 0)
  {
    return false;
  }
  double _Complex z = 0;
  double _Complex dc = 0;
  double mz2 = 1.0 / 0.0;
  uint64_t iterations = m->iterations;
  for (uint64_t i = 1; i <= iterations; ++i)
  {
    dc = 2 * z * dc + 1;
    z = z * z + c;
    double z2 = cnorm(z);
    if (! (z2 < 65536))
    {
      double de = sqrt(z2) * log(z2) / cabs(dc);
      bool keep = ! (de > m->distance);
      if (keep)
      {
        #pragma omp atomic
        m->count++;
      }
      return keep;
    }
    if (z2 < mz2)
    {
      double de = interior_de(z, c, i);
      if (0 <= de)
      {
        bool keep = ! (de > m->distance);
        if (keep)
        {
          #pragma omp atomic
          m->count++;
        }
        return keep;
      }
      mz2 = z2;
    }
  }
  bool keep = true;
  if (keep)
  {
    #pragma omp atomic
    m->count++;
  }
  return keep;
}

volatile bool running = true;

void signal_handler(int sig)
{
  (void) sig;
  running = 0;
}

int main(int argc, char **argv)
{
  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);
  int implementation = 1;
  int threads = 1;
  int level = -1;
  if (argc > 1) implementation = atoi(argv[1]);
  if (argc > 2) threads = atoi(argv[2]);
  if (argc > 3) level = atoi(argv[3]);
  subdiv_error_t err = subdiv_success;
  if (level < 0)
  {
    level = implementation == 2 ? 3 : 00;
    char filename[100];
    snprintf(filename, sizeof(filename), "M.%d.%02d", implementation, level);
    err = subdiv_create(filename, implementation);
  }
  mandelbrot_t m;
  memset(&m, 0, sizeof(m));
  while (err == subdiv_success)
  {
    printf("%d\t%jd\n", m.level, (intmax_t) m.count);
    fflush(stdout);
    if (! running) break;
    char filename_src[100], filename_dst[100];
    snprintf(filename_src, sizeof(filename_src), "M.%d.%02d", implementation, level);
    ++level;
    snprintf(filename_dst, sizeof(filename_dst), "M.%d.%02d", implementation, level);
    m.level = level;
    m.size = 4.0 / (1ull << level);
    m.origin = -2.75 - 2 * I + (1 + I) * 0.5 * m.size;
    m.distance = m.size * 4 * sqrt(2);
    m.iterations = 2.129 / sqrt(m.size / 4);
    m.count = 0;
    err = subdiv_subdivide(filename_dst, filename_src, threads, mandelbrot_boundary, &m);
  }
  if (err != subdiv_success)
  {
    fprintf(stderr, "error %d\n", (int) err);
  }
  return err != subdiv_success;
}
