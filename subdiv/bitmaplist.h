#pragma once

#include "subdiv.h"

#define subdiv_bitmap_list_magic 0x535542444956424Cull

struct subdiv_bitmap_list_t;
typedef struct subdiv_bitmap_list_t subdiv_bitmap_list_t;

subdiv_error_t subdiv_bitmap_list_create(const char *filename);
subdiv_error_t subdiv_bitmap_list_map(subdiv_bitmap_list_t *ptr, size_t size, int threads, subdiv_map_t map, void *data);
subdiv_error_t subdiv_bitmap_list_subdivide(const char *filename, subdiv_bitmap_list_t *ptr, size_t size, int threads, subdiv_filter_t filter, void *data);
