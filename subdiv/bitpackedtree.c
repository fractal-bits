#include "bitpackedtree.h"

struct subdiv_bitpacked_tree_t
{
  uint64_t magic;
  uint64_t level;
  uint64_t count;
  uint8_t tree[];
};

subdiv_error_t subdiv_bitpacked_tree_create(const char *filename)
{
  subdiv_bitpacked_tree_t *out;
  size_t bytes = sizeof(*out) + 1;
  void *ptr;
  int fd;
  subdiv_error_t err = subdiv_mmap_write(filename, &fd, &ptr, bytes);
  if (err)
  {
    return err;
  }
  out = ptr;
  out->magic = subdiv_bitpacked_tree_magic;
  out->level = 0;
  out->count = 1;
  out->tree[0] = 1;
  return subdiv_munmap(fd, ptr, bytes, bytes);
}

void subdiv_bitpacked_tree_map_rec(uint8_t *tree, int depth, int level, uint64_t i, uint64_t j, uint64_t *cursor, subdiv_map_t map, void *data)
{
  int bit = tree[*cursor / 8] & (1 << (*cursor % 8));
  (*cursor) += 1;
  if (level == depth)
  {
    if (bit)
    {
      map(data, level, i, j);
    }
  }
  else
  {
    if (bit)
    {
      for (uint64_t ii = 0; ii < 2; ++ii)
      for (uint64_t jj = 0; jj < 2; ++jj)
        subdiv_bitpacked_tree_map_rec(tree, depth, level + 1, 2 * i + ii, 2 * j + jj, cursor, map, data);
    }
  }
}

bool subdiv_bitpacked_tree_subdivide_rec(uint8_t *otree, uint8_t *itree, int depth, int level, uint64_t i, uint64_t j, uint64_t *icursor, uint64_t *ocursor, subdiv_filter_t filter, void *data)
{
  int bit = itree[*icursor / 8] & (1 << (*icursor % 8));
  (*icursor) += 1;
#define emit(b) \
  do \
  { \
    if (b) otree[*ocursor / 8] |= (1 << (*ocursor % 8)); \
    else otree[*ocursor / 8] &= ~(1 << (*ocursor % 8)); \
    (*ocursor) += 1; \
  } while(0)
  if (level == depth)
  {
    if (bit)
    {
      bool o[2][2];
      bool any = false;
      for (uint64_t ii = 0; ii < 2; ++ii)
      for (uint64_t jj = 0; jj < 2; ++jj)
        any |= o[ii][jj] = filter(data, level + 1, 2 * i + ii, 2 * j + jj);
      emit(any);
      if (any)
      {
        emit(o[0][0]);
        emit(o[0][1]);
        emit(o[1][0]);
        emit(o[1][1]);
      }
      return any;
    }
    else
    {
      emit(false);
      return false;
    }
  }
  else
  {
    if (bit)
    {
      uint64_t saved = *ocursor;
      emit(true);
      bool any = false;
      for (uint64_t ii = 0; ii < 2; ++ii)
      for (uint64_t jj = 0; jj < 2; ++jj)
        any |= subdiv_bitpacked_tree_subdivide_rec(otree, itree, depth, level + 1, 2 * i + ii, 2 * j + jj, icursor, ocursor, filter, data);
      if (any)
      {
        return true;
      }
      else
      {
        *ocursor = saved;
        emit(false);
        return false;
      }
    }
    else
    {
      emit(false);
      return false;
    }
  }
#undef emit
}

subdiv_error_t subdiv_bitpacked_tree_map(subdiv_bitpacked_tree_t *ptr, size_t size, int threads, subdiv_map_t map, void *data)
{
  (void) size;
  (void) threads;
  uint64_t cursor = 0;
  subdiv_bitpacked_tree_map_rec(&ptr->tree[0], ptr->level, 0, 0, 0, &cursor, map, data);
  return subdiv_success;
}

subdiv_error_t subdiv_bitpacked_tree_subdivide(const char *filename, subdiv_bitpacked_tree_t *in, size_t isize, int threads, subdiv_filter_t filter, void *data)
{
  (void) isize;
  (void) threads;
  subdiv_bitpacked_tree_t *out;
  size_t osize = sizeof(*out) + (5 * in->count + 7) / 8;
  void *optr;
  int ofd;
  subdiv_error_t err = subdiv_mmap_write(filename, &ofd, &optr, osize);
  if (err)
  {
    return err;
  }
  out = optr;
  out->magic = subdiv_bitpacked_tree_magic;
  out->level = in->level + 1;
  uint64_t icursor = 0;
  uint64_t ocursor = 0;
  subdiv_bitpacked_tree_subdivide_rec(&out->tree[0], &in->tree[0], in->level, 0, 0, 0, &icursor, &ocursor, filter, data);
  out->count = ocursor;
  size_t osize2 = sizeof(*out) + (out->count + 7) / 8;
  return subdiv_munmap(ofd, optr, osize, osize2);
}
