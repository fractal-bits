#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

struct subdiv_t;
typedef struct subdiv_t subdiv_t;

typedef enum
{
  subdiv_quad_list = 1,
  subdiv_bitmap_list = 2,
  subdiv_bitpacked_tree = 3
} subdiv_implementation_t;

typedef enum
{
  subdiv_success = 0,
  subdiv_error_invalid = 1,
  subdiv_error_read_open = 2,
  subdiv_error_read_mmap = 3,
  subdiv_error_read_parse = 4,
  subdiv_error_read_stat = 5,
  subdiv_error_msync= 6,
  subdiv_error_munmap = 7,
  subdiv_error_close = 8,
  subdiv_error_write_open = 9,
  subdiv_error_write_truncate = 10,
  subdiv_error_write_mmap = 11,
  subdiv_error_write_msync = 12
} subdiv_error_t;

typedef void (*subdiv_map_t)(void *data, int level, uint64_t i, uint64_t j);
typedef bool (*subdiv_filter_t)(void *data, int level, uint64_t i, uint64_t j);

subdiv_error_t subdiv_create(const char *filename_dst, subdiv_implementation_t implementation);
subdiv_error_t subdiv_map(const char *filename_src, int threads, subdiv_map_t map, void *data);
subdiv_error_t subdiv_subdivide(const char *filename_dst, const char *filename_src, int threads, subdiv_filter_t filter, void *data);

subdiv_error_t subdiv_mmap_read(const char *filename, int *fd, void **ptr, size_t *size);
subdiv_error_t subdiv_mmap_write(const char *filename, int *fd, void **ptr, size_t size);
subdiv_error_t subdiv_munmap(int fd, void *ptr, size_t size, size_t size2);
