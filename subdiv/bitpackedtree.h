#pragma once

#include "subdiv.h"

#define subdiv_bitpacked_tree_magic 0x5355424449565154ull

struct subdiv_bitpacked_tree_t;
typedef struct subdiv_bitpacked_tree_t subdiv_bitpacked_tree_t;

subdiv_error_t subdiv_bitpacked_tree_create(const char *filename);
subdiv_error_t subdiv_bitpacked_tree_map(subdiv_bitpacked_tree_t *ptr, size_t size, int threads, subdiv_map_t map, void *data);
subdiv_error_t subdiv_bitpacked_tree_subdivide(const char *filename, subdiv_bitpacked_tree_t *ptr, size_t size, int threads, subdiv_filter_t filter, void *data);
