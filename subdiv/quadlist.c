#include "quadlist.h"

typedef struct
{
  uint32_t i;
  uint32_t j;
} subdiv_quad_list_quad_t;

struct subdiv_quad_list_t
{
  uint64_t magic;
  uint64_t level;
  uint64_t count;
  subdiv_quad_list_quad_t quads[];
};

subdiv_error_t subdiv_quad_list_create(const char *filename)
{
  subdiv_quad_list_t *out;
  size_t bytes = sizeof(*out) + sizeof(subdiv_quad_list_quad_t);
  void *ptr;
  int fd;
  subdiv_error_t err = subdiv_mmap_write(filename, &fd, &ptr, bytes);
  if (err)
  {
    return err;
  }
  out = ptr;
  out->magic = subdiv_quad_list_magic;
  out->level = 0;
  out->count = 1;
  out->quads[0].i = 0;
  out->quads[0].j = 0;
  return subdiv_munmap(fd, ptr, bytes, bytes);
}

subdiv_error_t subdiv_quad_list_map(subdiv_quad_list_t *ptr, size_t size, int threads, subdiv_map_t map, void *data)
{
  (void) size;
  int level = ptr->level;
  if (threads > 1)
  {
    uint64_t k_shared = 0;
    #pragma omp parallel for schedule(static, 1) num_threads(threads)
    for (int t = 0; t < threads; ++t)
    {
      while (true)
      {
        uint64_t k;
        #pragma omp atomic capture
        k = k_shared++;
        if (k < ptr->count)
        {
          map(data, level, ptr->quads[k].i, ptr->quads[k].j);
        }
        else
        {
          break;
        }
      }
    }
  }
  else
  {
    for (uint64_t k = 0; k < ptr->count; ++k)
    {
      map(data, level, ptr->quads[k].i, ptr->quads[k].j);
    }
  }
  return subdiv_success;
}

subdiv_error_t subdiv_quad_list_subdivide(const char *filename, subdiv_quad_list_t *in, size_t isize, int threads, subdiv_filter_t filter, void *data)
{
  (void) isize;
  subdiv_quad_list_t *out;
  size_t osize = sizeof(*out) + 4 * in->count * sizeof(subdiv_quad_list_quad_t);
  void *optr;
  int ofd;
  subdiv_error_t err = subdiv_mmap_write(filename, &ofd, &optr, osize);
  if (err)
  {
    return err;
  }
  out = optr;
  out->magic = subdiv_quad_list_magic;
  int level = out->level = in->level + 1;
  if (threads > 1)
  {
    uint64_t in_count = in->count;
    uint64_t k_shared = 0;
    uint64_t o_shared = 0;
    #pragma omp parallel for schedule(static, 1) num_threads(threads)
    for (int t = 0; t < threads; ++t)
    {
      while (true)
      {
        uint64_t k;
        #pragma omp atomic capture
        k = k_shared++;
        if (k < in_count)
        {
          uint64_t i = in->quads[k].i;
          uint64_t j = in->quads[k].j;
          for (uint64_t ii = 2 * i; ii < 2 * i + 2; ++ii)
          for (uint64_t jj = 2 * j; jj < 2 * j + 2; ++jj)
          {
            if (filter(data, level, ii, jj))
            {
              uint64_t o;
              #pragma omp atomic capture
              o = o_shared++;
              out->quads[o].i = ii;
              out->quads[o].j = jj;
            }
          }
        }
        else
        {
          break;
        }
      }
    }
    out->count = o_shared;
  }
  else
  {
    uint64_t o = 0;
    for (uint64_t k = 0; k < in->count; ++k)
    {
      uint64_t i = in->quads[k].i;
      uint64_t j = in->quads[k].j;
      for (uint64_t ii = 2 * i; ii < 2 * i + 2; ++ii)
      for (uint64_t jj = 2 * j; jj < 2 * j + 2; ++jj)
      {
        if (filter(data, level, ii, jj))
        {
          out->quads[o].i = ii;
          out->quads[o].j = jj;
          ++o;
        }
      }
    }
    out->count = o;
  }
  size_t osize2 = sizeof(*out) + out->count * sizeof(subdiv_quad_list_quad_t);
  return subdiv_munmap(ofd, optr, osize, osize2);
}
