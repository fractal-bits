#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "subdiv.h"

#include "quadlist.h"
#include "bitmaplist.h"
#include "bitpackedtree.h"

subdiv_error_t subdiv_mmap_read(const char *filename, int *fd, void **ptr, size_t *size)
{
  *fd = open(filename, O_RDONLY);
  if (*fd == -1)
  {
    return subdiv_error_read_open;
  }
  struct stat statbuf;
  int err = fstat(*fd, &statbuf);
  if (err == -1)
  {
    close(*fd);
    return subdiv_error_read_stat;
  }
  *size = statbuf.st_size;
  *ptr = mmap(0, *size, PROT_READ, MAP_SHARED, *fd, 0);
  if (*ptr == MAP_FAILED)
  {
    close(*fd);
    return subdiv_error_read_mmap;
  }
  return subdiv_success;
}

subdiv_error_t subdiv_mmap_write(const char *filename, int *fd, void **ptr, size_t size)
{
  *fd = open(filename, O_RDWR | O_CREAT | O_EXCL, 0644);
  if (*fd == -1)
  {
    return subdiv_error_write_open;
  }
  int err = ftruncate(*fd, size);
  if (err == -1)
  {
    close(*fd);
    return subdiv_error_write_truncate;
  }
  *ptr = mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, *fd, 0);
  if (*ptr == MAP_FAILED)
  {
    close(*fd);
    return subdiv_error_write_mmap;
  }
  return subdiv_success;
}

subdiv_error_t subdiv_munmap(int fd, void *ptr, size_t size, size_t size2)
{
  subdiv_error_t err = subdiv_success;
  if (0 != msync(ptr, size2, MS_SYNC))
  {
    err = subdiv_error_msync;
  }
  if (0 != munmap(ptr, size))
  {
    err = subdiv_error_munmap;
  }
  if (size2 < size)
  {
    if (0 != ftruncate(fd, size2))
    {
      err = subdiv_error_write_truncate;
    }
  }
  if (0 != close(fd))
  {
    err = subdiv_error_close;
  }
  return err;
}

subdiv_error_t subdiv_create(const char *filename_dst, subdiv_implementation_t implementation)
{
  switch (implementation)
  {
    case subdiv_quad_list: return subdiv_quad_list_create(filename_dst);
    case subdiv_bitmap_list: return subdiv_bitmap_list_create(filename_dst);
    case subdiv_bitpacked_tree: return subdiv_bitpacked_tree_create(filename_dst);
    default: return subdiv_error_invalid;
  }
}

subdiv_error_t subdiv_map(const char *filename, int threads, subdiv_map_t map, void *data)
{
  int fd;
  void *ptr;
  size_t size;
  subdiv_error_t err = subdiv_mmap_read(filename, &fd, &ptr, &size);
  if (err)
  {
    return err;
  }
  if (size >= sizeof(uint64_t))
  {
    const uint64_t *u64 = ptr;
    switch (u64[0])
    {
      case subdiv_quad_list_magic:
      {
        err = subdiv_quad_list_map(ptr, size, threads, map, data);
        break;
      }
      case subdiv_bitmap_list_magic:
      {
        err = subdiv_bitmap_list_map(ptr, size, threads, map, data);
        break;
      }
      case subdiv_bitpacked_tree_magic:
      {
        err = subdiv_bitpacked_tree_map(ptr, size, threads, map, data);
        break;
      }
      default:
      {
        err = subdiv_error_read_parse;
      }
    }
  }
  subdiv_munmap(fd, ptr, size, size);
  return err;
}

subdiv_error_t subdiv_subdivide(const char *filename_dst, const char *filename_src, int threads, subdiv_filter_t filter, void *data)
{
  int fd;
  void *ptr;
  size_t size;
  subdiv_error_t err = subdiv_mmap_read(filename_src, &fd, &ptr, &size);
  if (err)
  {
    return err;
  }
  if (size >= sizeof(uint64_t))
  {
    const uint64_t *u64 = ptr;
    switch (u64[0])
    {
      case subdiv_quad_list_magic:
      {
        err = subdiv_quad_list_subdivide(filename_dst, ptr, size, threads, filter, data);
        break;
      }
      case subdiv_bitmap_list_magic:
      {
        err = subdiv_bitmap_list_subdivide(filename_dst, ptr, size, threads, filter, data);
        break;
      }
      case subdiv_bitpacked_tree_magic:
      {
        err = subdiv_bitpacked_tree_subdivide(filename_dst, ptr, size, threads, filter, data);
        break;
      }
      default:
      {
        err = subdiv_error_read_parse;
        break;
      }
    }
  }
  else
  {
    err = subdiv_error_read_parse;
  }
  subdiv_munmap(fd, ptr, size, size);
  return err;
}

