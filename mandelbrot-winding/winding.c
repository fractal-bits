// gcc -std=c99 -Wall -Wextra -pedantic -O3 -fopenmp -march=native -o winding winding.c -lm
// ./winding > winding.pgm

#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define PI 3.141592653589793

static double cnorm(double _Complex z)
{
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

// based on mndlbrot::turn() from Wolf Jung's Mandel 5.14, under GPL license
// available from http://www.mndynamics.com/indexp.html
static double winding(double _Complex c, int maxiters)
{
  double _Complex z = c;
  double theta = carg(z);
  double s = 1;
  const double _Complex uv = csqrt(0.25 - c);
  const double _Complex XY = 0.5 - uv;
  const double a = creal(c);
  const double b = cimag(c);
  const double X = creal(XY);
  const double Y = cimag(XY);
  for (int k = 1; k < maxiters; ++k)
  {
    s *= 0.5;
    z = z * z + c;
    double u = carg(z / (z - c));
    const double x = creal(z);
    const double y = cimag(z);
    if ( (y*a - x*b)*(Y*a - X*b) > 0
      && (y*X - x*Y)*(b*X - a*Y) > 0
      && ((b-y)*(a-X) - (a-x)*(b-Y))*(a*Y - b*X) > 0)
    { if (u < 0) u += 2*PI; else u -= 2*PI; }
    theta += s * u;
    if (cnorm(z) > 1e18 * s) break;
    if (cnorm(z) < 1e-12) return 0.5;
  }
  theta *= 0.5 / PI;
  return theta - floor(theta);
}

extern int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  double _Complex c0 = -0.75;
  double r0 = 2;
  int maxiters = 64;
  int width = 1024;
  int height = 1024;
  int bytes = width * height;
  char *pgm = malloc(bytes);
  #pragma omp parallel for
  for (int j = 0; j < height; ++j)
  {
    double y = ((j + 0.5) / height - 0.5) * 2;
    for (int i = 0; i < width; ++i)
    {
      double x = ((i + 0.5) / width - 0.5) * 2 * width / height;
      double _Complex c = c0 + r0 * (x + I * y);
      double t = winding(c, maxiters);
      pgm[j * width + i] = 256 * t;
    }
  }
  printf("P5\n%d %d\n255\n", width, height);
  fwrite(pgm, bytes, 1, stdout);
  free(pgm);
  return 0;
}
