function argparse(prev, item)
{
  prev[prev.length] = '--' + item.split('=')[0];
  prev[prev.length] = item.split('=')[1];
  return prev;
}
Module['arguments'] = location.search.substr(1).split('&').reduce(argparse, []);
function printappend(str)
{
  var output = document.getElementById('output');
  var content = output.value;
  var lines = content.split('\n').filter(function(s) { return s != ''; });
  if (str.charAt(0) == '\r')
  {
    lines.pop();
    str = str.substr(1);
  }
  str = str.replace('\n', '');
  lines[lines.length] = str;
  output.value = lines.join('\n');
  output.scrollTop = output.scrollHeight;
}
Module['print'] = printappend;
Module['printErr'] = printappend;
