#ifndef __EMSCRIPTEN__
#error this file needs to be compiled by emscripten
#endif

#include <SDL/SDL.h>
#include <emscripten.h>

#include "args.h"

struct main_context
{
  N width;
  N height;
  SDL_Surface *screen;
};
main_context the_main_context;

// callback with results from web worker
void display(char *data, int size, void *arg)
{
  uint8_t *rgb = (uint8_t *) data;
  if (rgb[size - 1] == 255)
  {
    // final response with image data
    main_context *context = (main_context *) arg;
    if (SDL_MUSTLOCK(context->screen)) SDL_LockSurface(context->screen);
    for (N y = 0; y < context->height; ++y)
      for (N x = 0; x < context->width; ++x)
      {
        N k = (y * context->width + x) * 3;
        *((Uint32*)context->screen->pixels + context->width * y + x) =
          SDL_MapRGBA(context->screen->format, rgb[k+0], rgb[k+1], rgb[k+2], 255);
      }
    if (SDL_MUSTLOCK(context->screen)) SDL_UnlockSurface(context->screen);
    SDL_Flip(context->screen);
  }
  else
  {
    // provisional response with logging data
    printf("%s", data);
  }
}

// entry point
extern "C" int main(int argc, char **argv)
{
  // initial defaults
  N width = 640;
  N height = 360;
  N maxiters = 1 << 8;
  N order = 16;
  N stop = 0;
  N accuracy = 24;
  N precision = 0; // auto
  const char *sre = "-0.75";
  const char *sim = "0.0";
  const char *sradius = "1.5";
  const char *filename = "out.ppm";
  // parse arguments
  if (parse(argc, argv, width, height, maxiters, order, stop, accuracy, precision, sre, sim, sradius, filename))
    return 1;
  // create worker thread
  worker_handle worker = emscripten_create_worker("worker.js");
  // prepare video
  SDL_Init(SDL_INIT_VIDEO);
  the_main_context.width = width;
  the_main_context.height = height;
  the_main_context.screen = SDL_SetVideoMode(width, height, 32, SDL_SWSURFACE);
  // render image in web worker background thread
  size_t size;
  char *data = pack(size, width, height, maxiters, order, stop, accuracy, precision, sre, sim, sradius, filename);
  emscripten_call_worker(worker, "render", data, size, display, &the_main_context);
  return 0;
}
