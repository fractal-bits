#ifndef ARGS_H
#define ARGS_H 1

#include <iostream>
#include <cstdlib>
#include <cstring>
using namespace std;

typedef int N;

// packed arguments
struct arguments
{
  N width;
  N height;
  N maxiters;
  N order;
  N stop;
  N accuracy;
  N precision;
};

// pack arguments
inline char *pack(size_t &size, N width, N height, N maxiters, N order, N stop, N accuracy, N precision, const char *sre, const char *sim, const char *sradius, const char *filename)
{
  size = sizeof(arguments) + strlen(sre) + 1 + strlen(sim) + 1 + strlen(sradius) + 1 + strlen(filename) + 1;
  arguments *args = (arguments *) malloc(size);
  args->width = width;
  args->height = height;
  args->maxiters = maxiters;
  args->order = order;
  args->stop = stop;
  args->accuracy = accuracy;
  args->precision = precision;
  char *data = (char *) (args + 1);
  memset(data, 0, size - sizeof(arguments));
  strcat(data, sre);
  data += strlen(data) + 1;
  strcat(data, sim);
  data += strlen(data) + 1;
  strcat(data, sradius);
  data += strlen(data) + 1;
  strcat(data, filename);
  return (char *) args;
}

// unpack arguments
inline void unpack(char *data, size_t size, N &width, N &height, N &maxiters, N &order, N &stop, N &accuracy, N &precision, const char *&sre, const char *&sim, const char *&sradius, const char *&filename)
{
  (void) size;
  arguments *args = (arguments *) data;
  width = args->width;
  height = args->height;
  maxiters = args->maxiters;
  order = args->order;
  stop = args->stop;
  accuracy = args->accuracy;
  precision = args->precision;
  data += sizeof(arguments);
  sre = data;
  data += strlen(data) + 1;
  sim = data;
  data += strlen(data) + 1;
  sradius = data;
  data += strlen(data) + 1;
  filename = data;
}

// parse arguments
inline N parse(int argc, char **argv, N &width, N &height, N &maxiters, N &order, N &stop, N &accuracy, N &precision, const char *&sre, const char *&sim, const char *&sradius, const char *&filename)
{
  // parse arguments
  for (N a = 1; a < argc; ++a)
  {
    if (!strcmp("--width", argv[a]) && a + 1 < argc)
      width = atoi(argv[++a]);
    else
    if (!strcmp("--height", argv[a]) && a + 1 < argc)
      height = atoi(argv[++a]);
    else
    if (!strcmp("--maxiters", argv[a]) && a + 1 < argc)
      maxiters = atoi(argv[++a]);
    else
    if (!strcmp("--order", argv[a]) && a + 1 < argc)
      order = atoi(argv[++a]);
    else
    if (!strcmp("--stopping", argv[a]) && a + 1 < argc)
    {
      N b(++a);
      if (!strcmp("knighty", argv[b]))
        stop = 0;
      else
      if (!strcmp("quaz0r", argv[b]))
        stop = 1;
      else
      if (!strcmp("knighty2", argv[b]))
        stop = 2;
      else
      {
        cerr << argv[0] << ": bad stopping: " << argv[b] << endl;
        return 1;
      }
    }
    else
    if (!strcmp("--accuracy", argv[a]) && a + 1 < argc)
      accuracy = atoi(argv[++a]);
    else
    if (!strcmp("--precision", argv[a]) && a + 1 < argc)
      precision = atoi(argv[++a]);
    else
    if (!strcmp("--re", argv[a]) && a + 1 < argc)
      sre = argv[++a];
    else
    if (!strcmp("--im", argv[a]) && a + 1 < argc)
      sim = argv[++a];
    else
    if (!strcmp("--radius", argv[a]) && a + 1 < argc)
      sradius = argv[++a];
    else
    if (!strcmp("--output", argv[a]) && a + 1 < argc)
      filename = argv[++a];
    else
    {
      cerr << argv[0] << ": unexpected: " << argv[a] << endl;
      cerr
        << "usage:" << endl
        << "  --width n" << endl
        << "  --height n" << endl
        << "  --maxiters n" << endl
        << "  --order n" << endl
        << "  --stopping knighty|quaz0r|knighty2" << endl
        << "  --accuracy n" << endl
        << "  --precision n" << endl
        << "  --re f" << endl
        << "  --im f" << endl
        << "  --radius f" << endl
        << "  --output s.ppm" << endl
        ;
      return 1;
    }
  }
  return 0;
}

#endif
