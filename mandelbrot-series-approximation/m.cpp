// m.cpp 2016,2017 Claude Heiland-Allen
// LICENSE: public domain, cc0, whatever
// this is demonstration code based on algorithms found in fractalforums.com
//
// perturbation technique: mrflay
// * http://www.fractalforums.com/announcements-and-news/superfractalthing-arbitrary-precision-mandelbrot-set-rendering-in-java/msg59980/#msg59980
// * http://www.superfractalthing.co.nf/sft_maths.pdf
// glitch detection method: Pauldelbrot
// * http://www.fractalforums.com/announcements-and-news/pertubation-theory-glitches-improvement/msg73027/#msg73027
// series stopping condition: knighty
// * http://www.fractalforums.com/announcements-and-news/*continued*-superfractalthing-arbitrary-precision-mandelbrot-set-rendering-in-ja/msg95532/#msg95532
// * http://www.fractalforums.com/announcements-and-news/*continued*-superfractalthing-arbitrary-precision-mandelbrot-set-rendering-in-ja/msg95707/#msg95707
// series stopping condition: quaz0r
// * http://www.fractalforums.com/announcements-and-news/*continued*-superfractalthing-arbitrary-precision-mandelbrot-set-rendering-in-ja/msg95838/#msg95838
// * http://www.fractalforums.com/announcements-and-news/*continued*-superfractalthing-arbitrary-precision-mandelbrot-set-rendering-in-ja/msg96245/#msg96245
// series stopping condition: knighty2
// * http://www.fractalforums.com/announcements-and-news/*continued*-superfractalthing-arbitrary-precision-mandelbrot-set-rendering-in-ja/msg96343/#msg96343
// glitch correction new references: claude
// * http://www.fractalforums.com/announcements-and-news/pertubation-theory-glitches-improvement/msg84154/#msg84154
// * http://www.fractalforums.com/mandelbrot-and-julia-set/calculating-new-reference-points-using-information-from-glitch-detection/msg86138/#msg86138
//
// compile:
//  g++ -std=c++11 -O3 -march=native m.cpp -lmpfr -Wall -Wextra -pedantic -fopenmp
// run:
//  ./a.out --help
// view results:
//  display out.ppm

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cstring>

#include <algorithm>
#include <complex>
#include <iostream>
#include <vector>

#include <boost/multiprecision/mpfr.hpp>
#include <mpfr.h>

#ifdef __EMSCRIPTEN__
#include <SDL/SDL.h>
#include <emscripten.h>
#define LOG(x) \
do { \
  ostringstream cout; \
  x << endl; \
  emscripten_worker_respond_provisionally(const_cast<char *>(cout.str().c_str()), strlen(cout.str().c_str()) + 1); \
} while(0)
#else
#define LOG(x) x
#endif

#include "args.h"

using namespace std;
using namespace boost;
using namespace multiprecision;

inline double sign(double x) {
  return (x > 0) - (x < 0);
}

class edouble {
private:
  static const int64_t maxexponent = (1LL << 60) - 2;
  static const int64_t minexponent = 2 - (1LL << 60);
  static const int64_t supexponent = maxexponent - 2000;
  static const int64_t infexponent = minexponent + 2080;
  double x;
  long e;
public:
  inline edouble() : x(0), e(0) { };
  inline edouble(const edouble &that) : x(that.x), e(that.e) { };
  inline edouble(const double &x0, const int64_t &e0) {
    if (x0 == 0 || std::isnan(x0) || std::isinf(x0)) {
      x = x0;
      e = 0;
    } else {
      int tmp(0);
      double x1(std::frexp(x0, &tmp));
      int64_t e1(tmp);
      e1 += e0;
      if (e0 > supexponent || e1 > maxexponent) {
        x = sign(x0) / 0.0;
        e = 0;
      } else if (e0 < infexponent || e1 < minexponent) {
        x = sign(x0) * 0.0;
        e = 0;
      } else {
        x = x1;
        e = e1;
      }
    }
  };
  inline edouble(const long double &x0, const int64_t &e0) {
    if (x0 == 0 || std::isnan(x0) || std::isinf(x0)) {
      x = x0;
      e = 0;
    } else {
      int tmp(0);
      long double x1(frexp(x0, &tmp));
      int64_t e1(tmp);
      e1 += e0;
      if (e0 > supexponent || e1 > maxexponent) {
        x = sign(x0) / 0.0;
        e = 0;
      } else if (e0 < infexponent || e1 < minexponent) {
        x = sign(x0) * 0.0;
        e = 0;
      } else {
        x = x1;
        e = e1;
      }
    }
  };
  inline edouble(const double &x) : edouble(x, 0) { };
  inline edouble(const long double &x) : edouble(x, 0) { };
  inline explicit edouble(const mpfr_t &a) {
    long e(0);
    double x(mpfr_get_d_2exp(&e, a, MPFR_RNDN));
    *this = edouble(x, e);
  }
  inline edouble(const int &x) : edouble(double(x)) { };
  inline ~edouble() { };
  inline int64_t exponent() const { return e; };
  inline void to_mpfr(mpfr_t &m) {
    mpfr_set_d(m, x, MPFR_RNDN);
    mpfr_mul_2si(m, m, e, MPFR_RNDN);
  };
  inline long double to_ld() {
    int tmp(e);
    if (e > int64_t(tmp)) {
      return sign(x) / 0.0;
    }
    if (e < int64_t(tmp)) {
      return sign(x) * 0.0;
    }
    return std::ldexp((long double) x, tmp);
  }
  friend inline bool operator==(const edouble &a, const edouble &b);
  friend inline bool operator!=(const edouble &a, const edouble &b);
  friend inline int compare(const edouble &a, const edouble &b);
  friend inline int sign(const edouble &a);
  friend inline edouble abs(const edouble &a);
  friend inline edouble sqrt(const edouble &a);
  friend inline edouble operator+(const edouble &a, const edouble &b);
  friend inline edouble operator-(const edouble &a);
  friend inline edouble operator-(const edouble &a, const edouble &b);
  friend inline edouble operator*(const edouble &a, const edouble &b);
  friend inline edouble recip(const edouble &a);
  friend inline edouble operator/(const edouble &a, const edouble &b);
  friend inline bool isnan(const edouble &a);
  friend inline bool isinf(const edouble &a);
  friend inline edouble ldexp(const edouble &a, int64_t e);
  friend inline std::ostream& operator<<(std::ostream& o, const edouble& a);
  friend inline std::istream& operator>>(std::istream& i, edouble& a);
  friend inline mpfr_float r_hi(const edouble &z);
  friend inline edouble copysign(const edouble &a, const edouble &b);
  friend inline edouble hypot(const edouble &a, const edouble &b);
  friend inline edouble scalbn(const edouble &a, int b);
  friend inline edouble fabs(const edouble &a);
  friend inline edouble fmax(const edouble &a, const edouble &b);
  friend inline edouble logb(const edouble &a);
  explicit inline operator int() { return this->to_ld(); }
  explicit inline operator double() { return this->to_ld(); }
  inline edouble &operator+=(const edouble &a) {
    *this = *this + a;
    return *this;
  };
  inline edouble &operator-=(const edouble &a) {
    *this = *this - a;
    return *this;
  };
  inline edouble &operator*=(const edouble &a) {
    *this = *this * a;
    return *this;
  };
  inline edouble &operator/=(const edouble &a) {
    *this = *this / a;
    return *this;
  };
};

inline bool operator==(const edouble &a, const edouble &b) {
  return a.e == b.e && a.x == b.x;
}

inline bool operator!=(const edouble &a, const edouble &b) {
  return a.e != b.e || a.x != b.x;
}

inline int compare(const double &a, const double &b) {
  return sign(a - b);
}

inline int compare(const edouble &a, const edouble &b) {
  if (a.x == 0.0) {
    return -sign(b.x);
  }
  if (b.x == 0.0) {
    return sign(a.x);
  }
  int64_t e(std::max(a.e, b.e));
  int64_t da(a.e - e);
  int64_t db(b.e - e);
  int ia(da);
  int ib(db);
  if (int64_t(ia) != da) {
    // a -> 0
    return -sign(b.x);
  }
  if (int64_t(ib) != db) {
    // b -> 0
    return sign(a.x);
  }
  return compare(std::ldexp(a.x, ia), std::ldexp(b.x, ib));
}

inline bool operator<(const edouble &a, const edouble &b) {
  return compare(a, b) < 0;
}

inline bool operator<=(const edouble &a, const edouble &b) {
  return compare(a, b) <= 0;
}

inline bool operator>(const edouble &a, const edouble &b) {
  return compare(a, b) > 0;
}

inline bool operator>=(const edouble &a, const edouble &b) {
  return compare(a, b) >= 0;
}

inline int sign(const edouble &a) {
  return sign(a.x);
}

inline edouble abs(const edouble &a) {
  return { std::abs(a.x), a.e };
}

inline edouble sqrt(const edouble &a) {
  return { std::sqrt((a.e & 1) ? 2.0 * a.x : a.x), (a.e & 1) ? (a.e - 1) / 2 : a.e / 2 };
}

inline edouble operator+(const edouble &a, const edouble &b) {
  if (a.x == 0.0) {
    return b;
  }
  if (b.x == 0.0) {
    return a;
  }
  int64_t e(std::max(a.e, b.e));
  int64_t da(a.e - e);
  int64_t db(b.e - e);
  int ia(da);
  int ib(db);
  if (int64_t(ia) != da) {
    // a -> 0
    return b;
  }
  if (int64_t(ib) != db) {
    // b -> 0
    return a;
  }
  return edouble(std::ldexp(a.x, ia) + std::ldexp(b.x, ib), e);
}

inline edouble operator-(const edouble &a) {
  return { -a.x, a.e };
}

inline edouble operator-(const edouble &a, const edouble &b) {
  if (a.x == 0.0) {
    return -b;
  }
  if (b.x == 0.0) {
    return a;
  }
  int64_t e(std::max(a.e, b.e));
  int64_t da(a.e - e);
  int64_t db(b.e - e);
  int ia(da);
  int ib(db);
  if (int64_t(ia) != da) {
    // a -> 0
    return -b;
  }
  if (int64_t(ib) != db) {
    // b -> 0
    return a;
  }
  return edouble(std::ldexp(a.x, ia) - std::ldexp(b.x, ib), e);
}

inline edouble operator*(const edouble &a, const edouble &b) {
  return edouble(a.x * b.x, a.e + b.e);
}

inline double recip(const double &a) {
  return 1.0 / a;
}

inline edouble recip(const edouble &a) {
  return edouble(recip(a.x), -a.e);
}

inline edouble operator/(const edouble &a, const edouble &b) {
  return edouble(a.x / b.x, a.e - b.e);
}

inline bool isnan(const edouble &a) {
  return std::isnan(a.x);
}

inline bool isinf(const edouble &a) {
  return std::isinf(a.x);
}

inline edouble ldexp(const edouble &a, int64_t e) {
  return edouble(a.x, a.e + e);
}

inline void to_mpfr(float from, mpfr_t &to) {
  mpfr_set_flt(to, from, MPFR_RNDN);
}

inline void to_mpfr(double from, mpfr_t &to) {
  mpfr_set_d(to, from, MPFR_RNDN);
}

inline void to_mpfr(long double from, mpfr_t &to) {
  mpfr_set_ld(to, from, MPFR_RNDN);
}

inline void to_mpfr(edouble from, mpfr_t &to) {
  from.to_mpfr(to);
}

inline long double to_ld(float x) {
  return x;
}

inline long double to_ld(double x) {
  return x;
}

inline long double to_ld(long double x) {
  return x;
}

inline long double to_ld(edouble x) {
  return x.to_ld();
}

template <typename S, typename T>
inline T to_R(S x, const T &dummy) {
  (void) dummy;
  return T(to_ld(x));
}

inline edouble to_R(edouble x, const edouble &dummy) {
  (void) dummy;
  return x;
}

template <typename S, typename T>
inline std::complex<T> to_C(std::complex<S> x, const T &dummy) {
  return std::complex<T>(to_R(std::real(x), dummy), to_R(std::imag(x), dummy));
}

inline std::complex<edouble> to_C(std::complex<edouble> x, const edouble &dummy) {
  (void) dummy;
  return x;
}

inline std::ostream& operator<<(std::ostream& o, const edouble& a) {
  return o << a.x << " " << a.e;
}

inline std::istream& operator>>(std::istream& i, edouble& a) {
  double x;
  long e;
  i >> x >> e;
  a = edouble(x, e);
  return i;
}

inline int64_t exponent(float z)
{
  int e;
  frexp(z, &e);
  return e;
}

inline int64_t exponent(double z)
{
  int e;
  frexp(z, &e);
  return e;
}

inline int64_t exponent(long double z)
{
  int e;
  frexp(z, &e);
  return e;
}

inline int64_t exponent(edouble z)
{
  return z.exponent();
}

inline int64_t exponent(const mpfr_t z)
{
  if (mpfr_regular_p(z))
    return mpfr_get_exp(z);
  return 0;
}

inline bool isfinite(edouble z)
{
  return !(isinf(z) || isnan(z));
}

inline edouble copysign(const edouble &a, const edouble &b)
{
  return edouble(copysign(a.x, b.x), a.e);
}

inline edouble hypot(const edouble &a, const edouble &b)
{
  return sqrt(a * a + b * b);
}

inline edouble scalbn(const edouble &a, int b)
{
  return edouble(a.x, a.e + b);
}

inline edouble fabs(const edouble &a)
{
  return edouble(fabs(a.x), a.e);
}

inline edouble fmax(const edouble &a, const edouble &b)
{
  if (compare(a, b) >= 0)
    return a;
  else
    return b;
}

inline edouble logb(const edouble &a)
{
  if (isnan(a))
    return a;
  if (isinf(a))
    return fabs(a);
  if (a.x == 0.0)
    return edouble(double(edouble::minexponent));
  return logb(a.x) + a.e;
}

// type aliases
typedef int N;
typedef edouble R_lo;
typedef mpfr_float R_hi;
typedef complex<R_lo> C_lo;
typedef complex<R_hi> C_hi;

inline R_lo r_lo(const R_hi &z)
{
  return R_lo(z.backend().data());
}

inline R_lo r_lo(const char *s)
{
  unsigned p = mpfr_float::default_precision();
  mpfr_float::default_precision(20u);
  R_lo r(R_hi(s).backend().data());
  mpfr_float::default_precision(p);
  return r;
}

inline R_hi r_hi(const R_lo &z)
{
  R_hi w;
  to_mpfr(z, w.backend().data());
  return w;
}

inline C_lo c_lo(const C_hi &z)
{
  return C_lo(r_lo(real(z)), r_lo(imag(z)));
}

inline C_hi c_hi(const C_lo &z)
{
  return C_hi(r_hi(real(z)), r_hi(imag(z)));
}

inline bool isfinite(const R_hi &z)
{
  return mpfr_number_p(z.backend().data());
}

// helpers for Jordan curve method

template<typename T>
N sgn(T a)
{
  if (a < T(0))
    return -1;
  if (a > T(0))
    return  1;
  return 0;
}

template<typename R>
inline R cross(const complex<R> &a, const complex<R> &b)
{
  return imag(a) * real(b) - real(a) * imag(b);
}

template<typename R>
bool crosses_positive_real_axis(const complex<R> &a, const complex<R> &b)
{
  if (sgn(imag(a)) != sgn(imag(b)))
  {
    complex<R> d(b - a);
    N s(sgn(imag(d)));
    N t(sgn(cross(d, a)));
    return s == t;
  }
  return false;
}

template<typename R>
bool surrounds_origin(const complex<R> &a, const complex<R> &b, const complex<R> &c, const complex<R> &d)
{
  return 1 == (1 &
    ( crosses_positive_real_axis(a, b)
    + crosses_positive_real_axis(b, c)
    + crosses_positive_real_axis(c, d)
    + crosses_positive_real_axis(d, a)
    ));
}

// Jordan curve method for finding lowest period atom in a region
class boxperiod
{
public:
  N n;
  C_hi c[4];
  C_hi z[4];

  boxperiod(C_hi c0, R_lo r)
  : n(1)
  {
    c[0] = c0 + C_hi(r_hi( r), r_hi( r));
    c[1] = c0 + C_hi(r_hi(-r), r_hi( r));
    c[2] = c0 + C_hi(r_hi(-r), r_hi(-r));
    c[3] = c0 + C_hi(r_hi( r), r_hi(-r));
    z[0] = c[0];
    z[1] = c[1];
    z[2] = c[2];
    z[3] = c[3];
  };

  N period(N maxsteps)
  {
    LOG(cout << "computing period" << endl);
    for (N k = 0; k < maxsteps; ++k)
    {
      if (n % 1000 == 0)
        LOG(cout << "\rreference period " << n);
      if (surrounds_origin(z[0], z[1], z[2], z[3]))
      {
        LOG(cout << "\rreference period " << n << " (complete)" << endl);
        return n;
      }
      bool escaped(false);
      for (N j = 0; j < 4; ++j)
      {
        z[j] = z[j] * z[j] + c[j];
        escaped |= norm(z[j]) > R_hi(1 << 24);
      }
      ++n;
      if (escaped)
        return 0;
    }
    return 0;
  };
};

// find the last partial
N last_partial(C_hi c, N maxiters)
{
  C_hi z(c);
  R_hi zmin(norm(z));
  N partial(1);
  LOG(cout << "computing period (try 2)" << endl);
  for (N k = 2; k < maxiters + 2; ++k)
  {
    if (k % 1000 == 0)
      LOG(cout << "\rreference period (try 2) " << partial << " " << k);
    z = z * z + c;
    R_hi zn(norm(z));
    if (zn > R_hi(4))
      break;
    if (zn < zmin)
    {
      zmin = zn;
      partial = k;
    }
  }
  LOG(cout << "\rreference period (try 2) " << partial << " (complete)" << endl);
  return partial;
}

// Newton's method for nucleus

template<typename R>
bool isfinite(const complex<R> &a)
{
  return isfinite(real(a)) && isfinite(imag(a));
}

C_hi newton_nucleus(C_hi guess, N period, N maxsteps)
{
  C_hi c(guess);
  LOG(cout << "computing nucleus" << endl);
  N k;
  for (k = 0; k < maxsteps; ++k)
  {
    C_hi z(0, 0);
    C_hi dc(1, 0);
    for (N p = 0; p < period; ++p)
    {
      dc = R_hi(2) * dc * z + C_hi(1, 0);
      z = z * z + c;
      if (p % 1000 == 0)
        LOG(cout << "\rreference nucleus " << k << "/" << maxsteps << " " << p << "/" << period);
    }
    C_hi cnext(c - z / dc);
    if (isfinite(cnext))
      c = cnext;
    else
      break;
  }
  LOG(cout << "\rreference nucleus " << k << "/" << maxsteps << " (complete)" << endl);
  return c;
}

// reference orbit for perturbation rendering
class reference
{
public:
  N n0;
  N n;
  C_hi z;
  C_hi c;
  vector<C_lo> z_ref;
  vector<R_lo> z_size;  // scaled absolute value for glitch detection

  // constructor
  reference(N n, C_hi z, C_hi c)
  : n0(n)
  , n(n)
  , z(z)
  , c(c)
  {
    C_lo z_lo(c_lo(z));
    z_ref.push_back(z_lo);
    z_size.push_back(1.0e-6 * norm(z_lo));
  };

  // step a reference one iteration
  // returns false if reference escaped
  bool step()
  {
    n = n + 1;
    z = z * z + c;
    C_lo z_lo(c_lo(z));
    z_ref.push_back(z_lo);
    z_size.push_back(1.0e-6 * norm(z_lo));
    return (norm(z_lo) <= 4);
  };

  // step a reference until it escapes or maxiters reached
  // return false if escaped
  bool run(N maxiters)
  {
    LOG(cout << "computing reference" << endl);
    while (n < maxiters && step())
      if (n % 1000 == 0)
        LOG(cout << "\rreference iteration " << n);
    LOG(cout << "\rreference iteration " << n << " (complete)" << endl);
    return n == maxiters;
  }
};

// reference orbit for perturbation rendering with derivatives for interior de
class reference_interior
{
public:
  N p;
  N n;
  C_hi c;
  C_hi z;
  C_hi dz;
  C_hi dc;
  C_hi dzdz;
  C_hi dcdz;
  vector<R_lo> z_size;  // scaled absolute value for glitch detection
  vector<C_lo> z_ref;
  vector<C_lo> dz_ref;
  vector<C_lo> dc_ref;
  vector<C_lo> dzdz_ref;
  vector<C_lo> dcdz_ref;

  // constructor
  reference_interior(N p, C_hi c) // c must be a cardioid nucleus of period p
  : p(p)
  , n(0)
  , c(c)
  , z(0)
  , dz(1)
  , dc(0)
  , dzdz(0)
  , dcdz(0)
  {
    C_lo z_lo(c_lo(z));
    z_size.push_back(1.0e-6 * norm(z_lo));
    z_ref.push_back(z_lo);
    dz_ref.push_back(c_lo(dz));
    dc_ref.push_back(c_lo(dc));
    dzdz_ref.push_back(c_lo(dzdz));
    dcdz_ref.push_back(c_lo(dcdz));
  };

  // step a reference one iteration
  // returns false if reference escaped
  bool step()
  {
    n = n + 1;
    dcdz = 2 * z * dcdz + 2 * dc * dz;
    dzdz = 2 * z * dzdz + 2 * dz * dz;
    dc = 2 * z * dc + 1;
    dz = 2 * z * dz;
    z = z * z + c;
    C_lo z_lo(c_lo(z));
    z_size.push_back(1.0e-6 * norm(z_lo));
    z_ref.push_back(z_lo);
    dz_ref.push_back(c_lo(dz));
    dc_ref.push_back(c_lo(dc));
    dzdz_ref.push_back(c_lo(dzdz));
    dcdz_ref.push_back(c_lo(dcdz));
    return (norm(z_lo) <= 4);
  };

  // step a reference until it escapes or maxiters reached
  // return false if escaped
  bool run(N maxiters)
  {
    while (n < maxiters && step())
      cerr << "\rreference iteration " << n;
    cerr << endl;
    return n == maxiters;
  }
};

// perturbation technique per-pixel iterations with
// Pauldelbrot's glitch detection heuristic
class perturbation
{
public:
  N x;
  N y;
  N n;
  C_lo dc;
  C_lo dz;
  C_lo dzdc;
  bool glitched;
  bool escaped;

  // constructor initializes
  perturbation(N x, N y, N n, C_lo dc, C_lo dz, C_lo dzdc)
  : x(x)
  , y(y)
  , n(n)
  , dc(dc)
  , dz(dz)
  , dzdc(dzdc)
  , glitched(false)
  , escaped(false)
  { };

  // calculate pixel
  void run(const reference &r, N maxiters)
  {
    while (n < maxiters)
    {
      C_lo z(dz + r.z_ref[n - r.n0]);
      R_lo nz(norm(z));
      if (nz < r.z_size[n - r.n0])
      {
        glitched = true;
        dz = z;
        break;
      }
      if (nz > R_lo(65536))
      {
        escaped = true;
        dz = z;
        break;
      }
      dzdc = R_lo(2) * z * dzdc + C_lo(1);
      dz = R_lo(2) * r.z_ref[n - r.n0] * dz + dz * dz + dc;
      n = n + 1;
    }
  };
};

// series approximation with various stopping conditions
class series_approximation
{
public:
  enum stopping { knighty = 0, quaz0r = 1, knighty2 = 2 };

  N n;             // iteration count
  R_lo dt;         // pixel size
  R_lo tmax;       // view size
  C_hi z;          // reference z at high precision
  C_hi c;          // reference c at high precision
  N m;             // series order
  vector<C_lo> a;  // coefficients (including z at start and R at end)
  vector<C_lo> b;  // derivative coefficients (including dzdc at start)
  stopping stop;   // stopping condition
  N accuracy;      // number of bits to be accurate to (for knighty2)

  // constructor initializes coefficients
  series_approximation(C_hi c, R_lo dt, R_lo tmax, N m, stopping stop, N accuracy)
  : n(1)           // simplifies some things to start at iteration 1 with z = c
  , dt(dt)
  , tmax(tmax)
  , z(c)
  , c(c)
  , m(m)
  , a(m + 2)
  , b(m + 1)
  , stop(stop)
  , accuracy(accuracy)
  {
    assert(dt > 0);
    assert(tmax > 0);
    assert(m >= 1);
    a[0] = c_lo(c);
    a[1] = C_lo(1);
    for (N k = 2; k <= m + 1; ++k)
      a[k] = C_lo(0); // includes truncation error R at end
    b[0] = a[1];
    for (N k = 1; k <= m; ++k)
      b[k] = C_lo(0);
  };

  // advance if series approximation is valid
  bool step()
  {
    // calculate coefficients
    C_hi z_next(z * z + c);
    vector<C_lo> a_next(m + 2);
    a_next[0] = c_lo(z_next);
    a_next[1] = R_lo(2) * a[0] * a[1] + C_lo(1);
    for (N k = 2; k <= m; ++k)
    {
      C_lo sum(0);
      for (N j = 0; j <= (k - 1) / 2; ++j)
        sum += a[j] * a[k - j];
      sum *= R_lo(2);
      if (! (k & 1)) // if k is even
        sum += a[k/2] * a[k/2];
      a_next[k] = sum;
    }
    // calculate derivative coefficients
    vector<C_lo> b_next(m + 1);
    b_next[0] = a_next[1];
    for (N k = 1; k <= m; ++k)
    {
      C_lo sum(0);
      for (N j = 0; j <= k; ++j)
        sum += a[j] * b[k - j];
      b_next[k] = R_lo(2) * sum;
    }
    // calculate truncation error
    {

/*
      // knighty's first estimate
      vector<R_lo> a_abs(m + 2);
      for (N k = 0; k <= m + 1; ++k)
        a_abs[k] = abs(a[k]);
      R_lo sum(0);
      R_lo tmaxn(1);
      for (N j = 0; j <= m + 1; ++j)
      {
        R_lo sum2(0);
        for (N k = j; k <= m + 1; ++k)
          sum2 += a_abs[k] * a_abs[m + 1 + j - k];
        sum += sum2 * tmaxn;
        tmaxn *= tmax;
      }
*/

/*
      // knighty's second estimate
      R_lo sum(0);
      R_lo tmaxn(1);
      for (N k = m + 1; k <= 2 * m; ++k)
      {
        C_lo sum2(0);
        for (N j = k - m; j <= (k - 1) / 2; ++j)
          sum2 += a[j] * a[k - j];
        sum2 *= R_lo(2);
        if (! (k & 1)) // if k is even
          sum2 += a[k / 2] * a[k / 2];
        sum += abs(sum2) * tmaxn;
        tmaxn *= tmax;
      }
      tmaxn = R_lo(1);
      R_lo sum2(0);
      for (N j = 0; j <= m; ++j)
      {
        sum2 += abs(a[j]) * tmaxn;
        tmaxn *= tmax;
      }
      sum += 2 * abs(a[m + 1]) * sum2;
      sum += 2 * abs(a[m + 1]) * abs(a[m + 1]) * tmaxn;
      a_next[m + 1] = C_lo(sum);
*/

      // quaz0r's rewrite of knighty's formula
      R_lo sum(0);
      R_lo tmaxk(1);
      for (N k = 0; k <= m - 1; ++k)
      {
        C_lo sum2(0);
        for (N i = k; i <= (m - 1 + k - 1) / 2; ++i)
          sum2 += a[i + 1] * a[m - 1 + k - i + 1];
        sum2 *= R_lo(2);
        if (! (k & 1)) // if k is even
          sum2 += a[k/2 + 1] * a[k/2 + 1];
        sum += tmaxk * abs(sum2);
        tmaxk *= tmax;
      }
      R_lo sum2(abs(a[0]));
      R_lo tmaxi1(tmax);
      for (N i = 0; i <= m - 1; ++i)
      {
        sum2 += abs(a[i + 1]) * tmaxi1;
        tmaxi1 *= tmax;
      }
      sum2 *= R_lo(2) * abs(a[m + 1]);
      sum += sum2;
      sum += abs(a[m + 1]) * abs(a[m + 1]) * tmaxi1;
      a_next[m + 1] = C_lo(sum);

    }
    // check validity of next
    bool valid(false);
    switch (stop)
    {

    case knighty:
      { // knighty's stopping condition
        vector<R_lo> a_abs(m + 2);
        for (N k = 0; k <= m + 1; ++k)
          a_abs[k] = abs(a_next[k]);
        N max_k(1);
        R_lo max_term(a_abs[1]);
        R_lo tmaxn(1);
        for (N k = 1; k <= m; ++k)
        {
          R_lo term = R_lo(k) * a_abs[k] * tmaxn;
          if (term > max_term)
          {
            max_term = term;
            max_k = k;
          }
          tmaxn *= tmax;
        }
        R_lo rhs(max_term);
        tmaxn = 1;
        for (N k = 1; k <= m; ++k)
        {
          if (k != max_k)
            rhs -= R_lo(k) * a_abs[k] * tmaxn;
          tmaxn *= tmax;
        }
        rhs *= dt;
        R_lo lhs(a_abs[m + 1]);
        for (N k = 0; k < m + 1; ++k)
          lhs *= tmax;
        valid = lhs <= rhs;
      }
      break;

    case quaz0r:
      { // quaz0r's stopping condition
        vector<R_lo> a_abs(m + 2);
        for (N k = 0; k <= m + 1; ++k)
          a_abs[k] = abs(a_next[k]);
        R_lo zsa(0);
        R_lo tmaxn(1);
        for (N k = 0; k <= m; ++k)
        {
          zsa += a_abs[k] * tmaxn;
          tmaxn *= tmax;
        }
        R_lo dsa(0);
        tmaxn = 1;
        for (N k = 1; k <= m; ++k)
        {
          dsa += R_lo(k) * a_abs[k] * tmaxn;
          tmaxn *= tmax;
        }
        R_lo rhs(zsa * dsa * dt);
        R_lo lhs(a_abs[m + 1]);
        for (N k = 0; k < m + 1; ++k)
          lhs *= tmax;
        valid = lhs <= rhs;
      }
      break;

    case knighty2:
      { // knighty's second stopping condition
        vector<R_lo> a_abs(m + 2);
        for (N k = 0; k <= m + 1; ++k)
          a_abs[k] = abs(a_next[k]);
        R_lo sa(0);
        R_lo tmaxn(tmax);
        for (N k = 1; k <= m; ++k)
        {
          sa += a_abs[k] * tmaxn;
          tmaxn *= tmax;
        }
        R_lo rhs(sa);
        R_lo lhs(a_abs[m + 1]);
        for (N k = 0; k < m + 1; ++k)
          lhs *= tmax;
        valid = lhs <= R_lo(pow(2.0, -accuracy)) * rhs;
      }
      break;

    default:
      assert(! "valid stopping condition");
      break;
    }
    // advance
    if (valid)
    {
      n = n + 1;
      z = z_next;
      a = a_next;
      b = b_next;
    }
    return valid;
  };

  // keep stepping until invalid
  // TODO FIXME check escape?
  void run()
  {
    LOG(cout << "computing series approximation" << endl);
    while (step())
      if (n % 1000 == 0)
        LOG(cout << "\rseries approximation iteration " << n);
    LOG(cout << "\rseries approximation iteration " << n << " (complete)" << endl);
  };

  // initialize a pixel
  C_lo series_dz(const C_lo &dc1)
  {
    C_lo dcn(dc1);
    C_lo sum(0);
    for (N k = 1; k <= m; ++k)
    {
      sum += a[k] * dcn;
      dcn *= dc1;
    }
    return sum;
  };

  // initialize a pixel (derivative)
  C_lo series_dzdc(const C_lo &dc1)
  {
    C_lo dcn(1);
    C_lo sum(0);
    for (N k = 0; k <= m; ++k)
    {
      sum += b[k] * dcn;
      dcn *= dc1;
    }
    return sum;
  };

  // get a reference to carry on after series approximation initialisation
  reference get_reference()
  {
    return reference(n, z, c);
  };

};

// simple RGB24 image
class image
{
public:
  N width;
  N height;
  vector<uint8_t> rgb;

  // construct empty image
  image(N width, N height)
  : width(width)
  , height(height)
  , rgb(width * height * 3)
  { };

  // plot a point
  void plot(N x, N y, N r, N g, N b)
  {
    N k = (y * width + x) * 3;
    rgb[k++] = r;
    rgb[k++] = g;
    rgb[k++] = b;
  };

  // save to PPM format
  void save(const char *filename)
  {
#ifdef __EMSCRIPTEN__
    (void) filename;
    rgb.push_back(255);
    emscripten_worker_respond((char *) &rgb[0], width * height * 3 + 1);
#else
    FILE *out = fopen(filename, "wb");
    fprintf(out, "P6\n%d %d\n255\n", width, height);
    fwrite(&rgb[0], width * height * 3, 1, out);
    fflush(out);
    fclose(out);
#endif
  }
};

// wrap around image plot to plot distance estimator colouring
void plot(image &i, const perturbation &p, R_lo dt)
{
  N g(p.n & 255);
  if (! p.escaped && ! p.glitched)
    g = 255;
  if (p.escaped)
  {
    R_lo de(R_lo(2) * abs(p.dz) * R_lo(log(to_ld(abs(p.dz)))) / abs(p.dzdc));
    g = 255 * tanh(to_ld(de / dt));
  }
  i.plot(p.x, p.y, p.glitched ? 255 : g, g, g);
}

// sort perturbations by n, then within groups by |dz|
bool compare_glitched(const perturbation &p, const perturbation &q)
{
  if (p.n < q.n)
    return true;
  if (p.n > q.n)
    return false;
  return norm(p.dz) < norm(q.dz);
}

// solve glitches recursively
N solve_glitches(image &i, R_lo dt, N maxiters, const reference &r0, vector<perturbation> &glitches)
{
  N count(0);
  sort(glitches.begin(), glitches.end(), compare_glitched);
  // first glitch in groups of equal n has smallest |dz| (set to actual z)
  unsigned int k(0);
  while (k < glitches.size())
  {
    N n(glitches[k].n);
    // one iteration of Newton's method with low precision to find next reference
    // gives a few more bits
    C_hi c(r0.c + c_hi(glitches[k].dc) - c_hi(glitches[k].dz / glitches[k].dzdc));
    // rebase delta c against new reference
    C_lo dc(c_lo(c - r0.c));
    // new reference hits zero at period
    reference r(n, C_hi(0, 0), c); // C_hi needs two args otherwise gives NaN in Boost?
    r.run(maxiters);
    ++count;
    vector<perturbation> subglitches;
    while (k < glitches.size() && glitches[k].n == n)
    {
      // rebase to new reference
      const perturbation &p(glitches[k]);
      perturbation q(p.x, p.y, p.n, p.dc + dc, p.dz, p.dzdc);
      // iterate
      q.run(r, maxiters);
      // output
      if (q.glitched)
        subglitches.push_back(q);
      else
        plot(i, q, dt);
      ++k;
    }
    // recurse
    if (subglitches.size() > 0)
      count += solve_glitches(i, dt, maxiters, r, subglitches);
  }
  return count;
}

void render(N width, N height, N maxiters, N order, N stopn, N accuracy, N precision, const char *sre, const char *sim, const char *sradius, const char *filename)
{
  // prepare
  mpfr_float::default_precision(20u);
  R_lo radius(r_lo(sradius));
  N precision2 = fmax(24.0, 24.0 - double(logb(radius)));
  if (precision && precision < precision2)
    LOG(cout << "warning: insufficient precision: " << precision << " < " << precision2 << endl);
  else if (! precision)
    precision = precision2;
  LOG(cout << "precision bits " << precision << endl);
  precision *= log10(2.0);
  LOG(cout << "precision digits10 " << precision << endl);
  mpfr_float::default_precision(precision);
  C_hi c((R_hi(sre)), (R_hi(sim)));
  // find reference
  C_hi refc(c);
  boxperiod box(c, 2 * radius);
  N period(box.period(maxiters));
  if (! (period > 0))
    period = last_partial(c, maxiters);
  refc = newton_nucleus(c, period, 8); // FIXME best maxsteps value?
  C_lo dc0(c_lo(c - refc));
  LOG(cout << "reference delta " << dc0 << endl);
  // compute dt and tmax
  R_lo dt(radius / (height / 2));
  R_lo tmax(0);
  for (N y = 0; y < height; y += height - 1)
    for (N x = 0; x < width; x += width - 1)
    {
      C_lo dc(dc0 + R_lo(2) * radius *
        C_lo(width * ((x + 0.5) / width - 0.5) / height, (height - y - 0.5) / height - 0.5));
      R_lo t(abs(dc));
      tmax = max(tmax, t);
    }
  // render
  series_approximation::stopping stop;
  switch (stopn)
  {
    default:
    case 0: stop = series_approximation::knighty;  break;
    case 1: stop = series_approximation::quaz0r;   break;
    case 2: stop = series_approximation::knighty2; break;
  }
  series_approximation s(refc, dt, tmax, order, stop, accuracy);
  s.run();
  reference r(s.get_reference());
  r.run(maxiters);
  image i(width, height);
  N progress = 0;
  vector<perturbation> glitches;
  LOG(cout << "computing image" << endl);
  #pragma omp parallel for schedule(dynamic)
  for (N y = 0; y < height; ++y)
  {
    for (N x = 0; x < width; ++x)
    {
      C_lo dc(dc0 + R_lo(2) * radius *
        C_lo(width * ((x + 0.5) / width - 0.5) / height, (height - y - 0.5) / height - 0.5));
      perturbation p(x, y, r.n0, dc, s.series_dz(dc), s.series_dzdc(dc));
      p.run(r, maxiters);
      plot(i, p, dt);
      if (p.glitched)
      {
        #pragma omp critical
        glitches.push_back(p);
      }
    }
    #pragma omp critical
    LOG(cout << "\rimage scanline " << ++progress);
  }
  LOG(cout << "\rimage scanline " << progress << " (complete)" << endl);
  // correct glitches
  LOG(cout << "glitches " << glitches.size() << endl);
  N count(0);
  if (glitches.size() > 0)
  {
    LOG(cout << "solving glitches" << endl);
    count = solve_glitches(i, dt, maxiters, r, glitches);
  }
  LOG(cout << "secondary references " << count << endl);
  // save final image
  i.save(filename);
}

#ifdef __EMSCRIPTEN__

// worker thread worker function
extern "C" void EMSCRIPTEN_KEEPALIVE render(char *data, int size)
{
  N width;
  N height;
  N maxiters;
  N order;
  N stop;
  N accuracy;
  N precision;
  const char *sre;
  const char *sim;
  const char *sradius;
  const char *filename;
  unpack(data, size, width, height, maxiters, order, stop, accuracy, precision, sre, sim, sradius, filename);
  render(width, height, maxiters, order, stop, accuracy, precision, sre, sim, sradius, filename);
}

#else

// entry point
extern "C" int main(int argc, char **argv)
{
  // initial defaults
  N width = 640;
  N height = 360;
  N maxiters = 1 << 8;
  N order = 16;
  N stop = 0;
  N accuracy = 24;
  N precision = 0; // auto
  const char *sre = "-0.75";
  const char *sim = "0.0";
  const char *sradius = "1.5";
  const char *filename = "out.ppm";
  // parse arguments
  if (parse(argc, argv, width, height, maxiters, order, stop, accuracy, precision, sre, sim, sradius, filename))
    return 1;
  // render image
  render(width, height, maxiters, order, stop, accuracy, precision, sre, sim, sradius, filename);
  return 0;
}

#endif
