#version 130

#include "Progressive2D.frag"
#include "Complex.frag"
#info Mandelbrot
#group Mandelbrot

// Number of iterations
uniform int  Iterations; slider[10,200,1000]

bool cisnan(vec2 z)
{
  return isnan(z.x) || isnan(z.y);
}

bool newton_step(out vec2 dz_out, out vec2 z_out, vec2 z_guess, vec2 c, int period)
{
  vec2 z = z_guess;
  vec2 dz = vec2(1.0, 0.0);
  for (int i = 0; i < period; ++i)
  {
    vec2 e = cExp(z);
    dz = cMul(e, dz);
    z = e + c;
  }
  dz_out = dz;
  vec2 z_new = z_guess - cDiv(z - z_guess, dz - vec2(1.0, 0.0));
  z_out = cisnan(z_new) ? z_guess : z_new;
  return ! cisnan(z_new);
}

bool newton_steps(out vec2 dz_out, out vec2 z_out, vec2 z_guess, vec2 c, int period, int nsteps)
{
  vec2 dz = vec2(0.0, 0.0);
  vec2 z = z_guess;
  for (int i = 0; i < nsteps; ++i)
  {
    bool ok = newton_step(dz, z, z, c, period);
    if (!ok) return false;
  }
  dz_out = dz;
  z_out = z;
  return true;
}

bool interior_de(out float de_out, out vec2 dz_out, vec2 z, vec2 c, int p, int steps) {
  vec2 dz0 = vec2(0.0, 0.0);
  vec2 z0 = vec2(0.0, 0.0);
  bool ok = newton_steps(dz0, z0, z, c, p, steps);
  if (! ok) return false;
  if (cNorm(dz0) <= 1.0) {
    vec2 z1 = z0;
    vec2 dz1 = vec2(1.0, 0.0);
    vec2 dzdz1 = vec2(0.0, 0.0);
    vec2 dc1 = vec2(0.0, 0.0);
    vec2 dcdz1 = vec2(0.0, 0.0);
    for (int j = 0; j < p; ++j) {
      vec2 e = cExp(z1);
      dcdz1 = cMul(e, dcdz1 + cMul(dc1, dz1));
      dc1 = cMul(e, dc1) + vec2(1.0, 0.0);
      dzdz1 = cMul(e, dzdz1 + cMul(dz1, dz1));
      dz1 = cMul(e, dz1);
      z1 = e + c;
    }
    de_out = (1.0 - cNorm(dz1)) / cAbs(dcdz1 + cDiv(cMul(dzdz1, dc1), vec2(1.0, 0.0) - dz1));
    dz_out = dz1;
    return de_out > 0.0;
  }
  return false;
}

void hsv2rgb(float h, float s, float v, out float red, out float grn, out float blu)
{
  h -= floor(h);
  float hue = h * 6.0;
  float sat = s;
  float bri = v;
  int i = int(floor(hue));
  float f = hue - float(i);
  if (! ((i & 1) == 1)) f = 1.0 - f;
  float m = bri * (1.0 - sat);
  float n = bri * (1.0 - sat * f);
  float r = 0, g = 0, b = 0;
	switch (i)
	{
	case 6:
	case 0: b = bri;
		g = n;
		r = m;
		break;
	case 1: b = n;
		g = bri;
		r = m;
		break;
	case 2: b = m;
		g = bri;
		r = n;
		break;
	case 3: b = m;
		g = n;
		r = bri;
		break;
	case 4: b = n;
		g = m;
		r = bri;
		break;
	case 5: b = bri;
		g = m;
		r = n;
    break;
	}
  red = b;
  grn = g;
  blu = r;
}

vec3 color(vec2 c)
{
  float phi = (sqrt(5.0) + 1.0) / 2.0;
  float px = length(vec4(dFdx(c), dFdy(c)));
  vec2 dz = vec2(0.0, 0.0);
  float mz = 1.0 / 0.0;
  int period = 0;
  float de = 0;
  vec2 z1 = c;
  vec2 z2 = c;
  int k;
  for (k = 1; k < Iterations; ++k)
      {
        z1 = cExp(z1) + c;
        z2 = cExp(z2) + c;
        z2 = cExp(z2) + c;
        if (cisnan(z2)) break;
        float d = cNorm(z1 - z2) / cAbs(cMul(z1, z2));
        if (d < mz)
        {
          mz = d;
	  if (interior_de(de, dz, z2, c, k, 64))
	  {
	    period = k;
	    break;
	  }
        }
      }
      float h = float(period - 1) * phi / 24.0;
      float s = period <= 0 ? 0.0 : 0.5;
      float v = period <= 0 ? k == Iterations ? 1.0 : 0.0 : tanh(clamp(de / px, 0.0, 4.0));
      float r, g, b;
      hsv2rgb(h, s, v, r, g, b);
      return vec3(r, g, b);
}
