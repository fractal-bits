// gcc -std=c99 -Wall -Wextra -pedantic -O3 -fopenmp -march=native -o z-to-exp-z-plus-c z-to-exp-z-plus-c.c -lm

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int rand_r(unsigned int *seedp);


typedef float R;
typedef float _Complex C;
#define creal crealf
#define cimag cimagf
#define cabs cabsf
#define cexp cexpf
#define floor floorf

static R cnorm(C z)
{
  return creal(z) * creal(z) + cimag(z) * cimag(z);
}

static R cisnan(C z)
{
  return isnan(creal(z)) || isnan(cimag(z));
}

static bool newton_step(C *dz_out, C *z_out, C z_guess, C c, int period)
{
  C z = z_guess;
  C dz = 1;
  for (int i = 0; i < period; ++i)
  {
    C e = cexp(z);
    dz = e * dz;
    z = e + c;
  }
  *dz_out = dz;
  C z_new = z_guess - (z - z_guess) / (dz - 1);
  *z_out = cisnan(z_new) ? z_guess : z_new;
  return ! cisnan(z_new);
}

static bool newton_steps(C *dz_out, C *z_out, C z_guess, C c, int period, int nsteps)
{
  C dz = 0;
  C z = z_guess;
  for (int i = 0; i < nsteps; ++i)
  {
    bool ok = newton_step(&dz, &z, z, c, period);
    if (!ok) return false;
  }
  *dz_out = dz;
  *z_out = z;
  return true;
}

static bool interior_de(R *de_out, C *dz_out, C z, C c, int p, int steps) {
  C dz0 = 0;
  C z0 = 0;
  bool ok = newton_steps(&dz0, &z0, z, c, p, steps);
  if (! ok) return false;
  if (cnorm(dz0) <= 1) {
    C z1 = z0;
    C dz1 = 1;
    C dzdz1 = 0;
    C dc1 = 0;
    C dcdz1 = 0;
    for (int j = 0; j < p; ++j) {
      C e = cexp(z1);
      dcdz1 = e * (dcdz1 + dc1 * dz1);
      dc1 = e * dc1 + 1;
      dzdz1 = e * (dzdz1 + dz1 * dz1);
      dz1 = e * dz1;
      z1 = e + c;
    }
    *de_out = (1 - cnorm(dz1)) / cabs(dcdz1 + dzdz1 * dc1 / (1 - dz1));
    *dz_out = dz1;
    return *de_out > 0;
  }
  return false;
}

static void hsv2rgb(R h, R s, R v, R *red, R *grn, R *blu)
{
  h -= floor(h);
  R hue = h * 6;
  R sat = s;
  R bri = v;
	int i = (int) floor(hue);
	R f = hue - i;
	if (! (i & 1))
		f = 1 - f;
	R m = bri * (1 - sat);
	R n = bri * (1 - sat * f);
  R r = 0, g = 0, b = 0;
	switch (i)
	{
	case 6:
	case 0: b = bri;
		g = n;
		r = m;
		break;
	case 1: b = n;
		g = bri;
		r = m;
		break;
	case 2: b = m;
		g = bri;
		r = n;
		break;
	case 3: b = m;
		g = n;
		r = bri;
		break;
	case 4: b = n;
		g = m;
		r = bri;
		break;
	case 5: b = bri;
		g = m;
		r = n;
    break;
	}
  *red = b;
  *grn = g;
  *blu = r;
}

extern int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  srand(time(0));
  int sup = 1;
  int sub = 1;
  int width = 1920 * sup / sub;
  int height = 1080 * sup / sub;
  int bytes = width * height * 3;
  unsigned char *ppm = malloc(bytes);
  int maxiters = 6000;
  C c0 = 0.75 + I * 1.5;
  C r0 = 0.05;
  R phi = (sqrt(5) + 1) / 2;
  R px = r0 * 2 / height;
  #pragma omp parallel for schedule(static, 1)
  for (int j = 0; j < height; ++j)
  {
    unsigned int seed = rand();
    for (int i = 0; i < width; ++i)
    {
      R x = rand_r(&seed) / (R) RAND_MAX - 0.5;
      R y = rand_r(&seed) / (R) RAND_MAX - 0.5;
      int q = (j * width + i) * 3;
      C c = c0 + r0 * (((i + 0.5 + x) / width - 0.5) * width * 2.0 / height + I * ((j + 0.5 + y) / height - 0.5) * 2.0);
      C dz = 0;
      R mz = 1.0 / 0.0;
      int period = 0;
      R de = 0;
      C z1 = c;
      C z2 = c;
      for (int k = 1; k < maxiters; ++k)
      {
        z1 = cexp(z1) + c;
        z2 = cexp(z2) + c;
        z2 = cexp(z2) + c;
	if (cisnan(z2))
	{
	  break;
	}
        R d = cnorm(z1 - z2) / cabs(z1 * z2);
        if (d < mz)
        {
          mz = d;
	  if (interior_de(&de, &dz, z2, c, k, 64))
	  {
	    period = k;
	    break;
	  }
        }
      }
      R h = (period - 1) * phi / 24;
      R s = period <= 0 ? 0 : 0.5;
      R v = period <= 0 ? 0 : tanh(0.25 * de / px);
      R r, g, b;
      hsv2rgb(h, s, v, &r, &g, &b);
      ppm[q++] = 255 * r;
      ppm[q++] = 255 * g;
      ppm[q++] = 255 * b;
    }
  }
  printf("P6\n%d %d\n255\n", width, height);
  fwrite(ppm, bytes, 1, stdout);
  free(ppm);
  return 0;
}
