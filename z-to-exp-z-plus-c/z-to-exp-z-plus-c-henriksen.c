// gcc -std=c99 -Wall -Wextra -pedantic -O3 -fopenmp -march=native -o z-to-exp-z-plus-c z-to-exp-z-plus-c.c -lm

#include <complex.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int rand_r(unsigned int *seedp);

long double cnorml(long double _Complex z)
{
  return creall(z) * creall(z) + cimagl(z) * cimagl(z);
}

long double cisnanl(long double _Complex z)
{
  return isnan(creall(z)) || isnan(cimagl(z));
}

bool newton_step(long double _Complex *dz_out, long double _Complex *z_out, long double _Complex z_guess, long double _Complex c, int period)
{
  long double _Complex z = z_guess;
  long double _Complex dz = 1;
  for (int i = 0; i < period; ++i)
  {
    long double _Complex e = cexpl(z);
    dz = e * dz;
    z = e + c;
  }
  *dz_out = dz;
  long double _Complex z_new = z_guess - (z - z_guess) / (dz - 1);
  *z_out = cisnanl(z_new) ? z_guess : z_new;
  return ! cisnanl(z_new);
}

bool newton_steps(long double _Complex *dz_out, long double _Complex *z_out, long double _Complex z_guess, long double _Complex c, int period, int nsteps)
{
  long double _Complex dz = 0;
  long double _Complex z = z_guess;
  for (int i = 0; i < nsteps; ++i)
  {
    bool ok = newton_step(&dz, &z, z, c, period);
    if (!ok) return false;
  }
  *dz_out = dz;
  *z_out = z;
  return true;
}

bool interior_de(long double *de_out, long double _Complex *dz_out, long double _Complex z, long double _Complex c, int p, int steps) {
  long double _Complex dz0 = 0;
  long double _Complex z0 = 0;
  bool ok = newton_steps(&dz0, &z0, z, c, p, steps);
  if (! ok) return false;
  if (cnorml(dz0) <= 1) {
    long double _Complex z1 = z0;
    long double _Complex dz1 = 1;
    long double _Complex dzdz1 = 0;
    long double _Complex dc1 = 0;
    long double _Complex dcdz1 = 0;
    for (int j = 0; j < p; ++j) {
      long double _Complex e = cexpl(z1);
      dcdz1 = e * (dcdz1 + dc1 * dz1);
      dc1 = e * dc1 + 1;
      dzdz1 = e * (dzdz1 + dz1 * dz1);
      dz1 = e * dz1;
      z1 = e + c;
    }
    *de_out = (1 - cnorml(dz1)) / cabsl(dcdz1 + dzdz1 * dc1 / (1 - dz1));
    *dz_out = dz1;
    return *de_out > 0;
  }
  return false;
}

void hsv2rgb(long double h, long double s, long double v, long double *red, long double *grn, long double *blu)
{
  h -= floor(h);
  long double hue = h * 6;
  long double sat = s;
  long double bri = v;
	int i = (int) floorl(hue);
	long double f = hue - i;
	if (! (i & 1))
		f = 1 - f;
	long double m = bri * (1 - sat);
	long double n = bri * (1 - sat * f);
  long double r = 0, g = 0, b = 0;
	switch (i)
	{
	case 6:
	case 0: b = bri;
		g = n;
		r = m;
		break;
	case 1: b = n;
		g = bri;
		r = m;
		break;
	case 2: b = m;
		g = bri;
		r = n;
		break;
	case 3: b = m;
		g = n;
		r = bri;
		break;
	case 4: b = n;
		g = m;
		r = bri;
		break;
	case 5: b = bri;
		g = m;
		r = n;
    break;
	}
  *red = b;
  *grn = g;
  *blu = r;
}

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  srand(time(0));
  int sup = 1;
  int sub = 1;
  int width = 1920 * sup / sub;
  int height = 1080 * sup / sub;
  int bytes = width * height * 3;
  unsigned char *ppm = malloc(bytes);
  int maxiters = 100;
  long double _Complex c0 = 0.75 + I * 1.5;
  long double r0 = 0.05;
  long double px = r0 * 2 / height;
  #pragma omp parallel for
  for (int j = 0; j < height; ++j)
  {
    unsigned int seed = rand();
    for (int i = 0; i < width; ++i)
    {
      long double x = rand_r(&seed) / (long double) RAND_MAX - 0.5;
      long double y = rand_r(&seed) / (long double) RAND_MAX - 0.5;
      int q = (j * width + i) * 3;
      long double _Complex c = c0 + r0 * (((i + 0.5 + x) / width - 0.5) * width * 2.0 / height + I * ((j + 0.5 + y) / height - 0.5) * 2.0);
      long double _Complex z = c;
      long double _Complex dc = 1;
      long double _Complex dz = 1;
      long double l = 0;
      int k = 0;
      for (k = 0; k < maxiters; ++k)
      {
        long double _Complex e = cexpl(z);
        l += logl(cnorml(e));
        dz = e * dz;
	      dc = e * dc + 1;
        z = e + c;
        if (cnorml(dz) < 1.0e-6L) { k = -1; break; }
        if (cnorml(z + c) < cnorml(px * 0.1 * (dc + 1))) break;
      }
      l *= 0.5L / (k + 1);
      long double h = 0;
      long double s = 0;
      long double v = l < 0;
      long double r, g, b;
      hsv2rgb(h, s, v, &r, &g, &b);
      ppm[q++] = 255 * r;
      ppm[q++] = 255 * g;
      ppm[q++] = 255 * b;
    }
  }
  printf("P6\n%d %d\n255\n", width, height);
  fwrite(ppm, bytes, 1, stdout);
  free(ppm);
  return 0;
}
